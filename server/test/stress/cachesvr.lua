--message.lua
import("kernel/cache/cache_agent.lua")

local new_guid      = guid.new
local tinsert       = table.insert
local log_err       = logger.err
local check_failed  = utility.check_failed

local timer_mgr     = quanta.get("timer_mgr")
local cache_agent   = quanta.get("cache_agent")
local thread_mgr    = quanta.get("thread_mgr")

local PeriodTime    = enum("PeriodTime")

local CacheSvrTest = singleton()
function CacheSvrTest:__init()
    self.tick_cnt                 = 0
    self.last_time                = quanta.now
    self.begin_time               = quanta.now  -- 用于统计累计耗时
    self.insert_success_cnt       = 0
    self.insert_cnt               = 0
    self.insert_total_success_cnt = 0
    self.insert_total_cnt         = 0
    self.find_success_cnt         = 0
    self.find_cnt                 = 0
    self.find_total_success_cnt   = 0
    self.find_total_cnt           = 0
    self.update_success_cnt       = 0
    self.update_cnt               = 0
    self.update_total_success_cnt = 0
    self.update_total_cnt         = 0
    self.tid2tname          = {}  -- 表
    self.tid2kindex         = {}  -- 每个表的组件使用索引位置
    for n = 1, 50 do
        self.tid2tname[n]  = string.format("test_table_%d", n)
        self.tid2kindex[n] = 0
    end
    print("start: gen keys")
    self.keys    = {}       -- 查询建
    self.key_cnt = 5000000
    for n = 1, self.key_cnt do
        tinsert(self.keys, new_guid())
    end
    print("finish: gen keys")

    timer_mgr:once(PeriodTime.SECOND_MS, function ()
        self:start()
    end)
end

function CacheSvrTest:start()
    timer_mgr:loop(PeriodTime.SECOND_MS, function()
        self:update()
    end)
end

function CacheSvrTest:test_once()
    for tid, tname in pairs(self.tid2tname or {}) do
        local s_key = new_guid()
        -- 预生成的主键还未使用完
        if self.tid2kindex[tid] < self.key_cnt then
            s_key = self.keys[self.tid2kindex[tid] + 1]
            self.tid2kindex[tid] = self.tid2kindex[tid] + 1
        end

        local row_data = {
            s_key = s_key,
            filed1 = "123123dfadf",
            filed2 = "1231g23dfadf",
            filed3 = "12312d3dfadf",
            filed4 = "123h123dfadf",
            filed5 = "123123dsfadf",
            arr_field1 = {
                filed1 = "1233vds123dfadf",
                filed2 = "123wrewqv123dfadf",
                filed3 = "1231sbfse23dfadf",
                filed4 = "1231ssftynyk23dfadf",
                filed5 = "12312345tr3dfadf",
            },
            arr_field2 = {
                filed1 = "123123dertbbdgffadf",
                filed2 = "12312gdbsgfsdbdfadf",
                filed3 = "123123dfadf",
                filed4 = "12312bgsdf3dfadf",
                filed5 = "123123bdsgfddfadf",
                arr_field = {
                    filed1 = "123123gfdfgdfadf",
                    filed2 = "123bdgs123dfadf",
                    filed3 = "123123bsdfgdfadf",
                    filed4 = "1231sbdgf3dfadf",
                    filed5 = "12312bdsg3dfadf",
                },
            },
        }

        local ok = self:insert_test_once(tname, s_key, row_data)
        if ok then
            self:find_test_once(tname, s_key)
            self:update_test_once(tname, s_key, row_data)
        end
    end
end

function CacheSvrTest:insert_test_once(table_name, s_key, row_data)
    self.insert_cnt       = self.insert_cnt + 1
    self.insert_total_cnt = self.insert_total_cnt + 1

    local insert_cmd = {
        table_name = table_name,
        key_name   = "s_key",
        key_value  = s_key,
        row_data   = row_data,
    }
    local ec = cache_agent:insert(1, s_key, insert_cmd)
    if check_failed(ec) then
        log_err("[CacheSvrTest][insert_test_once] failed! ec=%s", ec)
        return false
    end

    self.insert_success_cnt       = self.insert_success_cnt + 1
    self.insert_total_success_cnt = self.insert_total_success_cnt + 1

    return true
end

function CacheSvrTest:find_test_once(table_name, s_key)
    self.find_cnt       = self.find_cnt + 1
    self.find_total_cnt = self.find_total_cnt + 1

    local find_cmd = {
        table_name = table_name,
        key_name   = "s_key",
        key_value  = s_key,
    }
    local ec = cache_agent:find(1, s_key, find_cmd)
    if check_failed(ec) then
        log_err("[CacheSvrTest][find_test_once] failed: code: %s", ec)
        return
    end

    self.find_success_cnt       = self.find_success_cnt + 1
    self.find_total_success_cnt = self.find_total_success_cnt + 1
end

function CacheSvrTest:update_test_once(table_name, s_key, row_data)
    self.update_cnt       = self.update_cnt + 1
    self.update_total_cnt = self.update_total_cnt + 1

    local update_cmd = {
        table_name = table_name,
        key_name   = "s_key",
        key_value  = s_key,
        row_data   = row_data,
    }
    local ec = cache_agent:update(1, s_key, update_cmd)
    if check_failed(ec) then
        log_err("[CacheSvrTest][update_test_once] failed: code: %s", ec)
        return
    end

    self.update_success_cnta      = self.update_success_cnt + 1
    self.update_total_success_cnt = self.update_total_success_cnt + 1
end

function CacheSvrTest:update()
    self.tick_cnt = self.tick_cnt + 1
    local function CacheSvrTest_test()
        for n = 1, 256 do
            self:test_once()
        end
    end
    thread_mgr:fork(CacheSvrTest_test)

    --print("insert: ", self.insert_success_cnt, self.insert_cnt)
    -- 每ns重置一次历史数据
    local cycle = 10
    local cost_sec = quanta.now - self.last_time
    if cost_sec > cycle then
        self.last_time = quanta.now
        print(">>> cachesvr:")
        print("total seconds: ", quanta.now - self.begin_time)
        print("since last tick cost seconds: ", cost_sec)
        print("insert: qps: ", self.insert_success_cnt / cycle, self.insert_cnt / cost_sec)
        print("find: qps: ", self.find_success_cnt / cycle, self.find_cnt / cost_sec)
        print("update: qps: ", self.update_success_cnt / cycle, self.update_cnt / cost_sec)
        self.insert_success_cnt = 0
        self.insert_cnt         = 0
        self.find_success_cnt   = 0
        self.find_cnt           = 0
        self.update_success_cnt = 0
        self.update_cnt         = 0
    end
end

quanta.cachesvr_test = CacheSvrTest()

return CacheSvrTest
