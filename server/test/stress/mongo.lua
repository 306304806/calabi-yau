--mongo.lua
import("kernel/store/mongo_mgr.lua")

local otime         = os.time
local new_guid      = guid.new
local log_err       = logger.err
local get_time_ms   = quanta.get_time_ms
local check_failed  = utility.check_failed

local timer_mgr     = quanta.get("timer_mgr")
local mongo_mgr     = quanta.get("mongo_mgr")
local thread_mgr    = quanta.get("thread_mgr")

local TIMER_PERIOD  = 1000

local MongoTest = singleton()
function MongoTest:__init()
    self.tick_cnt                 = 0
    self.last_time                = quanta.now
    self.begin_time               = quanta.now  -- 用于统计累计耗时
    self.insert_success_cnt       = 0
    self.insert_cnt               = 0
    self.insert_cost_ms           = 0
    self.insert_total_success_cnt = 0
    self.insert_total_cnt         = 0
    self.find_success_cnt         = 0
    self.find_cnt                 = 0
    self.find_cost_ms             = 0
    self.find_total_success_cnt   = 0
    self.find_total_cnt           = 0
    self.update_success_cnt       = 0
    self.update_cnt               = 0
    self.update_cost_ms           = 0
    self.update_total_success_cnt = 0
    self.update_total_cnt         = 0
    self.test_total_cnt           = 0
    self.tid2tname                = {}
    for n = 1, 50 do
        self.tid2tname[n] = string.format("test_table_%d", n)
    end

    timer_mgr:once(TIMER_PERIOD, function ()
        self:start()
    end)

end

function MongoTest:start()
    timer_mgr:loop(TIMER_PERIOD, function()
        self:update()
    end)
end

function MongoTest:insert_test()
    for tid, tname in pairs(self.tid2tname or {}) do
        self.insert_cnt       = self.insert_cnt + 1
        self.insert_total_cnt = self.insert_total_cnt + 1
        self.test_total_cnt   = self.test_total_cnt + 1

        local s_key = new_guid()
        local row_data = {
            s_key = s_key,
            filed1 = "123123dfadf",
            filed2 = "1231g23dfadf",
            filed3 = "12312d3dfadf",
            filed4 = "123h123dfadf",
            filed5 = "123123dsfadf",
            arr_field1 = {
                filed1 = "1233vds123dfadf",
                filed2 = "123wrewqv123dfadf",
                filed3 = "1231sbfse23dfadf",
                filed4 = "1231ssftynyk23dfadf",
                filed5 = "12312345tr3dfadf",
            },
            arr_field2 = {
                filed1 = "123123dertbbdgffadf",
                filed2 = "12312gdbsgfsdbdfadf",
                filed3 = "123123dfadf",
                filed4 = "12312bgsdf3dfadf",
                filed5 = "123123bdsgfddfadf",
                arr_field = {
                    filed1 = "123123gfdfgdfadf",
                    filed2 = "123bdgs123dfadf",
                    filed3 = "123123bsdfgdfadf",
                    filed4 = "1231sbdgf3dfadf",
                    filed5 = "12312bdsg3dfadf",
                },
            },
        }

        local call_start_ms = get_time_ms()
        local ec = mongo_mgr:game_insert(1, tname, row_data)
        if check_failed(ec) then
            log_err("[MongoTest][insert_test] failed! ec=%s", ec)
            return
        end

        self.insert_cost_ms           = self.insert_cost_ms + get_time_ms() - call_start_ms
        self.insert_success_cnt       = self.insert_success_cnt + 1
        self.insert_total_success_cnt = self.insert_total_success_cnt + 1
    end
end

function MongoTest:find_test()
    for tid, tname in pairs(self.tid2tname or {}) do
        self.find_cnt       = self.find_cnt + 1
        self.find_total_cnt = self.find_total_cnt + 1
        self.test_total_cnt   = self.test_total_cnt + 1

        local call_start_ms = get_time_ms()
        local ec = mongo_mgr:game_find_one(1, tname, {s_key = tid})
        if check_failed(ec) then
            log_err("[MongoTest][find_test] failed: code: %s", ec)
            return
        end

        self.find_cost_ms           = self.find_cost_ms + get_time_ms() - call_start_ms
        self.find_success_cnt       = self.find_success_cnt + 1
        self.find_total_success_cnt = self.find_total_success_cnt + 1
    end
end

function MongoTest:update_test()
    for tid, tname in pairs(self.tid2tname or {}) do
        self.update_cnt       = self.update_cnt + 1
        self.update_total_cnt = self.update_total_cnt + 1
        self.test_total_cnt   = self.test_total_cnt + 1

        local s_key = new_guid()
        local row_data = {
            s_key = s_key,
            filed1 = "123123dfadf",
            filed2 = "1231g23dfadf",
            filed3 = "12312d3dfadf",
            filed4 = "123h123dfadf",
            filed5 = "123123dsfadf",
            arr_field1 = {
                filed1 = "1233vds123dfadf",
                filed2 = "123wrewqv123dfadf",
                filed3 = "1231sbfse23dfadf",
                filed4 = "1231ssftynyk23dfadf",
                filed5 = "12312345tr3dfadf",
            },
            arr_field2 = {
                filed1 = "123123dertbbdgffadf",
                filed2 = "12312gdbsgfsdbdfadf",
                filed3 = "123123dfadf",
                filed4 = "12312bgsdf3dfadf",
                filed5 = "123123bdsgfddfadf",
                arr_field = {
                    filed1 = "123123gfdfgdfadf",
                    filed2 = "123bdgs123dfadf",
                    filed3 = "123123bsdfgdfadf",
                    filed4 = "1231sbdgf3dfadf",
                    filed5 = "12312bdsg3dfadf",
                },
            },
        }

        local call_start_ms = get_time_ms()
        local ec = mongo_mgr:game_update(1, tname, row_data, {s_key = s_key})
        if check_failed(ec) then
            log_err("[MongoTest][update_test] failed: code: %s", ec)
            return false
        end

        self.update_cost_ms           = self.update_cost_ms + get_time_ms() - call_start_ms
        self.update_success_cnt       = self.update_success_cnt + 1
        self.update_total_success_cnt = self.update_total_success_cnt + 1
    end
end

function MongoTest:update()
    if not self._start then
        print("start")
        self._start = true
        self.begin_time = otime()
        self.last_time  = otime()
    else
        print("finish")
        return
    end

    self.tick_cnt = self.tick_cnt + 1
    local function MongoTest_test()
        for n = 1, 7000 do
            --self:insert_test()
            --self:find_test()
            self:update_test()
        end

        self:trace()
    end
    thread_mgr:fork(MongoTest_test)
end

function MongoTest:trace()
    local cost_sec = otime() - self.last_time

    --tick计数器不准了，直接用时钟
        self.last_time   = otime()
        local total_time = otime() - self.begin_time
        print(">>> mongo: count, second: ", self.test_total_cnt, cost_sec)
        print("total seconds: ", total_time)
        print("since last tick cost seconds: ", cost_sec)
        print("insert: qps, mspq, success_percent, qps_avg ",
            self.insert_cnt / cost_sec,
            self.insert_cost_ms / self.insert_cnt,
            self.insert_success_cnt / self.insert_cnt,
            self.insert_total_cnt / total_time)
        print("find: qps, mspq, success_percent, qps_avg ",
            self.find_cnt / cost_sec,
            self.find_cost_ms / self.find_cnt,
            self.find_success_cnt / self.find_cnt,
            self.find_total_cnt / total_time)
        print("update: qps, mspq, success_percent, qps_avg ",
            self.update_cnt / cost_sec,
            self.update_cost_ms / self.update_cnt,
            self.update_success_cnt / self.update_cnt,
            self.update_total_cnt / total_time)
        self.insert_success_cnt = 0
        self.insert_cnt         = 0
        self.insert_cost_ms     = 0
        self.find_success_cnt   = 0
        self.find_cnt           = 0
        self.find_cost_ms       = 0
        self.update_success_cnt = 0
        self.update_cnt         = 0
        self.update_cost_ms     = 0
end

quanta.mongo_test = MongoTest()

return MongoTest
