--logger.lua
--日志测试

local log_info      = logger.info
local get_time_ms   = quanta.get_time_ms

local function logger_test(cycle)
    local t1 = get_time_ms()
    for i = 1, cycle do
        log_info("logger_test : now output logger cycle : %d", i)
    end
    local t2 = get_time_ms()
    return t2 -t1
end

local params = { 10000, 50000, 100000, 200000, 500000, 1000000}
for _, cycle in pairs(params) do
    local time = logger_test(cycle)
    print(string.format("logger_test: cycle %d use time %s ms!", cycle, time))
end

logger.close()

print("logger test end")

os.exit()

