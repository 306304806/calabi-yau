--message.lua
local new_guid      = guid.new

local router_mgr    = quanta.get("router_mgr")
local thread_mgr    = quanta.get("thread_mgr")

local MessageTest = singleton()

function MessageTest:__init()
    self.player_id_list = {}
    for i = 1, 1000 do
        self.player_id_list[i] = new_guid(quanta.index)
    end
    quanta.join(self)
end

function MessageTest:update(frame)
    local function messageTest_test(cycle)
        local quanta_id = quanta.id
        local player_id_list = self.player_id_list
        for n = 1, 1 do
            local player_id = player_id_list[n]
            router_mgr:call_index_hash(player_id, "rpc_player_login", player_id, quanta_id)
        end
    end
    thread_mgr:fork(messageTest_test)
end

quanta.msg_test = MessageTest()

return MessageTest
