
import("api/plat/plat_api.lua")

local plat_api      = quanta.get("plat_api")
local thread_mgr    = quanta.get("thread_mgr")

local SafeTest = singleton()

function SafeTest:__init()
    self:setup()
end

function SafeTest:setup()
    thread_mgr:fork(function()
        thread_mgr:sleep(3000)
        local code = plat_api:fliterword_check(1, "我是温家宝，没带蒙汗药")
        print(code)
        local ok, res = plat_api:fliterword_trans(1, "我是温家宝，没带蒙汗药", "X")
        print(ok, res.content)
    end)
end

-- export
quanta.safe_test = SafeTest()

return SafeTest

