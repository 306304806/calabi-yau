-- mongo_test.lua

import("api/steam/steam_api.lua")

local thread_mgr    = quanta.get("thread_mgr")
local steam_api     = quanta.get("steam_api")

--local STEAM_APP_ID        = cfg_mgr:get_utility_by_key("steam_app_id")
--local STEAM_WEB_KEY       = cfg_mgr:get_utility_by_key("steam_web_key")

local SteamApiTest = singleton()

function SteamApiTest:__init()
    self:setup()
end

function SteamApiTest:setup()
    thread_mgr:fork(function()
        while true do
            thread_mgr:sleep(3000)
            local ec = steam_api:auth_user_token("76561199044825594", "")
            print(ec)
        end
    end)
end

-- export
quanta.steam_test = SteamApiTest()

return SteamApiTest

