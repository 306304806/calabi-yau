-- check_id_test.lua

local encrypt       = require("encrypt")
local ljson         = require("luacjson")

local log_info      = logger.info
local log_err       = logger.err
local serialize     = logger.serialize
local json_decode   = ljson.decode

local router_mgr    = quanta.get("router_mgr")
local thread_mgr    = quanta.get("thread_mgr")
local app_id        = "hpb8b7c61bc464fde4"
local secret        = "ab4ba081cbf45f1f"
local origin_url    = "https://testapi.hope.qq.com/cgi-bin/Hope.fcgi"

local CheckIDTest = singleton()

function CheckIDTest:__init()
    self:setup()
end

function CheckIDTest:setup()
    thread_mgr:fork(function()
        while true do
            thread_mgr:sleep(5000)
            local str_timestamp = tostring(os.time())
            local str_nonce = tostring(math.random(1000, 999999999))
            local temp_str = "appid=" .. app_id .. "&cmd=zxCheckIdCard&nonce=" .. str_nonce .. "&timestamp=" .. str_timestamp .. secret
            local flag, sign_str = encrypt.md5str(temp_str)
            log_info("flag:%s, sign_str:%s", flag, sign_str)
            if flag then
                local post_req = {
                    ["name"] = "乐淳雅",
                    ["certId"] = "130928198905281793",
                    ["context"] = "blabla",
                    ["authInfo"] = {
                      ["authUserType"] = 0,
                      ["authAppid"] = app_id,
                      ["authUserId"] = "24347382",
                      ["authKey"] = "244fe73ab2343803ce",
                    }
                }

                local headers = {
                    ["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8",
                }

                local params = {
                    ["cmd"] = "zxCheckIdCard",
                    ["appid"] = app_id,
                    ["nonce"] = str_nonce,
                    ["timestamp"] = str_timestamp,
                    ["sign"] = sign_str,
                }

                post_req = ljson.encode(post_req)
                local ok, code, post_res = router_mgr:call_proxy_hash(quanta.id, "rpc_http_post", origin_url, params, post_req, headers)

                if not ok or 200 ~= code then
                    log_err("CheckIDTest failed! ")
                    break
                end

                ok, post_res = pcall(json_decode, post_res)
                log_info("post_res->ok:%s, res:%s", ok, serialize(post_res))
                break
            end
        end
    end)
end

-- export
quanta.check_id_test = CheckIDTest()

return CheckIDTest

