-- robot_mgr.lua
-- @author: errorcpp@qq.com
-- @date:   2019-07-22
local Robot         = import("robot/robot.lua")
local ROBOT_CFGS    = import("robot/config/robot_cfg.lua")
local ROBOT_ENVS    = import("robot/config/robot_env.lua")

local guid_string   = guid.string
local sformat       = string.format
local log_debug     = logger.debug
local env_number    = environ.number

local PeriodTime    = enum("PeriodTime")

local timer_mgr     = quanta.get("timer_mgr")
local report_mgr    = quanta.get("report_mgr")
local thread_mgr    = quanta.get("thread_mgr")

local RobotMgr = singleton()
local prop = property(RobotMgr)
prop:accessor("robot_list", {})    --robot_list

function RobotMgr:__init()
    self:setup()
end

-- setup
function RobotMgr:setup()
    -- 测试用例配置方案
    local case_id = env_number("QUANTA_CASE", quanta.index)
    local conf = ROBOT_CFGS[case_id]
    local count = env_number("QUANTA_COUNT")
    if count then
        conf.count = count
    end
    local start = env_number("QUANTA_START")
    if start then
        conf.start = start
    end
    --指定账号模式，只能一个机器人
    if conf.openid_type == 2 then
        conf.count = 1
    end
    -- 配置上报管理器
    report_mgr:setup(conf.count)
    -- 创建机器人
    timer_mgr:once(PeriodTime.SECOND_MS, function()
        for index = 1, conf.count do
            self:create_robot(conf, index)
        end
    end)
end

-- setup
function RobotMgr:create_robot(conf, index)
    log_debug("[RobotMgr][create_robot]: %d", index)
    local robot = Robot(conf, index)
    --读取环境变量
    local envs = ROBOT_ENVS[conf.environ]
    if conf.openid_type == 0 then
        robot:set_open_id(guid_string())
    elseif conf.openid_type == 1 then
        robot:set_open_id(sformat("%s_%d", envs.open_id, index))
    else
        robot:set_open_id(envs.open_id)
    end
    robot:set_dir_ip(envs.dir_ip)
    robot:set_plat_id(envs.plat_id)
    robot:set_dir_port(envs.dir_port)
    robot:set_access_token(envs.access_token)

    thread_mgr:fork(function()
        while robot:is_running() do
            local sleep_ms = robot:update()
            if sleep_ms > 0 then
                thread_mgr:sleep(sleep_ms)
            end
        end
    end)
    self.robot_list[index] = robot
end

function RobotMgr:get_robot(index)
    return self.robot_list[index]
end

quanta.robot_mgr = RobotMgr()

return RobotMgr
