-- logout.lua

local LogoutLobby    = import("robot/node/login/logout_lobby.lua")

local create_node   = luabt.create_node
local SEQUENCE      = luabt.NODE_TYPE.SEQUENCE

local LogoutNode = create_node(SEQUENCE,
    LogoutLobby()
)

return LogoutNode
