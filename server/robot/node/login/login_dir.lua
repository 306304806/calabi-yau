--login_dir.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local LoginDir = class()
function LoginDir:__init()
    self.name = "login_dir"
end

function LoginDir:open(tree)
    local robot = tree.robot
    if robot:get_lobby_info() then
        return SUCCESS
    end
end

function LoginDir:run(tree)
    local robot = tree.robot
    robot:report("login_dir_entry")
    local dir_agent = robot:get_dir_agent()
    local success = dir_agent:connect(robot.dir_ip, robot.dir_port, true)
    if success then
        dir_agent:send_get_area_list_req()
        return FAIL
    end
    robot:set_wait_time(500)
    return FAIL
end

function LoginDir:close()
end

return LoginDir
