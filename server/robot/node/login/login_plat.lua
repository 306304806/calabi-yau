--login_plat.lua
local tunpack   = table.unpack
local ssplit    = string_ext.split
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local LoginPlat = class()
function LoginPlat:__init()
    self.name = "login_plat"
end

function LoginPlat:open(tree)
    local robot = tree.robot
    if not robot:is_login_status() then
        return FAIL
    end
    if robot:is_platform_status() then
        return SUCCESS
    end
end

function LoginPlat:run(tree)
    local robot = tree.robot
    robot:report("login_plat_entry")
    local plat_url = robot:get_platform_url()
    local plat_agent = robot:get_plat_agent()
    local plat_ip, plat_port = tunpack(ssplit(plat_url, ":"))
    local success = plat_agent:connect(plat_ip, plat_port, true)
    if success then
        if plat_agent:send_login_platform_req() then
            return FAIL
        end
    end
    robot:set_wait_time(500)
    return FAIL
end

function LoginPlat:close()
end

return LoginPlat
