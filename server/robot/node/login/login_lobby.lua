--login_lobby.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local ncmdid    = ncmd_cs.NCmdId

local LoginLobby = class()
function LoginLobby:__init()
    self.name = "login_lobby"
end

function LoginLobby:open(tree)
    local robot = tree.robot
    local account_id = robot:get_account_id()
    if account_id and account_id > 0 then
        return SUCCESS
    end
end

function LoginLobby:run(tree)
    local robot = tree.robot
    robot:report("login_lobby_entry")
    local lobby_info = robot:get_lobby_info()
    local lobby_agent = robot:get_lobby_agent()
    local success = lobby_agent:connect(lobby_info.ip, lobby_info.port, true)
    if success then
        local ok = lobby_agent:send_login_req()
        if ok then
            if robot:wait_lobby(ncmdid.NID_PLAYER_SYNC_NTF, 1000) then
                return FAIL
            end
        end
    end
    robot:set_wait_time(500)
    return FAIL
end

function LoginLobby:close()
end

return LoginLobby
