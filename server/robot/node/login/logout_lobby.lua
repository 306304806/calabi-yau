--login_lobby.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local LogoutLobby = class()
function LogoutLobby:__init()
    self.name = "logout_lobby"
end

function LogoutLobby:open(tree)
    local robot = tree.robot
    if not robot:get_login_status() then
        return SUCCESS
    end
end

function LogoutLobby:run(tree)
    local robot = tree.robot
    local lobby_agent = robot:get_lobby_agent()
    lobby_agent:send_logout_req()
    return FAIL
end

function LogoutLobby:close()
end

return LogoutLobby
