--get_standings_info.lua
local SUCCESS   = luabt.BTConst.SUCCESS

local GetStandingsInfo = class()
function GetStandingsInfo:__init()
    self.name = "get_standings_info"
end

function GetStandingsInfo:open(tree)

end

function GetStandingsInfo:run(tree)
    local robot = tree.robot
    robot:send_get_standings_info_req(robot:get_player_id())
    return SUCCESS
end

function GetStandingsInfo:close()
end

return GetStandingsInfo