--get_standings_titles.lua
local SUCCESS   = luabt.BTConst.SUCCESS

local GetStandingsTitles = class()
function GetStandingsTitles:__init()
    self.name = "get_standings_titles"
end

function GetStandingsTitles:open(tree)
end

function GetStandingsTitles:run(tree)
    local robot = tree.robot
    robot:send_get_standings_titles_req(robot:get_player_id())
    return SUCCESS
end

function GetStandingsTitles:close()
end

return GetStandingsTitles