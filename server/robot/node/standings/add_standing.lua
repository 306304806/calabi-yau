--add_standings.lua
local SUCCESS   = luabt.BTConst.SUCCESS

local AddStandings = class()
function AddStandings:__init()
    self.name = "add_standings"
end

function AddStandings:open(tree)
end

function AddStandings:run(tree)
    local robot = tree.robot
    robot:send_add_standings_req(robot:get_player_id())

    robot:send_get_standings_info_req(robot:get_player_id())

    robot:send_get_standings_titles_req(robot:get_player_id())
    return SUCCESS
end

function AddStandings:close()
end

return AddStandings