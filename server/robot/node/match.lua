--match.lua

--team node
local LoginNode     = import("robot/node/login.lua")
local LeaderNode    = import("robot/node/team/leader.lua")
local TeamCreate    = import("robot/node/team/create.lua")
local TeamInvite    = import("robot/node/team/invite.lua")
local TeamReady     = import("robot/node/team/ready.lua")

--match node
local MatchJoin     = import("robot/node/match/join.lua")
local MatchQuit     = import("robot/node/match/quit.lua")

local create_node   = luabt.create_node
local SEQUENCE      = luabt.NODE_TYPE.SEQUENCE
local CONDITION     = luabt.NODE_TYPE.CONDITION

local MatchNode = create_node(SEQUENCE,
    LoginNode,
    create_node(CONDITION,
        LeaderNode(),
        create_node(SEQUENCE,
            TeamCreate(),
            TeamInvite(),
            MatchJoin(),
            MatchQuit()
        ),
        create_node(SEQUENCE,
            TeamReady()
        )
    )
)

return MatchNode