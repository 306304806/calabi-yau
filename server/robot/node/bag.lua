--bag.lua

local LoginNode             = import("robot/node/login.lua")
local AddItemNode           = import("robot/node/bag/add_item.lua")
local UseItemNode           = import("robot/node/bag/use_item.lua")
local UseAddditionCardNode  = import("robot/node/bag/use_addition_card.lua")
local UseModNameNode        = import("robot/node/bag/use_mod_name.lua")


local AddWeaponExpNode      = import("robot/node/weapon/add_weapon_exp.lua")
local EquipAttachmentNode   = import("robot/node/weapon/equip_attachment.lua")
local EquipWeaponNode       = import("robot/node/weapon/equip_weapon.lua")
local UnequipWeaponNode     = import("robot/node/weapon/unequip_weapon.lua")
local UnequipAttachNode     = import("robot/node/weapon/unequip_attachment.lua")
local UnlockAttachmentNode  = import("robot/node/weapon/unlock_attachment.lua")

local create_node   = luabt.create_node
local SEQUENCE      = luabt.NODE_TYPE.SEQUENCE

local BagNode = create_node(SEQUENCE,
    LoginNode,
    -- 添加姿势物品
    --AddItemNode(4101010, 1),

    -- 1.添加枪械物品
    AddItemNode(103010010, 10),

    -- 2.给指定角色指定武器背包指定位置装备枪械
    EquipWeaponNode(101, 1, 1, 10301001),

    -- 3.给指定武器id添加经验
    AddWeaponExpNode(10301001, 100000),

    -- 4.先加解锁配件所需物品
    AddItemNode(30001, 10),
    -- 5.再使用物品解锁枪械配件
    UnlockAttachmentNode(101, 1, 1, 1, 15, 30001),

    -- 6.装备配件
    EquipAttachmentNode(101, 1, 1, 1, 15),

    -- 7.卸下配件
    UnequipAttachNode(101, 1, 1, 1, 15),

    -- 8.卸下指定角色指定武器背包指定位置枪械
   UnequipWeaponNode(101, 1, 1),

    -- todo:
    -- 7.添加常规手雷
    --AddItemNode(12100001, 10),
    -- 10.装备常规手雷
    -- 8.添加闪光弹
    --AddItemNode(12200001, 10),
    -- 10.装备闪光弹
    -- 9.添加显影弹
    --AddItemNode(12300001, 10),
    -- 10.装备显影弹
    -- 11.卸下常规手雷

    -- 添加礼包道具
    AddItemNode(50003, 1),

    -- 使用礼包道具
    UseItemNode(50003, 1),

    -- 添加加成卡
    AddItemNode(10101, 5),

    -- 使用加成卡
    UseAddditionCardNode(10101, 3),

    -- 添加改名卡
    AddItemNode(10001, 3),

    -- 使用改名卡
    UseModNameNode(10001, "张三")
)

return BagNode
