--join.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local MatchJoin = class()
function MatchJoin:__init()
    self.name = "match_join"
end

function MatchJoin:open(tree)
    local robot = tree.robot
    if not robot:get_team_id() then
        return FAIL
    end
    if robot:get_room_id() then
        return SUCCESS
    end
end

function MatchJoin:run(tree)
    local robot = tree.robot
    robot:send_match_join_req()
    return FAIL
end

function MatchJoin:close()
end

return MatchJoin
