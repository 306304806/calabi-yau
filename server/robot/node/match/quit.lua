--quit.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local MatchQuit = class()
function MatchQuit:__init()
    self.name = "match_quit"
    self.quit = false
end

function MatchQuit:open(tree)
    local robot = tree.robot
    if self.quit then
        return SUCCESS
    end
    if not robot:get_team_id() then
        return FAIL
    end
    if not robot:get_room_id() then
        return SUCCESS
    end
end

function MatchQuit:run(tree)
    local robot = tree.robot
    robot:send_match_quit_req()
    self.quit = true
    return FAIL
end

function MatchQuit:close()
end

return MatchQuit
