--ready.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local TeamReady = class()
function TeamReady:__init()
    self.name = "team_ready"
end

function TeamReady:open(tree)
    local robot = tree.robot
    if not robot:get_team_id() then
        return FAIL
    end
    if robot:get_team_ready() then
        return SUCCESS
    end
end

function TeamReady:run(tree)
    local robot = tree.robot
    robot:send_ready_team_req()
    robot:set_wait_time(500)
    return FAIL
end

function TeamReady:close()
end

return TeamReady
