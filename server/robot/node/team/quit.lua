--quit.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local TeamQuit = class()
function TeamQuit:__init()
    self.name = "team_quit"
    self.quit = false
end

function TeamQuit:open(tree)
    if self.quit then
        return SUCCESS
    end
    local robot = tree.robot
    if not robot:get_team_id() then
        return FAIL
    end
end

function TeamQuit:run(tree)
    local robot = tree.robot
    robot:send_exit_team_req()
    self.quit = true
    return FAIL
end

function TeamQuit:close()
end

return TeamQuit
