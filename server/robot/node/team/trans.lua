--trans.lua
local tsize     = table_ext.size
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local TeamTrans = class()
function TeamTrans:__init()
    self.name = "team_trans"
    self.trans = false
end

function TeamTrans:open(tree)
    if self.trans then
        return SUCCESS
    end
    local robot = tree.robot
    if not robot:get_team_id() then
        return FAIL
    end
    local members = robot:get_team_members()
    if not robot:is_leader() or tsize(members) < 2 then
        return FAIL
    end
end

function TeamTrans:run(tree)
    local robot = tree.robot
    local player_id = robot:get_player_id()
    local members = robot:get_team_members()
    for _, member in pairs(members) do
        if player_id ~= member.player_id then
            robot:send_trans_leader_req(member.player_id)
            self.trans = true
            return FAIL
        end
    end
    return FAIL
end

function TeamTrans:close()
end

return TeamTrans
