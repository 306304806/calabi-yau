--leader.lua

local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local LeaderNode = class()
function LeaderNode:__init()
    self.name = "leader"
end

function LeaderNode:open(tree)
    local args = tree.args
    if args and args.leader then
        return SUCCESS
    end
    return FAIL
end

function LeaderNode:close()
end

return LeaderNode
