--create.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local TeamCreate = class()
function TeamCreate:__init()
    self.name = "team_create"
end

function TeamCreate:open(tree)
    local robot = tree.robot
    if robot:get_team_id() then
        return SUCCESS
    end
end

function TeamCreate:run(tree)
    local robot = tree.robot
    robot:send_create_team_req()
    return FAIL
end

function TeamCreate:close()
end

return TeamCreate
