--invite.lua
local tsize     = table_ext.size
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local robot_mgr = quanta.robot_mgr

local TeamInvite = class()
function TeamInvite:__init()
    self.name = "team_invite"
end

function TeamInvite:open(tree)
    local robot = tree.robot
    if not robot:get_team_id() then
        return FAIL
    end
    if robot:get_team_leader() ~= robot:get_player_id() then
        return FAIL
    end
    local args = tree.args
    local members = robot:get_team_members()
    if args and args.size == tsize(members) then
        return SUCCESS
    end
end

function TeamInvite:run(tree)
    local robot = tree.robot
    local index = robot:get_index()
    for idx = index + 1, index + 4 do
        local target = robot_mgr:get_robot(idx)
        if target and target:get_team_id() == 0 then
            robot:send_invite_team_req(target:get_player_id())
        end
    end
    robot:set_wait_time(1000)
    return FAIL
end

function TeamInvite:close()
end

return TeamInvite
