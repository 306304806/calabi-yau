--kick.lua
local tsize     = table_ext.size
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local TeamKick = class()
function TeamKick:__init()
    self.name = "team_kick"
    self.kick = false
end

function TeamKick:open(tree)
    if self.kick then
        return SUCCESS
    end
    local robot = tree.robot
    if not robot:get_team_id() then
        return FAIL
    end
    local members = robot:get_team_members()
    if not robot:is_leader() or tsize(members) < 2 then
        return FAIL
    end
end

function TeamKick:run(tree)
    local robot = tree.robot
    local player_id = robot:get_player_id()
    local members = robot:get_team_members()
    for _, member in pairs(members) do
        if member.player_id ~= player_id then
            robot:send_kick_member_req(member.player_id)
            self.kick = true
            return FAIL
        end
    end
    return FAIL
end

function TeamKick:close()
end

return TeamKick
