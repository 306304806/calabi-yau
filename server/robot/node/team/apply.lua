--apply.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local TeamApply = class()
function TeamApply:__init()
    self.name = "team_apply"
end

function TeamApply:open(tree)
    local robot = tree.robot
    if robot:get_team_id() then
        return SUCCESS
    end
end

function TeamApply:run(tree)
    local robot = tree.robot
    local players = robot:send_get_online_players()
    for _, member in pairs(players) do
        if member.team_id > 0 then
            robot:send_apply_member_req(member.team_id)
            robot:set_wait_time(1000)
            break
        end
    end
    return FAIL
end

function TeamApply:close()
end

return TeamApply
