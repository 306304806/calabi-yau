--claim_item.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local ClaimItemde = class()
function ClaimItemde:__init(payer_id, commodity_id, count)
    self.payer_id = payer_id
    self.commodity_id = commodity_id
    self.count = count
    self.name = "claim_item"
    self.req = false
end

function ClaimItemde:open(tree)
    if self.req then
        return SUCCESS
    end
end

function ClaimItemde:run(tree)
    local robot = tree.robot
    if robot:get_platform_status() then
        self.req = true
        robot:req_claim_commodity(self.payer_id, self.commodity_id, self.count)
        return FAIL
    end
end

function ClaimItemde:close()
end

return ClaimItemde
