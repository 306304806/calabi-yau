--req_commodity.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local ReqCommodityNode = class()
function ReqCommodityNode:__init()
    self.name = "req_commodity"
    self.req = false
end

function ReqCommodityNode:open(tree)
    if self.req then
        return SUCCESS
    end
end

function ReqCommodityNode:run(tree)
    local robot = tree.robot
    if robot:get_platform_status() then
        self.req = true
        robot:req_all_commodity()
        logger.debug("ReqCommodityNode:run")
        return FAIL
    end
end

function ReqCommodityNode:close()
end

return ReqCommodityNode
