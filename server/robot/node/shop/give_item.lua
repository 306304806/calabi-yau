--give_item.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local GiveItemde = class()
function GiveItemde:__init(reciver_id, commodity_id, count)
    self.reciver_id     = reciver_id
    self.commodity_id   = commodity_id
    self.count      = count
    self.name       = "give_item"
    self.req        = false
end

function GiveItemde:open(tree)
    if self.req then
        return SUCCESS
    end
end

function GiveItemde:run(tree)
    local robot = tree.robot
    if robot:get_platform_status() then
        self.req = true
        robot:req_give_commodity(self.reciver_id, self.commodity_id, self.count)
        return FAIL
    end
end

function GiveItemde:close()
end

return GiveItemde
