--self_buy.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local SelfBuyNode = class()
function SelfBuyNode:__init(id, count, pay_type)
    self.name = "self_buy"
    self.commodity_id = id
    self.count = count
    self.pay_type = pay_type
    self.req = false
end

function SelfBuyNode:open(tree)
    if self.req then
        return SUCCESS
    end
end

function SelfBuyNode:run(tree)
    local robot = tree.robot
    if robot:get_platform_status() then
        robot:req_self_buy_commodity(self.commodity_id, self.count, self.pay_type)
        self.req = true
        return FAIL
    end
end

function SelfBuyNode:close()
end

return SelfBuyNode
