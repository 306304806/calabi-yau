--rank.lua

local LoginNode             = import("robot/node/login.lua")
local ReqCommodityNode      = import("robot/node/shop/req_commodity.lua")
local SelfBuyNode           = import("robot/node/shop/self_buy.lua")
local GiveItemde            = import("robot/node/shop/give_item.lua")
local ClaimItemde           = import("robot/node/shop/claim_item.lua")

local create_node   = luabt.create_node
local SEQUENCE      = luabt.NODE_TYPE.SEQUENCE

local ShopNode = create_node(SEQUENCE,
    -- 登录
    LoginNode,

    -- 请求商城数据
    ReqCommodityNode(),

    -- 自购
    SelfBuyNode(1, 1, 1),

    -- 赠送
    GiveItemde(743049906511414273, 2, 1),

    -- 索要
    ClaimItemde(743049906511414273, 3, 1)
)

return ShopNode
