--equip_attachment.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local EquipAttachment = class()
function EquipAttachment:__init(attach_type, attach_id, weapon_id)
    self.name = "equip_attachment"
    self.attach_type = attach_type
    self.attach_id   = attach_id
    self.weapon_id   = weapon_id
end

function EquipAttachment:open(tree)
    local board = tree.blackboard
    if board.b_equip then
        return SUCCESS
    end
end

function EquipAttachment:run(tree)
    local board = tree.blackboard
    local robot = tree.robot
    board.b_equip = true
    robot:send_attachment_equip_req(self.attach_type, self.attach_id, self.weapon_id)
    return FAIL
end

function EquipAttachment:close()
end

return EquipAttachment
