--unlock_attachment.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS
--local log_debug     = logger.debug

local UnlockAttachment = class()
function UnlockAttachment:__init(attach_type, attach_id, weapon_id, item_id)
    self.name = "unlock_attachment"
    self.item_id     = item_id
    self.attach_type = attach_type
    self.attach_id   = attach_id
    self.weapon_id   = weapon_id
end

function UnlockAttachment:open(tree)
    local board = tree.blackboard
    if board.b_unlock then
        return SUCCESS
    end
end

function UnlockAttachment:run(tree)
    local board = tree.blackboard
    local robot = tree.robot
    board.b_equip = true
    robot:send_attachment_unlock_req(self.attach_type, self.attach_id, self.weapon_id, self.item_id)
    return FAIL
end

function UnlockAttachment:close()
end

return UnlockAttachment
