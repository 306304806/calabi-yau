--rank.lua

local LoginNode             = import("robot/node/login.lua")
local ReqStarsRankNode      = import("robot/node/rank/req_stars_rank.lua")
local create_node   = luabt.create_node
local SEQUENCE      = luabt.NODE_TYPE.SEQUENCE

local RankNode = create_node(SEQUENCE,
    LoginNode,
    -- 1.获取排行榜数据
    ReqStarsRankNode
)

return RankNode
