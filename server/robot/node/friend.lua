--friend.lua

local create_node   = luabt.create_node
local SEQUENCE      = luabt.NODE_TYPE.SEQUENCE

local LoginNode = import("robot/node/login.lua")
local FriendAdd = import("robot/node/friend/add.lua")
local FriendDel = import("robot/node/friend/del.lua")
local FriendNode = create_node(SEQUENCE,
    LoginNode,
    create_node(SEQUENCE,
        FriendAdd(),
        FriendDel()
    )
)

return FriendNode