-- career.lua

local GetImageInfo      = import("robot/node/career/get_image_info.lua")
local GetHeadIcons      = import("robot/node/career/get_head_icons.lua")
local SetHeadIcon       = import("robot/node/career/set_head_icon.lua")
local Laud              = import("robot/node/career/laud.lua")
local create_node       = luabt.create_node
local SEQUENCE          = luabt.NODE_TYPE.SEQUENCE

local CareerNode = create_node(SEQUENCE,
    GetImageInfo(),
    GetHeadIcons(),
    SetHeadIcon(),
    Laud()
)

return CareerNode
