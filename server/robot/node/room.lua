--room.lua
local LoginNode     = import("robot/node/login.lua")
local MasterNode    = import("robot/node/room/master.lua")
local RoomCreate    = import("robot/node/room/create.lua")
local RoomModify    = import("robot/node/room/modify.lua")
local RoomDestory   = import("robot/node/room/destory.lua")
local RoomInvite    = import("robot/node/room/invite.lua")
local RoomKick      = import("robot/node/room/kick.lua")
local RoomTrans     = import("robot/node/room/trans.lua")
local RoomBegin     = import("robot/node/room/begin.lua")
local RoomEnter     = import("robot/node/room/enter.lua")
local RoomQuit      = import("robot/node/room/quit.lua")
local RoomSwitch    = import("robot/node/room/switch.lua")
local RoomReady     = import("robot/node/room/ready.lua")

local create_node   = luabt.create_node
local SEQUENCE      = luabt.NODE_TYPE.SEQUENCE
local CONDITION     = luabt.NODE_TYPE.CONDITION

local RoomAllNode = create_node(SEQUENCE,
    LoginNode,
    create_node(CONDITION,
        MasterNode(),
        create_node(SEQUENCE,
            RoomCreate(),
            RoomDestory(),
            RoomModify(),
            RoomSwitch(),
            RoomInvite(),
            RoomKick(),
            RoomTrans(),
            RoomBegin()
        ),
        create_node(SEQUENCE,
            RoomEnter(),
            RoomQuit(),
            RoomSwitch(),
            RoomReady(),
            RoomTrans()
        )
    )
)

local RoomNode = create_node(SEQUENCE,
    LoginNode,
    create_node(CONDITION,
        MasterNode(),
        create_node(SEQUENCE,
            RoomCreate()
        --    RoomBegin()
        ),
        create_node(SEQUENCE,
            RoomEnter(),
            RoomQuit()
        )
    )
)

return RoomAllNode, RoomNode
