-- vcard.lua

local UpdateVCard = import("robot/node/vcard/update_vcard.lua")
local create_node     = luabt.create_node
local SEQUENCE        = luabt.NODE_TYPE.SEQUENCE

local VCardNode = create_node(SEQUENCE,
    UpdateVCard()
)


return VCardNode
