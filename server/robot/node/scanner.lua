-- scanner.lua
local mrandom       = math.random
local tinsert       = table.insert
local random_array  = table_ext.random_array
local FAIL          = luabt.BTConst.FAIL
local SUCCESS       = luabt.BTConst.SUCCESS

local config_mgr    = quanta.get("config_mgr")
local protobuf_mgr  = quanta.get("protobuf_mgr")

local pbscan_db     = config_mgr:init_table("pbscan", "type")

local ScannerNode = class()
function ScannerNode:__init()
    self.name = "scanner"
    self.scaned = false
end

function ScannerNode:open(tree)
    if self.scaned then
        return SUCCESS
    end
end

--排除不检测的协议
function ScannerNode:is_vaild(proto_name)
    if proto_name:find("login") then
        return false
    end
    if proto_name:find("logout") then
        return false
    end
    if proto_name:find("heart") then
        return false
    end
    if proto_name:find("req") then
        return true
    end
    return false
end

--构建单个字段
function ScannerNode:build_field(field)
    if field.type == "TYPE_MESSAGE" then
        --去掉type_name最前面的点
        local pb_infos = protobuf_mgr:get_pb_infos()
        local type_name = field.type_name:sub(2, #field.type_name)
        local pb_define = pb_infos[type_name]
        return self:build_message(pb_define.field)
    else
        local scan_row = pbscan_db:find_one(field.type)
        return random_array(scan_row.ref_value)
    end
end

--构建一个message
function ScannerNode:build_message(pb_field)
    local data = {}
    for _, field in pairs(pb_field) do
        if field.label == "LABEL_REPEATED" then
            local repeates = {}
            local count = mrandom(1, 5)
            for i = 1, count do
                tinsert(repeates, self:build_field(field))
            end
            data[field.name] = repeates
        else
            data[field.name] = self:build_field(field)
        end
    end
    return data
end

--扫描一条协议
function ScannerNode:scan_proro(robot, pb_name, pb_define)
    local proto_data = self:build_message(pb_define.field)
    if pb_name:find("lobby") then
        robot:call_lobby(pb_define.id, proto_data)
    elseif pb_name:find("plat") then
        robot:call_plat(pb_define.id, proto_data)
    elseif pb_name:find("dir") then
        robot:call_dir(pb_define.id, proto_data)
    end
end

function ScannerNode:run(tree)
    self.scaned = true
    local robot = tree.robot
    local pb_infos = protobuf_mgr:get_pb_infos()
    for pb_name, pb_define in pairs(pb_infos) do
        if self:is_vaild(pb_name) then
            self:scan_proro(robot, pb_name, pb_define)
        end
    end
    return FAIL
end

function ScannerNode:close()
end

return ScannerNode
