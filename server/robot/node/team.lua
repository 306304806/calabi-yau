--team.lua
local LoginNode     = import("robot/node/login.lua")
local LeaderNode    = import("robot/node/team/leader.lua")
local TeamCreate    = import("robot/node/team/create.lua")
local TeamKick      = import("robot/node/team/kick.lua")
local TeamTrans     = import("robot/node/team/trans.lua")
local TeamInvite    = import("robot/node/team/invite.lua")
local TeamApply     = import("robot/node/team/apply.lua")
local TeamQuit      = import("robot/node/team/quit.lua")
local TeamReady     = import("robot/node/team/ready.lua")

local create_node   = luabt.create_node
local SEQUENCE      = luabt.NODE_TYPE.SEQUENCE
local CONDITION     = luabt.NODE_TYPE.CONDITION

local TeamAllNode = create_node(SEQUENCE,
    LoginNode,
    create_node(CONDITION,
        LeaderNode(),
        create_node(SEQUENCE,
            TeamCreate(),
            TeamQuit(),
            TeamInvite(),
            TeamKick(),
            TeamTrans()
        ),
        create_node(SEQUENCE,
            TeamQuit(),
            TeamReady(),
            TeamTrans()
        )
    )
)

local TeamNode = create_node(SEQUENCE,
    LoginNode,
    create_node(CONDITION,
        LeaderNode(),
        create_node(SEQUENCE,
            TeamCreate()
        ),
        create_node(SEQUENCE,
            TeamApply(),
            TeamReady()
        )
    )
)

return TeamNode, TeamAllNode

