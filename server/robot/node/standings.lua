-- career.lua

local AddStandings       = import("robot/node/standings/add_standings.lua")
local create_node       = luabt.create_node
local SEQUENCE          = luabt.NODE_TYPE.SEQUENCE

local StandingsNode = create_node(SEQUENCE,
    AddStandings()
)

return StandingsNode
