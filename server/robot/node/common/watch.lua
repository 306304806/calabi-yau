--watch.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local WatchNode = class()
function WatchNode:__init()
    self.name = "watch"
end

function WatchNode:open(tree)
    local robot = tree.robot
    if robot:get_watch_room_id() then
        return SUCCESS
    end
end

function WatchNode:run(tree)
    local robot = tree.robot
    local players = robot:send_get_online_players()
    if players then
        robot:send_watch_other_player(players)
    end
    return FAIL
end

function WatchNode:close()
end

return WatchNode
