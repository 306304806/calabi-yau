--finish_statis.lua

local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local TIME_OUT  = 30

local FinishStatis = class()
function FinishStatis:__init()
    self.name = "finish_statis"
end

function FinishStatis:open(tree)
    --log_debug("FinishStatis:open")
    -- 设置超时
    if not tree.robot.__statis_finish_timeout then
        tree.robot.__statis_finish_timeout = quanta.now
    end

    -- 登录超时
    if (not tree.robot.__statis_finish) and (tree.robot.__statis_finish_timeout + TIME_OUT >= quanta.now) then
        --log_debug("statis timeout !")
        if quanta.statis_module then
            quanta.statis_module:statis_robot_finish(tree.robot.index)
        end
        tree.robot.__statis_finish = true
    end

    -- 正常登录完成
    if (not tree.robot.__statis_finish) then
        if quanta.statis_module then
            quanta.statis_module:statis_robot_finish(tree.robot.index)
        end
        tree.robot.__statis_finish = true
    end

    return SUCCESS
end

function FinishStatis:run(tree)
    return FAIL
end

function FinishStatis:close()
end

return FinishStatis
