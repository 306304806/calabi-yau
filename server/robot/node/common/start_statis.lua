--start_statis.lua

local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local StartStatis = class()
function StartStatis:__init()
    self.name = "start_statis"
end

function StartStatis:open(tree)
    if not tree.robot.__statis_start then
        --log_debug("____________________StartStatis:open:%d", tree.robot.index)
        if quanta.statis_module then
            quanta.statis_module:statis_robot_start(tree.robot.index)
        end

        tree.robot.__statis_start = true
    end

    return SUCCESS
end

function StartStatis:run(tree)
    return FAIL
end

function StartStatis:close()
end

return StartStatis
