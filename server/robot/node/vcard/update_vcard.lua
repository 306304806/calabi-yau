local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local UpdateVCard = class()
function UpdateVCard:__init()
    self.name = "UpdateVCard"
end

function UpdateVCard:open(tree)
    local board = tree.blackboard

    if board.learn_vcard_ing then
        return SUCCESS
    end

end

function UpdateVCard:run(tree)
    local robot = tree.robot
    local board = tree.blackboard

    robot:send_self_vcard_update_req()
    board.learn_vcard_ing = true

    return FAIL
end

function UpdateVCard:close()
end

return UpdateVCard
