-- role.lua

local SelectRoleSkin       = import("robot/node/role/select_role_skin.lua")
local UpdateDisplayingRole = import("robot/node/role/update_displaying_role.lua")
local create_node          = luabt.create_node
local SEQUENCE             = luabt.NODE_TYPE.SEQUENCE

local RoleNode = create_node(SEQUENCE,
    SelectRoleSkin(),
    UpdateDisplayingRole()
)


return RoleNode