local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local SyncSetting = class()
function SyncSetting:__init()
    self.name = "SyncSetting"
end

function SyncSetting:open(tree)
    local robot = tree.robot

    if robot:get_setting() then
        return SUCCESS
    end

end

function SyncSetting:run(tree)
    local robot = tree.robot
    robot:send_setting_sync_req()
    return FAIL
end

function SyncSetting:close()
end

return SyncSetting
