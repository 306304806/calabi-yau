local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local UpdateSetting = class()
function UpdateSetting:__init()
    self.name = "UpdateSetting"
end

function UpdateSetting:open(tree)
    local board = tree.blackboard

    if board.update_setting_ing then
        return SUCCESS
    end
end

function UpdateSetting:run(tree)
    local robot = tree.robot
    local board = tree.blackboard

    board.update_setting_ing = true
    robot:send_setting_update_req()

    return FAIL
end

function UpdateSetting:close()
end

return UpdateSetting
