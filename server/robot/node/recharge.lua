--rank.lua

local SyncRechargeCommodity      = import("robot/node/recharge/sync_recharge_commodity.lua")
local CreateRechargeOrder        = import("robot/node/recharge/create_recharge_order.lua")
local SendRechargeOrder          = import("robot/node/recharge/send_recharge_order.lua")

local create_node   = luabt.create_node
local SEQUENCE      = luabt.NODE_TYPE.SEQUENCE

local RechargeNode = create_node(SEQUENCE,
    SyncRechargeCommodity,
    CreateRechargeOrder,
    SendRechargeOrder
)

return RechargeNode
