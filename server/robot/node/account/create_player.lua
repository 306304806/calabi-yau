--create_player.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS
local tsize     = table_ext.size

local CreatePlayer = class()
function CreatePlayer:__init()
    self.name = "create_player"
end

function CreatePlayer:open(tree)
    local robot = tree.robot
    -- 已有角色就跳过
    local players = robot:get_player_infos()
    if tsize(players) > 0 then
        return SUCCESS
    end
end

function CreatePlayer:run(tree)
    local robot = tree.robot
    robot:send_player_create_req("create_req", 1)
    return FAIL
end

function CreatePlayer:close()
end

return CreatePlayer