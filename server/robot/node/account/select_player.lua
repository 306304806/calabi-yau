-- select_player.lua

local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local SelectPlayer= class()
function SelectPlayer:__init()
    self.name = "select_player"
end

function SelectPlayer:open(tree)
    local robot = tree.robot
    -- player选中完成后跳过
    if robot:get_player_id() then
        return SUCCESS
    end
end

function SelectPlayer:run(tree)
    return FAIL
end

function SelectPlayer:close()
end

return SelectPlayer
