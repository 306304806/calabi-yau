--laud.lua
local SUCCESS   = luabt.BTConst.SUCCESS

local Laud = class()
function Laud:__init()
    self.name = "laud"
end

function Laud:open(tree)
end

function Laud:run(tree)
    local robot = tree.robot
    robot:send_laud_req(robot:get_player_id())
    return SUCCESS
end

function Laud:close()
end

return Laud