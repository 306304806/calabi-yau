--set_head_icon.lua
local SUCCESS   = luabt.BTConst.SUCCESS

local SetHeadIcon = class()
function SetHeadIcon:__init()
    self.name = "set_head_icon"
end

function SetHeadIcon:open(tree)

end

function SetHeadIcon:run(tree)
    local robot = tree.robot
    robot:send_set_head_icon_req(robot:get_player_id())
    return SUCCESS
end

function SetHeadIcon:close()
end

return SetHeadIcon