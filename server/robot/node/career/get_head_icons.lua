--get_head_icons.lua
local SUCCESS   = luabt.BTConst.SUCCESS

local GetHeadIcons = class()
function GetHeadIcons:__init()
    self.name = "get_head_icons"
end

function GetHeadIcons:open(tree)
end

function GetHeadIcons:run(tree)
    local robot = tree.robot
    robot:send_get_head_icons_req(robot:get_player_id())
    return SUCCESS
end

function GetHeadIcons:close()
end

return GetHeadIcons