--get_image_info.lua
local SUCCESS   = luabt.BTConst.SUCCESS

local GetImageInfo = class()
function GetImageInfo:__init()
    self.name = "get_image_info"
end

function GetImageInfo:open(tree)
end

function GetImageInfo:run(tree)
    local robot = tree.robot
    robot:send_career_info_info_req(robot:get_player_id())
    return SUCCESS
end

function GetImageInfo:close()
end

return GetImageInfo