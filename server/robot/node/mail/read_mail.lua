--sync_mail.lua

local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS
--local log_info  = logger.info
--local serialize = logger.serialize

local ReadMail = class()
function ReadMail:__init()
    self.name = "read_mail"
end

function ReadMail:open(tree)
    local robot = tree.robot
    local board = tree.blackboard

    -- 没有未读邮件直接跳过
    if robot:get_unread_mail_count() < 1 then
        return SUCCESS
    end

    -- 本轮测试读操作满足一次不再进入
    if board.read_op_count then
        return SUCCESS
    end
end

function ReadMail:run(tree)
    local robot = tree.robot
    local board = tree.blackboard

    -- 平台连接成功后才执行实际逻辑
    if not robot:get_platform_status() then
        return
    end

    robot:send_mail_read_req()
    board.read_op_count = (board.read_op_count and board.read_op_count or 0) + 1

    return FAIL
end

function ReadMail:close()
end

return ReadMail
