--sync_mail.lua

local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS
--local log_info  = logger.info
--local serialize = logger.serialize

local SyncMail = class()
function SyncMail:__init(force)
    self.name  = "sync_mail"
    self.force = force
end

function SyncMail:open(tree)
    local robot = tree.robot

    if self.force then
        return
    end

    if robot:get_mid2mail() then
        return SUCCESS
    end
end

function SyncMail:run(tree)
    local robot = tree.robot

    -- 平台连接成功后才执行实际逻辑
    if not robot:get_platform_status() then
        return
    end

    robot:send_mail_sync_req()
    self.force = false

    return FAIL
end

function SyncMail:close()
end

return SyncMail
