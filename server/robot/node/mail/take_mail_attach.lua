--sync_mail.lua

local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS
--local log_info  = logger.info
--local serialize = logger.serialize

local TakeMailAttach = class()
function TakeMailAttach:__init()
    self.name = "take_mail_attach"
end

function TakeMailAttach:open(tree)
    local board = tree.blackboard
    if not board.take_attach_op_count then
        board.take_attach_op_count = 0
    end

    -- 本轮已经领取过，不需要再删除
    if board.take_attach_op_count > 0 then
        return SUCCESS
    end
end

function TakeMailAttach:run(tree)
    local robot = tree.robot
    local board = tree.blackboard

    robot:send_mail_attach_take_req()
    board.take_attach_op_count = board.take_attach_op_count + 1

    return FAIL
end

function TakeMailAttach:close()
end

return TakeMailAttach
