--sync_mail.lua

local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS
--local log_info  = logger.info
--local serialize = logger.serialize

local SendMail = class()
function SendMail:__init()
    self.name = "send_mail"
end

function SendMail:open(tree)
    local board = tree.blackboard

    if board.send_mail_count then
        return SUCCESS
    end
end

function SendMail:run(tree)
    local robot = tree.robot
    local board = tree.blackboard

    -- 平台连接成功后才执行实际逻辑
    if not robot:get_platform_status() and board.send_mail_count then
        return
    end

    robot:send_mail_send_req()
    board.send_mail_count = 1

    return FAIL
end

function SendMail:close()
end

return SendMail
