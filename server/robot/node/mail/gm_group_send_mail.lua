--sync_mail.lua

local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS
--local log_info  = logger.info
--local serialize = logger.serialize

local GMGroupSendMail = class()
function GMGroupSendMail:__init()
    self.name = "gm_group_send_mail"
end

function GMGroupSendMail:open(tree)
    --local robot = tree.robot
    local board = tree.blackboard
    if not board.gm_group_send_op_count then
        board.gm_group_send_op_count = 0
    end

    -- 本轮已经删除过，不需要再删除
    if board.gm_group_send_op_count > 0 then
        return SUCCESS
    end
end

function GMGroupSendMail:run(tree)
    local robot = tree.robot
    local board = tree.blackboard

    robot:send_gm_send_group_mail()

    --robot:send_gm_send_group_mail()
    --robot:send_gm_send_group_mail()
    --robot:send_gm_send_group_mail()
    --robot:send_gm_send_group_mail()
    --robot:send_gm_send_group_mail()
    --robot:send_gm_send_group_mail()
    --robot:send_gm_send_group_mail()
    --robot:send_gm_send_group_mail()
    --robot:send_gm_send_group_mail()
    --robot:send_gm_send_group_mail()
    --robot:send_gm_send_group_mail()
    --robot:send_gm_send_group_mail()
    --robot:send_gm_send_group_mail()
    --robot:send_gm_send_group_mail()
    --robot:send_gm_send_group_mail()
    --robot:send_gm_send_group_mail()
    --robot:send_gm_send_group_mail()
    --robot:send_gm_send_group_mail()
    --robot:send_gm_send_group_mail()
    --robot:send_gm_send_group_mail()

    board.gm_group_send_op_count = board.gm_group_send_op_count + 1

    return FAIL
end

function GMGroupSendMail:close()
end

return GMGroupSendMail
