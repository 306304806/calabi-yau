--sync_mail.lua

local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS
--local log_info  = logger.info
--local serialize = logger.serialize

local DeleteMail = class()
function DeleteMail:__init()
    self.name = "delete_mail"
end

function DeleteMail:open(tree)
    local robot = tree.robot
    local board = tree.blackboard
    if not board.delete_op_count then
        board.delete_op_count = 0
    end

    -- 没有邮件直接跳过
    if robot:get_mail_count() < 1 then
        return SUCCESS
    end

    -- 本轮已经删除过，不需要再删除
    if board.delete_op_count > 0 then
        return SUCCESS
    end
end

function DeleteMail:run(tree)
    local robot = tree.robot
    local board = tree.blackboard

    robot:send_mail_delete_req()
    board.delete_op_count = board.delete_op_count + 1

    return FAIL
end

function DeleteMail:close()
end

return DeleteMail
