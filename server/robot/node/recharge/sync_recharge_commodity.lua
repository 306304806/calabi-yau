--req_commodity.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local SyncRechargeCommodity = class()
function SyncRechargeCommodity:__init()
    self.name = "sync_recharge_commodity"
    self.req = false
end

function SyncRechargeCommodity:open(tree)
    if self.req then
        return SUCCESS
    end
end

function SyncRechargeCommodity:run(tree)
    local robot = tree.robot
    if robot:get_platform_status() then
        self.req = true
        robot:send_recharge_commodity_sync_req()
        return FAIL
    end
end

function SyncRechargeCommodity:close()
end

return SyncRechargeCommodity
