--req_commodity.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local SendRechargeOrder = class()
function SendRechargeOrder:__init()
    self.name = "send_recharge_order"
    self.req = false
end

function SendRechargeOrder:open(tree)
    if self.req then
        return SUCCESS
    end
end

function SendRechargeOrder:run(tree)
    local robot = tree.robot
    if robot:get_platform_status() then
        self.req = true
        robot:send_recharge_order_send_req()
        return FAIL
    end
end

function SendRechargeOrder:close()
end

return SendRechargeOrder
