--req_commodity.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local CreateRechargeOrder = class()
function CreateRechargeOrder:__init()
    self.name = "create_recharge_order"
    self.req = false
end

function CreateRechargeOrder:open(tree)
    if self.req then
        return SUCCESS
    end
end

function CreateRechargeOrder:run(tree)
    local robot = tree.robot
    if robot:get_platform_status() then
        self.req = true
        robot:send_recharge_order_create_req()
        return FAIL
    end
end

function CreateRechargeOrder:close()
end

return CreateRechargeOrder
