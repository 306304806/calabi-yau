--room.lua
local SUCCESS   = luabt.BTConst.SUCCESS
local FAIL      = luabt.BTConst.FAIL

local RoomCreate = class()
function RoomCreate:__init()
    self.name = "room_create"
end

function RoomCreate:open(tree)
    local robot = tree.robot
    if robot:get_room_id() then
        return SUCCESS
    end
end

function RoomCreate:run(tree)
    local robot = tree.robot
    robot:send_create_room_req()
    return FAIL
end

function RoomCreate:close()
end

return RoomCreate
