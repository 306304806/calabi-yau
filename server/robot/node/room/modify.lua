--modify.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local RoomModify = class()
function RoomModify:__init()
    self.name = "room_modify"
    self.modify = false
end

function RoomModify:open(tree)
    if self.modify then
        return SUCCESS
    end
    local robot = tree.robot
    if not robot:get_room_id() then
        return FAIL
    end
end

function RoomModify:run(tree)
    local robot = tree.robot
    local room_id = robot:get_room_id()
    robot:send_modify_room_req(room_id)
    self.modify = true
    return FAIL
end

function RoomModify:close()
end

return RoomModify
