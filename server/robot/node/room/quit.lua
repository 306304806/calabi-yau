--quit.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local RoomQuit = class()
function RoomQuit:__init()
    self.name = "room_quit"
    self.quit = false
end

function RoomQuit:open(tree)
    if self.quit then
        return SUCCESS
    end
    local robot = tree.robot
    if not robot:get_room_id() then
        return FAIL
    end
end

function RoomQuit:run(tree)
    local robot = tree.robot
    local room_id = robot:get_room_id()
    robot:send_room_quit_req(room_id)
    self.quit = true
    return FAIL
end

function RoomQuit:close()
end

return RoomQuit
