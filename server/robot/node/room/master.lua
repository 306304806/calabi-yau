--matser.lua

local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local MasterNode = class()
function MasterNode:__init()
    self.name = "matser"
end

function MasterNode:open(tree)
    local args = tree.args
    if args and args.master then
        return SUCCESS
    end
    return FAIL
end

function MasterNode:close()
end

return MasterNode

