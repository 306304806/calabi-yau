--trans.lua
local tsize     = table_ext.size
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local RoomTrans = class()
function RoomTrans:__init()
    self.name = "room_trans"
    self.trans = false
end

function RoomTrans:open(tree)
    if self.trans then
        return SUCCESS
    end
    local robot = tree.robot
    if not robot:get_room_id() then
        return FAIL
    end
    local members = robot:get_room_members()
    if not robot:is_master() or tsize(members) < 2 then
        return FAIL
    end
end

function RoomTrans:run(tree)
    local robot = tree.robot
    local player_id = robot:get_player_id()
    local room_id = robot:get_room_id()
    local members = robot:get_room_members()
    for index = 1, 10 do
        local member = members[index]
        if member and member.status == 2 and player_id ~= member.player_id then
            robot:send_trans_leader_req(room_id, member.player_id)
            self.trans = true
            return FAIL
        end
    end
    return FAIL
end

function RoomTrans:close()
end

return RoomTrans
