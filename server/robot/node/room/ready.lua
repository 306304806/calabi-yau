--ready.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local RoomReady = class()
function RoomReady:__init()
    self.name = "room_ready"
end

function RoomReady:open(tree)
    local robot = tree.robot
    if not robot:get_room_id() then
        return FAIL
    end
    if robot:get_room_ready() then
        return SUCCESS
    end
end

function RoomReady:run(tree)
    local robot = tree.robot
    local room_id = robot:get_room_id()
    robot:send_room_ready_req(room_id, true)
    return FAIL
end

function RoomReady:close()
end

return RoomReady
