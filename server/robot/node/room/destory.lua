--destory.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local RoomDestory = class()
function RoomDestory:__init()
    self.name = "room_destory"
    self.destory = false
end

function RoomDestory:open(tree)
    if self.destory then
        return SUCCESS
    end
    local robot = tree.robot
    if not robot:get_room_id() then
        return FAIL
    end
end

function RoomDestory:run(tree)
    local robot = tree.robot
    local room_id = robot:get_room_id()
    robot:send_destory_room_req(room_id)
    self.destory = true
    return FAIL
end

function RoomDestory:close()
end

return RoomDestory
