--switch.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local RoomSwitch = class()
function RoomSwitch:__init()
    self.name = "room_switch"
    self.switch = false
end

function RoomSwitch:open(tree)
    if self.switch then
        return SUCCESS
    end
    local robot = tree.robot
    if not robot:get_room_id() then
        return FAIL
    end
end

function RoomSwitch:switch_empty(robot, room_id)
    local members = robot:get_room_members()
    for index = 1, 10 do
        local member = members[index]
        if not member or member.status == 0 then
            robot:send_room_switch_req(room_id, index)
            self.switch = true
            return FAIL
        end
    end
    self.switch = true
    return SUCCESS
end

function RoomSwitch:switch_user(robot, room_id)
    local player_id = robot:get_player_id()
    local members = robot:get_room_members()
    for index = 1, 10 do
        local member = members[index]
        if member and member.status == 2 and player_id ~= member.player_id then
            robot:send_room_switch_req(room_id, index)
            self.switch = true
            return FAIL
        end
    end
    self.switch = true
    return SUCCESS
end

function RoomSwitch:run(tree)
    local robot = tree.robot
    local args = tree.args
    local room_id = robot:get_room_id()
    if args and args.master then
        self:switch_empty(robot, room_id)
    else
        self:switch_user(robot, room_id)
    end
end

function RoomSwitch:close()
end

return RoomSwitch
