--begin.lua
local tsize     = table_ext.size
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local RoomBegin = class()
function RoomBegin:__init()
    self.name = "room_begin"
end

function RoomBegin:open(tree)
    local robot = tree.robot
    if not robot:get_room_id() then
        return FAIL
    end
    local room = robot:get_room_info()
    if room.status > 1 then
        return SUCCESS
    end
    local members = robot:get_room_members()
    if room.master ~= robot:get_player_id() or tsize(members) < 2 then
        return FAIL
    end
end

function RoomBegin:run(tree)
    local robot = tree.robot
    local room_id = robot:get_room_id()
    robot:send_room_begin_req(room_id)
    return FAIL
end

function RoomBegin:close()
end

return RoomBegin
