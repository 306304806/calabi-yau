--invite.lua
local tsize     = table_ext.size
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local robot_mgr = quanta.robot_mgr

local RoomInvite = class()
function RoomInvite:__init()
    self.name = "room_invite"
end

function RoomInvite:open(tree)
    local robot = tree.robot
    if not robot:get_room_id() then
        return FAIL
    end
    local room = robot:get_room_info()
    if room.status > 1 then
        return SUCCESS
    end
    if room.master ~= robot:get_player_id() then
        return FAIL
    end
    local args = tree.args
    local members = robot:get_room_members()
    if args and args.size == tsize(members) then
        return SUCCESS
    end
end

function RoomInvite:run(tree)
    local robot = tree.robot
    local index = robot:get_index()
    for idx = index + 1, index + 4 do
        local target = robot_mgr:get_robot(idx)
        if target and target:get_team_id() == 0 then
            robot:send_room_invite_req(target:get_player_id())
        end
    end
    robot:set_wait_time(500)
    return FAIL
end

function RoomInvite:close()
end

return RoomInvite
