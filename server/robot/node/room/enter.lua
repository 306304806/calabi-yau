--enter.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local RoomEnter = class()
function RoomEnter:__init()
    self.name = "room_enter"
end

function RoomEnter:open(tree)
    local robot = tree.robot
    if robot:get_room_id() then
        return SUCCESS
    end
end

function RoomEnter:run(tree)
    local robot = tree.robot
    robot:send_room_search_req()
    return FAIL
end

function RoomEnter:close()
end

return RoomEnter
