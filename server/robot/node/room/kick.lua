--kick.lua
local tsize     = table_ext.size
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local RoomKick = class()
function RoomKick:__init()
    self.name = "room_kick"
    self.kick = false
end

function RoomKick:open(tree)
    if self.kick then
        return SUCCESS
    end
    local robot = tree.robot
    if not robot:get_room_id() then
        return FAIL
    end
    local members = robot:get_room_members()
    if not robot:is_master() or tsize(members) < 2 then
        return FAIL
    end
end

function RoomKick:run(tree)
    local robot = tree.robot
    local player_id = robot:get_player_id()
    local room_id = robot:get_room_id()
    local members = robot:get_room_members()
    for index = 1, 10 do
        local member = members[index]
        if member and member.status == 2 and player_id ~= member.player_id then
            robot:send_room_kick_req(room_id, member.player_id)
            self.kick = true
            return FAIL
        end
    end
    return FAIL
end

function RoomKick:close()
end

return RoomKick
