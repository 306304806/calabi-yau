--sync_role.lua

local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local SelectRoleSkin = class()
function SelectRoleSkin:__init()
    self.name = "select_role_skin"
end

function SelectRoleSkin:open(tree)
    local board = tree.blackboard

    if board.finish_select_role_skin then
        return SUCCESS
    end
end

function SelectRoleSkin:run(tree)
    local robot = tree.robot
    local board = tree.blackboard

    robot:send_role_skin_select_req()
    board.finish_select_role_skin = true

    return FAIL
end

function SelectRoleSkin:close()
end

return SelectRoleSkin
