--sync_role.lua

local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local SyncRoleSkinCfg = class()
function SyncRoleSkinCfg:__init()
    self.name = "sync_role_skin_cfg"
end

function SyncRoleSkinCfg:open(tree)
    local robot = tree.robot

    if robot:get_role_skin_cfg() then
        return SUCCESS
    end
end

function SyncRoleSkinCfg:run(tree)
    local robot = tree.robot

    robot:send_role_skin_cfg_sync_req()

    return FAIL
end

function SyncRoleSkinCfg:close()
end

return SyncRoleSkinCfg
