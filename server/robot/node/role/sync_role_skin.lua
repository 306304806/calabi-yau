--sync_role.lua

local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local SyncRoleSkin = class()
function SyncRoleSkin:__init()
    self.name = "sync_role"
end

function SyncRoleSkin:open(tree)
    local robot = tree.robot

    if robot:get_role_skin_list() then
        return SUCCESS
    end
end

function SyncRoleSkin:run(tree)
    local robot = tree.robot

    robot:send_role_skin_sync_req()

    return FAIL
end

function SyncRoleSkin:close()
end

return SyncRoleSkin
