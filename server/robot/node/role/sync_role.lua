--sync_role.lua

local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local SyncRole = class()
function SyncRole:__init()
    self.name = "sync_role"
end

function SyncRole:open(tree)
    local robot = tree.robot

    if robot:get_role_list() then
        return SUCCESS
    end
end

function SyncRole:run(tree)
    local robot = tree.robot
    robot:send_role_sync_req()
    return FAIL
end

function SyncRole:close()
end

return SyncRole
