--sync_role.lua

local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local UpdateLovedRole = class()
function UpdateLovedRole:__init()
    self.name = "update_loved_role"
end

function UpdateLovedRole:open(tree)
    local board = tree.blackboard

    if board.finish_update_loved_role then
        return SUCCESS
    end
end

function UpdateLovedRole:run(tree)
    local robot = tree.robot
    local board = tree.blackboard

    robot:send_loved_role_update_req()
    board.finish_update_loved_role = true

    return FAIL
end

function UpdateLovedRole:close()
end

return UpdateLovedRole
