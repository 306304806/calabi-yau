--sync_role.lua

local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local UpdateDisplayingRole = class()
function UpdateDisplayingRole:__init()
    self.name = "update_displaying_role"
end

function UpdateDisplayingRole:open(tree)
    local board = tree.blackboard

    if board.finish_update_displaying_role then
        return SUCCESS
    end
end

function UpdateDisplayingRole:run(tree)
    local robot = tree.robot
    local board = tree.blackboard

    robot:send_displayging_role_update_req()
    board.finish_update_displaying_role = true

    return FAIL
end

function UpdateDisplayingRole:close()
end

return UpdateDisplayingRole
