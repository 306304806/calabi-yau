--sync_role.lua

local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local SyncRoleCfg = class()
function SyncRoleCfg:__init()
    self.name = "sync_role_cfg"
end

function SyncRoleCfg:open(tree)
    local robot = tree.robot

    if robot:get_role_cfg() then
        return SUCCESS
    end
end

function SyncRoleCfg:run(tree)
    local robot = tree.robot
    robot:send_role_cfg_sync_req()

    return FAIL
end

function SyncRoleCfg:close()
end

return SyncRoleCfg
