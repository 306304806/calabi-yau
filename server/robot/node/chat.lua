--chat.lua
--local log_info      = logger.info

local SendChatChannel = import("robot/node/chat/send_chat_channel.lua")
local create_node     = luabt.create_node
local SEQUENCE        = luabt.NODE_TYPE.SEQUENCE

local ChatNode = create_node(SEQUENCE,
    SendChatChannel()
)


return ChatNode
