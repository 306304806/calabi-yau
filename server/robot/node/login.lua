--watch.lua

local LoginDir      = import("robot/node/login/login_dir.lua")
local LoginPlat     = import("robot/node/login/login_plat.lua")
local LoginLobby    = import("robot/node/login/login_lobby.lua")
local CreatePlayer  = import("robot/node/account/create_player.lua")
local SelectPlayer  = import("robot/node/account/select_player.lua")

local create_node   = luabt.create_node
local SEQUENCE      = luabt.NODE_TYPE.SEQUENCE

local LoginNode = create_node(SEQUENCE,
    LoginDir(),
    LoginLobby(),
    CreatePlayer(),
    SelectPlayer(),
    LoginPlat()
)

return LoginNode
