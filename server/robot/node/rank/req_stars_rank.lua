--req_stars_rank.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS
local mrandom   = math.random

local ReqStarsRankNode = class()
function ReqStarsRankNode:__init()
    self.name = "req_stars_rank"
    self.req = false
end

function ReqStarsRankNode:open(tree)
    if self.req then
        return SUCCESS
    end
end

function ReqStarsRankNode:run(tree)
    local robot = tree.robot
    local season_index = 1
    if robot:get_platform_status() then
        self.req = true
        local start_pos = mrandom(1, 10)
        local end_pos = mrandom(11, 20)
        robot:send_starts_rank_req(season_index, start_pos, end_pos)
        return FAIL
    end
end

function ReqStarsRankNode:close()
end

return ReqStarsRankNode
