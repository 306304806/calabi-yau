--use_item.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS
--local log_debug     = logger.debug

local UseItemNode = class()
function UseItemNode:__init(item_id, count)
    self.name = "use_item"
    self.item_id = item_id
    self.count = count
    self.use = false
end

function UseItemNode:open(tree)
    if self.use then
        return SUCCESS
    end
end

function UseItemNode:run(tree)
    if not self.use then
        self.use = true
        local robot = tree.robot
        robot:send_use_item_req(self.item_id, self.count)
    end
    return FAIL
end

function UseItemNode:close()
end

return UseItemNode
