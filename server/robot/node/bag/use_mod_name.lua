--use_mod_name.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS
--local log_debug     = logger.debug

local UseAddditionCardNode = class()
function UseAddditionCardNode:__init(item_id, new_nick)
    self.name       = "use_mod_name"
    self.item_id    = item_id
    self.new_nick   = new_nick
    self.use        = false
end

function UseAddditionCardNode:open(tree)
    if self.use then
        return SUCCESS
    end
end

function UseAddditionCardNode:run(tree)
    if not self.use then
        self.use = true
        local robot = tree.robot
        robot:send_use_mod_name(self.item_id, self.new_nick)
    end
    return FAIL
end

function UseAddditionCardNode:close()
end

return UseAddditionCardNode
