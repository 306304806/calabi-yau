--use_addition_card.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS
--local log_debug     = logger.debug

local UseAddditionCardNode = class()
function UseAddditionCardNode:__init(item_id, count)
    self.name    = "use_addition_card"
    self.item_id = item_id
    self.count   = count
    self.use     = false
end

function UseAddditionCardNode:open(tree)
    if self.use then
        return SUCCESS
    end
end

function UseAddditionCardNode:run(tree)
    if not self.use then
        self.use = true
        local robot = tree.robot
        robot:send_use_addition_card(self.item_id, self.count)
    end
    return FAIL
end

function UseAddditionCardNode:close()
end

return UseAddditionCardNode
