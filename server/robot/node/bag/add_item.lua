--add_item.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local AddItemNode = class()
function AddItemNode:__init(item_id, count)
    self.name = "add_item"
    self.item_id = item_id
    self.count   = count
    self.add = false
end

function AddItemNode:open(tree)
    if self.add then
        return SUCCESS
    end

    local robot = tree.robot
    local bag_items = robot:get_bag_items()
    for item_uuid, item in pairs(bag_items) do
        if item.item_id == self.item_id and item.count >= self.count then
            return SUCCESS
        end
    end
end

function AddItemNode:run(tree)
    local robot = tree.robot
    robot:send_gm_add_item_req(self.item_id, self.count)
    self.add = true
    return FAIL
end

function AddItemNode:close()
end

return AddItemNode
