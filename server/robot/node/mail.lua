--mail.lua

local SyncMail        = import("robot/node/mail/sync_mail.lua")
local SendMail        = import("robot/node/mail/send_mail.lua")
local ReadMail        = import("robot/node/mail/read_mail.lua")
local DeleteMail      = import("robot/node/mail/delete_mail.lua")
local TakeMailAttach  = import("robot/node/mail/take_mail_attach.lua")
--local GMGroupSendMail = import("robot/node/mail/gm_group_send_mail.lua")
local create_node     = luabt.create_node
local SEQUENCE        = luabt.NODE_TYPE.SEQUENCE

local MailNode = create_node(SEQUENCE,
    SyncMail(),
    SendMail(),
    SyncMail(true),
    ReadMail(),
    DeleteMail(),
    SyncMail(true),
    --GMGroupSendMail(),
    SyncMail(true),
    TakeMailAttach()
)

return MailNode
