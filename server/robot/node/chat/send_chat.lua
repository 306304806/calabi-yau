
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS
--local log_info  = logger.info
--local serialize = logger.serialize

local SendChat = class()
function SendChat:__init()
    self.name = "send_chat"
end

function SendChat:open(tree)
    local board = tree.blackboard

    if board.send_chat_count then
        return SUCCESS
    end
end

function SendChat:run(tree)
    local robot = tree.robot
    local board = tree.blackboard
    robot:send_chat_channel_send_req()
    board.send_chat_count = 1

    return FAIL
end

function SendChat:close()
end

return SendChat
