
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS
--local log_info  = logger.info
--local serialize = logger.serialize

local SendChatChannel = class()
function SendChatChannel:__init()
    self.name = "send_chat_channel"
end

function SendChatChannel:open(tree)
    local board = tree.blackboard

    if board.send_chat_channel_count then
        return SUCCESS
    end
end

function SendChatChannel:run(tree)
    local robot = tree.robot
    local board = tree.blackboard
    robot:send_chat_channel_send_req()
    board.send_chat_channel_count = 1

    return FAIL
end

function SendChatChannel:close()
end

return SendChatChannel
