--add.lua
local trandom       = table_ext.random
local FAIL          = luabt.BTConst.FAIL
local SUCCESS       = luabt.BTConst.SUCCESS

local FriendType    = enum("FriendType")

local FriendAdd = class()
function FriendAdd:__init()
    self.name = "friend_add"
    self.search = false
end

function FriendAdd:open(tree)
    if self.search then
        return SUCCESS
    end
end

function FriendAdd:run(tree)
    local robot = tree.robot
    local friends = robot:send_friend_search_req(0, "兵")
    for _, friend in pairs(friends) do
        local _, friend_type = trandom(FriendType)
        robot:send_friend_add_req(540114574381155329, friend_type)
        break
    end
    self.search = true
    return FAIL
end

function FriendAdd:close()
end

return FriendAdd
