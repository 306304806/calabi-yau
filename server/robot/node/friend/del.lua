--del.lua
local FAIL          = luabt.BTConst.FAIL
local SUCCESS       = luabt.BTConst.SUCCESS

local FriendType    = enum("FriendType")

local FriendDel = class()
function FriendDel:__init()
    self.name = "friend_del"
    self.del = false
end

function FriendDel:open(tree)
    if self.del then
        return SUCCESS
    end
end

function FriendDel:run(tree)
    local robot = tree.robot
    local friends = robot:get_friends()
    for _, friend in pairs(friends) do
        if friend.friend_type == FriendType.BLACK then
            robot:send_friend_del_req(friend.player.player_id, FriendType.BLACK)
        end
    end
    for _, friend in pairs(friends) do
        if friend.friend_type == FriendType.FRIEND then
            robot:send_friend_del_req(friend.player.player_id, FriendType.FRIEND)
            break
        end
    end
    self.del = true
    return FAIL
end

function FriendDel:close()
end

return FriendDel
