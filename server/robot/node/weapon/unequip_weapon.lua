--unequip_weapon.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

local UnequipWeaponNode = class()
function UnequipWeaponNode:__init(role_id, bag_index, slot_pos)
    self.name       = "unequip_weapon"
    self.role_id    = role_id
    self.bag_index  = bag_index
    self.slot_pos   = slot_pos
end

function UnequipWeaponNode:open(tree)
    if self.req_unequip then
        return SUCCESS
    end
end

function UnequipWeaponNode:run(tree)
    local robot = tree.robot
    if self.req_unequip then
        self.req_unequip = true
        local unequip_uuid = robot:get_equiped_weapon_uuid(self.role_id, self.bag_index, self.slot_pos)
        robot:send_unequip_weapon_req(self.role_id, self.bag_index, self.slot_pos, unequip_uuid)
    end

    return FAIL
end

function UnequipWeaponNode:close()
end

return UnequipWeaponNode
