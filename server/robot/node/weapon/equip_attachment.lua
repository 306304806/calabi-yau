--equip_attachment.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS
--local log_debug     = logger.debug

local EquipAttachmentNode = class()
function EquipAttachmentNode:__init(role_id, bag_index, slot_pos, attach_type, attach_id, item_id)
    self.name = "equip_attachment"
    self.role_id     = role_id
    self.bag_index   = bag_index
    self.slot_pos    = slot_pos
    self.attach_type = attach_type
    self.attach_id   = attach_id
end

function EquipAttachmentNode:open(tree)
    if self.equip_attach_req then
        return SUCCESS
    end
end

function EquipAttachmentNode:run(tree)
    local robot = tree.robot
    if not self.equip_attach_req then
        self.equip_attach_req = true
        local equip_weapon_uuid = robot:get_equiped_weapon_uuid(self.role_id, self.bag_index, self.slot_pos)
        robot:send_equip_attachment(equip_weapon_uuid, self.attach_type, self.attach_id)
    end

    return FAIL
end

function EquipAttachmentNode:close()
end

return EquipAttachmentNode
