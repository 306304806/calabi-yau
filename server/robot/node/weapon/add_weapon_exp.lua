--add_weapon_exp.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS
--local log_debug     = logger.debug

local AddWeaponExpNode = class()
function AddWeaponExpNode:__init(weapon_id, add_exp)
    self.name = "add_weapon_exp"
    self.add_exp    = add_exp
    self.weapon_id  = weapon_id
end

function AddWeaponExpNode:open(tree)
    if self.req_add then
        return SUCCESS
    end
end

function AddWeaponExpNode:run(tree)
    local robot = tree.robot
    if not self.req_add then
        self.req_add = true
        robot:send_gm_add_weapon_exp(self.weapon_id, self.add_exp)
    end

    return FAIL
end

function AddWeaponExpNode:close()
end

return AddWeaponExpNode
