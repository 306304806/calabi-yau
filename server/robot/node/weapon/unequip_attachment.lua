--unequip_attachment.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS
--local log_debug     = logger.debug

local UnequipAttachNode = class()
function UnequipAttachNode:__init(role_id, bag_index, slot_pos, attach_type, attach_id)
    self.name = "unequip_attachment"
    self.role_id     = role_id
    self.bag_index   = bag_index
    self.slot_pos    = slot_pos
    self.attach_type = attach_type
    self.attach_id   = attach_id
end

function UnequipAttachNode:open(tree)
    if self.unequip_attach_req then
        return SUCCESS
    end
end

function UnequipAttachNode:run(tree)
    local robot = tree.robot
    if not self.unequip_attach_req then
        self.unequip_attach_req = true
        local equip_weapon_uuid = robot:get_equiped_weapon_uuid(self.role_id, self.bag_index, self.slot_pos)
        robot:send_unequip_attachment(equip_weapon_uuid, self.attach_type, self.attach_id)
    end

    return FAIL
end

function UnequipAttachNode:close()
end

return UnequipAttachNode