--unlock_attachment.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS
--local log_debug     = logger.debug

local UnlockAttachmentNode = class()
function UnlockAttachmentNode:__init(role_id, bag_index, slot_pos, attach_type, attach_id, item_id)
    self.name = "unlock_attachment"
    self.role_id     = role_id
    self.bag_index   = bag_index
    self.slot_pos    = slot_pos
    self.attach_type = attach_type
    self.attach_id   = attach_id
    self.item_id     = item_id
end

function UnlockAttachmentNode:open(tree)
    if self.unlock_attch_req then
        return SUCCESS
    end
end

function UnlockAttachmentNode:run(tree)
    local robot = tree.robot
    if not self.unlock_attch_req then
        self.unlock_attch_req = true
        local weapon_attach_uuid = robot:get_equiped_weapon_uuid(self.role_id, self.bag_index, self.slot_pos)
        robot:send_unlock_attachment(weapon_attach_uuid, self.attach_type, self.attach_id, self.item_id)
    end

    return FAIL
end

function UnlockAttachmentNode:close()
end

return UnlockAttachmentNode
