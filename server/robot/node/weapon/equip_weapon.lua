--equip_weapon.lua
local FAIL      = luabt.BTConst.FAIL
local SUCCESS   = luabt.BTConst.SUCCESS

--local log_debug     = logger.debug

local EquipWeaponNode = class()
function EquipWeaponNode:__init(role_id, bag_index, slot_pos, weapon_id)
    self.name = "equip_weapon"
    self.role_id    = role_id
    self.bag_index  = bag_index
    self.slot_pos   = slot_pos
    self.weapon_id  = weapon_id
end

function EquipWeaponNode:open(tree)
    if self.req_equip then
        return SUCCESS
    end
end

function EquipWeaponNode:run(tree)
    local robot = tree.robot
    if not self.req_equip then
        self.req_equip = true
        robot:send_equip_weapon_req(self.role_id, self.bag_index, self.slot_pos, self.weapon_id)
    end

    return FAIL
end

function EquipWeaponNode:close()
end

return EquipWeaponNode
