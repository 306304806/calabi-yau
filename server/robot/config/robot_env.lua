--robot_env.lua

local ROBOT_ENVS = {
    ["env_local"] = {
        dir_ip   = "127.0.0.1",     --dir IP
        dir_port = 20013,           --dir port
        plat_id  = 1,               --plat_id
        open_id  = "test666_1",        --open_id前缀
        access_token = "123123",    --access_token
    },
    ["env_82"] = {
        dir_ip   = "192.168.125.82",     --dir IP
        dir_port = 20013,           --dir port
        plat_id  = 1,               --plat_id
        open_id  = "t12est666_1",        --open_id前缀
        access_token = "123123",    --access_token
    },
    ["env_44"] = {
        dir_ip = "10.100.0.19",     --dir IP
        dir_port = 20013,           --dir port
        plat_id = 1,                --plat_id
        open_id = "test_2",         --open_id前缀
        access_token = "123123",    --access_token
    },
    ["env_127"] = {
        dir_ip = "127.0.0.1",       --dir IP
        dir_port = 20013,           --dir port
        plat_id = 1,                --plat_id
        open_id = "test_127-",         --open_id前缀
        access_token = "123123",    --access_token
    },
    ["env_131"] = {
        dir_ip = "10.72.17.131",     --dir IP
        dir_port = 20013,           --dir port
        plat_id = 1,                --plat_id
        open_id = "test_131-",         --open_id前缀
        access_token = "123123",    --access_token
    },
    ["env_83"] = {
        dir_ip = "192.168.125.81",     --dir IP
        dir_port = 20013,              --dir port
        plat_id = 1,                   --plat_id
        open_id = "jack_123",         --open_id前缀
        access_token = "111",          --access_token
    },
}

return ROBOT_ENVS
