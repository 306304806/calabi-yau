--robot_tree.lua
local BagNode           = import("robot/node/bag.lua")
local RoomNode          = import("robot/node/room.lua")
local TeamNode          = import("robot/node/team.lua")
local LoginNode         = import("robot/node/login.lua")
local LogoutNode        = import("robot/node/logout.lua")
local MatchNode         = import("robot/node/match.lua")
local FriendNode        = import("robot/node/friend.lua")
local WatchNode         = import("robot/node/common/watch.lua")
local StartStatis       = import("robot/node/common/start_statis.lua")
local FinishStatis      = import("robot/node/common/finish_statis.lua")
local SyncRoleCfg       = import("robot/node/role/sync_role_cfg.lua")
local SyncRole          = import("robot/node/role/sync_role.lua")
local SyncRoleSkin      = import("robot/node/role/sync_role_skin.lua")
local SyncSetting       = import("robot/node/setting/sync_setting.lua")
local UpdateSetting     = import("robot/node/setting/update_setting.lua")
local SyncRoleSkinCfg   = import("robot/node/role/sync_role_skin_cfg.lua")
local UpdateLovedRole   = import("robot/node/role/update_loved_role.lua")
local RoleNode          = import("robot/node/role.lua")
local MailNode          = import("robot/node/mail.lua")
local ChatNode          = import("robot/node/chat.lua")
local RankNode          = import("robot/node/rank.lua")
local VCardNode         = import("robot/node/vcard.lua")
local ShopNode          = import("robot/node/shop.lua")
local CareerNode        = import("robot/node/career.lua")
local ScannerNode       = import("robot/node/scanner.lua")
local RechargeNode      = import("robot/node/recharge.lua")

local create_node       = luabt.create_node
local SEQUENCE          = luabt.NODE_TYPE.SEQUENCE

--行为树配置
local ROBOT_TREES = {
    --参数扫描
    [1000] = create_node(SEQUENCE,
        LoginNode,
        ScannerNode
    ),
    -- 登录测试用例
    [1001] = LoginNode,
    -- 匹配测试用例
    [1002] = MatchNode,
    -- 顶号测试用例
    [1003] = create_node(SEQUENCE),
    -- 获取英雄列表测试用例
    [1004] = create_node(SEQUENCE,
        LoginNode,
        SyncRole()
    ),
    -- 统计测试用例
    [1005] = create_node(SEQUENCE,
        StartStatis(),
        LoginNode,
        FinishStatis()
    ),
    -- 观战测试用例
    [1006] = create_node(SEQUENCE,
        LoginNode,
        WatchNode()
    ),
    -- 组队测试用例
    [1007] = TeamNode,
    -- 房间测试用例
    [1008] = RoomNode,
    -- 背包测试用例
    [1009] = BagNode,
    -- 好友测试用例
    [1010] = FriendNode,
    -- 排行榜测试用例
    [1011] = RankNode,
    -- 商城测试用例
    [1012] = ShopNode,
    -- 生涯个人信息测试用例
    [1013] = create_node(SEQUENCE,
        LoginNode,
        CareerNode
    ),
    -- enic专用
    [666] = create_node(SEQUENCE,
        LoginNode,
        SyncRoleCfg(),      -- 同步英雄配置
        SyncRole(),         -- 同步英雄信息
        SyncRoleSkinCfg(),  -- 同步皮肤配置
        SyncRoleSkin(),     -- 同步皮肤列表
        UpdateLovedRole(),  -- 更新最喜爱的英雄
        SyncSetting(),      -- 同步设置
        UpdateSetting(),    -- 更新设置
        MailNode,           -- 邮件相关用例
        ChatNode,
        RoleNode,
        VCardNode
    ),
    -- 登出
    [667] = create_node(SEQUENCE,
        LoginNode,
        LogoutNode
    ),
    -- 充值
    [668] = create_node(SEQUENCE,
        LoginNode,
        RechargeNode
    )
}

return ROBOT_TREES
