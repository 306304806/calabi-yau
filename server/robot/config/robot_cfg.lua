--robot_cfg.lua

local ROBOT_CFGS = {
    [1] = {
        environ = "env_44",         --environ
        tree_id = 1001,             --行为树配置
        args_id = 1000,             --参数模版
        openid_type = 0,            --openid类型 0随机1组合2指定
        start = 1000,               --机器人起始ID
        count = 1                   --机器人数量
    },
    [2] = {
        environ = "env_local",      --environ
        tree_id = 1007,             --行为树配置
        args_id = 1000,             --参数模版
        openid_type = 1,            --openid类型 0随机1组合2指定
        start = 1500,               --机器人起始ID
        count = 1                   --机器人数量
    },
    [3] = {
        environ = "env_local",      --environ
        tree_id = 1000,             --行为树配置
        args_id = 1000,             --参数模版
        openid_type = 1,            --openid类型 0随机1组合2指定
        start = 2000,               --机器人起始ID
        count = 1                   --机器人数量
    },
    [4] = {
        environ = "env_local",      --environ
        tree_id = 1001,             --行为树配置
        args_id = 1000,             --参数模版
        openid_type = 0,            --openid类型 0随机1组合2指定
        start = 2500,               --机器人起始ID
        count = 1                   --机器人数量
    },
    [5] = {
        environ = "env_local",      --environ
        tree_id = 1001,             --行为树配置
        args_id = 1000,             --参数模版
        openid_type = 0,            --openid类型 0随机1组合2指定
        start = 3000,               --机器人起始ID
        count = 1                   --机器人数量
    },
    [6] = {
        environ = "env_82",         --environ
        tree_id = 668,--666,             --行为树配置
        args_id = 1000,             --参数模版
        openid_type = 0,            --openid类型 0随机1组合2指定
        start = 1500,               --机器人起始ID
        count = 1                   --机器人数量
    },
    [7] = {
        environ = "env_local",      --environ
        tree_id = 1001,             --行为树配置
        args_id = 1000,             --参数模版
        start = 1500,               --机器人起始ID
        count = 1                   --机器人数量
    },
    [8] = {
        environ = "env_local",      --environ
        tree_id = 1009,             --行为树配置
        args_id = 1001,             --参数模版
        openid_type = 0,            --openid类型 0随机1组合2指定
        start = 1500,               --机器人起始ID
        count = 1                   --机器人数量
    },
    [9] = {
        environ = "env_local",      --environ
        tree_id = 667,              --行为树配置
        args_id = 1001,             --参数模版
        openid_type = 0,            --openid类型 0随机1组合2指定
        random = false,             --随机账号
        start = 1500,               --机器人起始ID
        count = 1                   --机器人数量
    },
    [10] = {
        environ = "env_local",      --environ
        tree_id = 1001,             --行为树配置
        args_id = 1000,             --参数模版
        openid_type = 0,            --openid类型 0随机1组合2指定
        start = 5000,               --机器人起始ID
        count = 35                  --机器人数量
    },
    [11] = {
        environ = "env_83",         --environ
        tree_id = 1012,             --行为树配置
        args_id = 1001,             --参数模版
        openid_type = 2,            --openid类型 0随机1组合2指定
        start = 1500,               --机器人起始ID
        count = 1                   --机器人数量
    },
    [12] = {
        environ = "env_83",         --environ
        tree_id = 1011,             --行为树配置
        args_id = 1001,             --参数模版
        openid_type = 2,            --openid类型 0随机1组合2指定
        start = 1500,               --机器人起始ID
        count = 1                   --机器人数量
    },
    [44] = {
        environ = "env_44",         --environ
        tree_id = 1001,             --行为树配置
        args_id = 1000,             --参数模版
        openid_type = 0,            --openid类型 0随机1组合2指定
        start = 5000,               --机器人起始ID
        count = 1                   --机器人数量
    },
    [127] = {
        environ = "env_127",        --environ
        tree_id = 1001,             --行为树配置
        args_id = 1000,             --参数模版
        openid_type = 0,            --openid类型 0随机1组合2指定
        start = 5000,               --机器人起始ID
        count = 1000                --机器人数量
    },
    [131] = {
        environ = "env_131",        --environ
        tree_id = 1001,             --行为树配置
        args_id = 1000,             --参数模版
        openid_type = 0,            --openid类型 0随机1组合2指定
        start = 5000,               --机器人起始ID
        count = 1000                --机器人数量
    },

    [132] = {
        environ = "env_local",      --environ
        tree_id = 1012,             --行为树配置
        args_id = 1000,             --参数模版
        openid_type = 0,            --openid类型 0随机1组合2指定
        start = 6000,               --机器人起始ID
        count = 1                   --机器人数量
    },

    [133] = {
        environ = "env_local",      --environ
        tree_id = 1013,             --行为树配置
        args_id = 1000,             --参数模版
        openid_type = 0,            --openid类型 0随机1组合2指定
        start = 6000,               --机器人起始ID
        count = 1                   --机器人数量
    },
}

return ROBOT_CFGS
