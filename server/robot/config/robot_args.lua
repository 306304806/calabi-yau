--robot_args.lua

local ROBOT_ARGS = {
    [1000] = {
        [1] = {type = "team", leader = true, size = 5},
        [6] = {type = "team", leader = true, size = 5},
    },
    [1001] = {
        [1] = {type = "room", master = true},
    },
}

return ROBOT_ARGS