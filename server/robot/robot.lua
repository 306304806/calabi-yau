-- robot.lua
local SessionModule     = import("robot/module/session.lua")
local BagModule         = import("robot/module/bag.lua")
local TeamModule        = import("robot/module/team.lua")
local RoomModule        = import("robot/module/room.lua")
local RoleModule        = import("robot/module/role.lua")
local WatchModule       = import("robot/module/watch.lua")
local MatchModule       = import("robot/module/match.lua")
local AccountModule     = import("robot/module/account.lua")
local SettingModule     = import("robot/module/setting.lua")
local MailModule        = import("robot/module/mail.lua")
local ChatModule        = import("robot/module/chat.lua")
local FriendModule      = import("robot/module/friend.lua")
local DivisionsModule   = import("robot/module/division.lua")
local AttributeModule   = import("robot/module/attribute.lua")
local RankModule        = import("robot/module/rank.lua")
local VCardModule       = import("robot/module/vcard.lua")
local ShopMoudle        = import("robot/module/shop.lua")
local CareerModule      = import("robot/module/career.lua")
local RechargeModule    = import("robot/module/recharge.lua")
local DirAgent          = import("robot/agent/agent_dir.lua")
local LobbyAgent        = import("robot/agent/agent_lobby.lua")
local PlatAgent         = import("robot/agent/agent_plat.lua")

local LuaBT             = import("luabt/luabt.lua")
local ROBOT_ARGS        = import("robot/config/robot_args.lua")
local ROBOT_TREES       = import("robot/config/robot_tree.lua")

local report_mgr        = quanta.get("report_mgr")

local HEARTBEAT_PEROID  = 5

local Robot = class(nil, SessionModule, AccountModule, RoleModule,
    BagModule, TeamModule, RoomModule, MatchModule, WatchModule, SettingModule,
    MailModule, ChatModule, FriendModule, DivisionsModule, AttributeModule,
    RankModule, VCardModule, ShopMoudle, CareerModule, RechargeModule)

local prop = property(Robot)
prop:accessor("index", nil)                 --index
prop:accessor("robot_id", nil)              --robot_id
prop:accessor("running", true)              --running
prop:accessor("dir_agent", nil)             --连接成功对象
prop:accessor("lobby_agent", nil)           --连接成功对象
prop:accessor("plat_agent", nil)            --连接成功对象
--账号属性
prop:accessor("account_id", nil)            --account_id
prop:accessor("player_id", nil)             --player_id
prop:accessor("open_id", nil)               --open_id
prop:accessor("access_token", nil)          --open_id访问令牌
prop:accessor("active_code", nil)           --激活码
--环境属性
prop:accessor("area_id", nil)               --小区id
prop:accessor("plat_id", nil)               --plat_id
prop:accessor("dir_ip", nil)                --dir_ip
prop:accessor("dir_port", nil)              --dir_port
--行为树属性
prop:accessor("lua_tree", nil)              --行为树
prop:accessor("wait_time", nil)             --sleep
--平台属性
prop:accessor("platform_url", "")           --platform_url
prop:accessor("platform_token", "")         --platform_token
--lobby属性
prop:accessor("lobby_info", nil)            --lobby_info
prop:accessor("lobby_access_token", nil)    --lobby访问令牌
--状态属性
prop:accessor("login_status", false)        --login_status
prop:accessor("platform_status", false)     --platform_status

function Robot:__init(conf, index)
    self.index = index
    self.heart_tick = quanta.now
    self.robot_id = index + conf.start
    self.dir_agent = DirAgent(self)
    self.plat_agent = PlatAgent(self)
    self.lobby_agent = LobbyAgent(self)
    --创建行为树
    self.lua_tree = LuaBT(self, ROBOT_TREES[conf.tree_id])
    self.lua_tree.blackboard = {}
    if conf.args_id and ROBOT_ARGS[conf.args_id] then
        self.lua_tree.args = ROBOT_ARGS[conf.args_id][index]
    end
end

function Robot:clear_lobby_data()
    self:set_account_id(nil)
    self:clear_room_data()
    self:clear_team_data()
end

function Robot:get_board()
    return self.lua_tree.blackboard
end

function Robot:report(event, ...)
    report_mgr:robot_report(self.robot_id, event, ...)
end

function Robot:update()
    --心跳
    local now = quanta.now
    if now - self.heart_tick > HEARTBEAT_PEROID then
        self.heart_tick = now
        self.plat_agent:send_heartbeat()
        self.lobby_agent:send_heartbeat()
    end
    --清理状态
    self.wait_time = nil
    --行为树执行
    if self.lua_tree:tick() then
        self.running = false
        return 0
    end
    --返回wait_time
    return self.wait_time or 100
end

return Robot
