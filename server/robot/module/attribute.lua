--setting.lua
local log_err       = logger.err
local log_info      = logger.info
local check_failed  = utility.check_failed
local serialize     = logger.serialize
local ncmdid        = ncmd_cs.NCmdId

local AttributeModule = mixin(
    "send_attribute_sync_req"
)

local prop = property(AttributeModule)
prop:accessor("id2attr", {})  -- 原始属性集map<id,value>

function AttributeModule:__init()
    self:register_doer(ncmdid.NID_ATTRIBUTE_SYNC_NTF , AttributeModule, "on_attribute_sync_ntf")
end

-- 请求自己的设置信息
function AttributeModule:send_attribute_sync_req()
    local net_req = {}
    local ok, res = self:call_lobby(ncmdid.NID_ATTRIBUTE_SYNC_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[AttributeModule:send_attribute_sync_req] faild: %s", ok and res.code or res)
    end

    log_info("[AttributeModule] attribute: %s", serialize(res.items))
end

-- 属性变更通知
function AttributeModule:on_attribute_update_ntf(ntf)
    log_info("[AttributeModule][on_attribute_update_ntf] %s", serialize(ntf))
end

-- 属性同步通知
function AttributeModule:on_attribute_sync_ntf(ntf)
    log_info("[AttributeModule][on_attribute_sync_ntf] %s", serialize(ntf))
end

return AttributeModule
