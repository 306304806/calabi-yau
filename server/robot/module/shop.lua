--bag.lua
local sformat       = string.format
local log_err       = logger.err
--local log_info      = logger.info
local log_debug     = logger.debug
local check_failed  = utility.check_failed
local serialize     = logger.serialize

local PlatCSCmdID   = ncmd_plat.CCmdId
local CSCmdID       = ncmd_cs.NCmdId

local timer_mgr     = quanta.get("timer_mgr")

local ShopModule = mixin(
    "req_self_buy_commodity",           -- 请求自购
    "req_all_commodity",                -- 获取商城所有商品信息
    "req_give_commodity",               -- 请求赠送商品
    "req_claim_commodity",              -- 请求索要商品
    "timer_pull_change_commodity"       -- 拉取商城变化信息
)

local prop = property(ShopModule)
prop:accessor("commodity_list", {})             -- 商品配置列表
prop:accessor("commodity_version", 0)           -- 商品配置版本
prop:accessor("limit_buy_list", {})             -- 限购列表
prop:accessor("ad_list", {})                    -- 广告列表

function ShopModule:__init()

end

--请求 rpc
--------------------------------------------------------------------------------
-- 获取商城所有商品信息
function ShopModule:req_all_commodity()
    local net_req = {player_id = self.player_id}
    local ok, res = self:call_plat(PlatCSCmdID.NID_SHOP_ALL_COMMODITY_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[ShopModule][req_all_commodity] req commodity failed! %s", ok and res.code)
        return false
    end
    log_debug("[ShopModule][req_all_commodity]->code: %s", serialize(res))

    self.commodity_version = res.version
    for _, data in pairs(res.commodity_list) do
        self.commodity_list[data.id] = data
    end
    self.limit_buy_list    = res.limit_buy_list
    self.ad_list           = res.ad_list

    timer_mgr:loop(5000, function()
        self:timer_pull_change_commodity()
    end)

    return true
end

-- 拉取商城变化信息
function ShopModule:timer_pull_change_commodity()
    local net_req = {version = self.commodity_version}
    local ok, res = self:call_plat(PlatCSCmdID.NID_SHOP_PULL_CHANGE_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[ShopModule][timer_pull_change_commodity] req change commodity failed! %s", ok and res.code)
        return false
    end
    log_debug("[ShopModule][timer_pull_change_commodity]->code: %s", serialize(res.code))

    self.limit_buy_list    = res.limit_buy_list
    self.ad_list           = res.ad_list
    self.commodity_version = res.version
    for _, data in pairs(res.change_list) do
        self.commodity_list[data.id] = data
    end
end

-- 请求自购
function ShopModule:req_self_buy_commodity(id, count, pay_type)
    -- 先加钱
    local net_req = { command = sformat("set_attribute 5 10000") }
    local ok, res = self:call_lobby(CSCmdID.NID_GM_COMMAND_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("1.[ShopModule][req_self_buy_commodity] add money failed! %s", res.code)
        return false
    end

    net_req = { command = sformat("set_attribute 6 10000") }
    ok, res = self:call_lobby(CSCmdID.NID_GM_COMMAND_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("2.[ShopModule][req_self_buy_commodity] add money failed! %s", res.code)
        return false
    end

    net_req = {commodity_id = id, count = count, pay_type = pay_type}
    ok, res = self:call_plat(PlatCSCmdID.NID_SHOP_BUY_COMMODITY_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[ShopModule][req_self_buy_commodity] req failed! %s", ok and res.code)
        return false
    end

    log_debug("[ShopModule][req_self_buy_commodity]->res.order_id:%s", res.order_id)

    net_req = { order_id = res.order_id }
    ok, res = self:call_lobby(CSCmdID.NID_SHOP_PAY_SELF_BUY_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[ShopModule][req_self_buy_commodity] req failed! %s", ok and res.code)
        return false
    end

    log_debug("[ShopModule][req_self_buy_commodity]pay success!")
    return true
end

function ShopModule:req_give_commodity(receiver_id, commodity_id, count)
    local net_req = { command = sformat("set_attribute 3 50000") }
    local ok, res = self:call_lobby(CSCmdID.NID_GM_COMMAND_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("1.[ShopModule][req_self_buy_commodity] add money failed! %s", res.code)
        return false
    end

    net_req = {commodity_id = commodity_id, count = count, receiver_id = receiver_id}
    ok, res = self:call_plat(PlatCSCmdID.NID_SHOP_GIVING_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[ShopModule][req_give_commodity] req failed! %s", ok and res.code)
        return false
    end

    log_debug("[ShopModule][req_give_commodity]->res.order_id:%s", res.order_id)

    net_req = { order_id = res.order_id }
    ok, res = self:call_lobby(CSCmdID.NID_SHOP_PAY_GIVING_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[ShopModule][req_give_commodity] req failed! %s", ok and res.code)
        return false
    end

    log_debug("[ShopModule][req_give_commodity]->code:%s", res.code)
    return true
end

function ShopModule:req_claim_commodity(target_id, commodity_id, count)
    local net_req = {commodity_id = commodity_id, count = count, target_id = target_id}
    local ok, res = self:call_plat(PlatCSCmdID.NID_SHOP_CLAIMING_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[ShopModule][req_claim_commodity] req failed! %s", ok and res.code)
        return false
    end

    log_debug("[ShopModule][req_claim_commodity]->code:%s", res.code)

    return true
end

return ShopModule
