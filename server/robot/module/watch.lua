--watch.lua
local trandom       = table_ext.random
local log_err       = logger.err
local log_info      = logger.info
local check_failed  = utility.check_failed
local serialize     = logger.serialize

local ncmdid        = ncmd_cs.NCmdId

local WatchModule = mixin(
    "send_watch_other_player",
    "send_get_online_players"
)

local prop = property(WatchModule)
prop:accessor("watch_player_id", nil)               --观战玩家

function WatchModule:__init()
end

--请求 rpc
--------------------------------------------------------------------------------
-- 请求获取可观战玩家列表
function WatchModule:send_get_online_players()
    local net_req = {}
    local ok, res = self:call_lobby(ncmdid.NID_GET_ONLINE_PLAYERS_REQ, net_req)
    if not ok then
        log_err("[WatchModule][send_get_online_players] error : %s", res)
        return
    end
    log_info("[WatchModule][send_get_online_players] success: %s", serialize(res))
    return res.players
end

function WatchModule:send_watch_other_player(players)
    local _, watch_pleyer_id = trandom(players)
    local net_req2 = {
        watch_type = 1,
        fighter_uid = watch_pleyer_id
    }
    local ok, res = self:call_lobby(ncmdid.NID_WATCH_OTHER_PLAYER_REQ, net_req2)
    if not ok or check_failed(res.code) then
        log_err("[WatchModule][send_watch_other_player] failed: %s", ok and res.code or res)
        return
    end
    log_info("[WatchModule][send_watch_other_player] success: %s", serialize(res))
    self:set_watch_player_id(watch_pleyer_id)
end

return WatchModule
