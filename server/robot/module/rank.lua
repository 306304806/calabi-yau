--rank.lua
local log_err       = logger.err
local log_debug     = logger.debug
local check_failed  = utility.check_failed
local serialize     = logger.serialize

local platc_cmdid   = ncmd_plat.CCmdId

local RankModule = mixin(
    "send_starts_rank_req"
)

local prop = property(RankModule)
prop:accessor("rank_lists", {})              -- 排行榜数据

function RankModule:__init()

end

--请求 rpc
function RankModule:send_starts_rank_req(season_index, start_pos, end_pos)
    local net_req = {}
    net_req.rank_type           = 1
    net_req.custom              = season_index
    net_req.start_pos           = start_pos
    net_req.end_pos             = end_pos
    net_req.version             = 0

    log_debug("[RankModule][send_starts_rank_req]->net_req:%s", serialize(net_req))
    local ok, net_res = self:call_plat(platc_cmdid.NID_RANK_DATA_REQ, net_req)
    if not ok or check_failed(net_res.code) then
        log_err("[RankModule][send_starts_rank_req] faild: %s", ok and net_res.code or net_res)
    end

    for _, data in pairs(net_res.rows_info) do
        for _, info in pairs(data) do
            log_debug("[RankModule][send_stars_rank_req] ->info:%s", serialize(info))
        end
        log_debug("[RankModule][send_stars_rank_req] player_id:%s, data:%s", self:get_player_id(), serialize(data))
    end
end


return RankModule
