--division.lua
local log_info      = logger.info
local serialize     = logger.serialize

local ncmdid        = ncmd_cs.NCmdId

local DivisionsModule = mixin()
local prop = property(DivisionsModule)
prop:accessor("season", 0)              -- 当前赛季
prop:accessor("stars", 0)               -- 当前星星数
prop:accessor("scores", 0)              -- 当前勇者积分
prop:accessor("streak_win", 0)          -- 连胜场数

function DivisionsModule:__init()
    self:register_doer(ncmdid.NID_RANKINFO_SYNC_NTF, DivisionsModule, "on_sync_player_divisions_ntf")
end

function DivisionsModule:on_sync_player_divisions_ntf(data)
    log_info("[DivisionsModule][on_sync_player_divisions_ntf]->data:%s", serialize(data))
    self:set_season(data.season)
    self:set_stars(data.stars)
    self:set_scores(data.scores)
    self:set_streak_win(data.streak_win)
end

return DivisionsModule
