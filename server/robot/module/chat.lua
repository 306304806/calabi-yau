--chat.lua
local log_err             = logger.err
local log_info            = logger.info
local check_failed        = utility.check_failed

local platc_cmdid         = ncmd_plat.CCmdId

local ChatModule = mixin(
    "send_chat_send_req",
    "send_chat_group_send_req",
    "send_chat_channel_send_req"
)
local prop = property(ChatModule)
prop:accessor("cid2channel", {})              -- map<channel_id, channel>

function ChatModule:__init()
    self:register_doer(platc_cmdid.NID_CHAT_RECV_NTF, ChatModule, "on_chat_recv_ntf")
    self:register_doer(platc_cmdid.NID_CHAT_GROUP_INFO_NTF, ChatModule, "on_chat_group_info_ntf")
    self:register_doer(platc_cmdid.NID_CHAT_GROUP_JOIN_NTF, ChatModule, "on_chat_group_join_ntf")
    self:register_doer(platc_cmdid.NID_CHAT_GROUP_QUIT_NTF, ChatModule, "on_chat_group_quit_ntf")
    self:register_doer(platc_cmdid.NID_CHAT_GROUP_RECV_NTF, ChatModule, "on_chat_group_recv_ntf")
    self:register_doer(platc_cmdid.NID_CHAT_CHANNEL_LIST_SYNC_NTF, ChatModule, "on_chat_channel_list_sync_ntf")
    self:register_doer(platc_cmdid.NID_CHAT_CHANNEL_SYNC_NTF, ChatModule, "on_chat_channel_sync_ntf")
    self:register_doer(platc_cmdid.NID_CHAT_CHANNEL_RECV_NTF, ChatModule, "on_chat_channel_recv_ntf")
end

-- 发送私聊
function ChatModule:send_chat_send_req(tar_player_id)
    local net_req = {
        tar_player_id = tar_player_id,
        msg           = "test_msg",
    }
    local ok, net_res = self:call_plat(platc_cmdid.NID_CHAT_SEND_REQ, net_req)
    if not ok or check_failed(net_res.code) then
        log_err("[ChatModule:send_chat_send_req] faild: %s", ok and net_res.code or net_res)
    end

    log_info("[ChatModule:send_chat_send_req] code=%s", net_res.code)

end

-- 发送群聊
function ChatModule:send_chat_group_send_req()
    local net_req = {
        channel_id = 1,
        msg        = "test_msg",
    }
    local ok, net_res = self:call_plat(platc_cmdid.NID_CHAT_SEND_REQ, net_req)
    if not ok or check_failed(net_res.code) then
        log_err("[ChatModule:send_chat_group_send_req] faild: %s", ok and net_res.code or net_res)
    end

    log_info("[ChatModule:send_chat_group_send_req] code=%s", net_res.code)

end

-- 发送频道聊天
function ChatModule:send_chat_channel_send_req()
    local net_req = {
        channel_id = 1,
        msg        = "test_channel_msg",
    }
    local ok, net_res = self:call_plat(platc_cmdid.NID_CHAT_CHANNEL_SEND_REQ, net_req)
    if not ok or check_failed(net_res.code) then
        log_err("[ChatModule:send_chat_channel_send_req] faild: %s", ok and net_res.code or net_res)
    end

    log_info("[ChatModule:send_chat_channel_send_req] code=%s", net_res.code)

end

function ChatModule:on_chat_recv_ntf(ntf)
    log_info("[ChatModule:ChatModule]")
end

function ChatModule:on_chat_group_info_ntf(ntf)
    log_info("[ChatModule:on_chat_group_info_ntf]")
end

function ChatModule:on_chat_group_join_ntf(ntf)
    log_info("[ChatModule:on_chat_group_join_ntf]")
end

function ChatModule:on_chat_group_quit_ntf(ntf)
    log_info("[ChatModule:on_chat_group_quit_ntf]")
end

function ChatModule:on_chat_group_recv_ntf(ntf)
    log_info("[ChatModule:on_chat_group_recv_ntf]")
end

function ChatModule:on_chat_channel_list_sync_ntf(ntf)
    log_info("[ChatModule:on_chat_channel_list_sync_ntf]")
end

function ChatModule:on_chat_channel_sync_ntf(ntf)
    log_info("[ChatModule:on_chat_channel_sync_ntf]")
end

function ChatModule:on_chat_channel_recv_ntf(ntf)
    log_info("[ChatModule:on_chat_channel_recv_ntf]")
end


return ChatModule