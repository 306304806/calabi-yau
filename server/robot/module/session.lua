--session.lua
local tunpack   = table.unpack
local log_warn  = logger.warn

local SessionModule = mixin(
    "send_dir",
    "call_dir",
    "send_lobby",
    "call_lobby",
    "wait_lobby",
    "send_plat",
    "call_plat",
    "wait_plat",
    "register_doer",
    "execute_doer"
)

local prop = property(SessionModule)
prop:accessor("cmd_doers", {})  --事件集

function SessionModule:__init()
end

function SessionModule:register_doer(cmdid, module, doer)
    self.cmd_doers[cmdid] = { module, doer }
end

function SessionModule:execute_doer(cmdid, data)
    local doer_obj = self.cmd_doers[cmdid]
    if not doer_obj then
        log_warn("[SessionModule][execute_doer] cmdid %s is not register doer!", cmdid)
        return
    end
    local module, doer = tunpack(doer_obj)
    module[doer](self, data)
end

function SessionModule:send_dir(cmdid, data)
    local client = self.dir_agent:get_client()
    if client then
        return client:send_dx(cmdid, data)
    end
    return false
end

function SessionModule:call_dir(cmdid, data)
    local client = self.dir_agent:get_client()
    if client then
        return client:call_dx(cmdid, data)
    end
    return false
end

function SessionModule:send_lobby(cmdid, data)
    local client = self.lobby_agent:get_client()
    if client then
        return client:send_dx(cmdid, data)
    end
end

function SessionModule:call_lobby(cmdid, data)
    local client = self.lobby_agent:get_client()
    if client then
        return client:call_dx(cmdid, data)
    end
end

function SessionModule:send_plat(cmdid, data)
    local client = self.plat_agent:get_client()
    if client then
        return client:send_dx(cmdid, data)
    end
end

function SessionModule:call_plat(cmdid, data)
    local client = self.plat_agent:get_client()
    if client then
        return client:call_dx(cmdid, data)
    end
end

function SessionModule:wait_lobby(cmdid, time)
    local client = self.lobby_agent:get_client()
    if client then
        return client:wait_dx(cmdid, time)
    end
    return false
end

function SessionModule:wait_plat(cmdid, time)
    local client = self.plat_agent:get_client()
    if client then
        return client:wait_dx(cmdid, time)
    end
    return false
end

return SessionModule
