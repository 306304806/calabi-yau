--role.lua
local log_err       = logger.err
local log_info      = logger.info
local log_warn      = logger.warn
local tinsert       = table.insert
local sformat       = string.format
local tsize         = table_ext.size
local serialize     = logger.serialize
local check_failed  = utility.check_failed

local MailType      = enum("MailType")
local MailState     = enum("MailState")

local CSCmdID       = ncmd_cs.NCmdId
local PlatCSCmdID   = ncmd_plat.CCmdId

local MailModule = mixin(
    "send_mail_sync_req",
    "send_mail_send_req",
    "send_mail_read_req",
    "send_mail_delete_req",
    "send_mail_attach_take_req",
    "send_gm_send_group_mail",

    "get_unread_mail_count",
    "get_mail_count"
)
local prop = property(MailModule)
prop:accessor("mid2mail", nil)              -- map<mail_id, mail_info>

function MailModule:__init()
    self:register_doer(PlatCSCmdID.NID_MAIL_NEW_NTF, MailModule, "on_mail_new_ntf")
    self:register_doer(CSCmdID.NID_MAIL_OVERFLOW_NTF, MailModule, "on_mail_overflow_ntf")
end

--请求 rpc
--------------------------------------------------------------------------------
-- 请求邮件同步
function MailModule:send_mail_sync_req()
    local net_req = {
        last_send_time = 0,
    }

    local ok, res = self:call_lobby(CSCmdID.NID_MAIL_SYNC_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[MailModule:send_mail_sync_req] faild: %s", ok and res.code or res)
        return
    end

    local mid2mail = {}
    for _, mail in pairs(res.mails or {}) do
        mid2mail[mail.mail_id] = mail
    end

    self:set_mid2mail(mid2mail)

    log_info("[MailModule:send_mail_sync_req] mail_count=%s, unread_count=%s", tsize(res.mails), self:get_unread_mail_count())
end

-- 请求发送邮件
function MailModule:send_mail_send_req()
    local net_req = {
        type          = MailType.PLAYER_MAIL,
        tar_player_id = self:get_player_id(),
        title         = "test mail title",
        content       = "test mail content"
    }

    local ok, res = self:call_lobby(CSCmdID.NID_MAIL_SEND_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[MailModule:send_mail_send_req] faild: %s", ok and res.code or res)
    end

    log_info("[MailModule:send_mail_send_req]  code=%s", res.code)

end

-- 请求读邮件
function MailModule:send_mail_read_req()
    local mid2mail = self:get_mid2mail()
    local net_req = {mail_ids = {}}
    for mid, mail in pairs(mid2mail) do
        if MailState.UNREADED == mail.mail_state then
            tinsert(net_req.mail_ids, mid)
            break
        end
    end

    if net_req.mail_ids then
        local ok, res = self:call_lobby(CSCmdID.NID_MAIL_READ_REQ, net_req)
        if not ok or check_failed(res.code) then
            log_err("[MailModule:send_mail_read_req] faild: %s", ok and res.code or res)
        end

        log_info("[MailModule:send_mail_read_req] code=%s", res.code)
    end

end

-- 请求删除邮件
function MailModule:send_mail_delete_req()
    if self:get_mail_count() < 1 then
        return
    end

    -- 随机删除一个邮件
    local net_req = {mail_ids = {}}
    for mid, mail in pairs(self:get_mid2mail()) do
        if MailState.DELETED ~= mail.mail_state then
            tinsert(net_req.mail_ids, mid)
            break
        end
    end

    if not net_req.mail_id then
        return
    end

    local ok, res = self:call_lobby(CSCmdID.NID_MAIL_DELETE_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[MailModule:send_mail_delete_req] faild: %s", ok and res.code or res)
    end

    log_info("[MailModule:send_mail_delete_req] code=%s", res.code)

end

-- 请求领取邮件附件
function MailModule:send_mail_attach_take_req()
    -- 找一个未领附件的邮件id
    local tar_mid = nil
    for mid, mail in pairs(self.mid2mail) do
        if tsize(mail.attach_items) > 1 and not mail.attach_state then
            tar_mid = mid
        end
        --log_info(">>>  mail_id=%s,count=%s,attach_state=%s", mail.mail_id, tsize(mail.attach_items), mail.attach_state)
    end

    if not tar_mid then
        log_warn("[MailModule][send_mail_attach_take_req] can not find untake mail_id")
        return
    end

    local net_req = {
        mail_ids = {tar_mid}
    }

    local ok, res = self:call_lobby(CSCmdID.NID_MAIL_ATTACH_TAKE_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[MailModule:send_mail_attach_take_req] faild: %s", ok and res.code or res)
        return
    end

    log_info("[MailModule:send_mail_attach_take_req] code=%s,attach=%s", res.code, serialize(res.results))

end

-- gm命令发送群邮件
function MailModule:send_gm_send_group_mail()
    local net_req = {
        command = "",
    }

    -- area_id|title|content|attachs|
    net_req.command = sformat("group_send_mail  %s %s %s %s",
        1, "gm_mail_title", "gm_mail_content", "50001:2;50002:3;")
    --  net_req.command = sformat("group_send_mail  %s %s %s",
    --    1, "gm_mail_title", "gm_mail_content")
    local ok, res = self:call_lobby(CSCmdID.NID_GM_COMMAND_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[MailModule:send_gm_send_group_mail] faild: %s", ok and res.code or res)
        return
    end

    log_info("[MailModule:send_gm_send_group_mail] code=%s", res.code)

end

-- 有新的邮件
function MailModule:on_mail_new_ntf(ntf)
    log_info("[MailModule:on_mail_new_ntf] need_sync=%s, mail_count=%s", ntf.need_sync, tsize(ntf.mails))
end

function MailModule:on_mail_overflow_ntf(ntf)
    log_info("[MailModule:on_mail_overflow_ntf] take_count=%s, del_count=%s", tsize(ntf.take_results), tsize(ntf.del_results))
end
-----------------------------------------------------------------------------------

-- 未读邮件数量
function MailModule:get_unread_mail_count()
    if not self.mid2mail then
        return 0
    end

    local count = 0
    for _, mail in pairs(self.mid2mail or {}) do
        if MailState.UNREADED == mail.mail_state then
            count = count + 1
        end
    end

    return count
end

-- 全部邮件数量
function MailModule:get_mail_count()
    return tsize(self.mid2mail)
end

return MailModule
