--setting.lua
local log_err       = logger.err
local log_info      = logger.info
local check_failed  = utility.check_failed
local serialize     = logger.serialize

local ncmdid        = ncmd_cs.NCmdId

local SettingModule = mixin(
    "send_setting_sync_req",
    "send_setting_update_req"
)

local prop = property(SettingModule)
prop:accessor("setting", nil)               --设置数据

function SettingModule:__init()
end

-- 请求自己的设置信息
function SettingModule:send_setting_sync_req()
    local net_req = {}
    local ok, res = self:call_lobby(ncmdid.NID_SETTING_SYNC_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[SettingModule:send_setting_sync_req] faild: %s", ok and res.code or res)
    end

    log_info("[SettingModule] setting: %s", serialize(res))
    self:set_setting(res.setting_list or {})
end

-- 更新自己的设置信息
function SettingModule:send_setting_update_req()
    local net_req = {
        setting_list = {}
    }

    table.insert( net_req.setting_list, {key=1, value=-1})
    table.insert( net_req.setting_list, {key=2, value=2})

    local ok, res = self:call_lobby(ncmdid.NID_SETTING_UPDATE_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[SettingModule:send_setting_update_req] faild: %s", ok and res.code or res)
    end

    log_info("[SettingModule] update_setting: %s", serialize(res))
end

return SettingModule
