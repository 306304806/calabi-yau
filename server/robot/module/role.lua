--role.lua
local log_err       = logger.err
local log_info      = logger.info
local log_warn      = logger.warn
local tsize         = table_ext.size
local tinsert       = table.insert
local check_failed  = utility.check_failed
local serialize     = logger.serialize

local ncmdid        = ncmd_cs.NCmdId

local config_mgr    = quanta.get("config_mgr")
local roleskin_db   = config_mgr:init_table("roleskin", "role_skin_id")

local RoleModule = mixin(
    "send_role_sync_req",
    "send_role_cfg_sync_req",
    "send_role_skin_sync_req",
    "send_role_skin_select_req",
    "send_loved_role_update_req",
    "send_role_skin_cfg_sync_req",
    "send_displayging_role_update_req"
)
local prop = property(RoleModule)
prop:accessor("role_cfg", nil)              --英雄配置
prop:accessor("role_skin_cfg", nil)         --英雄皮肤配置
prop:accessor("role_list", nil)             --英雄列表
prop:accessor("role_skin_list", nil)        --英雄皮肤列表
prop:accessor("rid2skins", {})              --rid已有的皮肤

function RoleModule:__init()
    self:register_doer(ncmdid.NID_ROLE_SYNC_NTF, RoleModule, "on_role_sync_ntf")
    self:register_doer(ncmdid.NID_ROLE_CFG_SYNC_NTF, RoleModule, "on_role_cfg_sync_ntf")
    self:register_doer(ncmdid.NID_ROLE_SKIN_SYNC_NTF, RoleModule, "on_role_skin_sync_ntf")
    self:register_doer(ncmdid.NID_ROLE_SKIN_CFG_SYNC_NTF, RoleModule,"on_role_skin_cfg_sync_ntf")
end

--请求 rpc
--------------------------------------------------------------------------------
-- 请求同步英雄配置
function RoleModule:send_role_cfg_sync_req()
    local net_req = { version = "role_cfg:0.0.0"}
    local ok, res = self:call_lobby(ncmdid.NID_ROLE_CFG_SYNC_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[RoleModule:send_role_cfg_sync_req] faild: %s", ok and res.code or res)
        return
    end

    log_info("[RoleModule] role_cfg: %s", tsize(res.diff_list))
    self:set_role_cfg(res.diff_list)
end

-- 请求同步英雄列表
function RoleModule:send_role_sync_req()
    local net_req = { player_id = self:get_player_id()}
    local ok, res = self:call_lobby(ncmdid.NID_ROLE_SYNC_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[RoleModule][send_login_req] failed: %s", ok and res.code or res)
        return
    end

    log_info("[RoleModule] role_list: size=%s", tsize(res.role_list))
    self:set_role_list(res.role_list)
end

-- 请求英雄皮肤配置
function RoleModule:send_role_skin_cfg_sync_req()
    local net_req = { version = "role_skin_cfg:0.0.0"}
    local ok, res = self:call_lobby(ncmdid.NID_ROLE_SKIN_CFG_SYNC_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[RoleModule:send_role_skin_cfg_sync_req] faild: %s", ok and res.code or res)
        return
    end

    log_info("[RoleModule] role_skin_cfg: size=%s", tsize(res.diff_list))
    self:set_role_skin_cfg(res.diff_list or {})
end

-- 请求同步英雄皮肤
function RoleModule:send_role_skin_sync_req()
    local net_req = { player_id = self:get_player_id()}
    local ok, res = self:call_lobby(ncmdid.NID_ROLE_SKIN_SYNC_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[RoleModule:send_role_skin_sync_req] faild: %s", ok and res.code or res)
        return
    end

    log_info("[RoleModule] role_skin_list size=%s", tsize(res.role_skin_list))
    self:set_role_skin_list(res.role_skin_list or {})
end

-- 请求更新最喜爱的英雄
function RoleModule:send_loved_role_update_req()
    local role_list = self:get_role_list()
    if not role_list or tsize(role_list) <= 0 then
        log_warn("[RoleModule] can not update loved role !")
        return
    end

    local net_req = { role_id = role_list[1].role_id, loved = true }
    local ok, res = self:call_lobby(ncmdid.NID_LOVED_ROLE_UPDATE_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[RoleModule:send_loved_role_update_req] faild: %s", ok and res.code or res)
        return
    end

    log_info("[RoleModule] loved_role_update %s", serialize(res))

    net_req.loved = false
    local _, res2 = self:call_lobby(ncmdid.NID_LOVED_ROLE_UPDATE_REQ, net_req)
    log_info("[RoleModule] loved_role_update %s", serialize(res2))
end

function RoleModule:send_role_skin_select_req()
    local role_id      = 0
    local role_skin_id = 0
    if tsize(self.role_list) > 0 then
        role_id = self.role_list[1].role_id
    end
    if self.rid2skins[role_id] then
        role_skin_id = self.rid2skins[role_id][1].role_skin_id
    end
    local net_req = { role_id = role_id, role_skin_id = role_skin_id }

    local ok, res = self:call_lobby(ncmdid.NID_ROLE_SKIN_SELECT_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[RoleModule:send_role_skin_select_req] faild: %s", ok and res.code or res)
        return
    end

    log_info("[RoleModule][send_role_skin_select_req] %s", serialize(res))
end

function RoleModule:send_displayging_role_update_req()
    local role_id = 0
    if tsize(self.role_list) > 0 then
        role_id = self.role_list[1].role_id
    end
    local net_req = { role_id = role_id }
    local ok, res = self:call_lobby(ncmdid.NID_DISPLAYING_ROLE_UPDATE_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[RoleModule:send_displayging_role_update_req] faild: %s", ok and res.code or res)
        return
    end

    log_info("[RoleModule][send_displayging_role_update_req] %s", serialize(res))
end

--返回rpc
-------------------------------------------------------------------
function RoleModule:on_role_cfg_sync_ntf(ntf)
    --log_debug("[RoleModule:on_role_cfg_sync_ntf]:%s", serialize(ntf))
end

function RoleModule:on_role_skin_cfg_sync_ntf(ntf)
    --log_debug("[RoleModule:on_role_skin_cfg_sync_ntf]:%s", serialize(ntf))
end

function RoleModule:on_role_sync_ntf(ntf)
    --log_info("--------------:    %s", serialize(ntf))
    log_info("[RoleModule:on_role_sync_ntf]: count=%s,displaying=%s",tsize(ntf.role_list), ntf.displaying_role_id)
    self.role_list = ntf.role_list
end

function RoleModule:on_role_skin_sync_ntf(ntf)
    --log_info("--------------:    %s", serialize(ntf))
    log_info("[RoleModule:on_role_skin_sync_ntf]:count=%s", tsize(ntf.role_skin_list))
    self.role_skin_list = ntf.role_skin_list

    -- 配置自身索引生成
    local sid2scfg = {}
    for _, cfg_item in roleskin_db:iterator() do
        sid2scfg[cfg_item.role_skin_id] = cfg_item
    end

    -- 生成索引
    for _, skin in pairs(self.role_skin_list) do
        local cfg = sid2scfg[skin.role_skin_id]
        if cfg then
            if not self.rid2skins[cfg.role_id] then
                self.rid2skins[cfg.role_id] = {}
            end
            tinsert(self.rid2skins[cfg.role_id], skin)
        end
    end

    --log_info(">>> %s", serialize(self.rid2skins))
end

return RoleModule
