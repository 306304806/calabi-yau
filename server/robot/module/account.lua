--account.lua
local tinsert       = table.insert
local tsize         = table_ext.size
local log_warn      = logger.warn
local log_debug     = logger.debug
local log_info      = logger.info
local random_name   = quanta.random_name
local check_failed  = utility.check_failed
local serialize     = logger.serialize
local ncmdid        = ncmd_cs.NCmdId

local AccountModule = mixin(
    "send_player_create_req",
    "send_player_update_req"
)

local prop = property(AccountModule)
prop:accessor("player_infos", nil)          --players

function AccountModule:__init()
    self:register_doer(ncmdid.NID_PLAYER_SYNC_NTF, AccountModule, "on_player_sync_ntf")
end

-- 发送创建player请求
function AccountModule:send_player_create_req(nick, sex)
    local net_req = {
        nick = random_name(),
        sex  = sex or 1,
    }
    local ok, net_res = self:call_lobby(ncmdid.NID_PLAYER_CREATE_REQ, net_req)
    if not ok or check_failed(net_res.code) then
        log_warn("[AccountModule][send_player_create_req] ok=%s,ec=%s", ok, serialize(net_res))
        return
    end
    local players = self:get_player_infos()
    tinsert(players, net_res.player)
    self:set_player_infos(players)
    if tsize(players or {}) > 0 then
        log_info("[AccountModule][send_player_create_req]: player_id=%s", players[1].player_id)
        self:set_player_id(players[1].player_id)
    end
    log_debug("[AccountModule][send_player_create_req] res=%s", serialize(net_res))
end

-- 发送更新player请求
function AccountModule:send_player_update_req(nick, sex)
    local net_req = {
        nick = nick,
        sex  = sex,
    }
    local ok, net_res = self:call_lobby(ncmdid.NID_PLAYER_UPDATE_REQ, net_req)
    if not ok or check_failed(net_res.code) then
        log_warn("[AccountModule][send_player_update_req] ok=%s,ec=%s", ok, serialize(net_res.code))
        return
    end
    log_debug("[AccountModule][send_player_update_req] res=%s", serialize(net_res))
end

-- player列表通知
function AccountModule:on_player_sync_ntf(ntf)
    log_debug("[AccountModule][on_player_sync_ntf] %s", ntf)
    self:set_player_infos(ntf.players)
    if tsize(ntf.players or {}) > 0 then
        log_info("[LobbyAgent][on_player_sync_ntf]: player_id=%s", ntf.players[1].player_id)
        self:set_player_id(ntf.players[1].player_id)
    end
end

return AccountModule
