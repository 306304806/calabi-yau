--room.lua
local log_err       = logger.err
local log_debug     = logger.debug
local check_failed  = utility.check_failed
local serialize     = logger.serialize

local ncmdid        = ncmd_cs.NCmdId

local RoomModule = mixin(
    "is_master",
    "clear_room_data",
    "send_create_room_req",
    "send_destory_room_req",
    "send_modify_room_req",
    "send_room_search_req",
    "send_room_enter_req",
    "send_room_quit_req",
    "send_room_kick_req",
    "send_room_invite_req",
    "send_room_ready_req",
    "send_room_begin_req",
    "send_trans_leader_req",
    "send_room_switch_req",
    "send_room_siwtch_answer_req"
)
local prop = property(RoomModule)
prop:accessor("room_id", nil)               --房间id
prop:accessor("room_info", nil)             --房间信息
prop:accessor("room_ready", false)          --房间状态
prop:accessor("room_members", {})           --房间成员信息

function RoomModule:__init()
    self:register_doer(ncmdid.NID_ROOM_INFO_NTF, RoomModule, "on_room_info_ntf")
    self:register_doer(ncmdid.NID_ROOM_SWITCH_NTF, RoomModule, "on_room_switch_ntf")
    self:register_doer(ncmdid.NID_ROOM_MEMBER_NTF, RoomModule, "on_room_member_ntf")
    self:register_doer(ncmdid.NID_ROOM_MEMBERS_NTF, RoomModule, "on_room_members_ntf")
end

function RoomModule:is_master()
    local info = self:get_room_info()
    if info and info.master == self.player_id then
        return true
    end
    return false
end

function RoomModule:clear_room_data()
    self:set_room_id(nil)
    self:set_room_info(nil)
    self:set_room_ready(false)
    self:set_room_members({})
end

--请求 rpc
-------------------------------------------------------------------------
function RoomModule:send_create_room_req()
    local net_req = { name = "test", map_id = 101, passwd = "", visable = 0, version = 0 }
    local ok, res = self:call_lobby(ncmdid.NID_ROOM_CREATE_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[RoomModule][send_create_room_req] failed: %s", ok and res.code or res)
        return
    end
    log_debug("[RoomModule][send_create_room_req] success: %s", res.code)
end

function RoomModule:send_destory_room_req(room_id)
    local ok, res = self:call_lobby(ncmdid.NID_ROOM_DESTORY_REQ, { room_id = room_id })
    if not ok or check_failed(res.code) then
        log_err("[RoomModule][send_destory_room_req] failed: %s", ok and res.code or res)
        return
    end
    log_debug("[RoomModule][send_destory_room_req] success: %s", res.code)
end

function RoomModule:send_modify_room_req(room_id)
    local net_req = { room_id = room_id, mode = 1, type = 1, name = "test1", map_id = 1, passwd = "", }
    local ok, res = self:call_lobby(ncmdid.NID_ROOM_MODIFY_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[RoomModule][send_modify_room_req] failed: %s", ok and res.code or res)
        return
    end
    log_debug("[RoomModule][send_modify_room_req] success: %s", res.code)
end

function RoomModule:send_room_search_req()
    local net_req = { key = "" }
    local ok, res = self:call_lobby(ncmdid.NID_ROOM_SEARCH_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[RoomModule][send_room_search_req] failed: %s", ok and res.code or res)
        return
    end
    log_debug("[RoomModule][send_room_search_req] success: %s", serialize(res.rooms))
    for _, room in pairs(res.rooms) do
        self:send_room_enter_req(room.room_id, "")
        break
    end
end

function RoomModule:send_room_enter_req(room_id, passwd)
    local ok, res = self:call_lobby(ncmdid.NID_ROOM_ENTER_REQ, { room_id = room_id, passwd = passwd })
    if not ok or check_failed(res.code) then
        log_err("[RoomModule][send_room_enter_req] failed: %s", ok and res.code or res)
        return
    end
    log_debug("[RoomModule][send_room_enter_req] success: %s", res.code)
end

function RoomModule:send_room_quit_req(room_id)
    local ok, res = self:call_lobby(ncmdid.NID_ROOM_QUIT_REQ, { room_id = room_id })
    if not ok or check_failed(res.code) then
        log_err("[RoomModule][send_room_quit_req] failed: %s", ok and res.code or res)
        return
    end
    log_debug("[RoomModule][send_room_quit_req] success: %s", res.code)
    return true
end

function RoomModule:send_room_kick_req(room_id, tar_player_id)
    local ok, res = self:call_lobby(ncmdid.NID_ROOM_KICK_REQ, { room_id = room_id, tar_uid = tar_player_id })
    if not ok or check_failed(res.code) then
        log_err("[RoomModule][send_room_kick_req] failed: %s", ok and res.code or res)
        return
    end
    log_debug("[RoomModule][send_room_kick_req] success: %s", res.code)
    return true
end

function RoomModule:send_room_invite_req(target_id)
    local room_id = self:get_room_id()
    if not room_id then
        log_err("[RoomModule][send_room_invite_req] no room")
        return
    end
    local ok, res = self:call_lobby(ncmdid.NID_ROOM_INVITE_REQ, {room_id = room_id, player_id = target_id})
    if not ok or check_failed(res.code) then
        log_err("[TeamModule][send_room_invite_req] failed: %s", ok and res.code or res)
        return
    end
    log_debug("[TeamModule][send_room_invite_req] success: %s", res.code)
    return true
end

function RoomModule:send_room_ready_req(room_id, ready)
    local ok, res = self:call_lobby(ncmdid.NID_ROOM_READY_REQ, { room_id = room_id, ready = ready })
    if not ok or check_failed(res.code) then
        log_err("[RoomModule][send_room_ready_req] failed: %s", ok and res.code or res)
        return
    end
    log_debug("[RoomModule][send_room_ready_req] success: %s", res.code)
    return true
end

function RoomModule:send_room_begin_req(room_id)
    local ok, res = self:call_lobby(ncmdid.NID_ROOM_BEGIN_REQ, { room_id = room_id })
    if not ok or check_failed(res.code) then
        log_err("[RoomModule][send_room_begin_req] failed: %s", ok and res.code or res)
        return
    end
    log_debug("[RoomModule][send_room_begin_req] success: %s", res.code)
    return true
end

function RoomModule:send_trans_leader_req(room_id, tar_player_id)
    local ok, res = self:call_lobby(ncmdid.NID_ROOM_TRANS_LEADER_REQ, { room_id = room_id, tar_uid = tar_player_id })
    if not ok or check_failed(res.code) then
        log_err("[RoomModule][send_trans_leader_req] failed: %s", ok and res.code or res)
        return
    end
    log_debug("[RoomModule][send_trans_leader_req] success: %s", res.code)
    return true
end

function RoomModule:send_room_switch_req(room_id, pos)
    local ok, res = self:call_lobby(ncmdid.NID_ROOM_SWITCH_REQ, { room_id = room_id, pos = pos })
    if not ok or check_failed(res.code) then
        log_err("[RoomModule][send_room_switch_req] failed: %s", ok and res.code or res)
        return
    end
    log_debug("[RoomModule][send_room_switch_req] success: %s", res.code)
    return true
end

function RoomModule:send_room_siwtch_answer_req(room_id, tar_player_id, tar_pos, answer)
    local net_req = { room_id = room_id, tar_uid = tar_player_id, tar_pos = tar_pos, answer = answer }
    local ok, res = self:call_lobby(ncmdid.NID_ROOM_SWITCH_ANSWER_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[RoomModule][send_room_siwtch_answer_req] failed: %s", ok and res.code or res)
        return
    end
    log_debug("[RoomModule][send_room_siwtch_answer_req] success: %s", res.code)
    return true
end

--返回rpc
-------------------------------------------------------------------
function RoomModule:on_room_switch_ntf(data)
    log_debug("[RoomModule][on_room_switch_ntf]: %s", serialize(data))
    if self.room_id == data.room_id then
        self:send_room_siwtch_answer_req(data.room_id, data.player.player_id, data.player.pos, true)
    end
end

function RoomModule:on_room_info_ntf(data)
    log_debug("[RoomModule][on_room_info_ntf]: %s", serialize(data))
    if data.room.status > 0 then
        self:set_room_info(data.room)
        self:set_room_id(data.room.room_id)
    else
        self:clear_room_data()
    end
end

function RoomModule:on_room_members_ntf(data)
    log_debug("[RoomModule][on_room_members_ntf]: %s", serialize(data))
    local members = {}
    for _, player in pairs(data.players) do
        members[player.pos] = player
    end
    self:set_room_members(members)
end

function RoomModule:on_room_member_ntf(data)
    log_debug("[RoomModule][on_room_member_ntf]: %s", serialize(data))
    local player = data.player
    local player_id = self:get_player_id()
    if data.reason == 0 then
        if player.player_id == player_id then
            self:set_room_ready(player.ready)
        end
        if player.status == 0 then
            self.room_members[player.pos] = nil
        else
            self.room_members[player.pos] = player
        end
    else
        if player.player_id == player_id then
            self:clear_room_data()
        else
            self.room_members[player.pos] = nil
        end
    end
end

return RoomModule
