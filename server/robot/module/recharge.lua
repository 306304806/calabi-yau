--bag.lua
local tsize         = table_ext.size
local log_err       = logger.err
local log_debug     = logger.debug
local check_failed  = utility.check_failed
local serialize     = logger.serialize

local PlatCSCmdID   = ncmd_plat.CCmdId
local CSCmdID       = ncmd_cs.NCmdId

local RechargeModule = mixin(
    "send_recharge_commodity_sync_req",  -- 请求商品列表
    "send_recharge_order_create_req",    -- 创建订单
    "send_recharge_order_send_req"       -- 请求发货
)

local prop = property(RechargeModule)
prop:accessor("recharge_commodity_map", {})  -- 商品map
prop:accessor("recharge_paid_orders", {})    -- 已经支付订单号

function RechargeModule:__init()

end

function RechargeModule:send_recharge_commodity_sync_req()
    local net_req = { version = "" }
    local ok, net_res = self:call_plat(PlatCSCmdID.NID_RECHARGE_COMMODITY_SYNC_REQ, net_req)
    if not ok or check_failed(net_res.code) then
        log_err("[RechargeModule][send_recharge_commodity_sync_req] req failed! %s", ok and net_res.code)
        return false
    end

    for _, info in pairs(net_res.commodity_infos or {}) do
        self.recharge_commodity_map[info.commodity_id] = info
    end

    log_debug("[RechargeModule][send_recharge_commodity_sync_req] count=%s, code=%s", tsize(self.recharge_commodity_map), net_res.code)

    return true
end

function RechargeModule:send_recharge_order_create_req()
    local net_req = {
        pay_platform_id = 1,
        pay_open_id = "76561199044825594",
        commodities = {{commodity_id = 2, commodity_count = 3}},
        amount = 100,
        currency = "CNY",
        language = "zh",
    }
    local ok, net_res = self:call_plat(PlatCSCmdID.NID_RECHARGE_ORDER_CREATE_REQ, net_req)
    if not ok or check_failed(net_res.code) then
        log_err("[RechargeModule][send_recharge_order_create_req] req failed! %s", ok and serialize(net_res))
        return false
    end

    log_debug("[RechargeModule][send_recharge_order_create_req] order_id=%s", serialize(net_res))--net_res.order_id)

    return true
end

function RechargeModule:send_recharge_order_send_req()
    local net_req = {
        order_id = "12251509005800009"
    }

    local ok, res = self:call_lobby(CSCmdID.NID_RECHARGE_ORDER_SEND_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[RechargeModule][send_recharge_order_send_req] req failed! %s", ok and res.code)
        return false
    end

    log_debug("[RechargeModule][send_recharge_order_send_req] code:%s", res.code)

    return true
end

return RechargeModule
