--standings.lua
local log_debug     = logger.debug
local log_err       = logger.err
local check_failed  = utility.check_failed
local serialize     = logger.serialize

local CSCmdID       = ncmd_cs.NCmdId

local StandingsModule = mixin(
    "send_laud_req",
    "send_add_standings_req",
    "send_get_standings_info_req",
    "send_get_standings_titles_req"
)

function StandingsModule:__init()
end

-- 发送查询请求
function StandingsModule:send_add_standings_req(player_id)
    local net_req = {
        room_id = 123
    }
    local ok, net_res = self:call_lobby(CSCmdID.NID_STANDINGS_INFO_REQ, net_req)
    log_debug("[StandingsModule][send_standings_info_info_req] ok=%s,net_res=%s", ok, serialize(net_res))
    if not ok or check_failed(net_res.code) then
        return
    end
end

-- 发送查询请求
function StandingsModule:send_get_standings_info_req(player_id)
    log_debug("[StandingsModule][send_get_head_icons_req] player_id=%s", player_id)
    local net_req = {
        player_id = player_id
    }
    local ok, net_res = self:call_lobby(CSCmdID.NID_STANDINGS_GET_HEAD_ICONS_REQ, net_req)
    if not ok then
        log_err("[StandingsModule][send_get_head_icons_req] ok=%s,net_res=%s", ok, serialize(net_res))
    end
    log_debug("[StandingsModule][send_get_head_icons_req] ok=%s,net_res=%s", ok, serialize(net_res))
end

-- 发送查询请求
function StandingsModule:send_get_standings_titles_req(player_id)
    log_debug("[StandingsModule][send_set_head_icon_req] player_id=%s", player_id)
    local net_req = {
        -- player_id = player_id
        icon_id = 6
    }
    local ok, net_res = self:call_lobby(CSCmdID.NID_STANDINGS_SET_HEAD_ICON_REQ, net_req)
    if not ok or check_failed(net_res.code) then
        log_err("[StandingsModule][send_set_head_icon_req] ok=%s,net_res=%s", ok, serialize(net_res))
        return
    end
    log_debug("[StandingsModule][send_set_head_icon_req] ok=%s,net_res=%s", ok, serialize(net_res))
end

-- 发送查询请求
function StandingsModule:send_laud_req(player_id)
    local net_req = {
        -- player_id = 751933084292613121
        player_id = player_id
    }
    local ok, net_res = self:call_lobby(CSCmdID.NID_STANDINGS_LAUD_REQ, net_req)
    if not ok or check_failed(net_res.code) then
        log_err("[StandingsModule][send_laud_req] ok=%s,net_res=%s", ok, serialize(net_res))
        return
    end
    log_debug("[StandingsModule][send_laud_req] ok=%s,net_res=%s", ok, serialize(net_res))
end

return StandingsModule
