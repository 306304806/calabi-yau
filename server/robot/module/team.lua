--team.lua
local log_err       = logger.err
local log_debug     = logger.debug
local check_failed  = utility.check_failed
local serialize     = logger.serialize

local GamaMode      = enum("GameMode")

local ncmdid        = ncmd_cs.NCmdId

local TeamModule = mixin(
    "is_leader",
    "clear_team_data",
    "send_create_team_req",
    "send_ready_team_req",
    "send_kick_member_req",
    "send_exit_team_req",
    "send_apply_member_req",
    "send_invite_team_req",
    "send_invite_reply_req",
    "send_apply_reply_req"
)
local prop = property(TeamModule)
prop:accessor("team_id", nil)               --队伍id
prop:accessor("team_leader", nil)           --队伍leader
prop:accessor("team_ready", false)          --队伍状态
prop:accessor("team_members", {})           --队伍成员信息

function TeamModule:__init()
    self:register_doer(ncmdid.NID_TEAM_INFO_NTF, TeamModule, "on_team_info_ntf")
    self:register_doer(ncmdid.NID_TEAM_EXIT_NTF, TeamModule, "on_team_exit_ntf")
    self:register_doer(ncmdid.NID_TEAM_KICK_NTF, TeamModule, "on_team_kick_ntf")
    self:register_doer(ncmdid.NID_TEAM_APPLY_NTF, TeamModule, "on_team_apply_ntf")
    self:register_doer(ncmdid.NID_TEAM_ENTER_NTF, TeamModule, "on_team_enter_ntf")
    self:register_doer(ncmdid.NID_TEAM_READY_NTF, TeamModule, "on_team_ready_ntf")
    self:register_doer(ncmdid.NID_TEAM_INVITE_NTF, TeamModule, "on_team_invite_ntf")
    self:register_doer(ncmdid.NID_TEAM_REPLY_NTF, TeamModule, "on_team_reply_ntf")
    self:register_doer(ncmdid.NID_TEAM_APPLY_REPLY_NTF, TeamModule, "on_team_apply_reply_ntf")
    self:register_doer(ncmdid.NID_TEAM_TRANS_LEADER_NTF, TeamModule, "on_team_trans_leader_ntf")
end

function TeamModule:is_leader()
    return self.team_leader == self.player_id
end

function TeamModule:clear_team_data()
    self:set_team_id(nil)
    self:set_team_ready(false)
    self:set_team_leader(nil)
    self:set_team_members({})
end

--请求 rpc
-------------------------------------------------------------------------
function TeamModule:send_create_team_req()
    local ok, res = self:call_lobby(ncmdid.NID_TEAM_CREATE_REQ, { mode = GamaMode.RANK })
    if not ok or check_failed(res.code) then
        log_err("[TeamModule][send_create_team_req] failed: %s", ok and res.code or res)
        return
    end
    log_debug("[TeamModule][send_create_team_req] success: %s", res.code)
end

function TeamModule:send_ready_team_req()
    local team_id = self:get_team_id()
    if not team_id then
        log_err("[TeamModule][send_ready_team_req] no team or readyed")
        return
    end
    local ok, res = self:call_lobby(ncmdid.NID_TEAM_READY_REQ, {team_id = team_id, status = 1})
    if not ok or check_failed(res.code) then
        log_err("[TeamModule][send_ready_team_req] failed: %s", ok and res.code or res)
        return
    end
    log_debug("[TeamModule][send_ready_team_req] success: %s-%s", self.player_id, res.code)
end

function TeamModule:send_kick_member_req(tid)
    local team_id = self:get_team_id()
    if not team_id then
        log_err("[TeamModule][send_kick_member_req] no team")
        return
    end
    local ok, res = self:call_lobby(ncmdid.NID_TEAM_KICK_REQ, {team_id = team_id, kick_id = tid})
    if not ok or check_failed(res.code) then
        log_err("[TeamModule][send_kick_member_req] failed: %s", ok and res.code or res)
        return
    end
    log_debug("[TeamModule][send_kick_member_req] success: %s", res.code)
end

function TeamModule:send_exit_team_req()
    local team_id = self:get_team_id()
    if not team_id then
        log_err("[TeamModule][send_exit_team_req] no team")
        return
    end
    local ok, res = self:call_lobby(ncmdid.NID_TEAM_EXIT_REQ, {team_id = team_id})
    if not ok or check_failed(res.code) then
        log_err("[TeamModule][send_exit_team_req] failed: %s", ok and res.code or res)
        return
    end
    log_debug("[TeamModule][send_exit_team_req] success: %s", res.code)
    return true
end

function TeamModule:send_apply_member_req(team_id)
    local ok, res = self:call_lobby(ncmdid.NID_TEAM_APPLY_REQ, {team_id = team_id})
    if not ok or check_failed(res.code) then
        log_err("[TeamModule][send_apply_member_req] failed: %s", ok and res.code or res)
        return
    end
    log_debug("[TeamModule][send_apply_member_req] success: %s", res.code)
end

function TeamModule:send_invite_team_req(tid)
    local team_id = self:get_team_id()
    if not team_id then
        log_err("[TeamModule][send_invite_team_req] no team")
        return
    end
    local ok, res = self:call_lobby(ncmdid.NID_TEAM_INVITE_REQ, {team_id = team_id, player_id = tid})
    if not ok or check_failed(res.code) then
        log_err("[TeamModule][send_invite_team_req] failed: %s", ok and res.code or res)
        return
    end
    log_debug("[TeamModule][send_invite_team_req] success: %s", res.code)
end

function TeamModule:send_invite_reply_req(team_id, player_id, sure)
    if self:get_team_id() then
        log_err("[TeamModule][send_invite_reply_req] has team")
        return
    end
    local ok, res = self:call_lobby(ncmdid.NID_TEAM_REPLY_REQ, {player_id = player_id, team_id = team_id, sure = sure })
    if not ok or check_failed(res.code) then
        log_err("[TeamModule][send_invite_reply_req] failed: %s", ok and res.code or res)
        return
    end
    log_debug("[TeamModule][send_invite_reply_req] success: %s", res.code)
    return true
end

function TeamModule:send_apply_reply_req(team_id, player_id, sure)
    if not self:get_team_id() then
        log_err("[TeamModule][send_apply_reply_req] not team")
        return
    end
    local ok, res = self:call_lobby(ncmdid.NID_TEAM_APPLY_REPLY_REQ, {player_id = player_id, team_id = team_id, sure = sure })
    if not ok or check_failed(res.code) then
        log_err("[TeamModule][send_apply_reply_req] failed: %s", ok and res.code or res)
        return
    end
    log_debug("[TeamModule][send_apply_reply_req] success: %s", res.code)
    return true
end

function TeamModule:send_trans_leader_req(tar_player_id)
    local team_id = self:get_team_id()
    if not team_id then
        log_err("[TeamModule][send_trans_leader_req] no team")
        return
    end
    local ok, res = self:call_lobby(ncmdid.NID_TEAM_TRANS_LEADER_REQ, {team_id = team_id, player_id = tar_player_id})
    if not ok or check_failed(res.code) then
        log_err("[TeamModule][send_trans_leader_req] failed: %s", ok and res.code or res)
        return
    end
    log_debug("[TeamModule][send_trans_leader_req] success: %s", res.code)
    return true
end

--返回rpc
-------------------------------------------------------------------
function TeamModule:on_team_invite_ntf(data)
    log_debug("[TeamModule][on_team_invite_ntf]: %s", serialize(data))
    self:send_invite_reply_req(data.team_id, data.player_id, 1)
end

function TeamModule:on_team_trans_leader_ntf(data)
    log_debug("[TeamModule][on_team_trans_leader_ntf]: %s", serialize(data))
    self:set_team_leader(data.leader_id)
end

function TeamModule:on_team_info_ntf(data)
    log_debug("[TeamModule][on_team_info_ntf]: %s", serialize(data))
    self:set_team_id(data.team_id)
    self:set_team_leader(data.leader_id)
    local members = {}
    for _, player in pairs(data.members) do
        members[player.player_id] = player
    end
    self:set_team_members(members)
end

function TeamModule:on_team_exit_ntf(data)
    log_debug("[TeamModule][on_team_exit_ntf]: %s", serialize(data))
    if data.player_id == self.player_id then
        self:clear_team_data()
    else
        self.team_members[data.player_id] = nil
    end
end

function TeamModule:on_team_kick_ntf(data)
    log_debug("[TeamModule][on_team_kick_ntf]: %s", serialize(data))
    if data.player_id == self.player_id then
        self:clear_team_data()
    else
        self.team_members[data.player_id] = nil
    end
end

function TeamModule:on_team_apply_ntf(data)
    log_debug("[TeamModule][on_team_apply_ntf]: %s", serialize(data))
    self:send_apply_reply_req(self:get_team_id(), data.player_id, 1)
end

function TeamModule:on_team_reply_ntf(data)
    log_debug("[TeamModule][on_team_reply_ntf]: %s", serialize(data))
end

function TeamModule:on_team_apply_reply_ntf(data)
    log_debug("[TeamModule][on_team_apply_reply_ntf]: %s", serialize(data))
end

function TeamModule:on_team_enter_ntf(data)
    log_debug("[TeamModule][on_team_enter_ntf]: %s", serialize(data))
    self.team_members[data.member.player_id] = data.member
end

function TeamModule:on_team_ready_ntf(data)
    log_debug("[TeamModule][on_team_ready_ntf]: %s", serialize(data))
    if self.player_id == data.player_id then
        self:set_team_ready(data.status)
    end
    local member = self.team_members[data.player_id]
    member.status = data.status
end

return TeamModule
