--setting.lua
local log_err       = logger.err
local log_info      = logger.info
local check_failed  = utility.check_failed
local serialize     = logger.serialize
local ncmdid        = ncmd_cs.NCmdId

local VCardModule = mixin(
    "send_self_vcard_update_req"
)

local prop = property(VCardModule)
prop:accessor("vcard_info", {})

function VCardModule:__init()
    self:register_doer(ncmdid.NID_VCARD_RESOURCE_SYNC_NTF, VCardModule, "on_vcard_resource_sync_ntf")
end

function VCardModule:send_self_vcard_update_req()
    local net_req = {
        role_id      = 101,
        role_skin_id = 0,
        frame_id     = 1,
    }
    local ok, res = self:call_lobby(ncmdid.NID_SELF_VCARD_UPDATE_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[VCardModule][send_self_vcard_update_req] faild: %s", ok and res.code or res)
        return
    end

    log_info("[VCardModule][send_self_vcard_update_req]: %s", serialize(res))
end

function VCardModule:on_vcard_resource_sync_ntf(ntf)
    log_info("[VCardModule][on_vcard_resource_sync_ntf]: %s", serialize(ntf))
end

-- export
return VCardModule