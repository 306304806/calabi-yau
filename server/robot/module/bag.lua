--bag.lua
local sformat       = string.format
local log_err       = logger.err
--local log_info      = logger.info
local log_debug     = logger.debug
local check_failed  = utility.check_failed
local serialize     = logger.serialize

local ncmdid        = ncmd_cs.NCmdId

local BagModule = mixin(
    "get_equiped_weapon_uuid",
    "send_use_item_req",
    "send_equip_weapon_req",
    "send_unequip_weapon_req",
    "send_gm_add_weapon_exp",
    "parse_role_prepare_info",
    "send_unlock_attachment",
    "send_equip_attachment",
    "send_unequip_attachment",
    "send_use_addition_card",
    "send_use_mod_name",
    "send_gm_add_item_req"
)

local prop = property(BagModule)
prop:accessor("bag_items", {})              -- 背包物品
prop:accessor("weapon_items", {})           -- 武器相关
prop:accessor("role_prepares", {})          -- 角色备战信息
prop:accessor("player_prepares", {})        -- 玩家备战信息
prop:accessor("equiped_weapon", {})         -- 已装备武器集合

function BagModule:__init()
    self:register_doer(ncmdid.NID_BAG_ALL_ITEMS_SYNC_NTF, BagModule, "on_bag_sync_all_items_ntf")
    self:register_doer(ncmdid.NID_BAG_PART_ITEMS_SYNC_NTF, BagModule, "on_bag_sync_part_items_ntf")
    self:register_doer(ncmdid.NID_PLAYER_ALL_PREPARE_SYNC_NTF, BagModule, "on_prepares_sync_prepares_infos")
    self:register_doer(ncmdid.NID_ROLE_PREPARE_SYNC_NTF, BagModule, "on_prepares_sync_part_roles_infos")
    self:register_doer(ncmdid.NID_PLAYER_PREPARE_SYNC_NTF, BagModule, "on_prepares_sync_part_player_infos")
    self:register_doer(ncmdid.NID_BUFF_LIST_NTF, BagModule, "on_sync_buff_list")
end

--请求 rpc
--------------------------------------------------------------------------------
function BagModule:send_gm_add_item_req(item_id, count)
    log_debug("BagModule:send_gm_add_item_req->item_id:%s, count:%s", item_id, count)
    local net_req = { command = sformat("add_item    %s   %s", item_id, count) }
    local ok, res = self:call_lobby(ncmdid.NID_GM_COMMAND_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[BagModule][send_gm_add_item_req] failed: %s", ok and res.code or res)
        return false
    end
    log_debug("[BagModule][send_gm_add_item_req] success: %s", res.code)

    return true
end

function BagModule:send_use_item_req(item_id, count)
    local bag_items = self:get_bag_items()
    local item_uuid = nil
    --log_info("[BagModule][send_use_item_req]->bag_items:%s", serialize(bag_items))
    for id, item in pairs(bag_items) do
        if item_id == item.item_id then
            item_uuid = id
            break
        end
    end

    if not item_uuid then
        log_err("[BagModule][send_use_item_req] find item_uuid failed! item_uuid: %s, item_id:%s", item_uuid, item_id)
        return false
    end

    local net_req = { item_uuid = item_uuid, item_id = item_id, count = count }
    log_debug("[BagModule][send_use_item_req]->net_req:%s", serialize(net_req))
    local ok, res = self:call_lobby(ncmdid.NID_ITEM_USE_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[BagModule][send_use_item_req] failed: %s", ok and res.code or res)
        return false
    end
    --log_debug("[BagModule][send_use_item_req] success: %s, result_list:%s", res.code, serialize(res.result_list))

    return true
end

-- 装备武器
function BagModule:send_equip_weapon_req(role_id, bag_index, slot_pos, weapon_id)
    local equip_weapon_uuid = 0
    for uuid, weapon in pairs(self.weapon_items) do
        if weapon.weapon_base.item_id == weapon_id and not self.equiped_weapon[uuid] then
            equip_weapon_uuid = uuid
            break
        end
    end

    if equip_weapon_uuid <= 0 then
        log_err("[BagModule][send_equip_weapon_req]->get weapon_uuid failed! weapon_id:%s", weapon_id)
        return false
    end

    -- 遍历整个未装备的武器集合，直到成功装备
    local net_req = { role_id = role_id, weapon_uuid = equip_weapon_uuid, index = bag_index, slot_pos = slot_pos }
    log_debug("[BagModule][send_equip_weapon_req]->net_req:%s", serialize(net_req))
    local ok, res = self:call_lobby(ncmdid.NID_EQUIP_WEAPON_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[BagModule][send_equip_weapon_req] failed: %s", ok and res.code or res)
        return false
    end

    self.equiped_weapon[equip_weapon_uuid] = true

    return true
end

-- 发送gm添加武器经验
function BagModule:send_gm_add_weapon_exp(weapon_id, add_exp)
    --log_debug("BagModule:send_gm_add_weapon_exp->weapon_id:%s, add_exp:%s", weapon_id, add_exp)
    local net_req = { command = sformat("add_weapon_exp    %s   %s", weapon_id, add_exp) }
    local ok, res = self:call_lobby(ncmdid.NID_GM_COMMAND_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[BagModule][send_gm_add_weapon_exp] failed: %s", ok and res.code or res)
        return false
    end
    log_debug("[BagModule][send_gm_add_weapon_exp] success: %s", res.code)

    return true
end

-- 卸下武器
function BagModule:send_unequip_weapon_req(role_id, bag_index, slot_pos, weapon_uuid)
    local net_req = {role_id = role_id, index = bag_index, weapon_uuid = weapon_uuid, slot_pos = slot_pos}
    log_debug("[BagModule][send_unequip_weapon_req]->net_req:%s", serialize(net_req))
    local ok, res = self:call_lobby(ncmdid.NID_UNLOAD_WEAPON_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[BagModule][send_unequip_weapon_req] failed: %s", ok and res.code or res)
        return false
    end

    self.equiped_weapon[weapon_uuid] = nil
    return true
end

-- 请求解锁配件
function BagModule:send_unlock_attachment(weapon_uuid, attach_type, attach_id, item_id)
    local item_uuid = 0
    for uuid, item in pairs(self.bag_items) do
        if item.item_id == item_id then
            item_uuid = uuid
            break
        end
    end

    if item_uuid == 0 then
        log_err("[BagModule][send_unlock_attachment] get unlock item failed! weapon_uuid:%s, item_id:%s", weapon_uuid, item_id)
        return false
    end

    -- 先给玩家加点经验
    local gm_req = { command = sformat("add_attribute    %s   %s", 3, 10000) }
    local gm_ok, gm_res = self:call_lobby(ncmdid.NID_GM_COMMAND_REQ, gm_req)
    if not gm_ok or check_failed(gm_res.code) then
        log_err("[BagModule][send_unlock_attachment] gm add failed: %s", gm_ok and gm_res.code or gm_res)
        return false
    end
    log_debug("[BagModule][send_unlock_attachment]gm add exp success: %s", gm_res.code)

    local net_req = {weapon_uuid = weapon_uuid, attach_type = attach_type, attach_id = attach_id, item_uuid = item_uuid}
    log_debug("[BagModule][send_unlock_attachment]->net_req:%s", serialize(net_req))
    local ok, res = self:call_lobby(ncmdid.NID_ATTACHMENT_UNLOCK_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[BagModule][send_unlock_attachment] failed: %s", ok and res.code or res)
        return false
    end

    return true
end

-- 请求装备配件
function BagModule:send_equip_attachment(weapon_uuid, attach_type, attach_id)
    local net_req = {weapon_uuid = weapon_uuid, attach_type = attach_type, attach_id = attach_id}
    log_debug("[BagModule][send_equip_attachment]->net_req:%s", serialize(net_req))
    local ok, res = self:call_lobby(ncmdid.NID_ATTACHMENT_EQUIP_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[BagModule][send_equip_attachment] failed: %s", ok and res.code or res)
        return false
    end

    return true
end

-- 请求卸下配件
function BagModule:send_unequip_attachment(weapon_uuid, attach_type, attach_id)
    local net_req = {weapon_uuid = weapon_uuid, attach_type = attach_type, attach_id = attach_id}
    log_debug("[BagModule][send_unequip_attachment]->net_req:%s", serialize(net_req))
    local ok, res = self:call_lobby(ncmdid.NID_ATTACHMENT_UNLOAD_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[BagModule][send_unequip_attachment] failed: %s", ok and res.code or res)
        return false
    end

    return true
end

--回执 rpc
--------------------------------------------------------------------------------
-- 同步背包所有物品
function BagModule:on_bag_sync_all_items_ntf(data)
    --log_debug("[BagModule][on_bag_sync_all_items_ntf]->data:%s", serialize(data))
    local bag_items = self:get_bag_items()
    for idx, item in pairs(data.item_list) do
        bag_items[item.item_uuid] = item
    end

    local weapon_items = self:get_weapon_items()
    for idx, weapon in pairs(data.weapon_list) do
        weapon_items[weapon.weapon_base.item_uuid] = weapon
    end
end

-- 同步部分背包物品
function BagModule:on_bag_sync_part_items_ntf(data)
    --log_debug("[BagModule][on_bag_sync_part_items_ntf]->data:%s", serialize(data))
    local bag_items = self:get_bag_items()
    for idx, item in pairs(data.item_list) do
        if item.count == 0 then
            bag_items[item.item_uuid] = nil
        else
            bag_items[item.item_uuid] = item
        end
    end

    for idx, weapon in pairs(data.weapon_list) do
        if weapon.weapon_base.count == 0 then
            bag_items[weapon.weapon_base.item_uuid] = nil
        else
            bag_items[weapon.weapon_base.item_uuid] = weapon
        end
    end
end

-- 同步玩家备战数据[全量]
function BagModule:on_prepares_sync_prepares_infos(data)
    self:parse_role_prepare_info(data.roles)

    self.player_prepares.decals = data.decals
    self.player_prepares.vfxes  = data.vfxes
    self.player_prepares.bg_uuid = data.bg_uuid
    self.player_prepares.selected_role = data.selected_role
    --log_debug("[BagModule][on_prepares_sync_prepares_infos]->player_prepares:%s", serialize(data))
end

-- 解析角色备战数据
function BagModule:parse_role_prepare_info(roles)
    self.equiped_weapon = {}
    for _, info in pairs(roles) do
        if not self.role_prepares[info.role_id] then
            self.role_prepares[info.role_id] = {weapons = {}, selected = 0, postures = {}, stick_uuid = 0}
        end
        local role_prepare = self.role_prepares[info.role_id]
        role_prepare.selected = info.selected
        role_prepare.postures = info.postures
        role_prepare.stick_uuid = info.stick_uuid

        for _, weapon_info in pairs(info.weapons) do
            if not role_prepare.weapons[weapon_info.index] then
                role_prepare.weapons[weapon_info.index] = {}
            end
            local role_weapon = role_prepare.weapons[weapon_info.index]
            role_weapon.weapon_infos = weapon_info.weapon_infos

            for _, slot_info in pairs(weapon_info.weapon_infos) do
                self.equiped_weapon[slot_info.weapon_uuid] = true
            end
        end
    end
end

-- 同步角色备战数据[增量]
function BagModule:on_prepares_sync_part_roles_infos(data)
    self:parse_role_prepare_info(data.roles)
end

-- 同步玩家备战数据[增量]
function BagModule:on_prepares_sync_part_player_infos(data)
    local player_prepares           = self.player_prepares
    player_prepares.decals          = data.decals
    player_prepares.vfxes           = data.vfxes
    player_prepares.bg_uuid         = data.bg_uuid
    player_prepares.selected_role   = data.selected_role

    --log_debug("[BagModule][on_prepares_sync_part_player_infos]->player_prepares:%s", serialize(data))
end

-- 获取已装备武器uuid
function BagModule:get_equiped_weapon_uuid(role_id, bag_index, slot_pos)
    if not self.role_prepares[role_id] or not self.role_prepares[role_id].weapons or not self.role_prepares[role_id].weapons[bag_index] then
        log_err("[BagModule][get_equiped_weapon_uuid]->get weapons failed! role_id:%s, bag_index:%s", role_id, bag_index)
        return false
    end

    local weapons = self.role_prepares[role_id].weapons[bag_index]
    local weapon_uuid = 0
    for _, data in pairs(weapons.weapon_infos) do
        if data.slot_pos == slot_pos then
            return data.weapon_uuid
        end
    end

    log_err("[BagModule][get_equiped_weapon_uuid]->get weapon_uuid failed! role_id:%s, bag_index:%s, slot_pos:%s", role_id, bag_index, slot_pos)
    return weapon_uuid
end

-- 使用加成卡
function BagModule:send_use_addition_card(item_id, count)
    local bag_items = self:get_bag_items()
    local item_uuid = nil
    for id, item in pairs(bag_items) do
        if item_id == item.item_id then
            log_err("BagModule:send_use_addition_card->item_uuid:%s, item_id:%s", id, item_id)
            item_uuid = id
            break
        end
    end

    if not item_uuid then
        log_err("[BagModule][send_use_addition_card] find item_uuid failed! item_id:%s", item_id)
        return false
    end

    local net_req = {item_uuid = item_uuid, item_count = count}
    log_debug("[BagModule][send_use_addition_card]->net_req:%s", serialize(net_req))
    local ok, res = self:call_lobby(ncmdid.NID_BAG_ADDITION_CARD_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[BagModule][send_use_addition_card] failed: %s", ok and res.code or res)
        return false
    end

    return true
end

-- 使用改名卡
function BagModule:send_use_mod_name(item_id, new_nick)
    local bag_items = self:get_bag_items()
    local item_uuid = nil
    for id, item in pairs(bag_items) do
        if item_id == item.item_id then
            item_uuid = id
            break
        end
    end

    if not item_uuid then
        log_err("[BagModule][send_use_mod_name] find item_uuid failed! item_id:%s", item_id)
        return false
    end

    local net_req = {item_uuid = item_uuid, new_nick = new_nick}
    log_debug("[BagModule][send_use_mod_name]->net_req:%s", serialize(net_req))
    local ok, res = self:call_lobby(ncmdid.NID_BAG_MOD_NAME_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[BagModule][send_use_mod_name] failed: %s", ok and res.code or res)
        return false
    end

    return true
end

-- buff信息同步
function BagModule:on_sync_buff_list(data)
    log_debug("[BagModule][on_prepares_sync_part_player_infos]->player_prepares:%s", serialize(data))
end

return BagModule
