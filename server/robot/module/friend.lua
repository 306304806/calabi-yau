--friend.lua
local tinsert       = table.insert
local tremove       = table.remove
local log_warn      = logger.warn
local log_debug     = logger.debug
local check_failed  = utility.check_failed
local serialize     = logger.serialize

local platc_cmdid   = ncmd_plat.CCmdId

local FriendModule = mixin(
    "send_friend_add_req",
    "send_friend_del_req",
    "send_friend_reply_req",
    "send_friend_search_req",
    "send_friend_group_add_req",
    "send_friend_group_del_req"
)

local prop = property(FriendModule)
prop:accessor("groups", {})                 --分组
prop:accessor("friends", {})                --好友

function FriendModule:__init()
    self:register_doer(platc_cmdid.NID_FRIEND_ADD_NTF, FriendModule, "on_friend_add_ntf")
    self:register_doer(platc_cmdid.NID_FRIEND_DEL_NTF, FriendModule, "on_friend_del_ntf")
    self:register_doer(platc_cmdid.NID_FRIEND_MEMBER_NTF, FriendModule, "on_friend_member_ntf")

    self:register_doer(platc_cmdid.NID_FRIEND_LIST_NTF, FriendModule, "on_friend_list_ntf")
    self:register_doer(platc_cmdid.NID_FRIEND_GROUP_NTF, FriendModule, "on_friend_group_ntf")
end

-- 添加好友
function FriendModule:send_friend_add_req(target_id, friend_type)
    local net_req = { target_id = target_id, friend_type  = friend_type }
    local ok, net_res = self:call_plat(platc_cmdid.NID_FRIEND_ADD_REQ, net_req)
    if not ok or check_failed(net_res.code) then
        log_warn("[FriendModule][send_friend_add_req] ok=%s,ec=%s", ok, serialize(net_res))
        return
    end
    log_debug("[FriendModule][send_friend_add_req] res=%s", serialize(net_res))
end

-- 删除好友
function FriendModule:send_friend_del_req(target_id, friend_type)
    local net_req = { target_id = target_id, friend_type  = friend_type }
    local ok, net_res = self:call_plat(platc_cmdid.NID_FRIEND_DEL_REQ, net_req)
    if not ok or check_failed(net_res.code) then
        log_warn("[FriendModule][send_friend_del_req] ok=%s,ec=%s", ok, serialize(net_res))
        return
    end
    log_debug("[FriendModule][send_friend_del_req] res=%s", serialize(net_res))
end

-- 回复玩家
function FriendModule:send_friend_reply_req(target_id, reply)
    local net_req = { target_id = target_id, reply = reply }
    local ok, net_res = self:call_plat(platc_cmdid.NID_FRIEND_REPLY_REQ, net_req)
    if not ok or check_failed(net_res.code) then
        log_warn("[FriendModule][send_friend_reply_req] ok=%s,ec=%s", ok, serialize(net_res))
        return
    end
    log_debug("[FriendModule][send_friend_reply_req] res=%s", serialize(net_res))
end

-- 搜索玩家
function FriendModule:send_friend_search_req(player_id, nick)
    local net_req = { player_id = player_id, nick  = nick }
    local ok, net_res = self:call_plat(platc_cmdid.NID_FRIEND_SEARCH_REQ, net_req)
    if not ok or check_failed(net_res.code) then
        log_warn("[FriendModule][send_friend_search_req] ok=%s,ec=%s", ok, serialize(net_res))
        return
    end
    log_debug("[FriendModule][send_friend_search_req] res=%s", serialize(net_res))
    return net_res.friends
end

-- 添加分组
function FriendModule:send_friend_group_add_req(group_name)
    local net_req = { group_name = group_name }
    local ok, net_res = self:call_plat(platc_cmdid.NID_FRIEND_GROUP_ADD_REQ, net_req)
    if not ok or check_failed(net_res.code) then
        log_warn("[FriendModule][send_friend_group_add_req] ok=%s,ec=%s", ok, serialize(net_res))
        return
    end
    log_debug("[FriendModule][send_friend_group_add_req] res=%s", serialize(net_res))
end

-- 删除分组
function FriendModule:send_friend_group_del_req(group_id)
    local net_req = { group_id = group_id }
    local ok, net_res = self:call_plat(platc_cmdid.NID_FRIEND_GROUP_DEL_REQ, net_req)
    if not ok or check_failed(net_res.code) then
        log_warn("[FriendModule][send_friend_group_del_req] ok=%s,ec=%s", ok, serialize(net_res))
        return
    end
    log_debug("[FriendModule][send_friend_group_del_req] res=%s", serialize(net_res))
end

-- 修改分组
function FriendModule:send_friend_group_modify_req(group_id, group_name)
    local net_req = { group_id = group_id, group_name = group_name }
    local ok, net_res = self:call_plat(platc_cmdid.NID_FRIEND_GROUP_MODIFY_REQ, net_req)
    if not ok or check_failed(net_res.code) then
        log_warn("[FriendModule][send_friend_group_modify_req] ok=%s,ec=%s", ok, serialize(net_res))
        return
    end
    log_debug("[FriendModule][send_friend_group_modify_req] res=%s", serialize(net_res))
end

-- 交换分组
function FriendModule:send_friend_group_swap_req(group_id, index)
    local net_req = { group_id = group_id, index = index }
    local ok, net_res = self:call_plat(platc_cmdid.NID_FRIEND_GROUP_SWAP_REQ, net_req)
    if not ok or check_failed(net_res.code) then
        log_warn("[FriendModule][send_friend_group_swap_req] ok=%s,ec=%s", ok, serialize(net_res))
        return
    end
    log_debug("[FriendModule][send_friend_group_swap_req] res=%s", serialize(net_res))
end

-- 移到好友分组
function FriendModule:send_friend_group_move_req(group_id, target_id)
    local net_req = { group_id = group_id, target_id = target_id }
    local ok, net_res = self:call_plat(platc_cmdid.NID_FRIEND_GROUP_MOVE_REQ, net_req)
    if not ok or check_failed(net_res.code) then
        log_warn("[FriendModule][send_friend_group_move_req] ok=%s,ec=%s", ok, serialize(net_res))
        return
    end
    log_debug("[FriendModule][send_friend_group_move_req] res=%s", serialize(net_res))
end

-- 服务器回执
-------------------------------------------------------------------------------------------
-- 分组列表通知
function FriendModule:on_friend_group_ntf(data)
    log_debug("[FriendModule][on_friend_group_ntf] %s", serialize(data))
    self:set_groups(data.groups)
end

-- 好友列表通知
function FriendModule:on_friend_list_ntf(data)
    log_debug("[FriendModule][on_friend_list_ntf] %s", serialize(data))
    self:set_friends(data.friends)
end

-- 好友信息通知
function FriendModule:on_friend_member_ntf(data)
    log_debug("[FriendModule][on_friend_member_ntf] %s", serialize(data))
    tinsert(self.friends, data.friend)
end

-- 好友添加通知
function FriendModule:on_friend_add_ntf(data)
    log_debug("[FriendModule][on_friend_add_ntf] %s", serialize(data))
    --回复同意
    self:send_friend_reply_req(data.friend.player_id, 1)
end

-- 好友删除通知
function FriendModule:on_friend_del_ntf(data)
    log_debug("[FriendModule][on_friend_del_ntf] %s", serialize(data))
    for idx, friend in pairs(self.friends) do
        if friend.palyer.palyer_id == data.target_id then
            tremove(self.friends, idx)
            break
        end
    end
end

return FriendModule
