--match.lua
local log_err       = logger.err
local log_info      = logger.info
local check_failed  = utility.check_failed
local serialize     = logger.serialize

local ncmdid        = ncmd_cs.NCmdId

local MatchModule = mixin(
    "send_match_join_req",
    "send_match_quit_req"
)

local prop = property(MatchModule)
prop:accessor("match_room_id", nil)               --匹配房间

function MatchModule:__init()
    self:register_doer(ncmdid.NID_ENTER_BATTLE_NTF, MatchModule, "on_enter_battle_ntf")
    self:register_doer(ncmdid.NID_MATCH_COMPLETE_NTF, MatchModule, "on_match_complete_ntf")
end

--请求 rpc
--------------------------------------------------------------------------------
-- 请求匹配
function MatchModule:send_match_join_req()
    local net_req = { version = 1, }
    local ok, res = self:call_lobby(ncmdid.NID_MATCH_JOIN_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[MatchModule][send_match_join_req] failed: %s", ok and res.code or res)
        return
    end
    log_info("[WatchModule][om_match_join_res] success: %s", serialize(res))
    self:set_match_room_id(res.ticket)
end

-- 退出匹配
function MatchModule:send_match_quit_req()
    local net_req = { ticket = self:get_match_room_id() }
    local ok, res = self:call_lobby(ncmdid.NID_MATCH_QUIT_REQ, net_req)
    if not ok or check_failed(res.code) then
        log_err("[MatchModule][send_match_quit_req] failed: %s", ok and res.code or res)
        return
    end
    log_info("[WatchModule][om_match_quit_res] success: %s", serialize(res))
    self:set_match_room_id(nil)
end

--返回rpc
-------------------------------------------------------------------
function MatchModule:on_match_complete_ntf(res)
    if check_failed(res.code) then
        log_err("[MatchModule][on_match_complete_ntf] failed: %s", res.code)
        return
    end
    log_info("[MatchModule][on_match_complete_ntf] success: %s", serialize(res))
end

function MatchModule:on_enter_battle_ntf(res)
    if check_failed(res.code) then
        log_err("[MatchModule][on_enter_battle_ntf] failed: %s", res.code)
        return
    end
    log_info("[MatchModule][on_enter_battle_ntf] success: %s", serialize(res))
end

return MatchModule
