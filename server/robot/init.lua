--init.lua

import("share/share.lua")
import("constant/game_code.lua")
import("constant/game_const.lua")
import("api/plat/plat_const.lua")
import("api/plat/plat_proto.lua")
import("robot/report_mgr.lua")
import("robot/robot_mgr.lua")