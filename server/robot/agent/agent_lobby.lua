-- lobby_svr_agent.lua
-- @author: errorcpp@qq.com
-- @date:   2019-07-22
local log_err       = logger.err
local log_debug     = logger.debug
local serialize     = logger.serialize
local check_failed  = utility.check_failed

local CSCmdID       = ncmd_cs.NCmdId

local NetClient = import("kernel/network/net_client.lua")
local LobbyAgent = class()
local prop = property(LobbyAgent)
prop:accessor("serial", 0)
prop:accessor("robot", nil)
prop:accessor("client", nil)
function LobbyAgent:__init(robot)
    self.robot = robot
    self.client = NetClient(self)
end

function LobbyAgent:connect(ip, port, block)
    self.client:set_ip(ip)
    self.client:set_port(port)
    return self.client:connect(block)
end

-- 发送心跳
function LobbyAgent:send_heartbeat()
    self.client:call_dx(CSCmdID.NID_HEARTBEAT_REQ, {serial = self.serial})
end

-- 连接成回调
function LobbyAgent:on_socket_connect(client)
    log_debug("[LobbyAgent][on_socket_connect]: %d", self.robot:get_robot_id())
end

local cmd_switch = {
    [CSCmdID.NID_LOGIN_TOKEN_UPDATE_NTF]             = "on_token_update",
    [CSCmdID.NID_KICK_LOBBY_NTF]                     = "on_kick_out_lobby_ntf",
    [CSCmdID.NID_INIT_STATE_SYNC_FINISH_NTF]         = "on_init_state_sync_finish_ntf",
}

-- 数据回调
function LobbyAgent:on_socket_rpc(client, cmd_id, body, session_id)
    if cmd_id == CSCmdID.NID_HEARTBEAT_RES then
        self.serial = body.serial
        return
    end
    local handler = cmd_switch[cmd_id]
    if handler and LobbyAgent[handler] then
        LobbyAgent[handler](self, body)
        return
    end
    self.robot:execute_doer(cmd_id, body)
end

-- 连接关闭回调
function LobbyAgent:on_socket_err(client, err)
    if err ~= "active-close" then
        log_debug("[LobbyAgent][on_socket_err], err:%s", err)
    end
    local robot = self.robot
    robot:set_login_status(false)
    robot:clear_lobby_data()
end

--请求 rpc
--------------------------------------------------------------------------------
-- 请求login
function LobbyAgent:send_login_req()
    local robot = self.robot
    local net_req = {
        open_id             = robot:get_open_id(),
        platform_id         = robot:get_plat_id(),
        area_id             = robot:get_area_id(),
        access_token        = robot:get_access_token(),
        lobby_access_token  = robot:get_lobby_access_token(),
        active_code         = robot:get_active_code(),
    }
    local ok, login_info = robot:call_lobby(CSCmdID.NID_LOGIN_REQ, net_req)
    if not ok or check_failed(login_info.code) then
        log_err("[LobbyAgent][send_login_req] failed: %s", ok and login_info.code or login_info)
        return false
    end
    log_debug("[LobbyAgent][on_login_res]:%s", serialize(login_info))
    robot:set_account_id(login_info.account_id)
    robot:set_platform_url(login_info.platform_url)
    robot:set_platform_token(login_info.platform_token)
    return true
end

-- 请求logout
function LobbyAgent:send_logout_req()
    local robot = self.robot
    local ok, res = robot:call_lobby(CSCmdID.NID_LOGOUT_REQ)
    if not ok or check_failed(res.code) then
        log_err("[LobbyAgent][send_logout_req] failed: %s", ok and res.code or res)
        return
    end
    robot:set_wait_time(500)
    self:close()
end

--返回rpc
-------------------------------------------------------------------
function LobbyAgent:on_kick_out_lobby_ntf(body)
    log_debug("[LobbyAgent][on_kick_out_lobby_ntf]")
end

function LobbyAgent:on_token_update(body)
    log_debug("[LobbyAgent][on_token_update] ntf=%s", serialize(body))
end

function LobbyAgent:on_init_state_sync_finish_ntf(body)
    log_debug("[LobbyAgent][on_init_state_sync_finish_ntf]")
    local robot = self.robot
    robot:set_login_status(true)
    robot:report("login_lobby_success")
end

return LobbyAgent
