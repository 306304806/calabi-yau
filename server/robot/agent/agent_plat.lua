--agent_plat.lua
import("api/plat/plat_const.lua")
import("api/plat/plat_proto.lua")
local log_err       = logger.err
local log_debug     = logger.debug
local log_info      = logger.info
local serialize     = logger.serialize
local check_failed  = utility.check_failed

local PlatCSCmdID   = ncmd_plat.CCmdId

local NetClient = import("kernel/network/net_client.lua")
local PlatAgent = class()
local prop = property(PlatAgent)
prop:accessor("serial", 0)
prop:accessor("robot", nil)
prop:accessor("client", nil)
function PlatAgent:__init(robot)
    self.robot = robot
    self.client = NetClient(self)
end

function PlatAgent:connect(ip, port, block)
    self.client:set_ip(ip)
    self.client:set_port(port)
    return self.client:connect(block)
end

-- 发送心跳
function PlatAgent:send_heartbeat()
    self.client:call_dx(PlatCSCmdID.NID_CHEART_BEAT_REQ, {serial = self.serial})
end

-- 连接成回调
function PlatAgent:on_socket_connect(client)
    log_debug("[PlatAgent][on_socket_connect]: %d", self.robot:get_robot_id())
end

-- 数据回调
function PlatAgent:on_socket_rpc(client, cmd_id, body, session_id)
    if cmd_id == PlatCSCmdID.NID_CHEART_BEAT_RES then
        self.serial = body.serial
        return
    end
    self.robot:execute_doer(cmd_id, body)
end

-- 连接关闭回调
function PlatAgent:on_socket_err(client, err)
    if err ~= "active-close" then
        log_debug("[PlatAgent][on_close_impl], err:%s", err)
    end
    self.robot:set_platform_status(false)
end

-- 登录
function PlatAgent:send_login_platform_req()
    local robot = self.robot
    local plat_data = {
        area_id = robot:get_area_id(),
        player_id = robot:get_player_id(),
        token = robot:get_platform_token()
    }
    local ok, res = robot:call_plat(PlatCSCmdID.NID_CLIENT_LOGIN_REQ, plat_data)
    if not ok or check_failed(res.code) then
        log_err("[PlatAgent][send_login_platform_req] failed: %s", ok and res.code or res)
        return false
    end
    log_info("[PlatAgent][send_login_platform_req] success: %s",  serialize(res))
    robot:set_platform_status(true)
    robot:report("login_plat_success")
    return true
end

return PlatAgent
