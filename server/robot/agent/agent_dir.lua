--agent_dir.lua
local log_err       = logger.err
local log_debug     = logger.debug
local check_failed  = utility.check_failed
local serialize     = logger.serialize

local CSCmdID       = ncmd_cs.NCmdId

local NetClient = import("kernel/network/net_client.lua")
local DirAgent = class()
local prop = property(DirAgent)
prop:accessor("robot", nil)
prop:accessor("client", nil)
function DirAgent:__init(robot)
    self.robot = robot
    self.client = NetClient(self)
end

function DirAgent:connect(ip, port, block)
    self.client:set_ip(ip)
    self.client:set_port(port)
    return self.client:connect(block)
end

-- 连接成回调
function DirAgent:on_socket_connect(client)
    log_debug("[DirAgent][on_socket_connect]: %d", self.robot:get_robot_id())
end

local cmd_switch = {}
-- 数据回调
function DirAgent:on_socket_rpc(client, cmd_id, body, session_id)
    local handler = cmd_switch[cmd_id]
    if handler and DirAgent[handler] then
        DirAgent[handler](self, body)
    end
end

-- 连接关闭回调
function DirAgent:on_socket_err(client, err)
    if err ~= "active-close" then
        log_debug("[DirAgent][on_close_impl], err:%s", err)
    end
end

--请求 rpc
-------------------------------------------------------------------------
-- 请求token
function DirAgent:send_get_token_req(area_id, open_id)
    local net_req = {
        area_id = area_id,
        open_id = open_id
    }
    local robot = self.robot
    local ok, token_info = robot:call_dir(CSCmdID.NID_GET_TOKEN_REQ, net_req)
    if not ok or check_failed(token_info.code) then
        log_err("[DirAgent][send_get_token_req] failed: %s", ok and token_info.code or token_info)
        return
    end
    log_debug("[DirAgent][on_get_token_res]:%s", serialize(token_info))
    robot:set_lobby_access_token(token_info.lobby_access_token)
    robot:set_lobby_info(token_info.lobby_addr)
    robot:report("login_dir_success")
    self.client:close()
end

-- 请求区服列表
function DirAgent:send_get_area_list_req()
    local robot = self.robot
    local area_id = robot:get_area_id()
    if not area_id then
        local req = {device_type = 1}
        local ok, res = robot:call_dir(CSCmdID.NID_GET_AREA_LIST_REQ, req)
        if not ok then
            log_err("[DirAgent][send_get_area_list_req] failed: %s", res)
            return
        end
        area_id = 1
        --area_id = trand_array(res.area_list).area_id
        log_debug("[DirAgent][send_get_area_list_req]:%s", serialize(res))
        robot:set_area_id(area_id)
    end
    self:send_get_token_req(area_id, robot:get_open_id())
end

return DirAgent
