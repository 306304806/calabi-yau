--dsc_mgr.lua
local otime         = os.time
local log_err       = logger.err
local log_info      = logger.info
--local log_debug     = logger.debug
local serialize     = logger.serialize
local tsize         = table_ext.size
local env_get       = environ.get
local tinsert       = table.insert
local tsort         = table.sort

local timer_mgr     = quanta.get("timer_mgr")

local PeriodTime    = enum("PeriodTime")

local DsCenterMgr = singleton()
function DsCenterMgr:__init()
    -- 所有dsagent信息 <id, data>
    self.dsa_datas = {}
    -- dsagent状态信息 <id, alive>
    self.dsa_svr_status = {}
    -- 机器cpu & mem信息<id, info>
    self.dsa_machines = {}
    -- 预分配相关数据<id, data>
    self.pre_alloc_rooms = {}
end

function DsCenterMgr:setup()
    timer_mgr:loop(PeriodTime.SECOND_3_MS, function()
        self:on_timer_check()
    end)
end

--获取dsa信息
function DsCenterMgr:get_dsa_data(id)
    return self.dsa_datas[id]
end

-- 记录dsagent信息
function DsCenterMgr:record_dsa_info(dsagent_id, ds_list)
    self.dsa_datas[dsagent_id] = ds_list or {}
    self:dsa_status_change(dsagent_id, true)

    if self.pre_alloc_rooms[dsagent_id] then
        for _, data in pairs(ds_list) do
            self.pre_alloc_rooms[dsagent_id][data.room_id] = nil
        end
    end
end

-- 寻找合适的Dsagent
function DsCenterMgr:find_dsagent(room_id)
    if tsize(self.dsa_datas) <= 0 then
        log_err("[DsCenterMgr][find_dsagent]->dsagents is empty!")
        return 0
    end

    local is_precise = false
    local var = env_get("QUANTA_PRECSISE_SCHEDULE")
    if var and var > 0 then
        is_precise = true
    end

    if quanta.platform == "windows" then
        is_precise = false
    end

    local best_dsagent_id
    if is_precise then
        best_dsagent_id = self:precise_schedule_dsa()
    else
        best_dsagent_id = self:common_schedule_dsa()
    end

    if best_dsagent_id > 0 then
        if not self.pre_alloc_rooms[best_dsagent_id] then
            self.pre_alloc_rooms[best_dsagent_id] = {}
        end

        self.pre_alloc_rooms[best_dsagent_id][room_id] = otime()
    end

    return best_dsagent_id
end

-- 精确调度
function DsCenterMgr:precise_schedule_dsa()
    -- 首先避免调度到同一台物理机
    -- 其次根据cpu&内存来找最优的
    local sort_list = {}
    for dsagent_id, data in pairs(self.dsa_datas) do
        if self.dsa_svr_status[dsagent_id] then
            local average_info = self.dsa_machines[dsagent_id].average_info
            tinsert(sort_list, {dsa_svr_id = dsagent_id, pre_alloc_ds = tsize(self.pre_alloc_rooms[dsagent_id] or {}), started_ds = tsize(data),
                                avr_cpu = average_info.cpu_percent, avr_free_mem = average_info.free_mem})
        end
    end

    local list_size = #(sort_list)
    if list_size > 0 then
        if list_size > 1 then
            tsort(sort_list, function(left, right)
                if left.pre_alloc_ds ~= right.pre_alloc_ds then
                    return left.pre_alloc_ds < right.pre_alloc_ds
                end

                if left.started_ds ~= right.started_ds then
                    return left.started_ds < right.started_ds
                end

                if left.avr_cpu ~= right.avr_cpu then
                    return left.avr_cpu < right.avr_cpu
                end

                if left.avr_free_mem ~= right.avr_free_mem then
                    return left.avr_free_mem > right.avr_free_mem
                end
            end)
        end

        return list_size[1].dsa_svr_id
    end

    return 0
end

-- 普通调度
function DsCenterMgr:common_schedule_dsa()
    local best_dsagent_id = 0
    local total_ds_cnt = 99999
    for dsagent_id, data in pairs(self.dsa_datas) do
        local size_ds = tsize(data)
        if self.dsa_svr_status[dsagent_id] and total_ds_cnt > size_ds then
            total_ds_cnt = size_ds
            best_dsagent_id = dsagent_id
        end
    end

    return best_dsagent_id
end

-- 记录机器信息
function DsCenterMgr:record_machine_info(data)
    log_info("[DsCenterMgr][record_machine_info]->info:%s", serialize(data))
    -- 检查上报机器数据是有有对应的dsa
    for dsa_svr_id, svr_data in pairs(self.dsa_datas) do
        if svr_data.dsa_host_ip == data.host_ip then
            if not self.dsa_machines[dsa_svr_id] then
                self.dsa_machines[dsa_svr_id] = {report_list = {}, average_info = {}}
            end
            local machine_data = self.dsa_machines[dsa_svr_id]
            -- 只存放最近三分钟的信息
            local cur_time = otime()
            local total_cpu_percent = 0
            local total_free_mem    = 0
            local report_list = machine_data.report_list
            for report_time, details in pairs(report_list) do
                if cur_time - report_time > PeriodTime.MINUTE_MS * 3 then
                    report_list[report_time] = nil
                else
                    total_cpu_percent = total_cpu_percent + details.cpu_use_percent
                    total_free_mem    = total_free_mem + details.free_mem_toal
                end
            end
            report_list[cur_time] = data

            local list_size = tsize(report_list)
            machine_data.average_info.cpu_percent = total_cpu_percent / list_size
            machine_data.average_info.free_mem    = total_free_mem / list_size

            return
        end
    end

    log_err("[DsCenterMgr][record_machine_info]->failed! not find dsa! data:%s", serialize(data))
end

-- dsa服务器状态变化
function DsCenterMgr:dsa_status_change(svr_id, is_alive)
    self.dsa_svr_status[svr_id] = is_alive
end

function DsCenterMgr:on_timer_check()
    local cur_time = otime()
    for dsa_id, data in pairs(self.pre_alloc_ds) do
        for room_id, old_time in pairs(data) do
            if cur_time - old_time >= 10 then
                self.pre_alloc_ds[dsa_id][room_id] = nil
            end
        end
    end
end

quanta.dscenter_mgr = DsCenterMgr()

return DsCenterMgr
