--dsc_servlet.lua

local log_err       = logger.err
local log_info      = logger.info
--local log_debug     = logger.debug

local DsCode        = enum("DsCode")
local KernCode      = enum("KernCode")
local PeriodTime    = enum("PeriodTime")

local event_mgr     = quanta.get("event_mgr")
local dscenter_mgr  = quanta.get("dscenter_mgr")
local router_mgr    = quanta.get("router_mgr")
local thread_mgr    = quanta.get("thread_mgr")

local DscServlet = singleton()
function  DscServlet:__init()
    -- 注册事件
    -- rebuild
    event_mgr:add_listener(self, "rebuild_dsc_svr")
    -- s2s
    event_mgr:add_listener(self, "rpc_report_dsa_info")
    event_mgr:add_listener(self, "rpc_search_ds_agent")
    event_mgr:add_listener(self, "web_report_machine_info")

    -- 关注 dsagent 关闭事件
    router_mgr:watch_service_close(self, "dsagent")
    -- 关注 dsagent 服务器准备事件
    router_mgr:watch_service_ready(self, "dsagent")
end

-- 收到重建Dsc数据rpc
function DscServlet:rebuild_dsc_svr(dsagent_id, ds_list)
    log_info("[DscServlet][rebuild_dsc_svr]->dsagent_id:%s", dsagent_id)
    if not dsagent_id or not ds_list then
        log_err("[DscServlet][rebuild_dsc_svr]->param error!")
        return KernCode.PARAM_ERROR
    end

    dscenter_mgr:record_dsa_info(dsagent_id, ds_list)
    return KernCode.SUCCESS
end

-- 收到dsa上报数据rpc
function DscServlet:rpc_report_dsa_info(dsagent_id, ds_list)
    log_info("[DscServlet][rpc_report_dsa_info]->dsagent_id:%s", dsagent_id)
    if not dsagent_id or not ds_list then
        log_err("[DscServlet][rpc_report_dsa_info]->param is nil")
        return KernCode.PARAM_ERROR
    end

    dscenter_mgr:record_dsa_info(dsagent_id, ds_list)
    return KernCode.SUCCESS
end

-- 收到搜索最佳ds agent请求rpc
function DscServlet:rpc_search_ds_agent(room_id)
    if not room_id then
        log_err("[DscServlet][rpc_search_ds_agent]->failed! param is nil!, room_id:%s", room_id)
        return KernCode.PARAM_ERROR
    end

    local best_dsa_id = dscenter_mgr:find_dsagent(room_id)
    if not best_dsa_id or best_dsa_id <= 0 then
        return DsCode.DS_ERR_FIND_DSA_FAILURE, (best_dsa_id or 0)
    end

    log_info("[DscServlet][rpc_search_ds_agent]->find dsa id:%s, room_id:%s", best_dsa_id, room_id)
    return KernCode.SUCCESS, best_dsa_id
end

-- 上报机器信息
function DscServlet:web_report_machine_info(info)
    dscenter_mgr:record_machine_info(info)
    return KernCode.SUCCESS
end

-- 服务器启动准备事件
function DscServlet:on_service_ready(id, service_name)
    log_info("[DscServlet][on_service_ready]->id:%d, service_name:%s", id, service_name)
    local dsa_data = dscenter_mgr:get_dsa_data(id)
    if not dsa_data or not next(dsa_data) then
        return
    end
    thread_mgr:success_call(PeriodTime.SECOND_MS, function()
        local ok, err = router_mgr:call_target(id, "rebuild_dsa_from_dsc", quanta.id, dsa_data)
        if ok then
            log_info("DscServlet:on_service_ready to dsagent success! dsa_id:%d", id)
            return true
        end
        log_err("DscServlet:on_service_ready to dsagent failed!", err)
        return false
    end)
end

--dsa关闭事件
function DscServlet:on_service_close(id)
    log_info("[DscServlet][on_service_close] dsagent(%d) has lost!", id)
    dscenter_mgr:dsa_status_change(id, false)
end

quanta.dsc_servlet = DscServlet()

return DscServlet
