--team_mgr.lua
local new_guid      = guid.new
local log_info      = logger.info
local tinsert       = table.insert
local random_array  = table_ext.random_array

local Team          = import("team/team.lua")

local event_mgr     = quanta.get("event_mgr")
local router_mgr    = quanta.get("router_mgr")
local config_mgr    = quanta.get("config_mgr")

local map_db        = config_mgr:init_table("mapcfg", "id")

local TeamMgr = singleton()
function TeamMgr:__init()
    self.team_map = {}
    self.players = {}

    --读取地图配置
    local map_ids = {}
    for map_id, item in map_db:iterator() do
        if item.open then
            if not map_ids[item.type] then
                map_ids[item.type] = {}
            end
            tinsert(map_ids[item.type], map_id)
        end
    end
    self.map_ids = map_ids

    --添加事件监听
    event_mgr:add_listener(self, "ensure_player")
    event_mgr:add_listener(self, "unsure_player")
end

function TeamMgr:choose_map(team)
    local mode = team:get_mode()
    return random_array(self.map_ids[mode])
end

function TeamMgr:create_team(member, mode)
    local team_id = new_guid(quanta.index, quanta.service_id)
    local team = Team(team_id, member, mode)
    self.team_map[team_id] = team
    log_info("new team at:%s", team_id)
    return team_id
end

function TeamMgr:ensure_player(player_id, team_id)
    self.players[player_id] = team_id
    router_mgr:call_index_hash(player_id, "rpc_enter_team", player_id, team_id, quanta.id)
end

function TeamMgr:unsure_player(player_id, team_id)
    self.players[player_id] = nil
    router_mgr:call_index_hash(player_id, "rpc_quit_team", player_id, team_id, quanta.id)
end

function TeamMgr:find_player_team(player_id)
    local team_id = self.players[player_id]
    if team_id then
        return self.team_map[team_id]
    end
end

function TeamMgr:dismiss(id)
    local team = self.team_map[id]
    if team then
        team:dismiss()
        self.team_map[id] = nil
        log_info("team: %s dismiss", id)
    end
end

function TeamMgr:get_team(id)
    return self.team_map[id]
end

quanta.team_mgr = TeamMgr()

return TeamMgr
