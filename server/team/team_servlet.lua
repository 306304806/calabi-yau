--team_servlet.lua
local guid_index    = guid.index
local log_info      = logger.info
local log_debug     = logger.debug
local serialize     = logger.serialize
local smake_id      = service.make_id
local check_success = utility.check_success

local KernCode      = enum("KernCode")
local TeamCode      = enum("TeamCode")
local PlayerCode    = enum("PlayerCode")
local TeamStatus    = enum("TeamStatus")

local CSCmdID       = ncmd_cs.NCmdId
local SUCCESS       = KernCode.SUCCESS

local team_mgr      = quanta.get("team_mgr")
local event_mgr     = quanta.get("event_mgr")
local router_mgr    = quanta.get("router_mgr")

local TeamServlet = singleton()
function TeamServlet:__init()
    --注册事件
    event_mgr:add_listener(self, "rpc_create_team")
    event_mgr:add_listener(self, "rpc_reload_team")
    event_mgr:add_listener(self, "rpc_team_ready")
    event_mgr:add_listener(self, "rpc_team_mode")
    event_mgr:add_listener(self, "rpc_team_batch_ready")
    event_mgr:add_listener(self, "rpc_enter_team")
    event_mgr:add_listener(self, "rpc_apply_team")
    event_mgr:add_listener(self, "rpc_reply_apply_team")
    event_mgr:add_listener(self, "rpc_exit_team")
    event_mgr:add_listener(self, "rpc_kick_member")
    event_mgr:add_listener(self, "rpc_trans_leader")
    event_mgr:add_listener(self, "rpc_get_team_data")
    event_mgr:add_listener(self, "rpc_query_team_info")
end

function TeamServlet:check_member(team_id, player_id)
    local team = team_mgr:get_team(team_id)
    if not team then
        return TeamCode.TEAM_ERR_NOT_EXIST
    end
    local member = team:get_member(player_id)
    if not member then
        return TeamCode.TEAM_ERR_NOT_IN_TEAM
    end
    return SUCCESS, team, member
end

function TeamServlet:handle_exit_team(team_id, player_id)
    local code, team, member = self:check_member(team_id, player_id)
    if check_success(code) then
        if team:get_size() == 1 then
            team_mgr:dismiss(team_id)
        else
            team:leave(member)
        end
    end
    return code
end

--rpc
-----------------------------------------------------------------
function TeamServlet:rpc_query_team_info(team_id)
    log_debug("[TeamServlet][rpc_query_team_info] team_id:%s", team_id)
    local team = team_mgr:get_team(team_id)
    if not team then
        return TeamCode.TEAM_ERR_NOT_EXIST
    end
    return SUCCESS, team:serialize()
end

function TeamServlet:rpc_get_team_data(team_id, player_id)
    log_debug("[TeamServlet][rpc_get_team_data] team_id:%s", team_id)
    local code, team = self:check_member(team_id, player_id)
    if not check_success(code) then
        return code
    end
    team:ready(team:get_leader_id(), TeamStatus.TEAM_READY)
    if not team:is_all_ready() then
        team:board2client(CSCmdID.NID_MATCH_JOIN_NTF, { code = TeamCode.TEAM_ERR_NOT_ALL_READY })
        return TeamCode.TEAM_ERR_NOT_ALL_READY
    end
    team:set_map_id(team_mgr:choose_map(team))
    return SUCCESS, team:pack_team()
end

function TeamServlet:rpc_create_team(member, mode)
    log_debug("[TeamServlet][create_team] member:%s, mode:%s", member.player_id, mode)
    if team_mgr:find_player_team(member.player_id) then
        return PlayerCode.PLAYER_ERR_AREADY_IN_TEAM
    end
    if team_mgr:create_team(member, mode) then
        return SUCCESS
    end
    return TeamCode.TEAM_ERR_MAPINFO_EMPTY
end

function TeamServlet:rpc_reload_team(team_id, player_id)
    log_debug("[TeamServlet][rpc_reload_team] team_id:%s, player_id:%s", team_id, player_id)
    local code, team = self:check_member(team_id, player_id)
    if not check_success(code) then
        return code
    end
    team:ready(player_id, TeamStatus.TEAM_READY)
    return SUCCESS, team:reload(player_id)
end

function TeamServlet:rpc_team_ready(team_id, player_id, status)
    log_debug("[TeamServlet][rpc_team_ready] team_id:%s, player_id:%s, status:%s", team_id, player_id, status)
    local code, team = self:check_member(team_id, player_id)
    if not check_success(code) then
        return code
    end
    team:ready(player_id, status)
    return SUCCESS
end

function TeamServlet:rpc_team_mode(team_id, player_id, mode)
    log_debug("[TeamServlet][rpc_team_mode] team_id:%s, player_id:%s, mode:%s", team_id, player_id, mode)
    local code, team = self:check_member(team_id, player_id)
    if not check_success(code) then
        return code
    end
    if not team:is_leader(player_id) then
        return TeamCode.TEAM_ERR_PERMISSION_DENIED
    end
    team:change_mode(mode)
    return SUCCESS
end

function TeamServlet:rpc_team_batch_ready(team_id, readys)
    log_debug("[TeamServlet][rpc_team_batch_ready] team_id:%s, readys:%s", team_id, serialize(readys))
    local team = team_mgr:get_team(team_id)
    if not team then
        return TeamCode.TEAM_ERR_NOT_EXIST
    end
    team:batch_ready(readys)
    return SUCCESS
end

function TeamServlet:rpc_apply_team(team_id, player_id, member)
    log_debug("[TeamServlet][rpc_apply_team] team_id:%s, player_id:%s", team_id, player_id)
    local team = team_mgr:get_team(team_id)
    if not team then
        return TeamCode.TEAM_ERR_NOT_EXIST
    end
    team:add_apply(player_id, member)
    local apply_ntf = { player_id = player_id, icon = member.icon, nick = member.nick }
    team:send2client(team:get_leader_id(), CSCmdID.NID_TEAM_APPLY_NTF, apply_ntf)
    return SUCCESS
end

function TeamServlet:rpc_reply_apply_team(team_id, player_id, target_id, old_teamid, sure)
    log_debug("[TeamServlet][rpc_reply_apply_team] team_id:%s, player:%s, target:%s, old_teamid:%s, sure:%s", team_id, player_id, target_id, old_teamid, sure)
    local code, team, leader = self:check_member(team_id, player_id)
    if not check_success(code) then
        return code
    end
    local apply_reply_ntf = { sure = sure, player_id = player_id, icon = leader.icon, nick = leader.nick }
    team:send2client(target_id, CSCmdID.NID_TEAM_APPLY_REPLY_NTF, apply_reply_ntf)
    if sure == 1 then
        local member = team:reply_apply(target_id)
        if not member then
            return TeamCode.TEAM_ERR_APPLY_TIMEOUT
        end
        if old_teamid and member.team_id ~= old_teamid then
            return TeamCode.TEAM_ERR_TAR_IN_OTHER_TEAM
        end
        return self:rpc_enter_team(team_id, player_id, member, old_teamid, sure)
    end
    return SUCCESS
end

function TeamServlet:rpc_enter_team(team_id, invite_id, member, old_team_id, sure)
    local player_id = member.player_id
    log_debug("[TeamServlet][rpc_enter_team] team_id:%s, invite_id:%s, player_id:%s, old_team_id:%s sure:%s", team_id, invite_id, player_id, old_team_id, sure)
    local team = team_mgr:get_team(team_id)
    if not team then
        return TeamCode.TEAM_ERR_NOT_EXIST
    end
    if team:full() then
        return TeamCode.TEAM_ERR_NOT_EMPTY
    end
    if not team:get_member(invite_id) then
        return TeamCode.TEAM_ERR_INVITE_TIMEOUT
    end
    if team:get_member(player_id) then
        return TeamCode.TEAM_ERR_IN_THIS_TEAM
    end
    if old_team_id then
        local old_team_idx = guid_index(old_team_id)
        if old_team_idx == quanta.index then
            local code = self:handle_exit_team(old_team_id, player_id)
            if not check_success(code) then
                return code
            end
        else
            local team_sid = smake_id("team", old_team_idx)
            local ok, code = router_mgr:call_target(team_sid, "rpc_exit_team", old_team_id, player_id)
            if not ok or not check_success(code) then
                return code
            end
        end
    end
    team:enter(member, (sure == TeamStatus.TEAM_YIELD) and sure or 0)
    return SUCCESS
end

function TeamServlet:rpc_kick_member(team_id, player_id, kick_id)
    log_info("[TeamServlet][rpc_kick_member] team_id: %s, player_id:%s, kick_id:%s", team_id, player_id, kick_id)
    local code, team = self:check_member(team_id, player_id)
    if not check_success(code) then
        return code
    end
    if not team:is_leader(player_id) then
        return TeamCode.TEAM_ERR_PERMISSION_DENIED
    end
    if player_id == kick_id then
        return KernCode.OPERATOR_SELF
    end
    local target = team:get_member(kick_id)
    if not target then
        return TeamCode.TEAM_ERR_TAR_NOT_IN_TEAM
    end
    team:kick(target)
    return SUCCESS
end

function TeamServlet:rpc_exit_team(team_id, player_id)
    log_info("[TeamServlet][rpc_exit_team] team_id: %s, player_id:%s", team_id, player_id)
    local code = self:handle_exit_team(team_id, player_id)
    if code == TeamCode.TEAM_ERR_NOT_EXIST then
        return SUCCESS
    end
    return code
end

function TeamServlet:rpc_trans_leader(team_id, player_id, tar_player_id)
    log_info("[TeamServlet][rpc_trans_leader]: team_id:%s, player_id:%s, tar_player_id:%s", team_id, player_id, tar_player_id)
    local code, team = self:check_member(team_id, player_id)
    if not check_success(code) then
        return code
    end
    if not team:is_leader(player_id) then
        return TeamCode.TEAM_ERR_PERMISSION_DENIED
    end
    if not team:get_member(tar_player_id) then
        return TeamCode.TEAM_ERR_TAR_NOT_IN_TEAM
    end
    team:trans_leader(tar_player_id)
    return SUCCESS
end

quanta.team_servlet = TeamServlet()

return TeamServlet
