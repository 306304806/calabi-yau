--team.lua
import("api/plat/plat_api.lua")
local pairs         = pairs
local tinsert       = table.insert
local guid_index    = guid.index
local log_warn      = logger.warn
local log_info      = logger.info
local check_failed  = utility.check_failed

local ChatType      = enum("ChatType")
local GameConst     = enum("GameConst")
local TeamStatus    = enum("TeamStatus")
local PeriodTime    = enum("PeriodTime")

local CSCmdID       = ncmd_cs.NCmdId

local event_mgr     = quanta.get("event_mgr")
local router_mgr    = quanta.get("router_mgr")
local thread_mgr    = quanta.get("thread_mgr")

local plat_api      = quanta.get("plat_api")

local Team = class()
local prop = property(Team)
prop:accessor("size", 0)
prop:accessor("mode", 0)
prop:accessor("map_id", 0)
prop:accessor("team_id", nil)
prop:accessor("leader_id", nil)
prop:accessor("players", {})
prop:accessor("indexs", {})
prop:accessor("applys", {})
prop:accessor("chat_group_id", nil)

function Team:__init(team_id, member, mode)
    self.mode       = mode
    self.team_id    = team_id
    self.leader_id  = member.player_id
    self:enter(member, TeamStatus.TEAM_READY)
    self:create_chat_group()
end

function Team:empty()
    return self.size == 0
end

function Team:full()
    return self.size == GameConst.TEAM_SIZE
end

function Team:add_apply(player_id, apply)
    self.applys[player_id] = apply
end

function Team:reply_apply(player_id)
    local apply = self.applys[player_id]
    self.applys[player_id] = nil
    return apply
end

function Team:send2client(player_id, ...)
    router_mgr:send_index_hash(player_id, "transfer_message", player_id, "forward_player", player_id, ...)
end

function Team:board2client(...)
    for player_id in pairs(self.players) do
        router_mgr:send_index_hash(player_id, "transfer_message", player_id, "forward_player", player_id, ...)
    end
end

function Team:is_all_ready()
    for _, member in pairs(self.players) do
        if member.status ~= TeamStatus.TEAM_READY then
            return false
        end
    end
    return true
end

function Team:ready(player_id, status)
    local member = self.players[player_id]
    if member.status ~= status then
        member.status = status
        self:board2client(CSCmdID.NID_TEAM_READY_NTF, {readys = {{player_id = player_id, status = status}}})
    end
end

function Team:batch_ready(readys)
    local real_readys = {}
    for _, ready in pairs(readys) do
        local member = self.players[ready.player_id]
        if member.status ~= ready.status then
            member.status = ready.status
            tinsert(real_readys, ready)
        end
    end

    if #real_readys > 0 then
        self:board2client(CSCmdID.NID_TEAM_READY_NTF, {readys = real_readys})
    end
end

function Team:change_mode(mode)
    self.mode = mode
    self:board2client(CSCmdID.NID_TEAM_MODE_NTF, {team_id = self.team_id, mode = mode })
end

function Team:get_member(player_id)
    return self.players[player_id]
end

function Team:find_pos()
    for i = 1, GameConst.TEAM_SIZE do
        if not self.indexs[i] then
            return i
        end
    end
end

function Team:dismiss()
    self:destory_chat_group()
    for player_id in pairs(self.players) do
        event_mgr:notify_listener("unsure_player", player_id, self.team_id)
        self:send2client(player_id, CSCmdID.NID_TEAM_EXIT_NTF, { player_id = player_id })
    end
end

function Team:enter(member, status)
    member.status   = status
    member.team_id  = self.team_id
    member.pos = self:find_pos()
    if self.size > 0 then
        self:board2client(CSCmdID.NID_TEAM_ENTER_NTF, { member = self:pack_member(member) })
    end
    self.size = self.size + 1
    local player_id = member.player_id
    self.players[player_id] = member
    self.indexs[member.pos] = member
    event_mgr:notify_listener("ensure_player", player_id, self.team_id)
    self:send2client(player_id, CSCmdID.NID_TEAM_INFO_NTF, self:serialize())
    self:add_chat_group_member(member)
end

function Team:trans_leader(leader_id)
    self.leader_id = leader_id
    self:board2client(CSCmdID.NID_TEAM_TRANS_LEADER_NTF, { team_id = self.team_id, leader_id = leader_id })
    self:ready(leader_id, TeamStatus.TEAM_READY)
end

function Team:kick(member)
    local player_id = member.player_id
    self:board2client(CSCmdID.NID_TEAM_KICK_NTF, {player_id = player_id})
    event_mgr:notify_listener("unsure_player", player_id, self.team_id)
    self.size = self.size - 1
    self.players[player_id] = nil
    self.indexs[member.pos] = nil
    self:del_chat_group_member(player_id)
end

function Team:leave(member)
    local player_id = member.player_id
    self:board2client(CSCmdID.NID_TEAM_EXIT_NTF, { player_id = player_id })
    event_mgr:notify_listener("unsure_player", player_id, self.team_id)
    self.size = self.size - 1
    self.players[player_id] = nil
    self.indexs[member.pos] = nil

    if player_id == self.leader_id then
        for pla_id in pairs(self.players) do
            self:trans_leader(pla_id)
            break
        end
    end
    self:del_chat_group_member(player_id)
end

function Team:reload(player_id)
    self:send2client(player_id, CSCmdID.NID_TEAM_INFO_NTF, self:serialize())
    return self.team_id
end

function Team:is_leader(player_id)
    return self.leader_id == player_id
end

-- 获取成员的id列表
function Team:get_member_ids()
    local ids = {}
    for player_id in pairs(self.players) do
        tinsert(ids, player_id)
    end

    return ids
end

function Team:pack_member(player)
    return {
        pos         = player.pos,
        status      = player.status,
        nick        = player.nick,
        icon        = player.icon,
        sex         = player.sex,
        level       = player.level,
        rank        = player.rank,
        player_id   = player.player_id,
        vc_avatar_id= player.vc_avatar_id,
        vc_frame_id = player.vc_frame_id,
        vc_border_id= player.vc_border_id,
        vc_achie_id = player.vc_achie_id,
    }
end

function Team:pack_team()
    local data = {
        members     = {},
        mode        = self.mode,
        map_id      = self.map_id,
    }
    for _, player in pairs(self.players) do
        tinsert(data.members, player)
    end
    return data
end

function Team:pack_chat_member(player)
    return {
        team_id     = 0,
        nick        = player.nick,
        icon        = player.icon,
        player_id   = player.player_id,
    }
end

function Team:serialize()
    local data = {
        members     = {},
        mode        = self.mode,
        team_id     = self.team_id,
        leader_id   = self.leader_id,
    }
    for _, player in pairs(self.players) do
        tinsert(data.members, self:pack_member(player))
    end
    return data
end

------------------------------------------------
function Team:create_chat_group()
    local members = {}
    for _, player in pairs(self.players) do
        tinsert(members, self:pack_chat_member(player))
    end
    local area_id = guid_index(self.leader_id)
    local rpc_req = {group_type = ChatType.TEAM, members = members }
    thread_mgr:success_call(PeriodTime.HALF_MS, function()
        local code, rpc_res = plat_api:create_chat_group(area_id, rpc_req)
        if check_failed(code) then
            log_warn("[Team][create_chat_group] plat call faild: code=%s, team_id=%s", code, self.team_id)
            return false
        end
        local group_id = rpc_res.group_id
        self:set_chat_group_id(group_id)
        log_info("[Team][create_chat_group] team_id=%s, group_id=%s", self.team_id, group_id)
        return true
    end)
end

function Team:destory_chat_group()
    local group_id = self:get_chat_group_id()
    if not group_id then
        log_warn("[Team][destory_chat_group] no group_id find !")
        return
    end
    local area_id = guid_index(group_id)
    local rpc_req = { group_id = group_id}
    thread_mgr:success_call(PeriodTime.HALF_MS, function()
        local code = plat_api:destory_chat_group(area_id, rpc_req)
        if check_failed(code) then
            log_warn("[Team][destory_chat_group] plat call faild: code=%s,group_id=%s", code, group_id)
            return false
        end
        log_info("[Team][destory_chat_group] team_id=%s, group_id=%s", self.team_id, group_id)
        return true
    end)
end

function Team:add_chat_group_member(player)
    local group_id = self:get_chat_group_id()
    if not group_id then
        return
    end
    local area_id   = guid_index(group_id)
    local member = self:pack_chat_member(player)
    local rpc_req = {area_id = area_id, group_id = group_id, member = member}
    thread_mgr:success_call(PeriodTime.HALF_MS, function()
        local code = plat_api:add_chat_group_member(area_id, rpc_req)
        if check_failed(code) then
            log_warn("[Team][add_chat_group_member] plat call faild: code=%s,team_id=%s,player_id=%s", code, self.team_id, player.player_id)
            return false
        end
        log_info("[Team][add_chat_group_member] team_id=%s, group_id=%s, player_id=%s", self.team_id, group_id, member.player_id)
        return true
    end)
end

function Team:del_chat_group_member(player_id)
    local group_id = self:get_chat_group_id()
    if not group_id then
        log_warn("[Team][destory_chat_group] no group_id find !")
        return
    end
    local area_id = guid_index(group_id)
    local rpc_req = {area_id = area_id, group_id = group_id, player_id = player_id}
    thread_mgr:success_call(PeriodTime.HALF_MS, function()
        local code = plat_api:del_chat_group_member(area_id, rpc_req)
        if check_failed(code) then
            log_warn("[Team][del_chat_group_member] plat call faild: code=%s,team_id=%s,player_id=%s", code, self.team_id, player_id)
            return false
        end
        log_info("[Team][del_chat_group_member] team_id=%s, group_id=%s, player_id=%s", self.team_id, group_id, player_id)
        return true
    end)
end

return Team
