--game_const.lua

-- 游戏玩法
local GameMode = enum("GameMode", 0)
GameMode.TEAM                       = 1    -- 团竟
GameMode.BOMB                       = 2    -- 爆破
GameMode.ROOM                       = 3    -- 自定义房间

local SexType = enum("SexType", 0)
SexType.MAN                         = 0    -- 男
SexType.WOMAN                       = 1    -- 女

--RMSG类型
local RmsgGame = enum("RmsgGame", 0)
RmsgGame.BATTLE_SETTLE              = 1     -- 结算数据RMSG
RmsgGame.DROP_STAR                  = 2     -- 最强王者长时间未排位掉星
RmsgGame.STANDINGS                  = 3     -- 战绩

--通用常量
local GameConst = enum("GameConst", 0)
GameConst.SEARCH_PAGE               = 20    --房间单页搜索
GameConst.TEAM_SIZE                 = 5     --队伍人数
GameConst.OVERLAY_MAX               = 999   --物品格子数量上限
GameConst.RATE_BASE                 = 10000 --比例分母（万分比）
GameConst.MATCH_WAIT_TIME           = 8     --匹配进入战斗等待时间（s：5+3）
GameConst.ROOM_WAIT_TIME            = 5     --房间进入战斗等待时间（s：5）
GameConst.MAX_MAIL_COUNT            = 20    --配置的邮件最大值


--房间可见性
local RoomVisable = enum("RoomVisable", 0)
RoomVisable.ALL                     = 0     -- 所有人可见
RoomVisable.FRIEND                  = 1     -- 好友可见
RoomVisable.NONE                    = 2     -- 不可见
RoomVisable.FIGHT                   = 3     -- 战斗中

--段位星数
local DivisionStars = enum("DivisionStars", 0)
DivisionStars.STRONGEST             = 124   -- 最强王者

-- 队伍状态
local TeamStatus = enum("TeamStatus", 0)
TeamStatus.TEAM_NONE                = 0
TeamStatus.TEAM_READY               = 1
TeamStatus.TEAM_YIELD               = 2
TeamStatus.TEAM_OFFLINE             = 3
TeamStatus.TEAM_SETTLE              = 4

--房间准备状态
local ReadyStatus = enum("ReadyStatus", 0)
ReadyStatus.NONE                    = 0     --未准备
ReadyStatus.READY                   = 1     --准备
ReadyStatus.OFFLINE                 = 2     --掉线中
ReadyStatus.SETTLE                  = 3     --结算中

--房间类型
local RoomType = enum("RoomType", 0)
RoomType.MATCH                      = 1     -- 匹配房间
RoomType.FIGHT                      = 2     -- 战斗房间
RoomType.CONTEST                    = 3     -- 组队房间

--房间成员玩家状态
local MemberStatus = enum("MemberStatus", 0)
MemberStatus.READY                  = 0    -- 准备状态
MemberStatus.WAIT_ENTER             = 1    -- 等待进入DS
MemberStatus.ENTERD                 = 2    -- 成功进入DS
MemberStatus.OFFLINE                = 3    -- 离线
MemberStatus.EXIT                   = 4    -- 主动退出

--效果触发类型
local TriggerType = enum("TriggerType", 0)
TriggerType.START                   = 1     --添加触发
TriggerType.ACTIVE                  = 2     --激活触发
TriggerType.PERIOD                  = 3     --周期触发
TriggerType.STOP                    = 4     --结束触发

--buff叠加类型
local OverlapType = enum("OverlapType", 0)
OverlapType.NONE                    = 1     --放弃
OverlapType.COVER                   = 2     --覆盖
OverlapType.SHARE                   = 3     --共存
OverlapType.TIME                    = 4     --时间叠加

--behavior更新类型
local BehaviorOperationType = enum("BehaviorOperationType", 0)
BehaviorOperationType.NONE                      = 0   -- 无操作
BehaviorOperationType.INCREASE                  = 1     -- +
BehaviorOperationType.DECREASE                  = 2     -- -
BehaviorOperationType.ASSIGN                    = 3     -- =
BehaviorOperationType.GREATER_OR_EQUAL_ASSIGN   = 4     -- If >= then =
BehaviorOperationType.LESS_OR_EQUAL_ASSIGN      = 5     -- if <= then =
BehaviorOperationType.GREATER_OR_EQUAL_INCREASE = 6     -- if >= then self increase

-- behavior比较类型(完成)
local BehaviorCompareType = enum("BehaviorCompareType", 0)
BehaviorCompareType.GREATER_OR_EQUAL = 1     -- >=
BehaviorCompareType.LESS_OR_EQUAL    = 2     -- <=
BehaviorCompareType.EQUAL            = 3     -- =
BehaviorCompareType.GREATER          = 4     -- >
BehaviorCompareType.LESS             = 5     -- <


-- 特殊行动任务类型
local BattlePassTaskType = enum("BattlePassTaskType", 0)
BattlePassTaskType.DAY  = 1
BattlePassTaskType.WEEK = 2
BattlePassTaskType.LOOP = 3

-- 特殊行动任务状态
local BattlePassTaskState = enum("BattlePassTaskState", 0)
BattlePassTaskState.WAITING     = 1  -- 等待中(未开始，不用统计数据)
BattlePassTaskState.PROGRESSING = 2  -- 进行中
BattlePassTaskState.TIME_OUT    = 3  -- 超时
BattlePassTaskState.FINISH      = 4  -- 完成
BattlePassTaskState.PRIZE_TAKEN = 5  -- 已领奖
