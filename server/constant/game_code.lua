--plat_code.lua

--Player错误码
local PlayerCode = enum("PlayerCode", 0)
PlayerCode.TARGET_ERR_OFFLINE               = 1000  --目标不在线
PlayerCode.PLAYER_ERR_AREADY_IN_MATCH       = 1001  --已经在匹配中
PlayerCode.PLAYER_ERR_AREADY_IN_FIGHT       = 1002  --已经在战斗中
PlayerCode.PLAYER_ERR_AREADY_IN_ROOM        = 1003  --已经在房间中
PlayerCode.PLAYER_ERR_AREADY_IN_TEAM        = 1004  --已经在队伍中
PlayerCode.TARGET_ERR_AREADY_IN_MATCH       = 1005  --目标已经在匹配中
PlayerCode.TARGET_ERR_AREADY_IN_FIGHT       = 1006  --目标已经在战斗中
PlayerCode.TARGET_ERR_AREADY_IN_ROOM        = 1007  --目标已经在房间中
PlayerCode.TARGET_ERR_AREADY_IN_TEAM        = 1008  --目标已经在队伍中

--匹配/房间管理
local RoomCode = enum("RoomCode", 0)
RoomCode.ROOM_ERR_NOT_IN_TEAM               = 1051  --请先组队
RoomCode.ROOM_ERR_MEMBER_FULL               = 1052  --房间已满
RoomCode.ROOM_ERR_NOT_EXIST                 = 1053  --房间不存在
RoomCode.ROOM_ERR_NOT_MEMBER                = 1054  --不是房间成员
RoomCode.ROOM_ERR_IN_FIGHT                  = 1055  --房间已经在战斗中
RoomCode.ROOM_ERR_NOT_MASTER                = 1056  --不是房主
RoomCode.ROOM_ERR_TAR_NOT_MEMBER            = 1057  --目标不是房间成员
RoomCode.ROOM_ERR_INVITE_TIMEOUT            = 1058  --房间邀请过期
RoomCode.ROOM_ERR_PASSWD_WRONG              = 1059  --房间密码错误
RoomCode.ROOM_ERR_MAP_UPDATE                = 1060  --地图更新中断匹配
RoomCode.ROOM_ERR_POS_WRONG                 = 1061  --目标已经不在该位置
RoomCode.ROOM_ERR_IN_ROOM                   = 1062  --已经在该房间
RoomCode.ROOM_ERR_NOT_ALL_READY             = 1063  --还有人没有准备
RoomCode.ROOM_ERR_POS_OUT_RANGE             = 1065  --位置越界
RoomCode.ROOM_ERR_SERIAL_ID                 = 1066  --序列号生成失败
RoomCode.ROOM_ERR_ROBOT_FAILED              = 1068  --创建机器人失败
RoomCode.ROOM_ERR_ROBOT_LEADER              = 1069  --不能转让给机器人
RoomCode.ROOM_ERR_ROBOT_BAN                 = 1070  --此关卡无法添加AI
RoomCode.ROOM_ERR_ROBOT_FULL                = 1071  --无法添加更多AI
RoomCode.ROOM_ERR_NO_IN_FIGHT               = 1072  --不在战斗中
RoomCode.ROOM_ERR_INFO_CHANGE               = 1073  --房间信息变更

--DS相关
local DsCode = enum("DsCode", 0)
DsCode.DS_ERR_CREATE_OVERTIME               = 2001  --创建超时
DsCode.DS_ERR_LIFE_EXPIRED                  = 2002  --DS过期
DsCode.DS_ERR_FIND_DSA_FAILURE              = 2003  --查找DsAgent失败
DsCode.DS_ERR_IMAGE_NOT_EXIST               = 2004  --DS镜像不存在
DsCode.DS_ERR_CREATE_FAILURE                = 2005  --拉起ds失败
DsCode.DS_ERR_REPEATE_CREATE                = 2006  --重复创建DS

--Dir错误码
local DirCode = enum("DirCode", 0)
DirCode.DIR_NO_LOBBY                        = 3001      --无法找到lobby
DirCode.DIR_NO_PLATFORM                     = 3002      --无法找到PLATFORM

--lobby相关
local LobbyCode = enum("LobbyCode", 0)
LobbyCode.LOBBY_TOKEN_ERROR                 = 3051  --token错误（重连token）
LobbyCode.LOBBY_ACCOUNT_ID_NOT_EXIST        = 3052  --账号不存在
LobbyCode.LOBBY_GEN_ACCOUNT_ID_FAILD        = 3053  --生成账号失败
LobbyCode.LOBBY_LOGIN_CONFLICT              = 3054  --登录冲突，上一次登录尚未完成
LobbyCode.LOBBY_ACCOUNT_NOT_EXIST           = 3055  --账号不存在
LobbyCode.LOBBY_PWD_NOT_MATCH               = 3056  --密码错误
LobbyCode.LOBBY_ACCESS_TOKEN_ERROR          = 3057  --访问令牌错误
LobbyCode.LOBBY_NEED_RELOGIN                = 3058  --需要重新登录
LobbyCode.LOBBY_ACC_IN_BLACK_LIST           = 3059  --账号被加入黑名单
LobbyCode.LOBBY_ACC_IS_DISABLE              = 3060  --账号被禁用
LobbyCode.LOBBY_ACC_NEED_ACTIVE             = 3061  --账号未激活
LobbyCode.LOBBY_ACC_ACTIVE_CODE_NOT_EXIST   = 3062  --账号激活码不存在
LobbyCode.LOBBY_ACC_ACTIVE_CODE_IS_USED     = 3063  --激活码已经被使用
LobbyCode.LOBBY_PLAYER_NOT_EXIST            = 3064  --player不存在
LobbyCode.LOBBY_PLAYER_INFO_NOT_EXIST       = 3065  --player信息不存在
LobbyCode.LOBBY_OP_TOO_OFTEN                = 3066  --操作台频繁(有无法并发的操作尚未完成)
LobbyCode.LOBBY_NEED_LOGON                  = 3067  --需要先登录
LobbyCode.LOBBY_UNKNOWN_ACC_PLAT_ID         = 3068  --不支持的平台ID
LobbyCode.LOBBY_RECONN_TOKEN_ERR            = 3069  --重连token错误
--LobbyCode.LOBBY_NEED_RELOGON                = 3070  --需要重新登录
LobbyCode.LOBBY_SESSION_ALREADY_LOGIN       = 3071  --当前会话已经完成登录

-- 角色系统
local RoleCode = enum("RoleCode", 0)
RoleCode.PLAYER_EXIST                       = 3100  -- 已经存在(可修改无法创建)
RoleCode.PLAYER_REFUSE                      = 3101  -- 操作拒绝(部分字段比如昵称，不允许修改)
RoleCode.PLAYER_NEED_CREATE                 = 3102  -- 需要先创建才能做其他操作
RoleCode.ROLE_NOT_EXIST                     = 3103  -- 角色不存在
RoleCode.ROLE_CFG_NOT_EXIST                 = 3104  -- 角色配置不存在
RoleCode.ROLE_SKIN_NOT_EXIST                = 3105  -- 角色皮肤不存在
RoleCode.ROLE_SKIN_NOT_MATCH                = 3106  -- 角色皮肤不匹配
RoleCode.ROLE_NOT_OWNED                     = 3107  -- 为拥有角色

--Team错误码
local TeamCode = enum("TeamCode", 0)
TeamCode.TEAM_ERR_NOT_EXIST                 = 4001  -- 队伍不存在
TeamCode.TEAM_ERR_INVITE_TIMEOUT            = 4002  -- 队伍邀请过期
TeamCode.TEAM_ERR_PERMISSION_DENIED         = 4003  -- 不是队长
TeamCode.TEAM_ERR_IN_THIS_TEAM              = 4005  -- 目标已经在此队伍
TeamCode.TEAM_ERR_NOT_IN_TEAM               = 4006  -- 自己不在队伍中
TeamCode.TEAM_ERR_TAR_NOT_IN_TEAM           = 4007  -- 目标不在队伍中
TeamCode.TEAM_ERR_NOT_EMPTY                 = 4008  -- 队伍已满
TeamCode.TEAM_ERR_NOT_ALL_READY             = 4009  -- 队伍还有人未准备
TeamCode.TEAM_ERR_TAR_IN_OTHER_TEAM         = 4010  -- 目标已经进入其他队伍
TeamCode.TEAM_ERR_APPLY_TIMEOUT             = 4011  -- 申请过期
TeamCode.TEAM_ERR_MAPINFO_EMPTY             = 4012  -- 地图配置不存在
TeamCode.TEAM_ERR_SOCIAL_SECRET             = 4013  -- 队伍隐私限制

--观战相关
local WatchCode = enum("WatchCode", 0)
WatchCode.WATCH_ERR_PLAYER_OFFLINE          = 5001  --被观战对象不在线
WatchCode.WATCH_ERR_ROOM_NOT_EXIST          = 5002  --被观战房间不存在
WatchCode.WATCH_ERR_AREADY_IN_WATCH         = 5003  --已观战状态
WatchCode.WATCH_ERR_ROOM_NOT_RUNNING        = 5004  --未在战斗中

-- 背包相关
local BagCode = enum("BagCode", 0)
BagCode.BAG_ERR_ITEM_NOT_EXIST              = 5051 -- 物品不存在
BagCode.BAG_ERR_NUM_NOT_ENOUGH              = 5052 -- 物品数量不足
BagCode.BAG_ERR_EFFECT_NOT_EXIST            = 5053 -- 物品使用效果创建失败
BagCode.BAG_ERR_REDUCE_ITEM_FAILED          = 5054 -- 消耗物品失败
BagCode.BAG_ERR_CFG_NOT_EXIST               = 5055 -- 物品配置不存在
BagCode.BAG_ERR_MOD_NAME_FREQUENT           = 5056 -- 改名频繁
BagCode.BAG_ERR_ITEM_NOT_SELL               = 5057 -- 物品不能出售
BagCode.BAG_ERR_SELL_HANLE_FAIL             = 5058 -- 出售加钱失败

-- 效果相关
local EffectCode = enum("EffectCode", 0)
EffectCode.EFFECT_ERR_CFG_NOT_EXIST         = 5101 -- 效果配置不存在
EffectCode.EFFECT_ERR_RUN_FAILED            = 5102 -- 效果执行失败
EffectCode.EFFECT_ERR_ROLE_EXISTED          = 5103 -- 效果 角色已存在

-- 结算相关
local SettleCode = enum("SettleCode", 0)
SettleCode.SETTLE_ERR_NOT_EXIST             = 5301 -- 结算数据不存在
SettleCode.SETTLE_ERR_CFG                   = 5302 -- 结算配置错误

-- 武器配件相关
local AttachmentCode = enum("AttachmentCode", 0)
AttachmentCode.ATTACHMENT_ERR_NOT_ENOUGH_LV = 5401 -- 等级不够
AttachmentCode.ATTACHMENT_ERR_EQUIPED       = 5402 -- 已装备该配件
AttachmentCode.ATTACHMENT_ERR_UNLOCK        = 5403 -- 配件未解锁

-- 奖励
local RewardCode = enum("RewardCode", 0)
RewardCode.REWARD_NOT_EXIST                 = 5501  --奖励不存在
RewardCode.REWARD_RECIVED                   = 5502  --奖励已领取
RewardCode.REWARD_EXPIRED                   = 5503  --奖励已过期

-- BUFF
local BuffCode = enum("BuffCode", 0)
BuffCode.CFG_NOT_EXIST                      = 5601  --buff配置不存在

-- 属性
local AttrCode = enum("AttrCode", 0)
AttrCode.NEED_MORE_IDEAL                    = 8101  -- 理想币不足
AttrCode.NEED_MORE_CRYSTAL                  = 8202  -- 巴布水晶不足
AttrCode.NEED_MORE_HERMES                   = 8203  -- 赫尔币不足

-- 名品框
local VcardCode = enum("VcardCode", 0)
VcardCode.VCARD_FRAME_NOT_EXIST             = 8401;  -- 名品资源不存在

-- 生涯
local CareerCode = enum("CareerCode", 0)
CareerCode.HEAD_ICON_NO_EXIST               = 8601  -- 该头像不存在
CareerCode.HEAD_ICON_NO_GET                 = 8602  -- 没有获得该头像

-- 战绩
local StandingsCode = enum("StandingsCode", 0)
StandingsCode.STANDINGS_NO_EXIST            = 8701  -- 战绩不存在

-- 特殊行动
local BattlePassCode = enum("BattlePassCode", 0)
BattlePassCode.TASK_NOT_EXIST               = 8801  -- 任务不存在
BattlePassCode.TYPE_CAN_NOT_CHANGE          = 8802  -- 当前类型不允许更换
BattlePassCode.TASK_HAS_FINISH              = 8803  -- 任务已经完成
BattlePassCode.CLUE_NOT_EXIST               = 8804  -- 线索不存在
BattlePassCode.REWARD_RECEIVED              = 8805  -- 奖励已领取
BattlePassCode.REWARD_TAKE_FAILED           = 8806  -- 奖励发放失败
BattlePassCode.REWARD_NOT_EXIST             = 8807  -- 奖励等级不存在
BattlePassCode.REWARD_NOT_UNLOCK            = 8808  -- 奖励尚未解锁
BattlePassCode.CLUE_NOT_UNLOCK              = 8809  -- 线索尚未解锁