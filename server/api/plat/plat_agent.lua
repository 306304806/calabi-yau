--plat_agent.lua
local ljson = require("luacjson")
local NetClient = import("kernel/network/net_client.lua")

local log_info          = logger.info
local log_debug         = logger.debug
local serialize         = logger.serialize
local json_encode       = ljson.encode
local json_decode       = ljson.decode
local tunpack           = table.unpack
local tinsert           = table.insert
local ssplit            = string_ext.split
local check_success     = utility.check_success

local NetwkTime         = enum("NetwkTime")
local PeriodTime        = enum("PeriodTime")
local PlatSSCmdID       = enum("PlatSSCmdID")

local timer_mgr         = quanta.get("timer_mgr")
local event_mgr         = quanta.get("event_mgr")
local thread_mgr        = quanta.get("thread_mgr")
local router_mgr        = quanta.get("router_mgr")

local PlatformAgent = singleton()
local prop = property(PlatformAgent)
prop:accessor("serial", 0)
prop:accessor("client", nil)
prop:accessor("subscribes", {})
prop:accessor("next_connect_time", 0)
function PlatformAgent:__init()
    ljson.encode_sparse_array(true)
    --定时器
    timer_mgr:loop(PeriodTime.SECOND_5_MS, function()
        self:on_timer()
    end)
end

--定时器
function PlatformAgent:on_timer()
    self:send_heartbeat()
    self:check_conn()
end

-- 发送心跳
function PlatformAgent:send_heartbeat()
    if self.client then
        self.client:send_dx(PlatSSCmdID.NID_SERVER_HEART_REQ, {serial = self.serial})
    end
end

-- 连接检查
function PlatformAgent:check_conn()
    if not self.client then
        thread_mgr:success_call(PeriodTime.SECOND_MS, function()
            local ok, code, plat_url = router_mgr:call_dirsvr_hash(quanta.now, "rpc_platform_gateway")
            if ok and check_success(code) then
                log_info("[PlatformAgent][setup] get platform gateways success")
                local ip, port = tunpack(ssplit(plat_url, ":"))
                local client = NetClient(self, ip, port)
                client:set_encoder(function(cmd_id, data)
                    return json_encode(data)
                end)
                client:set_decoder(function(cmd_id, data)
                    local cmd_name = "json." .. cmd_id
                    return json_decode(data), cmd_name
                end)
                self.client = client
                return true
            end
            log_info("[PlatformAgent][setup] get platform gateways failed, code: %s", code)
            return false
        end)
    end
    if self.client and not self.client:is_alive() then
        if quanta.now >= self.next_connect_time then
            self.next_connect_time = quanta.now + NetwkTime.RECONNECT_TIME
            self.client:connect()
        end
    end
end

-- 连接成功回调
function PlatformAgent:on_socket_connect(client)
    log_info("[PlatformAgent][on_socket_connect]: connect platform success!")
    -- 注册节点
    local node_data = { node_id = quanta.id }
    self.client:call_dx(PlatSSCmdID.NID_SERVER_REGISTER_REQ, node_data)
    --发送订阅消息
    for _, subscribe in pairs(self.subscribes) do
        self:call(PlatSSCmdID.NID_SERVER_SUBSCRIBE_REQ, subscribe)
    end
    event_mgr:notify_trigger("evt_platform_connect")
end

-- 订阅注册
function PlatformAgent:subscribe_register(area_id, message)
    tinsert(self.subscribes, {area_id = area_id, message = message})
end

--订阅回调
function PlatformAgent:on_subscribe_ntf(body)
    local message, data = body.message, body.data
    log_debug("[PlatformAgent][on_subscribe_ntf] message %s: data=%s", message, serialize(data))
    event_mgr:notify_trigger(message, data)
end

-- 连接关闭回调
function PlatformAgent:on_socket_err(client, err)
    log_debug("[PlatformAgent][on_socket_err]: platform lost %s!", err)
    self.client = nil
    self.next_connect_time = 0
    self:check_conn()
end

-- 网络消息回调
function PlatformAgent:on_socket_rpc(client, cmd_id, body, session_id)
    if cmd_id == PlatSSCmdID.NID_SERVER_HEART_RES then
        self.serial = body.serial
        return
    end
    if cmd_id == PlatSSCmdID.NID_SERVER_SUBSCRIBE_NTF then
        self:on_subscribe_ntf(body)
        return
    end
    event_mgr:notify_command(cmd_id, body, session_id)
end

-- 请求服务
function PlatformAgent:call(cmd_id, cmd_data)
    if not self.client then
        return false
    end
    return self.client:call_dx(cmd_id, cmd_data)
end

-- 请求服务
function PlatformAgent:send(cmd_id, cmd_data)
    if not self.client then
        return false
    end
    return self.client:send_dx(cmd_id, cmd_data)
end

quanta.plat_agent = PlatformAgent()

return PlatformAgent
