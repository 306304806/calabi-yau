--plat_const.lua

-- 账号平台类型
local AccountPlatform = enum("AccountPlatform", 0)
AccountPlatform.GUEST               = 1     -- 游客
AccountPlatform.WECHAT              = 2     -- 微信
AccountPlatform.QQ                  = 3     -- QQ
AccountPlatform.OTHER               = 4     -- 其他
AccountPlatform.STEAM               = 5     -- steam账号

--平台RMSG类型
local RmsgPlat = enum("RmsgPlat", 0)
RmsgPlat.PLAT_FRIEND            = 1     -- 平台好友功能
RmsgPlat.PLAT_MAIL              = 2     -- 平台邮件功能更
RmsgPlat.PLAT_CHAT              = 3     -- 平台聊天功能

-- 邮件状态
local MailState = enum("MailState", 0)
MailState.UNREADED              = 0     -- 未读
MailState.READED                = 1     -- 已读
MailState.DELETED               = 2     -- 已删除

-- 附件状态
local AttachState = enum("AttachState", 0)
AttachState.UNTAKE          = 0     -- 未领取
AttachState.TAKED           = 1     -- 已领取

--好友类型
local FriendType = enum("FriendType", 0)
FriendType.FRIEND               = 1     --好友
FriendType.BLACK                = 2     --黑名单
FriendType.APPLY                = 3     --申请列表
FriendType.NEAR                 = 4     --最近好友

--好友上限
local FriendLimit = enum("FriendLimit", 0)
FriendLimit.FRIEND              = 15    --好友上限
FriendLimit.APPLY               = 5     --申请列表上限
FriendLimit.NEAR                = 5     --最近列表上限

local SocialSecret = enum("SocialSecret", 0)
SocialSecret.PUBLIC                 = 0    --公共
SocialSecret.FRIEND                 = 1    --好友
SocialSecret.PRIVATE                = 2    --私密

-- 红点类型
local ReddotType = enum("ReddotType", 0)
ReddotType.INTIMACY                 = 1     --好友好感度
ReddotType.NEW_ITEM                 = 2     --新获得ITEM
ReddotType.NEW_ROLE                 = 3     --获得新角色
ReddotType.NEW_ROLE_SKIN            = 4     --获得新角色皮肤
ReddotType.UNLOCK_ATTACHMENT        = 5     --解锁武器配件
ReddotType.NEW_DETAIL               = 6
ReddotType.NEW_VOICE                = 7
ReddotType.NEW_ACTION               = 8

--好友设置类型
local FriendSetup = enum("FriendSetup", 0)
FriendSetup.SETUP_ONLINE            = 1
FriendSetup.SETUP_SECRET            = 2

--通用常量
local PlatCommon = enum("PlatCommon", 0)
PlatCommon.FIGHT_INTIMACY           = 50    --战斗增加好感度
PlatCommon.SEARCH_COUNT             = 20    --单次搜索好友数量
PlatCommon.DATA_CACHE_TIME          = 1800  --角色数据缓存事件
PlatCommon.GLOBAL_DEFAULT_AREA_ID   = 1     -- 全区排行默认区ID


local RankType = enum("RankType", 0)
RankType.STARS                      = 1     -- 星星排行

-- 限购类型
local LimitPurchaseType = enum("LimitPurchaseType", 0)
LimitPurchaseType.NULL              = 0      -- 不限购
LimitPurchaseType.DAY               = 1      -- 日限购
LimitPurchaseType.WEEK              = 2      -- 周限购
LimitPurchaseType.MONTH             = 3      -- 月限购
LimitPurchaseType.FORVER            = 4      -- 永久限购
LimitPurchaseType.BATTLEPASS        = 5      -- battlepass赛季

-- 订单类型
local OrderType = enum("OrderType", 0)
OrderType.SELF_BUY                  = 1    -- 自购
OrderType.GIVING                    = 2    -- 赠送
OrderType.CLAIMING                  = 3    -- 索要

-- 订单状态
local OrderStatus = enum("OrderStatus", 0)
OrderStatus.CREATED                 = 1    -- 已创建
OrderStatus.PAYED                   = 2    -- 已支付
OrderStatus.RECIVED                 = 3    -- 已收货

local PayType = enum("PayType", 0)
PayType.CRYSTAL                     = 1     -- 巴布晶体
PayType.IDEAL                       = 2     -- 理想币
PayType.ITEM                        = 3     -- 物品

-- 邮件类型
local MailType = enum("MailType", 0)
MailType.PLAYER_MAIL                = 1     -- 玩家邮件(1-1私聊)
MailType.GLOBAL_MAIL                = 2     -- 全局邮件
MailType.GIFT_MAIL                  = 3     -- 礼物邮件
MailType.SYS_MAIL                   = 4     -- 系统邮件

-- 礼物邮件子类型
local SubGiftType = enum("SubGiftType", 0)
SubGiftType.GIFT_RECEIVE            = 1     -- 收到礼物
SubGiftType.GIFT_CLAIM              = 2     -- 索要消息
SubGiftType.GIFT_RECORD             = 3     -- 赠礼记录

-- 排行榜更新方式
local RankUpdateType = enum("RankUpdateType", 0)
RankUpdateType.HOUR                 = 1     -- 整点更新

--申请上限
quanta_const.APPLY_LIMIT = {
    {0, 10},       --100以下申请上限
    {5, 8},        --200以下申请上限
    {10, 5},      --250以下申请上限
    {13, 2},      --300以下申请上限
}

-- 充值订单状态
local RechargeOrderState = enum("RechargeOrderState", 0)
RechargeOrderState.WAIT_PAY  = 1  -- 等待支付
RechargeOrderState.WAIT_SEND = 2  -- 等待发货
RechargeOrderState.FINISH    = 3  -- 结束(已经发货)
RechargeOrderState.CANCLE    = 4  -- 已经取消
RechargeOrderState.TIMEOUT   = 5  -- 已超时

-- 充值平台
local RechargePlatformId = enum("RechargePlatformId", 0)
RechargePlatformId.STEAM     = 1  -- steam支付平台

-- 付费充值上报状态
local DlogRechageState  = enum("DlogRechageState", 0)
DlogRechageState.NO_PAY      = 1    -- 未支付
DlogRechageState.CANCEL      = 2    -- 支付前取消
DlogRechageState.PAY_SUCCESS = 3    -- 支付成功
DlogRechageState.SEND_FAIL   = 4    -- 发货失败
DlogRechageState.FINISH      = 5    -- 支付完成
DlogRechageState.RETURN      = 6    -- 退货退款

-- 邮件上报状态
local DlogMailState     = enum("DlogMailState", 0)
DlogMailState.NEW            = 1    -- 获得邮件
DlogMailState.READ           = 2    -- 阅读邮件
DlogMailState.CLICK_LINK     = 3    -- 点击链接
DlogMailState.RECIVED        = 4    -- 获得邮件
DlogMailState.BUY_GIFT       = 5    -- 购买邮件礼包

-- 聊天上报状态
local DlogChatState     = enum("DlogChatState", 0)
DlogChatState.PRIVATE        = 1    -- 私聊
DlogChatState.CLAN           = 2    -- 行会
DlogChatState.TEAM           = 3    -- 组队
DlogChatState.NEAR           = 4    -- 附近
DlogChatState.WORLD          = 5    -- 世界
DlogChatState.TRUMPET        = 6    -- 小喇叭
DlogChatState.DANMU          = 7    -- 弹幕

-- 功能解锁模块
local FuncUnlockID      = enum("FuncUnlockID", 0)
FuncUnlockID.GIVING          = 102  -- 商品赠送
FuncUnlockID.CLAIM           = 103  -- 商品索要

-- 小区服务器状态
local AreaStatus = enum("AreaStatus", 0)
AreaStatus.MAINTAIN             = 0     --维护
AreaStatus.DEBUG                = 1     --调试
AreaStatus.ACTIVE               = 2     --激活码
AreaStatus.NEWBEE               = 3     --新服
AreaStatus.NORMAL               = 4     --正常
AreaStatus.PROPOSE              = 5     --推荐
AreaStatus.FULL                 = 6     --爆满

-- 身份认证证件类型
local CertType = enum("CertType", 0)
CertType.ID_CARD                = 1    -- 身份证
CertType.PASS_CARD              = 2    -- 港澳台通行证
CertType.HK_CARD                = 3    -- 港澳台身份证
CertType.PASSPORT               = 4    -- 护照
CertType.SOLDIER                = 5    -- 军人身份证

--群类型
local ChatType = enum("ChatType", 0)
ChatType.WORLD                 = 1    -- 世界群
ChatType.AREA                  = 2    -- 分区群
ChatType.SYSTEM                = 3    -- 系统群
ChatType.TEAM                  = 4    -- 队伍群
ChatType.ROOM                  = 5    -- 房间群
ChatType.FIGHT                 = 6    -- 战斗群
