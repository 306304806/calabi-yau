--plat_api.lua
import("api/plat/plat_const.lua")
import("api/plat/plat_proto.lua")
import("api/plat/plat_agent.lua")

local tostring      = tostring
local log_err       = logger.err
local guid_index    = guid.index

local KernCode      = enum("KernCode")
local SSCmdID       = enum("PlatSSCmdID")
local SUCCESS       = KernCode.SUCCESS
local RPC_FAILED    = KernCode.RPC_FAILED

local plat_agent    = quanta.get("plat_agent")

local PlatformApi = singleton()
function PlatformApi:__init()
end

-- 订阅注册
function PlatformApi:subscribe_register(area_id, message)
    plat_agent:subscribe_register(area_id, message)
end

-- 账号登录
function PlatformApi:login_account(area_id, platform_id, open_id, token)
    local rpc_req = {area_id = area_id, platform_id = platform_id, open_id = open_id, token = token }
    local ok, rpc_res = plat_agent:call(SSCmdID.NID_SS_LOGIN_ACCOUNT_REQ, rpc_req)
    if not ok then
        log_err("[PlatformApi][get_account_info] rpc faild: platform_id=%s, open_id=%s, area_id=%s", platform_id, open_id, area_id)
        return KernCode.RPC_FAILED
    end
    return rpc_res.code, rpc_res.account_id
end

--玩家登录
function PlatformApi:login_player(player_id, player_data, token, account_data)
    local area_id = guid_index(player_id)
    local cmd_data = {
        token = token,
        area_id = area_id,
        player = player_data,
        account_data = account_data,
    }
    local ok, rpc_res = plat_agent:call(SSCmdID.NID_PLAYER_LOGIN_REQ, cmd_data)if not ok then
        log_err("[PlatformApi][login_player] rpc faild: player_id=%s, area_id=%s", player_id, area_id)
        return KernCode.RPC_FAILED
    end
    return rpc_res.code
end

--玩家登录
function PlatformApi:create_player(player_id, player_data, token, account_data)
    local area_id = guid_index(player_id)
    local cmd_data = {
        token = token,
        area_id = area_id,
        player = player_data,
        account_data = account_data,
    }
    local ok, rpc_res = plat_agent:call(SSCmdID.NID_PLAYER_CREATE_REQ, cmd_data)
    if not ok then
        log_err("[PlatformApi][create_player] rpc faild: player_id=%s, area_id=%s", player_id, area_id)
        return KernCode.RPC_FAILED
    end
    return rpc_res.code
end

--玩家数据更新
function PlatformApi:update_player(player_id, player_data)
    player_data.player_id = player_id
    local area_id = guid_index(player_id)
    local cmd_data = {
        area_id = area_id,
        player = player_data
    }
    plat_agent:send(SSCmdID.NID_PLAYER_UPDATE_REQ, cmd_data)
end

--玩家登出
function PlatformApi:logout_player(player_id, player_data)
    player_data.player_id = player_id
    local area_id = guid_index(player_id)
    local cmd_data = {
        area_id = area_id,
        player = player_data
    }
    plat_agent:send(SSCmdID.NID_PLAYER_LOGOUT_REQ, cmd_data)
end

-- 离线玩家数据更新
function PlatformApi:offline_player_update(player_id, player_data)
    player_data.player_id = player_id
    local area_id = guid_index(player_id)
    local cmd_data = {
        area_id = area_id,
        player = player_data
    }
    plat_agent:send(SSCmdID.NID_OFFLINE_UPDATE_REQ, cmd_data)
end

--生成最近好友
function PlatformApi:build_recent_friend(targets)
    for _, player_id in pairs(targets) do
        local area_id = guid_index(player_id)
        local cmd_data = {
            area_id = area_id,
            targets = targets,
            player_id = player_id,
        }
        plat_agent:send(SSCmdID.NID_SS_FRIEND_BUILD_RECENT_REQ, cmd_data)
    end
end

--玩家添加好感度
function PlatformApi:add_friend_intimacy(player_id, target_id, intimacy)
    local area_id = guid_index(player_id)
    local cmd_data = {
        area_id     = area_id,
        player_id   = player_id,
        target_id   = target_id,
        intimacy    = intimacy,
    }
    plat_agent:send(SSCmdID.NID_SS_FRIEND_ADD_INTIMACY_REQ, cmd_data)
end

--玩家团队隐私查询
function PlatformApi:query_friend_secret(player_id, target_id)
    local area_id = guid_index(player_id)
    local cmd_data = {
        area_id     = area_id,
        player_id   = player_id,
        target_id   = target_id,
    }
    local ok, rpc_res = plat_agent:call(SSCmdID.NID_SS_FRIEND_SECRET_REQ, cmd_data)
    if ok and rpc_res.code == SUCCESS then
        return true
    end
    return false
end

--玩家查询邮件需要发放的附件
function PlatformApi:take_mail_attach(player_id)
    local rpc_req = { player_id = player_id, area_id = guid_index(player_id) }
    local ok, rpc_res = plat_agent:call(SSCmdID.NID_SS_MAIL_ATTACH_GET_REQ, rpc_req)
    if not ok then
        log_err("[PlatformApi][take_mail_attach] rpc faild: player_id=%s,ok=false", player_id)
        return RPC_FAILED
    end
    return rpc_res.code, rpc_res.attach_items
end

--玩家删除已经发放附件的邮件
function PlatformApi:del_mail_attach(player_id, mail_ids)
    local rpc_req = { player_id = player_id, area_id = guid_index(player_id), mail_ids = mail_ids }
    local ok, rpc_res = plat_agent:call(SSCmdID.NID_SS_MAIL_ATTACH_DEL_REQ, rpc_req)
    if not ok then
        log_err("[PlatformApi][del_mail_attach] rpc faild: player_id=%s,ok=false", player_id)
        return RPC_FAILED
    end
    return rpc_res.code
end

--发送玩家邮件
function PlatformApi:send_player_mail(player_id, mail_info)
    local rpc_req = { mail_info = mail_info, area_id = guid_index(player_id) }
    local ok, rpc_res = plat_agent:call(SSCmdID.NID_SS_MAIL_SEND_REQ, rpc_req)
    if not ok then
        log_err("[PlatformApi][send_player_mail] rpc faild: player_id=%s,ok=false", player_id)
        return RPC_FAILED
    end
    return rpc_res.code, rpc_res
end

-- 发送全局邮件
function PlatformApi:send_global_mail(area_id, mail_info)
    local rpc_req = { area_id = area_id, mail_info = mail_info }
    local ok, rpc_res = plat_agent:call(SSCmdID.NID_SS_MAIL_SEND_REQ, rpc_req)
    if not ok then
        log_err("[PlatformApi][sys_send_group_mail] rpc faild: ok=false")
        return RPC_FAILED
    end
    return rpc_res.code, rpc_res
end

-- 请求创建聊天群
function PlatformApi:create_chat_group(area_id, rpc_req)
    rpc_req.area_id = area_id
    local ok, rpc_res = plat_agent:call(SSCmdID.NID_SS_CHAT_GROUP_CREATE_REQ, rpc_req)
    if not ok then
        log_err("[PlatformApi][create_chat_group] rpc faild: ok=false")
        return RPC_FAILED
    end
    return rpc_res.code, rpc_res
end

-- 请求解散聊天群
function PlatformApi:destory_chat_group(area_id, rpc_req)
    rpc_req.area_id = area_id
    local ok, rpc_res = plat_agent:call(SSCmdID.NID_SS_CHAT_GROUP_DESTORY_REQ, rpc_req)
    if not ok then
        log_err("[PlatformApi][destory_chat_group] rpc faild: ok=false")
        return RPC_FAILED
    end
    return rpc_res.code, rpc_res
end

-- 请求加入指定玩家到聊天群
function PlatformApi:add_chat_group_member(area_id, rpc_req)
    rpc_req.area_id = area_id
    local ok, rpc_res = plat_agent:call(SSCmdID.NID_SS_CHAT_GROUP_MEMBER_JOIN_REQ, rpc_req)
    if not ok then
        log_err("[PlatformApi][add_chat_group_member] rpc faild: ok=false")
        return RPC_FAILED
    end
    return rpc_res.code, rpc_res
end

-- 请求移除指定玩家出聊天群
function PlatformApi:del_chat_group_member(area_id, rpc_req)
    rpc_req.area_id = area_id
    local ok, rpc_res = plat_agent:call(SSCmdID.NID_SS_CHAT_GROUP_MEMBER_QUIT_REQ, rpc_req)
    if not ok then
        log_err("[PlatformApi][del_chat_group_member] rpc faild: ok=false")
        return RPC_FAILED
    end
    return rpc_res.code, rpc_res
end

-- 更新玩家群内平道
function PlatformApi:update_chat_group_member(area_id, rpc_req)
    rpc_req.area_id = area_id
    local ok, rpc_res = plat_agent:call(SSCmdID.NID_SS_CHAT_GROUP_MEMBER_UPDATE_REQ, rpc_req)
    if not ok then
        log_err("[PlatformApi][update_chat_group_member] rpc faild: ok=false")
        return RPC_FAILED
    end
    return rpc_res.code, rpc_res
end

-- 添加红点请求
function PlatformApi:reddot_add_req(player_id, reddot_type, reddot_rid, overtime, custom)
    local area_id = guid_index(player_id)
    if custom and type(custom) ~= 'string' then
        custom = tostring(custom)
    end
    local cmd_data = {
        custom = custom,
        area_id = area_id,
        overtime = overtime,
        player_id = player_id,
        reddot_rid = reddot_rid,
        reddot_type = reddot_type,
    }
    local ok = plat_agent:send(SSCmdID.NID_SS_REDDOT_ADD_REQ, cmd_data)
    if not ok then
        log_err("[PlatformApi][reddot_add_req] rpc faild")
    end
end

-- 获取订单支付信息
function PlatformApi:order_pay_info_req(player_id, order_id)
    local area_id = guid_index(player_id)
    local cmd_data = {
        area_id     = area_id,
        player_id   = player_id,
        order_id    = order_id,
    }
    local ok, rpc_res = plat_agent:call(SSCmdID.NID_SS_ORDER_PAY_INFO_REQ, cmd_data)
    if not ok then
        log_err("[PlatformApi][order_pay_info_req] rpc faild: ok=false! player_id:%s, order_id:%s", player_id, order_id)
        return RPC_FAILED
    end
    return rpc_res.code, rpc_res.pay_info
end

-- 获取订单发货信息
function PlatformApi:order_pay_finish_req(player_id, order_id)
    local area_id = guid_index(player_id)
    local cmd_data = {
        area_id     = area_id,
        player_id   = player_id,
        order_id    = order_id,
    }

    local ok, rpc_res = plat_agent:call(SSCmdID.NID_SS_ORDER_PAY_FINISH_REQ, cmd_data)
    if not ok then
        log_err("[PlatformApi][order_pay_finish_req] rpc faild: ok=false! player_id:%s, order_id:%s", player_id, order_id)
        return RPC_FAILED
    else
        return rpc_res.code, rpc_res.shipment_info
    end
end
-- 敏感词检测
function PlatformApi:fliterword_check(player_id, content)
    local area_id = guid_index(player_id)
    local rpc_req = {
        area_id = area_id,
        content = content,
    }
    local ok, code = plat_agent:call(SSCmdID.NID_SS_SAFE_FILTERWORD_CHECK_REQ, rpc_req)
    if not ok then
        log_err("[PlatformApi][fliterword_check] rpc faild: ok=false")
        return RPC_FAILED
    end
    return code
end

-- 敏感词转换
function PlatformApi:fliterword_trans(player_id, content, rword)
    local area_id = guid_index(player_id)
    local rpc_req = {
        area_id = area_id,
        content = content,
        rword   = rword
    }
    local ok, rpc_res = plat_agent:call(SSCmdID.NID_SS_SAFE_FILTERWORD_TRANS_REQ, rpc_req)
    if not ok then
        log_err("[PlatformApi][fliterword_trans] rpc faild: ok=false")
        return RPC_FAILED
    end
    return rpc_res.code, rpc_res
end

-- 校验昵称是否可用
function PlatformApi:check_player_name_used(player_id, nick)
    local rpc_req = {
        area_id = guid_index(player_id),
        nick    = nick,
    }
    local ok, rpc_res = plat_agent:call(SSCmdID.NID_SS_SAFE_PLAYER_NAME_USED_REQ, rpc_req)
    if not ok then
        log_err("[PlatformApi][check_player_name_used] rpc faild: ok=false")
        return RPC_FAILED
    end
    return rpc_res.code
end

-- 充值订单发货
function PlatformApi:recharge_order_send(player_id, order_id)
    local rpc_req = {
        area_id   = guid_index(player_id),
        player_id = player_id,
        order_id  = order_id,
    }
    local ok, rpc_res = plat_agent:call(SSCmdID.NID_SS_RECHARGE_ORDER_SEND_REQ, rpc_req)
    if not ok then
        log_err("[PlatformApi][recharge_order_send] rpc faild: ok=false")
        return RPC_FAILED
    end
    return rpc_res.code, rpc_res
end

-- 充值订单查询
function PlatformApi:recharge_order_query(area_id, player_id, order_state)
    local rpc_req = {
        area_id     = area_id,
        player_id   = player_id,
        order_state = order_state,  -- 状态
    }
    local ok, rpc_res = plat_agent:call(SSCmdID.NID_SS_RECHARGE_ORDER_QUERY_REQ, rpc_req)
    if not ok then
        return RPC_FAILED
    end
    return rpc_res.code, rpc_res
end

-- 查询玩家的未发货订单id列表
function PlatformApi:recharge_ws_order_ids_query(player_id)
    local rpc_req = {
        area_id   = guid_index(player_id),
        player_id = player_id,
    }
    local ok, rpc_res = plat_agent:call(SSCmdID.NID_SS_RECHARGE_WS_ORDER_IDS_QUERY_REQ, rpc_req)
    if not ok then
        return RPC_FAILED
    end
    return rpc_res.code, rpc_res
end

-- 查询队伍好友关系
function PlatformApi:query_players_friendship(area_id, player_group)
    local rpc_req = {
        area_id = area_id,
        player_group = player_group,
    }
    local ok, rpc_res = plat_agent:call(SSCmdID.NID_SS_FRIENDSHIP_QUERY_REQ, rpc_req)
    if not ok then
        return RPC_FAILED
    end
    return rpc_res.code, rpc_res.friendship
end

-- 查询赛季信息
function PlatformApi:query_season_info(area_id)
    local rpc_req = {
        area_id = area_id,
    }
    local ok, rpc_res = plat_agent:call(SSCmdID.NID_SS_GET_SEASON_INFO_REQ, rpc_req)
    if not ok then
        return RPC_FAILED
    end
    return rpc_res.code, rpc_res.season_info
end

-- export
quanta.plat_api = PlatformApi()

return PlatformApi
