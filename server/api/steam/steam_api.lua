-- steam_api.lua
local ljson = require("luacjson")
local tinsert = table_ext.insert
local otime   = os.time

local json_decode   = ljson.decode
local log_warn      = logger.warn
local log_err       = logger.err
local log_debug     = logger.debug
local serialize     = logger.serialize

local KernCode      = enum("KernCode")
local router_mgr    = quanta.get("router_mgr")
local config_mgr    = quanta.get("config_mgr")
local utility_db    = config_mgr:init_table("utility", "id")

local SteamApi = singleton()
function SteamApi:__init()
    ljson.encode_sparse_array(true)
    self:setup()
end

function SteamApi:setup()
    self.app_id      = utility_db:find_value("value", "steam_app_id")
    self.web_key     = utility_db:find_value("value", "steam_web_key")
    self.publish_key = utility_db:find_value("value", "steam_web_publish_key")
end

-- 验证玩家tick
-- ISteamUserAuth/AuthenticateUserTicket/v1/
-- return: ec, code
function SteamApi:auth_user_token(open_id, token)
    local url = "http://api.steampowered.com/ISteamUserAuth/AuthenticateUserTicket/v1/"
    local query_parameter = {
        key    = self.web_key,
        appid  = self.app_id,
        ticket = token,
    }

    local headers = {
        ["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8",
    }

    -- 请求steam服务器
    local ok, code, res = router_mgr:call_proxy_hash(open_id, "rpc_http_get", url, query_parameter, headers)
    if not ok or 200 ~= code then
        log_err("[SteamApi][auth_user_token] call faild: ok=%s,code=%s,res=%s", ok, code, serialize(res))
        return KernCode.NETWORK_ERROR
    end

    ok, res = pcall(json_decode, res)
    if not ok or not res or not res.response then
        log_err("[SteamApi][auth_user_token] json_decode faild: ok=%s,code=%s,res=%s", ok, code, serialize(res))
        return KernCode.NETWORK_ERROR
    end

    log_debug("[SteamApi][auth_user_token]: open_id=%s,response=%s", open_id, serialize(res))

    -- 校验结果
    if res.response.error then
        return KernCode.TOKEN_ERROR
    end

    -- 验证是否是正确的open_id
    if res.response.params then
        if "OK" == res.response.params.result and res.response.params.steamid == open_id then
            return KernCode.SUCCESS
        end
        return KernCode.TOKEN_ERROR
    end

    return KernCode.NETWORK_ERROR
end

-- (开始交易)请求充值充值订单
-- in:
--   open_id: 目标玩家的steamid
--   order_id: 游戏服务器自己生成的订单号
--   shopping_cart_item_cnt: 购物车中的物品数量
--   item_id: 游戏系统定义的物品id
--   item_cnt: 物品的数量
--   item_desc: 物品描述
--   language: 物品描述的ISO 639-1语言代码
--   amount: 总价格(单位是分/或者个)
--   currency: 描述amount的币种  ISO4217货币代码
--   is_sandbox：是否沙盒模式
function SteamApi:init_txn(
    open_id, order_id, shopping_cart_item_cnt, item_id, item_cnt, item_desc, language, amount, currency, is_sandbox)
    local url = "https://partner.steam-api.com/ISteamMicroTxn/InitTxn/v3/"
    -- 沙盒支付需要输出警告提示，避免运营事故
    if is_sandbox then
        url = "https://partner.steam-api.com/ISteamMicroTxnSandbox/InitTxn/v3/"
        log_warn("[SteamApi][init_txn] sandbox model: open_id=%s,item_id=%s,item_cnt=%s,amount=%s,currency=%s",
            open_id, item_id, item_cnt, amount, currency)
    end

    -- 请求steam服务器
    local post_req = {
        ["key"]            = self.publish_key,
        ["orderid"]        = order_id,
        ["steamid"]        = open_id,
        ["appid"]          = self.app_id,
        ["itemcount"]      = shopping_cart_item_cnt,
        ["language"]       = language,
        ["currency"]       = currency,
        ["itemid[0]"]      = item_id,
        ["qty[0]"]         = item_cnt,
        ["amount[0]"]      = amount,
        ["description[0]"] = item_desc,
    }
    local ok, code, post_res = router_mgr:call_proxy_hash(order_id, "rpc_http_post", url, {}, post_req, {})
    if not ok or 200 ~= code then
        log_err("[SteamApi][init_txn] call faild: ok=%s,code=%s,req=%s,res=%s", ok, code,serialize(post_req), serialize(post_res))
        return KernCode.NETWORK_ERROR
    end

    ok, post_res = pcall(json_decode, post_res)
    if not ok or not post_res or not post_res.response then
        log_err("[SteamApi][init_txn] json_decode faild: ok=%s,code=%s,res=%s", ok, code, serialize(post_res))
        return KernCode.NETWORK_ERROR
    end

    local steam_ok = ("OK" == post_res.response.result)  -- 是否下单成功
    local steam_code
    local steam_msg
    if not steam_ok then
        steam_code = post_res.response.error.errorcode  -- steam返回的错误码
        steam_msg  = post_res.response.error.errordesc  -- steam返回的错误信息
    end

    return KernCode.SUCCESS, {
        ok         = steam_ok,
        order_id   = tostring(post_res.response.params.orderid), -- 游戏生成的订单号
        trans_id   = tostring(post_res.response.params.transid), -- stream生成的订单号
        steam_code = steam_code,
        steam_msg  = steam_msg,
    }
end

-- (结束交易)查询订单交易状态结果
function SteamApi:finalize_txn(order_id, is_sandbox)
    local url = "https://partner.steam-api.com/ISteamMicroTxn/FinalizeTxn/v2/"
    -- 沙盒支付需要输出警告提示，避免运营事故
    if is_sandbox then
        url = "https://partner.steam-api.com/ISteamMicroTxnSandbox/FinalizeTxn/v2/"
        log_warn("[SteamApi][init_txn] sandbox model: order_id=%s", order_id)
    end

    local post_req = {
        key     = self.publish_key,
        orderid = order_id,
        appid   = self.app_id,
    }

    local ok, code, post_res = router_mgr:call_proxy_hash(order_id, "rpc_http_post", url, {}, post_req, {})
    if not ok or 200 ~= code then
        if 400 == code then
            return KernCode.SUCCESS, { pay_ok = false, }
        end

        log_err("[SteamApi][finalize_txn] call faild: ok=%s,code=%s,req=%s,res=%s", ok, code,serialize(post_req), serialize(post_res))
        return KernCode.NETWORK_ERROR
    end

    ok, post_res = pcall(json_decode, post_res)
    if not ok or not post_res or not post_res.response then
        log_err("[SteamApi][finalize_txn] json_decode faild: ok=%s,code=%s,res=%s", ok, code, serialize(post_res))
        return KernCode.NETWORK_ERROR
    end

    local pay_ok   = ("OK" == post_res.response.result)
    local trans_id = post_res.response.params.transid

    return KernCode.SUCCESS, {
        pay_ok   = pay_ok,
        order_id = order_id,
        trans_id = trans_id,
    }
end

-- (查询交易信息)查询steam保存的订单详情
function SteamApi:query_txn(order_id, is_sandbox)
    local url = "https://partner.steam-api.com/ISteamMicroTxn/QueryTxn/v2/"
    -- 沙盒支付需要输出警告提示，避免运营事故
    if is_sandbox then
        url = "https://partner.steam-api.com/ISteamMicroTxnSandbox/QueryTxn/v2/"
        log_warn("[SteamApi][query_txn] sandbox model: order_id=%s", order_id)
    end

    local get_req = {
        key     = self.publish_key,
        appid   = self.app_id,
        orderid = order_id,
        transid = nil,
    }

    local result = {
        query_ok     = false,
        order_id     = nil,
        pay_order_id = nil,
        pay_open_id  = nil,
        currency     = nil,
        pay_finish   = false,  -- 支付完成
        items        = {
            -- item_id
            -- iteam_count
            -- amount
            -- pay_finish
        },
        insert_time = nil,
    }

    local ok, code, get_res = router_mgr:call_proxy_hash(order_id, "rpc_http_get", url, get_req, {})
    if not ok or 200 ~= code then
        log_err("[SteamApi][query_txn] call faild: ok=%s,code=%s,req=%s,res=%s", ok, code,serialize(get_req), serialize(get_res))

        -- steam返回订单不存在
        if 400 == code then
          return KernCode.SUCCESS, result
        end

        return KernCode.NETWORK_ERROR
    end

    ok, get_res = pcall(json_decode, get_res)
    if not ok or not get_res or not get_res.response then
        log_err("[SteamApi][query_txn] json_decode faild: ok=%s,code=%s,res=%s", ok, code, serialize(get_res))
        return KernCode.NETWORK_ERROR
    end

    result.query_ok = ("OK" == get_res.response.result)
    if result.query_ok then
        result.order_id     = result.response.params.orderid
        result.trans_id = result.response.params.transid
        result.open_id  = result.response.params.steamid
        result.currency     = result.response.params.currency
        result.pay_finish   = ("Approved" == result.response.status)  -- Init 支付完成
        --result.items        = {
            -- item_id
            -- iteam_count
            -- amount
            -- pay_finish
        --},
        for _, order_item in pairs(result.response.params.items or {}) do
            tinsert(result.items, {
                item_id = order_item.itemid,
                item_count = order_item.qty,
                amount     = order_item.amount,
                pay_finish = ("Approved" == order_item.status)
            })
        end
        result.insert_time = otime()
    end

    return KernCode.SUCCESS, result
end


-- export
quanta.steam_api = SteamApi()