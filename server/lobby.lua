#!./quanta
import("kernel.lua")
local log_info      = logger.info
local env_addr      = environ.addr
local qxpcall       = quanta.xpcall
local quanta_update = quanta.update
local qxpcall_quit  = quanta.xpcall_quit

quanta.run = function()
    qxpcall(quanta_update, "quanta_update error: %s")
end

if not quanta.init_flag then
    local function startup()
        --初始化quanta
        quanta.init()
        --初始化gm
        quanta.init_gm("center")
        --创建客户端网络管理
        local NetServer = import("kernel/network/net_server.lua")
        local client_mgr = NetServer("lobby")
        local cip, cport = env_addr("QUANTA_LOBBY_ADDR")
        client_mgr:setup(cip, cport, true)
        quanta.client_mgr = client_mgr
        --创建rmsg
        local RmsgMgr = import("kernel/store/rmsg_mgr.lua")
        quanta.rmsg_drop_star = RmsgMgr("rmsg_drop_star")
        quanta.rmsg_settlement = RmsgMgr("rmsg_settlement")
        --初始化lobby
        import("lobby/init.lua")

        log_info("lobby %d now startup!", quanta.id)
    end
    qxpcall_quit(startup, "quanta startup error: %s")
    quanta.init_flag = true
end
