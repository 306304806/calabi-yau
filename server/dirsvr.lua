#!./quanta
import("kernel.lua")
local log_info      = logger.info
local env_addr      = environ.addr
local qxpcall       = quanta.xpcall
local quanta_update = quanta.update
local qxpcall_quit  = quanta.xpcall_quit

quanta.run = function()
    qxpcall(quanta_update, "quanta_update error: %s")
end

if not quanta.init_flag then
    local function startup()
        --初始化quanta
        quanta.init()
        --创建客户端网络管理
        local NetServer = import("kernel/network/net_server.lua")
        local client_mgr = NetServer("dir")
        local cip, cport = env_addr("QUANTA_DIR_ADDR")
        client_mgr:setup(cip, cport, true)
        quanta.client_mgr = client_mgr
        --初始化dirsvr
        import("share/share.lua")
        import("dirsvr/name_servlet.lua")
        import("dirsvr/token_servlet.lua")
        log_info("dirsvr %d now startup!", quanta.id)
    end
    qxpcall_quit(startup, "quanta startup error: %s")
    quanta.init_flag = true
end
