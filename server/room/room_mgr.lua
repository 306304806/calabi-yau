--room_mgr.lua
local pairs         = pairs
local guid_index    = guid.index
local new_guid      = guid.new
local guid_group    = guid.group
local room_type     = guid.room_type
local room_group    = guid.room_group
local sfind         = string.find
local slower        = string.lower
local log_info      = logger.info
local serialize     = logger.serialize
local tinsert       = table.insert
local qtry_call     = quanta.try_call
local sid2name      = service.id2name
local smake_id      = service.make_id
local check_success = utility.check_success

local RoomCode      = enum("RoomCode")
local KernCode      = enum("KernCode")
local GameMode      = enum("GameMode")
local RoomType      = enum("RoomType")
local RoomStatus    = enum("RoomStatus")
local PeriodTime    = enum("PeriodTime")
local TeamStatus    = enum("TeamStatus")
local RoomVisable   = enum("RoomVisable")

local Room          = import("room/room.lua")
local ContestRoom   = import("room/contest.lua")

local SUCCESS       = KernCode.SUCCESS

local timer_mgr     = quanta.get("timer_mgr")
local event_mgr     = quanta.get("event_mgr")
local router_mgr    = quanta.get("router_mgr")
local thread_mgr    = quanta.get("thread_mgr")

local RoomMgr = singleton()
function RoomMgr:__init()
    self.rooms = {}
    self.players = {}

    -- 初始化监听事件
    event_mgr:add_listener(self, "ensure_player")
    event_mgr:add_listener(self, "unsure_player")
    event_mgr:add_listener(self, "rebuild_room_svr")

    -- 关注服务器注册事件
    router_mgr:watch_service_ready(self, "dsagent")

    -- 定时器
    timer_mgr:loop(PeriodTime.SECOND_MS, function()
        self:update()
    end)
end

--创建比赛房间
function RoomMgr:create_contest(player, config)
    local group = room_group(RoomType.CONTEST, GameMode.ROOM)
    local room_id = new_guid(quanta.index, group)
    local try_func = function()
        local ok, code, serial_id = router_mgr:call_center_master("rpc_general_serial_id", room_id)
        if ok and check_success(code) then
            local room = ContestRoom(room_id)
            room:set_serial(serial_id)
            room:set_master(player.player_id)
            room:setup({player}, config)
            self.rooms[room_id] = room
            log_info("[RoomMgr][create_contest] room_id=%s, player=%s, config=%s", room_id, serialize(player), serialize(config))
            return true
        end
    end
    if qtry_call(try_func, 3) then
        return SUCCESS, room_id
    end
    return RoomCode.ROOM_ERR_SERIAL_ID
end

--创建战斗房间
function RoomMgr:create_room(players, config)
    local group = room_group(RoomType.FIGHT, config.mode)
    local room_id = new_guid(quanta.index, group)
    log_info("[RoomMgr][create_room] room_id=(%s), players=%s, config=%s", room_id, serialize(players), serialize(config))
    local room = Room(room_id)
    self.rooms[room_id] = room
    room:setup(players, config)
    room:room_begin()
end

--转换成战斗房间
function RoomMgr:enter_fight(room, old_room_id)
    local group = room_group(RoomType.FIGHT, GameMode.ROOM)
    local room_id = new_guid(quanta.index, group)
    --重建索引
    self.rooms[room_id] = room
    self.rooms[old_room_id] = nil
    room:enter_fight(room_id)
    log_info("[RoomMgr][enter_fight] room_id=(%s->%s)", old_room_id, room_id)
end

--查询房间
function RoomMgr:search_contest(key)
    local contests = {}
    for room_id, room in pairs(self.rooms) do
        if room:get_serial() <= 0 then
            goto continue
        end
        local room_info = room:get_search_info()
        if room_info.visable == RoomVisable.NONE then
            goto continue
        end
        if #key == 0 or sfind(slower(room_info.name), slower(key)) then
            tinsert(contests, room_info)
            goto continue
        end
        if room_info.serial == tonumber(key) then
            tinsert(contests, room_info)
            goto continue
        end
        :: continue ::
    end
    return SUCCESS, contests
end

--转换成自定义房间
function RoomMgr:exit_fight(room, old_room_id, ds_status)
    local group = room_group(RoomType.CONTEST, GameMode.ROOM)
    local room_id = new_guid(quanta.index, group)
    --重建索引
    self.rooms[room_id] = room
    self.rooms[old_room_id] = nil
    room:exit_fight(room_id, ds_status)
    log_info("[RoomMgr][exit_fight] room_id=(%s->%s)", old_room_id, room_id)
end

function RoomMgr:ensure_player(player_id, room_id)
    self.players[player_id] = room_id
    router_mgr:call_index_hash(player_id, "rpc_enter_room", player_id, room_id, quanta.id)
end

function RoomMgr:unsure_player(player_id, room_id, player)
    self.players[player_id] = nil
    router_mgr:call_index_hash(player_id, "rpc_quit_room", player_id, room_id, quanta.id)

    if player and player.team_id then
        local team_id = player.team_id
        local team_sid = smake_id("team", guid_index(team_id))
        router_mgr:call_target(team_sid, "rpc_team_ready", team_id, player_id, TeamStatus.TEAM_NONE)
    end
end

function RoomMgr:get_room(room_id)
    return self.rooms[room_id]
end

function RoomMgr:get_player_room(player_id)
    local room_id = self.players[player_id]
    if not room_id then
        return
    end
    return self.rooms[room_id]
end

function RoomMgr:destory(room_id)
    local room = self.rooms[room_id]
    if room then
        room:destory()
        local serial_id = room:get_serial()
        if serial_id > 0 then
            --回收序列号
            thread_mgr:success_call(PeriodTime.HALF_MS, function()
                local ok, code = router_mgr:call_center_master("rpc_recover_serial_id", serial_id, room_id)
                if ok and check_success(code) then
                    log_info("[RoomMgr][destory] recover success serial_id:%s room_id:%s!",serial_id, room_id)
                    return true
                end
                return false
            end)
        end
        self.rooms[room_id] = nil
        log_info("[RoomMgr][destory] room_id=%s, serial=%s", room_id, room:get_serial())
    end
end

function RoomMgr:update()
    local now = quanta.now
    for room_id, room in pairs(self.rooms) do
        local status = room:get_status()
        local is_time_out = ((now - room:get_create()) > PeriodTime.MINUTE_30_S)
        if status == RoomStatus.CLOSED or (is_time_out and status >= RoomStatus.WAITING) then
            log_info("[RoomMgr][update] room_id=%s timeout, will be destory!", room_id)
            self:destory(room_id)
        end
    end
end

-- 重建处理
function RoomMgr:rebuild_room_svr(dsa_id, room_data)
    local room = Room(room_data.room_id, room_data.serial)
    room:rebuild(room_data)
    self.rooms[room_data.room_id] = room
    log_info("[RoomMgr][rebuild_room_svr_resp] dsa_id:%d, room_id:%s", dsa_id, room_data.room_id)
end

--服务器注册处理
function RoomMgr:on_service_ready(id, service_name)
    log_info("[RoomMgr][on_service_ready] id:%d, group:%s", id, sid2name(id))
    -- 没有数据就不用重建了
    if not next(self.rooms) then
        return
    end

    thread_mgr:fork(function()
        for _, room in pairs(self.rooms) do
            room:replicate_room_to_dsa()
        end
    end)
end

function RoomMgr:get_room_type(room_id)
    local group = guid_group(room_id)
    local r_type = room_type(group)
    return r_type
end

quanta.room_mgr = RoomMgr()

return RoomMgr
