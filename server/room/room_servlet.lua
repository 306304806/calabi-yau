--room_servlet.lua
local tinsert       = table.insert
local guid_index    = guid.index
local smake_id      = service.make_id
local log_debug     = logger.debug
local log_err       = logger.err
local check_success = utility.check_success
local serialize     = logger.serialize

local KernCode      = enum("KernCode")
local SwitchType    = enum("SwitchType")
local RoomStatus    = enum("RoomStatus")
local RoomCode      = enum("RoomCode")
local WatchCode     = enum("WatchCode")
local ReadyStatus   = enum("ReadyStatus")
local TeamStatus    = enum("TeamStatus")
local MemberStatus  = enum("MemberStatus")
local PlayerCode    = enum("PlayerCode")

local CSCmdID       = ncmd_cs.NCmdId
local SUCCESS       = KernCode.SUCCESS

local plat_api      = quanta.get("plat_api")
local room_mgr      = quanta.get("room_mgr")
local event_mgr     = quanta.get("event_mgr")
local router_mgr    = quanta.get("router_mgr")

local RoomServlet = singleton()
function RoomServlet:__init()
    --注册事件
    event_mgr:add_listener(self, "rpc_create_contest")
    event_mgr:add_listener(self, "rpc_search_contest")
    event_mgr:add_listener(self, "rpc_modify_contest")
    event_mgr:add_listener(self, "rpc_destory_contest")
    event_mgr:add_listener(self, "rpc_enter_contest")
    event_mgr:add_listener(self, "rpc_invite_contest")
    event_mgr:add_listener(self, "rpc_quit_contest")
    event_mgr:add_listener(self, "rpc_kick_contest")
    event_mgr:add_listener(self, "rpc_ready_contest")
    event_mgr:add_listener(self, "rpc_begin_contest")
    event_mgr:add_listener(self, "rpc_trans_leader")
    event_mgr:add_listener(self, "rpc_robot_contest")
    event_mgr:add_listener(self, "rpc_switch_contest")
    event_mgr:add_listener(self, "rpc_switch_answer")
    event_mgr:add_listener(self, "rpc_create_room")

    event_mgr:add_listener(self, "rpc_enter_watch")
    event_mgr:add_listener(self, "rpc_exit_watch")

    event_mgr:add_listener(self, "rpc_battle_settle")
    event_mgr:add_listener(self, "rpc_update_status")
    event_mgr:add_listener(self, "rpc_reload_room")
    event_mgr:add_listener(self, "rpc_room_finish")

    event_mgr:add_listener(self, "rpc_trans_player_ds_roles")
    event_mgr:add_listener(self, "rpc_ntf_clients_ds_info")
    event_mgr:add_listener(self, "rpc_ntf_add_weapon_exp")
    event_mgr:add_listener(self, "rpc_achieve_reach")
    event_mgr:add_listener(self, "rpc_battle_update")
end

function RoomServlet:check_member(room_id, player_id)
    local room = room_mgr:get_room(room_id)
    if not room then
        return RoomCode.ROOM_ERR_NOT_EXIST
    end
    local member = room:get_member(player_id)
    if not member then
        return RoomCode.ROOM_ERR_NOT_MEMBER
    end
    return SUCCESS, room, member
end

function RoomServlet:switch_contest(room, member, pos, target_id, pattern)
    if pattern ~= SwitchType.REQUEST then
        if not room:is_master(member.player_id) then
            return RoomCode.ROOM_ERR_NOT_MASTER
        end
    end
    if pattern == SwitchType.LEADER then
        member = room:get_member(target_id)
        if not member then
            return RoomCode.ROOM_ERR_NOT_MEMBER
        end
    end
    if pattern == SwitchType.BLANCE then
        room:member_blance()
        return SUCCESS
    end
    if pos == member.pos then
        return SUCCESS
    end
    if room:out_range(pos) then
        return RoomCode.ROOM_ERR_POS_OUT_RANGE
    end
    if pattern == SwitchType.REQUEST then
        local target = room:get_holder(pos)
        if target and not target.robot then
            room:switch_notify(member.player_id, target.player_id)
            return SUCCESS
        end
    end
    room:member_switch(member.player_id, pos)
end

-----------------------------------------------------------------
function RoomServlet:rpc_create_room(players, config)
    room_mgr:create_room(players, config)
end

function RoomServlet:rpc_create_contest(player, config)
    if room_mgr:get_player_room(player.player_id) then
        return PlayerCode.PLAYER_ERR_AREADY_IN_ROOM
    end
    return room_mgr:create_contest(player, config)
end

function RoomServlet:rpc_search_contest(key)
    return room_mgr:search_contest(key)
end

-- 重新加入
function RoomServlet:rpc_reload_room(room_id, player_id)
    log_debug("[RoomServlet][rpc_reload_room]: (%s, %s)", room_id, player_id)
    local code, room, member = self:check_member(room_id, player_id)
    if not check_success(code) then
        return code
    end
    room:reload_room(player_id, member)
    return SUCCESS
end

function RoomServlet:rpc_modify_contest(room_id, player_id, config)
    log_debug("[RoomServlet][rpc_modify_contest]: (%s, %s, %s)", room_id, player_id, serialize(config))
    local room = room_mgr:get_room(room_id)
    if not room then
        return RoomCode.ROOM_ERR_NOT_EXIST
    end
    if room:get_status() >= RoomStatus.WAITING then
        return RoomCode.ROOM_ERR_IN_FIGHT
    end
    if not room:is_master(player_id) then
        return RoomCode.ROOM_ERR_NOT_MASTER
    end
    room:modify_contest(config)
    return SUCCESS
end

function RoomServlet:rpc_destory_contest(room_id, player_id)
    log_debug("[RoomServlet][rpc_destory_contest]: (%s, %s)", room_id, player_id)
    local room = room_mgr:get_room(room_id)
    if not room then
        return RoomCode.ROOM_ERR_NOT_EXIST
    end
    if not room:is_master(player_id) then
        return RoomCode.ROOM_ERR_NOT_MASTER
    end
    room:destory()
    return SUCCESS
end

function RoomServlet:rpc_enter_contest(room_id, player, passwd)
    log_debug("[RoomServlet][rpc_enter_contest]: (%s, %s, %s)", room_id, serialize(player), passwd)
    local room = room_mgr:get_room(room_id)
    if not room then
        return RoomCode.ROOM_ERR_INFO_CHANGE
    end
    local member = room:get_member(player.player_id)
    if member then
        return RoomCode.ROOM_ERR_IN_ROOM
    end
    if room:get_status() >= RoomStatus.WAITING then
        return RoomCode.ROOM_ERR_IN_FIGHT
    end
    if room:is_real_full() then
        return RoomCode.ROOM_ERR_MEMBER_FULL
    end
    if not room:check_passwd(passwd) then
        return RoomCode.ROOM_ERR_PASSWD_WRONG
    end
    if room:is_full() then
        room:kick_robot()
    end
    room:member_enter(player)
    return SUCCESS, room_id
end

function RoomServlet:rpc_robot_contest(room_id, player_id, role, rank, count, team)
    log_debug("[RoomServlet][rpc_robot_contest]: (%s, %s, %s, %s, %s, %s)", room_id, player_id, role, rank, count, team)
    local room = room_mgr:get_room(room_id)
    if not room then
        return RoomCode.ROOM_ERR_NOT_EXIST
    end
    if room:get_status() >= RoomStatus.WAITING then
        return RoomCode.ROOM_ERR_IN_FIGHT
    end
    if not room:is_master(player_id) then
        return RoomCode.ROOM_ERR_NOT_MASTER
    end
    local can_add, empty = room:get_robot_need(team)
    if not can_add then
        return RoomCode.ROOM_ERR_ROBOT_BAN
    end
    if empty < count then
        count = empty
    end
    if count <= 0 then
        return RoomCode.ROOM_ERR_ROBOT_FULL
    end
    local robot_indexs = room:get_free_robot_indexs(count)
    local ok, robots = router_mgr:call_center_master("rpc_create_robot", count, robot_indexs)
    if not ok then
        return RoomCode.ROOM_ERR_ROBOT_FAILED
    end
    for _, robot in pairs(robots) do
        robot.rank = rank or robot.rank
        room:member_enter(robot)
        room:member_ready(robot.player_id, ReadyStatus.READY)
    end
    return SUCCESS
end

function RoomServlet:rpc_invite_contest(room_id, player, invite_id, pos)
    log_debug("[RoomServlet][rpc_invite_contest]: (%s, %s, %s)", room_id, serialize(player), pos)
    local room = room_mgr:get_room(room_id)
    if not room then
        return RoomCode.ROOM_ERR_NOT_EXIST
    end
    if not room:get_member(invite_id) then
        return RoomCode.ROOM_ERR_INVITE_TIMEOUT
    end
    if room:get_member(player.player_id) then
        return RoomCode.ROOM_ERR_IN_ROOM
    end
    if room:get_status() >= RoomStatus.WAITING then
        return RoomCode.ROOM_ERR_IN_FIGHT
    end
    if room:is_real_full() then
        return RoomCode.ROOM_ERR_MEMBER_FULL
    end
    room:member_enter(player, pos)
    return SUCCESS, room_id
end

function RoomServlet:rpc_quit_contest(room_id, player_id, force)
    log_debug("[RoomServlet][rpc_quit_contest]: (%s, %s, %s)", room_id, player_id, force)
    local code, room = self:check_member(room_id, player_id)
    if not check_success(code) then
        return code
    end
    if not force and room:get_status() >= RoomStatus.WAITING then
        return RoomCode.ROOM_ERR_IN_FIGHT
    end
    room:member_quit(player_id)
    return SUCCESS
end

function RoomServlet:rpc_ready_contest(room_id, player_id, ready)
    log_debug("[RoomServlet][rpc_ready_contest]: (%s, %s, %s)", room_id, player_id, ready)
    local code, room = self:check_member(room_id, player_id)
    if not check_success(code) then
        return code
    end
    if room:get_status() >= RoomStatus.WAITING then
        return RoomCode.ROOM_ERR_IN_FIGHT
    end
    room:member_ready(player_id, ready)
    return SUCCESS
end

function RoomServlet:rpc_begin_contest(room_id, player_id)
    log_debug("[RoomServlet][rpc_begin_contest]: (%s, %s)", room_id, player_id)
    local code, room = self:check_member(room_id, player_id)
    if not check_success(code) then
        return code
    end
    if not room:is_master(player_id) then
        return RoomCode.ROOM_ERR_NOT_MASTER
    end
    room:member_ready(player_id, ReadyStatus.READY)
    if not room:is_all_ready() then
        room:board2client(CSCmdID.NID_MATCH_COMPLETE_NTF, { code = RoomCode.ROOM_ERR_NOT_ALL_READY })
        return RoomCode.ROOM_ERR_NOT_ALL_READY
    end
    if room:get_status() >= RoomStatus.WAITING then
        return RoomCode.ROOM_ERR_IN_FIGHT
    end
    room_mgr:enter_fight(room, room_id)
    return SUCCESS
end

function RoomServlet:rpc_switch_contest(room_id, player_id, pos, target_id, pattern)
    log_debug("[RoomServlet][rpc_switch_contest]: (%s, %s, %s, %s, %s)", room_id, player_id, pos, target_id, pattern)
    local code, room, member = self:check_member(room_id, player_id)
    if not check_success(code) then
        return code
    end
    if room:get_status() >= RoomStatus.WAITING then
        return RoomCode.ROOM_ERR_IN_FIGHT
    end
    self:switch_contest(room, member, pos, target_id, pattern)
    return SUCCESS
end

function RoomServlet:rpc_switch_answer(room_id, player_id, tar_player_id, tar_pos, answer)
    log_debug("[RoomServlet][rpc_switch_answer]: (%s, %s, %s, %s, %s)", room_id, player_id, tar_player_id, tar_pos, answer)
    local code, room = self:check_member(room_id, player_id)
    if not check_success(code) then
        return code
    end
    if room:get_status() >= RoomStatus.WAITING then
        return RoomCode.ROOM_ERR_IN_FIGHT
    end
    local target = room:get_member(tar_player_id)
    if not target then
        return RoomCode.ROOM_ERR_TAR_NOT_MEMBER
    end
    if target.pos ~= tar_pos then
        return RoomCode.ROOM_ERR_POS_WRONG
    end
    room:member_switch(player_id, tar_pos)
    return SUCCESS
end

function RoomServlet:rpc_trans_leader(room_id, player_id, tar_player_id)
    log_debug("[RoomServlet][rpc_trans_leader]: (%s, %s, %s)", room_id, player_id, tar_player_id)
    local code, room = self:check_member(room_id, player_id)
    if not check_success(code) then
        return code
    end
    if not room:is_master(player_id) then
        return RoomCode.ROOM_ERR_NOT_MASTER
    end
    if room:get_status() >= RoomStatus.WAITING then
        return RoomCode.ROOM_ERR_IN_FIGHT
    end
    local target = room:get_member(tar_player_id)
    if not target then
        return RoomCode.ROOM_ERR_TAR_NOT_MEMBER
    end
    if target.robot then
        return RoomCode.ROOM_ERR_ROBOT_LEADER
    end
    room:trans_leader(tar_player_id)
    return SUCCESS
end

function RoomServlet:rpc_kick_contest(room_id, player_id, tar_player_id)
    log_debug("[RoomServlet][rpc_kick_contest]: (%s, %s, %s)", room_id, player_id, tar_player_id)
    local code, room = self:check_member(room_id, player_id)
    if not check_success(code) then
        return code
    end
    if room:get_status() >= RoomStatus.WAITING then
        return RoomCode.ROOM_ERR_IN_FIGHT
    end
    if not room:is_master(player_id) then
        return RoomCode.ROOM_ERR_NOT_MASTER
    end
    if player_id == tar_player_id then
        return KernCode.OPERATOR_SELF
    end
    local target = room:get_member(tar_player_id)
    if not target then
        return RoomCode.ROOM_ERR_TAR_NOT_MEMBER
    end
    room:member_kick(tar_player_id)
    return SUCCESS
end

function RoomServlet:rpc_update_status(room_id, player_id, status)
    log_debug("[RoomServlet][rpc_update_status]->room_id:%s, player_id:%s, status:%s", room_id, player_id, status)
    local room = room_mgr:get_room(room_id)
    if not room then
        -- contest 在结算的时候，room_id改变了，此时ds通知还是原来的room_id
        room = room_mgr:get_player_room(player_id)
        if not room or room:get_old_id() ~= room_id or room:get_status() ~= RoomStatus.READY then
            return RoomCode.ROOM_ERR_NOT_EXIST
        end
        if status == MemberStatus.EXIT then
            if room:get_master() == player_id then
                room:member_ready(player_id, ReadyStatus.READY)
            else
                room:member_ready(player_id, ReadyStatus.NONE)
            end
        end
        return SUCCESS
    end
    room:update_player_status(player_id, status)
    return SUCCESS
end

function RoomServlet:rpc_battle_settle(room_id, players)
    log_debug("[RoomServlet][rpc_battle_settle]->room_id:%s", room_id)
    local room = room_mgr:get_room(room_id)
    if not room then
        return RoomCode.ROOM_ERR_NOT_EXIST
    end
    for _, player_id in pairs(players) do
        room:send2player(player_id, "rpc_player_settle", room_id)
    end
    if room:get_serial() > 0 then
        room_mgr:exit_fight(room, room_id)
    end
    local ok = router_mgr:call_center_master("rpc_record_standings", room_id)
    if not ok then
        log_err("[RoomServlet][rpc_battle_settle] rpc_record_standings not ok")
    end

    local team_members = room:get_team_members(players)
    for team_id, player_ids in pairs(team_members) do
        local team_sid = smake_id("team", guid_index(team_id))
        local readys = {}
        for _, player_id in pairs(player_ids) do
            tinsert(readys, {player_id = player_id, status = TeamStatus.TEAM_SETTLE})
        end
        router_mgr:call_target(team_sid, "rpc_team_batch_ready", team_id, readys)
    end

    self:check_battle_with_friend(room, team_members)

    return SUCCESS
end

-- team_members maybe modify
function RoomServlet:check_battle_with_friend(room, team_members)
    log_debug("[RoomServlet][check_battle_with_friend]->room_id:%s", room.id)
    local player_id_map = {}
    -- 玩家所在队伍人数大于1才处理
    for team_id, player_ids in pairs(team_members) do
        if #player_ids == 1 then
            team_members[team_id] = nil
        else
            for _, player_id in pairs(player_ids) do
                player_id_map[player_id] = true
            end
        end
    end

    local area_vis = {}
    local friendship = {}
    -- 需要去查询每个玩家的好友
    for player_id, _ in pairs(player_id_map) do
        local area_id = guid_index(player_id)

        -- 1）该玩家还没有被查到有好友关系 2）该小区没有查过，每次查询会查所有在该小区的玩家的好友关系
        if not friendship[player_id] and not area_vis[area_id] then
            area_vis[area_id] = true
            local code, tmp_friendship = plat_api:query_players_friendship(area_id, team_members)
            if check_success(code) then
                for _, tmp_player_id in pairs(tmp_friendship) do
                    friendship[tmp_player_id] = true
                end
            else
                log_err("[RoomServlet][check_battle_with_friend] query_players_friendship failed! code = %s", code)
            end
        end
    end

    for player_id, _ in pairs(friendship) do
        room:send2player(player_id, "rpc_battle_with_friend")
    end
end

function RoomServlet:rpc_room_finish(room_id, ds_status)
    log_debug("[RoomServlet][rpc_room_finish]->room_id:%s,ds_status:%s", room_id, ds_status)
    local room = room_mgr:get_room(room_id)
    if not room then
        return RoomCode.ROOM_ERR_NOT_EXIST
    end
    if room:get_serial() == 0 then
        room:destory()
    elseif self.status ~= RoomStatus.READY then
        room_mgr:exit_fight(room, room_id, ds_status)
    end
    room:board2client(CSCmdID.NID_QUIT_BATTLE_NTF, {room_id = room_id, status = ds_status})
    return SUCCESS
end

function RoomServlet:rpc_enter_watch(room_id, watched_id, player_id)
    log_debug("[RoomServlet][rpc_enter_watch]->room_id:%s, watched_id:%s, player_id:%s", room_id, watched_id, player_id)
    local room = room_mgr:get_room(room_id)
    if not room then
        log_err("[RoomServlet][rpc_enter_watch]find room failed!->room_id:%s, watcher:%s", room_id, player_id)
        return WatchCode.WATCH_ERR_ROOM_NOT_EXIST
    end
    if room:get_status() ~= RoomStatus.RUNNING then
        return WatchCode.WATCH_ERR_ROOM_NOT_RUNNING
    end
    local watcher1 = room:get_watcher(watched_id)
    if not watcher1 then
        return WatchCode.WATCH_ERR_PLAYER_OFFLINE
    end
    local watcher2 = room:get_watcher(player_id)
    if watcher2 then
        return WatchCode.WATCH_ERR_AREADY_IN_WATCH
    end
    return room:watch(watched_id, player_id)
end

function RoomServlet:rpc_exit_watch(room_id, watcher_player_id)
    log_debug("[RoomServlet][rpc_exit_watch]->room_id:%s, watcher:%s", room_id, watcher_player_id)
    local code, room = self:check_member(room_id, watcher_player_id)
    if not check_success(code) then
        return code
    end
    room:exit_watch(watcher_player_id)
    return code
end

function RoomServlet:rpc_ntf_add_weapon_exp(report_data)
    log_debug("[RoomServlet][rpc_ntf_add_weapon_exp]->report_data:%s", serialize(report_data))
    for room_id, player_data in pairs(report_data) do
        local room = room_mgr:get_room(room_id)
        if room and next(player_data) then
            for player_id, weapon_exp_data in pairs(player_data) do
                room:send2player(player_id, "rpc_change_weapon_exp", weapon_exp_data)
            end
        end
    end

    return SUCCESS
end

function RoomServlet:rpc_ntf_clients_ds_info(room_id, ds_info)
    log_debug("[RoomServlet][rpc_ntf_clients_ds_info]->room_id:%s, ds_info:%s", room_id, serialize(ds_info))
    local room = room_mgr:get_room(room_id)
    if not room then
        return RoomCode.ROOM_ERR_NOT_EXIST
    end

    room:ntf_client_ds_info(ds_info)

    return SUCCESS
end

-- 传递玩家备战数据
function RoomServlet:rpc_trans_player_ds_roles(room_id, player_id, ds_roles)
    log_debug("[RoomServlet][rpc_trans_player_ds_roles]->room_id:%s, player_id:%s, ds_roles:%s", room_id, player_id, serialize(ds_roles))
    local room = room_mgr:get_room(room_id)
    if room then
        room:build_player_ds_roles(player_id, ds_roles)
    end
    return SUCCESS
end

-- 成就达成
function RoomServlet:rpc_achieve_reach(room_id, achievements)
    log_debug("[RoomServlet][rpc_achieve_reach]->room_id:%s", room_id)
    local room = room_mgr:get_room(room_id)
    if not room then
        return RoomCode.ROOM_ERR_NOT_EXIST
    end
    for _, achive_reach in pairs(achievements) do
        if achive_reach.achievement_ids then
            room:send2player(achive_reach.player_id, "rpc_player_achieve_reach", achive_reach.achievement_ids)
        end
    end
end

-- 战斗数据更新
function RoomServlet:rpc_battle_update(room_id, battle_datas)
    log_debug("[RoomServlet][rpc_battle_update]->room_id:%s", room_id)
    local room = room_mgr:get_room(room_id)
    if not room then
        return RoomCode.ROOM_ERR_NOT_EXIST
    end
    for _, data in pairs(battle_datas) do
        room:send2player(data.player_id, "rpc_player_battle_update", data.battle_evts)
    end
end

quanta.room_servlet  = RoomServlet()

return RoomServlet
