--room.lua
local pairs         = pairs
local tinsert       = table.insert
local new_guid      = guid.new
local guid_index    = guid.index
local log_err       = logger.err
local log_info      = logger.info
local log_warn      = logger.warn
local check_failed  = utility.check_failed

local KernCode      = enum("KernCode")
local RoomCamp      = enum("RoomCamp")
local GameConst     = enum("GameConst")
local ChatType      = enum("ChatType")
local PeriodTime    = enum("PeriodTime")
local RoomStatus    = enum("RoomStatus")
local ReadyStatus   = enum("ReadyStatus")
local MemberStatus  = enum("MemberStatus")

local CSCmdID       = ncmd_cs.NCmdId
local SUCCESS       = KernCode.SUCCESS
local TEAM_SIZE     = GameConst.TEAM_SIZE

local plat_api      = quanta.get("plat_api")
local event_mgr     = quanta.get("event_mgr")
local timer_mgr     = quanta.get("timer_mgr")
local thread_mgr    = quanta.get("thread_mgr")
local router_mgr    = quanta.get("router_mgr")

local Room = class()
local prop = property(Room)
prop:accessor("id", 0)
prop:accessor("old_id", 0)
prop:accessor("master", 0)
prop:accessor("map_id", 0)
prop:accessor("name", "")
prop:accessor("passwd", "")
prop:accessor("version", 0)
prop:accessor("visable", 0)
prop:accessor("ds_info", nil)
prop:accessor("serial", 0)
prop:reader("count", 0)
prop:reader("robot_count", 0)
prop:reader("total", 0)
prop:reader("create", 0)
prop:reader("status", 0)
prop:reader("agent_id", 0)    --ds代理id
prop:reader("players", {})
prop:reader("indexs", {})
prop:reader("watchers", {})
prop:accessor("chat_group_id",nil)  -- 聊天组id
prop:accessor("robot_indexs", {})   -- 机器人索引
prop:accessor("choose_time", 0)   -- 选英雄时间

function Room:__init(id)
    self.id     = id
    self.time   = 0
    self.create = quanta.now
end

function Room:is_real_full()
    return (self.count == self.total and self.robot_count == 0)
end

function Room:is_full()
    return (self.count == self.total)
end

function Room:check_passwd(passwd)
    local cur_passwd = self.passwd
    if cur_passwd and #cur_passwd > 0 then
        return cur_passwd == passwd
    end
    return true
end

function Room:out_range(pos)
    return pos <= 0 or pos > TEAM_SIZE * 2
end

function Room:is_master(player_id)
    return player_id == self.master
end

function Room:get_master_name()
    local member = self:get_member(self.master)
    return member and member.nick
end

function Room:is_all_ready()
    for _, player in pairs(self.players) do
        if player.ready ~= ReadyStatus.READY then
            return false
        end
    end
    return true
end

function Room:is_all_robot()
    for _, player in pairs(self.players) do
        if not player.robot then
            return false
        end
    end
    return true
end

function Room:setup(players, config)
    for _, player in pairs(players) do
        if self.master == 0 then
            self.master = player.player_id
        end
        self:build_member(player)
        self:add_member(player)
    end
    self.total = TEAM_SIZE * 2
    self.map_id = config.map_id
    self.name = config.name or ""
    self.passwd = config.passwd or ""
    self.version = config.version
    self.visable = config.visable or 0
    self.choose_time = config.choose_time
    self.status = RoomStatus.READY
    self:create_chat_group()
    self:on_room_create()
end

function Room:update(escape)
    self.time = self.time + escape
    if self.time >= self.choose_time * 1000 then
        self:room_open()
    end
end

--房间成员
function Room:pack_member(player)
    return {
        sex         = player.sex,
        pos         = player.pos,
        rank        = player.rank,
        nick        = player.nick,
        icon        = player.icon,
        level       = player.level,
        campid      = player.campid,
        status      = player.status,
        player_id   = player.player_id,
        robot       = player.robot and true or false,
    }
end

--聊天成员
function Room:pack_chat_member(player)
    return {
        nick        = player.nick,
        icon        = player.icon,
        team_id     = player.campid,
        player_id   = player.player_id,
    }
end

-- 成员player_id列表
function Room:get_member_ids()
    local ids = {}
    for player_id, player in pairs(self.players or {}) do
        if not player.robot then
            tinsert(ids, player_id)
        end
    end
    return ids
end

-- 获取制定阵营的成员列表
function Room:get_member_ids_by_camp(campid)
    local ids = {}
    for player_id, player in pairs(self.players or {}) do
        if not player.robot and player.campid == campid then
            tinsert(ids, player_id)
        end
    end
    return ids
end

-- 获取所有team成员
function Room:get_team_members(players)
    local vis = {}
    for _, player_id in pairs(players) do
        vis[player_id] = true
    end
    local team_members = {}
    for _, player in pairs(self.players or {}) do
        if not player.robot and player.team_id and vis[player.player_id] then
            local members = team_members[player.team_id] or {}
            tinsert(members, player.player_id)
            team_members[player.team_id] = members
        end
    end
    return team_members
end

--序列化
function Room:serialize()
    local room_info = {
        players = {},
        room_id = self.id,
        total = self.total,
        map_id = self.map_id,
        timeout = (self.choose_time - self.time // 1000),
    }
    for _, player in pairs(self.players) do
        local player_info = self:pack_member(player)
        player_info.vc_achie_id = player.vc_achie_id
        player_info.vc_frame_id = player.vc_frame_id
        player_info.vc_avatar_id = player.vc_avatar_id
        player_info.vc_border_id = player.vc_border_id
        tinsert(room_info.players, player_info)
    end
    return room_info
end

--序列化给ds
function Room:serialize2ds()
    local room_info = {
        players     = {},
        room_id     = self.id,
        map_id      = self.map_id,
        version     = self.version,
    }
    for _, player in pairs(self.players) do
        --分配英雄
        local data = self:pack_member(player)
        data.token = player.token
        data.ds_roles = player.ds_roles
        tinsert(room_info.players, data)
    end
    return room_info
end

--房间开始
function Room:room_begin()
    self.time = 0
    self.status = RoomStatus.WAITING
    local function on_timer(escape)
        self:update(escape)
    end
    self.timer_id = timer_mgr:register(PeriodTime.SECOND_MS, PeriodTime.SECOND_MS, self.choose_time, on_timer)
    --通知匹配完成
    local room_info = self:serialize()
    room_info.code = SUCCESS

    self:board2client(CSCmdID.NID_MATCH_COMPLETE_NTF, room_info)
end

function Room:room_open()
    if self.status ~= RoomStatus.WAITING then
        return
    end
    timer_mgr:unregister(self.timer_id)
    self.status = RoomStatus.RUNNING
    local ok, code, agent_id = router_mgr:call_dscenter_master("rpc_search_ds_agent", self.id)
    if not ok or check_failed(code) then
        log_err("[Room][room_open] rpc_search_ds_agent failed:%s", ok and agent_id or code)
        self:board2client(CSCmdID.NID_ENTER_BATTLE_NTF, {code = ok and code or KernCode.LOGIC_FAILED})
        self:destory()
        return
    end
    self.agent_id = agent_id
    self:create_ds()
end

function Room:create_ds()
    local room_info = self:serialize2ds()
    local ok, code, ds_info = router_mgr:call_target(self.agent_id, "rpc_create_ds", quanta.id, room_info)
    if not ok or check_failed(code) then
        log_err("[Room][create_ds] create_ds_req failed:%s", ok and ds_info or code)
        self:board2client(CSCmdID.NID_ENTER_BATTLE_NTF, {code = ok and code or KernCode.LOGIC_FAILED})
        self:destory()
        return
    end
    --通知一起战斗
    plat_api:build_recent_friend(self:get_member_ids())
end

function Room:ntf_client_ds_info(ds_info)
    ds_info.code = SUCCESS
    ds_info.room_id = self.id
    for player_id, player in pairs(self.players) do
        if not player.robot then
            ds_info.token = player.token
            self:send2client(player_id, CSCmdID.NID_ENTER_BATTLE_NTF, ds_info)
        end
        player.status = MemberStatus.WAIT_ENTER
    end
    self.ds_info = ds_info
    self.status = RoomStatus.RUNNING

    -- 通知dsagent拷贝一份房间数据
    self:replicate_room_to_dsa()
end

function Room:update_player_status(player_id, status)
    local player = self.players[player_id]
    if player and player.status ~= status then
        player.status = status
        if MemberStatus.EXIT == player.status then
            self:del_member(player_id)
        end
    end
end

function Room:destory()
    if self.status == RoomStatus.CLOSED then
        return
    end
    self:on_room_destory()
    for player_id, player in pairs(self.players) do
        if not player.robot then
            event_mgr:notify_listener("unsure_player", player_id, self.id, player)
        end
    end
    self:destory_chat_group()
    self.status = RoomStatus.CLOSED
end

-- 重置玩家状态
function Room:reset_member(player, ready_status)
    player.status = MemberStatus.READY
    player.ready = player.robot and ReadyStatus.READY or ready_status
end

function Room:init_member(player, pos, campid)
    if self.indexs[pos] then
        return false
    end
    player.pos = pos
    player.campid = campid
    player.status = player.status or MemberStatus.READY
    if not player.ready then
        player.ready = self:is_master(player.player_id) and ReadyStatus.READY or ReadyStatus.NONE
    end
    self.indexs[pos] = player
    return true
end

function Room:build_member(player, force)
    for pos = 1, TEAM_SIZE do
        for campid = RoomCamp.CAMP_1, RoomCamp.CAMP_2 do
            if force or not player.campid or player.campid == campid then
                local index = (campid - 1) * TEAM_SIZE + pos
                if self:init_member(player, index, campid) then
                    return
                end
            end
        end
    end
end

function Room:add_member(player)
    local player_id = player.player_id
    self.players[player_id] = player
    self.count = self.count + 1
    if not player.robot then
        player.token = new_guid(quanta.index, quanta.group)
        event_mgr:notify_listener("ensure_player", player_id, self.id)
    else
        self.robot_count = self.robot_count + 1
        local robot_indexs = self:get_free_robot_indexs(1)
        self.robot_indexs[robot_indexs[1]] = player_id
    end
    self:add_chat_group_member(player)
end

function Room:del_member(player_id)
    local player = self.players[player_id]
    if player then
        self.players[player_id] = nil
        self.count = self.count - 1
        local index = player.pos
        if index then
            self.indexs[index] = nil
        end
        if not player.robot then
            event_mgr:notify_listener("unsure_player", player_id, self.id, player)
        else
            self.robot_count = self.robot_count - 1
            for i = 1, TEAM_SIZE * 2, 1 do
                if self.robot_indexs[i] == player_id then
                    self.robot_indexs[i] = nil
                    break
                end
            end
        end
        self:del_chat_group_member(player_id)
    end
end

function Room:get_member(player_id)
    return self.players[player_id]
end

function Room:get_holder(pos)
    return self.indexs[pos]
end

-- 玩家备战数据
function Room:build_player_ds_roles(player_id, ds_roles)
    local player = self:get_member(player_id)
    if player then
        player.ds_roles = ds_roles
    end
end

-- 断线重新进入房间
function Room:reload_room(player_id, player)
    --通知客户端
    local room_info = self:serialize()
    room_info.code = SUCCESS
    self:send2client(player_id, CSCmdID.NID_MATCH_COMPLETE_NTF, room_info)
    if self.ds_info then
        local ds_info = self.ds_info
        ds_info.token = player.token
        ds_info.reconnect = true
        self:send2client(player_id, CSCmdID.NID_ENTER_BATTLE_NTF, ds_info)
    end
end

function Room:send2client(player_id, ...)
    router_mgr:send_index_hash(player_id, "transfer_message", player_id, "forward_player", player_id, ...)
end

function Room:send2player(player_id, rpc, ...)
    router_mgr:send_index_hash(player_id, "transfer_message", player_id, rpc, player_id, ...)
end

function Room:board2client(...)
    for player_id, player in pairs(self.players) do
        if not player.robot then
            router_mgr:send_index_hash(player_id, "transfer_message", player_id, "forward_player", player_id, ...)
        end
    end
end

function Room:get_watcher(watched_id)
    return self.watchers[watched_id]
end

function Room:watch(watched_id, player_id)
    local real_watch_id = watched_id
    if self.watchers[watched_id] then
        real_watch_id = self.watchers[watched_id]
    end
    -- 通知dsa进入观战
    local ok, ec, loading_data = router_mgr:call_target(self.agent_id, "rpc_enter_ds_watch", self.id, real_watch_id, player_id, self.ds_info.token)
    if not ok or ec ~= SUCCESS then
        return ec
    end
    self.watchers[player_id] = real_watch_id
    local result = {}
    result.players = {}
    result.map_id  = self.map_id
    result.ds_info = self.ds_info
    for _, player in pairs(self.players) do
        local temp = self:pack_member(player)
        temp.team_index = loading_data[player.player_id] or 0
        tinsert(result.players, temp)
    end
    event_mgr:notify_listener("ensure_player", player_id, self.id)
    return SUCCESS, result
end

function Room:exit_watch(player_id)
    if self.watchers[player_id] then
        self.watchers[player_id] = nil
        event_mgr:notify_listener("unsure_player", player_id, self.id)
        self:send2player(player_id, "rpc_lobby_exit_watch", player_id)
    end
end

-- 数据重建
function Room:rebuild(room_data)
    --self.id        = room_data.id
    self.old_id    = room_data.old_id
    self.master    = room_data.master
    self.map_id    = room_data.map_id
    self.name      = room_data.name
    self.passwd    = room_data.passwd
    self.version   = room_data.version
    self.visable   = room_data.visable
    self.ds_info   = room_data.ds_info
    self.serial    = room_data.serial
    self.count     = room_data.count
    self.total     = room_data.total
    self.create    = room_data.create
    self.status    = room_data.status
    self.agent_id  = room_data.agent_id
    self.players   = room_data.players
    self.indexs    = room_data.indexs
    self.watchers  = room_data.watchers
    self.chat_group_id = room_data.chat_group_id
    self.robot_count   = room_data.robot_count
    self.robot_indexs  = room_data.robot_indexs
    log_info("[Room][rebuild] room_id:%s", self.id)
end

--------------------------------------------------------------
--子类实现

-- 房间创建回调
function Room:on_room_create()
end

-- 房间销毁回调
function Room:on_room_destory()
end

function Room:member_enter(player, pos)
end

function Room:member_quit(player_id)
    self:del_member(player_id)
end

function Room:member_ready(player_id, ready)
end

function Room:member_kick(tar_player_id)
end

function Room:modify_contest(config)
end

function Room:trans_leader(tar_player_id)
end

function Room:member_blance()
end

function Room:kick_robot()
end

function Room:get_free_robot_indexs(count)
    local free_indexs = {}
    for pos = 1, TEAM_SIZE * 2 do
        if not self.robot_indexs[pos] then
            tinsert(free_indexs, pos)
            count = count - 1
            if count == 0 then
                return free_indexs
            end
        end
    end
end

-------------------------------------------------------
-- 创建聊天群
function Room:create_chat_group()
    local members = {}
    for _, player in pairs(self.players) do
        tinsert(members, self:pack_chat_member(player))
    end
    -- 根据阵营信息，生成频道信息列表
    local area_id = guid_index(self.master)
    local rpc_req = { group_type = ChatType.ROOM, members = members }
    thread_mgr:success_call(PeriodTime.HALF_MS, function()
        -- 根据阵营信息，生成频道信息列表
        local code, rpc_res = plat_api:create_chat_group(area_id, rpc_req)
        if check_failed(code) then
            log_warn("[Room][create_chat_group] plat call faild: code=%s,room_id=%s", code, self.id)
            return false
        end
        local group_id = rpc_res.group_id
        log_info("[Room][create_chat_group] room_id=%s, group_id=%s", self.id, group_id)
        self:set_chat_group_id(group_id)
        return true
    end)
end

-- 销毁聊天群
function Room:destory_chat_group()
    -- 从群id获取小区id
    local group_id = self:get_chat_group_id()
    if not group_id then
        log_warn("[Room][destory_chat_group] no group_id find !")
        return
    end
    local area_id = guid_index(group_id)
    local rpc_req = {group_id = group_id}
    thread_mgr:success_call(PeriodTime.HALF_MS, function()
        local code = plat_api:destory_chat_group(area_id, rpc_req)
        if check_failed(code) then
            log_warn("[Room][destory_chat_group] plat call faild: code=%s,group_id=%s", code, group_id)
            return false
        end
        log_info("[Room][destory_chat_group] room_id=%s, group_id=%s", self.id, group_id)
        return true
    end)
end

-- 添加玩家到群聊
function Room:add_chat_group_member(player)
    -- 从群id获取小区id
    local group_id = self:get_chat_group_id()
    if not group_id then
        return
    end
    -- 公共
    local area_id = guid_index(group_id)
    local member = self:pack_chat_member(player)
    local rpc_req = {area_id = area_id, group_id = group_id, member = member }
    thread_mgr:success_call(PeriodTime.HALF_MS, function()
        local code = plat_api:add_chat_group_member(area_id, rpc_req)
        if check_failed(code) then
            log_warn("[Room][add_chat_group_member] plat call faild: code=%s,room_id=%s,player_id=%s", code, self.id, member.player_id)
            return false
        end
        log_info("[Room][add_chat_group_member] room_id=%s, group_id=%s, player_id=%s", self.id, group_id, member.player_id)
        return true
    end)
end

-- 玩家移出群聊
function Room:del_chat_group_member(player_id)
    -- 从群id获取小区id
    local group_id = self:get_chat_group_id()
    if not group_id then
        log_warn("[Room][del_chat_group_member] no group_id find !")
        return
    end
    local area_id = guid_index(group_id)
    local rpc_req = {area_id = area_id, group_id = group_id, player_id = player_id}
    thread_mgr:success_call(PeriodTime.HALF_MS, function()
        local code = plat_api:del_chat_group_member(area_id, rpc_req)
        if check_failed(code) then
            log_warn("[Room][del_chat_group_member] plat call faild: code=%s,room_id=%s,player_id=%s", code, self.id, player_id)
            return false
        end
        log_info("[Room][del_chat_group_member] room_id=%s, group_id=%s, player_id=%s", self.id, group_id, player_id)
        return true
    end)
end

-- 玩家群聊队伍更新
function Room:update_chat_group_member(player_id, team_id)
    -- 从群id获取小区id
    local group_id = self:get_chat_group_id()
    if not group_id then
        log_warn("[Room][update_chat_group_member] no group_id find !")
        return
    end
    local area_id = guid_index(group_id)
    local rpc_req = { group_id = group_id, player_id = player_id, team_id = team_id }
    thread_mgr:success_call(PeriodTime.HALF_MS, function()
        local code = plat_api:update_chat_group_member(area_id, rpc_req)
        if check_failed(code) then
            log_warn("[Room][update_chat_group_member] plat call faild: code=%s,group_id=%s,player_id=%s", code, group_id, player_id)
            return false
        end
        log_info("[Room][update_chat_group_member] room_id=%s, group_id=%s, player_id=%s", self.id, group_id, player_id)
        return true
    end)
end

function Room:replicate_room_to_dsa()
    local room_copy = {}
    room_copy.id        = self.id
    room_copy.time      = self.time
    room_copy.create    = self.create
    room_copy.old_id    = self.old_id
    room_copy.master    = self.master
    room_copy.map_id    = self.map_id
    room_copy.name      = self.name
    room_copy.passwd    = self.passwd
    room_copy.version   = self.version
    room_copy.visable   = self.visable
    room_copy.ds_info   = self.ds_info
    room_copy.serial    = self.serial
    room_copy.count     = self.count
    room_copy.total     = self.total
    room_copy.create    = self.create
    room_copy.status    = self.status
    room_copy.agent_id  = self.agent_id
    room_copy.players   = self.players
    room_copy.indexs    = self.indexs
    room_copy.watchers  = self.watchers
    room_copy.chat_group_id = self.chat_group_id
    room_copy.robot_count   = self.robot_count
    room_copy.robot_indexs  = self.robot_indexs
    router_mgr:call_target(self.agent_id, "rpc_replicate_room_data", room_copy)
end

return Room
