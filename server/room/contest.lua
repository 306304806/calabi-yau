--room.lua
local pairs         = pairs
local mmin          = math.min
local mrandom       = math.random
local tinsert       = table.insert
local ncmdid        = ncmd_cs.NCmdId

local ExitReason    = enum("ExitReason")
local DsStatus      = enum("DsStatus")
local RoomStatus    = enum("RoomStatus")
local ReadyStatus   = enum("ReadyStatus")
local GameConst     = enum("GameConst")
local MemberStatus  = enum("MemberStatus")
local RoomCamp      = enum("RoomCamp")

local TEAM_SIZE     = GameConst.TEAM_SIZE

local event_mgr     = quanta.get("event_mgr")
local config_mgr    = quanta.get("config_mgr")
local map_db        = config_mgr:init_table("mapcfg", "id")

local Room          = import("room/room.lua")

local ContestRoom = class(Room)
local prop = property(ContestRoom)
prop:accessor("search_info", nil)

function ContestRoom:__init(id)
end

function ContestRoom:pack_info(search)
    return {
        room_id     = self.id,
        total       = self.total,
        serial      = self.serial,
        master      = self.master,
        name        = self.name,
        map_id      = self.map_id,
        visable     = self.visable,
        passkey     = (#self.passwd > 0),
        mname       = self:get_master_name(),
        passwd      = search and "" or self.passwd,
        count       = self.count - self.robot_count,
    }
end

function ContestRoom:pack_room()
    return { room = self:pack_info()}
end

--单个玩家信息
function ContestRoom:pack_player(player)
    if player.player_id > 0 then
        return {
            pos         = player.pos,
            nick        = player.nick,
            icon        = player.icon,
            sex         = player.sex,
            level       = player.level,
            ready       = player.ready,
            rank        = player.rank,
            robot       = player.robot,
            player_id   = player.player_id,
            vc_avatar_id= player.vc_avatar_id,
            vc_frame_id = player.vc_frame_id,
            vc_border_id= player.vc_border_id,
            vc_achie_id = player.vc_achie_id,
        }
    end
    return player
end

--所有玩家信息
function ContestRoom:pack_members()
    local members = {
        room_id = self.id,
        players = {}
    }
    --玩家
    for _, player in pairs(self.indexs) do
        tinsert(members.players, self:pack_player(player))
    end
    return members
end

--玩家更新信息
function ContestRoom:pack_update(member, reason)
    return {
        room_id = self.id,
        player = self:pack_player(member),
        reason = reason or ExitReason.NONE
    }
end

--查询新队长
function ContestRoom:find_leader(player_id)
    for tuid in pairs(self.players) do
        if tuid ~= player_id then
            return tuid
        end
    end
end

-- 房间创建成功
function ContestRoom:on_room_create()
    self:send2client(self.master, ncmdid.NID_ROOM_INFO_NTF, self:pack_room())
    self:send2client(self.master, ncmdid.NID_ROOM_MEMBERS_NTF, self:pack_members())
    self.search_info = self:pack_info(true)
end

-- 进入战斗房间
function ContestRoom:enter_fight(room_id)
    --更新玩家信息
    for player_id, player in pairs(self.players) do
        if not player.robot then
            event_mgr:notify_listener("ensure_player", player_id, room_id)
        end
    end
    self.old_id = self.id
    self.id = room_id
    self.create = quanta.now
    --更新房间列表信息
    self.search_info = self:pack_info(true)
    --开始战斗
    self:room_begin()
end

-- 退出战斗房间
function ContestRoom:exit_fight(room_id, ds_status)
    --更新玩家信息并同步
    self.old_id = self.id
    self.id     = room_id
    self.status = RoomStatus.READY
    self.create = quanta.now
    local ready_status = ds_status == DsStatus.CRASH and ReadyStatus.NONE or ReadyStatus.SETTLE
    for player_id, player in pairs(self.players) do
        self:reset_member(player, ready_status)
    end
    --更新房间列表信息
    self.search_info = self:pack_info(true)
    for player_id, player in pairs(self.players) do
        if not player.robot then
            event_mgr:notify_listener("ensure_player", player_id, room_id)
            self:send2client(player_id, ncmdid.NID_ROOM_INFO_NTF, self:pack_room())
            self:send2client(player_id, ncmdid.NID_ROOM_MEMBERS_NTF, self:pack_members())
        end
    end
end

-- 重新加载房间
function ContestRoom:reload_room(player_id, member)
    self:member_ready(player_id, ReadyStatus.READY)
    self:send2client(player_id, ncmdid.NID_ROOM_INFO_NTF, self:pack_room())
    self:send2client(player_id, ncmdid.NID_ROOM_MEMBERS_NTF, self:pack_members())
    if self.status >= RoomStatus.WAITING then
        Room.reload_room(self, player_id, member)
    end
end

-- 房间销毁
function ContestRoom:on_room_destory()
    local res = { room = {room_id = self.id, serial = 0 } }
    self:board2client(ncmdid.NID_ROOM_INFO_NTF, res)
end

-- 成员进入
function ContestRoom:member_enter(player, pos)
    local binit = false
    if pos and pos > 0 then
        --先尝试指定位置
        binit = self:init_member(player, pos, (pos - 1) // TEAM_SIZE + 1)
    end
    if not binit then
        self:build_member(player)
    end
    --同步玩家信息
    self:board2client(ncmdid.NID_ROOM_MEMBER_NTF, self:pack_update(player))
    self:add_member(player)
    --同步房间信息
    self:send2client(player.player_id, ncmdid.NID_ROOM_INFO_NTF, self:pack_room())
    self:send2client(player.player_id, ncmdid.NID_ROOM_MEMBERS_NTF, self:pack_members())
    if not player.robot then
        self.search_info.count = self.count - self.robot_count
    end
end

-- 成员平衡
function ContestRoom:member_blance()
    self.indexs = {}
    local players = {}
    for _, player in pairs(self.players) do
        tinsert(players, mrandom(#players + 1), player)
    end
    for _, player in pairs(players) do
        self:build_member(player, true)
    end
    self:board2client(ncmdid.NID_ROOM_MEMBERS_NTF, self:pack_members())
end

-- 踢一个机器人
function ContestRoom:kick_robot(count)
    count = count or 1
    local camp_robots = {}
    for _, player in pairs(self.players) do
        if player.robot then
            local robots = camp_robots[player.campid] or {ids = {}, count = 0}
            tinsert(robots.ids, player.player_id)
            robots.count = robots.count + 1
            camp_robots[player.campid] = robots
        end
    end

    while count > 0 do
        local campid
        for id = RoomCamp.CAMP_1, RoomCamp.CAMP_2 do
            if not campid or camp_robots[campid].count < camp_robots[id].count then
                campid = id
            end
        end
        local robots = camp_robots[campid]
        if robots.count <= 0 then
            break
        end
        local player_id = robots.ids[robots.count]
        self:del_member(player_id)
        robots.count = robots.count - 1
        count = count - 1
    end

    self:board2client(ncmdid.NID_ROOM_MEMBERS_NTF, self:pack_members())
end

-- 成员退出
function ContestRoom:member_quit(player_id)
    if (self.count - self.robot_count) == 1 then
        self:destory()
        return
    end
    --同步玩家信息
    local player = self:get_member(player_id)
    local holder = {pos = player.pos, player_id = 0}
    self:board2client(ncmdid.NID_ROOM_MEMBER_NTF, self:pack_update(holder, ExitReason.EXIT))
    self:del_member(player_id)
    if player_id == self.master then
        --查找新队长转让
        local leader_id = self:find_leader(player_id)
        if leader_id then
            self:trans_leader(leader_id)
        end
    end
    self.search_info.count = self.count - self.robot_count
end

-- 踢掉成员
function ContestRoom:member_kick(player_id)
    --同步玩家信息
    local player = self:get_member(player_id)
    local holder = {pos = player.pos, player_id = 0}
    self:board2client(ncmdid.NID_ROOM_MEMBER_NTF, self:pack_update(holder, ExitReason.KICK))
    self:del_member(player_id)
    self.search_info.count = self.count - self.robot_count
end

-- 成员准备
function ContestRoom:member_ready(player_id, ready)
    local player = self:get_member(player_id)
    if player.ready ~= ready then
        player.ready = ready
        --同步玩家信息
        self:board2client(ncmdid.NID_ROOM_MEMBER_NTF, self:pack_update(player))
    end
end

--修改房间信息
function ContestRoom:modify_contest(config)
    self.name = config.name
    self.map_id = config.map_id
    self.passwd = config.passwd
    self.visable = config.visable
    self.search_info = self:pack_info(true)
    self:board2client(ncmdid.NID_ROOM_INFO_NTF, self:pack_room())
    local cfg_item = map_db:find_one(self.map_id)
    local kick_count = self.robot_count - cfg_item.bots
    if kick_count > 0 then
        self:kick_robot(kick_count)
    end
end

--转让队长
function ContestRoom:trans_leader(tar_player_id)
    --同步房间信息
    self.master = tar_player_id
    self:board2client(ncmdid.NID_ROOM_INFO_NTF, self:pack_room())
    local player = self:get_member(tar_player_id)
    if player.ready == ReadyStatus.NONE then
        self:member_ready(tar_player_id, ReadyStatus.READY)
    end
    --更新房间列表信息
    self.search_info = self:pack_info(true)
end

-- 交换位置
function ContestRoom:member_switch(player_id, pos)
    local player = self.players[player_id]
    local target = self.indexs[pos]
    local player_campid = player.campid
    if target then
        if target.campid ~= player_campid then
            self:update_chat_group_member(target.player_id, player_campid)
        end
        target.pos = player.pos
        target.campid = player_campid
        self.indexs[player.pos] = target
        self:board2client(ncmdid.NID_ROOM_MEMBER_NTF, self:pack_update(target))
    else
        self.indexs[player.pos] = nil
        local holder = {pos = player.pos, player_id = 0}
        self:board2client(ncmdid.NID_ROOM_MEMBER_NTF, self:pack_update(holder))
    end
    local new_campid = pos > TEAM_SIZE and 2 or 1
    if new_campid ~= player_campid then
        self:update_chat_group_member(player_id, new_campid)
    end
    player.pos = pos
    player.campid = new_campid
    self.indexs[pos] = player
    self:board2client(ncmdid.NID_ROOM_MEMBER_NTF, self:pack_update(player))
end

--通知交换
function ContestRoom:switch_notify(player_id, tar_player_id)
    local player = self.players[player_id]
    local notify = { room_id = self.id, player = self:pack_player(player) }
    self:send2client(tar_player_id, ncmdid.NID_ROOM_SWITCH_NTF, notify)
end

function ContestRoom:update_player_status(player_id, status)
    local player = self.players[player_id]
    if player and player.status ~= status then
        player.status = status
        if MemberStatus.EXIT == player.status then
            self:member_quit(player_id)
        end
    end
end

function ContestRoom:get_robot_need(camp)
    local cfg_item = map_db:find_one(self.map_id)
    if cfg_item.bots == 0 then
        return false, 0
    end
    local free_pos
    if camp == RoomCamp.CAMP_0 then
        free_pos = self.total - self.count
    else
        local count = 0
        for _, player in pairs(self.players or {}) do
            if player.campid == camp then
                count = count + 1
            end
        end
        free_pos = TEAM_SIZE - count
    end
    return true, mmin(cfg_item.bots - self.robot_count, free_pos)
end

return ContestRoom
