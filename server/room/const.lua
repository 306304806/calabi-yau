--const.lua

-- DS状态枚举
local DsStatus = enum("DsStatus", 0)
DsStatus.CREATE                 = 0 -- 创建DS
DsStatus.RUN                    = 1 -- DS运行
DsStatus.EXIT                   = 2 -- DS退出
DsStatus.UNCONNECT              = 3 -- DS 未连接
DsStatus.CRASH                  = 4 -- DS Crash
DsStatus.SETTLE                 = 5 -- DS结算

--房间状态
local RoomStatus = enum("RoomStatus", 0)
RoomStatus.READY        = 1
RoomStatus.WAITING      = 2
RoomStatus.RUNNING      = 3
RoomStatus.ENDING       = 4
RoomStatus.CLOSED       = 5

--房间阵营
local RoomCamp = enum("RoomCamp", 0)
RoomCamp.CAMP_0         = 0
RoomCamp.CAMP_1         = 1
RoomCamp.CAMP_2         = 2

--交互位置类型
local SwitchType = enum("SwitchType", 0)
SwitchType.REQUEST      = 0     --请求换队
SwitchType.LEADER       = 1     --队长调整
SwitchType.BLANCE       = 2     --队长平衡调整

--退出原因
local ExitReason = enum("ExitReason", 0)
ExitReason.NONE  = 0
ExitReason.EXIT  = 1    --主动退出
ExitReason.KICK  = 2    --踢出队伍
