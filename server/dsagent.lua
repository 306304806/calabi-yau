#!./quanta
import("kernel.lua")
local log_info      = logger.info
local env_addr      = environ.addr
local qxpcall       = quanta.xpcall
local quanta_update = quanta.update
local qxpcall_quit  = quanta.xpcall_quit

quanta.run = function()
    qxpcall(quanta_update, "quanta_update error: %s")
end

if not quanta.init_flag then
    local function startup()
        --初始化quanta
        quanta.init()
        --创建客户端网络管理
        local NetServer = import("kernel/network/net_server.lua")
        local client_mgr = NetServer("dsa")
        local cip, cport = env_addr("QUANTA_DSA_ADDR")
        client_mgr:setup(cip, cport)
        quanta.client_mgr = client_mgr
        --可靠消息
        local RmsgMgr = import("kernel/store/rmsg_mgr.lua")
        quanta.rmsg_settlement = RmsgMgr("rmsg_settlement")
        quanta.rmsg_standings = RmsgMgr("rmsg_standings")
        --初始化dsagent
        import("share/share.lua")
        import("dsagent/init.lua")
        log_info("dsagent %d now startup!", quanta.id)
    end
    qxpcall_quit(startup, "quanta startup error: %s")
    quanta.init_flag = true
end
