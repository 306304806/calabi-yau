--name_servlet.lua
local ljson         = require("luacjson")
local mhuge         = math.huge
local log_err       = logger.err
local log_info      = logger.info
local env_get       = environ.get
local json_decode   = ljson.decode
local random_array  = table_ext.random_array
local check_failed  = utility.check_failed

local DirCode       = enum("DirCode")
local KernCode      = enum("KernCode")
local PeriodTime    = enum("PeriodTime")
local CSCmdID       = ncmd_cs.NCmdId
local SUCCESS       = KernCode.SUCCESS

local timer_mgr     = quanta.get("timer_mgr")
local event_mgr     = quanta.get("event_mgr")
local router_mgr    = quanta.get("router_mgr")
local thread_mgr    = quanta.get("thread_mgr")

-- 名字服务
local NameServlet = singleton()
local prop = property(NameServlet)
prop:accessor("platform_url", nil)
prop:accessor("areas", {})
prop:accessor("id2lobby", {})
prop:accessor("pid2lobbys", {})
prop:accessor("gate_clients", {})
prop:accessor("gate_servers", {})
function NameServlet:__init()
    --rpc消息
    event_mgr:add_listener(self, "rpc_lobby_report")
    event_mgr:add_listener(self, "rpc_platform_gateway")
    -- 事件监听
    event_mgr:add_listener(self, "on_session_cmd")
    event_mgr:add_listener(self, "on_session_err")
    --服务发现
    router_mgr:watch_service_close(self, "lobby")
    --地址
    self.platform_url = env_get("QUANTA_PLATWEB_ADDR", "127.0.0.1:9501") .. "/platform_addr"
    --启动拉取信息
    thread_mgr:success_call(PeriodTime.SECOND_MS, function()
        return self:load_address_info()
    end)
    --定时拉取信息
    timer_mgr:loop(PeriodTime.MINUTE_MS, function()
        self:load_address_info()
    end)
end

-- 拉取最新的小区列表
function NameServlet:load_address_info()
    local ok, code, post_res = router_mgr:call_proxy_hash(quanta.id, "rpc_http_post", self.platform_url, {}, {}, {})
    if not ok or 200 ~= code then
        log_err("[NameServlet][load_address_info] call faild: ok=%s,code=%s,post_res=%s", ok, code, post_res)
        return false
    end
    local ok2, res_data = pcall(json_decode, post_res)
    if not ok2 or res_data.code ~= 0 then
        log_err("[NameServlet][load_address_info] load address faild: res=%s", post_res)
        return false
    end
    log_info("[NameServlet][load_address_info]result: res=%s", post_res)
    local addr_data = res_data.data
    self.areas        = addr_data.areas
    self.gate_servers = addr_data.gate_servers
    self.gate_clients = addr_data.gate_clients
    return true
end

--选择一个平台地址
function NameServlet:choose_platform()
    if #self.gate_clients == 0 then
        return DirCode.DIR_NO_PLATFORM
    end
    local plat_url = random_array(self.gate_clients)
    return SUCCESS, plat_url
end

-- lobby汇报事件
function NameServlet:rpc_lobby_report(data)
    local quanta_id = data.id
    local area_id = data.area_id
    if not self.id2lobby[quanta_id] then
        log_info("[NameServlet][rpc_lobby_report] new server info: area_id=%s, id=%d", area_id, quanta_id)
    end
    self.id2lobby[quanta_id] = data
    if area_id > 0 then
        if not self.pid2lobbys[area_id] then
            self.pid2lobbys[area_id] = {}
        end
        self.pid2lobbys[area_id][quanta_id] = true
    end
    return SUCCESS
end

-- 获取platform的gateway
function NameServlet:rpc_platform_gateway(data)
    if #self.gate_servers == 0 then
        return DirCode.DIR_NO_PLATFORM
    end
    local plat_url = random_array(self.gate_servers)
    return SUCCESS, plat_url
end

-- 获取一个lobby服务器信息
function NameServlet:choose_lobby_info(area_id)
    local choose_lobby = nil
    local online_count = mhuge
    local area_info = self.pid2lobbys[area_id]
    if area_info then
        --用于分区分服模式
        for lobby_id in pairs(area_info) do
            local lobby_info = self.id2lobby[lobby_id]
            if lobby_info.online_count < online_count then
                choose_lobby = lobby_info
            end
        end
    else
        --用于全区全服模式，从所有lobby中选择
        for _, lobby_info in pairs(self.id2lobby) do
            if lobby_info.online_count < online_count then
                choose_lobby = lobby_info
            end
        end
    end
    return choose_lobby
end

-- 为open_id分配lobby
function NameServlet:dispatch_lobby_by_open_id(open_id, area_id)
    local lobby_info = self:choose_lobby_info(area_id)
    if not lobby_info then
        log_err("[NameServlet][dispatch_lobby_by_open_id] choose_lobby_info faild: no lobby")
        return DirCode.DIR_NO_LOBBY
    end
    local rpc_req = {
        open_id     = open_id,
        area_id     = area_id,
        lobby_id    = lobby_info.id,
    }
    local ok, code, lobby_id = router_mgr:call_index_hash(open_id, "rpc_cas_dispatch_lobby", rpc_req)
    if not ok or check_failed(code) then
        log_err("[NameServlet][dispatch_lobby_by_open_id] call index dispatch faild: ok=%s,code=%s", ok, code)
        return ok and code or KernCode.RPC_FAILED
    end
    lobby_info = self.id2lobby[lobby_id]
    return SUCCESS, lobby_info
end

--网络事件
----------------------------------------------------------------
function NameServlet:on_session_err(session, err)
end

-- 会话消息
function NameServlet:on_session_cmd(session, cmd_id, body, session_id)
    if cmd_id == CSCmdID.NID_HEARTBEAT_REQ then
        return
    end
    event_mgr:notify_command(cmd_id, session, body, session_id)
end

-- 服务器断开事件
function NameServlet:on_service_close(id)
    local lobby_info = self.id2lobby[id]
    if lobby_info then
        self.id2lobby[id] = nil
        local area_id = lobby_info.area_id
        if area_id > 0 then
            self.pid2lobbys[area_id][id] = nil
        end
        log_info("[NameServlet][on_service_close] remove server info: id=%d", id)
    end
end

-- export
quanta.name_servlet = NameServlet()

return NameServlet
