--token_servlet.lua
local log_debug         = logger.debug
local check_failed      = utility.check_failed

local DirCode           = enum("DirCode")
local KernCode          = enum("KernCode")
local CSCmdID           = ncmd_cs.NCmdId
local SUCCESS           = KernCode.SUCCESS

local event_mgr         = quanta.get("event_mgr")
local router_mgr        = quanta.get("router_mgr")
local client_mgr        = quanta.get("client_mgr")
local name_servlet      = quanta.get("name_servlet")

-- 令牌服务
local TokenServlet = singleton()
function TokenServlet:__init()
    event_mgr:add_cmd_listener(self, CSCmdID.NID_GET_TOKEN_REQ, "on_get_token_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_GET_SYSTEM_NOTICE_REQ, "on_get_system_notice_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_GET_AREA_LIST_REQ, "on_get_area_list_req")
end

-- 玩家请求lobbd地址和访问令牌
function TokenServlet:on_get_token_req(session, net_req, session_id)
    local open_id, area_id = net_req.open_id, net_req.area_id
    log_debug("[TokenServlet][on_get_token_req]: area_id=%s, open_id=%s", area_id, open_id)
    local code, plat_url = name_servlet:choose_platform()
    if check_failed(code) then
        log_debug("[TokenServlet][on_get_token_req] cannot find useable platform gateway!")
        client_mgr:callback_dx(session, CSCmdID.NID_GET_TOKEN_RES, {code = code }, session_id)
        return
    end
    -- 随机分配一个lobby先
    local code2, lobby_info = name_servlet:dispatch_lobby_by_open_id(open_id, area_id)
    if check_failed(code2) or not lobby_info then
        log_debug("[TokenServlet][on_get_token_req] cannot find useable lobby!")
        client_mgr:callback_dx(session, CSCmdID.NID_GET_TOKEN_RES, {code = DirCode.DIR_NO_LOBBY }, session_id)
        return
    end
    -- 获取id和token
    local ok, code3, lobby_access_token = router_mgr:call_target(lobby_info.id, "rpc_get_lobby_access_token", open_id, plat_url)
    if not ok or check_failed(code3) then
        log_debug("[TokenServlet][on_get_token_req] cannot find useable lobby!")
        client_mgr:callback_dx(session, CSCmdID.NID_GET_TOKEN_RES, {code = ok and ok or KernCode.RPC_FAILED }, session_id)
        return
    end
    -- 完成lobby地址分配
    local net_res = {
        code = SUCCESS,
        lobby_access_token = lobby_access_token,
        lobby_addr = {
            host = lobby_info.host,
            ip   = lobby_info.listen_ip,
            port = lobby_info.listen_port,
        },
    }
    log_debug("[TokenServlet][on_get_token_req] success: area_id=%s, open_id=%s", area_id, open_id)
    client_mgr:callback_dx(session, CSCmdID.NID_GET_TOKEN_RES, net_res, session_id)
end

-- 请求小区列表
function TokenServlet:on_get_area_list_req(session, body, session_id)
    log_debug("[TokenServlet][on_get_area_list_req]!")
    local net_res = { area_list = name_servlet:get_areas() }
    client_mgr:callback_dx(session, CSCmdID.NID_GET_AREA_LIST_RES, net_res, session_id)
end

-- 请求系统公告
function TokenServlet:on_get_system_notice_req(session, _, session_id)
    -- 当前固定数据
    local system_notice = {id = 1, msg = "这是一条系统消息！"}
    local net_res = {notice_list = {system_notice}}
    client_mgr:callback_dx(session, CSCmdID.NID_GET_SYSTEM_NOTICE_RES, net_res, session_id)
end

-- export
quanta.token_servlet  = TokenServlet()

return TokenServlet
