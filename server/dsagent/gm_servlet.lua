--gm_servlet.lua
local ljson = require("luacjson")

local fdate         = os.date
local ftime         = os.time
local tinsert       = table.insert
local sformat       = string.format
local json_encode   = ljson.encode
local log_info      = logger.info
local serialize     = logger.serialize

local KernCode      = enum("KernCode")
local SUCCESS       = KernCode.SUCCESS

local event_mgr     = quanta.get("event_mgr")

local GMServlet = singleton()
function GMServlet:__init()
    -- 待查用户数据
    self.users = {}
    self:setup()
end

function GMServlet:setup()
    -- 添加触发器
    event_mgr:add_trigger(self, "ds_user")
    event_mgr:add_trigger(self, "user_enter_game")
    event_mgr:add_trigger(self, "user_leave_game")
    event_mgr:add_trigger(self, "user_disconnect")
end

-- 用户进入局内记录
function GMServlet:user_enter_game(user_info)
    if not user_info or not user_info.name then return end
    log_info("[GMServlet][user_enter_game]->user_info:%s", serialize(user_info))
    local today_users = self:get_today_users()
    if not today_users[user_info.name] then
        today_users[user_info.name] = {}
    end
    local user = today_users[user_info.name]
    if not user.name then
        user.name = user_info.name
    end
    if not user.ip then
        user.ip = user_info.ip
    end
    if not user.player_ids then
        user.player_ids = {}
    end
    user.player_ids[user_info.id] = 1
    user.enter_time = ftime()
    user.is_online = true
end

-- 用户离开局内记录
function GMServlet:user_leave_game(player_id)
    if not player_id then
        return
    end
    self:update_user_disonline(player_id)
end

-- 用户断开连接
function GMServlet:user_disconnect(player_id)
    if not player_id then
        return
    end
    self:update_user_disonline(player_id)
end

-- 更新用户数据
function GMServlet:update_user_disonline(player_id)
    local user = nil
    for name, data in pairs(self:get_today_users()) do
        if data.player_ids and data.player_ids[player_id] then
            user = data
            break
        end
    end
    if not user then return end
    if user.enter_time then
        user.online_total = (user.online_total or 0) + ftime() - user.enter_time
    end
    user.is_online = false
    log_info("[GMServlet][update_user_disonline]->player_id:%s", player_id)
end

-- 获取今日用户数据表
function GMServlet:get_today_users()
    local cur_time = ftime()
    local today = fdate("%y/%m/%d", cur_time)
    if not self.users[today] then
        self.users[today] = {}
    end
    return self.users[today]
end

function GMServlet:ds_user(body)
    local ret_ = {}
    local cur_time = ftime()
    for name, data in pairs(self:get_today_users()) do
        if data.is_online then
            local total_time = data.online_total or 0 + cur_time - data.enter_time
            tinsert(ret_, sformat("name:%s  total_time:%ds  ip:%s  online:%s", name, total_time, data.ip, "True"))
        else
            tinsert(ret_, sformat("name:%s  total_time:%ds  ip:%s  online:%s", name, data.online_total or 0, data.ip, "False"))
        end
    end

    local resp_data = {}
    resp_data.session_id = body.session_id
    resp_data.cmd_str    = body.cmd_str
    resp_data.err_code   = SUCCESS
    resp_data.result = json_encode(ret_)

    quanta.monitor:on_response_exec_cmd(resp_data)
end

quanta.gm_servlet  =  GMServlet()

return GMServlet
