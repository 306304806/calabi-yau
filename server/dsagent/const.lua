--const.lua

-- DS状态枚举
local DsStatus = enum("DsStatus", 0)
DsStatus.CREATE                 = 0 -- 创建DS
DsStatus.RUN                    = 1 -- DS运行
DsStatus.EXIT                   = 2 -- DS退出
DsStatus.UNCONNECT              = 3 -- DS 未连接
DsStatus.CRASH                  = 4 -- DS Crash
DsStatus.SETTLE                 = 5 -- DS结算

-- 删除DS房间枚举
local DelRoom = enum("DelRoom", 0)
DelRoom.GAME_END                = 0 -- 游戏正常结束
DelRoom.CREATE_OVERTIME         = 1 -- 创建超时
DelRoom.DS_EXPIRED              = 2 -- ds过期强杀
DelRoom.STOP_SVR                = 3 -- 停服强杀
DelRoom.DS_CRASH                = 4 -- ds crash

local DsExit = enum("DsExit", 0)
DsExit.TIMEOUT                  = 1 -- 超时通知DS主动退出
DsExit.DSA_CLOSE                = 2 -- Dsa准备关闭通知DS主动退出
DsExit.BATLLE_FINISH            = 3 -- 局内正常结束通知DS主动退出
