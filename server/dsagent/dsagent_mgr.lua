--dsagent_mgr.lua
local otime             = os.time
local odate             = os.date
local tsort             = table.sort
local tinsert           = table.insert
local guid_group        = guid.group
local g_room_mode       = guid.room_mode
local log_err           = logger.err
local log_info          = logger.info
local log_warn          = logger.warn
local log_debug         = logger.debug
local env_addr          = environ.addr
local env_get           = environ.get
local serialize         = logger.serialize
local check_failed      = utility.check_failed

local DsExit            = enum("DsExit")
local DelRoom           = enum("DelRoom")
local DsStatus          = enum("DsStatus")
local KernCode          = enum("KernCode")
local RmsgGame          = enum("RmsgGame")
local PeriodTime        = enum("PeriodTime")
local MemberStatus      = enum("MemberStatus")
local GameMode          = enum("GameMode")

local SUCCESS           = KernCode.SUCCESS
local DSCmdID           = ncmd_ds.DsCmdId

local monitor           = quanta.get("monitor")
local timer_mgr         = quanta.get("timer_mgr")
local event_mgr         = quanta.get("event_mgr")
local router_mgr        = quanta.get("router_mgr")
local client_mgr        = quanta.get("client_mgr")
local rmsg_standings    = quanta.get("rmsg_standings")
local rmsg_settlement   = quanta.get("rmsg_settlement")
local config_mgr        = quanta.get("config_mgr")
local map_db            = config_mgr:init_table("mapcfg", "id")

local function sort_func(left, right)
    return left > right
end

local DsAgentMgr = singleton()
function DsAgentMgr:__init()
    -- 房间信息列表 [room_id] = {}
    self.room_list = {}
    -- 上报dsc标记 默认为true
    self.report_dsc = true
    -- ds状态上报web标记 默认为true
    self.report_web = true
    -- 局外观战loading信息
    self.loading_data = {}
    --初始化
    self:setup()
end

function DsAgentMgr:setup()
    timer_mgr:loop(PeriodTime.SECOND_3_MS, function()
        self:check_ds_expired()
        self:report_to_dsc()
    end)

    -- 监听ds连接会话关闭
    event_mgr:add_listener(self, "on_session_err")
    -- 退出游戏注册
    event_mgr:add_trigger(self, "quanta_frame_exit")

    local minp, maxp = env_addr("QUANTA_DS_OUT_PORT_RANGE")
    self.ds_out_port_min = tonumber(minp)
    self.ds_out_port_max = tonumber(maxp)

    self.timer_report_ds_node = timer_mgr:loop(PeriodTime.SECOND_30_MS, function()
        self:report_ds_node(0)
    end)
    self.timer_report_weapon_exp = timer_mgr:loop(PeriodTime.SECOND_5_MS, function()
        self:report_weapon_exp_change(0)
    end)
end

-- 获取房间信息
function DsAgentMgr:get_room_info(room_id)
    return self.room_list[room_id]
end

-- 创建房间信息
function DsAgentMgr:create_room_info(room_id, info)
    if self.room_list[room_id] then
        log_err("[DsAgentMgr][create_room_info]->repeat! room_id:%s", room_id)
        return
    end
    self.room_list[room_id] = info
    self.report_dsc = true
end

-- 删除房间信息
function DsAgentMgr:del_room_info(room_id, reason)

    local room_data = self.room_list[room_id]
    if room_data then
        self.room_list[room_id] = nil
        self.report_dsc = true
        log_info("[DsAgentMgr][del_room_info]->room_id:%s, reason:%s", room_id, reason)
    else
        log_err("[DsAgentMgr][del_room_info]->find room failed! room_id:%s", room_id)
    end
end

-- 获取所有房间信息
function DsAgentMgr:get_all_rooms()
    return self.room_list
end

-- 插入玩家数据
function DsAgentMgr:insert_player(room_id, player_info)
    local room_data = self:get_room_info(room_id)
    if not room_data or not player_info.player_id then
        return false
    end
    room_data.players[player_info.player_id] = player_info
    log_info("[DsAgentMgr][insert_player]->room_id:%s, player_id:%s", room_id, player_info.player_id)
    return true
end

-- 删除玩家数据
function DsAgentMgr:del_player(room_id, player_id)
    local room_data = self:get_room_info(room_id)
    if room_data then
        room_data.players[player_id] = nil
        log_info("[DsAgentMgr][del_player]->room_id:%s, player_id:%s", room_id, player_id)
    end
end

-- 发送消息给Ds
function DsAgentMgr:send_to_ds(room_id, cmd_id, msg_body)
    local room_data = self:get_room_info(room_id)
    if not room_data then
        log_err("[DsAgentMgr][send_to_ds]->get room failed! room_id:%s", room_id)
        return false
    end
    local ds_session = client_mgr:get_session_by_token(room_data.ds_token)
    if not ds_session then
        log_err("[DsAgentMgr][send_to_ds]->get ds session failed! room_id:%s, ds_token:%s", room_id, room_data.ds_token)
        return false
    end
    client_mgr:send_dx(ds_session, cmd_id, msg_body)
    log_info("[DsAgentMgr][send_to_ds]->room_id:%s, cmd_id:%s", room_id, cmd_id)
    return true
end

-- create match回复
function DsAgentMgr:create_match_resp(ds_session, resp)
    local ds_room_id = resp.room_id
    local room_data = self.room_list[ds_room_id]
    if not room_data then
        log_err("[DsAgentMgr][create_match_resp]->get room failed! token:%d, room_id:%s", ds_session.token, ds_room_id)
        return
    end
    if room_data.ds_start_flag then return end
    room_data.ds_start_flag = true
    if not room_data.ds_start_time then
        room_data.ds_start_time = otime()
    end
    -- 通知房间服
    local agent_info = {}
    agent_info.ip = env_get("QUANTA_HOST_IP")
    agent_info.port =  room_data.ntf_client_port
    log_info("[DsAgentMgr][create_match_resp]->agent_info:%s", serialize(agent_info))
    local ok, code = router_mgr:call_target(room_data.from_svr_id, "rpc_ntf_clients_ds_info", ds_room_id, agent_info)
    if not ok or check_failed(code) then
        log_err("[DsAgentMgr][create_match_resp]->rpc_ntf_clients_ds_info failed! ds_room_id:%s", ds_room_id)
    end
end

-- 获取客户端连接ds端口
function DsAgentMgr:get_ds_out_port()
    if not self.ds_out_port then
        self.ds_out_port = self.ds_out_port_min
    end
    if self.ds_out_port >= self.ds_out_port_max then
        self.ds_out_port = self.ds_out_port_min
    end
    self.ds_out_port = self.ds_out_port + 1
    return self.ds_out_port
end

-- 更新玩家状态
function DsAgentMgr:update_player_status(room_id, player_id, status)
    local room_data = self:get_room_info(room_id)
    if not room_data then
        log_err("[DsAgentMgr][update_player_status]->find room failed! room_id:%s", room_id)
        return
    end
    local player = room_data.players[player_id]
    if not player then
        log_err("[DsAgentMgr][update_player_status]->find player failed! room_id:%s, player_id:%s", room_id, player_id)
        return
    end
    player.status = status
    if status >= MemberStatus.ENTERD then
        router_mgr:call_target(room_data.from_svr_id, "rpc_update_status", room_id, player_id, status)
    end
    log_info("[DsAgentMgr][update_player_status]->room_id:%s, palyer_uid:%s, status:%d", room_id, player_id, status)
end

-- 更新DS状态
function DsAgentMgr:update_ds_status(room_id, ds_status)
    local room_data = self:get_room_info(room_id)
    if not room_data then
        log_err("[DsAgentMgr][update_ds_status]->find room failed! room_id:%s", room_id)
        return
    end
    room_data.ds_status = ds_status
    if ds_status ~= DsStatus.CREATE or ds_status == DsStatus.SETTLE then
        self:report_ds_node(room_id)
    end
    log_info("[DsAgentMgr][update_ds_status]->room_id:%s, ds_status:%s", room_id, ds_status)
end

-- 获取DS状态
function DsAgentMgr:get_ds_status(room_id)
    local room_data = self:get_room_info(room_id)
    if not room_data then
        log_err("[DsAgentMgr][get_ds_status]->find room failed! room_id:%s", room_id)
        return
    end
    return room_data.ds_status or 0
end

-- Ds会话断开连接
function DsAgentMgr:on_session_err(session)
    for room_id, data in pairs(self.room_list) do
        if data.ds_token == session.token then
            if data.ds_status == DsStatus.EXIT then
                self:del_room_info(room_id, DelRoom.GAME_END)
            else
                data.ds_token = -1
                self:ntf_roomsvr_destroy(room_id, DsStatus.CRASH)
                self:update_ds_status(room_id, DsStatus.CRASH)
                self:del_room_info(room_id, DelRoom.DS_CRASH)
                log_info("[DsAgentMgr][on_session_err]->room_id:%s", room_id)
            end
            break
        end
    end
end

-- ds battle finish
function DsAgentMgr:battle_finish(room_id, settle_data)
    local room_data = self:get_room_info(room_id)
    if not room_data then
        log_err("[DsAgentMgr][battle_finish]->find room failed! room_id:%s", room_id)
        return
    end

    local players = settle_data.players
    if not players or not next(players) then
        log_err("[DsAgentMgr][battle_finish]->get settle data error! room_id:%s", room_id)
        return
    end

    self:fill_settle_data(room_id, room_data, settle_data)

    local ntf_players = {}
    for idx, data in pairs(players) do
        -- take care!!! send_message(from, to, typ, body, id) 此处形参内容---> from:所在dsa, to:玩家ID, typ:战斗结算枚举, body:结算内容, id:nil
        local ok = rmsg_settlement:send_message(quanta.name, data.player_id, RmsgGame.BATTLE_SETTLE, settle_data)
        if not ok then
            log_err("[DsAgentMgr][battle_finish]->save rmsg failed! player_id:%s, data:%s", data.player_id, serialize(settle_data))
        else
            tinsert(ntf_players, data.player_id)
        end
    end
    self:report_weapon_exp_change(room_id)

    -- 战绩数据
    self:record_standings(room_id, room_data, settle_data)

    local ok, code = router_mgr:call_target(room_data.from_svr_id, "rpc_battle_settle", room_id, ntf_players)
    if not ok or code ~= SUCCESS then
        log_err("[DsAgentMgr][battle_finish]->call rpc_battle_settle failed! room_id:%s", room_id)
        return
    end

    log_debug("[DsAgentMgr][battle_finish]->rpc_battle_settle->room_id:%s, ntf_players:%s", room_id, serialize(ntf_players))
end

-- 填充结算数据
function DsAgentMgr:fill_settle_data(room_id, room_data, settle_data)
    local players = settle_data.players
    local map_cfg = map_db:find_one(settle_data.map_id)
    if not map_cfg then
        log_err("[DsAgentMgr][fill_settle_data]->get map cfg failed! map_id:%s", settle_data.map_id)
        return
    end
    settle_data.room_mode = g_room_mode(guid_group(room_id))
    settle_data.play_mode = map_cfg.type
    if settle_data.room_mode ~= GameMode.ROOM then
        local pos_rank, pos_team, power_prize = self:calc_rank(room_data, players)
        for _, data in pairs(players) do
            data.pos = pos_rank[data.scores]                                -- 局内排名
            data.team_pos = pos_team[data.team_id][data.scores]             -- 队内排名
            data.average_rate = power_prize[data.team_id].average or 0      -- 平均胜率
            data.median_rate = power_prize[data.team_id].median or 0        -- 中位数胜率
        end
    end
    for _, data in pairs(players) do
        data.role_id = room_data.players[data.player_id].hero           -- 角色id
    end
end

function DsAgentMgr:calc_rank(room_data, players)
    local team_rank, rank, team_win_rate = self:build_rank_data(room_data, players)
    -- 局内排名处理
    tsort(rank, sort_func)
    local pos_rank = {}
    for idx = 1, #rank do
        if rank[idx] ~= rank[idx-1] then
            -- 分数不等于上一个，名次使用当前idx
            pos_rank[rank[idx]] = idx
        else
            -- 分数相等并列排序
            pos_rank[rank[idx]] = pos_rank[rank[idx-1]]
        end
    end

    -- 组内排名处理
    local pos_team = {}
    for team_id, data in pairs(team_rank) do
        tsort(data, sort_func)
        pos_team[team_id] = pos_team[team_id] or {}
        for idx = 1, #data do
            if data[idx] ~= data[idx - 1] then
                pos_team[team_id][data[idx]] = idx
            else
                pos_team[team_id][data[idx]] = pos_team[team_id][data[idx - 1]]
            end
        end
    end

    local power_prize = {}
    for team_id, data in pairs(team_win_rate) do
        if #data <= 0 then
            break
        end
        tsort(data, sort_func)
        power_prize[team_id] = power_prize[team_id] or {}
        for idx, rate in pairs(data) do
            power_prize[team_id].total = (power_prize[team_id].total or 0) + rate
        end
        power_prize[team_id].average = power_prize[team_id].total / (#data)
        power_prize[team_id].median = data[math.ceil(#data / 2)]
    end
    log_debug("[DsAgentMgr][calc_rank]->pos_rank:%s, pos_team:%s, power_prize:%s", serialize(pos_rank), serialize(pos_team), serialize(power_prize))
    return pos_rank, pos_team, power_prize
end

function DsAgentMgr:build_rank_data(room_data, players)
    local team_rank = {}
    local rank = {}
    local team_win_rate = {}
    for idx, data in pairs(players) do
        tinsert(rank, data.scores)
        team_rank[data.team_id] = team_rank[data.team_id] or {}
        tinsert(team_rank[data.team_id], data.scores)
        team_win_rate[data.team_id] = team_win_rate[data.team_id] or {}
        local room_player_info = room_data.players[data.player_id]
        if room_player_info then
            tinsert(team_win_rate[data.team_id], room_player_info and room_player_info.win_rate or 0)
        end
    end
    return team_rank, rank, team_win_rate
end

-- 记录战绩
function DsAgentMgr:record_standings(room_id, room_data, settle_data)
    local standings = { players = {} }
    for _, player in pairs(settle_data.players) do
        local room_player = room_data.players[player.player_id]
        local standings_player = {
            player_id   = player.player_id,
            nick_name   = room_player.nick,
            gender      = room_player.sex,
            camp        = room_player.campid,
            icon        = room_player.icon,
            role_id     = room_player.hero,
            skin_id     = room_player.skin,
            kill        = player.kill_num,
            injure      = player.damage,
            scores      = player.scores,
            achievements= player.achievement_ids,
        }
        tinsert(standings.players, standings_player)
    end
    local data = { standings = standings }
    local ok = rmsg_standings:send_message(quanta.name, room_id, RmsgGame.STANDINGS, data)
    if not ok then
        log_err("[DsAgentMgr][record_standings]->save rmsg_standings rmsg failed! room_id:%s, data:%s", room_id, serialize(data))
    end
end

-- 上报dsa的数据到dsc
function DsAgentMgr:report_to_dsc()
    if not self.report_dsc then
        return
    end

    local send_info = {}
    for id, data in pairs(self.room_list) do
        tinsert( send_info, self:build_report_dsc(id) )
    end
    local ok, err = router_mgr:call_dscenter_master("rpc_report_dsa_info", quanta.id, send_info)
    log_info("[DsAgentMgr][report_to_dsc]->ok:%s, err:%s", ok, err)
    if ok and SUCCESS == err then
        self.report_dsc = false
        log_info("[DsAgentMgr][report_to_dsc]->rpc_report_dsa_info")
    end
end

-- 构建上报dsc数据
function DsAgentMgr:build_report_dsc(room_id)
    local room_data = self.room_list[room_id]
    if not room_data then
        return
    end

    return {room_id = room_id, ntf_client_port = room_data.ntf_client_port, ds_start_time = room_data.ds_start_time,
            ds_status = room_data.ds_status, dsa_host_ip = env_get("QUANTA_HOST_IP")}
end

--  收到room数据进行重建
function DsAgentMgr:rebuild_from_room(room_svr_id, room_list)
    for _, data in pairs(room_list) do
        if not self.room_list[data.room_id] then
            self.room_list[data.room_id] = {}
        end
        self.room_list[data.room_id].config = data.config
        self.room_list[data.room_id].players = data.players
        self.room_list[data.room_id].from_svr_id = room_svr_id
    end
end

-- 收到dsc数据进行重建
function DsAgentMgr:rebuild_from_dsc(dsc_id, room_info)
    for _, data in pairs(room_info) do
        if not self.room_list[data.room_id] then
            self.room_list[data.room_id] = {}
        end
        self.room_list[data.room_id].ntf_client_port = data.ntf_client_port
        self.room_list[data.room_id].ds_start_time = data.ds_start_time
        self.room_list[data.room_id].ds_status = data.ds_status
    end
end

-- 检查ds过期
function DsAgentMgr:check_ds_expired()
    local cur_time = otime()
    local kill_ds_list = {}
    for id, data in pairs(self.room_list) do
        if data.ds_start_time and cur_time > data.ds_start_time + PeriodTime.MINUTE_30_S then
            kill_ds_list[id] = data.ds_start_time
        end
    end
    for room_id, start_time in pairs(kill_ds_list) do
        self:ntf_ds_exit(room_id, DsExit.TIMEOUT)
        log_warn("[DsAgentMgr][check_ds_expired] ds lifetime expired! room_id:%s, ds_start_time:%s", room_id, start_time)
        self:del_room_info(room_id, DelRoom.DS_EXPIRED)
    end
end

-- 主动退出游戏回调
function DsAgentMgr:quanta_frame_exit()
    -- 强制关闭所有ds
    for room_id, data in pairs(self.room_list) do
        self:ntf_ds_exit(room_id, DsExit.DSA_CLOSE)
    end
end

-- 通知Ds主动退出
function DsAgentMgr:ntf_ds_exit(room_id, reason)
    local room_data = self:get_room_info(room_id)
    if not room_data then
        log_err("[DsAgentMgr][ntf_ds_exit]->find room failed! room_id:%s", room_id)
        return
    end
    local ntf_data = {}
    ntf_data.room_id = room_id
    ntf_data.ds_pid  = room_data.ds_pid
    ntf_data.reason  = reason

    if not self:send_to_ds(room_id, DSCmdID.NID_DS_EXIT_NTF, ntf_data) then
        log_err("[DsAgentMgr][ntf_ds_exit]->send to ds failed! room_id:%s", room_id)
    end
    self:ntf_roomsvr_destroy(room_id, DsStatus.EXIT)
    self:update_ds_status(room_id, DsStatus.EXIT)
    log_info("[DsAgentMgr][ntf_ds_exit]->room_id:%s, reason:%s", room_id, reason)
end

function DsAgentMgr:ntf_roomsvr_destroy(room_id, ds_status)
    log_info("[DsAgentMgr][ntf_roomsvr_destroy]->room_id:%s, ds_status:%s", room_id, ds_status)
    local room_data = self:get_room_info(room_id)
    if not room_data or not room_data.from_svr_id then
        log_err("[DsAgentMgr][ntf_roomsvr_destroy] get room data failed! room_id:%s, room_data:%s", room_id, serialize(room_data))
        return
    end
    -- 通知room svr销毁
    local ok, code = router_mgr:call_target(room_data.from_svr_id, "rpc_room_finish", room_id, ds_status)
    if not ok or code ~= SUCCESS then
        log_err("[DsAgentMgr][ntf_roomsvr_destroy]->call rpc_room_finish failed! room_id:%s,code:%s", room_id, code)
        return
    end
    log_info("[DsAgentMgr][ntf_roomsvr_destroy]->ntf_roomsvr_destroy->room_id:%s", room_id)
end

function DsAgentMgr:report_ds_node(room_id)
    if quanta.platform == "windows" then
        return
    end
    if not next(self.room_list) then return end
    local report_data = {}
    report_data.host   = env_get("QUANTA_HOST_IP")
    report_data.nodes = {}
    if room_id == 0 then
        for id, room_data in pairs(self.room_list) do
            tinsert(report_data.nodes, { room_id = tostring(id), image = room_data.docker_image, map = room_data.map_id,
                                        start = odate("%Y/%m/%d-%H:%M:%S", room_data.create_time), state = room_data.ds_status })
        end
    else
        local room_data = self:get_room_info(room_id)
        if room_data then
            tinsert(report_data.nodes, { room_id = tostring(room_id), image = room_data.docker_image, map = room_data.map_id,
                                         start = odate("%Y/%m/%d-%H:%M:%S", room_data.create_time), state = room_data.ds_status })
        end
    end
    if next(report_data.nodes) then
        local ok, code, _ = monitor:service_request("ds_node_status", report_data)
        if not ok or SUCCESS ~= code then
            log_err("[DsAgentMgr][report_ds_node]->service_request failed!")
        end
        --log_info("[DsAgentMgr][report_ds_node]->repoort_data:%s", serialize(report_data))
    end
end

-- 更新loading数据
function DsAgentMgr:update_loading_data(player_id, team_index)
    self.loading_data[player_id] = team_index
end

-- 获取loading数据
function DsAgentMgr:get_loading_data()
    return self.loading_data
end

-- 上报武器经验变化
function DsAgentMgr:report_weapon_exp_change(room_id)
    local report_data = self:get_weapon_exp_report(room_id)
    if not next(report_data) then
        return
    end
    for svr_id, data in pairs(report_data) do
        local ok, code = router_mgr:call_target(svr_id, "rpc_ntf_add_weapon_exp", data)
        if not ok or check_failed(code) then
            log_err("[DsAgentMgr][report_weapon_exp_change]->rpc_ntf_add_weapon_exp failed! data:%s", serialize(data))
        end
    end
end

-- 获取武器变化信息
function DsAgentMgr:get_weapon_exp_report(room_id)
    local report_data = {}
    for id, room_data in pairs(self.room_list) do
        if room_id == 0 or ( room_id > 0 and id ~= room_id) then
            for player_id, player_data in pairs(room_data.players) do
                if player_data.weapon_exp_info then
                    for weapon_uuid, add_exp in pairs(player_data.weapon_exp_info) do
                        local from_svr_id = room_data.from_svr_id
                        if not report_data[from_svr_id] then
                            report_data[from_svr_id] = {}
                        end
                        local svr_report = report_data[from_svr_id]
                        if not svr_report[id] then
                            svr_report[id] = {}
                        end
                        local room_report = svr_report[id]
                        if not room_report[player_id] then
                            room_report[player_id] = {}
                        end
                        local player_report = room_report[player_id]
                        player_report[weapon_uuid] = add_exp
                    end
                    player_data.weapon_exp_info = {}
                end
            end
        end
    end
    return report_data
end

quanta.dsagent_mgr = DsAgentMgr()

return DsAgentMgr
