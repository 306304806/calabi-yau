--rebuild_mgr.lua
local log_debug     = logger.debug
--local serialize     = logger.serialize

local event_mgr     = quanta.get("event_mgr")

local RebuildMgr = singleton()
local prop = property(RebuildMgr)
prop:accessor("replicate_rooms", {})

-- 初始化
function RebuildMgr:__init()
    event_mgr:add_trigger(self, "evt_destroy_room")
end

-- 新增房间拷贝数据
function RebuildMgr:add_replicate_room(data)
    self.replicate_rooms[data.id] = data
end

-- 删除房间拷贝数据
function RebuildMgr:del_replicate_room(room_id)
    self.replicate_rooms[room_id] = nil
    log_debug("[RebuildMgr][del_replicate_room]")
end

-- 房间销毁
function RebuildMgr:evt_destroy_room(room_id)
    self.replicate_rooms[room_id] = nil
    log_debug("[RebuildMgr][evt_destroy_room] room_id->%s", room_id)
end

quanta.rebuild_mgr = RebuildMgr()

return RebuildMgr
