--dsa_servlet.lua
local ljson   = require("luacjson")
local oexec         = os.execute
local json_decode   = ljson.decode
local log_err       = logger.err
local log_info      = logger.info
local sformat       = string.format
local serialize     = logger.serialize
local log_debug     = logger.debug
local tcopy         = table_ext.copy
local new_guid      = guid.new
local guid_group    = guid.group
local guid_room_mode = guid.room_mode

local DsExit        = enum("DsExit")
local DsStatus      = enum("DsStatus")
local KernCode      = enum("KernCode")
local RoomCode      = enum("RoomCode")
local PeriodTime    = enum("PeriodTime")
local MemberStatus  = enum("MemberStatus")
local GameMode      = enum("GameMode")

local SUCCESS       = KernCode.SUCCESS
local DSCmdID       = ncmd_ds.DsCmdId

local timer_mgr     = quanta.get("timer_mgr")
local event_mgr     = quanta.get("event_mgr")
local router_mgr    = quanta.get("router_mgr")
local dsagent_mgr   = quanta.get("dsagent_mgr")
local client_mgr    = quanta.get("client_mgr")
local config_mgr    = quanta.get("config_mgr")

local role_db       = config_mgr:init_table("role", "role_id")

local DsaServlet = singleton()
function  DsaServlet:__init()
    -- 注册rpc事件
    event_mgr:add_listener(self, "rpc_half_enter")
    event_mgr:add_listener(self, "rpc_enter_ds_watch")

    -- ds2s
    event_mgr:add_listener(self, "on_session_cmd")
    event_mgr:add_cmd_listener(self, DSCmdID.NID_DS_HEARTBEAT_REQ,         "on_ds_heartbeat_req")
    event_mgr:add_cmd_listener(self, DSCmdID.NID_PLAYER_LOGIN_NTF,         "on_ds_ntf_player_login")
    event_mgr:add_cmd_listener(self, DSCmdID.NID_PLAYER_LOGOUT_NTF,        "on_ds_ntf_player_logout")
    event_mgr:add_cmd_listener(self, DSCmdID.NID_PLAYER_DISCONNECT_NTF,    "on_ds_ntf_player_disconnct")
    event_mgr:add_cmd_listener(self, DSCmdID.NID_DS_GAME_SETTLE_REQ,       "on_ds_game_end_settle")
    event_mgr:add_cmd_listener(self, DSCmdID.NID_PLAYER_EXIT_WATCH_NTF,    "on_ds_exit_watch")
    event_mgr:add_cmd_listener(self, DSCmdID.NID_PLAYER_WEAPON_EXP_NTF,    "on_ds_player_weapon_add_exp")
    event_mgr:add_cmd_listener(self, DSCmdID.NID_DS_ACHIEVE_REACH_NTF,     "on_ds_achieve_reach_ntf")
    event_mgr:add_cmd_listener(self, DSCmdID.NID_DS_BATTLE_DATA_NTF,       "on_ds_battle_data_ntf")
end

-- 会话消息
function DsaServlet:on_session_cmd(session, cmd_id, body, session_id)
    event_mgr:notify_command(cmd_id, session, body, session_id)
end

-- 中途进入请求
function DsaServlet:rpc_half_enter(room_id, player)
    log_info("[DsaServlet][rpc_half_enter]->room_id:%s, player:%s", room_id, serialize(player))
    if not room_id or not player then
        log_err("[DsaServlet][rpc_half_enter]->param error!")
        return KernCode.LOGIC_FAILED
    end
    local room_data = dsagent_mgr:get_room_info(room_id)
    if not room_data then
        log_err("[DsaServlet][rpc_half_enter]->find room failed! room_id:%s", room_id)
        return RoomCode.ROOM_ERR_NOT_EXIST
    end
    if not dsagent_mgr:insert_player(room_id, player) then
        return RoomCode.ROOM_ERR_NOT_EXIST
    end
    --将玩家数据通知ds
    local send_data = {room_id = room_id, player = player}
    if not dsagent_mgr:send_to_ds(room_id, DSCmdID.NID_PLAYER_HALFENTER_NTF, send_data) then
        return KernCode.LOGIC_FAILED
    end
    log_info("[DsaServlet][rpc_half_enter]->room_id:%s, player_id:%s", room_id, player.player_id)
    return SUCCESS
end

-- ds心跳通知
function DsaServlet:on_ds_heartbeat_req(ds_session, decode_hb, session_id)
    --log_info("[DsaServlet][on_ds_heartbeat_req")
    local ds_room_id = decode_hb.room_id
    local room_data = dsagent_mgr:get_room_info(ds_room_id)
    if not room_data then
        log_info("[DsaServlet][on_ds_heartbeat_req]->get room failed! token:%d, room_id:%s", ds_session.token, ds_room_id)
        return
    end

    if room_data.ds_pid ~= decode_hb.ds_pid then
        room_data.ds_pid = decode_hb.ds_pid
    end

    if room_data.ds_token ~= ds_session.token then
        room_data.ds_token = ds_session.token
    end

    if dsagent_mgr:get_ds_status(ds_room_id) < DsStatus.RUN then
        dsagent_mgr:update_ds_status(ds_room_id, DsStatus.RUN)
    end

    --心跳回应
    local ack_data = {ds_pid = decode_hb.ds_pid, room_id = decode_hb.room_id}
    client_mgr:callback_dx(ds_session, DSCmdID.NID_DS_HEARTBEAT_RES, ack_data)

    if not room_data.hb_time and room_data.players then
        -- 第一次心跳 将ds局内需要数据发送过去
        local send_data = {}
        send_data.room_id   = ds_room_id
        send_data.map_id    = room_data.map_id
        send_data.players   = room_data.players
        client_mgr:send_dx(ds_session, DSCmdID.NID_DS_CREATEMATCH_REQ, send_data)
        log_debug("[DsaServlet][on_ds_heartbeat_req]->send_data:%s", serialize(send_data))
    end
    room_data.hb_time = os.time()
end

-- ds通知玩家进入
function DsaServlet:on_ds_ntf_player_login(ds_session, ntf, session_id)
    local room_id, player_id = ntf.room_id, ntf.player_id
    local room_data = dsagent_mgr:get_room_info(room_id)
    if not room_data then
        log_err("[DsaServlet][on_ds_ntf_player_login]->get room failed! room_id:%s", room_id)
        return
    end

    dsagent_mgr:update_loading_data(player_id, ntf.team_index)

    dsagent_mgr:update_player_status(room_id, player_id, MemberStatus.ENTERD)
end

-- ds通知玩家登出
function DsaServlet:on_ds_ntf_player_logout(ds_session, ntf, session_id)
    local room_id, player_id = ntf.room_id, ntf.player_id
    dsagent_mgr:update_player_status(room_id, player_id, MemberStatus.EXIT)

    event_mgr:notify_trigger("user_leave_game", player_id)
end

-- ds通知玩家断连
function DsaServlet:on_ds_ntf_player_disconnct(ds_session, ntf, session_id)
    local room_id, player_id = ntf.room_id, ntf.player_id
    dsagent_mgr:update_player_status(room_id, player_id, MemberStatus.OFFLINE)

    event_mgr:notify_trigger("user_disconnect", player_id)
end

-- ds游戏结算请求
function DsaServlet:on_ds_game_end_settle(ds_session, resp, session_id)
    local room_id = resp.room_id
    local room_data = dsagent_mgr:get_room_info(room_id)
    if not room_data then
        log_err("[DsaServlet][on_ds_game_end_settle]->get room  room_id:%s", room_id)
        return
    end

    log_info("[DsaServlet][on_ds_game_end_settle]->resp:%s", serialize(resp))

    local settle_data = tcopy(resp)
    settle_data.json_data = json_decode(settle_data.json_data)
    dsagent_mgr:battle_finish(room_id, settle_data)

    dsagent_mgr:update_ds_status(room_id, DsStatus.SETTLE)

    timer_mgr:once(PeriodTime.MINUTE_MS, function()
        self:delay_ntf_ds_exit(room_id)
    end)
end

-- 延迟通知ds退出
function DsaServlet:delay_ntf_ds_exit(room_id)
    log_info("[DsaServlet][delay_ntf_ds_exit]->room_id:%s", room_id)
    dsagent_mgr:ntf_ds_exit(room_id, DsExit.BATLLE_FINISH)

    -- 注:DS收到退出通知执行退出操作后ds镜像状态转为Exited，延迟清理一下，目前只处理正常结算的DS镜像
    timer_mgr:once(PeriodTime.SECOND_3_MS, function()
        self:delay_rm_ds_image(room_id)
    end)
end

-- 延迟删除通知ds镜像
function DsaServlet:delay_rm_ds_image(room_id)
    if quanta.platform == "windows" then
        return
    end

    local image_name = "dssvr_" .. room_id
    local docker_params = sformat("docker rm %s", image_name)
    local flag, str_err = oexec(docker_params)
    if not flag then
        log_err("[DsaServlet][delay_rm_ds_image]->exec docker failed: %s!", str_err)
        return
    end
    log_info("[DsaServlet][delay_rm_ds_image]->rm ds image:%s", image_name)
end

function DsaServlet:rpc_enter_ds_watch(room_id, fighter, watcher, token)
    log_debug("[DsaServlet][rpc_enter_ds_watch]->room_id:%s, fighter:%s, watcher:%s, token:%s", room_id, fighter, watcher, token)
    local room_data = dsagent_mgr:get_room_info(room_id)
    if not room_data then
        return RoomCode.ROOM_ERR_NOT_EXIST
    end

    local ntf_data = {}
    ntf_data.fighter_id = fighter
    ntf_data.wathcer_id = watcher
    ntf_data.token      = token
    if not dsagent_mgr:send_to_ds(room_id, DSCmdID.NID_PLAYER_ENTER_WATCH_NTF, ntf_data) then
        return KernCode.LOGIC_FAILED
    end

    local loading_data = dsagent_mgr:get_loading_data()

    return SUCCESS, loading_data
end

function DsaServlet:on_ds_exit_watch(ds_session, ntf, session_id)
    local room_id, player_id = ntf.room_id, ntf.player_id
    log_debug("[DsaServlet][on_ds_exit_watch]->room_id:%s, player_id:%s", room_id, player_id)
    local room_data = dsagent_mgr:get_room_info(room_id)
    if not room_data then
        log_err("[DsaServlet][on_ds_exit_watch]->get room failed! room_id:%s", room_id)
        return
    end

    local ok, _ = router_mgr:call_target(room_data.from_svr_id, "rpc_exit_watch", room_id, player_id)
    if not ok then
        log_err("[DsaServlet][on_ds_exit_watch]->rpc exec failed! room_id:%s, player_id:%s", room_id, player_id)
    end
end

-- 玩家武器经验变化
function DsaServlet:on_ds_player_weapon_add_exp(ds_session, ntf, session_id)
    local room_id, player_id = ntf.room_id, ntf.player_id
    local room_data = dsagent_mgr:get_room_info(room_id)
    if not room_data then
        log_err("[DsaServlet][on_ds_player_weapon_add_exp]->get room failed! room_id:%s", room_id)
        return
    end

    -- 策划要求：自定义房间不给武器增加经验
    if guid_room_mode(guid_group(room_id)) == GameMode.ROOM then
        return
    end

    local player_data = room_data.players[player_id]
    if not player_data then
        log_err("[DsaServlet][on_ds_player_weapon_add_exp]->get player failed! room_id:%s, player_id:%s", room_id, player_id)
        return
    end

    if not player_data.weapon_exp_info then
        player_data.weapon_exp_info = {}
    end
    local weapon_exp_info = player_data.weapon_exp_info

    local weapon_uuid = ntf.weapon_uid
    local extra_add_exp = weapon_exp_info[weapon_uuid] or 0
    weapon_exp_info[weapon_uuid] = ntf.add_exp  + extra_add_exp
end

function DsaServlet:build_robot_weapons(robot_role_id)
    local role_cfg = role_db:find_one(robot_role_id)
    if not role_cfg then
        log_err("[DsaServlet][build_robot_weapons]get role cfg failed! role_id:%s", robot_role_id)
        return
    end

    return {
                grenades            = {role_cfg.default_weapon4, role_cfg.default_weapon5},
                primary_components  = {},
                primary_id          = role_cfg.default_weapon1,
                secondary_uuid      = new_guid(),
                primary_level       = 1,
                secondary_id        = role_cfg.default_weapon2,
                primary_uuid        = new_guid(),
                secondary_components= {},
                secondary_exp       = 0,
                primary_exp         = 0,
                secondary_level     = 1,
                melee_id            = role_cfg.default_weapon3
            }
end

-- 成就达成
function DsaServlet:on_ds_achieve_reach_ntf(ds_session, ntf, session_id)
    local room_id, achievements = ntf.room_id, ntf.achievements
    log_debug("[DsaServlet][on_ds_achieve_reach_ntf]->room_id:%s, achievements:%s", room_id, serialize(achievements))
    local room_data = dsagent_mgr:get_room_info(room_id)
    if not room_data then
        log_err("[DsaServlet][on_ds_achieve_reach_ntf]->get room failed! room_id:%s", room_id)
        return
    end

    local ok, _ = router_mgr:call_target(room_data.from_svr_id, "rpc_achieve_reach", room_id, achievements)
    if not ok then
        log_err("[DsaServlet][on_ds_achieve_reach_ntf]->rpc exec failed! room_id:%s, achievements:%s", room_id, serialize(achievements))
    end
end

-- 战斗数据上报
function DsaServlet:on_ds_battle_data_ntf(ds_session, ntf, session_id)
    local room_id, battle_datas = ntf.room_id, ntf.battle_datas
    log_debug("[DsaServlet][on_ds_battle_data_ntf]->room_id:%s, battle_datas:%s", room_id, serialize(battle_datas))
    local room_data = dsagent_mgr:get_room_info(room_id)
    if not room_data then
        log_err("[DsaServlet][on_ds_battle_data_ntf]->get room failed! room_id:%s", room_id)
        return
    end

    local ok, _ = router_mgr:call_target(room_data.from_svr_id, "rpc_battle_update", room_id, battle_datas)
    if not ok then
        log_err("[DsaServlet][on_ds_battle_data_ntf]->rpc exec failed! room_id:%s, battle_datas:%s", room_id, serialize(battle_datas))
    end
end

quanta.dsa_servlet = DsaServlet()

return DsaServlet
