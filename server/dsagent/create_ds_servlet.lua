--create_ds_servlet.lua
local otime         = os.time
local odate         = os.date
local lmkdir        = lfs.mkdir
local oexec         = os.execute
local iopen         = io.open
local popen         = io.popen
local log_err       = logger.err
local log_warn      = logger.warn
local log_info      = logger.info
local log_debug     = logger.debug
local env_get       = environ.get
local env_addr      = environ.addr
local sformat       = string.format
local tsize         = table_ext.size
local ssplit        = string_ext.split
local serialize     = logger.serialize
local check_failed  = utility.check_failed

local monitor       = quanta.get("monitor")
local timer_mgr     = quanta.get("timer_mgr")
local event_mgr     = quanta.get("event_mgr")
local dsagent_mgr   = quanta.get("dsagent_mgr")


local config_mgr    = quanta.get("config_mgr")
local map_table     = config_mgr:init_table("mapcfg", "id")

local DelRoom       = enum("DelRoom")
local DsStatus      = enum("DsStatus")
local PeriodTime    = enum("PeriodTime")
local MemberStatus  = enum("MemberStatus")
local DsCode        = enum("DsCode")
local KernCode      = enum("KernCode")

local DSCmdID       = ncmd_ds.DsCmdId

local slash         = "/"

local CreateDsServlet = singleton()
function  CreateDsServlet:__init()
    self.ds_images = {}
    self:setup()
end

function  CreateDsServlet:setup()
    -- 监听rpc
    event_mgr:add_listener(self, "rpc_create_ds")

    -- ds回复create match
    event_mgr:add_cmd_listener(self, DSCmdID.NID_DS_CREATEMATCH_RES, "on_ds_create_match_resp")

    timer_mgr:loop(PeriodTime.SECOND_2_MS, function()
        self:on_timer_check()
    end)

    if quanta.platform == "linux" then
        self:on_timer_read_ds_image()
        timer_mgr:loop(PeriodTime.MINUTE_MS, function()
            self:on_timer_read_ds_image()
        end)

        -- 创建DS日志/CoreDump目录
        self.ds_log_path = env_get("QUANTA_LOG_PATH") .. slash .. "ds_logs"
        --self.ds_core_path = env_get("QUANTA_LOG_PATH") .. slash .. "ds_core"
        lmkdir(self.ds_log_path)
        --lmkdir(self.ds_core_path)
    end
end

-- rpc拉起ds请求
function CreateDsServlet:rpc_create_ds(room_svr_id, info)
    local room_id = info.room_id
    log_info("[CreateDsServlet][rpc_create_ds]->room_id:%s, room_svr_id:%s, info:%s", room_id, room_svr_id, serialize(info))

    if dsagent_mgr:get_room_info(room_id) then
        log_err("[CreateDsServlet][rpc_create_ds]->room id repeat! room_id:%s", room_id)
        return DsCode.DS_ERR_REPEATE_CREATE
    end

    for _, data in pairs(info.players) do
        data.status = MemberStatus.WAIT_ENTER
    end

    -- 执行创建ds进程逻辑
    local ntf_client_port = dsagent_mgr:get_ds_out_port() or 0
    local _, dsa_listen_port = env_addr("QUANTA_DSA_ADDR")

    local map_id = info.map_id
    local battle_map = map_table:find_one(map_id)
    if not battle_map then
        log_err("[CreateDsServlet][rpc_create_ds]->find map cfg failed! map_id:%s", map_id)
        return KernCode.PARAM_ERROR
    end

    -- NoSteam内测版启动，Steam版本需要去掉
    local log_name = sformat("%s_%s.log", odate("%Y-%m-%d-%H-%M-%S", otime()), room_id)
    local param_string = sformat(" %s?MAPID=%d?MaxPlayers=%d?bIsLanMatch -log -DPORT=%d -PORT=%d -ROOMID=%d -NoSteam -NoVerifyGC LOG=%s",
                battle_map.loading, map_id, tsize(info.players), dsa_listen_port, ntf_client_port, room_id, log_name)
    log_debug("[CreateDsServlet][rpc_create_ds]->param_string->%s", param_string)

    local room_data = {}
    room_data.from_svr_id   = room_svr_id
    room_data.map_id        = info.map_id
    room_data.match_id      = info.match_id
    room_data.ds_status     = DsStatus.CREATE
    room_data.ntf_client_port = ntf_client_port
    room_data.ds_cmd_params = param_string
    room_data.battle_map    = battle_map
    room_data.players       = {}
    for _, player_info in pairs(info.players) do
        local player_id = player_info.player_id
        if not player_info.robot and not player_info.ds_roles then
            log_err("[CreateDsServlet][rpc_create_ds]->ds_roles not exist! player_id:%s, role_id:%s", player_id, player_info.hero)
            return KernCode.PARAM_ERROR
        end
        room_data.players[player_id] = player_info
    end

    local create_code
    if quanta.platform == "windows" then
        create_code = self:windosw_create_ds(room_data, param_string)
    else
        create_code = self:linux_create_ds(room_data, param_string, info, room_id, map_id, ntf_client_port)
    end

    if check_failed(create_code) then
        log_err("[CreateDsServlet][rpc_create_ds]->create ds failed!->room_id:%s, err_code:%s", room_id, create_code)
        return create_code
    end

    room_data.create_time   = otime()
    dsagent_mgr:create_room_info(room_id, room_data)

    return KernCode.SUCCESS
end

function CreateDsServlet:linux_create_ds(room_data, param_string, info, room_id, map_id, ntf_client_port)
    if not monitor then
        log_err("[CreateDsServlet][rpc_create_ds]->failed! monitor is nil")
        return DsCode.DS_ERR_CREATE_FAILURE
    end

    local version = info.version
    if not version then
        log_err("[CreateDsServlet][rpc_create_ds]->failed! get version failed!")
        return DsCode.DS_ERR_CREATE_FAILURE
    end

    -- 检查docker镜像存在
    local image = self.ds_images[version]
    if not image then
        log_err("[CreateDsServlet][rpc_create_ds]->failed! get ds image failed! version:%s", version)
        return DsCode.DS_ERR_IMAGE_NOT_EXIST
    end

    -- 创建日期目录
    local cur_time = otime()
    local date_path = odate("%Y_%m_%d", cur_time)
    lmkdir(self.ds_log_path .. slash .. date_path)

    local detail_time = odate("%H_%M_%S", cur_time)
    -- 每个DS以房间ID创建一个目录，用来存放该DS产生的日志/配置/Crash文件
    local temp_path = detail_time .. "_" .. room_id .. "_" .. map_id
    local room_path = self.ds_log_path .. slash .. date_path .. slash .. temp_path
    lmkdir(room_path)

    local docker_name = sformat(" --name=dssvr_%s", room_id)
    local ntf_client_host = env_get("QUANTA_HOST_IP")
    local docker_ports = sformat(" -p %d:%d/udp", ntf_client_port, ntf_client_port)
    local docker_volume = sformat(" -v %s:/data/ds/LinuxServer/PM/Saved/", room_path)
    local docker_env = sformat(" -e QUANTA_HOST_IP=%s", ntf_client_host)
    local docker_params = sformat("docker run -it -d %s %s %s %s %s %s\n", docker_name, docker_ports, docker_volume, docker_env, image, param_string)
    local flag, str_err = oexec(docker_params)
    if not flag then
        log_err("[CreateDsServlet][rpc_create_ds]->failed! str_err:%s", str_err)
        return DsCode.DS_ERR_CREATE_FAILURE
    end

    room_data.docker_image = image
    room_data.docker_name = docker_name
    return KernCode.SUCCESS
end

function CreateDsServlet:windosw_create_ds(room_data, param_string)
    local ds_exec = env_get("QUANTA_DS_PATH")
    local ds_file = iopen(ds_exec, "r")
    if not ds_file then
        log_err("[CreateDsServlet][windosw_create_ds]->ds_exec is not exist! ds_exec:%s", ds_exec)
        return DsCode.DS_ERR_CREATE_FAILURE
    end
    ds_file:close()
    local command = "start " .. ds_exec .. " " .. param_string
    --local flag, str_err = util.system(command)
    local flag, str_err = os.execute(command)
    if not flag then
        log_err("[CreateDsServlet][windosw_create_ds]->failed! str_err:%s", str_err)
        return DsCode.DS_ERR_CREATE_FAILURE
    end

    room_data.docker_image = ""
    return KernCode.SUCCESS
end

-- 拉起DS超时
function CreateDsServlet:on_timer_check()
    local overtimer_rooms = {}
    local cur_time = otime()
    local rooms = dsagent_mgr:get_all_rooms()
    for room_id, data in pairs(rooms) do
        if (not data.ds_status or data.ds_status == DsStatus.CREATE) and
           (not data.create_time or cur_time > data.create_time + 10) then
            overtimer_rooms[room_id] = 1
        end
    end
    for room_id, _ in pairs(overtimer_rooms) do
        local room_data = dsagent_mgr:get_room_info(room_id)
        if room_data then
            log_warn("[CreateDsServlet][on_timer_check]->ds over time no reponse! room_id:%s, create_time:%s", room_id, room_data.create_time)
            dsagent_mgr:update_ds_status(room_id, DsStatus.UNCONNECT)
            dsagent_mgr:ntf_roomsvr_destroy(room_id)
            dsagent_mgr:del_room_info(room_id, DelRoom.CREATE_OVERTIME)
        end
    end
end

-- ds回复create match
function CreateDsServlet:on_ds_create_match_resp(ds_session, resp, session_id)
    log_info("[CreateDsServlet][on_ds_create_match_resp]")
    dsagent_mgr:create_match_resp(ds_session, resp)
end

-- 读取ds镜像
function CreateDsServlet:on_timer_read_ds_image()
    local images_file = popen("docker images | grep admin_ds | awk '{print $1}'")
    if nil == images_file then
        log_err("[CreateDsServlet][on_timer_read_ds_image]->exec popen failed!")
        return
    end
    self.ds_images = {}
    for line in images_file:lines() do
        local options = ssplit(line, "_")
        if options and options[3] then
            self.ds_images[options[3]] = line
        end
    end
    images_file:close()
    --log_info("CreateDsServlet:on_timer_read_ds_image->ds_images:%s", serialize(self.ds_images))
end

quanta.create_ds_servlet = CreateDsServlet()

return CreateDsServlet
