--rebuild_servlet.lua
local tinsert       = table.insert
local log_err       = logger.err
local log_info      = logger.info
local serialize     = logger.serialize

local event_mgr     = quanta.get("event_mgr")
local router_mgr    = quanta.get("router_mgr")
local dsagent_mgr   = quanta.get("dsagent_mgr")
local thread_mgr    = quanta.get("thread_mgr")
local rebuild_mgr   = quanta.get("rebuild_mgr")

local RebuildServlet = singleton()

-- 初始化
function RebuildServlet:__init()
    --rpc
    event_mgr:add_listener(self, "rebuild_dsa_from_room")
    event_mgr:add_listener(self, "rebuild_dsa_from_dsc")
    event_mgr:add_listener(self, "rpc_replicate_room_data")

    -- 关注服务器注册事件
    router_mgr:watch_service_ready(self, "room")
    router_mgr:watch_service_ready(self, "dscenter")
end

-- 收到来自room服务器的数据进行重建
function RebuildServlet:rebuild_dsa_from_room(room_svr_id, room_list)
    if not room_list or not room_svr_id then
        log_err("[RebuildServlet][rebuild_dsa_from_room]->param error!")
        return
    end
    dsagent_mgr:rebuild_from_room(room_svr_id, room_list)
    --log_info("[RebuildServlet][rebuild_dsa_from_room]->room_svr:%d, room_list:%s", room_svr_id, serialize(room_list))
end

-- 收到来自dsc服务器的数据进行重建
function RebuildServlet:rebuild_dsa_from_dsc(dsc_id, room_info)
    if not dsc_id or not room_info then
        log_err("[RebuildServlet][rebuild_dsa_from_dsc]->param error!")
        return
    end
    dsagent_mgr:rebuild_from_dsc(dsc_id, room_info)
    log_info("[RebuildServlet][rebuild_dsa_from_dsc]->dsc_id:%d, room_info:%s", dsc_id, serialize(room_info))
end

--服务器注册处理
function RebuildServlet:on_service_ready(id, service_name)
    log_info("[RebuildServlet][on_service_ready]->id:%d, service_name:%s", id, service_name)
    if service_name == "roomsvr" then
        local replicate_rooms = rebuild_mgr:get_replicate_rooms()
        if not next(replicate_rooms) then
            return
        end
        thread_mgr:fork(function()
            for room_id, room_data in pairs(replicate_rooms) do
                local ok, err = router_mgr:call_target(id, "rebuild_room_svr", quanta.id, room_data)
                if not ok then
                    log_err("[RebuildServlet][on_service_ready] rebuild room failed! room_id:%s, err:%s", room_id, err)
                end
            end
        end)
    elseif service_name == "dscenter" then
        local send_info = {}
        for rid, data in pairs(dsagent_mgr:get_all_rooms()) do
            tinsert(send_info, dsagent_mgr:build_report_dsc(rid))
        end
        -- 没有数据就不用重建了
        if not next(send_info) then
            return
        end
        thread_mgr:fork(function()
            while true do
                thread_mgr:sleep(500)
                local ok, err = router_mgr:call_target(id, "rebuild_dsc_svr", quanta.id, send_info)
                if ok then
                    log_info("[RebuildServlet][on_service_ready] to dscenter success!")
                    break
                else
                    log_err("[RebuildServlet][on_service_ready] to dscenter failed!", err)
                end
            end
        end)
    end
end

function RebuildServlet:rpc_replicate_room_data(room_copy)
    --log_info("[RebuildServlet][rpc_replicate_room_data]->room_copy:%s", serialize(room_copy))
    rebuild_mgr:add_replicate_room(room_copy)
end

quanta.rebuild_servlet = RebuildServlet()

return RebuildServlet
