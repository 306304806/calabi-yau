--share.lua

import("constant/game_code.lua")
import("constant/game_const.lua")
local mrandom       = math.random
local tsort         = table.sort

local config_mgr    = quanta.get("config_mgr")
local name_db       = config_mgr:init_table("name", "id")
local name_frist    = name_db:select({type = 1})
local name_second   = name_db:select({type = 2})
local name_three    = name_db:select({type = 3})
local name_four     = name_db:select({type = 4})
local name_five     = nil

function quanta.random_name()
    local first = name_frist[mrandom(#name_frist)]
    local second = name_second[mrandom(#name_second)]
    local three = name_three[mrandom(#name_three)]
    return first.name .. second.name .. three.name
end

function quanta.random_robot_name(second_idx)
    if not name_five then
        name_five = name_db:select({type = 5})
        tsort(name_five, function (item1, item2) return item1.id < item2.id end)
    end
    local first = name_four[mrandom(#name_four)]
    second_idx = name_five[second_idx] and second_idx or mrandom(#name_five)
    local second = name_five[second_idx]
    return first.name .. second.name
end