-- standings_dao.lua
local log_err       = logger.err
local serialize     = logger.serialize
local check_failed  = utility.check_failed

local DBGroup       = enum("DBGroup")

local db_agent      = quanta.get("db_agent")

local StandingsDao = class()
function StandingsDao:__init()
end

function StandingsDao:load_standings(room_id)
    local query = { "standings", {room_id = room_id}, {_id = 0} }
    local ok, code, res = db_agent:find_one(room_id, query, DBGroup.HASH, room_id)
    if not ok or check_failed(code) then
        log_err("[StandingsDao][load_standings] failed! code: %s, res: %s", code, serialize(res))
        return
    end
    if not res or not res.standings then
        return
    end
    return res
end

function StandingsDao:update_standings(room_id, data)
    local query = { "standings", data, {room_id = room_id}, true }
    local ok, code, res = db_agent:update(room_id, query, DBGroup.HASH, room_id)
    if not ok or check_failed(code) then
        log_err("[StandingsDao][update_standings] failed! code: %s, res: %s", code, serialize(res))
        return false
    end
    return true
end

function StandingsDao:delete_standings(room_id)
    local query = {"standings", { room_id = room_id }, true}
    local ok, code, res = db_agent:delete(room_id, query, DBGroup.HASH, room_id)
    if not ok or check_failed(code) then
        log_err("[StandingsDao][delete_standings] failed! code: %s, res: %s", code, serialize(res))
        return false
    end
    return true
end

-- export
quanta.standings_dao = StandingsDao()

return StandingsDao
