-- achieve_dao.lua
local log_err       = logger.err
local log_info      = logger.info
local serialize     = logger.serialize
local check_failed  = utility.check_failed
local get_time_ms   = quanta.get_time_ms

local db_agent      = quanta.get("db_agent")

local AchieveDao = class()
function AchieveDao:__init()
end

function AchieveDao:load_achieve_count(selector)
    local now_ms = get_time_ms()
    local ok, code, res = db_agent:count(1, { "player_achieve", selector})
    if not ok or check_failed(code) then
        log_err("[AchieveDao][load_achieve_count] failed: code: %s", code, serialize(res))
        return false
    end

    log_info("[AchieveDao][load_achieve_count] success: count: %s, cost: %s", res, get_time_ms() - now_ms)
    return true, res
end

-- export
quanta.achieve_dao = AchieveDao()

return AchieveDao
