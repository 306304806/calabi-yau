--strongest_dao.lua

local log_err       = logger.err
local check_failed  = utility.check_failed

local DBGroup       = enum("DBGroup")
local GLOBAL_DB     = 1

local db_agent      = quanta.get("db_agent")

local StrongestDao  = singleton()

function StrongestDao:__init()
end

-- 加载最强王者玩家数据
function StrongestDao:load_strongest_players(season)
    local query = { "global_strongest_player", { season = season }, {_id = 0}}
    local ok, code, res = db_agent:find_one(quanta.id, query, DBGroup.GLOBAL, GLOBAL_DB)
    if not ok or check_failed(code) then
        log_err("[StrongestDao][load_strongest_players] failed! code: %s, res: %s", code, res)
        return false
    end
    return true, res or {}
end

-- 更新最强王者玩家数据
function StrongestDao:update_strongest_players(season, data)
    local query = { "global_strongest_player", data, { season = season }, true }
    local ok, code, res = db_agent:update(quanta.id, query, DBGroup.GLOBAL, GLOBAL_DB)
    if not ok or check_failed(code) then
        log_err("[StrongestDao][update_strongest_players] failed! code: %s, res: %s", code, res)
        return false
    end
    return true
end

quanta.strongest_dao = StrongestDao()

return StrongestDao