--achievementrank_mgr.lua
local otime                 = os.time
local mceil                 = math.ceil
local log_err               = logger.err
local log_info              = logger.info
local get_time_ms           = quanta.get_time_ms

local event_mgr             = quanta.get("event_mgr")
local router_mgr            = quanta.get("router_mgr")
local timer_mgr             = quanta.get("timer_mgr")
local achieve_dao           = quanta.get("achieve_dao")
local config_mgr            = quanta.get("config_mgr")

local PeriodTime            = enum("PeriodTime")
local achievement_rank_db   = config_mgr:init_table("achievementrank", "type", "seg")

local AchievementMgr = singleton()

function AchievementMgr:__init()
    self:setup()
end

function AchievementMgr:setup()
    self:init_rank()

    -- 启动延时去数据库拉取排行数据
    self.load_timer = timer_mgr:once(PeriodTime.SECOND_30_MS, function()
        self:refresh_rank()
    end)

    -- ss
    event_mgr:add_listener(self, "rpc_get_achieve_rank")
end

function AchievementMgr:init_rank()
    self.big_seg_rates      = {}
    self.small_seg_rates    = {}
    self.big_seg_counts     = {}
    self.small_seg_counts   = {}
    self.totals             = {}
end

-- 加载排行数据
function AchievementMgr:refresh_rank()
    self:init_rank()
    local now_ms = get_time_ms()
    local field_name = {"medal_cnt", "epic_cnt"}
    for _, item in achievement_rank_db:iterator() do
        local field = field_name[item.type]
        local selector = {}
        selector[field] = { ["$gte"] = item.min_val, ["$lte"] = item.max_val}
        local ok, count = achieve_dao:load_achieve_count(selector)
        if not ok then
            count = 0
        end
        self.totals[item.type] = self.totals[item.type] or 0
        self.totals[item.type] = self.totals[item.type] + count
        self.small_seg_counts[item.type] = self.small_seg_counts[item.type] or {}
        self.small_seg_counts[item.type][item.seg] = count
    end

    -- 计算出大段
    for achieve_type, small_seg_count in pairs(self.small_seg_counts) do
        self.big_seg_counts[achieve_type] = {small_seg_count[1]}
        local big_seg_count = self.big_seg_counts[achieve_type]
        for seg = 2, #small_seg_count, 1 do
            big_seg_count[seg] = big_seg_count[seg - 1] + small_seg_count[seg]
        end
    end

    -- 计算出大段和小段的占比单位
    for _, item in achievement_rank_db:iterator() do
        local big_seg_rate, small_seg_rate = 0, 0
        local total = self.totals[item.type]
        if total > 0 then
            big_seg_rate = self.big_seg_counts[item.type][item.seg] / total
            small_seg_rate = self.small_seg_counts[item.type][item.seg] / total / (item.max_val - item.min_val)
        end

        self.big_seg_rates[item.type] = self.big_seg_rates[item.type] or {}
        self.big_seg_rates[item.type][item.seg] = big_seg_rate
        self.small_seg_rates[item.type] = self.small_seg_rates[item.type] or {}
        self.small_seg_rates[item.type][item.seg] = small_seg_rate
    end

    local time = otime()
    local next_update_time = PeriodTime.DAY_S - ((time + PeriodTime.HOUR_S * 8) % PeriodTime.DAY_S)
    log_info("[AchievementMgr][load_data] success! cost = %s, next_update_time = %s", get_time_ms() - now_ms, next_update_time)
    timer_mgr:once(next_update_time * 1000, function() self:refresh_rank() end)

    router_mgr:call_lobby_all("rpc_achieve_rank_reset")
end

-- 获取排名
function AchievementMgr:rpc_get_achieve_rank(achieve_type, count)
    log_info("[AchievementMgr][rpc_get_achieve_rank] achieve_type = %s, count = %s", achieve_type, count)
    local item = self:binary_search(achieve_type, count)
    if not item then
        log_err("[AchievementMgr][rpc_get_achieve_rank] item = nil!")
        return 0
    end

    local val = count - item.min_val
    local rate = val * self.small_seg_rates[achieve_type][item.seg]
    if item.seg > 1 then
        rate = rate + self.big_seg_rates[achieve_type][item.seg - 1]
    end
    return rate
end

-- 二分查找属于哪一段
function AchievementMgr:binary_search(achieve_type, count)
    local left = 1
    local right = #self.big_seg_rates[achieve_type]
    local mid = mceil((left + right)/2)
    while left <= mid do
        local item = achievement_rank_db:find_one(achieve_type, mid)
        if item.min_val <= count and item.max_val >= count then
            return item
        end

        if item.max_val < count then
            left = mid + 1
        else
            right = mid - 1
        end

        mid = mceil((left + right)/2)
    end
end

quanta.achievement_mgr = AchievementMgr()

return AchievementMgr
