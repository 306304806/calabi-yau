--standings_mgr.lua
local log_err           = logger.err
local log_info          = logger.info
local log_debug         = logger.debug
local serialize         = logger.serialize

local event_mgr         = quanta.get("event_mgr")
local standings_dao     = quanta.get("standings_dao")
local rmsg_standings    = quanta.get("rmsg_standings")
local QueueLRU          = import("container/queue_lru.lua")

local StandingsMgr = singleton()
local prop = property(StandingsMgr)
prop:accessor("standings", nil)

function StandingsMgr:__init()
    self.standings = QueueLRU(10000)
    --订阅事件
    event_mgr:add_listener(self, "rpc_record_standings")
    event_mgr:add_listener(self, "rpc_get_standings")
end

function StandingsMgr:rpc_record_standings(room_id)
    local ret_data = rmsg_standings:list_message(room_id)
    if not ret_data then
        log_err("[StandingsMgr][rpc_record_standings] find list failed! room_id:%s", room_id)
        return
    end
    rmsg_standings:delete_message(room_id, ret_data.uuid)
    for _, record in pairs(ret_data) do
        local data = {
            room_id = room_id,
            standings = record.body.standings,
        }
        self.standings:set(room_id, data)
        standings_dao:update_standings(room_id, data)
        log_info("[StandingsMgr][rpc_record_standings] room_id=%s,data=%s", room_id, serialize(data))
    end
end

function StandingsMgr:rpc_get_standings(room_id)
    local cdata = self.standings:get(room_id)
    if cdata then
        log_debug("[StandingsMgr][rpc_get_standings] success room_id=%s", room_id)
        return cdata.standings
    end
    local data = standings_dao:load_standings(room_id)
    if not data then
        log_err("[StandingsMgr][rpc_get_standings] load standings failed room_id=%s", room_id)
        return
    end
    self.standings:set(room_id, data)
    log_debug("[StandingsMgr][rpc_get_standings] success room_id=%s", room_id)
    return data.standings
end

-- export
quanta.standings_mgr = StandingsMgr()

return StandingsMgr
