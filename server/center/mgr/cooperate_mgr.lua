---cooperate_mgr.lua
local tinsert       = table.insert
local mrandom       = math.random
local log_err       = logger.err
local tunpack       = table.unpack

local event_mgr     = quanta.get("event_mgr")
local timer_mgr     = quanta.get("timer_mgr")
local router_mgr    = quanta.get("router_mgr")

local GMType        = enum("GMType")
local PeriodTime    = enum("PeriodTime")
local AdminMgr      = import("kernel/admin/admin_mgr.lua")

local CooperateMgr = singleton(AdminMgr)
local prop = property(CooperateMgr)
prop:accessor("lobby_online_count", {})  -- 在线人数

function CooperateMgr:__init()
    event_mgr:add_listener(self, "gm_get_online_count")
    event_mgr:add_listener(self, "rpc_gm_dispatch")
    event_mgr:add_listener(self, "rpc_report_online_count")
    event_mgr:add_listener(self, "gm_get_account_info")
    event_mgr:add_listener(self, "gm_set_log_level")

    --注册GM指令
    timer_mgr:once(PeriodTime.SECOND_MS, function()
        local cmd_list = {
            {gm_type = GMType.GLOBAL, name = "gm_get_online_count", desc = "获取在线人数", args = ""},
            {gm_type = GMType.GLOBAL, name = "gm_get_account_info", desc = "获取账号信息", args = "open_id|string area_id|number"},
            {gm_type = GMType.GLOBAL, name = "gm_set_log_level", desc = "设置日志等级", args = "svr_name|string level|number"},
        }
        self:report_cmd(cmd_list, quanta.id)
    end)
end

function CooperateMgr:rpc_gm_dispatch(cmd_args, gm_type)
    if gm_type then
        if gm_type == GMType.GLOBAL then
            return self:exec_global_cmd(tunpack(cmd_args))
        end
    else
        return self:exec_player_cmd(tunpack(cmd_args))
    end
    return {code = 1, msg = "err command"}
end

function CooperateMgr:exec_global_cmd(cmd_name, ...)
    local ok, res = tunpack(event_mgr:notify_listener(cmd_name, ...))
    if not ok then
        return {code = 1, msg = res}
    end
    return res
end

function CooperateMgr:exec_player_cmd(cmd_name, player_id, ...)
    if not player_id then
        return {code = 1, msg = "player_id not exist!"}
    end
    local ok_1, lobby = router_mgr:call_index_hash(player_id, "query_player_lobby", player_id)
    if not ok_1 then
		log_err("[CooperateMgr][exec_player_cmd] query_player_lobby failed! player_id=%s", player_id)
        return {code = 1, msg = "find lobby failed!" }
    end

    if lobby == 0 then
        local ok, res = router_mgr:call_lobby_hash(mrandom(1, 10), "rpc_gm_execute", cmd_name, player_id, ...)
        if not ok then
            log_err("[CooperateMgr][exec_player_cmd] call_lobby_hash rpc_gm_execute failed! player_id=%s", player_id)
            return {code = 1, msg = "exec gm failed!" }
        end
        return res
    end

    local ok_2, res = router_mgr:call_target(lobby, "rpc_gm_execute", cmd_name, player_id, ...)
    if not ok_2 then
        log_err("[CooperateMgr][exec_player_cmd] call_target rpc_gm_execute failed! player_id=%s", player_id)
        return {code = 1, msg = "exec gm failed!" }
    end
    return res
end

-- 上报在线人数
function CooperateMgr:rpc_report_online_count(online_count, service_id)
    self.lobby_online_count[service_id] = online_count
end

-- 获取在线人数
function CooperateMgr:gm_get_online_count()
    local data = {}
    for service_id, online_count in pairs(self.lobby_online_count) do
        tinsert(data, {scr_id = service_id, online_count = online_count})
    end
    return {code = 0, data = data}
end

-- 获取账号信息
function CooperateMgr:gm_get_account_info(open_id, area_id)
    local ok, res = router_mgr:call_lobby_hash(mrandom(1, 10), "rpc_gm_execute", "gm_get_account_info", open_id, area_id)
    if not ok then
        log_err("[CooperateMgr][gm_get_account_info] gm_get_account_info failed!")
        return {code = 1, msg = "gm_get_account_info failed" }
    end

    return res
end

-- 设置日志等级
function CooperateMgr:gm_set_log_level(svr_name, level)
    log_err("[CooperateMgr][gm_set_log_level] gm_set_log_level %s, %s", svr_name, level)
    if svr_name == "lobby" then
        local func = router_mgr["call_" .. svr_name .. "_all"]
        local ok, _ = func(router_mgr, "gm_set_log_level", level)
        if not ok then
            log_err("[CooperateMgr][gm_set_log_level] gm_set_log_level failed!")
            return {code = 1, msg = "gm_set_log_level failed" }
        end
        return { code = 0 }
    end

    return {code = 1, msg = "svr_name not support" }
end

quanta.cooperate_mgr = CooperateMgr()

return CooperateMgr
