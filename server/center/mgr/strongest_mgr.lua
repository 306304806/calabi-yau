--strongest_mgr.lua
local otime                 = os.time
local log_err               = logger.err
local log_info              = logger.info
local log_debug             = logger.debug

local plat_api              = quanta.get("plat_api")
local timer_mgr             = quanta.get("timer_mgr")
local router_mgr            = quanta.get("router_mgr")
local thread_mgr            = quanta.get("thread_mgr")
local strongest_dao         = quanta.get("strongest_dao")

local RmsgGame              = enum("RmsgGame")
local PeriodTime            = enum("PeriodTime")
local DivisionStars         = enum("DivisionStars")

local StrongestMgr = singleton()
function StrongestMgr:__init()
    self.cur_season      = nil        -- 所处赛季
    self.strongest_players  = {}    -- 最强王者相关数据
end

-- 加载最强王者的星星数据
function StrongestMgr:load_strongest_players_data()
    thread_mgr:success_call(PeriodTime.SECOND_MS, function()
        local ok, res = strongest_dao:load_strongest_players(self.cur_season)
        if ok and res then
            log_info("[StrongestMgr][load_strongest_players_data] success!")
            self.strongest_players = res.strongest_players or {}
            -- 加载完成启动扫描定时器
            timer_mgr:loop(PeriodTime.HOUR_MS, function()
                self:on_check_strongest_expired()
            end)
            return true
        end
        log_err("[StrongestMgr][load_strongest_players_data]->load failed!")
        return false
    end)
end

-- 保存最强王者的星星数据
function StrongestMgr:save_strongest_players_data()
    local cur_season = self.cur_season
    local save_data = { season = cur_season, strongest_players = self.strongest_players}
    if strongest_dao:update_strongest_players(cur_season, save_data) then
        log_err("[StrongestMgr][save_strongest_players_data] failed! season:%s", cur_season)
    end
end

-- 赛季更新
function StrongestMgr:update_new_season(rank_season)
    self.strongest_players = {}
    self.cur_season = rank_season
    self:load_strongest_players_data()
end

-- 定时扫描最强王者玩家星星数是否过期
function StrongestMgr:on_check_strongest_expired()
    local has_drop   = false
    local cur_time = otime()
    for player_id, data in pairs(self.strongest_players) do
        if cur_time > data.last_time + PeriodTime.WEEK_S then
            if not data.drop_time or cur_time > data.drop_time + PeriodTime.DAY_S then
                local new_star = data.cur_star - 1
                self:strongest_drop_star(player_id)
                self:update_player_star(player_id, new_star, cur_time)
                plat_api:offline_player_update(player_id, { stars = new_star })
                has_drop = true
            end
        end
    end
    if has_drop then
        self:save_strongest_players_data()
    end
end

-- 掉星处理
function StrongestMgr:strongest_drop_star(player_id)
    --写rmsg
    local drop_info = { player_id = player_id, drop_star = 1 }
    local ok_send = quanta.get("rmsg_drop_star"):send_message(quanta.name, player_id, RmsgGame.DROP_STAR, drop_info)
    if not ok_send then
        log_err("[StrongestMgr][strongest_drop_star]->save rmsg failed! player_id:%s", player_id)
        return false
    end
    --通知lobby
    router_mgr:call_index_hash(player_id, "transfer_message", player_id, "rpc_player_drop_star_ntf", player_id)
    return true
end

-- 最强王者玩家结束排位赛
function StrongestMgr:update_player_star(player_id, cur_star, drop_time)
    log_debug("[StrongestMgr][update_player_star]->player_id:%s, cur_star:%s, drop_time:%s", player_id, cur_star, drop_time)
    if cur_star < DivisionStars.STRONGEST then
        self.strongest_players[player_id] = nil
    else
        self.strongest_players[player_id] = { cur_star = cur_star, last_time = otime(), drop_time = drop_time}
    end
    self:save_strongest_players_data()
end

quanta.strongest_mgr = StrongestMgr()

return StrongestMgr
