--serial_mgr.la
local log_info      = logger.info
local tinsert       = table.insert
local tremove       = table.remove

local PeriodTime    = enum("PeriodTime")
local KernCode      = enum("KernCode")
local SUCCESS       = KernCode.SUCCESS

local event_mgr     = quanta.get("event_mgr")

local SerialMgr = singleton()
local prop = property(SerialMgr)
prop:accessor("serial_idle", {})
prop:accessor("serial_using", {})
prop:accessor("last_time", 0)

function SerialMgr:__init()
    --构建serial_idle
    for i = 0xffff, 1, -1 do
        tinsert(self.serial_idle, i)
    end
    --添加事件监听
    event_mgr:add_listener(self, "rpc_general_serial_id")
    event_mgr:add_listener(self, "rpc_recover_serial_id")
end

function SerialMgr:rpc_general_serial_id(room_id)
    local now_tick = quanta.now
    if now_tick > self.last_time + 10 then
        self.last_time = now_tick
        --回收超时的序列号
        local escape_tick = now_tick - PeriodTime.HOUR_S
        for serial_id, info in pairs(self.serial_using) do
            if escape_tick > info.tick then
                self:rpc_recover_serial_id(serial_id, info.room_id)
            end
        end
    end
    local serial_id = tremove(self.serial_idle)
    log_info("[RoomMgr][rpc_general_serial_id] serial_id=%s, room_id=%s", serial_id, room_id)
    self.serial_using[serial_id] = { tick = now_tick, room_id = room_id }
    return SUCCESS, serial_id
end

function SerialMgr:rpc_recover_serial_id(serial_id, room_id)
    log_info("[RoomMgr][rpc_recover_serial_id] serial_id=%s, room_id=%s", serial_id, room_id)
    if self.serial_using[serial_id] then
        tinsert(self.serial_idle, serial_id)
        self.serial_using[serial_id] = nil
    end
    return SUCCESS
end

quanta.serial_mgr = SerialMgr()

return SerialMgr
