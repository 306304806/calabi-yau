--strongest_servlet.lua

local log_info      = logger.info

local KernCode      = enum("KernCode")
local SUCCESS           = KernCode.SUCCESS

local event_mgr         = quanta.get("event_mgr")
local strongest_mgr     = quanta.get("strongest_mgr")

local StrongestServlet = singleton()
function StrongestServlet:__init()
    event_mgr:add_listener(self, "rpc_strongest_finish_qualifying")
    event_mgr:add_listener(self, "rpc_strongest_rank_season_ntf")
end

-- 最强王者玩家打完排位赛
function StrongestServlet:rpc_strongest_finish_qualifying(player_id, star)
    strongest_mgr:update_player_star(player_id, star)
    return SUCCESS
end

-- 通知当前赛季id
function StrongestServlet:rpc_strongest_rank_season_ntf(rank_season)
    log_info("[StrongestServlet][rpc_strongest_rank_season_ntf]-> ntf_season:%s", rank_season)
    strongest_mgr:update_new_season(rank_season)
    return SUCCESS
end

quanta.strongest_servlet = StrongestServlet()

return StrongestServlet