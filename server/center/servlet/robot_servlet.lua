---robot_servlet.lua
local new_guid          = guid.new
local mrandom           = math.random
local tinsert           = table.insert
local random_robot_name = quanta.random_robot_name

local event_mgr         = quanta.get("event_mgr")

local RobotServlet = singleton()
function RobotServlet:__init()
    event_mgr:add_listener(self, "rpc_create_robot")
end

function RobotServlet:create_robot(second_idx)
    return {
        vc_avatar_id= 0,
        vc_frame_id = 0,
        vc_border_id= 0,
        vc_achie_id = 0,
        level       = 1,
        robot       = true,
        rank        = mrandom(1, 10),
        icon        = mrandom(1, 10),
        sex         = mrandom(0, 10000) % 2,
        nick        = random_robot_name(second_idx),
        player_id   = new_guid(),
    }
end

function RobotServlet:rpc_create_robot(need, robot_indexs)
    local robots = {}
    for i = 1, need do
        local second_idx = robot_indexs and robot_indexs[i] or i
        local robot = self:create_robot(second_idx)
        tinsert(robots, robot)
    end
    return robots
end

quanta.robot_servlet = RobotServlet()

return RobotServlet
