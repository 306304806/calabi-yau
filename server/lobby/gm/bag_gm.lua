--bag_gm.lua
local log_debug     = logger.debug
local serialize     = logger.serialize
local log_err       = logger.err
local tinsert       = table.insert

local KernCode      = enum("KernCode")
local SUCCESS       = KernCode.SUCCESS

local ResAddType    = enum("ResAddType")
local ResDelType    = enum("ResDelType")
local ReddotType    = enum("ReddotType")
local ItemType      = enum("ItemType")

local gm_agent      = quanta.get("gm_agent")
local plat_api      = quanta.get("plat_api")
local event_mgr     = quanta.get("event_mgr")
local player_mgr    = quanta.get("player_mgr")
local item_util     = quanta.get("item_util")
local config_mgr    = quanta.get("config_mgr")

local utility_db    = config_mgr:get_table("utility")

local BagGM = singleton()
function BagGM:__init()
    --注册事件
    event_mgr:add_listener(self, "gm_add_item")
    event_mgr:add_listener(self, "gm_del_item")
    event_mgr:add_listener(self, "gm_add_weapon")
    event_mgr:add_listener(self, "gm_add_weapon_exp")
    event_mgr:add_listener(self, "gm_add_gun_exp")
    --注册GM
    self:register()
end

--GM服务器
function BagGM:register()
    local cmd_list = {
        {name = "gm_add_item", desc = "添加物品GM指令", args = "player_id|number item_id|number count|number"},
        {name = "gm_del_item", desc = "删除物品GM指令", args = "player_id|number item_id|number count|number"},
        {name = "gm_add_weapon", desc = "添加一把武器", args = "player_id|number weapon_id|number"},
        {name = "gm_add_weapon_exp", desc = "添加某种武器经验", args = "player_id|number weapon_id|number exp|number"},
        {name = "gm_add_gun_exp",    desc = "添加单个武器经验", args = "player_id|number role_id|number slot_pos|number exp|number"},
    }
    gm_agent:insert_cmd(cmd_list)
end

-- 添加物品
function BagGM:gm_add_item(player_id, item_id, count)
    --log_debug("[BagGM][gm_add_item]->player_id:%s, item_id:%s, count:%s", player_id, item_id, count)
    local player = player_mgr:get_player(player_id)
    if not player then
        log_err("[BagGM][gm_add_item]->get player failed! player_id:%s", player_id)
        return {name = "gm_add_item", msg = "玩家不存在!", code = 1}
    end
    if not item_id or not count then
        log_err("[BagGM][gm_add_item]->param error! player_id:%s, item_id:%s, count:%s", player_id, item_id, count)
        return {name = "gm_add_item", msg = "玩家不存在!", code = 1}
    end

    local item_type = item_util:get_item_type(item_id)
    if item_type == ItemType.BagItem_NormalBP then
        log_err("[BagGM][gm_add_item]->not support add normal battlepass!")
    elseif item_type == ItemType.BagItem_AdvanceBP then
        player:gain_battlepass_vip(0)
    elseif item_type == ItemType.BagItem_AdvanceBPPlus then
        player:gain_battlepass_vip(utility_db:find_value("value", "plus_give_explores"))
    elseif item_type == ItemType.BagItem_Expolre then
        player:add_explore(count)
    else
        local ret, result_list = player:add_item(item_id, count, ResAddType.GM)
        if not ret then
            return {name = "gm_add_item", msg = "添加物品失败!", code = 1}
        end
        log_debug("[BagGM][gm_add_item]->player_id:%s, item_id:%s, count:%s, result_list:%s", player_id, item_id, count, serialize(result_list))
    end

    return {name = "gm_add_item", code = 0}
end

-- 删除物品
function BagGM:gm_del_item(player_id, item_id, count)
    local player = player_mgr:get_player(player_id)
    if not player then
        return {name = "gm_del_item", msg = "玩家不存在!", code = 1}
    end
    local ec, _ = player:remove_item(item_id, count, ResDelType.GM)
    if ec ~= SUCCESS then
        return {name = "gm_del_item", msg = "删除物品失败!", code = 1}
    end
    return {name = "gm_del_item", code = 0}
end

-- 给武器加经验
function BagGM:gm_add_weapon_exp(player_id, weapon_id, exp)
    log_debug("[BagGM][gm_add_weapon_exp]->player_id:%s, weapon_id:%s, exp:%s", player_id, weapon_id, exp)
    if not weapon_id or not exp then
        return {name = "add_weapon_exp", msg = "参数错误!", code = 1}
    end
    local player = player_mgr:get_player(player_id)
    if not player then
        return {name = "add_weapon_exp", msg = "玩家不存在!", code = 1}
    end
    local target_weapon = player:get_weapon_item_by_id(weapon_id)
    if not target_weapon then
        return {name = "add_weapon_exp", msg = "仓库中无对应武器!", code = 1}
    end

    local target_weapon_uuid = target_weapon:get_item_uuid()
    local flag, attach_ids = player:add_weapon_exp(target_weapon_uuid, exp)
    if flag then
        local sync_items = { [target_weapon_uuid] = {item_type = ItemType.Weapon, item_id = target_weapon:get_item_id(), item_uuid = target_weapon_uuid} }
        event_mgr:notify_trigger("ntf_part_items_to_client", player, sync_items)

        for attach_id in pairs(attach_ids) do
            plat_api:reddot_add_req(player_id, ReddotType.UNLOCK_ATTACHMENT, target_weapon_uuid, nil, attach_id)
        end
        log_debug("[BagGM][add_weapon_exp]->player_id:%s, weapon_id:%s, exp:%s", player_id, weapon_id, exp)
    end

    return {name = "add_weapon_exp", code = 0}
end

function BagGM:gm_add_gun_exp(player_id, role_id, slot_pos, exp)
    log_debug("[BagGM][gm_add_gun_exp]->player_id:%s, role_id:%s, slot_pos:%s, exp:%s",player_id, role_id, slot_pos, exp)
    local player = player_mgr:get_player(player_id)
    if not player then
        return {name = "add_weapon_exp", msg = "玩家不存在!", code = 1}
    end
    local target_weapon_uuid = player:get_prepare_weapon_uuid(role_id, slot_pos)
    local target_weapon = player:get_weapon_item_by_uuid(target_weapon_uuid)
    if not target_weapon then
        return {name = "add_weapon_exp", msg = "仓库中无对应武器!", code = 1}
    end
    local flag, attach_ids = player:add_weapon_exp(target_weapon_uuid, exp)
    if flag then
        local sync_items = { [target_weapon_uuid] = {item_type = ItemType.Weapon, item_id = target_weapon:get_item_id(), item_uuid = target_weapon_uuid} }
        event_mgr:notify_trigger("ntf_part_items_to_client", player, sync_items)

        for attach_id in pairs(attach_ids) do
            plat_api:reddot_add_req(player_id, ReddotType.UNLOCK_ATTACHMENT, target_weapon_uuid, nil, attach_id)
        end
        log_debug("[BagGM][gm_add_gun_exp]->player_id:%s, role_id:%s, slot_pos:%s, exp:%s", player_id, role_id, slot_pos, exp)
    end
    return {name = "gm_add_gun_exp", code = 0}
end

function BagGM:gm_add_weapon(player_id, weapon_id)
    log_debug("[BagGM][gm_add_weapon]->player_id:%s, weapon_id:%s", player_id, weapon_id)
    local player = player_mgr:get_player(player_id)
    if not player then
        return {name = "add_weapon_exp", msg = "玩家不存在!", code = 1}
    end

    weapon_id = tonumber(weapon_id)
    local add_weapon_items = {}
    tinsert(add_weapon_items,  {item_id=weapon_id, num=1, add_reason=ResAddType.GM, expire_time=0})
    player:add_items(add_weapon_items)

    return {name = "gm_add_weapon", code = 0}
end

quanta.bag_gm = BagGM()

return BagGM
