-- attribute_gm.lua

local tonumber          = tonumber
local log_info          = logger.info
local log_warn          = logger.warn

local gm_agent          = quanta.get("gm_agent")
local event_mgr         = quanta.get("event_mgr")
local player_mgr        = quanta.get("player_mgr")
local config_mgr        = quanta.get("config_mgr")

local PlayerAttrID      = enum("PlayerAttrID")

local attribute_db      = config_mgr:get_table("attribute")

local AttributeGM = singleton()
function AttributeGM:__init()
    --注册事件
    event_mgr:add_listener(self, "gm_set_attribute")
    event_mgr:add_listener(self, "gm_add_attribute")

    --注册GM
    self:register()
end

--GM服务器
function AttributeGM:register()
    local cmd_list = {
        {name = "gm_set_attribute", desc = "设置属性", args = "player_id|number attr_id|number value|string"},
        {name = "gm_add_attribute", desc = "添加属性(数值类)", args = "player_id|number attr_id|number value|string"}
    }
    gm_agent:insert_cmd(cmd_list)
end

-- GM设置属性
function AttributeGM:gm_set_attribute(player_id, attr_id, value)
    log_info("[AttributeGM][gm_set_attribute]")
    local player = player_mgr:get_player(player_id)
    if not player then
        log_warn("[AttributeGM][gm_set_attribute] can not find player=%s", player_id)
        return {name = "gm_set_attribute", code = 1}
    end
    -- 查询配置
    local attr_cfg = attribute_db:find_one(attr_id)
    if not attr_cfg then
        log_warn("[AttributeGM][gm_set_attribute] can not find attr_id=%s", attr_id)
        return {name = "gm_set_attribute", code = 1}
    end
    if "string" == attr_cfg.type then
        value = value
    else
        value = tonumber(value)
    end
    if value then
        player:set_attribute(attr_id, value)
        return {name = "gm_set_attribute", code = 0}
    end
    log_warn("[AttributeGM][gm_set_attribute] unknown attr_id=%s", attr_id)
    return {name = "gm_set_attribute", code = 1}
end

function AttributeGM:gm_add_attribute(player_id, attr_id, value)
    log_info("[AttributeGM][gm_add_attribute]")
    local player = player_mgr:get_player(player_id)
    if not player then
        log_warn("[AttributeGM][gm_add_attribute] can not find player=%s", player_id)
        return {name = "gm_add_attribute", code = 1}
    end
    -- 查询配置
    local attr_cfg = attribute_db:find_one(attr_id)
    if not attr_cfg then
        log_warn("[AttributeGM][gm_add_attribute] can not find attr_id=%s", attr_id)
        return {name = "gm_add_attribute", code = 1}
    end
    if "string" == attr_cfg.type then
        value = value
    else
        value = tonumber(value)
    end
    if value then
        if PlayerAttrID.EXP == attr_id then
            player:add_exp(value)
        elseif PlayerAttrID.CRYSTAL == attr_id then
            player:add_crystal(value)
        elseif PlayerAttrID.IDEAL == attr_id then
            player:add_ideal(value)
        elseif PlayerAttrID.EXPLORE == attr_id then
            player:add_explore(value)
        else
            player:set_attribute(attr_id, value)
        end
        return {name = "gm_set_attribute", code = 0}
    end
    log_warn("[AttributeGM][gm_set_attribute] unknown attr_id=%s", attr_id)
    return {name = "gm_set_attribute", code = 1}
end

-- 导出
quanta.attribute_gm = AttributeGM()

return AttributeGM
