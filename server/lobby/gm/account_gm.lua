-- account_gm.lua
local guid_index        = guid.index
local check_failed      = utility.check_failed

local gm_agent          = quanta.get("gm_agent")
local event_mgr         = quanta.get("event_mgr")
local timer_mgr         = quanta.get("timer_mgr")
local admin_dao         = quanta.get("admin_dao")
local player_mgr        = quanta.get("player_mgr")
local router_mgr        = quanta.get("router_mgr")
local account_mgr       = quanta.get("account_mgr")
local account_servlet   = quanta.get("account_servlet")

local PeriodTime        = enum("PeriodTime")

local AccountGM = singleton()
local prop = property(AccountGM)
prop:accessor("cache_player_count", 0)

function AccountGM:__init()
    --注册事件
    event_mgr:add_listener(self, "gm_kick_out_of_lobby")
    event_mgr:add_listener(self, "gm_get_player_info")
    event_mgr:add_listener(self, "gm_get_account_info")
    --注册GM
    self:register()

    timer_mgr:loop(PeriodTime.SECOND_5_MS, function()
        self:report_online_player_count()
    end)
end

--GM服务器
function AccountGM:register()
    local cmd_list = {
        { name = "gm_kick_out_of_lobby", desc = "踢玩家下线", args = "player_id|number reason|string"},
        { name = "gm_get_player_info", desc = "获取角色信息", args = "player_id|number"},
    }
    gm_agent:insert_cmd(cmd_list)
end

-- T玩家下线
function AccountGM:gm_kick_out_of_lobby(player_id, reason)
    -- player_id
    local player = player_mgr:get_player(player_id)
    if not player then
        return {code = 1, msg = "player not found!" }
    end

    local code = account_servlet:kick_out_of_lobby(player:get_open_id())
    if check_failed(code) then
        return {code = 1, msg = "operation failed!" }
    end

    return { code = 0 }
end

-- 上报在线玩家数量
function AccountGM:report_online_player_count()
    local count = player_mgr:get_player_count()
    if count == self.cache_player_count then
        return
    end

    self.cache_player_count = count
    local svr_id = gm_agent:get_report_svr_id()
    if svr_id then
        router_mgr:call_target(svr_id, "rpc_report_online_count", count, quanta.service_id)
    end
end

-- 获取角色信息
function AccountGM:gm_get_player_info(player_id)
    local player = player_mgr:get_player(player_id)
    local data
    if not player then
        local area_id = guid_index(player_id)
        local ok, attr_map = admin_dao:load_player_attribute(area_id, player_id)
        if not ok then
            return {code = 1, msg = "player load_player_attribute failed"}
        end
        data = attr_map
    else
        data = player:get_attr_map()
    end

    return {code = 0, data = data}
end

-- 获取账号信息
function AccountGM:gm_get_account_info(open_id, area_id)
    local account = account_mgr:get_account(open_id)
    local data
    if not account or account:get_area_id() ~= area_id then
        local ok, account_info = admin_dao:load_account(open_id)
        if not ok then
            return {code = 1, msg = "account load_account failed"}
        end
        data = account_info
    else
        data = account:build_account_data()
    end

    return {code = 0, data = data}
end

-- 导出
quanta.account_gm = AccountGM()

return AccountGM
