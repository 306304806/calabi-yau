-- misc_gm.lua
local log_info      = logger.info
local lfilter       = logger.filter

local gm_agent      = quanta.get("gm_agent")
local event_mgr     = quanta.get("event_mgr")

local MiscGm = singleton()
function MiscGm:__init()
    --注册事件
    event_mgr:add_listener(self, "gm_set_log_level")
    --注册GM
    self:register()
end

--GM服务器
function MiscGm:register()
    local cmd_list = {
    }
    gm_agent:insert_cmd(cmd_list)
end

-- 设置日志等级
function MiscGm:gm_set_log_level(level)
    log_info("[MiscGm][gm_set_log_level] level = %s", level)
    lfilter(level)
end

-- 导出
quanta.misc_gm = MiscGm()

return MiscGm
