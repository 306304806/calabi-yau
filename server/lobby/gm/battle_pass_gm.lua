-- battlepass_gm.lua
local log_info      = logger.info
local log_warn      = logger.warn

local gm_agent      = quanta.get("gm_agent")
local event_mgr     = quanta.get("event_mgr")
local player_mgr    = quanta.get("player_mgr")

local BattlePassGM = singleton()
function BattlePassGM:__init()
    --注册事件
    event_mgr:add_listener(self, "gm_behavior_value_update")
    --注册GM
    self:register()
end

--GM服务器
function BattlePassGM:register()
    local cmd_list = {
        { name = "gm_behavior_value_update", desc = "", args = "player_id|number behavior_id|number value|number" }
    }
    gm_agent:insert_cmd(cmd_list)
end

-- 发送邮件
function BattlePassGM:gm_behavior_value_update(player_id, behavior_id, value)
    log_info("[BattlePassGM][gm_behavior_value_update]")
    local player = player_mgr:get_player(player_id)
    if not player then
        log_warn("[BattlePassGM][gm_behavior_value_update] can not find player=%s", player_id)
        return {name = "gm_behavior_value_update", code = 1}
    end

    log_info("[BattlePassGM][gm_behavior_value_update] player_id=%s, behavior_id=%s, value=%s", player_id, behavior_id, value)
    event_mgr:notify_trigger("ntf_behavior_event", player_id, behavior_id, value)
    return {name = "gm_behavior_value_update", code = 0}
end

-- 导出
quanta.bpass_gm = BattlePassGM()


return BattlePassGM
