--role_gm.lua

local ResAddType    = enum("ResAddType")

local gm_agent      = quanta.get("gm_agent")
local event_mgr     = quanta.get("event_mgr")
local player_mgr    = quanta.get("player_mgr")
local config_mgr    = quanta.get("config_mgr")

local role_db       = config_mgr:get_table("role")
local roleskin_db   = config_mgr:get_table("roleskin")
local rolevoice_db  = config_mgr:get_table("rolevoice")
local roleaction_db = config_mgr:get_table("roleaction")

local RoleGM = singleton()
function RoleGM:__init()
    --注册事件
    event_mgr:add_listener(self, "gm_add_role")
    event_mgr:add_listener(self, "gm_add_role_skin")
    event_mgr:add_listener(self, "gm_add_role_voice")
    event_mgr:add_listener(self, "gm_add_role_action")
    event_mgr:add_listener(self, "gm_add_role_exp")
    --注册GM
    self:register()
end

--GM服务器
function RoleGM:register()
    local cmd_list = {
        {name = "gm_add_role", desc = "添加角色", args = "player_id|number role_id|number life_time|number"},
        {name = "gm_add_role_skin", desc = "添加皮肤", args = "player_id|number role_skin_id|number life_time|number"},
        {name = "gm_add_role_voice", desc = "添加声音", args = "player_id|number voice_id|number"},
        {name = "gm_add_role_action", desc = "添加动作", args = "player_id|number action_id|number"},
        {name = "gm_add_role_exp",    desc = "添加角色经验", args = "player_id|number role_id|number exp|number"},
    }
    gm_agent:insert_cmd(cmd_list)
end

-- 添加角色
function RoleGM:gm_add_role(player_id, role_id, life_time)
    local player = player_mgr:get_player(player_id)
    if not player then
        return {name = "gm_add_role", msg = "玩家不存在!", code = 1}
    end

    local cfg = role_db:find_one(role_id)
    if not cfg then
        return {name = "gm_add_role", msg = "角色配置不存在", code = 1}
    end

    local add_cmd = {role_id = role_id, life_time = life_time, reason = ResAddType.GM}
    player:add_role(add_cmd)

    return {name = "gm_add_role", code = 0}
end

-- 删除角色
function RoleGM:gm_add_role_skin(player_id, role_skin_id, life_time)
    local player = player_mgr:get_player(player_id)
    if not player then
        return {name = "gm_add_role_skin", msg = "玩家不存在!", code = 1}
    end

    local cfg = roleskin_db:find_one(role_skin_id)
    if not cfg then
        return {name = "gm_add_role_skin", msg = "皮肤配置不存在", code = 1}
    end

    local add_cmd = {role_skin_id = role_skin_id, life_time = life_time, reason = ResAddType.GM}
    player:add_role_skin(add_cmd)

    return {name = "gm_add_role_skin", code = 0}
end

-- 增加语音
function RoleGM:gm_add_role_voice(player_id, voice_id)
    local player = player_mgr:get_player(player_id)
    if not player then
        return {name = "gm_add_role_voice", msg = "玩家不存在!", code = 1}
    end

    local cfg = rolevoice_db:find_one(voice_id)
    if not cfg then
        return {name = "gm_add_role_voice", msg = "声音配置不存在", code = 1}
    end

    player:add_role_voice(cfg.role_id, voice_id)

    return {name = "gm_add_role_voice", code = 0}
end

-- 添加角色动作
function RoleGM:gm_add_role_action(player_id, action_id)
    local player = player_mgr:get_player(player_id)
    if not player then
        return {name = "gm_add_role_action", msg = "玩家不存在!", code = 1}
    end

    local cfg = roleaction_db:find_one(action_id)
    if not cfg then
        return {name = "gm_add_role_action", msg = "动作配置不存在", code = 1}
    end

    player:add_role_action(cfg.role_id, action_id)

    return {name = "gm_add_role_action", code = 0}
end

function RoleGM:gm_add_role_exp(player_id, role_id, exp)
    local player = player_mgr:get_player(player_id)
    if not player then
        return {name = "gm_add_role_action", msg = "玩家不存在!", code = 1}
    end

    player:add_role_exp(role_id, exp)

    return {name = "gm_add_role_exp", code = 0}
end

quanta.role_gm = RoleGM()

return RoleGM
