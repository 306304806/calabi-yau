--setting_component.lua

local player_dao    = quanta.get("player_dao")

local SettingComponent = mixin(
    "get_setting",     -- 获取设置列表
    "update_setting"  -- 更新设置列表
)

local prop = property(SettingComponent)
prop:reader("setting_dirty", false)
prop:reader("settings", {}) -- 配置集合

function SettingComponent:__init(open_id, player_id, area_id, session)
end

function SettingComponent:setup(db_data)
    -- 尝试加载
    local player_setting = db_data.player_setting
    if not player_setting then
        return false
    end
    self.settings = player_setting.settings or {}
    return true
end

-- 获取设置列表
function SettingComponent:get_setting()
    return self.settings
end

-- 更新设置
function SettingComponent:update_setting(settings)
    self.settings = settings
    self.setting_dirty = true
end

-- 打包数据
function SettingComponent:package_to_data(data)
    data["setting"] = self.settings
end

-- 数据库操作函数
function SettingComponent:update_data_2db(flush)
    if not self.setting_dirty then
        return true
    end
    local ok = player_dao:update_player_setting(self.player_id, self.settings, flush)
    if ok then
        self.setting_dirty = false
        return true
    end
    return false
end

return SettingComponent
