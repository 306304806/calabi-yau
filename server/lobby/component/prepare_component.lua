--prepare_component.lua
-- 注意：这里只管理玩家相关备战数据，角色备战数据位于RoleComponent
local tinsert           = table.insert
local log_err           = logger.err
local player_dao        = quanta.get("player_dao")

local WeaponSlot        = enum("WeaponSlot")
local ResAddType        = enum("ResAddType")
local ROLE_DEFAULT      = ResAddType.ROLE_DEFAULT

local item_util         = quanta.get("item_util")
local config_mgr        = quanta.get("config_mgr")
local role_db           = config_mgr:get_table("role")

local PrepareComponent = mixin(
    "add_role_default_item",        -- 添加新角色添加默认物品
    "get_prepare_weapon_uuid",      -- 获取备战武器uuid
    "get_role_grenade_slots",       -- 获取角色能够装备手雷槽位

    "check_prepare_items",          -- 检查玩家备战信息
    "get_weapon_prepared_roles",    -- 获取武器已备战角色
    "replace_prepared_weapon",      -- 替换备战武器

    "prepare_equip_weapon",         -- 装备武器

    "get_role_prepare_data",        -- 获取下发客户端某个角色相关备战数据
    "get_role_ds_weapon"            -- 获取进入局内某个角色相关备战数据
)

local prop = property(PrepareComponent)
prop:accessor("roles_prepare", {})                 -- 所有备战数据
prop:accessor("prepare_dirty", false)              -- dirty标记,用于存库
-- 建立索引，快速查找
prop:accessor("player_prepare_uuids", {})          -- 玩家备战索引

function PrepareComponent:__init(open_id, player_id, area_id, session)
end

function PrepareComponent:setup(db_data)
    local player_prepare = db_data.player_prepare
    if not player_prepare then
        return false
    end
    local roles_prepare = player_prepare.prepare_data
    if roles_prepare then
        for _, role_prepare in pairs(roles_prepare or {}) do
            local role_id = role_prepare.role_id
            self.roles_prepare[role_id] = {role_id = role_id, weapons = role_prepare.weapons }
        end
    end
    return true
end

-- 添加角色默认物品
function PrepareComponent:add_role_default_item(role_id, sync_flag)
    local role_cfg = role_db:find_one(role_id)
    if not role_cfg then
        log_err("[PrepareComponent][add_role_default_item]-> get role failed! role_id:%s", role_id)
        return false
    end

    self.roles_prepare[role_id] = {role_id = role_id, weapons = { default_prepares = {}, cur_prepares = {} }, }
    local role_prepare_data = self.roles_prepare[role_id]

    -- 1.检查玩家武器背包中有没有默认的武器实例数据，没有则添加一把武器
    local primary_id, secondary_id, melee_id = role_cfg.default_weapon1, role_cfg.default_weapon2, role_cfg.default_weapon3
    local grenade_1, grenade_2 = role_cfg.default_weapon4, role_cfg.default_weapon5
    local default_weapon_ids = { primary_id, secondary_id, melee_id, grenade_1, grenade_2 }
    local add_weapon_items = {}
    for _, weapon_id in ipairs(default_weapon_ids) do
        if not self:get_weapon_item_by_id(weapon_id) then
            tinsert(add_weapon_items,  {item_id=weapon_id, num=1, add_reason=ROLE_DEFAULT, expire_time=0})
        end
    end

    if sync_flag then
        self:add_items(add_weapon_items)
    else
        self:add_items_no_sync(add_weapon_items)
    end

    local default_prepare_weapons = role_prepare_data.weapons.default_prepares
    local cur_prepare_weapons = role_prepare_data.weapons.cur_prepares
    for _, default_weapon_id in ipairs(default_weapon_ids) do
        local default_weapon_item = self:get_weapon_item_by_id(default_weapon_id)
        local insert_data = {weapon_uuid = default_weapon_item:get_item_uuid(), slot_pos =0}
        if default_weapon_id == primary_id then
            insert_data.slot_pos = WeaponSlot.PRIMARY
        elseif default_weapon_id == secondary_id then
            insert_data.slot_pos = WeaponSlot.SECONDARY
        elseif default_weapon_id == melee_id then
            insert_data.slot_pos = WeaponSlot.MELEE
        elseif default_weapon_id == grenade_1 then
            insert_data.slot_pos = WeaponSlot.GRENADE_A
        elseif default_weapon_id == grenade_2 then
            insert_data.slot_pos = WeaponSlot.GRENADE_B
        end
        tinsert(default_prepare_weapons, insert_data)
        tinsert(cur_prepare_weapons, insert_data)
    end

    self.prepare_dirty = true
    return true
end

-- 获取角色可装备手雷槽位
function PrepareComponent:get_role_grenade_slots(role_id)
    local ret_slots = {}
    local role_cfg =  role_db:find_one(role_id)
    if not role_cfg or not role_cfg.grenade_slot then
        log_err("[RoleComponent][get_role_grenade_slots]get role_cfg failed! player_id:%s,role_id:%s", self.player_id, role_id)
        return ret_slots
    end
    for idx = 0, role_cfg.grenade_slot - 1 do
        ret_slots[WeaponSlot.GRENADE_A + idx] = true
    end
    return ret_slots
end

function PrepareComponent:get_prepare_weapon_uuid(role_id, slot_pos)
    local role_prepare = self.roles_prepare[role_id]
    if role_prepare then
        local role_prepare_weapons = role_prepare.weapons
        for _, prepare_weapon_info in pairs(role_prepare_weapons.cur_prepares) do
            if prepare_weapon_info.slot_pos == slot_pos then
                return prepare_weapon_info.weapon_uuid
            end
        end
    end
end

-- 装备武器
function PrepareComponent:prepare_equip_weapon(role_id, slot_pos, weapon_uuid)
    local role_prepare = self.roles_prepare[role_id]
    if role_prepare then
        local role_prepare_weapons = role_prepare.weapons
        for _, prepare_weapon_info in pairs(role_prepare_weapons.cur_prepares) do
            if prepare_weapon_info.slot_pos == slot_pos then
                prepare_weapon_info.weapon_uuid = weapon_uuid
                self.prepare_dirty = true
                break
            end
        end
    end
end

-- 获取某个角色备战数据
function PrepareComponent:get_role_prepare_data(role_id)
    -- 注：修改表内字段需要同步修改对应pb字段
    local ret_data = { role_id = role_id, weapons = {}, }
    local role_prepare = self.roles_prepare[role_id]
    if not role_prepare then
        log_err("[PrepareComponent][get_role_prepare_data] get role_preare failed! role_id:%s", role_id)
        return ret_data
    end
    -- 武器备战信息
    local cur_prepare_weapons = role_prepare.weapons.cur_prepares
    for _, prepare_weapons in pairs(cur_prepare_weapons) do
        tinsert(ret_data.weapons, {slot_pos = prepare_weapons.slot_pos, weapon_uuid = prepare_weapons.weapon_uuid})
    end
    return ret_data
end

-- 获取某个角色进入局内所需武器数据
function PrepareComponent:get_role_ds_weapon(role_id)
    -- 注：修改表内字段需要同步修改对应pb字段
    local weapon_data = {}
    weapon_data.primary_id              = 0
    weapon_data.primary_uuid            = 0
    weapon_data.primary_level           = 0
    weapon_data.primary_exp             = 0
    weapon_data.primary_components      = {}
    weapon_data.secondary_id            = 0
    weapon_data.secondary_uuid          = 0
    weapon_data.secondary_exp           = 0
    weapon_data.secondary_level         = 0
    weapon_data.secondary_components    = {}
    weapon_data.melee_id                = 0
    weapon_data.grenades                = {0, 0}
    local role_prepare = self.roles_prepare[role_id]
    if not role_prepare or not next(role_prepare.weapons) then
        log_err("[PrepareComponent][get_role_ds_weapon] get role_preare failed! role_id:%s", role_id)
        return weapon_data
    end
    -- 武器备战信息
    for _, prepare_weapon_info in pairs(role_prepare.weapons.cur_prepares) do
        local slot_pos = prepare_weapon_info.slot_pos
        local weapon_uuid = prepare_weapon_info.weapon_uuid
        local weapon = self:get_weapon_item_by_uuid(weapon_uuid)
        if not weapon then
            log_err("[PrepareComponent][get_role_ds_weapon]get weapon failed! player_id:%s, role_id:%s, slot_pos:%s, weapon_uuid:%s",
                     self.player_id, role_id, slot_pos, weapon_uuid)
            break
        end
        if slot_pos == WeaponSlot.PRIMARY then
            weapon_data.primary_id          = weapon:get_item_id()
            weapon_data.primary_uuid        = weapon:get_item_uuid()
            weapon_data.primary_exp         = weapon:get_level()
            weapon_data.primary_level       = weapon:get_cur_exp()
            weapon_data.primary_components  = weapon:get_unlock_attachments()
        elseif slot_pos == WeaponSlot.SECONDARY then
            weapon_data.secondary_id        = weapon:get_item_id()
            weapon_data.secondary_uuid      = weapon:get_item_uuid()
            weapon_data.secondary_exp       = weapon:get_level()
            weapon_data.secondary_level     = weapon:get_cur_exp()
            weapon_data.secondary_components= weapon:get_unlock_attachments()
        elseif slot_pos == WeaponSlot.MELEE then
            weapon_data.melee_id            = weapon:get_item_id()
        elseif slot_pos == WeaponSlot.GRENADE_A then
            weapon_data.grenades[1]         = weapon:get_item_id()
        elseif slot_pos == WeaponSlot.GRENADE_B then
            weapon_data.grenades[2]         = weapon:get_item_id()
        end
    end
    return weapon_data
end

-- 检查玩家备战
function PrepareComponent:check_prepare_items(expired_uuid, itm_id)
    if not self.player_prepare_uuids[expired_uuid] then
        return false
    end
    self.player_prepare_uuids[expired_uuid] = false

    local player_prepare = self.prepare_data
    if item_util:is_decal_item_type(itm_id) then
        for pos, decal_uuid in pairs(player_prepare.decals) do
            if decal_uuid == expired_uuid then
                player_prepare.decals[pos] = 0
                return
            end
        end
    end
end

-- 检查武器是否处于备战
function PrepareComponent:get_weapon_prepared_roles(weapon_uuid)
    local ret_roles = {}
    local role_prepare = self.roles_prepare
    for role_id, data in pairs(role_prepare) do
        for _, prepare_weapon_info in pairs(data.weapons.cur_prepares) do
            if prepare_weapon_info.weapon_uuid == weapon_uuid then
                ret_roles[role_id] = true
                break
            end
        end
    end
    return ret_roles
end

-- 替换已过期的备战武器
function PrepareComponent:replace_prepared_weapon(expired_weapon_uuid)
    -- 将默认的备战武器替换已过期的备战武器
    for role_id, data in pairs(self.roles_prepare) do
        for _, prepare_weapon_info in pairs(data.weapons.cur_prepares) do
            if prepare_weapon_info.weapon_uuid == expired_weapon_uuid then
                for _, default_weapon_info in pairs(data.weapons.default_prepares) do
                    if prepare_weapon_info.slot_pos == default_weapon_info.slot_pos then
                        prepare_weapon_info.weapon_uuid = default_weapon_info.weapon_uuid
                        break
                    end
                end
                break
            end
        end
    end
end

-- 打包为数据库格式
local function package_for_db(self)
    local roles_prepare = {}
    for role_id, role_prepare in pairs(self.roles_prepare) do
        tinsert(roles_prepare, role_prepare)
    end
    return roles_prepare
end

function PrepareComponent:package_to_data(data)
    data["prepare"] = package_for_db(self)
end

-- 数据库操作函数
function PrepareComponent:update_data_2db(flush)
    if not self.prepare_dirty then
        return true
    end
    local ok = player_dao:update_player_prepare(self.player_id, package_for_db(self), flush)
    if ok then
        self.prepare_dirty = false
        return true
    end
    return false
end

return PrepareComponent
