-- achieve_component.lua
local tinsert       = table.insert

local event_mgr     = quanta.get("event_mgr")
local player_dao    = quanta.get("player_dao")

local AchieveType       = enum("AchieveType")
local AchieveHonourID   = enum("AchieveHonourID")

local AchieveComponent = mixin(
    "get_achieve",
    "set_achieve",
    "get_achieve_cnt",
    "set_achieve_cnt",
    "get_default_achieve",
    "get_achieve_level",
    "set_achieve_level"
)

local prop = property(AchieveComponent)
prop:accessor("achieve_dirty", false)
prop:accessor("id2achieves", {})
prop:accessor("achieve_cnts", {})
prop:accessor("achieve_levels", {})

function AchieveComponent:__init(open_id, player_id, area_id, session)
end

function AchieveComponent:setup(db_data)
    local player_achieve = db_data.player_achieve
    if not player_achieve then
        return false
    end
    for _, achieve in pairs(player_achieve.achieves or {}) do
        self.id2achieves[achieve.id] = achieve
    end
    local field_name = {"medal_cnt", "epic_cnt", "honour_cnt"}
    for achieve_type, field in pairs(field_name) do
        self.achieve_cnts[achieve_type] = player_achieve[field] or 0
    end
    return true
end

--日刷新
function AchieveComponent:flush_day(day_edition)
    event_mgr:notify_trigger("evt_achieve_update", self.player_id, AchieveType.HONOUR, AchieveHonourID.ACC_LOGIN, 1)
    if day_edition - self.day_edition > 1 then
        event_mgr:notify_trigger("evt_achieve_update", self.player_id, AchieveType.HONOUR, AchieveHonourID.CONTINUE_LOGIN, 0)
    end
    event_mgr:notify_trigger("evt_achieve_update", self.player_id, AchieveType.HONOUR, AchieveHonourID.CONTINUE_LOGIN, 1)
end

-- 打包为数据库格式
local function package_for_db(self)
    local db_data = {
        achieves = {},
    }
    for _, achieve in pairs(self.id2achieves) do
        tinsert(db_data.achieves, achieve)
    end
    local field_name = {"medal_cnt", "epic_cnt", "honour_cnt"}
    for achieve_type, field in pairs(field_name) do
        db_data[field] = self.achieve_cnts[achieve_type] or 0
    end
    return db_data
end

-- 打包数据
function AchieveComponent:package_to_data(data)
    data["achievement"] = package_for_db(self)
end

function AchieveComponent:update_data_2db(flush)
    if not self.achieve_dirty then
        return true
    end
    local db_data = package_for_db(self)
    local ok = player_dao:update_player_achieves(self.player_id, db_data, flush)
    if ok then
        self.achieve_dirty = false
        return true
    end
    return false
end

-- 获取成就数据
function AchieveComponent:get_achieve(id)
    return self.id2achieves[id] or self:get_default_achieve(id)
end

function AchieveComponent:get_default_achieve(id)
    local data = {}
    data.id = id
    data.progress = 0
    data.reach = 0

    return data
end

-- 获取成就数据
function AchieveComponent:set_achieve(achieve)
    self.id2achieves[achieve.id] = achieve
    self.achieve_dirty = true
end

-- 设置成就数
function AchieveComponent:set_achieve_cnt(achieve_type, cnt)
    self.achieve_cnts[achieve_type] = cnt
    self.achieve_dirty = true
end

-- 获取成就数
function AchieveComponent:get_achieve_cnt(achieve_type, cnt)
    return self.achieve_cnts[achieve_type] or 0
end

-- 设置成就等级
function AchieveComponent:set_achieve_level(achieve_type, level)
    self.achieve_levels[achieve_type] = level
end

-- 获取成就等级
function AchieveComponent:get_achieve_level(achieve_type, level)
    return self.achieve_levels[achieve_type] or 0
end

-- export
return AchieveComponent