-- bag_component.lua
-- 仓库组件
local BagItem       = import("lobby/entity/item/bag_item.lua")
local Weapon        = import("lobby/entity/item/weapon.lua")
local Decal         = import("lobby/entity/item/decal.lua")

local otime         = os.time
local tinsert       = table.insert
local new_guid      = guid.new
local mfloor        = math.floor
local log_err       = logger.err
local log_debug     = logger.debug
local serialize     = logger.serialize

local RoomType      = enum("RoomType")
local ResAddType    = enum("ResAddType")
local AchieveType   = enum("AchieveType")
local ItemType      = enum("ItemType")
local DlogWeapon    = enum("DlogWeapon")
local CurrencyType  = enum("CurrencyType")
local BehaviorAttrID= enum("BehaviorAttrID")
local AchieveHonourID = enum("AchieveHonourID")
local ROLE_DEFAULT  = ResAddType.ROLE_DEFAULT
local PLAYER_DEFAULT= ResAddType.PLAYER_DEFAULT

local event_mgr     = quanta.get("event_mgr")
local config_mgr    = quanta.get("config_mgr")
local player_dao    = quanta.get("player_dao")
local item_util     = quanta.get("item_util")

local bag_item_db       = config_mgr:get_table("item")
local weapon_item_db    = config_mgr:get_table("weapon")
local decal_item_db     = config_mgr:get_table("decal")
local currency_db       = config_mgr:get_table("currency")

local BagComponent = mixin(
    "build_save_items",
    "parse_db_items",

    "get_all_bag_items",      -- 获取背包中所有物品
    "get_all_weapon_items",   -- 获取所有武器
    "get_all_decal_items",    -- 获取所有喷漆

    "get_weapon_item_by_id",  -- 通过武器id获取武器物品
    "get_decal_item_by_id",   -- 通过喷漆id获取喷漆物品
    "get_bag_items_by_id",    -- 通过道具id获取道具

    "get_bag_item_by_uuid",   -- 通过uuid获取背包物品
    "get_weapon_item_by_uuid",-- 通过uuid获取武器
    "get_decal_item_by_uuid", -- 通过uuid获取喷漆

    "get_item_count",         -- 获取item数量[]
    "get_guns_count",         -- 获取枪的数量
    "get_bag_item_count",     -- 获取背包物品个数

    "add_item",               -- 添加单个item [同步客户端]
    "add_items",              -- 添加多个item [同步客户端]
    "add_item_no_sync",       -- 添加单个item [不同步客户端]
    "add_items_no_sync",      -- 添加多个item [不同步客户端]

    "remove_item",            -- 消耗单个item [同步客户端]
    "remove_items",           -- 消耗多个item [同步客户端]
    "remove_item_no_sync",    -- 消耗单个item [不同步客户端]
    "remove_items_no_sync",   -- 消耗多个item [不同步客户端]

    "change_items_data_sync_client", -- item数据变化同步客户端

    "add_currency_item",      -- 添加货币道具
    "del_currency_item",      -- 消耗货币道具

    "add_weapon_exp",           -- 添加武器经验
    "equip_weapon_attachment",  -- 装备武器配件
    "unlock_weapon_attachment", -- 解锁武器配件
    "unload_weapon_attachment", -- 卸下武器配件

    -- 操作item
    "use_bag_item"            -- 使用背包物品
)

local prop = property(BagComponent)
prop:accessor("bag_items", {})          --背包物品
prop:accessor("weapon_items", {})       --武器物品
prop:accessor("decal_items", {})        --喷漆物品
prop:reader("items_dirty", false)  -- items dirty标记

function BagComponent:__init(open_id, player_id, area_id, session)
end

function BagComponent:setup(db_data)
    local player_bag = db_data.player_bag
    if not player_bag then
        return false
    end
    self:parse_db_items(player_bag.bag_list or {})
    return true
end

-- 解析数据库中数据
function BagComponent:parse_db_items(bag_list)
    for _, data in ipairs(bag_list.bag_items or {}) do
        local bag_item = BagComponent.build_item_from_db(self, data)
        if bag_item then
            self.bag_items[bag_item:get_item_uuid()] = bag_item
        end
    end

    for _, data in ipairs(bag_list.weapon_items or {}) do
        local weapon_item = BagComponent.build_item_from_db(self, data)
        if weapon_item then
            self.weapon_items[weapon_item:get_item_uuid()] = weapon_item
        end
    end

    for _, data in ipairs(bag_list.decal_items or {}) do
        local decal_item = BagComponent.build_item_from_db(self, data)
        if decal_item then
            self.decal_items[decal_item:get_item_uuid()] = decal_item
        end
    end
end

-- 构建落地数据
function BagComponent:build_save_items()
    local save_bag_list = {bag_items = {}, decal_items = {}, weapon_items = {}}
    for _, bag_item in pairs(self.bag_items) do
        tinsert(save_bag_list.bag_items, bag_item:build_save_data())
    end

    for _, weapon_item in pairs(self.weapon_items) do
        tinsert(save_bag_list.weapon_items, weapon_item:build_save_data())
    end

    for _, decal_item in pairs(self.decal_items) do
        tinsert(save_bag_list.decal_items, decal_item:build_save_data())
    end

    return save_bag_list
end

-- 获取仓库所有道具
function BagComponent:get_all_bag_items()
    return self.bag_items
end

-- 获取所有武器
function BagComponent:get_all_weapon_items()
    return self.weapon_items
end

-- 获取所有喷漆
function BagComponent:get_all_decal_items()
    return self.decal_items
end

-- 根据武器id获取武器
function BagComponent:get_weapon_item_by_id(weapon_id)
    -- 当前游戏设计同账号下：同一武器id只会存在一个武器实例
    for _, weapon_item in pairs(self.weapon_items) do
        if weapon_item:get_item_id() == weapon_id then
            return weapon_item
        end
    end
end

-- 根据武器uuid获取武器
function BagComponent:get_weapon_item_by_uuid(weapon_uuid)
    return self.weapon_items[weapon_uuid or 0]
end

-- 通过id获取喷漆物品
function BagComponent:get_decal_item_by_id(decal_id)
    for _, decal_item in pairs(self.decal_items) do
        if decal_item:get_item_id() == decal_id then
            return decal_item
        end
    end
end

-- 通过decal uuid获取decal物品
function BagComponent:get_decal_item_by_uuid(decal_uuid)
    return self.decal_items[decal_uuid or 0]
end

-- 通过道具id获取道具
function BagComponent:get_bag_items_by_id(item_id)
    local bag_items = {}
    for _, bag_item in pairs(self.bag_items) do
        if bag_item:get_item_id() == item_id then
            tinsert(bag_items, bag_item)
        end
    end
    return bag_items
end

-- 通过道具uuid获取道具物品
function BagComponent:get_bag_item_by_uuid(item_uuid)
    return self.bag_items[item_uuid or 0]
end

-- 根据item id获取item 数量
function BagComponent:get_item_count(item_id)
    if item_util:is_bag_item_type(item_id) then
        local total = 0
        for _, bag_item in pairs(self.bag_items) do
            if bag_item:get_item_id() == item_id then
                total = total + bag_item:get_count()
            end
        end

        return total
    end

    if (item_util:is_weapon_item_type(item_id) and self:get_weapon_item_by_id(item_id)) then
        return 1
    end

    if item_util:is_decal_item_type(item_id) then
        local total = 0
        for _, decal_item in pairs(self.decal_items) do
            if decal_item:get_item_id() == item_id then
                total = total + decal_item:get_count()
            end
        end

        return total
    end

    return 0
end

-- 根据背包物品id获取数量
function BagComponent:get_bag_item_count(bag_item_id)
    local total = 0
    for _, bag_item in pairs(self.bag_items) do
        if bag_item:get_item_id() == bag_item_id then
            total = total + bag_item:get_count()
        end
    end

    return total
end

-- 获取枪的数量
function BagComponent:get_guns_count()
    local total = 0
    for _, weapon_item in pairs(self.weapon_items) do
        if weapon_item:is_gun() then
            total = total + 1
        end
    end
    return total
end

-- 添加单个item
-- @param item_id [number] 道具ID
-- @param num [number] 数量
-- @param add_reason [quantaconst.ResAddType] 原因
-- @param expire_time {time_t} 过期时间戳(0表示无限期)
function BagComponent:add_item(item_id, num, add_reason, expire_time)
    local items_info = {[1] = {item_id = item_id, num = num, add_reason = add_reason, expire_time = expire_time}}
    local ret, result_list = self:add_items_no_sync(items_info)
    if ret then
        self:change_items_data_sync_client(result_list)
    end

    return ret, result_list
end

-- 添加多种item_id物品
function BagComponent:add_items(items_info)
    if not next(items_info) then
        return false
    end

    local ret, result_list =  self:add_items_no_sync(items_info)
    if ret then
        self:change_items_data_sync_client(result_list)
    end

    return ret, result_list
end

-- 通知客户端物品数据更新
function BagComponent:change_items_data_sync_client(result_list)
    local sync_items_data = {bag_items_set = {}, weapon_items_set = {}, decal_items_set = {}}
    for _, result in pairs(result_list or {}) do
        if result.add_success then
            BagComponent.package_add_sync_data(self, result, sync_items_data)
        elseif result.rm_success then
            BagComponent.package_rm_sync_data(self, result, sync_items_data)
        else
            log_err("[BagComponent][change_items_data_sync_client] result info error! player_id:%s, result:%s", self.player_id, serialize(result))
        end
    end

    event_mgr:notify_trigger("ntf_part_items_to_client", self, sync_items_data)
end

-- 添加单item_id物品
function BagComponent:add_item_no_sync(item_id, num, add_reason, expire_time)
    return self:add_items_no_sync({[1] = {item_id = item_id, num = num, add_reason = add_reason, expire_time = expire_time}})
end

local function add_currency_item(player, item_id, item_amount)
    if 1 == item_id then
        player:add_ideal(item_amount)
    elseif 2 == item_id then
        player:add_hermes(item_amount)
    elseif 3 == item_id then
        player:add_crystal(item_amount)
    elseif 4 == item_id then
        player:add_explore(item_amount)
    else
        log_err("[BagComponent][add_currency_item] need add currency item_id:%s", item_id)
    end
end

-- 添加多个item
function BagComponent:add_items_no_sync(items_info)
    log_debug("[BagComponent][add_items_no_sync]->player_id:%s, items_info:%s", self.player_id, serialize(items_info))
    local result_list = {}
    local red_dot = true
    local flag = true
    for _, data in pairs(items_info) do
        local item_id = data.item_id
        if data.add_reason == PLAYER_DEFAULT or data.add_reason == ROLE_DEFAULT then
            red_dot = false
        end
        local result = {
                            add_success     = true,     -- 添加操作是否成功
                            item_id         = item_id,
                            add_reason      = data.add_reason,
                            new_items       = {},       -- 新生成物品数据
                            limit_conv_info = {},       -- 超过上限处理数据
                            overlay_items   = {},       -- 叠加物品数据
                            red_dot         = red_dot,  -- 红点提示
                            num             = data.num
                       }
        if item_util:is_bag_item_type(item_id) then
            local bag_item_cfg = bag_item_db:find_one(item_id)
            if bag_item_cfg then
                BagComponent.add_bag_item(self, data, result)
            else
                log_err("[BagComponent][add_items_no_sync] get item cfg failed! item_id:%s", item_id)
                result.add_success = false
            end
        elseif item_util:is_weapon_item_type(item_id) then
            local weapon_item_cfg = weapon_item_db:find_one(item_id)
            if weapon_item_cfg then
                BagComponent.add_weapon_item(self, data, result)
            else
                log_err("[BagComponent][add_items_no_sync] get weapon cfg failed! item_id:%s", item_id)
                result.add_success = false
            end
        elseif item_util:is_decal_item_type(item_id) then
            local deal_item_cfg = decal_item_db:find_one(item_id)
            if deal_item_cfg then
                BagComponent.add_decal_item(self, data, result)
            else
                log_err("[BagComponent][add_items_no_sync] get decal cfg failed! item_id:%s", item_id)
                result.add_success = false
            end
        elseif item_util:is_currency_item_type(item_id) then
            add_currency_item(self, item_id, data.num)
        else
            log_err("[BagComponent][add_items_no_sync] item_type not support add! item_id:%s", item_id)
        end

        if result.add_success then
            tinsert(result_list, result)
        else
            flag = false
            break
        end
    end

    self.items_dirty = true
    log_debug("[BagComponent][add_items_no_sync]->player_id:%s, flag:%s, result_list:%s", self.player_id, flag, serialize(result_list))
    return flag, result_list
end

-- 消耗单个item [外部接口]
function BagComponent:remove_item(item_id, num, del_reason, item_uuid)
    local items_info = { [1] = {item_id = item_id, num = num, del_reason = del_reason, item_uuid = item_uuid} }
    local ret, result_list =  self:remove_items_no_sync(items_info)
    if ret then
        self:change_items_data_sync_client(result_list)
    end

    return result_list
end

-- 消耗多个item [外部接口]
function BagComponent:remove_items(items_info)
    local ret, result_list =  self:remove_items_no_sync(items_info)
    if ret then
        self:change_items_data_sync_client(result_list)
    end

    return result_list
end

-- 消耗单个item
function BagComponent:remove_item_no_sync(item_id, num, del_reason, item_uuid)
    return self:remove_items_no_sync({[1] = {item_id = item_id, num = num, del_reason = del_reason, item_uuid = item_uuid}})
end

-- 消耗多个item
function BagComponent:remove_items_no_sync(items_info)
    local result_list = {}
    local flag = true
    for _, data in pairs(items_info) do
        local item_id = data.item_id
        local old_num = self:get_item_count(item_id)
        if old_num < data.num then
            log_err("[BagComponent][remove_items_no_sync] rm item num not enough! player_id:%s, item_id:%s", self.player_id, item_id)
            flag = false
            break
        end

        local result = {
                         rm_success = true,
                         item_id = item_id,
                         rm_items = {},
                         reduce_items = {},
                         old_num = old_num,
                         cur_num = 0,
                         del_reason = data.del_reason,
                      }
        if item_util:is_bag_item_type(item_id) then
            BagComponent.remove_bag_item(self, data, result)
        elseif item_util:is_weapon_item_type(item_id)  then
            BagComponent.remove_weapon_item(self, data, result)
        elseif item_util:is_decal_item_type(item_id) then
            BagComponent.remove_decal_item(self, data, result)
        end

        if result.rm_success then
            result.cur_num = self:get_item_count(item_id)
            tinsert(result_list, result)
        else
            flag = false
            log_err("[BagComponent][remove_items_no_sync] rm item failed! player_id:%s, item_id:%s", self.player_id, item_id)
            break
        end
    end

    self.items_dirty = true
    return flag, result_list
end

-- 定时检查物品是否过期
function BagComponent:update()
    local rm_type, _ = self:get_room_type()
    if rm_type and rm_type == RoomType.FIGHT then
        -- 局内战斗期间不检查过期
        return
    end

    local expired_items = {}
    local sync_items_data = {bag_items_set = {}, weapon_items_set = {}, decal_items_set = {}}
    for item_uuid, bag_item in pairs(self.bag_items) do
        if bag_item:is_expired() then
            local item_id = bag_item:get_item_id()
            expired_items[item_uuid] = item_id
            tinsert(sync_items_data.bag_items_set, { item_uuid = item_uuid, item_id = item_id,})
        end

    end

    for weapon_uuid, weapon_item in pairs(self.weapon_items) do
        if weapon_item:is_expired() then
            local weapon_id = weapon_item:get_item_id()
            expired_items[weapon_uuid] = weapon_id
            tinsert(sync_items_data.weapon_items_set, { item_uuid = weapon_uuid, item_id = weapon_id,})
        end
    end

    for decal_uuid, decal_item in pairs(self.decal_items) do
        if decal_item:is_expired() then
            local decal_id = decal_item:get_item_id()
            expired_items[decal_uuid] = decal_id
            tinsert(sync_items_data.decal_items_set, { item_uuid = decal_uuid, item_id = decal_id,})
        end
    end

    if next(expired_items) then
        event_mgr:notify_trigger("ntf_bag_items_expired", self, expired_items)
        event_mgr:notify_trigger("ntf_part_items_to_client", self, sync_items_data)
    end
end

-- 使用单个物品
function BagComponent:use_bag_item(item_uuid, use_reason)
    local bag_item = self:get_bag_item_by_uuid(item_uuid)
    if not bag_item then
        return false
    end

    local result_list = {}
    local item_id = bag_item:get_item_id()
    local context = {
                        player     = self,
                        goods_uuid = item_uuid,
                        goods_id   = item_id,
                        del_reason = use_reason,
                        add_reason = ResAddType.UES_BAG_ITEM,
                        sync_list  = {}
                    }
    local item_cfg = bag_item_db:find_one(item_id)
    local effects   = {}
    for idx, effect_id in ipairs(item_cfg.effect_id or {}) do
        log_debug("[BagComponent][use_bag_item]->player_id:%s, idx:%s, effect_id:%s", self.player_id, idx, effect_id)
        local effect = quanta.get("effect_mgr"):create_effect(effect_id)
        if not effect:test(context) then
            log_err("[BagComponent][use_bag_item]->test effect failed! player_id:%s, effect_id:%s", self.player_id, effect_id)
            return false
        end
        effects[idx] = effect
    end

    -- 先消耗goods,再执行效果
    local ret, ret_list = self:remove_item_no_sync(item_id, 1, context.del_reason, item_uuid)
    if not ret then
        log_err("[BagComponent][use_bag_item] remove goods failed! player_id:%s, item_uuid:%s, item_id:%s", self.player_id, item_uuid, item_id)
        return false
    end

    for _, data in pairs(ret_list) do
        tinsert(result_list, data)
    end

    for _, effect in ipairs(effects) do
        if not effect:run(context) then
            -- 执行效果失败，但物品已扣，物品不恢复，添加错误日志，通过错误日志进行找回
            log_err("[BagComponent][use_bag_item] run effect failed!player:%s,uuid:%s,item_id:%s, effect_id:%s", self.player_id, item_uuid, item_id, effect.id)
            return false
        end

        for _, data in pairs(context.sync_list) do
            tinsert(result_list, data)
        end
    end

    log_debug("[BagComponent][use_bag_item]->player_id:%s, result_list:%s", self.player_id, serialize(result_list))
    return true, result_list
end

-- 添加货币道具
function BagComponent:add_currency_item(currency_type, currency_num, reason)
    local currency_cfg = currency_db:find_one(currency_type)
    if not currency_cfg then
        log_err("[BagComponent][add_currency_item]get currency cfg failed! currency_type:%s", currency_type)
        return false
    end

    if currency_type == CurrencyType.IDEAL then
        self:add_ideal(currency_num)
    elseif currency_type == CurrencyType.Hermes then
        self:add_hermes(currency_num)
    elseif currency_type == CurrencyType.CRYSTAL then
        self:add_crystal(currency_num)
    end

    return true
end

-- 消耗货币道具
function BagComponent:del_currency_item(currency_type, currency_num, reason)
    local currency_cfg = currency_db:find_one(currency_type)
    if not currency_cfg then
        log_err("[BagComponent][add_currency_item]get currency cfg failed! currency_type:%s", currency_type)
        return false
    end

    if currency_type == CurrencyType.IDEAL then
        return self:cost_ideal(currency_num, reason)
    elseif currency_type == CurrencyType.Hermes then
        return self:cost_hermes(currency_num, reason)
    elseif currency_type == CurrencyType.CRYSTAL then
        return self:cost_crystal(currency_num, reason)
    else
        return false
    end
end

-------------  内部接口   --------------
-- 添加时打包同步数据
function BagComponent:package_add_sync_data(result, sync_items_data)
    local item_id = result.item_id
    local is_bag_item    = item_util:is_bag_item_type(item_id)
    local is_weapon_item = item_util:is_weapon_item_type(item_id)
    local is_decal_item  = item_util:is_decal_item_type(item_id)
    if next(result.new_items) then
        local data = result.new_items
        if is_bag_item then
            tinsert(
                    sync_items_data.bag_items_set,
                    {item_uuid = data.item_uuid, item_id = item_id, add_reason = data.add_reason, red_dot = result.red_dot}
                )
        elseif is_weapon_item then
            tinsert(
                    sync_items_data.weapon_items_set,
                    {item_uuid = data.item_uuid, item_id = item_id, add_reason = data.add_reason, red_dot = result.red_dot}
                   )
        elseif is_decal_item then
            tinsert(
                sync_items_data.decal_items_set,
                {item_uuid = data.item_uuid, item_id = item_id, add_reason = data.add_reason, red_dot = result.red_dot}
               )
        end
    end

    if next(result.overlay_items) then
        local data = result.overlay_items
        if is_bag_item then
            tinsert(sync_items_data.bag_items_set, {item_uuid = data.item_uuid, item_id = item_id, add_reason = data.add_reason})
        end
    end
end

-- 消耗时打包同步数据
function BagComponent:package_rm_sync_data(result, sync_items_data)
    local item_id = result.item_id
    local is_bag_item    = item_util:is_bag_item_type(item_id)
    local is_weapon_item = item_util:is_weapon_item_type(item_id)
    local is_decal_item  = item_util:is_decal_item_type(item_id)
    for item_uuid in pairs(result.rm_items or {}) do
        if is_bag_item then
            tinsert(sync_items_data.bag_items_set, {item_uuid = item_uuid, item_id = item_id, del_reason = result.del_reason})
        elseif is_weapon_item then
            tinsert(sync_items_data.weapon_items_set, {item_uuid = item_uuid, item_id = item_id, del_reason = result.del_reason})
        elseif is_decal_item then
            tinsert(sync_items_data.decal_items_set, {item_uuid = item_uuid, item_id = item_id, del_reason = result.del_reason})
        end
    end

    for item_uuid in pairs(result.reduce_items or {}) do
        if is_bag_item then
            tinsert(sync_items_data.bag_items_set, {item_uuid = item_uuid, item_id = item_id, del_reason = result.del_reason})
        elseif is_weapon_item then
            tinsert(sync_items_data.weapon_items_set, {item_uuid = item_uuid, item_id = item_id, del_reason = result.del_reason})
        elseif is_decal_item then
            tinsert(sync_items_data.decal_items_set, {item_uuid = item_uuid, item_id = item_id, del_reason = result.del_reason})
        end
    end
end

--创建item来自DB数据
function BagComponent:build_item_from_db(db_data)
    local item_id = db_data.item_id
    if not db_data or not item_id then
        log_err("[BagComponent][build_item_from_db]->param error!")
        return
    end

    local ret_item
    if item_util:is_bag_item_type(item_id) then
        ret_item = BagItem(db_data)
    else
        local item_type = item_util:get_item_type(item_id)
        if item_type  == ItemType.Weapon then
            ret_item = Weapon(db_data)
        elseif item_type  == ItemType.Decal then
            ret_item = Decal(db_data)
        else
            log_err("[BagComponent][build_item_from_db] item_type error! item_type:%s, item_id:%s", item_type, item_id)
        end
    end

    if not ret_item or ret_item:is_expired() then
        self.items_dirty = true
        return nil
    else
        return ret_item
    end
end

-- 创建背包物品
function BagComponent:create_bag_item(bag_item_id, num, add_reason, expire_time, red_dot)
    local bag_item_type = item_util:get_item_type(bag_item_id)
    local item_uuid = new_guid(self.area_id, bag_item_type)
    local item_data = {
                        item_type   = bag_item_type,
                        item_uuid       = item_uuid,
                        item_id         = bag_item_id,
                        count           = num,
                        obtain_time     = otime(),
                        approach        = add_reason,
                        expire_time     = expire_time or 0,
                        red_dot         = red_dot,
                      }
    local bag_item = BagItem(item_data)
    self.bag_items[item_uuid] = bag_item

    event_mgr:notify_trigger("evt_create_item", self, bag_item)
    return bag_item
end

-- 添加背包物品
function BagComponent:add_bag_item(item_info, result)
    local item_id = item_info.item_id
    local bag_item_cfg = bag_item_db:find_one(item_info.item_id)
    local over_num = 0          --超过上限数量
    local real_add_num          --实际添加数量
    local cur_num = self:get_bag_item_count(item_id)
    if cur_num + item_info.num > bag_item_cfg.limit_num then
        over_num = cur_num + item_info.num - bag_item_cfg.limit_num
        real_add_num = item_info.num - over_num
    else
        real_add_num = item_info.num
    end

    local add_reason = item_info.add_reason
    if real_add_num > 0 then
        local expire_time = item_info.expire_time
        if bag_item_cfg.superposition then
            -- 可以叠加
            if cur_num == 0 then
                -- 当前没有此类物品，直接新建
                local bag_item = BagComponent.create_bag_item(self, item_id, real_add_num, add_reason, expire_time, result.red_dot)
                result.new_items = {item_uuid = bag_item:get_item_uuid(), add_num = real_add_num}
            else
                -- 已有此类物品，直接修改叠加数量
                local bag_items = self:get_bag_items_by_id(item_id)
                if next(bag_items) then
                    local bag_item = bag_items[1]
                    local cur_cnt = bag_item:get_count()
                    bag_item:set_count(cur_cnt + real_add_num)
                    result.overlay_items = {item_uuid = bag_item:get_item_uuid(),  overlay_num = real_add_num}
                end
            end
        else
            -- 不可以叠加
            for idx = 1, real_add_num do
                local bag_item = BagComponent.create_bag_item(self, item_id, 1, add_reason, expire_time, result.red_dot)
                result.new_items = {item_uuid = bag_item:get_item_uuid(), add_num = 1}
            end
        end
    end

    if over_num > 0 and (add_reason ~= PLAYER_DEFAULT and add_reason ~= ROLE_DEFAULT) and bag_item_cfg.limit_type > 0 then
        local currency_id = bag_item_cfg.limit_type
        local currency_num = bag_item_cfg.limit_param * over_num
        if self:add_currency_item(currency_id, currency_num) then
            result.limit_conv_info    = {currency_id = currency_id, currency_num = currency_num, conv_num = over_num}
        end
    end
end

-- 添加武器物品
function BagComponent:add_weapon_item(item_info, result)
    local weapon_id = item_info.item_id
    local weapon_item_cfg = weapon_item_db:find_one(weapon_id)

    local add_reason = item_info.add_reason
    local weapon_item = self:get_weapon_item_by_id(weapon_id)
    if weapon_item then
        if (add_reason ~= PLAYER_DEFAULT and add_reason ~= ROLE_DEFAULT) and weapon_item_cfg.limit_type > 0 then
            local currency_id = weapon_item_cfg.limit_type
            local currency_num = weapon_item_cfg.limit_param * 1
            if self:add_currency_item(currency_id, currency_num) then
                result.limit_conv_info    = {currency_id = currency_id, currency_num = currency_num, conv_num = 1}
            end
        end
    else
        local expire_time = item_info.expire_time
        local weapon_uuid = new_guid(self.area_id, ItemType.Weapon)
        local item_data = {
                            item_type       = ItemType.Weapon,
                            item_uuid       = weapon_uuid,
                            item_id         = weapon_id,
                            count           = 1,
                            obtain_time     = otime(),
                            approach        = add_reason,
                            expire_time     = expire_time or 0,
                            red_dot         = result.red_dot,
                          }
        local new_weapon_item = Weapon(item_data)
        self.weapon_items[weapon_uuid] = new_weapon_item
        result.new_items = {item_uuid = weapon_uuid, add_num = 1}
    end
end

-- 添加喷漆物品
function BagComponent:add_decal_item(item_info, result)
    local decal_id = item_info.item_id
    local deal_item_cfg = decal_item_db:find_one(decal_id)
    local add_reason = item_info.add_reason
    local decal_cnt = self:get_item_count(decal_id)
    if decal_cnt >= deal_item_cfg.limit_num then
        if (add_reason ~= PLAYER_DEFAULT and add_reason ~= ROLE_DEFAULT) and deal_item_cfg.limit_type > 0 then
            local currency_id = deal_item_cfg.limit_type
            local currency_num = deal_item_cfg.limit_param * 1
            if self:add_currency_item(currency_id, currency_num) then
                result.limit_conv_info    = {currency_id = currency_id, currency_num = currency_num, conv_num = 1}
            end
        end
    else
        local expire_time = item_info.expire_time
        local decal_uuid = new_guid(self.area_id, ItemType.Decal)
        local item_data = {
                            item_type       = ItemType.Decal,
                            item_uuid       = decal_uuid,
                            item_id         = decal_id,
                            count           = 1,
                            obtain_time     = otime(),
                            approach        = add_reason,
                            expire_time     = expire_time or 0,
                            red_dot         = result.red_dot,
                          }
        local new_decal_item = Decal(item_data)
        self.decal_items[decal_uuid] = new_decal_item
        result.new_items = {item_uuid = decal_uuid, add_num = 1}
    end
end

-- 消耗背包物品
function BagComponent:remove_bag_item(item_info, result)
    local item_uuid = item_info.item_uuid
    local item_id   = item_info.item_id
    local use_num   = item_info.num
    local bag_item = self:get_bag_item_by_uuid(item_uuid or 0)
    if bag_item then
        local cur_cnt = bag_item:get_count()
        if cur_cnt < use_num then
            result.rm_success = false
            log_err("[BagComponent][remove_bag_item] item num not enouth! player_id:%, item_uuid:%s, use_num:%s", self.player_id, item_uuid, use_num)
            return
        elseif cur_cnt == use_num then
            self.bag_items[item_uuid] = nil
            result.rm_items[item_uuid] = true
        else
            bag_item:set_count(cur_cnt - use_num)
            result.reduce_items[item_uuid] = true
        end
    else
        local bag_items = self:get_bag_items_by_id(item_id)
        if not bag_items then
            result.rm_success = false
            log_err("[BagComponent][remove_bag_item] find bag items failed! ")
            return
        end

        for _, item in pairs(bag_items) do
            local uuid = item:get_item_uuid()
            if use_num <= 0 then
                break
            end
            local cur_cnt = item:get_count()
            if cur_cnt > use_num then
                item:set_count(cur_cnt - use_num)
                result.reduce_items[uuid] = true
            else
                self.bag_items[uuid] = nil
                result.rm_items[uuid] = true
            end
            use_num = use_num - cur_cnt
        end
    end
end

-- 消耗武器物品
function BagComponent:remove_weapon_item(item_info, result)
    local weapon_uuid = item_info.item_uuid
    local weapon_id   = item_info.item_id
    local weapon_item = self:get_weapon_item_by_uuid(weapon_uuid)
    if not weapon_item then
        weapon_item = self:get_weapon_item_by_id(weapon_id)
    end

    if not weapon_item then
        result.rm_success = false
        log_err("[BagComponent][remove_weapon_item]get weapon failed! player_id:%s, weapon_uuid:%s, weapon_id:%s", self.player_id, weapon_uuid, weapon_id)
        return
    end

    self.weapon_items[weapon_uuid] = nil
    result.rm_items[weapon_uuid] = true
end

-- 消耗喷漆物品
function BagComponent:remove_decal_item(item_info, result)
    local decal_uuid = item_info.item_uuid
    local decal_id = item_info.item_id
    local decal_item = self:get_decal_item_by_uuid(decal_uuid)
    if not decal_item then
        decal_item = self:get_decal_item_by_id(decal_id)
    end

    if not decal_item then
        result.rm_success = false
        log_err("[BagComponent][remove_weapon_item]get weapon failed! player_id:%s, dcecal_uuid:%s, decal_id:%s", self.player_id, decal_uuid, decal_id)
        return
    end

    self.decal_items[decal_uuid] = nil
    result.rm_items[decal_uuid] = true
end

-- 添加武器经验
function BagComponent:add_weapon_exp(weapon_uuid, value)
    log_debug("[BagComponent][add_weapon_exp]->player_id:%s, weapon_uuid:%s, value:%s", self.player_id, weapon_uuid, value)
    local weapon = self:get_weapon_item_by_uuid(weapon_uuid)
    if not weapon then
        log_err("[BagComponent][add_weapon_exp]->get weapon failed! player_id:%s, weapon_uuid:%s", self.player_id, weapon_uuid)
        return false
    end

    if not weapon:check_can_upgrade() then
        log_err("[BagComponent][add_weapon_exp]->weapon can't upgrade! player_id:%s, weapon_uuid:%s", self.player_id, weapon_uuid)
        return false
    end

    local old_lv = weapon:get_level()
    value = mfloor(value + value * self:get_weapon_exp_inc_rate())
    local flag, notify_red = weapon:add_exp(value)
    if flag then
        self.items_dirty = true
        local new_total_exp = self:get_behavior_attr(BehaviorAttrID.TOTAL_WEAPON_EXP) + value
        event_mgr:notify_trigger("evt_achieve_update", self.player_id, AchieveType.HONOUR, AchieveHonourID.GUN_PROFICIEN)
        event_mgr:notify_trigger("ntf_behavior_event", self.player_id, BehaviorAttrID.TOTAL_WEAPON_EXP, new_total_exp)
        event_mgr:notify_trigger("ntf_behavior_event", self.player_id, BehaviorAttrID.CUR_WEAPON_EXP, value)
    end
    if old_lv ~= weapon:get_level() then
        event_mgr:notify_trigger("evt_weapon_change", self, weapon, DlogWeapon.LEVEL_UP)
    end
    return flag, notify_red
end

-- 装备武器配件
function BagComponent:equip_weapon_attachment(weapon_uuid, attach_type, attach_id)
    local weapon = self:get_weapon_item_by_uuid(weapon_uuid)
    weapon:equip_attachment(attach_type, attach_id)
    self.items_dirty = true
end

-- 卸下武器配件
function BagComponent:unload_weapon_attachment(weapon_uuid, attach_type, attach_id)
    local weapon = self:get_weapon_item_by_uuid(weapon_uuid)
    weapon:unload_attachment(attach_type, attach_id)
    self.items_dirty = true
end

-- 解锁武器配件
function BagComponent:unlock_weapon_attachment(weapon_uuid, attach_id, item_id)
    local weapon = self:get_weapon_item_by_uuid(weapon_uuid)
    weapon:unlock_attachment(attach_id, item_id)
    self.items_dirty = true
end

-- 打包数据
function BagComponent:package_to_data(data)
    data["bag"] = self:build_save_items()
end

-- 数据库操作函数
function BagComponent:update_data_2db(flush)
    if not self.items_dirty then
        return true
    end
    local ok = player_dao:update_player_items(self.player_id, self:build_save_items(), flush)
    if ok then
        self.items_dirty = false
        return true
    end
    return false
end
return BagComponent