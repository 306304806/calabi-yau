--reward_component.lua
local  RewardObj =  import("lobby/entity/reward_obj.lua")

local otime             = os.time
local log_err           = logger.err
local log_debug         = logger.debug

local reward_mgr        = quanta.get("reward_mgr")
local player_dao        = quanta.get("player_dao")
local rankseason_mgr    = quanta.get("rankseason_mgr")

local RewardComponent = mixin(
    "reward_satisfy",
    "get_all_rewards_status",
    "get_reward_obj"
)

local prop = property(RewardComponent)
prop:accessor("module_rewards", {})
prop:accessor("reward_dirty", false)

-- 解析数据库中数据
local function parse_db_rewards(player, db_rewards)
    local cur_season = rankseason_mgr:get_season_id()
    for module_id, module_data in pairs(db_rewards.module_rewards) do
        player.module_rewards[module_id] = { reward_objs = {}, }
        local data = player.module_rewards[module_id]

        local b_continue = true
        if reward_mgr:is_bind_season(module_id) then
            if module_data.valid_season ~= cur_season then
                data.valid_season = cur_season
                b_continue = false
                player.reward_dirty = true
            end
        end

        if b_continue then
            for reward_id, info in pairs(module_data.rewards) do
                data.reward_objs[reward_id] = RewardObj(module_id, reward_id, info.active_time, info.recived_time)
            end
        end
    end
end

-- 构建落地数据
local function build_save_rewards(player)
    local db_rewards = { module_rewards = {  } }
    for module_id, module_data in pairs(player.module_rewards) do
        db_rewards.module_rewards[module_id] = { rewards = {}, }
        local data = db_rewards.module_rewards[module_id]

        if reward_mgr:is_bind_season() then
            data.valid_season = module_data.valid_season
        end

        for reward_id, obj in pairs(module_data.reward_objs) do
            data.rewards[reward_id] = { active_time = obj:get_active_time(), recived_time = obj:get_recived_time()}
        end
    end

    return db_rewards
end

function RewardComponent:__init(open_id, player_id, area_id, session)
end

function RewardComponent:setup(db_data)
    local player_reward = db_data.player_reward
    if not player_reward then
        return false
    end
    local rewards = player_reward.rewards
    if rewards then
        parse_db_rewards(self, rewards)
    end
    return true
end

-- 获取reward obj
function RewardComponent:get_reward_obj(module_id, reward_id)
    if self.module_rewards[module_id] then
        return self.module_rewards[module_id][reward_id]
    end
end

-- 获取奖励模块
function RewardComponent:get_all_rewards_status()
    local status_list = {}
    for module_id, module_data in pairs(self.module_rewards) do
        status_list[module_id] = {}
        local data = status_list[module_id]

        for reward_id, obj in pairs(module_data.reward_objs) do
            data[reward_id] = obj:get_status()
        end
    end

    return status_list
end

-- 创建 reward obj
function RewardComponent:create_reward_obj(module_id, reward_id)
    if not self.module_rewards[module_id] then
        self.module_rewards[module_id] = {}
    end
    local data = self.module_rewards[module_id]

    if reward_mgr:is_bind_season(module_id) and not data.valid_season then
        data.valid_season = rankseason_mgr:get_season_id()
    end

    if not data.reward_objs then
        data.reward_objs = {}
    end

    if data.reward_objs[reward_id] then
        return
    end

    data.reward_objs[reward_id] = RewardObj(module_id, reward_id, otime())

    self.reward_dirty = true
end

-- 通知满足奖励条件
function RewardComponent:reward_satisfy(module_id, reward_id)
    local reward_obj = self:get_reward_obj(module_id, reward_id)
    if reward_obj then
        log_debug("[RewardComponent][reward_satisfy] get reward obj exsited! module_id:%s, reward_id:%s", module_id, reward_id)
        return false
    end

    self:create_reward_obj(module_id, reward_id)

    return true
end

-- 当时检查
function RewardComponent:update()
    local cur_season = rankseason_mgr:get_season_id()
    for module_id, module_data in pairs(self.module_rewards) do
        -- 1.检查赛季过期
        if reward_mgr:is_bind_season(module_id) and module_data.valid_season ~= cur_season then
            log_err("[RewardComponent][update]->season expired! cur_season:%s, valid_season", module_data.valid_season, cur_season)
            module_data.reward_objs = {}
            self.reward_dirty = true
        end

        -- 2.检查单个奖励过期
        for reward_id, reward_objs in pairs(module_data.reward_objs) do
            if reward_objs:get_expired_time() == 0 and reward_objs:check_is_expired() then
                reward_objs:on_expired()
            end
        end
    end
end

function RewardComponent:package_to_data(data)
    data["reward"] = build_save_rewards(self)
end

-- 数据库操作函数
function RewardComponent:update_data_2db(flush)
    if not self.reward_dirty then
        return true
    end
    local update_ok = player_dao:update_player_rewards(self.player_id, build_save_rewards(self), flush)
    if update_ok then
        self.reward_dirty = false
        return true
    end
    return false
end

return RewardComponent