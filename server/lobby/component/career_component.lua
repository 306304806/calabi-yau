-- career_component.lua

local otime             = os.time
local log_info          = logger.info
local serialize         = logger.serialize

local player_dao        = quanta.get("player_dao")
local event_mgr         = quanta.get("event_mgr")
local config_mgr        = quanta.get("config_mgr")
local rankseason_mgr    = quanta.get("rankseason_mgr")

local CareerAttrTypes   = enum("CareerAttrTypes")
local GameMode          = enum("GameMode")
local CareerAttrID      = enum("CareerAttrID")

local career_attr_db    = config_mgr:get_table("careerattribute")

local CareerComponent = mixin(
    "set_career_his_attr",
    "add_career_his_attr",
    "set_career_cur_attr",
    "add_career_cur_attr",
    "set_career_attr",
    "get_career_his_attr",
    "get_career_cur_attr",
    "get_career_attr",
    "update_his_career_data",
    "update_cur_career_data",
    "init_new_career"
)

local prop = property(CareerComponent)
prop:accessor("career_dirty", false)
prop:accessor("career_map", {})             -- 生涯的属性列表

function CareerComponent:__init(open_id, player_id, area_id, session)
end

function CareerComponent:setup(db_data)
    -- 尝试加载
    local career = db_data.player_career
    if not career then
        return false
    end
    -- 填充默认属性
    for _, cfg in career_attr_db:iterator() do
        if cfg.image_only then
            goto continue
        end
        if cfg.is_his then
            self.career_map[CareerAttrTypes.HISTORY] = self.career_map[CareerAttrTypes.HISTORY] or {}
            self.career_map[CareerAttrTypes.HISTORY][cfg.id] = cfg.default
        end
        if cfg.is_cur then
            self.career_map[CareerAttrTypes.CURRENT] = self.career_map[CareerAttrTypes.CURRENT] or {}
            self.career_map[CareerAttrTypes.CURRENT][cfg.id] = cfg.default
        end
        ::continue::
    end
    -- 成功加载
    if career.attrs then
        for _, attrs in pairs(career.attrs) do
            local attr_map = {}
            for _, item in pairs(attrs) do
                attr_map[item.id] = item.value
            end
            local types = attr_map["types"]
            local tmp_att_map = self.career_map[types]
            for k, v in pairs(attr_map) do
                tmp_att_map[k] = v
            end
        end
    else
        self:init_new_career()
    end
    return true
end

-- 设置属性
function CareerComponent:init_new_career()
    -- 设置首次登录时间
    self:set_career_his_attr(CareerAttrID.FLOGIN_TIME, otime())
    -- 设置赛季信息
    self:set_career_cur_attr(CareerAttrID.CUR_SEASON, rankseason_mgr:get_season_id())
end

-- 设置属性
function CareerComponent:set_career_attr(id, types, value)
    local tmp_map = self.career_map[types]
    -- 不相等才会触发后续逻辑
    if value ~= tmp_map[id] then
        tmp_map[id] = value
        self.career_dirty = true
        event_mgr:notify_trigger("evt_dirty_career", self, id, types, value)
    end
end
-- 增加历史属性
-- @param id {CareerAttrID}: 属性id
-- @param value {any}: 属性值
function CareerComponent:add_career_his_attr(id, value)
    local old_val = self:get_career_attr(id, CareerAttrTypes.HISTORY)
    if not old_val then
        return
    end
    local new_val = old_val + value
    self:set_career_attr(id, CareerAttrTypes.HISTORY, new_val)
end

-- 设置历史属性
-- @param id {CareerAttrID}: 属性id
-- @param value {any}: 属性值
function CareerComponent:set_career_his_attr(id, value)
   self:set_career_attr(id, CareerAttrTypes.HISTORY, value)
end

-- 增加当前属性
-- @param id {CareerAttrID}: 属性id
-- @param value {any}: 属性值
function CareerComponent:add_career_cur_attr(id, value)
    local old_val = self:get_career_attr(id, CareerAttrTypes.CURRENT)
    if not old_val then
        return
    end
    local new_val = old_val + value
    self:set_career_attr(id, CareerAttrTypes.CURRENT, new_val)
end

-- 设置当前属性
-- @param id {CareerAttrID}: 属性id
-- @param value {any}: 属性值
function CareerComponent:set_career_cur_attr(id, value)
    self:set_career_attr(id, CareerAttrTypes.CURRENT, value)
end

-- 获取属性
function CareerComponent:get_career_attr(id, types)
    return self.career_map[types][id]
end

-- 获取历史属性
function CareerComponent:get_career_his_attr(id)
    return self:get_career_attr(id, CareerAttrTypes.HISTORY)
end

-- 获取当前属性
function CareerComponent:get_career_cur_attr(id)
    return self:get_career_attr(id, CareerAttrTypes.CURRENT)
end

function CareerComponent:update_his_career_data(context)
    log_info("[CareerComponent][update_his_career_data] player_id:%d, context:%s", self.player_id, serialize(context))
    local win_counts = context.win and 1 or 0
    local mvp_counts = context.mvp and 1 or 0
    if context.room_mode == GameMode.BOMB then
        self:add_career_his_attr(CareerAttrID.BOMB_ROUND, 1)
        self:add_career_his_attr(CareerAttrID.BOMB_KILL, context.kill_num)
        self:add_career_his_attr(CareerAttrID.BOMB_DAMAGE, context.damage)
        self:add_career_his_attr(CareerAttrID.BOMB_HIT_HEAD, context.hit_head)
        self:add_career_his_attr(CareerAttrID.BOMB_HIT_COUNT, context.hit_count)
        self:add_career_his_attr(CareerAttrID.BOMB_WIN, win_counts)
        self:add_career_his_attr(CareerAttrID.BOMB_MVP, mvp_counts)
    elseif context.room_mode == GameMode.TEAM then
        self:add_career_his_attr(CareerAttrID.TEAM_ROUND, 1)
        self:add_career_his_attr(CareerAttrID.TEAM_KILL, context.kill_num)
        self:add_career_his_attr(CareerAttrID.TEAM_DAMAGE, context.damage)
        self:add_career_his_attr(CareerAttrID.TEAM_HIT_HEAD, context.hit_head)
        self:add_career_his_attr(CareerAttrID.TEAM_HIT_COUNT, context.hit_count)
        self:add_career_his_attr(CareerAttrID.TEAM_WIN, win_counts)
        self:add_career_his_attr(CareerAttrID.TEAM_MVP, mvp_counts)
    end
end

function CareerComponent:update_cur_career_data(context)
    log_info("[CareerComponent][update_cur_career_data] player_id:%d, context:%s", self.player_id, serialize(context))
    local win_counts = context.win and 1 or 0
    local mvp_counts = context.mvp and 1 or 0
    if context.room_mode == GameMode.BOMB then
        self:add_career_cur_attr(CareerAttrID.BOMB_ROUND, 1)
        self:add_career_cur_attr(CareerAttrID.BOMB_KILL, context.kill_num)
        self:add_career_cur_attr(CareerAttrID.BOMB_DAMAGE, context.damage)
        self:add_career_cur_attr(CareerAttrID.BOMB_HIT_HEAD, context.hit_head)
        self:add_career_cur_attr(CareerAttrID.BOMB_HIT_COUNT, context.hit_count)
        self:add_career_cur_attr(CareerAttrID.BOMB_WIN, win_counts)
        self:add_career_cur_attr(CareerAttrID.BOMB_MVP, mvp_counts)
        self:set_career_cur_attr(CareerAttrID.BOMB_BATTLE_TIME, otime())
        if context.win then
            self:add_career_cur_attr(CareerAttrID.BOMB_STREAK_WIN, 1)
            self:set_career_cur_attr(CareerAttrID.BOMB_STREAK_LOSE, 0)
        else
            self:set_career_cur_attr(CareerAttrID.BOMB_STREAK_WIN, 0)
            self:add_career_cur_attr(CareerAttrID.BOMB_STREAK_LOSE, 1)
        end
    elseif context.room_mode == GameMode.TEAM then
        self:add_career_cur_attr(CareerAttrID.TEAM_ROUND, 1)
        self:add_career_cur_attr(CareerAttrID.TEAM_KILL, context.kill_num)
        self:add_career_cur_attr(CareerAttrID.TEAM_DAMAGE, context.damage)
        self:add_career_cur_attr(CareerAttrID.TEAM_HIT_HEAD, context.hit_head)
        self:add_career_cur_attr(CareerAttrID.TEAM_HIT_COUNT, context.hit_count)
        self:add_career_cur_attr(CareerAttrID.TEAM_WIN, win_counts)
        self:add_career_cur_attr(CareerAttrID.TEAM_MVP, mvp_counts)
        self:set_career_cur_attr(CareerAttrID.TEAM_BATTLE_TIME, otime())
        if context.win then
            self:add_career_cur_attr(CareerAttrID.TEAM_STREAK_WIN, 1)
            self:set_career_cur_attr(CareerAttrID.TEAM_STREAK_LOSE, 0)
        else
            self:set_career_cur_attr(CareerAttrID.TEAM_STREAK_WIN, 0)
            self:add_career_cur_attr(CareerAttrID.TEAM_STREAK_LOSE, 1)
        end
    end
end

local function package_for_db(self)
    return self.career_map
end

function CareerComponent:package_to_data(data)
    data["carerr"] = package_for_db(self)
end

-- 数据库操作函数
function CareerComponent:update_data_2db(flush)
    if not self.career_dirty then
        return true
    end
    local ok = player_dao:update_player_career(self.player_id, package_for_db(self), flush)
    if ok then
        self.career_dirty = false
        return true
    end
    return false
end

return CareerComponent