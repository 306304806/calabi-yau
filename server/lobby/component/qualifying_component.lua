-- qualifying_component.lua
-- 排位组件
local mmin              = math.min
local tinsert           = table.insert
local log_err           = logger.err
local log_debug         = logger.debug
local tindexof          = table_ext.indexof

local DivisionStars     = enum("DivisionStars")
local CareerAttrID      = enum("CareerAttrID")

local config_mgr        = quanta.get("config_mgr")
local event_mgr         = quanta.get("event_mgr")
local rankseason_mgr    = quanta.get("rankseason_mgr")

local divisionstar_db   = config_mgr:get_table("divisionstar")

local QualifyingComponent = mixin(
    "get_division_id",
    "reset_season_qualifying_data",
    "update_qualifying_data",
    "total_qualifying_wins",    -- 某星以上排位赛胜场
    "get_qualifying_win_rate",   -- 排位赛胜率
    "get_stars",
    "get_brave_score",
    "get_streak_win",
    "get_streak_lose",
    "get_win_games",
    "get_mvp_games",
    "get_total_games",
    "get_last_login_season"
)

function QualifyingComponent:get_stars()
    return self:get_career_cur_attr(CareerAttrID.CUR_SEG)
end

function QualifyingComponent:get_brave_score()
    return self:get_career_cur_attr(CareerAttrID.BRAVE_SCORE)
end

function QualifyingComponent:get_streak_win()
    local bomb_cnt = self:get_career_cur_attr(CareerAttrID.BOMB_STREAK_WIN)
    local team_cnt = self:get_career_cur_attr(CareerAttrID.TEAM_STREAK_WIN)
    return bomb_cnt + team_cnt
end

function QualifyingComponent:get_streak_lose()
    local bomb_cnt = self:get_career_cur_attr(CareerAttrID.BOMB_STREAK_LOSE)
    local team_cnt = self:get_career_cur_attr(CareerAttrID.TEAM_STREAK_LOSE)
    return bomb_cnt + team_cnt
end

function QualifyingComponent:get_win_games()
    local bomb_cnt = self:get_career_cur_attr(CareerAttrID.BOMB_WIN)
    local team_cnt = self:get_career_cur_attr(CareerAttrID.TEAM_WIN)
    return bomb_cnt + team_cnt
end

function QualifyingComponent:get_mvp_games()
    local bomb_cnt = self:get_career_cur_attr(CareerAttrID.BOMB_MVP)
    local team_cnt = self:get_career_cur_attr(CareerAttrID.TEAM_MVP)
    return bomb_cnt + team_cnt
end

function QualifyingComponent:get_total_games()
    local bomb_cnt = self:get_career_cur_attr(CareerAttrID.BOMB_ROUND)
    local team_cnt = self:get_career_cur_attr(CareerAttrID.TEAM_ROUND)
    return bomb_cnt + team_cnt
end

function QualifyingComponent:get_last_login_season()
    return self:get_career_cur_attr(CareerAttrID.CUR_SEASON)
end

-- 获取排位赛胜率[保留两位小数]
function QualifyingComponent:get_qualifying_win_rate()
    local win_games = self:get_win_games()
    local total_games = self:get_total_games()
    local rate = (win_games or 0) / (total_games or 1)
    if rate > 0 then
        return (rate - rate % 0.01)
    end
    return 0
end

-- 获取段位
function QualifyingComponent:get_division_id()
    local stars = self:get_career_cur_attr(CareerAttrID.CUR_SEG)
    stars = mmin(stars, DivisionStars.STRONGEST)
    local cfg_info = divisionstar_db:find_one(stars)
    if not cfg_info then
        log_err("[qualifying_win] find star cfg failed! stars:%s", stars)
        return 1
    end
    return cfg_info.division or 1
end

-- 某星以上排位赛胜场
function QualifyingComponent:total_qualifying_wins(start_stars)
    local total = 0
    local bomb_attr_value = self:get_career_cur_attr(CareerAttrID.BOMB_STAR_GAME)
    for _, data in pairs(bomb_attr_value) do
        if data.stars > start_stars then
            total = total + data.total
        end
    end
    local team_attr_value = self:get_career_cur_attr(CareerAttrID.TEAM_STAR_GAME)
    for _, data in pairs(team_attr_value) do
        if data.stars > start_stars then
            total = total + data.total
        end
    end
    return total
end

-- 新赛季重置段位信息
function QualifyingComponent:reset_season_qualifying_data()
    log_debug("[QualifyingComponent][reset_season_qualifying_data]")
    -- todo:重置星星数
    self:set_career_cur_attr(CareerAttrID.BOMB_STREAK_WIN, 0)
    self:set_career_cur_attr(CareerAttrID.BOMB_STREAK_LOSE, 0)
    self:set_career_cur_attr(CareerAttrID.BOMB_WIN, 0)
    self:set_career_cur_attr(CareerAttrID.BOMB_MVP, 0)
    self:set_career_cur_attr(CareerAttrID.BOMB_ROUND, 0)
    self:set_career_cur_attr(CareerAttrID.BOMB_BATTLE_TIME, 0)
    self:set_career_cur_attr(CareerAttrID.TEAM_STREAK_WIN, 0)
    self:set_career_cur_attr(CareerAttrID.TEAM_STREAK_LOSE, 0)
    self:set_career_cur_attr(CareerAttrID.TEAM_WIN, 0)
    self:set_career_cur_attr(CareerAttrID.TEAM_MVP, 0)
    self:set_career_cur_attr(CareerAttrID.TEAM_ROUND, 0)
    self:set_career_cur_attr(CareerAttrID.TEAM_BATTLE_TIME, 0)

    self:set_career_cur_attr(CareerAttrID.BRAVE_SCORE, 0)
    self:set_career_cur_attr(CareerAttrID.STAR_GAME, {})
    self:set_career_cur_attr(CareerAttrID.CUR_SEG, 0)
    self:set_career_cur_attr(CareerAttrID.CUR_SEASON, rankseason_mgr:get_season_id())
end

-- 更新玩家排位数据
function QualifyingComponent:update_qualifying_data(context)
    if context.is_win then
        --更新当前星数胜利场数
        local is_exist
        local stars = self:get_career_cur_attr(CareerAttrID.CUR_SEG)
        local attr_value = self:get_career_cur_attr(CareerAttrID.STAR_GAME)
        for _, data in pairs(attr_value) do
            if data.stars and data.total and data.stars == stars then
                data.total = data.total + 1
                is_exist = true
                break
            end
        end
        if not is_exist then
            tinsert(attr_value, {stars = stars, total = 1})
        end
        self:set_career_cur_attr(CareerAttrID.STAR_GAME, attr_value)
    end

    self:set_career_cur_attr(CareerAttrID.CUR_SEG, context.new_stars)
    -- 当前赛季最高段位
    self:set_career_cur_attr(CareerAttrID.BRAVE_SCORE, context.new_brave_score)
    local cur_top_seg = self:get_career_cur_attr(CareerAttrID.TOP_SEG)
    if context.new_stars > cur_top_seg then
        self:set_career_cur_attr(CareerAttrID.TOP_SEG, context.new_stars)
    end

    -- 历史赛季最高段位
    local cur_season = rankseason_mgr:get_season_id()
    local his_top_seg = self:get_career_his_attr(CareerAttrID.TOP_SEG)
    if context.new_stars > his_top_seg then
        self:set_career_his_attr(CareerAttrID.TOP_SEG, context.new_stars)
        self:set_career_his_attr(CareerAttrID.TOP_SEASONS, {cur_season})
    elseif context.new_stars == his_top_seg then
        local top_seasons = self:get_career_his_attr(CareerAttrID.TOP_SEASONS)
        if not tindexof(top_seasons, cur_season) then
            tinsert(top_seasons, cur_season)
            self:set_career_his_attr(CareerAttrID.TOP_SEASONS, top_seasons)
        end
    end
    event_mgr:notify_trigger("evt_qualifying_update", self)
end

return QualifyingComponent
