--buff_component.lua
local Buff =  import("lobby/entity/buff.lua")
local tinsert           = table.insert
local log_err           = logger.err
local log_debug         = logger.debug

local OverlapType       = enum("OverlapType")

local player_dao        = quanta.get("player_dao")
local config_mgr        = quanta.get("config_mgr")

local buff_db           = config_mgr:get_table("buff")

-- 检查互斥buff
local function check_mutex_buff(self, conf)
    if conf.mutex_group == 0 then
        return true
    end

    for _, buff in pairs(self.buffs) do
        if buff:get_mutex_group() == conf.mutex_group then
            if conf.mutex_priority < buff:get_mutex_priority() then
                --新buff不是最高优先级
                return false
            else
                self:remove_buff(buff:get_id())
                return true
            end
        end
    end
    return true
end

-- 检查叠加buff
local function check_overlap_buff(self, conf)
    local overlap_type = conf.overlap_type
    local overlap_group = conf.overlap_group
    for _, buff in pairs(self.buffs) do
        if buff:get_overlap_group() == overlap_group then
            if overlap_type == OverlapType.COVER then
                self:remove_buff(buff:get_id())
                return true
            elseif overlap_type == OverlapType.SHARE then
                return true
            elseif overlap_type == OverlapType.TIME then
                buff:overlap_time(conf)
                return false
            else
                return false
            end
        end
    end
    return true
end

-- 添加共享组buff
local function add_share_buff(self, new_buff)
    --处理共享组
    local active_buff = new_buff
    local share_group = new_buff:get_share_group()
    local share_proprity = new_buff:get_share_priority()
    for _, buff in pairs(self.buffs) do
        if buff:get_share_group() == share_group then
            if share_proprity <= buff:get_share_priority() then
                --新buff不是最高优先级
                active_buff = nil
            end
        end
    end

    self.buffs[new_buff.id] = new_buff
    self.buff_dirty = true
    --处理buff
    new_buff:start()

    if active_buff then
        active_buff:active(self)
    end
end

local function new_buff_index(self)
    self.buff_index = self.buff_index + 1
    return self.buff_index
end

local BuffComponent = mixin(
    "add_buff",
    "get_buff",
    "get_buffs_list",
    "remove_buff"
)

local prop = property(BuffComponent)
prop:reader("buffs", {})
prop:reader("buff_dirty", false)
prop:reader("buff_index", 0)

function BuffComponent:__init(open_id, player_id, area_id, session)
end

function BuffComponent:setup(db_data)
    local player_buff = db_data.player_buff
    if not player_buff then
        return false
    end
    --加载buff
    local now = quanta.now
    for _, buff_data in pairs(player_buff.buff_datas or {}) do
        local buff_id = buff_data.buff_id
        local conf = buff_db:find_one(buff_id)
        local buff = Buff(new_buff_index(self), conf, buff_data)
        if not buff:is_expire(now) then
            add_share_buff(self, buff)
        end
    end
    return true
end

-- 获取buff
function BuffComponent:get_buff(buff_id)
    return self.buffs[buff_id]
end

-- 添加buff
function BuffComponent:add_buff(buff_id)
    log_debug("[BuffComponent][add_buff]->player_id:%s, buff_id:%s", self.player_id, buff_id)
    local conf = buff_db:find_one(buff_id)
    if not conf then
        log_err("[BuffComponent][add_buff] conf:%s isn't exist!", buff_id)
        return false
    end
    --检查互斥
    if not check_mutex_buff(self, conf) then
        return false
    end
    --检查叠加
    if not check_overlap_buff(self, conf) then
        return false
    end
    --添加到共享列表
    add_share_buff(self, Buff(new_buff_index(self), conf))
    return true
end

-- 移除buff
function BuffComponent:remove_buff(buff_id)
    local buff = self.buffs[buff_id]
    if buff then
        buff:stop(self)
        self.buffs[buff_id] = nil
    end
    self.buff_dirty = true
end

--更新
function BuffComponent:update(now)
    local dead_buffs = {}
    for buff_id, buff in pairs(self.buffs) do
        local dead = buff:run(now, self)
        if dead then
            tinsert(dead_buffs, buff_id)
        end
    end
    for _, buff_id in pairs(dead_buffs) do
        self:remove_buff(buff_id)
    end
end

--获取所有buff
function BuffComponent:get_buffs_list()
    local buff_datas = {}
    for _, buff in pairs(self.buffs) do
        tinsert(buff_datas, buff:pack2client())
    end
    return buff_datas
end

-- 打包为数据库格式
local function package_for_db(self)
    local buff_datas = {}
    for _, buff in pairs(self.buffs) do
        if buff:is_store() then
            tinsert(buff_datas, buff:pack2db())
        end
    end
    return buff_datas
end

-- 打包数据
function BuffComponent:package_to_data(data)
    data["buff"] = package_for_db(self)
end

-- 数据库操作函数
function BuffComponent:update_data_2db(flush)
    if not self.buff_dirty then
        return true
    end
    local buff_datas = package_for_db(self)
    local update_ok = player_dao:update_player_buff(self.player_id, buff_datas, flush)
    if update_ok then
        self.buff_dirty = false
        return true
    end
    return false
end

return BuffComponent
