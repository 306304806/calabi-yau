-- role_component.lua
local Role          = import("lobby/entity/role.lua")

local otime         = os.time
local tinsert       = table.insert
local tmapv2array   = table_ext.mapv2array
local tsize         = table_ext.size
local log_warn      = logger.warn
local log_err       = logger.err
local serialize     = logger.serialize

local ReddotType    = enum("ReddotType")
local ResAddType    = enum("ResAddType")
local BehaviorAttrID= enum("BehaviorAttrID")

local plat_api      = quanta.get("plat_api")
local event_mgr     = quanta.get("event_mgr")
local config_mgr    = quanta.get("config_mgr")
local free_role_mgr = quanta.get("free_role_mgr")
local player_dao    = quanta.get("player_dao")

local role_db       = config_mgr:get_table("role")
local roleskin_db   = config_mgr:get_table("roleskin")
local rolevoice_db  = config_mgr:get_table("rolevoice")
local roleaction_db = config_mgr:get_table("roleaction")

-- 生成过期时间
local function gen_expire_time(is_new, old_expire_time, life_time, cur_sys_time)
    if is_new then
        return (0 == life_time) and 0 or (life_time + cur_sys_time)
    else
        -- 原来是永久或者当前是永久
        if old_expire_time == 0 or life_time == 0 then
            return 0
        else
            -- 已经过期的需要用当前时间作为基数
            if old_expire_time < cur_sys_time then
                return cur_sys_time + life_time
            else
                return old_expire_time + life_time
            end
        end
    end
end

-- 添加类型是否允许自动转换
local function need_auto_recycle(add_type)
    return add_type ~= ResAddType.UNDEFINE
        and add_type ~= ResAddType.ROLE_DEFAULT
        and add_type ~= ResAddType.PLAYER_DEFAULT
        and add_type ~= ResAddType.WEEK_FREE
        and add_type ~= ResAddType.NOT_OWNED
        and add_type ~= ResAddType.GM
end

-- 投递add_role相关事件
local function dispatch_add_role_event(has_changed, has_add, has_unlock, player, role, add_cmd)
    -- 添加事件
    if has_add then
        event_mgr:notify_trigger("evt_player_add_role", player, role, add_cmd.reason)
    end

    -- 设置红点
    if has_unlock and add_cmd.reason ~= ResAddType.PLAYER_DEFAULT and add_cmd.reason ~= ResAddType.WEEK_FREE then
        plat_api:reddot_add_req(player.player_id, ReddotType.NEW_ROLE, add_cmd.role_id, role.expire_time)
    end

    -- 有更新
    if has_changed then
        event_mgr:notify_trigger("evt_player_role_change", player, {role:package_role_info()})
    end
end

-- 投递add_role_skin相关事件
local function dispatch_add_role_skin_event(has_changed, has_unlock, self, skin_info, skin_add_cmd)
    if has_unlock then
        -- 设置红点
        if skin_add_cmd.reason ~= ResAddType.WEEK_FREE and skin_add_cmd.reason ~= ResAddType.PLAYER_DEFAULT then
            plat_api:reddot_add_req(self.player_id, ReddotType.NEW_ROLE_SKIN, skin_add_cmd.role_skin_id, skin_add_cmd.life_time)
        end

        event_mgr:notify_trigger("evt_player_add_role_skin", self, skin_add_cmd.role_skin_id)
    end

    if has_changed then
        event_mgr:notify_trigger("evt_player_role_skin_change", self, {skin_info})
    end
end

local RoleComponent = mixin(
    "get_role_infos",              -- 获取角色信息列表
    "get_usable_role_infos",       -- 获取可用角色信息列表
    "is_owned_role",               -- 是否已经拥有的角色
    "flush_free_roles",            -- 刷新免费角色
    "set_loved_role_id",           -- 设置最喜欢的角色
    "add_role",                    -- 添加新角色
    "get_role",                    -- 获取某个角色信息
    "get_roles_cnt",               -- 获取角色个数
    "get_role_skins_cnt",          -- 获取皮肤个数
    "get_role_skin_infos",         -- 获取皮肤信息列表
    "get_role_skin_info",
    "add_role_skin",               -- 添加新皮肤
    "get_role_voice",              -- 获取角色语音
    "add_role_voice",              -- 添加角色语音
    "get_role_action",             -- 获取角色动作
    "add_role_action",             -- 添加角色动作
    "add_role_exp",                -- 添加角色经验
    "update_displaying_role_id",   -- 获取当前展示角色
    "select_role_skin",            -- 选择角色皮肤
    "is_skin_exist",               -- 检查角色皮肤
    "get_all_voice_ids",
    "get_all_action_ids"
)

local prop = property(RoleComponent)
prop:reader("role_info_dirty", false)
prop:reader("role_skin_dirty", false)
prop:accessor("roles", {})          -- 角色集合
prop:accessor("displaying_role_id", 0) -- 当前暂时的角色
prop:accessor("sid2skin", {})          -- 皮肤集合
prop:accessor("loved_role_id", 0)      -- 最喜爱的角色
prop:accessor("free_role_version", nil)  -- 免费英雄版本

function RoleComponent:__init(open_id, player_id, area_id, session)
end

-- 执行数据加载
function RoleComponent:setup(db_data)
    local role_data = db_data.player_role_info
    if not role_data then
        return false
    end
    local skin_datas = db_data.player_role_skin
    if not skin_datas then
        return false
    end
    -- 构造内存数据
    self.roles           = {}
    self.sid2skin           = {}
    self.loved_role_id      = 0  -- 默认没有最喜爱的角色
    self.displaying_role_id = role_data.displaying_role_id or 0

    -- 关键内存数据构造
    -- 角色映射表 & 最喜爱角色
    for _, role_info in pairs(role_data.roles or {}) do
        local role = Role()
        if role:serialize_from_db(role_info) then
            self.roles[role:get_role_id()] = role
            if true == role_info.loved then
                self.loved_role_id = role_info.role_id
            end
        else
            log_err("[RoleComponent][setup] load role faild: player_id=%s, data=%s", self.player_id, serialize(role_info))
        end
    end

    -- 皮肤列表
    for _, skin_info in pairs(skin_datas.skins or {}) do
        self.sid2skin[skin_info.role_skin_id] = {
            role_skin_id = skin_info.role_skin_id,
            expire_time  = skin_info.expire_time or 0,
            obtain_time  = skin_info.obtain_time or otime(),
        }
    end

    -- 临时角色（当前为限免）
    self:flush_free_roles(self)

    return true
end

-- 获取角色信息列表
function RoleComponent:get_role_infos()
    local infos = {}
    for role_id, role in pairs(self.roles) do
        tinsert(infos, role:package_role_info())
    end
    return infos
end

-- 获取可用的角色基础信息
-- out: {{rid,sid},,,}
function RoleComponent:get_usable_role_infos()
    local base_infos = {}
    for role_id, role in pairs(self.roles) do
        if role:get_owned() or free_role_mgr:is_free_role(role_id) then
            local role_info = { role_id = role:get_role_id(), skin_id = role:get_skin_id() }
            tinsert(base_infos, role_info)
        end
    end
    return base_infos
end

-- 检查是否是已经拥有的角色
function RoleComponent:is_owned_role(role_id)
    local role = self.roles[role_id]
    return role and role:get_owned() or false
end

-- 获取角色数量
function RoleComponent:get_roles_cnt()
    return tsize(self.roles, function(role)
        return role:get_owned()
    end)
end

-- 获取皮肤数量
function RoleComponent:get_role_skins_cnt()
    return tsize(self.sid2skin)
end

-- 新增角色
-- @param role_add_cmd = {
--   role_id    -- 角色ID
--   life_time  -- 有时长
--   not_owned  -- 非实际拥有
--   reason     -- 添加原因
--   }
-- @return
--   是执行错误 true
--   超限返回规则 {item_id, limit_conv_info = {被转换后的结果}}
function RoleComponent:add_role(role_add_cmd)
    local cur_time = otime()
    local has_changed = false   -- 有改变行为
    local has_unlock  = false   -- 有解锁行为
    local has_add     = false   -- 有添加行为
    local result      = {}

    -- 未配置的角色
    local role_cfg = role_db:find_one(role_add_cmd.role_id)
    if not role_cfg then
        log_err("[RoleComponent][add_role] role config not found: add_cmd=%s", serialize(role_add_cmd))
        return false
    end

    local role = self.roles[role_add_cmd.role_id]
    if not role then
        role = Role()
        role:set_role_id(role_add_cmd.role_id)
        role:set_obtain_time(cur_time)
        role:set_expire_time(gen_expire_time(true, 0, role_add_cmd.life_time, cur_time))
        role:set_owned(not role_add_cmd.not_owned)
        self.roles[role_add_cmd.role_id] = role

        has_add = true
        has_changed = true
        has_unlock = not role_add_cmd.not_owned
    else
        -- 对已经添加的角色，只有需要解锁时才会有数据更新
        if not role_add_cmd.not_owned then
            -- 已拥有的续期和自动回收处理
            if role:get_owned(role_add_cmd.reason) then
                -- 永久角色重复添加
                if need_auto_recycle() and (0 == role:get_expire_time()) and (0 == role_add_cmd.life_time) then
                    if 0 ~= role_cfg.limit_type then
                        local currency_id  = role_cfg.limit_type
                        local currency_num = role_cfg.limit_param * 1
                        if self:add_currency_item(currency_id, currency_num) then
                            result.item_id = role_add_cmd.role_id
                            result.limit_conv_info = {currency_id = currency_id, currency_num = currency_num, conv_num = 1}
                        end
                    end
                else
                    -- 延长超时时间
                    role:set_expire_time(gen_expire_time(true, 0, role_add_cmd.life_time, cur_time))
                    has_changed = true
                end
            else  -- 已过期的续期处理
                -- 延长超时时间
                role:set_expire_time(gen_expire_time(true, 0, role_add_cmd.life_time, cur_time))
                has_changed = true
                has_unlock  = true
            end
        end
    end

        -- 防止后续逻辑错误导致无法标记脏数据
    if has_changed then
        self.role_info_dirty = has_changed
    end

    dispatch_add_role_event(has_changed, has_add, has_unlock, self, role, role_add_cmd)

    return true, result
end

-- 设置最喜爱的角色
function RoleComponent:set_loved_role_id(role_id, loved)
    local role           = self.roles[role_id]
    local cur_loved_role = self.roles[self.loved_role_id]

    -- 修改久数据
    if cur_loved_role and loved == true and role_id ~= self.loved_role_id then
        cur_loved_role:set_loved(false)
    end

    -- 设置新数据
    role:set_loved(loved)

    -- 更新当前最喜爱
    if true == loved then
        self.loved_role_id = role_id
    end
end

-- 获取皮肤列表
function RoleComponent:get_role_skin_infos()
    return tmapv2array(self.sid2skin)
end

-- 获取皮肤信息
function RoleComponent:get_role_skin_info(skin_id)
    return self.sid2skin[skin_id]
end

-- 添加皮肤
function RoleComponent:add_role_skin(skin_add_cmd)
    local cur_time = otime()
    local skin_cfg = roleskin_db:find_one(skin_add_cmd.role_skin_id)
    if not skin_cfg then
        log_err("[RoleComponent][add_role_skin] skin config not found: player_id=%s,add_cmd=%s", self.player_id, serialize(skin_add_cmd))
        return false
    end

    local result = {item_id = skin_add_cmd.role_skin_id, limit_conv_info = {}}
    local has_changed = false
    local has_unlock = false
    local skin_info = self.sid2skin[skin_add_cmd.role_skin_id]
    -- 已有皮肤续期
    if skin_info then
        -- 已拥有的是非永久皮肤
        if 0 ~= skin_info.expire_time then
            if skin_info.expire_time < otime() then
                has_unlock = true
            end
            skin_info.expire_time = gen_expire_time(false, skin_info.expire_time, skin_add_cmd.life_time, cur_time)
            has_changed = true
        else
            -- 重复添加永久皮肤
            if 0 == skin_add_cmd.life_time then
                if need_auto_recycle(skin_add_cmd.reason) and 0 ~= skin_cfg.limit_type then
                    local currency_id  = skin_cfg.limit_type
                    local currency_num = skin_cfg.limit_param * 1
                    if self:add_currency_item(currency_id, currency_num) then
                        result.limit_conv_info = {currency_id = currency_id, currency_num = currency_num, conv_num = 1}
                    end
                end
            end
        end
    else -- 新解锁
        skin_info = {
            role_skin_id = skin_add_cmd.role_skin_id,
            expire_time  = gen_expire_time(true, 0, skin_add_cmd.life_time, cur_time),
            obtain_time  = cur_time,
        }
        self.sid2skin[skin_add_cmd.role_skin_id] = skin_info
        has_changed = true
        has_unlock  = true
    end

    -- 防止后续逻辑错误导致无法标记脏数据
    if has_changed then
        self.role_skin_dirty = has_changed
    end

    dispatch_add_role_skin_event(has_changed, has_unlock, self, skin_info, skin_add_cmd)

    return true, result
end

-- 获取角色语音
function RoleComponent:get_role_voice(role_id, voice_id)
    local role = self.roles[role_id]
    if role then
        return role:get_voice(voice_id)
    end
end

-- 添加角色语音
function RoleComponent:add_role_voice(role_id, voice_id)
    local role = self.roles[role_id]
    if not role then
        self:add_role({role_id = role_id, life_time = 0, not_owned = true, reason = ResAddType.NOT_OWNED})
        role = self.roles[role_id]
    end
    if role then
        local cfg = rolevoice_db:find_one(voice_id)
        if cfg then
            if not role:get_voice(voice_id) then
                role:add_voice(voice_id)
                self.role_info_dirty = true
                plat_api:reddot_add_req(self.player_id, ReddotType.NEW_VOICE, voice_id)

                -- player, add_ids, rmove_ids
                event_mgr:notify_trigger("evt_player_role_voice_update", self, {voice_id}, {})
                return true
            else
                local result = {item_id = voice_id, limit_conv_info = {}}
                if 0 ~= cfg.limit_type then
                    result.limit_conv_info = {currency_id = cfg.limit_type, currency_num = cfg.limit_param, conv_num = 1}
                end
                return true, result
            end
        end
    end

    return false
end

-- 获取角色动作
function RoleComponent:get_role_action(role_id, action_id)
    local role = self.roles[role_id]
    if role then
        return role:get_action(action_id)
    end
end

-- 添加角色动作
function RoleComponent:add_role_action(role_id, action_id)
    local role = self.roles[role_id]
    if not role then
        self:add_role({role_id = role_id, life_time = 0, not_owned = true, reason = ResAddType.NOT_OWNED})
        role = self.roles[role_id]
    end
    if role then
        local cfg = roleaction_db:find_one(action_id)
        if cfg then
            if not role:get_action(action_id) then
                role:add_action(action_id)
                self.role_info_dirty = true
                plat_api:reddot_add_req(self.player_id, ReddotType.NEW_ACTION, action_id)

                -- player, add_ids, rmove_ids
                event_mgr:notify_trigger("evt_player_role_action_update", self, {action_id}, {})

                return true
            else  -- 自动转换
                local result = {item_id = action_id, limit_conv_info = {}}
                if 0 ~= cfg.limit_type then
                    result.limit_conv_info = {currency_id = cfg.limit_type, currency_num = cfg.limit_param, conv_num = 1}
                end
                return true, result
            end
        end
    end

    return false
end

-- 添加角色经验
function RoleComponent:add_role_exp(role_id, exp_changed)
    local role = self.roles[role_id]
    if role and (0 ~= exp_changed) then
        role:add_exp(exp_changed)

        -- player, role_id, exp
        event_mgr:notify_trigger("evt_player_role_exp_update", self, role_id, role:get_exp())

        event_mgr:notify_trigger("ntf_behavior_event", self.player_id, BehaviorAttrID.CUR_ROLE_EXP, exp_changed)
        event_mgr:notify_trigger("ntf_behavior_event", self.player_id, BehaviorAttrID.TOTAL_ROLE_EXP,
                                 self:get_behavior_attr(BehaviorAttrID.TOTAL_ROLE_EXP) + exp_changed)
    end
end

-- 更新展示角色
function RoleComponent:update_displaying_role_id(role_id)
    if self:get_role(role_id) then
        self.displaying_role_id = role_id
        self.role_info_dirty = true
    end
end

-- 装在皮肤
function RoleComponent:select_role_skin(role_id, skin_id)
   local role = self.roles[role_id]
   role:set_skin_id(skin_id)
   self.role_info_dirty = true

   event_mgr:notify_trigger("evt_player_role_skin_select", self, role_id, skin_id)
end

function RoleComponent:get_role(role_id)
    return self.roles[role_id]
end

function RoleComponent:update()
    local cur_time = otime()
    -- 限时道具扫描
    for role_id, role in pairs(self.roles) do
        if role:get_expire_time() > 0 and role:get_expire_time() < cur_time then
            -- 重置最最喜爱role
            if self.loved_role_id == role_id then
                self.loved_role_id = 0
            end

            -- 重置展示role
            if self.displaying_role_id == role_id then
                self.displaying_role_id = role_id
            end

            -- 标记未拥有
            role:set_owned(false)
            event_mgr:notify_trigger("evt_player_role_timeout", self, role_id)

            self.role_info_dirty = true
        end
    end

    for _, skin_info in pairs(self.sid2skin) do
        if skin_info.expire_time > 0 and skin_info.expire_time < cur_time then
            -- 如果当前皮肤正在使用，设回默认皮肤
            local skin_cfg = roleskin_db:find_one(skin_info.role_skin_id)
            local role = self:get_role(skin_cfg.role_id)
            if role and role:get_skin_id() == skin_info.role_skin_id then
                local role_cfg = role_db:find_one(skin_cfg.role_id)
                self:select_role_skin(skin_cfg.role_id, role_cfg.skin_id)
            end
            local skin_id = skin_info.role_skin_id
            self.sid2skin[skin_info.role_skin_id] = nil
            event_mgr:notify_trigger("evt_player_role_skin_timeout", self, skin_id)

            self.role_skin_dirty = true
        end
    end
end

-- 打包角色为数据库格式
function RoleComponent:package_role_for_db()
    local data = {
        displaying_role_id = 0,
        roles = {}
    }

    for _, role in pairs(self.roles) do
        tinsert(data.roles, role:serialize_to_db())
    end

    return data
end

-- 打包皮肤为数据库格式
function RoleComponent:package_skin_for_db()
    local skins = {}
    for _, skin in pairs(self.sid2skin) do
        tinsert(skins, skin)
    end

    return skins
end

-- 打包数据
function RoleComponent:package_to_data(data)
    data["role"] = RoleComponent.package_role_for_db(self)
    data["skin"] = RoleComponent.package_skin_for_db(self)
end

-- 数据库操作函数
function RoleComponent:update_data_2db(flush)
    -- 更新role
    if self.role_info_dirty then
        local role_data = RoleComponent.package_role_for_db(self)
        local ok = player_dao:update_player_role_info(self.player_id, role_data, flush)
        if ok then
            self.role_info_dirty = false
        end
    end
    -- 更新skin
    if self.role_skin_dirty then
        local skin_data = RoleComponent.package_skin_for_db(self)
        local ok1 = player_dao:update_player_role_skin(self.player_id, skin_data, flush)
        if ok1 then
            self.role_skin_dirty = false
        end
    end

    if not self.role_info_dirty or not self.role_skin_dirty then
        return true
    end

    return false
end

-- 检查皮肤存在
function RoleComponent:is_skin_exist(role_skin_id)
    if self.sid2skin[role_skin_id] then
        return true
    else
        return false
    end
end

-- 限免刷新
function RoleComponent:flush_free_roles()
    if self.free_role_version ~= free_role_mgr:get_free_role_version() then
        local frole_id_set = free_role_mgr:get_free_role_id_set()
        for rid, _ in pairs(frole_id_set) do
            if not self.roles[rid] then
                local role_cfg  = role_db:find_one(rid)
                local skin_cfg  = roleskin_db:find_one(role_cfg.role_skin)
                if not role_cfg or not skin_cfg then
                    log_warn("[RoleComponent][flush_free_roles] role or skin not configed: role_id=%s,skin_id=%s", rid, role_cfg.role_skin)
                else
                    -- 添加角色(默认皮肤会自动添加)
                    self:add_role({role_id = rid, life_time = 0, not_owned = true, reason = ResAddType.WEEK_FREE})
                end
            end
        end

        self.free_role_version = free_role_mgr:get_free_role_version()
    end
end

-- 获取已解锁的全部声音id
function RoleComponent:get_all_voice_ids()
    local ids = {}
    for _, role in pairs(self.roles) do
        for _, voice in pairs(role.voices or {}) do
            tinsert(ids, voice)
        end
    end

    return ids
end

-- 获取已解锁的全部动作id
function RoleComponent:get_all_action_ids()
    local ids = {}
    for _, role in pairs(self.roles) do
        for _, action in pairs(role.actions or {}) do
            tinsert(ids, action)
        end
    end

    return ids
end

return RoleComponent

