-- vcard_component.lua
local otime         = os.time
local tinsert       = table.insert
local log_info      = logger.info

local event_mgr     = quanta.get("event_mgr")
local config_mgr    = quanta.get("config_mgr")
local player_dao    = quanta.get("player_dao")

local PlayerAttrID  = enum("PlayerAttrID")
local idcard_db     = config_mgr:get_table("idcard")

local VCardComponent = mixin(
    "add_vcard_resource",
    "get_vcard_resource",
    "add_vcard_resources",
    "get_vcard_resource_attr",
    "pack_vcard_resource"
)

local prop = property(VCardComponent)
prop:reader("vcard_dirty", false)
prop:reader("vcard_resources", {})

function VCardComponent:__init(open_id, player_id, area_id, session)
end

function VCardComponent:setup(db_data)
    local vcard = db_data.player_vcard
    if not vcard then
        return false
    end
    local vcards = vcard.vcards
    if vcards and #vcards > 0 then
        for _, resource in pairs(vcards) do
            self.vcard_resources[resource.resource_id] = resource
        end
    end
    return true
end

local function package_for_db(self)
    local resources = {}
    for _, resource in pairs(self.vcard_resources or {}) do
        tinsert(resources, resource)
    end
    return resources
end

function VCardComponent:package_to_data(data)
    data["vcard"] = package_for_db(self)
end

function VCardComponent:update_data_2db(flush)
    if not self.vcard_dirty then
        return true
    end
    local ok = player_dao:update_player_vcard(self.player_id, package_for_db(self), flush)
    if ok then
        self.vcard_dirty = false
        return true
    end
    return false
end

function VCardComponent:update()
    -- 限时名品框扫描
    local cur_time = quanta.now
    for res_id, resource in pairs(self.vcard_resources or {}) do
        if resource.expire_time > 0 and resource.expire_time < cur_time then
            log_info("[VCardComponent][update] vcard timeout: frame_id=%s", res_id)
            -- 检查当前是否正在使用这个名片框，如果正在使用需要设置为默认值
            local attr_id = self:get_vcard_resource_attr(res_id)
            if self:get_attribute(attr_id) == res_id then
                self:set_attribute(attr_id, 0)
            end
            self.resources[res_id] = nil
            event_mgr:notify_trigger("ntf_vcard_resource_timeout", self)
            self.vcard_dirty = true
        end
    end
end

-- 添加名片框
function VCardComponent:add_vcard_resource(resource_id, life_time)
    local cur_time = otime()
    local cfg = idcard_db:find_one(resource_id)
    if cfg then
        local cur_frame = self.vcard_resources[resource_id]
        if not cur_frame then
            self.vcard_resources[resource_id] = {
                obtain_time = cur_time,
                resource_id = resource_id,
                expire_time = (0 == life_time) and 0 or (cur_time + life_time),
            }

            return true
        elseif 0 ~= cur_frame.expire_time then
            cur_frame.expire_time = (0 == life_time) and 0 or (cur_frame.expire_time + life_time)
        else
            local result = {item_id = resource_id, limit_conv_info = {}}
            if 0 ~= cfg.limit_type then
                result.limit_conv_info = {currency_id = cfg.limit_type, currency_num = cfg.limit_param, conv_num = 1}
            end

            return true, result
        end
    end

    return false
end

-- 批量添加名片框
function VCardComponent:add_vcard_resources(resources)
    for _, resource in pairs(resources) do
        self:add_vcard_resource(resource.resource_id, resource.life_time)
    end
    -- 进程内广播
    event_mgr:notify_trigger("ntf_vcard_resource_sync", self)
    self.vcard_dirty = true
end

-- 根据_id获取名片资源信息
function VCardComponent:get_vcard_resource(id)
    return self.vcard_resources[id]
end

-- 获取名片框列表
function VCardComponent:pack_vcard_resource()
    local vcard = {
        resources = package_for_db(self),
    }
    return vcard
end

-- 更新名片(调用方必须自己验证好参数合法性)
function VCardComponent:get_vcard_resource_attr(res_id)
    local vcard_res_attrs = {
        [1] = PlayerAttrID.VCARD_AVATAR_ID,
        [2] = PlayerAttrID.VCARD_FRAME_ID,
        [3] = PlayerAttrID.VCARD_BORDER_ID,
    }
    local resource = idcard_db:find_one(res_id)
    if resource then
        return vcard_res_attrs[resource.type]
    end
end

-- export
return VCardComponent