-- role_component.lua

local mfloor            = math.floor
local tinsert           = table.insert

local event_mgr         = quanta.get("event_mgr")
local config_mgr        = quanta.get("config_mgr")
local player_dao        = quanta.get("player_dao")

local attribute_db      = config_mgr:get_table("attribute")
local utility_db        = config_mgr:get_table("utility")

local GameConst         = enum("GameConst")
local PlayerAttrID      = enum("PlayerAttrID")

local RATE_BASE         = GameConst.RATE_BASE
local WEEK_EXP_LIMIT    = utility_db:find_value("value", "week_exp_limit")
local WEEK_IDEAL_LIMIT  = utility_db:find_value("value", "week_ideal_limit")

local AttributeComponent = mixin(
    "set_attribute",
    "get_attribute",
    "add_exp",
    "get_exp",
    "set_level",
    "get_level",
    "get_crystal",
    "add_crystal",
    "cost_crystal",
    "get_hermes",
    "add_hermes",
    "cost_hermes",
    "get_explore",
    "set_explore",
    "add_explore",
    "get_ideal",
    "add_ideal",
    "cost_ideal",
    "get_login_time",
    "set_login_time",
    "get_logout_time",
    "get_weapon_exp_inc_rate",
    "set_logout_time",
    "get_nick",
    "set_nick",
    "get_sex",
    "set_sex",
    "get_icon",
    "set_icon"
)

local prop = property(AttributeComponent)
prop:reader("attr_dirty", false)
prop:reader("attr_map", {})       -- 玩家的属性列表
prop:reader("attr_modifys", {})   -- 需要推送给客户端的属性

function AttributeComponent:__init(open_id, player_id, area_id, session)
end

function AttributeComponent:setup(db_data)
    local player_attribute = db_data.player_attribute
    if not player_attribute then
        return false
    end
    -- 填充默认属性
    for _, cfg in attribute_db:iterator() do
        self.attr_map[cfg.id] = { id = cfg.id, value_type = cfg.type, value = cfg.default, save2db = cfg.save2db }
    end
    for _, item in pairs(player_attribute.attributes or {}) do
        self.attr_map[item.id].value = item.value
    end
    return true
end

--周刷新
function AttributeComponent:flush_week(week_edition)
    self:set_attribute(PlayerAttrID.WEEK_EXP, 0)
    self:set_attribute(PlayerAttrID.WEEK_IDEAL, 0)
end

-- 设置属性
function AttributeComponent:set_attribute(id, value, reason)
    local attr = self.attr_map[id]
    if attr then
        -- 不相等才会触发后续逻辑
        local old_value = attr.value
        if value ~= old_value then
            attr.value = value
            self.attr_modifys[id] = attr
            self.attr_dirty = true
            event_mgr:notify_trigger("evt_player_attribute_change", self, id, old_value, value, reason)
        end
    end
end

-- 获取属性
function AttributeComponent:get_attribute(id)
    local attr = self.attr_map[id]
    if attr then
        return attr.value
    end
end

-- 设置等级
function AttributeComponent:set_level(level)
    self:set_attribute(PlayerAttrID.LEVEL, level)
end

-- 获取等级
function AttributeComponent:get_level()
    return self:get_attribute(PlayerAttrID.LEVEL)
end

-- 获取经验
function AttributeComponent:get_exp()
    return self:get_attribute(PlayerAttrID.EXP)
end

-- 增加经验
function AttributeComponent:add_exp(cnt, direct)
    local exp = self:get_exp()
    if not direct then
        if cnt > 0 then
            cnt = mfloor(cnt + cnt * self:get_attribute(PlayerAttrID.EXP_INC_RATE) / RATE_BASE)
        end
    end
    --处理周上限
    local week_exp = self:get_attribute(PlayerAttrID.WEEK_EXP)
    if week_exp + cnt > WEEK_EXP_LIMIT then
        cnt = WEEK_EXP_LIMIT - week_exp
    end
    if cnt > 0 then
        self:set_attribute(PlayerAttrID.WEEK_EXP, week_exp + cnt)
        self:set_attribute(PlayerAttrID.EXP, exp + cnt)
    end
    return cnt
end

-- 获取巴布晶体
function AttributeComponent:get_crystal()
    return self:get_attribute(PlayerAttrID.CRYSTAL)
end

-- 增加巴布晶体
function AttributeComponent:add_crystal(cnt, reason, direct)
    local cur_cnt = self:get_crystal()
    if not direct then
        if cnt > 0 then
            cnt = mfloor(cnt + cnt * self:get_attribute(PlayerAttrID.CRYSTAL_INC_RATE) / RATE_BASE)
        end
    end
    self:set_attribute(PlayerAttrID.CRYSTAL, cur_cnt + cnt, reason)
    return cnt
end

-- 消耗巴布晶体
function AttributeComponent:cost_crystal(cnt, reason)
    local cur_crystal = self:get_crystal()
    if cur_crystal < cnt then
        return false
    end
    self:set_attribute(PlayerAttrID.CRYSTAL, cur_crystal - cnt, reason)
    return true
end

-- 获取理想币
function AttributeComponent:get_ideal()
    return self:get_attribute(PlayerAttrID.IDEAL)
end

-- 增加理想币
function AttributeComponent:add_ideal(cnt, reason, direct)
    local cur_cnt = self:get_ideal()
    if not direct then
        if cnt > 0 then
            cnt = mfloor(cnt + cnt * self:get_attribute(PlayerAttrID.IDEAL_INC_RATE) / RATE_BASE)
        end
    end
    --处理周上限
    local week_ideal = self:get_attribute(PlayerAttrID.WEEK_IDEAL)
    if week_ideal + cnt > WEEK_IDEAL_LIMIT then
        cnt = WEEK_IDEAL_LIMIT - week_ideal
    end
    if cnt > 0 then
        self:set_attribute(PlayerAttrID.IDEAL, cur_cnt + cnt, reason)
        self:set_attribute(PlayerAttrID.WEEK_IDEAL, week_ideal + cnt)
    end
    return cnt
end

-- 消耗理想币
function AttributeComponent:cost_ideal(cnt, reason)
    local cur_ideal = self:get_ideal()
    if cur_ideal < cnt then
        return false
    end
    self:set_attribute(PlayerAttrID.IDEAL, cur_ideal - cnt, reason)
    return true
end

-- 获取赫尔币
function AttributeComponent:get_hermes()
    return self:get_attribute(PlayerAttrID.HERMES)
end

--- 增加赫尔币
function AttributeComponent:add_hermes(cnt, reason)
    local cur_cnt = self:get_hermes()
    self:set_attribute(PlayerAttrID.HERMES, cur_cnt + cnt, reason)
    return cnt
end

-- 消耗赫尔币
function AttributeComponent:cost_hermes(cnt, reason)
    local cur_hermes = self:get_hermes()
    if cur_hermes < cnt then
        return false
    end
    self:set_attribute(PlayerAttrID.HERMES, cur_hermes - cnt, reason)
    return true
end

--武器经验加成
function AttributeComponent:get_weapon_exp_inc_rate()
    return self:get_attribute(PlayerAttrID.WEAPON_EXP_INC_RATE) / RATE_BASE
end

--- 设置探索点
function AttributeComponent:set_explore(cnt, reason)
    self:set_attribute(PlayerAttrID.EXPLORE, cnt, reason)
end

-- 获取探索点
function AttributeComponent:get_explore()
    return self:get_attribute(PlayerAttrID.EXPLORE)
end

--- 增加探索点
function AttributeComponent:add_explore(cnt, reason)
    local cur_cnt = self:get_explore()
    self:set_explore(cur_cnt + cnt, reason)
    return cnt
end

function AttributeComponent:get_nick()
    return self:get_attribute(PlayerAttrID.NICK)
end

function AttributeComponent:set_nick(nick)
    return self:set_attribute(PlayerAttrID.NICK, nick)
end

function AttributeComponent:get_sex()
    return self:get_attribute(PlayerAttrID.SEX)
end

function AttributeComponent:set_sex(sex)
    return self:set_attribute(PlayerAttrID.SEX, sex)
end

function AttributeComponent:get_icon()
    return self:get_attribute(PlayerAttrID.ICON)
end

function AttributeComponent:set_icon(icon_id)
    return self:set_attribute(PlayerAttrID.ICON, icon_id)
end

function AttributeComponent:get_login_time()
    return self:get_attribute(PlayerAttrID.LOGIN_TIME)
end

function AttributeComponent:set_login_time()
    return self:set_attribute(PlayerAttrID.LOGIN_TIME, quanta.now)
end

function AttributeComponent:get_logout_time()
    return self:get_attribute(PlayerAttrID.LOGOUT_TIME)
end

function AttributeComponent:set_logout_time()
    return self:set_attribute(PlayerAttrID.LOGOUT_TIME, quanta.now)
end

function AttributeComponent:update()
    if next(self.attr_modifys) then
        event_mgr:notify_trigger("ntf_dirty_attribute", self, self.attr_modifys)
        self.attr_modifys = {}
    end
end

-- 打包为数据库格式
local function package_for_db(self)
    local attrs = {}
    for id, attr in pairs(self.attr_map) do
        if attr.save2db then
            tinsert(attrs, { id = id, value = attr.value })
        end
    end
    return attrs
end

-- 数据打包
function AttributeComponent:package_to_data(data)
    data.attribute = package_for_db(self)
end

-- 数据库操作函数
function AttributeComponent:update_data_2db(flush)
    if not self.attr_dirty then
        return true
    end
    local ok = player_dao:update_player_attribute(self.player_id, package_for_db(self), flush)
    if ok then
        self.attr_dirty = false
        return true
    end
    return false
end

return AttributeComponent