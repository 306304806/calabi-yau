-- battlepass_component.lua
local BattlePassTask = import("lobby/entity/battlepass_task.lua")

local otime         = os.time
local log_debug     = logger.debug
local log_info      = logger.info
local log_err       = logger.err
local serialize     = logger.serialize
local tinsert       = table.insert
local uedition_utc  = utility.edition_utc

local event_mgr      = quanta.get("event_mgr")
local player_dao     = quanta.get("player_dao")
local config_mgr     = quanta.get("config_mgr")
local battlepass_mgr = quanta.get("battlepass_mgr")

local utility_db        = config_mgr:get_table("utility")
local behavior_attr_db  = config_mgr:get_table("behaviorattribute")

local FLUSH_TIME        = utility_db:find_value("value", "day_flush_time")

local ResAddType          = enum("ResAddType")
local BattlePassTaskType  = enum("BattlePassTaskType")
local BattlePassTaskState = enum("BattlePassTaskState")
local BattlePassPrizeMode = enum("BattlePassPrizeMode")

--构造包含物品自动超限转换的获得列表信息
local function build_obtain_data(result_list)
    local obtain_data = { result_list = {}, convert_list = {}}
    local temp_add_item = {}
    local temp_conv = {}
    for _, result in pairs(result_list) do
        if result.add_success then
            local add_item_id = result.item_id
            if not temp_add_item[add_item_id] then
                temp_add_item[add_item_id] = 0
            end
            temp_add_item[add_item_id] = temp_add_item[add_item_id] + (result.num or 0)

            if next(result.limit_conv_info) then
                if not temp_conv[add_item_id] then
                    temp_conv[add_item_id] = result.limit_conv_info
                else
                    local conv_data = temp_conv[add_item_id]
                    conv_data.currency_num =  conv_data.currency_num + result.limit_conv_info.currency_num
                    conv_data.conv_num = conv_data.conv_num + result.limit_conv_info.conv_num
                end
            end
        end
    end
    for id, num in pairs(temp_add_item) do
        tinsert(obtain_data.result_list, {item_id = id, count = num})
    end
    for id, data in pairs(temp_conv) do
        tinsert(obtain_data.convert_list, {item_id = id, count = data.conv_num, currency_id = data.currency_id, currency_cnt = data.currency_num})
    end
    return obtain_data
end

-- 从数据库反序列化: 任务
local function unpackage_btask_from_db(player, btask_data)
    for _, task_data in pairs(btask_data or {}) do
        local task = BattlePassTask()
        if not task:deserialize_from_db(task_data) then
            log_err("[unpackage_btask_from_db] player task load faild: player_id=%s,data=%s", player.player_id, serialize(task_data))
        else
            if not task:init(player) then
                log_err("[unpackage_btask_from_db] player task init faild: player_id=%s,data=%s", player.player_id, serialize(task_data))
            else
                if task:get_task_type() == BattlePassTaskType.DAY then
                    player.day_tasks[task:get_task_id()] = task
                elseif task:get_task_type() == BattlePassTaskType.WEEK then
                    player.week_tasks[task:get_task_id()] = task
                elseif task:get_task_type() == BattlePassTaskType.LOOP then
                    player.loop_tasks[task:get_task_id()] = task
                else
                    log_err("[unpackage_btask_from_db] player task init faild: player_id=%s,task_id=%s,task_type=%s",
                        player.player_id, task:get_task_id(), task:get_task_type())
                end
            end
        end
    end
end

-- 从数据库反序列化
local function unpackage_from_db(player, data)
    if data.battlepass then
        local battlepass = data.battlepass
        for _, lvl in pairs(battlepass.explore_rewards or {}) do
            player.explore_rewards[lvl] = true
        end
        for _, lvl in pairs(battlepass.senior_rewards or {}) do
            player.senior_rewards[lvl] = true
        end
        for _, attr in pairs(battlepass.behavior_attrs or {}) do
            if player.behavior_attrs[attr.attr_id] then
                player.behavior_attrs[attr.attr_id] = attr.attr_value
            end
        end
        for _, clue_id in pairs(battlepass.clueboard_rewards or {}) do
            player.clueboard_rewards[clue_id] = true
        end
        player.battlepass_vip = battlepass.battlepass_vip
        player.battlepass_season = battlepass.battlepass_season
        player.battlepass_edition = battlepass.battlepass_edition
        -- 任务系统反序列化
        unpackage_btask_from_db(player, battlepass.battlepass_tasks or {})
    end
end

-- 打包为数据库格式
local function package_for_share(player)
    local senior_rewards = {}
    local explore_rewards = {}
    local clueboard_rewards = {}
    for lvl in pairs(player.explore_rewards) do
        tinsert(explore_rewards, lvl)
    end
    for lvl in pairs(player.senior_rewards) do
        tinsert(senior_rewards, lvl)
    end
    for clue_id, flag in pairs(player.clueboard_rewards) do
        tinsert(clueboard_rewards, clue_id)
    end
    return {
        senior_rewards = senior_rewards,
        explore_rewards = explore_rewards,
        clueboard_rewards = clueboard_rewards,
        battlepass_vip = player.battlepass_vip,
    }
end

-- 打包为数据库格式
local function package_for_db(player)
    local battlepass_tasks = {}
    local behavior_attrs = {}
    for attr_id, attr_value in pairs(player.behavior_attrs) do
        tinsert(behavior_attrs, {attr_id = attr_id, attr_value = attr_value})
    end
    for _, task in pairs(player.day_tasks or {}) do
        tinsert(battlepass_tasks, task:serialize_to_db())
    end
    for _, task in pairs(player.week_tasks or {}) do
        tinsert(battlepass_tasks, task:serialize_to_db())
    end
    for _, task in pairs(player.loop_tasks or {}) do
        tinsert(battlepass_tasks, task:serialize_to_db())
    end
    local data = package_for_share(player)
    data.behavior_attrs = behavior_attrs
    data.battlepass_tasks = battlepass_tasks
    data.battlepass_season = battlepass_mgr:get_season_id()
    return data
end

local function build_default_behavior_attr(player)
    for _, cfg in behavior_attr_db:iterator() do
        if cfg.store then
            player.behavior_attrs[cfg.id] = cfg.default
        end
    end
end

local BattlepassComponent = mixin(
    "get_task",
    "flush_day_task",
    "flush_week_task",
    "flush_loop_task",
    "flush_wtask_state",
    "change_day_task",
    "take_clue_prize",
    "take_btask_prize",
    "take_bpass_prize",
    "get_task_progress",
    "set_behavior_attr",
    "get_behavior_attr",
    "clueboard_received",
    "battlepsaa_received",
    "pack_battlepass_data",
    "gain_battlepass_vip",
    "gain_balltepass_rewards",
    "gain_clueboard_rewards",
    "change_battpepass_season"
)

local prop = property(BattlepassComponent)
prop:accessor("senior_rewards", {})
prop:accessor("behavior_attrs", {})
prop:accessor("explore_rewards", {})
prop:accessor("clueboard_rewards", {})
prop:accessor("battlepass_vip", false)
prop:accessor("battlepass_dirty", false)
prop:accessor("day_tasks", {})              -- 日任务
prop:accessor("week_tasks", {})             -- 周任务
prop:accessor("loop_tasks", {})             -- 循环任务
prop:accessor("battlepass_season", 0)       -- 当前赛季id
prop:accessor("battlepass_edition", 0)      -- 当前每日任务刷新时间

function BattlepassComponent:__init(open_id, player_id, area_id, session)
end

function BattlepassComponent:update(now, day_flush)
    if day_flush then
        self:flush_day_task()
        self:flush_wtask_state()
    end
end

function BattlepassComponent:setup(db_data)
    local player_battlepass = db_data.player_battlepass
    if not player_battlepass then
        return false
    end
    build_default_behavior_attr(self)
    unpackage_from_db(self, player_battlepass)
    self.battlepass_dirty = false
    --赛季更新
    local new_season = battlepass_mgr:get_season_id()
    if self.battlepass_season ~= new_season then
        self:change_battpepass_season(self.battlepass_season, new_season)
    end
    --更新任务
    self:flush_day_task(true)
    return true
end

function BattlepassComponent:unload()
    for _, task in pairs(self.day_tasks) do
        task:uninit()
    end
    for _, task in pairs(self.week_tasks) do
        task:uninit()
    end
    for _, task in pairs(self.loop_tasks) do
        task:uninit()
    end
    return true
end

-- 设置行为属性
function BattlepassComponent:set_behavior_attr(attr_id, attr_var)
    if self.behavior_attrs[attr_id] then
        log_debug("BattlepassComponent:set_behavior_attr->player_id:%s, attr_id:%s, attr_var:%s", self.player_id, attr_id, attr_var)
        self.behavior_attrs[attr_id] = attr_var
        self.battlepass_dirty = true
    end
end

-- 获取行为属性
function BattlepassComponent:get_behavior_attr(attr_id)
    return self.behavior_attrs[attr_id]
end

--获取某一级的奖励
function BattlepassComponent:gain_balltepass_rewards(lvl, senior)
    if senior then
        self.senior_rewards[lvl] = true
    else
        self.explore_rewards[lvl] = true
    end
    self.battlepass_dirty = true
end

--获取battlepass的vip
function BattlepassComponent:gain_battlepass_vip(explore)
    if explore > 0 then
        self:add_explore(explore)
    end
    self.battlepass_vip = true
    event_mgr:notify_trigger("evt_battlepass_vip", self, self.battlepass_vip)
    self.battlepass_dirty = true
end

--battlepass赛季变更
function BattlepassComponent:change_battpepass_season(old_season, new_season)
    if old_season > 0 then
        event_mgr:notify_trigger("evt_battlepass_season_prize", self, old_season)
    end
    --探索点清零
    self:set_explore(0)
    self.battlepass_vip = false
    self.battlepass_season = new_season
    self.senior_rewards = {}
    self.explore_rewards = {}
    self.clueboard_rewards = {}
    self.behavior_attrs = {}
    self:flush_week_task()
    self:flush_loop_task()
    build_default_behavior_attr(self)
    self.battlepass_dirty = true
end

--获取线索板奖励
function BattlepassComponent:gain_clueboard_rewards(clud_id)
    self.clueboard_rewards[clud_id] = true
    self.battlepass_dirty = true
end

--线索板奖励领取标记
function BattlepassComponent:clueboard_received(clue_id)
    return self.clueboard_rewards[clue_id]
end

--奖励领取标记
function BattlepassComponent:battlepsaa_received(level, vip)
    if vip then
        return self.senior_rewards[level]
    end
    return self.explore_rewards[level]
end

-- 数据打包
function BattlepassComponent:package_to_data(data)
    data["battlepass"] = package_for_db(self)
end

-- 数据打包
function BattlepassComponent:pack_battlepass_data()
    return package_for_share(self)
end

function BattlepassComponent:update_data_2db(flush)
    if not self.battlepass_dirty then
        return true
    end
    local ok = player_dao:update_player_battlepass(self.player_id, package_for_db(self), flush)
    if ok then
        self.battlepass_dirty = false
        return true
    end
    return false
end

--刷新日任务
function BattlepassComponent:flush_day_task(no_sync)
    local edition = uedition_utc("day", otime(), FLUSH_TIME)
    if edition ~= self.battlepass_edition then
        -- 卸载旧的每日任务
        local drop_ids = {}
        for _, task in pairs(self.day_tasks or {}) do
            tinsert(drop_ids, task:get_task_id())
            task:uninit()
        end
        -- 记录刷新时间
        self.battlepass_edition = edition
        self.day_tasks = {}
        -- 从任务管理器分配新的每日任务
        local succeed_cnt = 0
        local new_task_datas = {}
        local tasks = battlepass_mgr:gen_day_tasks()
        for task_id, task in pairs(tasks or {}) do
            if task:init(self) then
                self.day_tasks[task_id] = task
                tinsert(new_task_datas, task:serialize_to_progress_data())
                succeed_cnt = succeed_cnt + 1
            else
                log_err("[BattlepassComponent][flush_day_task] player_id=%s, init task_id=%s faild", self.player_id, task_id)
            end
        end
        log_info("[BattlepassComponent][flush_day_task] player_id=%s,succeed=%s", self.player_id, succeed_cnt)
        self.battlepass_dirty = true
        if not no_sync then
            event_mgr:notify_trigger("evt_btask_update", self, drop_ids, new_task_datas)
        end
    end
end

--刷新周任务状态
function BattlepassComponent:flush_wtask_state()
    -- 需要刷新任务状态
    local week_id = battlepass_mgr:get_current_season_week_id()
    for _, task in pairs(self.week_tasks or {}) do
        if task:get_week_id() <= week_id then
            if BattlePassTaskState.WAITING == task:get_state() then
                task:reset_state(BattlePassTaskState.PROGRESSING)
                self.battlepass_dirty = true
            end
        end
    end
end

-- 刷新周任务
function BattlepassComponent:flush_week_task()
    -- 卸载旧的每周任务
    local drop_ids = {}
    for _, task in pairs(self.week_tasks or {}) do
        tinsert(drop_ids, task:get_task_id())
        task:uninit()
    end
    -- 装载新任务
    self.week_tasks = {}
    local succeed_cnt = 0
    local tasks = battlepass_mgr:gen_week_tasks()
    for task_id, task in pairs(tasks or {}) do
        if task:init(self) then
            self.week_tasks[task_id] = task
            succeed_cnt = succeed_cnt + 1
        else
            log_err("[BattlepassComponent][flush_week_task] player_id=%s, init task_id=%s faild", self.player_id, task_id)
        end
    end
    log_info("[BattlepassComponent][flush_week_task] player_id=%s, succeed=%s", self.player_id, succeed_cnt)
    self:flush_wtask_state()
    self.battlepass_dirty = true
end

--刷新循环任务
function BattlepassComponent:flush_loop_task()
    self.loop_tasks = {}
    local succeed_cnt = 0
    local tasks = battlepass_mgr:gen_loop_tasks()
    for task_id, task in pairs(tasks or {}) do
        if task:init(self) then
            self.loop_tasks[task_id] = task
            succeed_cnt = succeed_cnt + 1
        else
            log_err("[BattlepassComponent][flush_loop_task] player_id=%s, init task_id=%s faild", self.player_id, task_id)
        end
    end
    log_info("[BattlepassComponent][flush_loop_task] player_id=%s, succeed=%s", self.player_id, succeed_cnt)
    self.battlepass_dirty = true
end

-- 获取任务对象
function BattlepassComponent:get_task(task_id)
    local task = self.day_tasks[task_id]
    if not task then
        task = self.week_tasks[task_id]
    end
    if not task then
        task = self.loop_tasks[task_id]
    end
    return task
end

-- 打包全部任务的进度数据
function BattlepassComponent:get_task_progress()
    local btask_progresses = {}
    for _, task in pairs(self.day_tasks or {}) do
        tinsert(btask_progresses, task:serialize_to_progress_data())
    end
    local week_id   = battlepass_mgr:get_current_season_week_id()
    for _, task in pairs(self.week_tasks or {}) do
        if task:get_week_id() <= week_id + 1 then
            --只多发送一周任务
            tinsert(btask_progresses, task:serialize_to_progress_data())
        end
    end
    for _, task in pairs(self.loop_tasks or {}) do
        tinsert(btask_progresses, task:serialize_to_progress_data())
    end
    return btask_progresses
end

-- 获取任务奖励
function BattlepassComponent:take_btask_prize(task_id)
    local btask = self:get_task(task_id)
    local kv_pairs = btask:get_prize()
    local ok, obtain_data = self:take_bpass_prize(kv_pairs, BattlePassPrizeMode.NONE, "")
    if not ok then
        log_err("[BattlepassComponent][take_btask_prize] take bpass task prize faild: player_id=%s, task_id=%s, prize=%s",
            self:get_player_id(), task_id, serialize(kv_pairs))
    end
    -- 标记已领奖
    btask:reset_state(BattlePassTaskState.PRIZE_TAKEN)
    return ok, obtain_data
end

-- 更换任务
function BattlepassComponent:change_day_task(task_id)
    local btask = self:get_task(task_id)
    -- 只有日任务能更换
    if not btask or BattlePassTaskType.DAY ~= btask:get_task_type() then
        return
    end
    local new_task = battlepass_mgr:gen_day_task(self)
    if not new_task then
        return
    end
    local drop_ids = {}
    tinsert(drop_ids, task_id)
    if new_task:init(self) then
        log_debug("[BattlepassComponent][change_day_task] old_task_id=%s,new_task_id=%s", task_id, new_task:get_task_id())
        -- 卸载并删除旧任务
        self.day_tasks[task_id]:uninit()
        self.day_tasks[task_id] = nil
        -- 装载新任务
        self.day_tasks[new_task:get_task_id()] = new_task
        self.battlepass_dirty = true
        event_mgr:notify_trigger("evt_btask_update", self, drop_ids, {new_task:serialize_to_progress_data()})
    end
end

-- 发放battlepass奖励
function BattlepassComponent:take_bpass_prize(item_kv_pairs, module_id, context)
    log_info("[BattlepassComponent][take_bpass_prize] player_id, module_id=%s,context=%s,items=%s",
        self.player_id, serialize(module_id), serialize(context), serialize(item_kv_pairs))
    local add_cmds = {}
    for _, kv_pair in pairs(item_kv_pairs or {}) do
        local add_item_cmd = {item_id = kv_pair.item_id, num = kv_pair.item_amount, add_reason = ResAddType.BATTLE_PASS, expire_time = 0}
        tinsert(add_cmds, add_item_cmd)
    end

    local ok, result_list =  self:add_items_no_sync(add_cmds)
    if not ok then
        log_err("[BattlepassComponent][take_bpass_prize] player_id=%s, add_items=[%s] faild, ok=%s", self.player_id, serialize(add_cmds), ok)
        return false
    end

    self:change_items_data_sync_client(result_list)
    local obtain_data = build_obtain_data(result_list)
    event_mgr:notify_trigger("evt_battle_pass_take_prize", self, obtain_data, module_id, context)

    return true, obtain_data
end

-- 获取线索板奖励
function BattlepassComponent:take_clue_prize(clue_id)
    local clue_cfg = battlepass_mgr:get_clueboard_config(clue_id)
    local kv_pairs = clue_cfg.prize1
    local ok, obtain_data = self:take_bpass_prize(kv_pairs, BattlePassPrizeMode.CLUE, "")
    if not ok then
        log_err("[BattlepassComponent][take_clue_prize] take bpass task prize faild: player_id=%s, clue_id=%s, prize=%s",
            self:get_player_id(), clue_id, serialize(kv_pairs))
        return false
    end
    return true, obtain_data
end

-- export
return BattlepassComponent