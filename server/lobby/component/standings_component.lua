-- standings_component.lua
local tinsert       = table.insert
local tremove       = table.remove

local player_dao    = quanta.get("player_dao")

local StandingsConst= enum("StandingsConst")

local StandingsComponent = mixin(
    "add_player_standings"
)

local prop = property(StandingsComponent)
prop:reader("standings_dirty", false)
prop:reader("standings", {})

function StandingsComponent:__init(open_id, player_id, area_id, session)
end

function StandingsComponent:setup(db_data)
    local player_standings = db_data.player_standings
    if not player_standings then
        return false
    end
    self.standings = player_standings.standings or {}
    return true
end

-- 打包为数据库格式
local function package_for_db(self)
    return self.standings
end

-- 数据打包
function StandingsComponent:package_to_data(data)
    data["standings"] = package_for_db(self)
end

function StandingsComponent:update_data_2db(flush)
    if not self.standings_dirty then
        return true
    end
    local ok = player_dao:update_player_standings(self.player_id, package_for_db(self), flush)
    if ok then
        self.standings_dirty = false
        return true
    end
    return false
end

function StandingsComponent:add_player_standings(one_standings)
    tinsert(self.standings, one_standings)
    self.standings_dirty = true
    if #self.standings > StandingsConst.MAX_RECORD then
        tremove(self.standings, 1)
    end
end

-- export
return StandingsComponent