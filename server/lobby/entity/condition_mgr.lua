--conditon_mgr.lua
-- 条件管理器

local CondType = enum("CondType")

local CondStarsWinCnt = import("lobby/entity/condition/cond_stars_win_cnt.lua")
local CondPlayerStars = import("lobby/entity/condition/cond_player_stars.lua")

local condition_class = {
    [CondType.CondStarsWinCnt]           = CondStarsWinCnt,
    [CondType.CondPlayerStars]           = CondPlayerStars,
  }

local ConditionMgr  = singleton()
function ConditionMgr:__init()

end

function ConditionMgr:create_condition(cond_type, ...)
    if condition_class[cond_type] then
        return condition_class[cond_type](...)
    end
end

quanta.condition_mgr = ConditionMgr()

return ConditionMgr
