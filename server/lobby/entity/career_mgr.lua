--career_mgr.lua

local log_info      = logger.info
local log_debug     = logger.debug
local log_warn      = logger.warn
local log_err       = logger.err
local serialize     = logger.serialize

local timer_mgr     = quanta.get("timer_mgr")
local event_mgr     = quanta.get("event_mgr")
local player_mgr    = quanta.get("player_mgr")

local PeriodTime    = enum("PeriodTime")
local CareerAttrID  = enum("CareerAttrID")
local KernCode      = enum("KernCode")
local ItemType      = enum("ItemType")
local PlayerAttrID  = enum("PlayerAttrID")

local PLAYER_ATT_2CAREER_ATT = {
    [PlayerAttrID.NICK]             = CareerAttrID.NAME,   -- 昵称
    [PlayerAttrID.SEX]              = CareerAttrID.SEX,    -- 性别
    [PlayerAttrID.EXP]              = CareerAttrID.EXP,    -- 经验
    [PlayerAttrID.LEVEL]            = CareerAttrID.LEVEL,  -- 等级
    [PlayerAttrID.ICON]             = CareerAttrID.ICON,   -- 头像
    [PlayerAttrID.VCARD_AVATAR_ID]  = CareerAttrID.VCARD_AVATAR_ID, -- 名片外形ID
    [PlayerAttrID.VCARD_FRAME_ID]   = CareerAttrID.VCARD_FRAME_ID,  -- 名片底框ID
    [PlayerAttrID.VCARD_BORDER_ID]  = CareerAttrID.VCARD_BORDER_ID, -- 名片边框ID
    [PlayerAttrID.VCARD_ACHIE_ID]   = CareerAttrID.VCARD_ACHIE_ID,  -- 名片成就ID
}

local CareerMgr = singleton()
function CareerMgr:__init()
    self.image_count     = 0
    self.id2image        = {}

    -- 定时器，部分代码必须在协程中执行，放到定时器里边
    timer_mgr:loop(PeriodTime.SECOND_MS, function()
        self:on_timer()
    end)

    --注册转发消息
    event_mgr:add_trigger(self, "on_load_player_begin")
    event_mgr:add_trigger(self, "on_player_logout")
    event_mgr:add_trigger(self, "evt_dirty_career")
    event_mgr:add_trigger(self, "evt_player_attribute_change")
    event_mgr:add_trigger(self, "evt_create_new_player")
    event_mgr:add_trigger(self, "evt_role_employ_update")
    event_mgr:add_trigger(self, "evt_player_add_role")
    event_mgr:add_trigger(self, "evt_player_add_role_skin")
    event_mgr:add_trigger(self, "evt_create_item")
end

-- 定时器
function CareerMgr:on_timer()
    for player_id, image in pairs(self.id2image) do
        image:save()
    end
end

-- 创建对象
-- is_new: true 新增数据，无需从数据库加载
-- delay_load: true 延迟从数据库加载
function CareerMgr:build_image(player_id, is_new, delay_load)
    log_info("[CareerMgr][build_image] begin build image: player_id=%s, is_new=%s, delay_load=%s", player_id, is_new, delay_load)
    -- 外部需要自己判断是否已经存在
    if self:get_image(player_id) then
        log_warn("[CareerMgr][build_image] can not build exists image: player_id=%s, is_new=%s, delay_load=%s", player_id, is_new, delay_load)
        return
    end

    local CareerImage = import("lobby/entity/career_image.lua")
    local image = CareerImage(player_id)
    -- to do mul build
    -- 有组件初始化失败直接返回错误
    if not image:setup(is_new, delay_load) then
        self.id2image[player_id] = nil
        return
    end
    self.id2image[player_id] = image
    log_info("[CareerMgr][build_image] build image succeed: player_id=%s", player_id)
    return image
end

--删除玩家
function CareerMgr:destroy_image(player_id)
    log_info("[CareerMgr][destroy_image] begin player_id=%s", player_id)
    if not self:get_image(player_id) then
        log_err("[CareerMgr][destroy_image] image not exists: player_id=%s", player_id)
        return
    end

    -- 脏对象不能直接销毁
    local image = self.id2image[player_id]
    local ditry = image:get_career_dirty()
    if ditry then
        local ok = image:save()
        log_info("[CareerMgr][destroy_image] save data player_id=%s, ok=%s", player_id, ok)
        if not ok then
            return
        end
    end

    --注销
    self.image_count = self.image_count - 1
    self.id2image[player_id] = nil
    log_info("[CareerMgr][destroy_image] end player_id=%s", player_id)
end

-- 新增玩家
function CareerMgr:evt_create_new_player(player, player_id)
    local image = self:build_image(player_id, true, false)
    if not image then
        log_err("[CareerMgr][evt_create_new_player] image = nil")
        return
    end
    image:init_new_image()
end

--登录事件
function CareerMgr:on_load_player_begin(player_id, player)
    if self:get_image(player_id) then
        log_info("[CareerMgr][on_load_player_begin] image is created")
        return
    end

    self:build_image(player_id, false, true)
end

--登出事件
function CareerMgr:on_player_logout(player_id, player)
    local image = self:get_image(player_id)
    if not image then
        return
    end

    self:destroy_image(player_id)
end

-- 生涯数据变化事件
function CareerMgr:evt_dirty_career(player, id, types, value)
    local image = self:get_image(player:get_player_id())
    if not image then
        return
    end

    image:set_career_attr(id, types, value)
end

-- 人物数据变化事件
function CareerMgr:evt_player_attribute_change(player, attr_id, old_value, new_value, reason)
    local image = self:get_image(player:get_player_id())
    local car_att_id = PLAYER_ATT_2CAREER_ATT[attr_id]
    if image and car_att_id then
        image:set_career_his_attr(car_att_id, new_value)
    end
end

-- 英雄使用计数变化事件
function CareerMgr:evt_role_employ_update(player_id, game_mode, role_id, cnt)
    local image = self:get_image(player_id)
    if not image then
        return
    end

    local old_cnt = image:get_role_employ(game_mode, role_id)
    image:set_role_employ(game_mode, role_id, old_cnt + cnt)
end

-- 增加英雄事件
function CareerMgr:evt_player_add_role(player, role, reason)
    local image = self:get_image(player:get_player_id())
    if not image then
        return
    end

    image:set_career_his_attr(CareerAttrID.ROLE_CNT, player:get_roles_cnt())
end

-- 增加皮肤事件
function CareerMgr:evt_player_add_role_skin(player)
    local image = self:get_image(player:get_player_id())
    if not image then
        return
    end

    image:set_career_his_attr(CareerAttrID.SKIN_CNT, player:get_role_skins_cnt())
end

-- 增加物品事件
function CareerMgr:evt_create_item(player, item)
    if item:get_item_type() ~= ItemType.Weapon or not item:is_gun() then
        return
    end

    local image = self:get_image(player:get_player_id())
    if not image then
        return
    end

    image:set_career_his_attr(CareerAttrID.WEAPON_CNT, player:get_guns_count())
end

--查找玩家
function CareerMgr:get_image(player_id)
    return self.id2image[player_id]
end

-- 获取生涯数据
function CareerMgr:get_image_packege(player_id)
    local image = self:get_image(player_id)
    if not image then
        image = self:build_image(player_id, false, false)
    end

    if not image then
        log_err("[CareerMgr][get_image_packege] image build failed: player_id=%s", player_id)
        return KernCode.LOGIC_FAILED
    end

    -- 检查记载数据
    if not image:check_load_db() then
        return KernCode.MONGO_FAILED
    end

    local res = image:package()
    local player = player_mgr:get_player(player_id)
    -- 不在线玩家image需要卸载掉
    if not player then
        self:destroy_image(player_id)
    end

    log_debug("[CareerMgr][get_image_packege] res=%s", serialize(res))
    return KernCode.SUCCESS, res
end

-- export
quanta.career_mgr = CareerMgr()
return CareerMgr