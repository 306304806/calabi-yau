--career_image.lua
local tinsert                   = table.insert
local guid_index                = guid.index
local log_warn                  = logger.warn
local log_err                   = logger.err

local player_mgr                = quanta.get("player_mgr")
local config_mgr                = quanta.get("config_mgr")
local servlet_util              = quanta.get("servlet_util")
local career_image_dao          = quanta.get("career_image_dao")

local PlayerAttrID              = enum("PlayerAttrID")
local GameMode                  = enum("GameMode")
local DBLoading                 = enum("DBLoading")
local CareerAttrTypes           = enum("CareerAttrTypes")
local CareerAttrID              = enum("CareerAttrID")

local role_db                   = config_mgr:get_table("role")
local roleskin_db               = config_mgr:get_table("roleskin")
local career_attr_db            = config_mgr:get_table("careerattribute")

--创建角色数据
local CareerImage = class()

local prop = property(CareerImage)
prop:accessor("area_id", 0)
prop:accessor("player_id", 0)
prop:accessor("career_loading", DBLoading.INIT)
prop:accessor("career_dirty", false)
prop:accessor("career_map", {})             -- 生涯的属性列表
prop:accessor("career_cache_map", {})       -- 生涯的缓存属性列表
prop:accessor("role_employs", {})           -- 角色使用列表
prop:accessor("role_cache_employs", {})     -- 角色使用缓存列表

function CareerImage:__init(player_id)
    self.player_id = player_id
    self.area_id = guid_index(player_id)
end

-- is_new: 新增数据，无需从数据库加载
-- delay_load: true 延迟从数据库加载
function CareerImage:setup(is_new, delay_load)
    -- 填充默认属性
    for _, cfg in career_attr_db:iterator() do
        if cfg.is_his then
            self.career_map[CareerAttrTypes.HISTORY] = self.career_map[CareerAttrTypes.HISTORY] or {}
            self.career_map[CareerAttrTypes.HISTORY][cfg.id] = cfg.default
        end
        if cfg.is_cur then
            self.career_map[CareerAttrTypes.CURRENT] = self.career_map[CareerAttrTypes.CURRENT] or {}
            self.career_map[CareerAttrTypes.CURRENT][cfg.id] = cfg.default
        end
    end
    if is_new or delay_load then
        return true
    end
    return self:load_db_data()
end

-- 加载数据库
function CareerImage:load_db_data()
    -- 尝试加载
    local ok, db_data = career_image_dao:load_career_image(self.player_id)
    if not ok then
        log_err("[CareerImage][load_db_data] is not ok")
        return false
    end
    self.career_loading = DBLoading.SUCCESS
    -- 成功加载
    if db_data then
        self:merge_cache_data(db_data)
    else
        self:init_new_image()
    end
    return true
end

-- 合并缓存数据
function  CareerImage:merge_cache_data(db_data)
    self.role_employs = db_data.role_employs
    -- 合并使用计数表
    if #self.role_cache_employs > 0 then
        for game_mode, employs in pairs(self.role_cache_employs) do
            local tmp_employs = self.role_employs[game_mode] or {}
            for id, cnt in pairs(employs) do
                tmp_employs[id] = tmp_employs[id] or 0
                tmp_employs[id] = tmp_employs[id] + cnt
            end
            self.role_employs[game_mode] = tmp_employs
        end
        self.role_cache_employs = {}
    end

    for types, att_map in pairs(db_data.attrs) do
        local tmp_att_map = self.career_map[types]
        for k, v in pairs(att_map) do
            tmp_att_map[k] = v
        end
    end
    -- 合并属性表
    if #self.career_cache_map > 0 then
        for types, att_map in pairs(self.career_cache_map) do
            local tmp_att_map = self.career_map[types]
            for k, v in pairs(att_map) do
                tmp_att_map[k] = v
            end
        end
        self.career_cache_map = {}
    end
end

-- 检查加载db
function CareerImage:check_load_db()
    if self.career_loading ~= DBLoading.SUCCESS then
        return self:load_db_data()
    end
    return true
end

-- 数据库操作函数
function CareerImage:save()
    self:check_load_db()

    if DBLoading.SUCCESS ~= self.career_loading then
        return false
    end

    if not self.career_dirty then
        return true
    end

    local db_data = self.career_map
    db_data.role_employs = self.role_employs
    local ok = career_image_dao:update_career_image(self.player_id, db_data)
    if ok then
        self.career_dirty = false
        return true
    end

    return false
end

-- 初始化一个镜像信息
function CareerImage:init_new_image()
    local player = player_mgr:get_player(self.player_id)
    local flogin_time = player:get_career_his_attr(CareerAttrID.FLOGIN_TIME)
    self:set_career_his_attr(CareerAttrID.FLOGIN_TIME, flogin_time)
    self:set_career_his_attr(CareerAttrID.ID, self.player_id)
    self:set_career_his_attr(CareerAttrID.NAME, player:get_nick())
    self:set_career_his_attr(CareerAttrID.SEX, player:get_sex())
    self:set_career_his_attr(CareerAttrID.EXP, player:get_exp())
    self:set_career_his_attr(CareerAttrID.LEVEL, player:get_level())
    self:set_career_his_attr(CareerAttrID.ICON, player:get_icon())
    self:set_career_his_attr(CareerAttrID.VCARD_AVATAR_ID, player:get_attribute(PlayerAttrID.VCARD_AVATAR_ID))
    self:set_career_his_attr(CareerAttrID.VCARD_FRAME_ID, player:get_attribute(PlayerAttrID.VCARD_BORDER_ID))
    self:set_career_his_attr(CareerAttrID.VCARD_BORDER_ID, player:get_attribute(PlayerAttrID.VCARD_FRAME_ID))
    self:set_career_his_attr(CareerAttrID.VCARD_ACHIE_ID, player:get_attribute(PlayerAttrID.VCARD_ACHIE_ID))
    self:set_career_his_attr(CareerAttrID.ROLE_CNT, player:get_roles_cnt())
    self:set_career_his_attr(CareerAttrID.SKIN_CNT, player:get_role_skins_cnt())
    self:set_career_his_attr(CareerAttrID.WEAPON_CNT, player:get_guns_count())
end

-- 增加历史属性值
function CareerImage:add_career_his_attr(id, add_value)
    local val = self:get_career_his_attr(id)
    val = val + add_value
    self:set_career_his_attr(id, val)
end

-- 设置属性
function CareerImage:set_career_attr(id, types, value)
    if self.career_loading ~= DBLoading.SUCCESS then
        self.career_cache_map[types] = self.career_cache_map[types] or {}
        self.career_cache_map[types][id] = value
        self.career_dirty = true
        return
    end

    -- 不相等才会触发后续逻辑
    local tmp_map = self.career_map[types]
    if value ~= tmp_map[id] then
        tmp_map[id] = value
        self.career_dirty = true
    end
end

-- 设置历史属性
-- @param id {CareerAttrID}: 属性id
-- @param value {any}: 属性值
function CareerImage:set_career_his_attr(id, value)
    self:set_career_attr(id, CareerAttrTypes.HISTORY, value)
end

 -- 设置当前属性
 -- @param id {CareerAttrID}: 属性id
 -- @param value {any}: 属性值
 function CareerImage:set_career_cur_attr(id, value)
     self:set_career_attr(id, CareerAttrTypes.CURRENT, value)
 end

-- 获取属性
function CareerImage:get_career_attr(id, types)
    if self.career_loading ~= DBLoading.SUCCESS then
        log_err("[CareerImage][get_career_attr] no load_db")
        return
    end

    return self.career_map[types][id]
end

-- 获取历史属性
-- @param id {CareerAttrID}: 属性id
function CareerImage:get_career_his_attr(id)
    return self:get_career_attr(id, CareerAttrTypes.HISTORY)
end

-- 获取当前属性
-- @param id {CareerAttrID}: 属性id
function CareerImage:get_career_cur_attr(id)
    return self:get_career_attr(id, CareerAttrTypes.CURRENT)
end

-- 设置英雄使用计数
function CareerImage:set_role_employ(game_mode, role_id, cnt)
    if cnt == 0 then
        log_warn("[CareerImage][set_role_employ] game_mode=%s, role_id=%s, cnt=%s", game_mode, role_id, cnt)
        return
    end

    if self.career_loading ~= DBLoading.SUCCESS then
        self.role_cache_employs[game_mode] = self.role_cache_employs[game_mode] or {}
        self.role_cache_employs[game_mode][role_id] = cnt
        self.career_dirty = true
        return
    end

    local employs = self.role_employs[game_mode] or {}
    if employs[role_id] ~= cnt then
        employs[role_id] = cnt
        self.role_employs[game_mode] = employs
        self.career_dirty = true
    end
end

-- 获取英雄使用计数
function CareerImage:get_role_employ(game_mode, role_id)
    local employs = self.role_employs[game_mode] or {}
    return employs[role_id] or 0
end

-- 获取所有英雄使用计数
function CareerImage:pack_role_employs()
    -- 角色使用数据
    local role_employs = {}
    for game_mode, employs in pairs(self.role_employs) do
        if game_mode ~= GameMode.ROOM then
            for id, value in pairs(employs) do
                tinsert(role_employs, {game_mode = game_mode, role_id = id, count = value})
            end
        end
    end

    return role_employs
end

local function mdivied(a, b)
    if b == 0 then
        return 0
    end
    return a / b
end

-- package
function CareerImage:package()
    local player = {
        player_id   = self:get_career_his_attr(CareerAttrID.ID),
        nick        = self:get_career_his_attr(CareerAttrID.NAME),
        icon        = self:get_career_his_attr(CareerAttrID.ICON),
        sex         = self:get_career_his_attr(CareerAttrID.SEX),
        exp         = self:get_career_his_attr(CareerAttrID.EXP),
        level       = self:get_career_his_attr(CareerAttrID.LEVEL),
        avatar_id   = self:get_career_his_attr(CareerAttrID.VCARD_AVATAR_ID),
        border_id   = self:get_career_his_attr(CareerAttrID.VCARD_BORDER_ID),
        frame_id    = self:get_career_his_attr(CareerAttrID.VCARD_FRAME_ID),
        achie_id    = self:get_career_his_attr(CareerAttrID.VCARD_ACHIE_ID),
        laud        = self:get_career_his_attr(CareerAttrID.LAUD),
        flogin_time = self:get_career_his_attr(CareerAttrID.FLOGIN_TIME)
    }

    local bomb_round = self:get_career_his_attr(CareerAttrID.BOMB_ROUND)
    local bomb_hit_count = self:get_career_his_attr(CareerAttrID.BOMB_HIT_COUNT)
    local bomb_battle = {
        round            = bomb_round,
        game_mode        = GameMode.BOMB,
        win_rate         = mdivied(self:get_career_his_attr(CareerAttrID.BOMB_WIN), bomb_round),
        mvp_rate         = mdivied(self:get_career_his_attr(CareerAttrID.BOMB_MVP), bomb_round),
        ave_damage       = mdivied(self:get_career_his_attr(CareerAttrID.BOMB_DAMAGE), bomb_round),
        ave_kill         = mdivied(self:get_career_his_attr(CareerAttrID.BOMB_KILL), bomb_round),
        head_burst_rate  = mdivied(self:get_career_his_attr(CareerAttrID.BOMB_HIT_HEAD), bomb_hit_count),
    }

    local team_round = self:get_career_his_attr(CareerAttrID.TEAM_ROUND)
    local team_hit_count = self:get_career_his_attr(CareerAttrID.TEAM_HIT_COUNT) or 0
    local team_battle = {
        round            = team_round,
        game_mode        = GameMode.TEAM,
        win_rate         = mdivied(self:get_career_his_attr(CareerAttrID.TEAM_WIN), team_round),
        mvp_rate         = mdivied(self:get_career_his_attr(CareerAttrID.TEAM_MVP), team_round),
        ave_damage       = mdivied(self:get_career_his_attr(CareerAttrID.TEAM_DAMAGE), team_round),
        ave_kill         = mdivied(self:get_career_his_attr(CareerAttrID.TEAM_KILL), team_round),
        head_burst_rate  = mdivied(self:get_career_his_attr(CareerAttrID.TEAM_HIT_HEAD), team_hit_count),
    }
    local rank = {
        top_seasons    = self:get_career_his_attr(CareerAttrID.TOP_SEASONS),
        top_seg        = self:get_career_his_attr(CareerAttrID.TOP_SEG),
        cur_season     = self:get_career_cur_attr(CareerAttrID.CUR_SEASON),
        cur_top_seg    = self:get_career_cur_attr(CareerAttrID.TOP_SEG),
        cur_seg        = self:get_career_cur_attr(CareerAttrID.CUR_SEG),
    }
    local collect = {
        role_own    = self:get_career_his_attr(CareerAttrID.ROLE_CNT),
        role_max    = role_db:get_count(),
        skin_own    = self:get_career_his_attr(CareerAttrID.SKIN_CNT),
        skin_max    = roleskin_db:get_count(),
        weapon_own  = self:get_career_his_attr(CareerAttrID.WEAPON_CNT),
        weapon_max  = servlet_util:get_guns_total_cnt(),
    }

    local battle_infos = {}
    tinsert(battle_infos, bomb_battle)
    tinsert(battle_infos, team_battle)
    local role_employs = self:pack_role_employs()

    local career = {
        player          = player,
        battle_infos    = battle_infos,
        rank            = rank,
        collect         = collect,
        role_employs    = role_employs,
    }

    return career
end

return CareerImage
