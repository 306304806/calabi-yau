-- buff.lua
-- buff的实体类
local tinsert       = table.insert

local TriggerType   = enum("TriggerType")
local KernCode      = enum("KernCode")
local SUCCESS       = KernCode.SUCCESS

local effect_mgr    = quanta.get("effect_mgr")

local Buff = class()
local prop = property(Buff)
prop:accessor("id", 0)                      -- id
prop:accessor("buff_id", 0)                  -- buffid(配置)
prop:accessor("layer", 1)                   -- 到期时间
prop:accessor("expire", 0)                  -- 到期时间
prop:accessor("mutex_group", 0)             -- 互斥组
prop:accessor("mutex_priority", 0)          -- 互斥优先级
prop:accessor("share_group", 0)             -- 共享组
prop:accessor("share_priority", 0)          -- 共享优先级
prop:accessor("overlap_group", 0)           -- 叠加组
prop:accessor("overlap_type", 0)            -- 叠加类型
prop:accessor("overlap_layer", 1)           -- 叠加层级
prop:accessor("max_layer", 1)               -- 最大叠加层级
prop:accessor("effects", {})                -- 效果配置列表
prop:accessor("period_effects", {})         -- 周期效果列表
prop:accessor("store", false)               -- 是否存库
prop:accessor("running", false)             -- 是否运行
prop:accessor("effect_objs", {})            -- 效果对象列表

function Buff:__init(id, conf, data)
    self.id             = id
    self.buff_id        = conf.buff_id
    self.mutex_group    = conf.mutex_group
    self.mutex_priority = conf.mutex_priority
    self.share_group    = conf.share_group
    self.share_priority = conf.share_priority
    self.overlap_group  = conf.overlap_group
    self.overlap_type   = conf.overlap_type
    self.overlap_layer  = conf.overlap_layer
    self.effects        = conf.effects
    self.store          = conf.store
    self.time           = conf.time
    if data then
        self.expire         = data.expire
        self.running        = data.running
        self.layer          = data.layer
    end
end

function Buff:pack2db()
    return {
        id          = self.id,
        buff_id     = self.buff_id,
        layer       = self.layer,
        expire      = self.expire,
        running     = self.running,
    }
end

function Buff:pack2client()
    return {
        id          = self.id,
        buff_id     = self.buff_id,
        expire      = self.expire,
        running     = self.running,
    }
end

--开始
function Buff:start(player)
    if self.expire == 0 then
        self.expire = self.time + quanta.now
    end
    self:trigger(TriggerType.START, player)
end

--激活
function Buff:active(player)
    --记录周期buff
    for _, effect in pairs(self.effects) do
        if effect.trigger_type == TriggerType.PERIOD then
            tinsert(self.period_effects, {
                period = effect.period,
                effect_id = effect.effect_id,
                trigger_time = quanta.now + effect.period,
            })
        end
    end
    --触发效果
    self:trigger(TriggerType.ACTIVE, player)
    self.running = true
end

--失活
function Buff:disactive(player)
    for _, effect_obj in pairs(self.effect_objs) do
        local context = {player = player}
        effect_obj:break_run(context)
    end
end

--结束
function Buff:stop(player)
    self:disactive(player)
    --self:trigger(TriggerType.STOP, player)
    self.running = false
end

--触发
function Buff:trigger(trigger_type, player)
    for _, effect in pairs(self.effects) do
        if effect.trigger_type == trigger_type then
            local effect_id = effect.effect_id
            local effect_obj = effect_mgr:create_effect(effect_id)
            local context = {player = player}
            if effect_obj:test(context) then
                effect_obj:run(context)
            end
            if effect_obj:is_continuous() then
                self.effect_objs[effect_id] = effect_obj
            end
        end
    end
end

--是否过期
function Buff:is_expire(now)
    return now > self.expire
end

--叠加时间
function Buff:overlap_time(conf)
    self.expire = self.expire + conf.time
end


--结束
function Buff:run(now, player)
    if self.running then
        --检查周期效果
        for _, effect in pairs(self.period_effects) do
            if now >= effect.trigger_time then
                effect.trigger_time = effect.trigger_time + effect.period
                local effect_id = effect.effect_id
                local effect_obj = quanta.get("effect_mgr"):create_effect(effect_id)
                local context = {player = player}
                if effect_obj:test(context) == SUCCESS then
                    effect_obj:run(context)
                end
                if effect_obj:is_continuous() then
                    self.effect_objs[effect_id] = effect_obj
                end
            end
        end
    end
    return self:is_expire(now)
end

return Buff
