-- role.lua
-- role的实体类

local tinsert  = table.insert
local tindexof = table_ext.indexof

local Role = class()
local prop = property(Role)
prop:accessor("role_id", 0)                -- 角色id(配置)
prop:accessor("loved", false)              -- 是否最喜爱的
prop:accessor("skin_id", 0)                -- 当前装载的皮肤
prop:accessor("expire_time", 0)            -- 到期时间 [0表示永久]
prop:accessor("obtain_time", 0)            -- 获得时间
prop:accessor("exp", 0)                    -- 经验(熟练度)
prop:accessor("owned", true)               -- 是否实际拥有
prop:accessor("actions", {})               -- 当前已解锁动作
prop:accessor("voices", {})                -- 当前已经解锁的声音

function Role:__init()
end

-- 打包到db role_info
function Role:serialize_to_db()
    local info = {
        role_id     = self.role_id,
        skin_id     = self.skin_id,
        owned       = self.owned,
        loved       = self.loved,
        exp         = self.exp,
        actions     = self.actions,
        voices      = self.voices,
        expire_time = self.expire_time or 0,
        obtain_time = self.obtain_time or 0,
    }

    return info
end

function Role:serialize_from_db(data)
    if not data or not data.role_id then
        return false
    end

    self.role_id     = data.role_id
    self.skin_id     = data.skin_id or 0
    self.owned       = data.owned or false
    self.loved       = data.loved or false
    self.exp         = data.exp or 0
    self.actions     = data.actions or {}
    self.voices      = data.voices or {}
    self.expire_time = data.expire_time or 0
    self.obtain_time = data.obtain_time or 0

    return true
end

-- 打包成role_info
function Role:package_role_info()
    local info = {
        role_id     = self.role_id,
        skin_id     = self.skin_id,
        loved       = self.loved,
        exp         = self.exp,
        expire_time = self.expire_time or 0,
        obtain_time = self.obtain_time or 0,
        owned       = self.owned
    }

    return info
end

-- 获取当前已解锁动作
function Role:get_action(action_id)
    local index = tindexof(self.actions, action_id)
    if not index then
        return nil
    end

    return self.actions[index]
end

-- 添加动作
function Role:add_action(action_id)
    tinsert(self.actions, action_id)
end

-- 获取制定的已解锁声音
function Role:get_voice(voice_id)
    local index = tindexof(self.voices, voice_id)
    if not index then
        return nil
    end

    return self.voices[index]
end

-- 添加声音
function Role:add_voice(voice_id)
    tinsert(self.voices, voice_id)
end

-- 添加经验
function Role:add_exp(exp)
    self.exp = self.exp + exp
end

-- 获取经验
function Role:get_exp()
    return self.exp
end

return Role
