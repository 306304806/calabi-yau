-- Battlepass_task

local tinsert   = table.insert
local otime     = os.time
local log_debug = logger.debug
local log_err   = logger.err
local log_warn  = logger.warn
--local log_info  = logger.info
local serialize = logger.serialize

local event_mgr      = quanta.get("event_mgr")
local config_mgr     = quanta.get("config_mgr")

local bpass_dtask_db= config_mgr:get_table("battlepassdaytask")
local bpass_wtask_db= config_mgr:get_table("battlepassweektask")
local bpass_ltask_db= config_mgr:get_table("battlepasslooptask")

local BattlePassTaskType    = enum("BattlePassTaskType")
local BattlePassTaskState   = enum("BattlePassTaskState")
local BehaviorOperationType = enum("BehaviorOperationType")
local BehaviorCompareType   = enum("BehaviorCompareType")

-- 账号实体类
local BattlePassTask = class()
local prop = property(BattlePassTask)
prop:accessor("task_type", 0)
prop:accessor("task_id", 0)
prop:accessor("behavior_map", {})                   -- 进度  map<behavior_id,count>
prop:accessor("state", BattlePassTaskState.WAITING) -- 状态
prop:accessor("obtain_time", 0)                     -- 任务的领取时间
prop:accessor("inited", false)                      -- 是否完成了初始化
prop:accessor("bevent_listening", false)            -- 是否正在监听behavior事件
prop:accessor("host_player", nil)                   -- 宿主玩家ID
prop:accessor("cfg", nil)                           -- 原始配置

-- 从数据库反序列化
function BattlePassTask:deserialize_from_db(data)
    self.task_type = data.task_type
    self.task_id   = data.task_id
    if not self:load_config() then
        log_err("[BattlePassTask][deserialize_from_db] deserialize faild: %s", serialize(data))
        return false
    end
    for _, progress in pairs(data.progresses or {}) do
        self.behavior_map[progress.behavior_id] = progress.value
    end
    self.state            = data.state
    self.obtain_time      = data.obtain_time
    self.inited           = false
    self.bevent_listening = false
    return true
end

-- 序列化到db
function BattlePassTask:serialize_to_db()
    local data = {
        task_type   = self.task_type,
        task_id     = self.task_id,
        state       = self.state,
        obtain_time = self.obtain_time,
        progresses  = {},
    }
    for behavior_id, value in pairs(self.behavior_map or {}) do
        tinsert(data.progresses, {behavior_id = behavior_id, value = value})
    end
    return data
end

-- 序列化到任务进度
function BattlePassTask:serialize_to_progress_data()
    local data = {
        task_id          = self.task_id,
        task_type        = self.task_type,
        --task_obtain_time = self.obtain_time,
        state            = self.state,
        progresses       = {},
    }
    for behavior_id, value in pairs(self.behavior_map or {}) do
        tinsert(data.progresses, {behavior_id = behavior_id, value = value})
    end
    return data
end

function BattlePassTask:get_week_id()
    return self.cfg.week
end

-- 从配置构造
function BattlePassTask:reset_from_cfg(cfg)
    self.task_type = cfg.type
    self.task_id   = cfg.id
    self.cfg       = cfg
    for _, condition in pairs(cfg.conditions or {}) do
        self.behavior_map[condition.behavior_id] = 0
    end
    self.state            = BattlePassTaskState.WAITING
    self.obtain_time      = otime()
    self.inited           = false
    self.bevent_listening = false
    return true
end

-- 初始化
function BattlePassTask:init(player)
    self.host_player = player
    -- 尝试激活等待中的任务
    local battlepass_mgr = quanta.get("battlepass_mgr")
    if self.state == BattlePassTaskState.WAITING then
        if self.task_type == BattlePassTaskType.WEEK then
            if self.cfg.week <= battlepass_mgr:get_current_season_week_id() then
                self:reset_state(BattlePassTaskState.PROGRESSING)
            end
        else
            self:reset_state(BattlePassTaskState.PROGRESSING)
        end
    end
    self.inited = true
    if not self.bevent_listening then
        self:_install_listener()
    end
    return true
end

-- 反初始化
function BattlePassTask:uninit()
    self:_uninstall_listener()
    self.inited = false
    return true
end

-- 重新开始
function BattlePassTask:restart()
    log_debug("[BattlePassTask:restart] task restart: player_id=%s,task_id=%s", self.host_player:get_player_id(), self.task_id)
    for behavior_id, _ in pairs(self.behavior_map or {}) do
        self.behavior_map[behavior_id] = 0
    end
    self:reset_state(BattlePassTaskState.PROGRESSING)
end

-- 重置状态
function BattlePassTask:reset_state(state)
    if self.state ~= state then
        self.state = state
        -- 初始化完的需要发出事件
        if self.inited then
            event_mgr:notify_trigger("evt_btask_update", self.host_player, {}, {self:serialize_to_progress_data()})

            -- 完成任务需要额外事件
            if BattlePassTaskState.FINISH == self.state then
                event_mgr:notify_trigger("evt_btask_finish", self.host_player, self.task_id)
            elseif BattlePassTaskState.PRIZE_TAKEN == self.state then -- 领奖完成后循环任务需要重启
                if self.cfg.circle then
                    self:restart()
                end
            end
        end
        self.host_player.battlepass_dirty = true
    end

    -- 初始化完成后需要根据状态变化决定是否监听或者取消监听事件
    if self.inited then
        if BattlePassTaskState.PROGRESSING == self.state then
            if not self.bevent_listening then
                self:_install_listener()
            end
        else
            self:_uninstall_listener()
        end
    end
end

-- 析构函数（gc强制触发）
function BattlePassTask:__release()
    if self.inited then
        self:uninit()
    end
end

function BattlePassTask:_install_listener()
    if self.bevent_listening then
        log_warn("[BattlePassTask][_install_listener] reinstall listener: task_id=%s", self.task_id)
        return
    end
    -- 注册事件
    for _, condition in pairs(self.cfg.conditions or {}) do
        event_mgr:notify_trigger("behavior_add_subscribe", self.host_player:get_player_id(), condition.behavior_id, self)
    end
    self.bevent_listening = true
end

function BattlePassTask:_uninstall_listener()
    if not self.bevent_listening then
        return
    end
    -- 反注册事件
    for _, condition in pairs(self.cfg.conditions or {}) do
        event_mgr:notify_trigger("behavior_remove_subscribe", self.host_player:get_player_id(), condition.behavior_id, self)
    end
    self.bevent_listening = false
end

-- 更新任务行为统计值
local function update_task_behavior_value(task, behavior_id, value, op_type)
    if BehaviorOperationType.INCREASE == op_type then  -- +
        task.behavior_map[behavior_id] = task.behavior_map[behavior_id] + value
        return true
    elseif BehaviorOperationType.DECREASE == op_type then  -- -
        task.behavior_map[behavior_id] = task.behavior_map[behavior_id] - value
        return true
    elseif BehaviorOperationType.ASSIGN == op_type then -- =
        task.behavior_map[behavior_id] = value
        return true
    elseif BehaviorOperationType.GREATER_OR_EQUAL_ASSIGN == op_type then -- If >= then =
        if value > task.behavior_map[behavior_id] then
            task.behavior_map[behavior_id] = value
            return true
        end
    elseif BehaviorOperationType.LESS_OR_EQUAL_ASSIGN == op_type then -- if <= then =
        if value < task.behavior_map[behavior_id] then
            task.behavior_map[behavior_id] = value
            return true
        end
    elseif BehaviorOperationType.GREATER_OR_EQUAL_INCREASE == op_type then -- if >= then self increase
        if value >= task.behavior_map[behavior_id] then
            task.behavior_map[behavior_id] = task.behavior_map[behavior_id] + 1
            return true
        end
    end
    return false
end

--luacheck: ignore 561
-- 判断任务是否已经完成
local function check_task_is_finish(task, behaviors)
    local task_finish = true
    for bid, cfg in pairs(behaviors or {}) do
        if BehaviorCompareType.GREATER_OR_EQUAL == cfg.cmp_type then -- >=
            if not (task.behavior_map[bid] >= cfg.value) then
                task_finish = false
            end
        elseif BehaviorCompareType.LESS_OR_EQUAL == cfg.cmp_type then -- <=
            if not (task.behavior_map[bid] <= cfg.value) then
                task_finish = false
            end
        elseif BehaviorCompareType.EQUAL == cfg.cmp_type then -- =
            if not (task.behavior_map[bid] == cfg.value) then
                task_finish = false
            end
        elseif BehaviorCompareType.GREATER == cfg.cmp_type then -- >
            if not (task.behavior_map[bid] > cfg.value) then
                task_finish = false
            end
        elseif BehaviorCompareType.LESS == cfg.cmp_type then -- <
            if not (task.behavior_map[bid] < cfg.value) then
                task_finish = false
            end
        else
            task_finish = false
        end
    end
    return task_finish
end

-- 回调
function BattlePassTask:evt_behavior_update(player, behavior_id, value)
    -- 只有进行中的任务才会更新数组
    if self.state ~= BattlePassTaskState.PROGRESSING then
        return
    end
    if not self.behavior_map[behavior_id] or self.host_player:get_player_id() ~= player:get_player_id() then
        return
    end
    -- 根据行为表中会标识更新方式修改数值
    local battlepass_mgr = quanta.get("battlepass_mgr")
    local behaviors = battlepass_mgr:get_task_behaviors(self.task_id)
    if not behaviors then
        return
    end
    if update_task_behavior_value(self, behavior_id, value, behaviors[behavior_id].op_type) then
        -- 检查任务是否已经完成
        if check_task_is_finish(self, behaviors) then
            self:reset_state(BattlePassTaskState.FINISH)
        else
            event_mgr:notify_trigger("evt_btask_update", self.host_player, {}, {self:serialize_to_progress_data()})
        end
        self.host_player.battlepass_dirty = true
    end
end

function BattlePassTask:get_prize()
    local config = self:load_config()
    if config then
        return config.prize
    end
end

-- 获取当前任务原始配置
function BattlePassTask:load_config()
    if not self.cfg then
        if self.task_type == BattlePassTaskType.LOOP then
            self.cfg = bpass_ltask_db:find_one(self.task_id)
        elseif self.task_type == BattlePassTaskType.DAY then
            self.cfg = bpass_dtask_db:find_one(self.task_id)
        elseif self.task_type == BattlePassTaskType.WEEK then
            self.cfg = bpass_wtask_db:find_one(self.task_id)
        end
    end
    return self.cfg
end

return BattlePassTask