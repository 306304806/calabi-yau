-- cond_stars_win_cnt.lua
-- 玩家某星以上胜利次数判断

local tunpack       = table.unpack

local event_mgr     = quanta.get("event_mgr")

local Condition = import("lobby/entity/condition/condition.lua")
local CondStarsWinCnt = class(Condition)

function CondStarsWinCnt:__init(belonger, conf, params)
    logger.debug("CondStarsWinCnt:__init->belonger:%s, conf:%s, params:%s", type(belonger), type(conf), type(params))

    self:init(belonger, conf, params)

    event_mgr:add_trigger(self, "on_player_win_qualifying")
end

function CondStarsWinCnt:test(context)
    local player = context.player
    local conf_stars = self.cond.stars or 0
    local win_count  = self.cond.win_count or 0
    if player then
        return player:total_qualifying_wins(conf_stars) >= win_count
    end

    return false
end

-- 玩家排位赛取得胜利触发回调
function CondStarsWinCnt:on_player_win_qualifying(player)
    local context = {player = player}
    if self:test(context) then
        event_mgr:notify_trigger(self.conf.trigger_event, player, self.belonger, tunpack(self.params))
    end
end

return CondStarsWinCnt
