--condition.lua
-- 条件基类

local Condition = class()
local prop = property(Condition)
prop:accessor("belonger", {})       -- 条件对象归属者
prop:accessor("conf", {})           -- 条件相关配置信息
prop:accessor("params", {})         -- 回调参数信息

function Condition:__init()

end

function Condition:init(belonger, conf, params)
    self.belonger = belonger
    self.conf     = conf
    self.params   = params
end

function Condition:test(context)
    return false
end

return Condition
