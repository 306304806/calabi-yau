-- cond_player_stars.lua
-- 玩家星数判断
local tunpack       = table.unpack

local event_mgr     = quanta.get("event_mgr")

local Condition = import("lobby/entity/condition/condition.lua")
local CondPlayerStars = class(Condition)

function CondPlayerStars:__init(belonger, conf, params)
    self:init(belonger, conf, params)

    event_mgr:add_trigger(self, "on_change_player_stars")
end

function CondPlayerStars:test(context)
    local player = context.player
    local conf_stars = self.cond.stars
    if player and conf_stars then
        return player:get_stars() >= conf_stars
    end

    return false
end

function CondPlayerStars:on_change_player_stars(player)
    local context = {player = player}
    if self:test(context) then
        event_mgr:notify_trigger(self.conf.trigger_event, player, self.belonger, tunpack(self.params))
    end
end

return CondPlayerStars
