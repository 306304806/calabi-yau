--token_mgr.lua
local otime         = os.time
local log_err       = logger.err

local PeriodTime    = enum("PeriodTime")

local timer_mgr     = quanta.get("timer_mgr")

local config_mgr    = quanta.get("config_mgr")
local event_mgr     = quanta.get("event_mgr")

local role_db       = config_mgr:get_table("role")
local roleskin_db   = config_mgr:get_table("roleskin")
local free_role_db  = config_mgr:get_table("freerole")

-- 角色管理器
local FreeRoleMgr = singleton()
local prop = property(FreeRoleMgr)
prop:accessor("wfree_roles_info", nil)       -- 周免角色id列表
prop:accessor("free_role_id_set", {})        -- 免费英雄set
prop:accessor("free_role_version", otime())  -- 免费英雄版本号
function FreeRoleMgr:__init()
    timer_mgr:loop(PeriodTime.SECOND_MS, function()
        self:on_timer()
    end)

    self:setup()
end

-- 初始化
function FreeRoleMgr:setup()
    self:update_week_free_roles_from_cfg()
end

-- 定时器回调
function FreeRoleMgr:on_timer()

    if self:is_current_wfree_need_update() then
        self:update_week_free_roles_from_cfg()
    end
end

-- 当前配置是否在可用
function FreeRoleMgr:is_current_wfree_need_update()
    local cur_time = otime()
    -- 当前配置生效中
    if self.wfree_roles_info and
        (self.wfree_roles_info.start_time <= cur_time and cur_time <= self.wfree_roles_info.end_time) then
        return false
    end

    return true;
end

-- 寻找当前可用周免配置
function FreeRoleMgr:find_current_wfree_cfg()
    local cur_time = otime()
    for _, cfg in free_role_db:iterator() do
        if cfg.start_time <= cur_time and cur_time <= cfg.end_time then
            return cfg
        end
    end
end

-- 从配置更新周免英雄
function FreeRoleMgr:update_week_free_roles_from_cfg()
    local reset_wfree = false
    if self.wfree_roles_info then
        -- 清空失效id
        for _, rid in pairs(self.wfree_roles_info and self.wfree_roles_info.role_ids or {}) do
            self.free_role_id_set[rid] = nil
        end
        self.wfree_roles_info   = nil
        reset_wfree = true
    end

    self.wfree_roles_info = self:find_current_wfree_cfg()
    if self.wfree_roles_info then
        -- 免费map更新
        for _, rid in pairs(self.wfree_roles_info.role_ids) do
            local role_cfg = role_db:find_one(rid)
            local skin_cfg
            if role_cfg then
                skin_cfg  = roleskin_db:find_one(role_cfg.role_skin)
            end
            if not role_cfg or not skin_cfg then
                log_err("[FreeRoleMgr][update_week_free_roles_from_cfg] config error role_id or default skin invalid: role_id=%s", rid)
                goto leb_next_role
            end

            self.free_role_id_set[rid] = true

            :: leb_next_role ::
        end
        self.free_role_version = otime()
        reset_wfree = true
    end

    -- 更新版本
    if reset_wfree then
        self.free_role_version  = otime()
        event_mgr:notify_trigger("evt_free_roles_update")
    end

    return true
end

-- 是否免费角色
function FreeRoleMgr:is_free_role(role_id)
    return self.free_role_id_set[role_id] and true or false
end

-- 获取免费角色id列表
function FreeRoleMgr:get_free_role_ids()
    return self.wfree_roles_info and self.wfree_roles_info.role_ids or {}
end

-- 获取免费角色id_set
function FreeRoleMgr:get_free_role_id_set()
    return self.free_role_id_set
end

-- 获取周免信息
function FreeRoleMgr:get_wfree_info()
    local wfree_roles_info = {
        start_time = 0,
        end_time   = 0,
        role_ids   = {},
    }

    if self.wfree_roles_info then
        wfree_roles_info.start_time = self.wfree_roles_info.start_time
        wfree_roles_info.end_time   = self.wfree_roles_info.end_time
        wfree_roles_info.role_ids   = self.wfree_roles_info.role_ids
    end

    return wfree_roles_info
end



-- export
quanta.free_role_mgr = FreeRoleMgr()

return FreeRoleMgr
