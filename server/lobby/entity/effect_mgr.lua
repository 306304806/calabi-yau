-- effect_mgr.lua
local log_err           = logger.err

local EffAddGoods       = import("lobby/entity/effect/effect_add_goods.lua")
local EffAddGoodsGroup  = import("lobby/entity/effect/effect_add_goods_group.lua")
local EffAddRole        = import("lobby/entity/effect/effect_add_role.lua")
local EffAddRoleSkin    = import("lobby/entity/effect/effect_add_skin.lua")
local EffAddWeapon      = import("lobby/entity/effect/effect_add_weapon.lua")
local EffAddDecal       = import("lobby/entity/effect/effect_add_decal.lua")
local EffAddVCardRes    = import("lobby/entity/effect/effect_add_vcard_res.lua")
local EffAddBuff        = import("lobby/entity/effect/effect_add_buff.lua")
local EffModAttr        = import("lobby/entity/effect/effect_mod_attribute.lua")
local EffAddCurrency    = import("lobby/entity/effect/effect_add_currency.lua")
local EffAddRoleAction  = import("lobby/entity/effect/effect_add_role_action.lua")
local EffAddRoleVoice   = import("lobby/entity/effect/effect_add_role_voice.lua")

local EffectType        = enum("EffectType")

local config_mgr        = quanta.get("config_mgr")
local effect_db         = config_mgr:get_table("effect")

local EffectMgr  = singleton()
function EffectMgr:__init()

end

-- 创建效果
function EffectMgr:create_effect(id)
    local eff_cfg = effect_db:find_one(id)
    if not eff_cfg then
        log_err("[EffectMgr][create_effect] get effect config failed! effect_id:%s", id)
        return
    end

    local effect_class = {
        [EffectType.ADD_GOODS]          = EffAddGoods,
        [EffectType.ADD_GOODS_GROUP]    = EffAddGoodsGroup,
        [EffectType.ADD_ROLE]           = EffAddRole,
        [EffectType.ADD_ROLESKIN]       = EffAddRoleSkin,
        [EffectType.ADD_WEAPON]         = EffAddWeapon,
        [EffectType.ADD_DECAL]          = EffAddDecal,
        [EffectType.ADD_BUFF]           = EffAddBuff,
        [EffectType.MOD_ATTRIBUTE]      = EffModAttr,
        [EffectType.ADD_CURRENCY]       = EffAddCurrency,

        [EffectType.ADD_VCARD_AVATAR]   = EffAddVCardRes,
        [EffectType.ADD_VCARD_BG]       = EffAddVCardRes,
        [EffectType.ADD_VCARD_FRAME]    = EffAddVCardRes,

        [EffectType.ADD_ROLEACTION]     = EffAddRoleAction,
        [EffectType.ADD_ROLEVOICE]      = EffAddRoleVoice,

    }
    return effect_class[eff_cfg.effect_type](id)
end

quanta.effect_mgr = EffectMgr()

return EffectMgr
