--player.lua
local log_warn          = logger.warn
local tinsert           = table.insert
local guid_group        = guid.group
local room_type         = guid.room_type
local uedition_utc      = utility.edition_utc

local PlayerAttrID      = enum("PlayerAttrID")
local OnlineStatus      = enum("OnlineStatus")
local AchieveType       = enum("AchieveType")
local CSCmdID           = enum("CSCmdID")
local KernCode          = enum("KernCode")

local plat_api          = quanta.get("plat_api")
local client_mgr        = quanta.get("client_mgr")
local config_mgr        = quanta.get("config_mgr")

local utility_db        = config_mgr:get_table("utility")
local FLUSH_TIME        = utility_db:find_value("value", "day_flush_time")

local RoleComponent         = import("lobby/component/role_component.lua")
local BagComponent          = import("lobby/component/bag_component.lua")
local QualifyingComponent   = import("lobby/component/qualifying_component.lua")
local SettingComponent      = import("lobby/component/setting_component.lua")
local PrepareComponent      = import("lobby/component/prepare_component.lua")
local AttributeComponent    = import("lobby/component/attribute_component.lua")
local RewardComponent       = import("lobby/component/reward_component.lua")
local VCardComponent        = import("lobby/component/vcard_component.lua")
local CareerComponent       = import("lobby/component/career_component.lua")
local BuffComponent         = import("lobby/component/buff_component.lua")
local StandingsComponent    = import("lobby/component/standings_component.lua")
local AchieveComponent      = import("lobby/component/achieve_component.lua")
local BattlePassComponent   = import("lobby/component/battlepass_component.lua")

--创建角色数据
local Player = class(nil,
    AttributeComponent,
    RoleComponent,
    BagComponent,
    SettingComponent,
    QualifyingComponent,
    PrepareComponent,
    RewardComponent,
    VCardComponent,
    CareerComponent,
    BuffComponent,
    StandingsComponent,
    AchieveComponent,
    BattlePassComponent
)

local prop = property(Player)
prop:reader("open_id")              --open_id
prop:reader("player_id")            --player_id
prop:reader("area_id")              --小区id
prop:reader("day_edition", 0)       --日刷新版本号
prop:reader("week_edition", 0)      --周刷新版本号
prop:accessor("rank", 1)            --rank
prop:accessor("session", nil)       --session
prop:accessor("team_id", nil)       --team_id
prop:accessor("room_id", nil)       --room_id

function Player:__init(open_id, player_id, area_id)
    self.open_id    = open_id
    self.player_id  = player_id
    self.area_id    = area_id
end

-- 初始化
function Player:setup(db_data, day_edition, week_edition)
    local setup_ok = self:collect("setup", db_data)
    if not setup_ok then
        log_warn("[Player:setup] player %s setup faild!", self.player_id)
        return setup_ok
    end
    --上次的周期更新时间
    local logout_time = self:get_logout_time()
    self.day_editon = uedition_utc("day", logout_time, FLUSH_TIME)
    self.week_editon = uedition_utc("week", logout_time, FLUSH_TIME)
    --周期刷新
    self:period_flush(day_edition, week_edition)
    return setup_ok
end

--get_room_type
function Player:get_room_type()
    if self.room_id then
        local group = guid_group(self.room_id)
        local r_type = room_type(group)
        return r_type, self.room_id
    end
end

--周期刷新
function Player:period_flush(day_edition, week_edition)
    if day_edition ~= self.day_edition then
        self:invoke("flush_day", day_edition)
        self.day_edition = day_edition
    end
    if week_edition ~= self.week_edition then
        self.week_edition = week_edition
        self:invoke("flush_week", week_edition)
    end
end

--update
function Player:update(day_edition, week_edition)
    --周期刷新
    self:period_flush(day_edition, week_edition)
    self:invoke("update", quanta.now)
end

--unload
function Player:unload()
    self:collect("unload")
end

-- package all
function Player:package_debug_data()
    local data = {}
    self:invoke("package_to_data", data)
    return data
end

--package
function Player:package()
    local info = {
        rank            = self.rank,
        player_id       = self.player_id,
        icon            = self:get_icon(),
        sex             = self:get_sex(),
        nick            = self:get_nick(),
        level           = self:get_level(),
        vc_avatar_id    = self:get_attribute(PlayerAttrID.VCARD_AVATAR_ID),
        vc_border_id    = self:get_attribute(PlayerAttrID.VCARD_BORDER_ID),
        vc_frame_id     = self:get_attribute(PlayerAttrID.VCARD_FRAME_ID),
        vc_achie_id     = self:get_attribute(PlayerAttrID.VCARD_ACHIE_ID),
    }
    return info
end

-- 构建进入战斗房间数据
function Player:build_ds_roles()
    local fight_infos = {}
    for role_id, role in pairs(self:get_roles()) do
        local role_info = {
            role_id = role_id,
            skin_id = role:get_skin_id(),
            weapons = self:get_role_ds_weapon(role_id),
        }
        tinsert(fight_infos, role_info)
    end
    return fight_infos
end

--发送client消息
function Player:send(cmd, data, session_id)
    if self.session then
        client_mgr:send_dx(self.session, cmd, data, session_id)
    end
end

--发送client消息
function Player:callback(cmd, data, session_id)
    if self.session then
        client_mgr:callback_dx(self.session, cmd, data, session_id)
    end
end

-- 保存到DB（卸载之前必须保存，否则可能导致数据丢失）
function Player:save(flush)
    return self:collect("update_data_2db", flush)
end

-- 发送通用提示
function Player:send_tips(code, ttype, ...)
    local tips = {
        code = code,
        type = ttype,
        params = {...},
    }
    self:send(CSCmdID.NID_COMMON_TIPS_NTF, tips)
    return KernCode.DYNAMIC
end

--plat_api 上报接口
--------------------------------------------------------------------
function Player:platform_login(token, machine_id, is_create)
    local account_data = {machine_id = machine_id, open_id = self.open_id, ip = self.session.ip}
    if is_create then
        local player_data = {
            player_id   = self.player_id,           --player_id
            nick        = self:get_nick(),          --nick
            icon        = self:get_icon(),          --icon
            sex         = self:get_sex(),           --sex
            rank        = self.rank,                --rank
            team_id     = self.team_id or 0,        --team_id
            room_id     = self.room_id or 0,        --room_id
            status      = OnlineStatus.ONLINE,      --状态
            level       = self:get_level(),         --level
            stars       = self:get_stars(),         -- 当前星数
            win_games   = self:get_win_games(),     -- 排位胜利场数
            mvp_count   = self:get_mvp_games(),     -- 排位mvp场数
            total_games = self:get_total_games(),   -- 排位总场数
            avr_kill    = 0,                        -- 排位平均击杀数
            freq_roles  = {101, 102, 103},          -- 排位常用英雄
            medal_cnt   = self:get_achieve_cnt(AchieveType.MEDAL),
            epic_cnt    = self:get_achieve_cnt(AchieveType.EPIC),
            honour_cnt  = self:get_achieve_cnt(AchieveType.HONOUR),
        }
        return plat_api:create_player(self.player_id, player_data, token, account_data)
    else
        local player_data = {
            player_id   = self.player_id,       --player_id
            team_id     = self.team_id or 0,    --team_id
            room_id     = self.room_id or 0,    --room_id
            status      = OnlineStatus.ONLINE,  --状态
        }
        return plat_api:login_player(self.player_id, player_data, token, account_data)
    end
end

function Player:platform_logout()
    plat_api:logout_player(self.player_id, { status = OnlineStatus.OFFLINE })
end

function Player:platform_offline()
    plat_api:update_player(self.player_id, { status = OnlineStatus.LOST })
end

function Player:platform_watching(is_watch)
    plat_api:update_player(self.player_id, { status = is_watch and OnlineStatus.WATCH or OnlineStatus.ONLINE })
end

function Player:platform_room_sync()
    plat_api:update_player(self.player_id, { room_id = self.room_id or 0 })
end

function Player:platform_team_sync()
    plat_api:update_player(self.player_id, { team_id = self.team_id or 0 })
end

-- 排位相关数据同步
function Player:platform_qualifying_sync()
    local info = {
        stars       = self:get_stars(),
        win_games   = self:get_win_games(),
        mvp_count   = self:get_mvp_games(),
        total_games = self:get_total_games(),
        avr_kill    = 0,
        freq_roles = {101, 102, 103},
    }
    plat_api:update_player(self.player_id, info)
end

function Player:platform_level_sync()
    plat_api:update_player(self.player_id, { level = self:get_level() })
end

function Player:platform_nick_sync()
    plat_api:update_player(self.player_id, { nick = self:get_nick() })
end

-- 精分上报-公共字段
function Player:build_dlog_public_fields()
    return {area_id = self.area_id, open_id = self.open_id, character_id = self.player_id, character_level = self:get_level()}
end

-- 成就数上报
function Player:platform_achieve_sync()
    local info = {
        medal_cnt   = self:get_achieve_cnt(AchieveType.MEDAL),
        epic_cnt    = self:get_achieve_cnt(AchieveType.EPIC),
        honour_cnt  = self:get_achieve_cnt(AchieveType.HONOUR),
    }
    plat_api:update_player(self.player_id, info)
end

return Player
