--player_mgr.lua
local WheelMap      = import("kernel/basic/wheel_map.lua")

local log_err       = logger.err
local log_info      = logger.info
local log_warn      = logger.warn
local log_debug     = logger.debug
local serialize     = logger.serialize
local tinsert       = table.insert
local tjoin         = table_ext.join
local hash_code     = utility.hash_code
local uedition_utc  = utility.edition_utc
local check_failed  = utility.check_failed

local ncmdid        = ncmd_cs.NCmdId
local PeriodTime    = enum("PeriodTime")
local ResAddType    = enum("ResAddType")

local timer_mgr     = quanta.get("timer_mgr")
local event_mgr     = quanta.get("event_mgr")
local config_mgr    = quanta.get("config_mgr")
local cache_agent   = quanta.get("cache_agent")
local player_dao    = quanta.get("player_dao")

local utility_db    = config_mgr:init_table("utility", "id")
local player_def_db = config_mgr:get_table("accountdefaultdata")

local FLUSH_TIME    = utility_db:find_value("value", "day_flush_time")

-- 命令白名单（不需要登录权限的）
local comand_white_list = {
    [ncmdid.NID_LOGIN_REQ]     = 1,
    [ncmdid.NID_RECONNECT_REQ] = 1,
}

local PlayerMgr = singleton()
local prop = property(PlayerMgr)
prop:reader("player_map", nil)
prop:reader("day_edition", 0)
prop:reader("week_edition", 0)
prop:reader("default_items", {})
prop:reader("default_roles", {})
prop:reader("default_skins", {})
prop:reader("default_vcards", {})

function PlayerMgr:__init()
    self:init_player_config()
    self.player_map = WheelMap(10)
    --注册转发消息
    event_mgr:add_listener(self, "forward_player")
    event_mgr:add_listener(self, "rebuild_cache")

    timer_mgr:loop(PeriodTime.SECOND_MS, function()
        self:on_timer()
    end)
end

--初始化玩家默认配置
function PlayerMgr:init_player_config()
    local player_def_cfg = player_def_db:find_one(1)
    if player_def_cfg then
        local vcard_ids, item_ids = {}, {}
        self.default_roles = player_def_cfg.default_role or {}
        self.default_skins = player_def_cfg.default_roleskin or {}
        tjoin(player_def_cfg.default_item, item_ids)
        tjoin(player_def_cfg.default_weapon, item_ids)
        tjoin(player_def_cfg.default_frame, vcard_ids)
        tjoin(player_def_cfg.default_appearance, vcard_ids)
        tjoin(player_def_cfg.default_background, vcard_ids)
        for _, rid in pairs(item_ids) do
            tinsert(self.default_items, {item_id = rid, num = 1, add_reason = ResAddType.ROLE_DEFAULT, expire_time = 0})
        end
        for _, rid in pairs(vcard_ids) do
            tinsert(self.default_vcards, {resource_id = rid, life_time = 0, add_reason = ResAddType.ROLE_DEFAULT})
        end
    end
end

--初始化玩家默认数据
function PlayerMgr:init_player_default_data(player)
    player:add_items(self.default_items)
    player:add_vcard_resources(self.default_vcards)
    for _, rid in pairs(self.default_roles) do
        player:add_role({ role_id = rid, life_time = 0, reason = ResAddType.ROLE_DEFAULT})
    end
    for _, rid in pairs(self.default_skins) do
        player:add_role_skin({ role_skin_id = rid, life_time = 0, reason = ResAddType.ROLE_DEFAULT})
    end
end

function PlayerMgr:on_timer()
    local day_edition = uedition_utc("day", quanta.now, FLUSH_TIME)
    if day_edition ~= self.day_edition then
        self.day_edition = day_edition
        self.week_edition = uedition_utc("week", quanta.now, FLUSH_TIME)
    end
    for _, player in self.player_map:iterator() do
        player:update(day_edition, self.week_edition)
        player:save()
    end
end

--创建玩家
function PlayerMgr:create_player(open_id, player_id, area_id)
    local Player = import("lobby/entity/player.lua")
    local player = Player(open_id, player_id, area_id)
    local ok, db_data = player_dao:load_player(player_id)
    if ok and player:setup(db_data, self.day_edition, self.week_edition) then
        return player
    end
end


-- 设置玩家
function PlayerMgr:set_player(player_id, player)
    log_info("[PlayerMgr][set_player] player_id=%s", player_id)
    self.player_map:set(player_id, player)
end

-- 移除玩家
function PlayerMgr:remove_player(player, player_id)
    log_info("[PlayerMgr][remove_player] player_id=%s", player_id)
    player:unload()
    player_dao:flush_player(player_id)
    self.player_map:set(player_id, nil)
end

--查找玩家
function PlayerMgr:get_player(player_id)
    return self.player_map:get(player_id)
end

--获取玩家数量
function PlayerMgr:get_player_count()
    return self.player_map:get_count()
end

--查找玩家
function PlayerMgr:iterator()
    return self.player_map:iterator()
end

--重建缓存
function PlayerMgr:rebuild_cache(hash, count)
    local players = {}
    for player_id in self.player_map:iterator() do
        local hash_key = hash_code(player_id, count)
        if hash_key == hash then
            tinsert(players, player_id)
        end
    end
    if #players > 0 then
        local ec = cache_agent:rebuild(players)
        if check_failed(ec) then
            log_err("[PlayerMgr][rebuild_cache] failed: ec=%s", ec)
        end
    end
end

-- 会话需要同步
function PlayerMgr:on_session_sync(session)
    local player_id = session.player_id
    local player = self:get_player(player_id)
    if player then
        log_debug("[PlayerMgr][on_session_sync] player_id:%s", player_id)
        event_mgr:notify_trigger("on_player_sync", player_id, player)
    end
end

-- 会话消息
function PlayerMgr:on_session_cmd(session, cmd_id, body, session_id)
    -- 验证session是否有登录后绑定的user_id
    local player_id = session.player_id
    local player = self:get_player(player_id)
    -- 验证当前会话是否已有登录权限
    if not player and not comand_white_list[cmd_id] then
        log_warn("[PlayerMgr][on_session_cmd] need login, cmd_id=%s, token=%s, player_id=%s", cmd_id, player, player_id)
        return
    end
    event_mgr:notify_command(cmd_id, player or session, player_id, body, session_id)
end

--转发其他服务器给客户端的消息
function PlayerMgr:forward_player(player_id, cmd_id, msg)
    local player = self:get_player(player_id)
    if player then
        log_debug("[PlayerMgr][forward_player] player_id:%s cmd:%s, msg:%s", player_id, cmd_id, serialize(msg))
        player:send(cmd_id, msg)
    end
end

-- 广播消息
function PlayerMgr:boardcast_message(cmd_id, data)
    for _, player in self.player_map:iterator() do
        player:send(cmd_id, data)
    end
end

-- export
quanta.player_mgr = PlayerMgr()

return PlayerMgr
