--item.lua
-- 仓库物品基类

local otime         = os.time

local Item = class()
local prop = property(Item)
prop:accessor("item_uuid",      0)          --uuid, item全服唯一识别码
prop:accessor("item_id",        0)          --配置id, 原生id
prop:accessor("count",          0)          --数量
prop:accessor("item_type",      1)          --类型 [物品/武器/姿势/贴画/贴花...]
prop:accessor("obtain_time",    0)          --获得时间
prop:accessor("expire_time",    0)          --到期时间 [0表示永久]
prop:accessor("approach",       0)          --获取途径

function Item:__init()

end

function Item:init(item_data)
    self.item_uuid   = item_data.item_uuid
    self.item_id     = item_data.item_id
    self.count       = item_data.count
    self.item_type   = item_data.item_type
    self.obtain_time = item_data.obtain_time
    self.approach    = item_data.approach
    self.expire_time = item_data.expire_time or 0
end

-- 构造item存储数据
function Item:build_save_data()
    local item_data = {
                        item_uuid   = self.item_uuid,
                        item_id     = self.item_id,
                        count       = self.count,
                        item_type   = self.item_type,
                        obtain_time = self.obtain_time,
                        approach    = self.approach,
                      }

    if self.expire_time > 0 then
        item_data.expire_time = self.expire_time
    end

    return item_data
end

-- 检查是否过期
function Item:is_expired()
    local cur_time = otime()
    -- expire_time小于等于0代表永久
    if self.expire_time <= 0 or self.expire_time > cur_time then
        return false
    else
        return true
    end
end

function Item:sync_client_expire_time()
    local cur_time = otime()
    local expire_time = self:get_expire_time()
    if expire_time > 0 and expire_time > cur_time then
        return expire_time - cur_time
    end

    return 0
end

-- 是否枪械
function Item:is_gun()
    return false
end

return Item
