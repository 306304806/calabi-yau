--decal.lua
-- 贴花【喷漆】

local Item = import("lobby/entity/item/item.lua")

local Decal = class(Item)

function Decal:__init(item_data)
    self:init(item_data)
end

-- 构造存储数据
function Decal:build_save_data()
    local item_data = {
                        item_uuid   = self.item_uuid,
                        item_id     = self.item_id,
                        count       = self.count,
                        item_type   = self.item_type,
                        obtain_time = self.obtain_time,
                        approach    = self.approach,
                      }

    if self.expire_time > 0 then
        item_data.expire_time   = self.expire_time
    end

    return item_data
end

return Decal