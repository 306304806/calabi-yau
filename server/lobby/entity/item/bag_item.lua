--bag_item.lua
-- 物品[道具/礼包等]

local Item = import("lobby/entity/item/item.lua")

local otime         = os.time
local tunpack       = table.unpack
local tinsert       = table.insert
local log_err       = logger.err
local ssplit        = string_ext.split
local check_failed  = utility.check_failed

local BagCode       = enum("BagCode")

local effect_mgr    = quanta.get("effect_mgr")
local config_mgr    = quanta.get("config_mgr")

local item_db       = config_mgr:get_table("item")

local ITEM_LOCK_TIME    = 5

local BagItem = class(Item)
function BagItem:__init(item_data)
    self:init(item_data)
    self.lock_cnt    = 0
    self.lock_time   = 0
end

-- 数量增加
function BagItem:increase(cnt)
    if cnt and cnt > 0 then
        self:set_count(self:get_count() + cnt)
        self:set_obtain_time(otime())
        return true, self.count
    else
        log_err("[BagItem][increase] failed! item_uid:%s, item_id:%s, cnt:%s", self.item_uuid, self.item_id, cnt)
        return false, self.count
    end
end

-- 数量减少
function BagItem:decrease(cnt)
    if cnt and cnt > 0 and cnt <= self.count then
        self.count = self.count - cnt
        return true, self.count
    else
        log_err("[BagItem][decrease] failed! item_uid:%s, item_id:%s, count:%s, use_cnt:%s", self.item_uuid, self.item_id, self.count, cnt)
        return false, self.count
    end
end

-- 锁定数量
--[[
    先检查是否已锁，若上锁时间比当前时间相差ITEM_LOCK_TIME，则解锁；否则判断锁定数量，锁定数量超过当前数量返回失败；
    锁定数量小于当前数量更新上锁时间。
]]
function BagItem:lock(num)
    local cur_time = otime()
    if cur_time > self.lock_time + ITEM_LOCK_TIME then
        self.lock_cnt  = 0
        self.lock_time = 0
    end

    if self.lock_cnt + num > self.count then
        return false
    end

    self.lock_cnt  = self.lock_cnt + num
    self.lock_time = cur_time
    return true
end

-- 解锁
function BagItem:unlock(num)
    if num and num <= self.lock_cnt then
        self.lock_cnt = self.lock_cnt - num
        return true
    else
        log_err("[BagItem][unlock] failed! item_uid:%s, item_id:%s, num:%s", self.item_uuid, self.item_id, num)
        return false
    end
end

-- 使用物品效果检查
function BagItem:effect_test(context)
    if self:get_count() < 1 then
        return BagCode.BAG_ERR_NUM_NOT_ENOUGH
    end

    local item_cfg = item_db:find_one(self.item_id)
    if not item_cfg then
        return BagCode.BAG_ERR_CFG_NOT_EXIST
    end

    -- todo:先发起一次使用物品投票，主要是其他部件或者玩家状态判定，如果有模块站出来反对则流程执行中止

    for _, effect_id in ipairs(tunpack(ssplit(item_cfg.effect_id, ','))) do
        local effect = effect_mgr:create_effect(effect_id)
        local code, effect_params = effect:test(context.player)
        if check_failed(code) then
            return code
        end
        tinsert(context.effects, {effect = effect, params = effect_params})
    end
end

return BagItem