--weapon.lua
-- 物品格子信息
local Item = import("lobby/entity/item/item.lua")
local tinsert       = table.insert
local log_err       = logger.err
local log_debug     = logger.debug

local GunLevel      = enum("GunLevel")
local WeaponSlot    = enum("WeaponSlot")
local WeaponSubType = enum("WeaponSubType")
local AttachBelongType      = enum("AttachBelongType")
local AttachmentUnlockType  = enum("AttachmentUnlockType")

local config_mgr    = quanta.get("config_mgr")
local weapon_db     = config_mgr:get_table("weapon")
local component_db  = config_mgr:get_table("component")
local weapon_type_db= config_mgr:get_table("weapontype")
local weaponlv_db   = config_mgr:get_table("weaponlevel")

local Weapon = class(Item)
local prop = property(Weapon)
prop:accessor("level", 0)               -- 当前等级
prop:accessor("cur_exp", 0)             -- 当前经验值
prop:accessor("item_unlocks", {})       -- 道具解锁限定武器配件信息
prop:accessor("equip_attachs", {})      -- 已装备配件

-- 构建武器
function Weapon:__init(item_data)
    self:init(item_data)

    if self:is_gun() then
        self.level             = item_data.level or GunLevel.MIN
        self.cur_exp           = item_data.cur_exp or 0
        self.item_unlocks      = item_data.item_unlocks or {}
        self.equip_attachs     = {}
        self.item_unlocks      = {}
        for _, data in pairs(item_data.equip_attachs or {}) do
            self.equip_attachs[data.attache_type] = data.attach_id
        end
        for _, data in pairs(item_data.item_unlocks or {}) do
            self.equip_attachs[data.attach_id] = data.item_id
        end
    end
end

-- 构造存储数据
function Weapon:build_save_data()
    local item_data = {
        item_uuid   = self.item_uuid,
        item_id     = self.item_id,
        count       = self.count,
        item_type   = self.item_type,
        obtain_time = self.obtain_time,
        approach    = self.approach,
    }

    if self.expire_time > 0 then
        item_data.expire_time   = self.expire_time
    end

    if self.level > 0 and self:is_gun() then
        item_data.level = self.level
    end

    if self.cur_exp > 0 then
        item_data.cur_exp = self.cur_exp
    end

    if next(self.item_unlocks) then
        item_data.item_unlocks = {}
        for attach_id, item_id in pairs(self.item_unlocks) do
            tinsert(item_data.item_unlocks, {attach_id = attach_id, item_id = item_id})
        end
    end

    if next(self.equip_attachs) then
        item_data.equip_attachs = {}
        for attache_type, attach_id in pairs(self.equip_attachs) do
            tinsert(item_data.equip_attachs, {attache_type = attache_type, attach_id = attach_id})
        end
    end

    return item_data
end

-- 是否主武器
function Weapon:is_primary()
    if self:get_weapon_cfg_slot() == WeaponSlot.PRIMARY then
        return true
    else
        return false
    end
end

-- 是否副武器
function Weapon:is_secondary()
    if self:get_weapon_cfg_slot() == WeaponSlot.SECONDARY then
        return true
    else
        return false
    end
end

-- 是否枪械
function Weapon:is_gun()
    local weapon_id = self:get_item_id()
    local weapon_cfg = weapon_db:find_one(weapon_id)
    if not weapon_cfg then
        return false
    end

    return (weapon_cfg.type >= WeaponSubType.AR and weapon_cfg.type <= WeaponSubType.SACB)
end

-- 是否地雷
function Weapon:is_grenade()
    local weapon_id = self:get_item_id()
    local weapon_cfg = weapon_db:find_one(weapon_id)
    if not weapon_cfg then
        return false
    end

    return (weapon_cfg.type >= WeaponSubType.Grenade and weapon_cfg.type <= WeaponSubType.DetectionGrenade)
end

-- 是否近战武器
function Weapon:is_melee_weapon()
    local weapon_id = self:get_item_id()
    local weapon_cfg = weapon_db:find_one(weapon_id)
    if not weapon_cfg then
        return false
    end

    return (weapon_cfg.type >= WeaponSubType.Fist and weapon_cfg.type <= WeaponSubType.Pillow)
end

-- 检查武器类型是否相同
function Weapon:is_same_weapon_type(cmp_weapon)
    if not cmp_weapon then
        return false
    end

    local cur_weapon_id = self:get_item_id()
    local cur_weapon_cfg = weapon_db:find_one(cur_weapon_id)
    local cmp_weapon_id = cmp_weapon:get_item_id()
    local cmp_weapon_cfg = weapon_db:find_one(cmp_weapon_id)
    if cur_weapon_cfg and cmp_weapon_cfg and cur_weapon_cfg.type == cmp_weapon_cfg.type then
        return true
    end

    return false
end

-- 获取装备位，区分主/副武器/手雷
function Weapon:get_weapon_cfg_slot()
    local weapon_id = self:get_item_id()
    local weapon_cfg = weapon_db:find_one(weapon_id)
    if not weapon_cfg then
        log_err("[Weapon][get_weapon_cfg_slot] get weapon cfg failed! weapon_id:%s", weapon_id)
        return 0
    end

    local weapontype_cfg = weapon_type_db:find_one(weapon_cfg.type)
    if weapontype_cfg then
        return weapontype_cfg.slot
    end

    log_err("[Weapon][get_weapon_cfg_slot] get weapontype cfg failed! weapon_id:%s, weapon_type:%s", weapon_id, weapon_cfg.type)
    return 0
end

-- 获取武器小类
function Weapon:get_weapon_type()
    local weapon_id = self:get_item_id()
    local weapon_cfg = weapon_db:find_one(weapon_id)
    if not weapon_cfg then
        log_err("[Weapon][get_weapon_cfg_slot] get weapon cfg failed! weapon_id:%s", weapon_id)
        return 0
    end

    return weapon_cfg.type
end

-- 检查配件是否可解锁
function Weapon:check_unlock(attach_id)
    local attach_cfg = component_db:find_one(attach_id)
    if not attach_cfg then
        return false
    end

    if attach_cfg.belo_type == AttachBelongType.SpecialWeaponId then
        -- 指定武器id检查
        local cfg_sp_weapon_id = attach_cfg.belo_param[1]
        if cfg_sp_weapon_id ~= self:get_item_id() then
            return false
        end
    elseif attach_cfg.belo_type == AttachBelongType.SpecialWeaponType then
        -- 指定武器类型检查
        local cfg_sp_weapon_ty = attach_cfg.belo_param[1]
        if cfg_sp_weapon_ty ~= self:get_weapon_type() then
            return false
        end
    end

    if attach_cfg.unlock_type == AttachmentUnlockType.ItemGoods then
        -- 解锁等级检查
        local need_weapon_level = attach_cfg.unlock_param[1]
        if need_weapon_level > self.level then
            return false
        end
    elseif attach_cfg.unlock_type == AttachmentUnlockType.ItemGoods then
        -- 解锁道具检查
        local unlock_item_id = attach_cfg.unlock_param[1]
        if unlock_item_id > 0 and self.item_unlocks[attach_id] and self.item_unlocks[attach_id] ~= unlock_item_id then
            return false
        end
    end

    return true
end

-- 是否已装备配件
function Weapon:is_equiped_attachment(attach_type, attach_id)
    if attach_type then
        return self.equip_attachs[attach_type] == attach_id
    end

    return false
end

-- 装备配件
function Weapon:equip_attachment(attach_type, attach_id)
    self.equip_attachs[attach_type] = attach_id
end

-- 卸下配件
function Weapon:unload_attachment(attach_type, attach_id)
    self.equip_attachs[attach_type] = nil
end

-- 解锁配件
function Weapon:unlock_attachment(attach_id, unlock_item_id)
    self.item_unlocks[attach_id] = unlock_item_id
end

-- 同步客户端武器配件数据
function Weapon:equiped_attachment_to_client()
    local ret_data = {}
    for attach_type, attach_id in pairs(self.equip_attachs) do
        tinsert(ret_data, {attach_type = attach_type, attach_id = attach_id, })
    end

    return ret_data
end

-- 同步客户端武器道具解锁配件数据
function Weapon:unlock_attachment_to_client()
    local ret_data = {}
    for attach_id in pairs(self.item_unlocks) do
        local attach_cfg = component_db:find_one(attach_id)
        if attach_cfg then
            tinsert(ret_data, {attach_type = attach_cfg.type, attach_id = attach_id, })
        end
    end
end

-- 根据当前等级获取解锁配件id
function Weapon:get_attchment_cfgid(cur_level)
    local cfg = component_db:select({weapon_id = self:get_item_id(), lock_type = 1})
    local unlock_levels = {}
    for _, attach_cfg in pairs(cfg) do
        unlock_levels[attach_cfg.weapon_level] = attach_cfg.component_id
    end

    local cfg_attach_id = unlock_levels[cur_level]
    if not cfg_attach_id then
        return 0
    end

    return cfg_attach_id
end

-- 获取已解锁配件
function Weapon:get_unlock_attachments()
    local ret_data = {}
    for _, attach_id in pairs(self.equip_attachs) do
        tinsert(ret_data, attach_id)
    end
    return ret_data
end

-- 能否装备槽位
function Weapon:can_equip_slot(slot_pos)
    if not slot_pos then
        return false
    end

    if self:is_primary() then
        return slot_pos == WeaponSlot.PRIMARY
    elseif self:is_secondary() then
        return slot_pos == WeaponSlot.SECONDARY
    elseif self:is_melee_weapon() then
        return slot_pos == WeaponSlot.MELEE
    elseif self:is_grenade() then
        return slot_pos >= WeaponSlot.GRENADE_A and slot_pos <= WeaponSlot.GRENADE_B
    end
end

-- 能否升级武器
function Weapon:check_can_upgrade()
    local weapon_id = self:get_item_id()
    local weapon_cfg = weapon_db:find_one(weapon_id)
    if not weapon_cfg then
        log_err("[Weapon][check_can_upgrade] get weapon cfg failed! weapon_id:%s", weapon_id)
        return false
    end

    local weapontype_cfg = weapon_type_db:find_one(weapon_cfg.type)
    if weapontype_cfg then
        return weapontype_cfg.upgrade
    end

    return false
end

-- 添加经验
function Weapon:add_exp(value)
    local ret, unlock_ids = false, {}
    if not self:is_gun() then
        log_err("[Weapon][add_exp]->is not gun! weapon_id:%s", self.item_id)
        return ret, unlock_ids
    end

    if not value or value <= 0 or self.level >= GunLevel.MAX then
        return ret, unlock_ids
    end
    --经验加成
    --log_debug("[Weapon][add_exp]->weapon_uuid:%s, weapon_id:%s, add_exp:%s", self.item_uuid, self.item_id, value)
    local cur_exp = self.cur_exp + value
    local cur_level = self.level
    local item = weaponlv_db:find_one(cur_level) or {}
    local upgrade_exp = item.exp
    while (upgrade_exp and cur_exp >= upgrade_exp) do
        cur_exp = cur_exp - upgrade_exp
        cur_level = cur_level + 1
        item = weaponlv_db:find_one(cur_level) or {}
        upgrade_exp = item.exp
        local cfg_attach_id = self:get_attchment_cfgid(cur_level)
        if cfg_attach_id > 0 then
            unlock_ids[cfg_attach_id] = true
        end
    end

    log_debug("[Weapon][add_exp]->weapon upgrade! weapon_id:%s, old_level:%s, old_exp:%s, cur_level:%s, cur_exp:%s",
                self.item_id, self.level, self.cur_exp, cur_level, cur_exp)

    self.level   = cur_level
    self.cur_exp = cur_exp
    if self.level >= GunLevel.MAX then
        self.cur_exp = 0
    end
    return true, unlock_ids
end

return Weapon
