--reward_mgr.lua
local log_err       = logger.err

local CondType      = enum("CondType")
local RewardType    = enum("RewardType")
local RewardStatus  = enum("RewardStatus")
local ResAddType    = enum("ResAddType")
local RewardModule  = enum("RewardModule")

local event_mgr     = quanta.get("event_mgr")
local config_mgr    = quanta.get("config_mgr")
local cond_mgr      = quanta.get("condition_mgr")

local reward_db     = config_mgr:get_table("divisionreward")

-- 绑定赛季奖励模块
local BIND_SEASON_MODULE = {
    [RewardModule.DIVISION] = true,
}

local RewardMgr = singleton()
function RewardMgr:__init()
    self.reward_conditions = {}
end

function RewardMgr:setup()
    -- 根据配置构造条件
    self:build_reward_conditions()

    -- 订阅奖励条件满足回调
    event_mgr:add_trigger(self, "on_reward_condition_satisfy")
end

-- 获取奖励条件
function RewardMgr:get_reward_condition(reward_module, reward_id)
    local conditions = self.reward_conditions[reward_module]
    if conditions then
        return conditions[reward_id]
    end
end

-- 构造奖励条件
function RewardMgr:build_reward_conditions()
    -- 构造段位奖励条件
    self:build_division_reward()
end

-- 构造段位奖励条件
function RewardMgr:build_division_reward()
    local module_id = RewardModule.DIVISION
    if self.reward_conditions[module_id] then
        self.reward_conditions[module_id] = {}
    end

    local division_conditions = self.reward_conditions[module_id]
    for _, data in reward_db:iterator() do
        local conf = {stars = data.stars, win_count = data.win_count}
        local params = { module_id = module_id, reward_id = data.id, }
        division_conditions[data.id] = cond_mgr:create_condition(CondType.CondStarsWinCnt, self, conf, params)
    end
end

-- 订阅奖励条件满足回调
function RewardMgr:on_reward_condition_satisfy(player, belonger, module_id, reward_id)
    if self ~= belonger then
        log_err("[RewardMgr][on_reward_condition_satisfy]->belonger not match!")
        return
    end

    if player:reward_satisfy(module_id, reward_id) then
        local reward_status = {}
        reward_status[reward_id] = RewardStatus.RECIVING
        event_mgr:notify_trigger("nft_player_recive_reward", player, module_id, reward_status)
    end
end

-- 检查奖励是否绑定赛季
function RewardMgr:is_bind_season(module_id)
    return BIND_SEASON_MODULE[module_id]
end

-- 获取奖励内容
function RewardMgr:get_reward_content(module_id, reward_id)
    -- todo:策划配置暂未，需跟策划确认
    -- test data
    local test_data = {}
    test_data[RewardType.ITEMS]  = {
        item_id     = 50003,
        item_count  = 1,
        add_reason = ResAddType.REWARD_DIVISION,
    }
    return test_data
end

-- 获取奖励配置有效期
function RewardMgr:get_reward_life_time(module_id, reward_id)
    -- todo:目前策划未配置奖励有效期，需跟策划确认
    return 0
end

-- export
quanta.reward_mgr = RewardMgr()

return RewardMgr
