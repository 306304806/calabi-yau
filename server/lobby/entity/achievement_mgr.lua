--achievement_mgr.lua
-- 成就管理器
local otime                     = os.time
local tinsert                   = table.insert
local tconcat                   = table.concat

local log_info                  = logger.info
local log_warn                  = logger.warn
local log_err                   = logger.err
local serialize                 = logger.serialize
local check_failed              = utility.check_failed

local plat_api                  = quanta.get("plat_api")
local dlog_mgr                  = quanta.get("dlog_mgr")
local player_mgr                = quanta.get("player_mgr")
local router_mgr                = quanta.get("router_mgr")
local event_mgr                 = quanta.get("event_mgr")
local config_mgr                = quanta.get("config_mgr")

local divisionstar_db           = config_mgr:get_table("divisionstar")
local achievement_db            = config_mgr:get_table("achievement")
local achievement_type_db       = config_mgr:get_table("achievementtype")

local ItemType                  = enum("ItemType")
local MailType                  = enum("MailType")
local AchieveType               = enum("AchieveType")
local AchieveEpicID             = enum("AchieveEpicID")
local AchieveMedalID            = enum("AchieveMedalID")
local AchieveHonourID           = enum("AchieveHonourID")

local Achievement               = import("lobby/entity/achievement/achievement.lua")
local AccHonourAchieve          = import("lobby/entity/achievement/acc_honour_achieve.lua")
local CoverHonourAchieve        = import("lobby/entity/achievement/cover_honour_achieve.lua")
local RoleHonourAchieve         = import("lobby/entity/achievement/role_honour_achieve.lua")
local GunHonourAchieve          = import("lobby/entity/achievement/gun_honour_achieve.lua")

local ACHIEVE_MAP = {
    [AchieveType.MEDAL] = {
        [AchieveMedalID.DAMAGE]               = Achievement,
        [AchieveMedalID.SPARY]                = Achievement,
        [AchieveMedalID.KILL]                 = Achievement,
        [AchieveMedalID.ALIVE]                = Achievement,
        [AchieveMedalID.HIT]                  = Achievement,
        [AchieveMedalID.HIT_HEAD]             = Achievement,
        [AchieveMedalID.CONTINUE_KILL]        = Achievement,
        [AchieveMedalID.FLY]                  = Achievement,
        [AchieveMedalID.SLIDE]                = Achievement,
        [AchieveMedalID.OTHER_DAMAGE]         = Achievement,
        [AchieveMedalID.PLACE_BOMB]           = Achievement,
        [AchieveMedalID.DEFUSE_BOMB]          = Achievement,
    },
    [AchieveType.EPIC] = {
        [AchieveEpicID.BOMB_KILL]             = Achievement,
        [AchieveEpicID.ALL_KILL]              = Achievement,
        [AchieveEpicID.DEFUSE_BOMB]           = Achievement,
        [AchieveEpicID.DEATH_WIN]             = Achievement,
        [AchieveEpicID.TEAM_ACC_KILL]         = Achievement,
        [AchieveEpicID.TEAM_CON_KILL]         = Achievement,
        [AchieveEpicID.TEAM_CON_HIT_HEAD]     = Achievement,
        [AchieveEpicID.TEAM_FIRST_KILL]       = Achievement,
        [AchieveEpicID.RANK_BOMB_KILL]        = Achievement,
        [AchieveEpicID.RANK_BOMB_FLUSH]       = Achievement,
    },
    [AchieveType.HONOUR] = {
        [AchieveHonourID.FRIENDS]            = CoverHonourAchieve,
        [AchieveHonourID.FRIEND_TEAM]        = AccHonourAchieve,
        [AchieveHonourID.LAUD]               = AccHonourAchieve,
        [AchieveHonourID.MVP]                = AccHonourAchieve,
        [AchieveHonourID.ACC_LOGIN]          = AccHonourAchieve,
        [AchieveHonourID.CONTINUE_LOGIN]     = AccHonourAchieve,
        [AchieveHonourID.RANK]               = AccHonourAchieve,
        [AchieveHonourID.ROLE]               = CoverHonourAchieve,
        [AchieveHonourID.SKIN]               = CoverHonourAchieve,
        [AchieveHonourID.GUN]                = CoverHonourAchieve,
        [AchieveHonourID.PROFICIEN]          = RoleHonourAchieve,
        [AchieveHonourID.GUN_PROFICIEN]      = GunHonourAchieve,
        [AchieveHonourID.LEVEL]              = CoverHonourAchieve,
        [AchieveHonourID.SEG]                = CoverHonourAchieve,
        [AchieveHonourID.PLACE_BOMB]         = AccHonourAchieve,
        [AchieveHonourID.DEFUSE_BOMB]        = AccHonourAchieve,
        [AchieveHonourID.DAMAGE]             = AccHonourAchieve,
        [AchieveHonourID.KILL]               = AccHonourAchieve,
        [AchieveHonourID.HIT_HEAD]           = AccHonourAchieve,
        [AchieveHonourID.SPARY]              = AccHonourAchieve,
        [AchieveHonourID.MEDAL]              = AccHonourAchieve,
        [AchieveHonourID.EPIC]               = AccHonourAchieve,
        [AchieveHonourID.HONOUR]             = AccHonourAchieve,
    },
}

local AchievementMgr  = singleton()
local prop = property(AchievementMgr)
prop:accessor("achievements", {})
prop:accessor("indexs", {})
prop:accessor("ranks", {})

function AchievementMgr:__init()
    event_mgr:add_trigger(self, "evt_on_achieve_reach")
    event_mgr:add_trigger(self, "evt_achieve_update")

    event_mgr:add_trigger(self, "evt_player_add_role")
    event_mgr:add_trigger(self, "evt_player_add_role_skin")
    event_mgr:add_trigger(self, "evt_player_level_update")
    event_mgr:add_trigger(self, "evt_qualifying_update")
    event_mgr:add_trigger(self, "evt_create_item")

    event_mgr:add_listener(self, "rpc_achieve_rank_reset")
    event_mgr:add_listener(self, "rpc_battle_with_friend")

    self:setup()
end

-- 初始化所有成就
function AchievementMgr:setup()
    for _, achieve_cfg in achievement_db:iterator() do
        self:create_achieve(achieve_cfg)
    end
end

-- 创建成就
function AchievementMgr:create_achieve(achieve_cfg)
    local Template = ACHIEVE_MAP[achieve_cfg.type] and ACHIEVE_MAP[achieve_cfg.type][achieve_cfg.param1]
    if not Template then
        log_err("[AchievementMgr][create_achieve] Template not found! achieve_cfg=%s", serialize(achieve_cfg))
        return
    end

    self.achievements[achieve_cfg.id] = Template(achieve_cfg)
    local index = tconcat({achieve_cfg.type, achieve_cfg.param1}, "_")
    local ids = self.indexs[index] or {}
    tinsert(ids, achieve_cfg.id)
    self.indexs[index] = ids
end

-- 获取成就排名
function AchievementMgr:get_achieve_rank(achieve_type, achieve_cnt)
    local rank = self.ranks[achieve_type] and self.ranks[achieve_type][achieve_cnt]
    if rank then
        return rank
    end

    local ok, new_rank = router_mgr:call_center_master("rpc_get_achieve_rank", achieve_type, achieve_cnt)
    if not ok then
        return 0
    end

    self.ranks[achieve_type] = self.ranks[achieve_type] or {}
    self.ranks[achieve_type][achieve_cnt] = new_rank
    return new_rank
end


-- 获取玩家的成就数据
function AchievementMgr:get_player_achieves(player_id, achievement_ids)
    log_info("[AchievementMgr][get_player_achieves] player_id=%s, achievement_ids=%s", player_id, serialize(achievement_ids))
    local player = player_mgr:get_player(player_id)
    if player then
        local data = {
            id2achieves = {},
            achieve_cnts = player:get_achieve_cnts()
        }
        local id2achieves = player:get_id2achieves()
        local increase_upadte = achievement_ids and true or false
        if not increase_upadte then
            data.id2achieves = id2achieves
        else
            for k, id in pairs(achievement_ids) do
                data.id2achieves[id] = id2achieves[id]
            end
        end
        return self:pack_achieves(data, increase_upadte)
    end
end

-- 打包成就
function AchievementMgr:pack_achieves(achieve_data, increase_upadte)
    local type2achieves = {}
    for _, achieve in pairs(achieve_data.id2achieves) do
        local cfg = achievement_db:find_one(achieve.id)
        type2achieves[cfg.type] = type2achieves[cfg.type] or {}
        tinsert(type2achieves[cfg.type], achieve)
    end
    local achievement_list = {}
    for t = AchieveType.MEDAL, AchieveType.HONOUR, 1 do
        if not increase_upadte or type2achieves[t] then
            local item = {}
            item.type = t
            local cnt = achieve_data.achieve_cnts[t]
            if cnt and t ~= AchieveType.HONOUR then
                item.rank = cnt > 0 and self:get_achieve_rank(t, cnt) or 0
            end
            item.achievements = type2achieves[t]
            tinsert(achievement_list, item)
        end
    end

    log_info("[AchievementMgr][pack_achieves] achievement_list=%s", serialize(achievement_list))
    return {achievement_list = achievement_list}
end

-- param_type 类型参数(AchieveMedalID,AchieveHonourID..)，必选
-- progress = n, 进度n，可选 0重置
-- no_sync = true 不自动同步给客户端，可选，默认同步客户端
function AchievementMgr:evt_achieve_update(player_id, achieve_type, param_type, progress, no_sync)
    log_info("[AchievementMgr][evt_achieve_update] player_id=%s, achieve_type=%s, param_type = %s, progress = %s, no_sync = %s",
        player_id, achieve_type, param_type, progress, no_sync)
    local player = player_mgr:get_player(player_id)
    if not player then
        log_warn("[AchievementMgr][evt_achieve_update] can not find player=%s", player_id)
        return
    end

    local achieve_ids = {}
    local achievements = self.achievements
    local index = tconcat({achieve_type, param_type}, "_")
    for _, id in pairs(self.indexs[index] or {}) do
        local is_update = achievements[id]:update(player, progress)
        if is_update then
            tinsert(achieve_ids, id)
        end
    end

    if not no_sync and #achieve_ids > 0 then
        event_mgr:notify_trigger("evt_achievement_update", player_id, achieve_ids)
    end

    return achieve_ids
end

-- 请求达成成就
-- achieve_ids 成就id列表
function AchievementMgr:evt_achieve_reach(player_id, achieve_ids)
    log_info("[AchievementMgr][evt_achieve_reach] player_id=%s, achieve_ids=%s", player_id, serialize(achieve_ids))
    local player = player_mgr:get_player(player_id)
    if not player or not achieve_ids then
        log_warn("[AchievementMgr][evt_achieve_reach] ids empty or cannot find player=%s", player_id)
        return
    end

    local achievements = self.achievements
    for _, id in pairs(achieve_ids or {}) do
        if achievements[id] then
            achievements[id]:update(player, 0, true)
        end
    end
end

-- 成就达成响应
function AchievementMgr:evt_on_achieve_reach(player, achievement)
    local achieve_cfg = achievement.achieve_cfg
    local achieve_type = achieve_cfg.type
    --荣耀类型统计
    local player_id = player:get_player_id()
    if achieve_type == AchieveType.MEDAL then
        self:evt_achieve_update(player_id, AchieveType.HONOUR, AchieveHonourID.MEDAL, 1)
    elseif achieve_type == AchieveType.EPIC then
        self:evt_achieve_update(player_id, AchieveType.HONOUR, AchieveHonourID.EPIC, 1)
    elseif achieve_type == AchieveType.HONOUR then
        self:evt_achieve_update(player_id, AchieveType.HONOUR, AchieveHonourID.HONOUR, 1)
    end
    -- 精分上报-玩家成就
    local public_fields = player:build_dlog_public_fields()
    local special_fields={achievement_id=achieve_cfg.id, achievemen_name = achieve_cfg.name, achievement_type = achieve_cfg.type, achievement_status = 1, }
    dlog_mgr:send_dlog_game_finish_achievement({public_fields = public_fields, special_fields = special_fields})

    local cur_level = player:get_achieve_level(achieve_cfg.type)
    local next_level = cur_level + 1
    local achieve_level_cfg = achievement_type_db:find_one(achieve_cfg.type, next_level)
    local achieve_cnt = player:get_achieve_cnt(achieve_cfg.type)
    if not achieve_level_cfg or achieve_level_cfg.need > achieve_cnt then
        return
    end
    player:set_achieve_level(achieve_cfg.type, next_level)
    -- send mail
    local rpc_req = {
        type          = MailType.SYS_MAIL,
        src_player_id = 0,
        tar_player_id = player_id,
        send_time     = otime(),
        title         = achieve_level_cfg.mail_title,
        content       = achieve_level_cfg.mail_content,
        attach_items  = {}
    }
    if achieve_level_cfg.reward then
        for _, item in pairs(achieve_level_cfg.reward) do
            local attach_item = {
                item_id = item.item_id,
                item_count = item.item_amount,
            }
            tinsert(rpc_req.attach_items, attach_item)
        end
        local code, _ = plat_api:send_player_mail(player_id, rpc_req)
        if check_failed(code) then
            log_err("[AchievementMgr][evt_on_achieve_reach] code = %s, player_id = %s, rpc_req = %s", code, player_id, serialize(rpc_req))
        end
    end
end

-- 添加角色事件响应
function AchievementMgr:evt_player_add_role(player)
    local roles_cnt = player:get_roles_cnt()
    self:evt_achieve_update(player:get_player_id(), AchieveType.HONOUR, AchieveHonourID.ROLE, roles_cnt)
end

-- 添加角色皮肤事件响应
function AchievementMgr:evt_player_add_role_skin(player)
    local skins_cnt = player:get_role_skins_cnt()
    self:evt_achieve_update(player:get_player_id(), AchieveType.HONOUR, AchieveHonourID.SKIN, skins_cnt)
end

-- 玩家等级更新事件响应
function AchievementMgr:evt_player_level_update(player, level)
    self:evt_achieve_update(player:get_player_id(), AchieveType.HONOUR, AchieveHonourID.LEVEL, level)
end

-- 排位数据更新事件响应
function AchievementMgr:evt_qualifying_update(player)
    self:evt_achieve_update(player:get_player_id(), AchieveType.HONOUR, AchieveHonourID.RANK, 1)
    local cfg_item = divisionstar_db:find_one(player:get_stars())
    if cfg_item then
        self:evt_achieve_update(player:get_player_id(), AchieveType.HONOUR, AchieveHonourID.SEG, cfg_item.division)
    end
end

-- 增加物品事件
function AchievementMgr:evt_create_item(player, item)
    if item:get_item_type() ~= ItemType.Weapon or not item:is_gun() then
        return
    end

    self:evt_achieve_update(player:get_player_id(), AchieveType.HONOUR, AchieveHonourID.GUN, player:get_guns_count())
end

-- 好友组队战斗通知响应
function AchievementMgr:rpc_battle_with_friend(player_id)
    log_info("[AchievementMgr][rpc_battle_with_friend] player_id = %s", player_id)
    self:evt_achieve_update(player_id, AchieveType.HONOUR, AchieveHonourID.FRIEND_TEAM, 1)
end

-- 成就排行更新重置
function AchievementMgr:rpc_achieve_rank_reset()
    self.ranks = {}
end

quanta.achievement_mgr = AchievementMgr()
return AchievementMgr