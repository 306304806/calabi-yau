--account_mgr.lua
local ljson = require("luacjson")
ljson.encode_sparse_array(true)

local log_info      = logger.info
local log_err       = logger.err
local log_warn      = logger.warn
local tinsert       = table.insert
local check_failed  = utility.check_failed
local json_encode   = ljson.encode

local PeriodTime    = enum("PeriodTime")
local KernCode      = enum("KernCode")
local LobbyCode     = enum("LobbyCode")
local CSCmdID       = ncmd_cs.NCmdId

local dlog_mgr      = quanta.get("dlog_mgr")
local timer_mgr     = quanta.get("timer_mgr")
local token_mgr     = quanta.get("token_mgr")
local client_mgr    = quanta.get("client_mgr")

local AccountMgr = singleton()
local prop = property(AccountMgr)
prop:accessor("oid2account", {})  -- 账号
prop:accessor("aid2account", {})

function AccountMgr:__init()
    -- 处理定时任务
    timer_mgr:loop(PeriodTime.SECOND_MS, function()
        self:on_timer()
    end)
end

--定时器
function AccountMgr:on_timer()
    for open_id, account in pairs(self.oid2account) do
        if account:is_offline_timeout() and (not account:is_playing()) then
            self:logout_account(open_id)
        end
    end
end

-- 根据open_id获取
function AccountMgr:get_account(open_id)
    return self.oid2account[open_id]
end

-- 根据账号id获取账号
function AccountMgr:get_account_by_account_id(account_id)
    return self.aid2account[account_id]
end

-- 移除指定open_id对应的账号
function AccountMgr:remove_account(open_id)
    local account = self.oid2account[open_id]
    if not account then
        return
    end
    log_info("[AccountMgr][remove_account] open_id=%s", open_id)
    self.oid2account[open_id] = nil
    local account_id = account:get_account_id()
    if account_id then
        self.aid2account[account_id] = nil
    end
end

-- 登录指定账号
function AccountMgr:login_account(session, area_id, open_id, account_id, machine_id)
    local account = self:get_account(open_id)
    if account then
        -- 校验是否独占
        if account:is_holding() then
            log_warn("[AccountMgr][login_account] account is busy: open_id=%s, logic_state=%s", open_id, account:get_login_state())
            return LobbyCode.LOBBY_OP_TOO_OFTEN
        end
        local old_session = account:get_session()
        -- 通知旧连接下线
        client_mgr:send_dx(old_session, CSCmdID.NID_KICK_LOBBY_NTF)
        -- 清理旧会话
        old_session.open_id    = nil
        old_session.account_id = nil
        old_session.player_id  = nil
        -- 延迟关闭
        timer_mgr:once(PeriodTime.SECOND_MS, function()
            client_mgr:close_session(old_session)
        end)
        -- 绑定新会话
        account:set_session(session)
    else
        local Account = import("lobby/entity/account.lua")
        account = Account(session, area_id, open_id, account_id)
        self.oid2account[open_id] = account
        self.aid2account[account_id] = account
    end
    -- 设置网络状态
    account:set_online()
    account:set_machine_id(machine_id)
    -- 执行登录
    local ec = account:login()
    if check_failed(ec) then
        self:remove_account(open_id)
        log_err("[AccountMgr][login_account] login error: open_id=%s, ec=%s", open_id, ec)
        return ec
    end
    -- 绑定会话数据
    session.open_id    = open_id
    session.account_id = account_id

    --自动选择player
    local own_players = account:get_own_players()
    if #own_players > 0 then
        local player_data = own_players[1]
        local cur_player_id = player_data.player_id
        log_info("[AccountMgr:login_account] auto select open_id=%s, account_id=%s, player_id=%s", open_id, account_id, cur_player_id)
        local code, player = account:login_player(cur_player_id)
        if check_failed(code) then
            log_err("[AccountMgr][login_account] account:login_player ec=%s", code)
            return code
        end
        -- 精分上报-玩家登陆
        local logout_time = player:get_logout_time()
        local special_fields = {character_name = player:get_nick(), last_logout_time = logout_time, imei_idfa = machine_id, ip = session.ip}
        dlog_mgr:send_dlog_game_character_login({public_fields = player:build_dlog_public_fields(), special_fields = special_fields})
    end
    return ec, account
end

-- 登出指定账号
function AccountMgr:logout_account(open_id)
    local account = self:get_account(open_id)
    if not account then
        return KernCode.LOBBY_ACCOUNT_NOT_EXIST
    end
    -- 精分上报-玩家登出
    local player = account:get_cur_player()
    if player then
        local special_fields = {}
        special_fields.imei_idfa      = account.machine_id
        special_fields.Ip             = account.session.ip
        special_fields.exp            = player:get_exp()
        special_fields.character_name = player:get_nick()
        special_fields.online_time    = quanta.now - player:get_login_time()
        local roles_info = {}
        for _, role in pairs(player:get_role_infos()) do
            tinsert(roles_info, {id = role.role_id, level = role.exp})
        end
        special_fields.character_info    = json_encode(roles_info)
        special_fields.main_money_stock = player:get_ideal()
        special_fields.sub_money_stock  = player:get_crystal()
        dlog_mgr:send_dlog_game_character_logout({public_fields = player:build_dlog_public_fields(), special_fields = special_fields})
    end
    local ec = account:logout()
    self:remove_account(open_id)
    log_info("[AccountMgr][logout_account] finish: open_id=%s, ec=%s", open_id, ec)
    return ec
end

-- 账号重连
function AccountMgr:reconnect_account(open_id, session)
    local account = self:get_account(open_id)
    -- 没有账号或者账号
    if not account then
        return LobbyCode.LOBBY_NEED_RELOGIN
    end
    -- 账号已经在线，需要吧旧会话下线
    if account:is_online() then
        local old_session = account:get_session()
        -- 通知旧连接下线
        client_mgr:send_dx(old_session, CSCmdID.NID_KICK_LOBBY_NTF)
        -- 清理旧会话
        old_session.open_id    = nil
        old_session.account_id = nil
        old_session.player_id  = nil
        -- 延迟关闭
        timer_mgr:once(PeriodTime.SECOND_MS, function()
            client_mgr:close_session(old_session)
        end)
    else
        -- 设置网络状态
        account:set_online()
    end
    -- 绑定新会话
    account:set_session(session)
    -- 绑定会话数据
    session.open_id    = open_id
    session.account_id = account:get_account_id()
    session.player_id  = account:get_selected_player_id()
    token_mgr:update_account_token(session.account_id)
    if session.player_id then
        account:reconnect_selected_player()
    end

    return KernCode.SUCCESS
end

-- export
quanta.account_mgr = AccountMgr()

return AccountMgr