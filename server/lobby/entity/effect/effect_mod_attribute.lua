--effect_mod_attribute.lua
local Effect    = import("lobby/entity/effect/effect.lua")

local log_err       = logger.err
local log_debug     = logger.debug

local ParseParams   = enum("ParseParams")

local config_mgr    = quanta.get("config_mgr")
local effect_helper = quanta.get("effect_helper")

local attribute_db  = config_mgr:get_table("attribute")

--属性变更效果
local EffModAttr = class(Effect)
function EffModAttr:__init()
    --log_debug("EffModAttr")
end

function EffModAttr:test(context)
    --log_debug("[EffModAttr][test]->effect_id:%s", self.id)
    local attr_id, percent_var = effect_helper:parse_effect_parameter(self.id, ParseParams.ATTRIBUTE)
    local attr_cfg = attribute_db:find_one(attr_id)
    if not attr_cfg then
        log_err("[EffModAttr][test] get attr cfg failed! attr_id:%s", attr_id)
        return false
    end

    if type(percent_var) ~= "number" then
        log_err("[EffModAttr][test] param error! percent_var:%s", percent_var)
        return false
    end

    self.attr_id      = attr_id
    self.percent_var  = percent_var

    return true
end

function EffModAttr:run(context)
    local player = context.player
    local old_attr_var = player:get_attribute(self.attr_id)
    local new_attr_var = old_attr_var + self.percent_var
    player:set_attribute(self.attr_id, new_attr_var)
    log_debug("[EffModAttr][run]->attr_id:%s, old_attr_var:%s, new_attr_var:%s", self.attr_id, old_attr_var, new_attr_var)
    return true
end

function EffModAttr:break_run(context)
    local player = context.player
    local old_attr_var = player:get_attribute(self.attr_id)
    local new_attr_var = old_attr_var - self.percent_var
    player:set_attribute(self.attr_id, new_attr_var)
    return true
end

return EffModAttr
