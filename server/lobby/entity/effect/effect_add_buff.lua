--effect_add_buff.lua
local Effect    = import("lobby/entity/effect/effect.lua")

local log_err       = logger.err

local ParseParams   = enum("ParseParams")

local config_mgr    = quanta.get("config_mgr")
local effect_helper = quanta.get("effect_helper")

local buff_db       = config_mgr:get_table("buff")

local EffAddBuff = class(Effect)

function EffAddBuff:__init()
end

function EffAddBuff:test(context)
    local buff_id = effect_helper:parse_effect_parameter(self.id, ParseParams.BUFF)
    local buff_cfg = buff_db:find_one(buff_id)
    if not buff_cfg then
        log_err("[EffAddBuff][test] get buff cfg failed! buff_id:%s", buff_id)
        return false
    end

    self.buff_id = buff_id
    return true
end

function EffAddBuff:run(context)
    local player = context.player
    player:add_buff(self.buff_id)
    return true
end

return EffAddBuff
