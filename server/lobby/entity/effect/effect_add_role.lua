--effect_add_role.lua
local Effect    = import("lobby/entity/effect/effect.lua")


local ParseParams   = enum("ParseParams")


local effect_helper = quanta.get("effect_helper")

--添加Role效果
local EffAddRole = class(Effect)
function EffAddRole:__init()
    --log_debug("EffAddRole")
end

function EffAddRole:test(context)
    self.role_id, self.life_time = effect_helper:parse_effect_parameter(self.id, ParseParams.ROLE)

    return true
end

function EffAddRole:run(context)
    if self.role_id then
        local add_cmd = {
            role_id     = self.role_id,
            life_time   = self.life_time,
            reason      = context.add_reason,
        }
        local ok, result = context.player:add_role(add_cmd)
        if ok then
            if result then
                context.sync_list = {result}
            end
            return true
        else
            return false
        end
    end

    return false
end

return EffAddRole
