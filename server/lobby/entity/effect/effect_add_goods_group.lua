--effect_add_goods_group.lua
local Effect    = import("lobby/entity/effect/effect.lua")

local mrandom       = math.random
local tinsert       = table.insert
local log_err       = logger.err
local log_debug     = logger.debug
local serialize     = logger.serialize

local ResAddType    = enum("ResAddType")
local ParseParams   = enum("ParseParams")

local config_mgr    = quanta.get("config_mgr")
local effect_helper = quanta.get("effect_helper")

local drop_db       = config_mgr:get_table("drop")

local EffAddGoodsGroup = class(Effect)
function EffAddGoodsGroup:__init()
    log_debug("EffAddGoodsGroup")
end

function EffAddGoodsGroup:test(context)
    self.drop_group_id, self.count, self.prob  = effect_helper:parse_effect_parameter(self.id, ParseParams.GOODSGROUP)
    if not self.drop_group_id or not self.count or not self.prob then
        log_err("[EffAddGoodsGroup][test]->param error!")
        return false
    end

    log_debug("[EffAddGoodsGroup][test]->effect_id:%s, drop_group_id:%s, count:%s, prob:%s", self.id, self.drop_group_id, self.count, self.prob)
    return true
end

function EffAddGoodsGroup:run(context)
    local drop_group = drop_db:select({group_id = self.drop_group_id})
    if not drop_group or not next(drop_group) then
        log_err("[EffAddGoodsGroup][run]->get drop group cfg failed! effect_id:%s", self.id)
        return false
    end

    local total_weight = nil
    local add_items = {}
    for idx = 1, self.count do
        --if mrandom(1, 10000) <= self.prob then
            if not total_weight then
                total_weight = 0
                for idx1 = 1, #drop_group do
                    total_weight = total_weight + drop_group[idx1].weight
                end
            end

            local rand_weight = mrandom(1, total_weight)
            for idx2 = 1, #drop_group do
                local info = drop_group[idx2]
                if rand_weight <= info.weight then
                    for _, drop_item in pairs(info.items) do
                        tinsert(add_items, {item_id = drop_item.item_id, num = drop_item.item_amount, add_reason = ResAddType.UES_BAG_ITEM})
                    end
                    break
                end

                rand_weight = rand_weight - info.weight
            end
        --end
    end

    log_debug("[EffAddGoodsGroup][run]->effect_id:%s, add_items:%s", self.id, serialize(add_items))

    local ret, result_list = context.player:add_items_no_sync(add_items)
    if not ret then
        return false
    end

    log_debug("[EffAddGoodsGroup][run]->effect_id:%s, result_list:%s", self.id, serialize(result_list))
    context.sync_list = result_list
    return true
end

return EffAddGoodsGroup
