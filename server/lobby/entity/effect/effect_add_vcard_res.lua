--effect_add_skin.lua
local Effect    = import("lobby/entity/effect/effect.lua")

local ParseParams   = enum("ParseParams")

local config_mgr    = quanta.get("config_mgr")
local effect_helper = quanta.get("effect_helper")

local idcard_db     = config_mgr:get_table("idcard")

--添加皮肤效果
local EffAddVCardRes = class(Effect)
function EffAddVCardRes:__init()
end

function EffAddVCardRes:test(context)
    local resource_id, life_time = effect_helper:parse_effect_parameter(self.id, ParseParams.VCARD_FRAME)
    self.resource_id    = resource_id
    self.life_time      = life_time and life_time or 0

    -- 验证头像框是否存在
    if not idcard_db:find_one(self.resource_id) then
        return false
    end

    return true
end

function EffAddVCardRes:run(context)
    -- 新增一个名片框
    local resources = {
        { resource_id = self.resource_id, life_time   = self.life_time, }
    }
    local ok, result = context.player:add_vcard_resources(resources)
    if ok then
        if result then
            context.sync_list = {result}
        end
        return true
    else
        return false
    end
end

return EffAddVCardRes