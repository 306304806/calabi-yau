--effect_add_role_action.lua
local Effect    = import("lobby/entity/effect/effect.lua")

local ParseParams   = enum("ParseParams")

local config_mgr    = quanta.get("config_mgr")
local effect_helper = quanta.get("effect_helper")

local roleaction_db = config_mgr:get_table("roleaction")

--添加Role效果
local EffAddRoleAction = class(Effect)
function EffAddRoleAction:__init()
    --log_debug("EffAddRoleAction")
end

function EffAddRoleAction:test(context)
    self.action_id = effect_helper:parse_effect_parameter(self.id, ParseParams.ROLEACTION)

    return true
end

function EffAddRoleAction:run(context)
    local cfg = roleaction_db:find_one(self.action_id)
    if not cfg then
        return false
    end

    local ok, result = context.player:add_role_action(cfg.role_id, self.action_id)
    if ok then
        if result then
            context.sync_list = {result}
        end
        return true
    else
        return false
    end
end

return EffAddRoleAction
