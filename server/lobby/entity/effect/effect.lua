-- effect.lua

local log_err           = logger.err
local config_mgr        = quanta.get("config_mgr")

local effect_db         = config_mgr:get_table("effect")

local Effect = class()
local prop = property(Effect)
prop:accessor("id", 0)              --效果id
prop:accessor("start", false)       --启动标记

function Effect:__init(id)
    self.id         = id
    self.start      = false
end

-- 检查效果能否作用
function Effect:test(context)
    return true
end

-- 启动效果
function Effect:run(context)
    return true
end

-- 效果完成
function Effect:finish()

end

-- 中断效果
function Effect:break_run()

end

-- 是否持续效果
function Effect:is_continuous()
    local eff_cfg = effect_db:find_one(self.id)
    if not eff_cfg then
        log_err("[EffectMgr][create_effect] get effect config failed! effect_id:%s", self.id)
        return false
    end

    return eff_cfg.continuous
end

return Effect
