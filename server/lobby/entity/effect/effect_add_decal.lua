--effect_add_decal.lua
local Effect    = import("lobby/entity/effect/effect.lua")

local otime         = os.time
local log_err       = logger.err
--local log_debug     = logger.debug

local ParseParams   = enum("ParseParams")

local config_mgr    = quanta.get("config_mgr")
local effect_helper = quanta.get("effect_helper")

local decal_db      = config_mgr:get_table("decal")

--添加贴画效果
local EffAddDecal = class(Effect)
function EffAddDecal:__init()
    --log_debug("EffAddDecal")
end

function EffAddDecal:test(context)
    --log_debug("[EffAddDecal][test]->effect_id:%s", self.id)
    local decal_id, time = effect_helper:parse_effect_parameter(self.id, ParseParams.DECAL)
    local decal_cfg = decal_db:find_one(decal_id)
    if not decal_cfg then
        log_err("[EffAddDecal][test] get stick cfg failed! decal_id:%s", decal_id)
        return false
    end

    if not time or time < 0 then
        log_err("[EffAddDecal][test] param error! time:%s", time)
        return false
    end

    self.decal_id  = decal_id
    self.time       = time

    return true
end

function EffAddDecal:run(context)
    local expire_time = 0
    if self.time > 0 then
        expire_time = otime() + self.time
    end
    local ret, result_list = context.player:add_item_no_sync(self.decal_id, 1, context.add_reason, expire_time)
    if not ret then
        return false
    end

    context.sync_list = result_list
    return true
end

return EffAddDecal