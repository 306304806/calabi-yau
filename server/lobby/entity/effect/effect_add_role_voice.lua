--effect_add_role_voice.lua
local Effect    = import("lobby/entity/effect/effect.lua")

local config_mgr    = quanta.get("config_mgr")
local effect_helper = quanta.get("effect_helper")

local ParseParams   = enum("ParseParams")

local rolevoice_db  = config_mgr:get_table("rolevoice")

--添加Role效果
local EffAddRoleVoice = class(Effect)
function EffAddRoleVoice:__init()
    --log_debug("EffAddRoleVoice")
end

function EffAddRoleVoice:test(context)
    self.voice_id = effect_helper:parse_effect_parameter(self.id, ParseParams.ROLEVOICE)

    return true
end

function EffAddRoleVoice:run(context)
    local cfg = rolevoice_db:find_one(self.voice_id)
    if not cfg then
        return false
    end

    local ok, result = context.player:add_role_voice(cfg.role_id, self.voice_id)
    if ok then
        if result then
            context.sync_list = {result}
        end
        return true
    else
        return false
    end
end

return EffAddRoleVoice
