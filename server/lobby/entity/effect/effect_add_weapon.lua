--effect_add_weapon.lua
local Effect    = import("lobby/entity/effect/effect.lua")

local otime         = os.time
local log_err       = logger.err
--local log_debug     = logger.debug

local ParseParams   = enum("ParseParams")


local config_mgr    = quanta.get("config_mgr")
local effect_helper = quanta.get("effect_helper")

local weapon_db     = config_mgr:get_table("weapon")

--添加枪械效果
local EffAddWeapon = class(Effect)
function EffAddWeapon:__init()
end

function EffAddWeapon:test(context)
    local weapon_id, time = effect_helper:parse_effect_parameter(self.id, ParseParams.WEAPON)
    local weapon_cfg = weapon_db:find_one(weapon_id)
    if not weapon_cfg then
        log_err("[EffAddWeapon][test] get weapon cfg failed! weapon_id:%s", weapon_id)
        return false
    end

    if not time then
        log_err("[EffAddWeapon][test] param error! time:%s", time)
        return false
    end

    self.weapon_id  = weapon_id
    self.time       = time

    return true
end

function EffAddWeapon:run(context)
    local expire_time = 0
    if self.time > 0 then
        expire_time = otime() + self.time
    end
    local ret, result_list = context.player:add_item_no_sync(self.weapon_id, 1, context.add_reason, expire_time)
    if not ret then
        return false
    end

    --log_debug("[EffAddWeapon][run]->effect_id:%s", self.id)
    context.sync_list = result_list
    return true
end

return EffAddWeapon
