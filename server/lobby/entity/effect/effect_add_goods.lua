--effect_add_goods.lua
local Effect    = import("lobby/entity/effect/effect.lua")

local log_err       = logger.err
local log_debug     = logger.debug
local serialize     = logger.serialize
local tinsert       = table.insert

local ParseParams   = enum("ParseParams")

local config_mgr    = quanta.get("config_mgr")
local effect_helper = quanta.get("effect_helper")

local item_db       = config_mgr:get_table("item")

local EffAddGoods = class(Effect)
function EffAddGoods:__init()
    log_debug("EffAddGoods")
end

function EffAddGoods:test(context)
    self.add_item_list = effect_helper:parse_effect_parameter(self.id, ParseParams.GOODS)
    log_debug("[EffAddGoods][test]->effect_id:%s, add_item_list:%s", self.id, serialize(self.add_item_list))
    for _, data in ipairs(self.add_item_list or {}) do
        local item_id  = data[1]
        local item_num = data[2]
        local item_cfg = item_db:find_one(item_id)
        if not item_cfg then
            log_err("[EffAddGoods][test]->get item cfg failed! item_id:%s", item_id)
            return false
        end

        if not item_num or item_num <= 0 then
            log_err("[EffAddGoods][test]->num error! effect_id:%s, item_id:%s, num:%s", self.id, item_id, item_num)
            return false
        end
    end


    return true
end

function EffAddGoods:run(context)
    local add_items_data = {}
    for _, data in ipairs(self.add_item_list) do
        tinsert(add_items_data, {item_id = data[1], num = data[2], add_reason = context.add_reason,})
    end

    local ret, result_list = context.player:add_items_no_sync(add_items_data)
    if not ret then
        log_err("[EffAddGoods][run]->run failed! effect_id:%s", self.id)
        return false
    end

    log_debug("[EffAddGoods][run]->effect_id:%s, result_list:%s", self.id, serialize(result_list))
    context.sync_list = result_list
    return true
end

return EffAddGoods
