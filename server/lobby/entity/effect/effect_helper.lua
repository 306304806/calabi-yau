-- effect_helper.lua

local log_err       = logger.err

local EffectCode    = enum("EffectCode")
local ParseParams   = enum("ParseParams")

local config_mgr    = quanta.get("config_mgr")
local effect_db     = config_mgr:get_table("effect")

local EffectHelper = singleton()

function EffectHelper:__init()
end

-- 获取item效果参数解析数据
function EffectHelper:parse_effect_parameter(effect_id, parse_ty)
    local effect_cfg = effect_db:find_one(effect_id)
    if not effect_cfg then
        log_err("[EffectHelper][parse_effect_parameter] get effect cfg failed! effect_id:%s", self.id)
        return EffectCode.EFFECT_ERR_CFG_NOT_EXIST
    end

    if type(effect_cfg.parameter) ~= "table" then
        log_err("[EffectHelper][parse_effect_parameter] get effect cfg parameter failed! effect_id:%s", self.id)
        return EffectCode.EFFECT_ERR_CFG_NOT_EXIST
    end

    local parse_func_1 = function(params)
        local cfg_id, time = params[1], params[2]
        if time == -1 then
            time = 0
        end
        cfg_id, time = cfg_id, time
        return cfg_id, time
    end

    --[[ 暂时屏蔽  by jack
    local parse_func_2 = function(params)
        local item_id, num = params[1], params[2]
        return item_id, num
    end
    ]]

    local parse_func_3 = function(params)
        local drop_group_id, count, prob  = params[1], params[2], params[3]
        return drop_group_id, count, prob
    end

    local parse_func_4 = function(params)
        local buff_id = params[1]
        return buff_id
    end

    local parse_func_5 = function(params)
        local attr_id, percent = params[1], params[2]
        return attr_id, percent
    end

    local parse_func_6 = function(params)
        local currency_id, currency_num = params[1], params[2]
        return currency_id, currency_num
    end

    local parse_func_7 = function(params)
        local items_list = params
        return items_list
    end

    local t_parse_func = {
        [ParseParams.ROLE]             = parse_func_1,
        [ParseParams.SKIN]             = parse_func_1,
        [ParseParams.WEAPON]           = parse_func_1,
        [ParseParams.DECAL]            = parse_func_1,
        [ParseParams.GOODS]            = parse_func_7,
        [ParseParams.GOODSGROUP]       = parse_func_3,
        [ParseParams.VCARD_FRAME]      = parse_func_1,
        [ParseParams.BUFF]             = parse_func_4,
        [ParseParams.ATTRIBUTE]        = parse_func_5,
        [ParseParams.CURRENCY]         = parse_func_6,
        [ParseParams.ROLEACTION]       = parse_func_1,
        [ParseParams.ROLEVOICE]        = parse_func_1,
    }

    return t_parse_func[parse_ty](effect_cfg.parameter)
end

quanta.effect_helper = EffectHelper
return EffectHelper
