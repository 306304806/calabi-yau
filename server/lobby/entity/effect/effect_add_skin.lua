--effect_add_skin.lua
local Effect    = import("lobby/entity/effect/effect.lua")

local ParseParams   = enum("ParseParams")

local effect_helper = quanta.get("effect_helper")

--添加皮肤效果
local EffAddRoleSkin = class(Effect)
function EffAddRoleSkin:__init()
    --log_debug("EffAddRoleSkin")
end

function EffAddRoleSkin:test(context)
    self.skin_id, self.life_time = effect_helper:parse_effect_parameter(self.id, ParseParams.SKIN)

    return true
end

function EffAddRoleSkin:run(context)
    local add_cmd = {
        role_skin_id = self.skin_id,
        life_time    = self.life_time,
        reason       = context.add_reason,
    }

    local ok, result = context.player:add_role_skin(add_cmd)
    if ok then
        if result then
            context.sync_list = {result}
        end
        return true
    else
        return false
    end
end

return EffAddRoleSkin