--effect_add_currency.lua
local Effect    = import("lobby/entity/effect/effect.lua")

local ParseParams   = enum("ParseParams")


local effect_helper = quanta.get("effect_helper")

--添加Role效果
local EffAddCurrency = class(Effect)
function EffAddCurrency:__init()
    --log_debug("EffAddCurrency")
end

function EffAddCurrency:test(context)
    self.currency_id, self.currency_num = effect_helper:parse_effect_parameter(self.id, ParseParams.CURRENCY)

    return true
end

function EffAddCurrency:run(context)
    if context.player:add_currency_item(self.currency_id, self.currency_num) then
        return true
    else
        return false
    end
end

return EffAddCurrency
