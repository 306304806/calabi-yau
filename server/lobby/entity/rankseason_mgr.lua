--rankseason_mgr.lua

local RankSeasonMgr = singleton()
local prop = property(RankSeasonMgr)
prop:accessor("season_id", 0)
prop:accessor("start_time", 0)
prop:accessor("finish_time", 0)

function RankSeasonMgr:__init()
end

-- 设置赛季时间信息
function RankSeasonMgr:set_season_info(season_id, start_time, finish_time)
    self.season_id = season_id
    self.start_time  = start_time
    self.finish_time = finish_time
end

-- export
quanta.rankseason_mgr = RankSeasonMgr()

return RankSeasonMgr
