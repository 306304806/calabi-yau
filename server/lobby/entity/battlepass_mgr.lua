-- battlepass_mgr.lua
local BattlePassTask    = import("lobby/entity/battlepass_task.lua")

local otime             = os.time
local mmin              = math.min
local mceil             = math.ceil
local tinsert           = table.insert
local log_info          = logger.info
local trandom_array     = table_ext.random_array

local config_mgr        = quanta.get("config_mgr")

local utility_db        = config_mgr:get_table("utility")
local battlepassclue_db = config_mgr:get_table("battlepassclue")
local battlepassprize_db= config_mgr:get_table("battlepassprize")
local bpass_dtask_db    = config_mgr:get_table("battlepassdaytask")
local bpass_wtask_db    = config_mgr:get_table("battlepassweektask")
local bpass_ltask_db    = config_mgr:get_table("battlepasslooptask")

local WEEK_SECOND       = 60 * 60 * 24 * 7
local DAY_FLUSH_TIME    = utility_db:find_value("value", "day_flush_time")
local DAY_TASK_COUNT    = utility_db:find_value("value", "battle_pass_day_task_cnt")
local LOOP_TASK_COUNT   = utility_db:find_value("value", "battle_pass_loop_task_cnt")
local WEEK_TASK_COUNT   = utility_db:find_value("value", "battle_pass_week_task_cnt")

local BattlepassMgr = singleton()
local prop = property(BattlepassMgr)
prop:accessor("season_id", 0)
prop:accessor("start_time", 0)
prop:accessor("finish_time", 0)
prop:accessor("rewards", {})
prop:accessor("lv2explore", {})
prop:accessor("cluerewards", {})
prop:accessor("day_tasks", {})
prop:accessor("week_tasks", {})
prop:accessor("loop_tasks", {})
prop:accessor("task_behaviors", {})  -- task_id-behavior_id-cfg{op_type,cmp_type}  方便内存索引的任务行为配置
function BattlepassMgr:__init()
end

-- 获取任务行为列表
function BattlepassMgr:get_task_behaviors(task_id)
    return self.task_behaviors[task_id]
end

-- 根据探索点数获取battlepass等级
function BattlepassMgr:get_battlepass_level(player)
    local explore = player:get_explore() or 0
    for level, cfg_explore in ipairs(self.lv2explore) do
        if explore < cfg_explore then
            return level - 1
        end
    end
    return (explore > 0) and #self.lv2explore or 1
end

-- 设置battlepass赛季id
function BattlepassMgr:load_battlepass_config(season_id)
    self.cluerewards = {}
    for _, cfg in battlepassclue_db:iterator() do
        if cfg.season == season_id then
            self.cluerewards[cfg.clue_id] = cfg
        end
    end
    self.rewards = {}
    self.lv2explore = {}
    for _, cfg in battlepassprize_db:iterator() do
        if cfg.season == season_id then
            self.rewards[cfg.id] = cfg
            -- battlepass等级映射探索点数
            self.lv2explore[cfg.id] = cfg.explore
        end
    end
end

-- 刷新加载日任务
function BattlepassMgr:load_day_task_config()
    self.day_tasks = {}
    for _, tcfg in bpass_dtask_db:iterator() do
        tinsert(self.day_tasks, tcfg)
        local behaviors = {}
        for _, bcfg in pairs(tcfg.conditions or {}) do
            behaviors[bcfg.behavior_id] = bcfg
        end
        self.task_behaviors[tcfg.id] = behaviors
    end
    log_info("[BattlepassMgr][load_day_task_config] finish: day task count=%s", #self.day_tasks)
end

-- 重新加载周任务
function BattlepassMgr:load_week_task_config()
    self.week_tasks = {}
    for _, tcfg in bpass_wtask_db:iterator() do
        local week = tcfg.week
        local week_tasks = self.week_tasks[week]
        if not week_tasks then
            week_tasks = {}
        end
        tinsert(week_tasks, tcfg)
        local behaviors = {}
        for _, bcfg in pairs(tcfg.conditions or {}) do
            behaviors[bcfg.behavior_id] = bcfg
        end
        self.task_behaviors[tcfg.id] = behaviors
        self.week_tasks[week] = week_tasks
    end
end

-- 重新加载循环任务
function BattlepassMgr:load_loop_task_config()
    self.loop_tasks = {}
    for _, tcfg in bpass_ltask_db:iterator() do
        tinsert(self.loop_tasks, tcfg)
        local behaviors = {}
        for _, bcfg in pairs(tcfg.conditions or {}) do
            behaviors[bcfg.behavior_id] = bcfg
        end
        self.task_behaviors[tcfg.id] = behaviors
    end
    log_info("[BattlepassMgr][load_loop_task_config] finish: loop task count=%s", #self.loop_tasks)
end

-- 设置赛季时间信息
function BattlepassMgr:set_season_info(season_id, start_time, finish_time)
    self.season_id = season_id
    self.start_time = start_time
    self.finish_time = finish_time
    self:load_battlepass_config(season_id)
    self:load_day_task_config()
    self:load_week_task_config()
    self:load_loop_task_config()
end

-- 获取赛季时间信息
function BattlepassMgr:get_season_info()
    return {
        day_flush_time     = DAY_FLUSH_TIME,
        season_id          = self.season_id,
        season_start_time  = self.start_time,
        season_finish_time = self.finish_time,
        week_cnt           = self:get_battlepass_week()
    }
end

--获取赛季
function BattlepassMgr:get_battlepass_week()
    return mceil((self.finish_time - self.start_time) / WEEK_SECOND)
end

--获取当前处于那个赛季id
function BattlepassMgr:get_current_season_week_id()
    local start_sec = otime() - self.start_time
    if start_sec > 0 then
        return mmin(mceil(start_sec / WEEK_SECOND), self:get_battlepass_week())
    end
    return 1
end

-- 获取线索版配置信息
function BattlepassMgr:get_clueboard_config(clue_id)
    return self.cluerewards[clue_id]
end

-- 获取线索版配置信息
function BattlepassMgr:get_reward_config(level, vip)
    local config = self.rewards[level]
    if config then
        return vip and config.prize2 or config.prize1
    end
end

-- 获取线索版赛季未领奖励列表
function BattlepassMgr:get_clue_season_prize(player, old_season)
    local attach_items = {}
    local battlepass_lv = self:get_battlepass_level(player)
    for _, cfg in battlepassclue_db:iterator() do
        -- battlepass线索版等级满足但玩家未领取
        if cfg.season == old_season and battlepass_lv >= cfg.unlock_level and not player:clueboard_received(cfg.clue_id) then
            for _, data in pairs(cfg.prize1) do
                tinsert(attach_items, {item_id = data.item_id, item_count = data.item_amount})
            end
        end
    end
    return attach_items
end

-- 获取进程赛季未领奖励列表
function BattlepassMgr:get_battlepass_season_prize(player, old_season)
    local attach_items = {}
    local battlepass_lv = self:get_battlepass_level(player)
    for _, cfg in battlepassprize_db:iterator() do
        -- battlepass进程等级满足但玩家未领取
        if cfg.season == old_season and battlepass_lv >= cfg.id then
            if not player:battlepsaa_received(cfg.id) then
                for _, data in pairs(cfg.prize1) do
                    tinsert(attach_items, {item_id = data.item_id, item_count = data.item_amount})
                end
            end
            if player:get_battlepass_vip() and not player:battlepsaa_received(cfg.id, true) then
                for _, data in pairs(cfg.prize2) do
                    tinsert(attach_items, {item_id = data.item_id, item_count = data.item_amount})
                end
            end
        end
    end
    return attach_items
end

function BattlepassMgr:gen_day_task(player)
    local tpoll_cnt = #self.day_tasks
    for index = 1, tpoll_cnt do
        local cfg = trandom_array(self.day_tasks)
        if cfg and not player:get_task(cfg.id) then
            local task = BattlePassTask()
            task:reset_from_cfg(cfg)
            return task
        end
    end
end

-- 生成n个每日任务
function BattlepassMgr:gen_day_tasks()
    local tasks = {}
    local cnt = DAY_TASK_COUNT
    local tpoll_cnt = #(self.day_tasks)
    if cnt > tpoll_cnt then
        cnt = tpoll_cnt
    end
    for index = 1, tpoll_cnt do
        if cnt <= 0 then
            break
        end
        local cfg = trandom_array(self.day_tasks)
        if cfg and not tasks[cfg.id] then
            local task = BattlePassTask()
            task:reset_from_cfg(cfg)
            tasks[cfg.id] = task
            cnt = cnt - 1
        end
    end
    return tasks
end

-- 生成每周任务
function BattlepassMgr:gen_week_task(tasks, week_id)
    local cnt = WEEK_TASK_COUNT
    local week_tasks = self.week_tasks[week_id] or {}
    local tpoll_cnt = #(week_tasks)
    if cnt > tpoll_cnt then
        cnt = tpoll_cnt
    end
    for index = 1, tpoll_cnt do
        if cnt <= 0 then
            break
        end
        local cfg = trandom_array(week_tasks)
        if cfg and not tasks[cfg.id] then
            local task = BattlePassTask()
            task:reset_from_cfg(cfg)
            tasks[cfg.id] = task
            cnt = cnt - 1
        end
    end
end

-- 生成第x周的y个每周任务
function BattlepassMgr:gen_week_tasks()
    local tasks = {}
    local week_cnt = self:get_battlepass_week()
    for week_id = 1, week_cnt do
        self:gen_week_task(tasks, week_id)
    end
    return tasks
end

-- 生成n个循环任务
function BattlepassMgr:gen_loop_tasks()
    local tasks = {}
    local cnt = LOOP_TASK_COUNT
    local tpoll_cnt = #(self.loop_tasks)
    if cnt > tpoll_cnt then
        cnt = tpoll_cnt
    end
    for index = 1, tpoll_cnt do
        if #tasks >= cnt then
            break
        end
        local cfg = trandom_array(self.loop_tasks)
        if cfg and not tasks[cfg.id] then
            local task = BattlePassTask()
            task:reset_from_cfg(cfg)
            tasks[cfg.id] = task
        end
    end
    return tasks
end

-- export
quanta.battlepass_mgr = BattlepassMgr()

return BattlepassMgr
