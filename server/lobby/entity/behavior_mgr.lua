
-- behavior_mgr.lua

local log_debug     = logger.debug
local log_err       = logger.err
local log_warn      = logger.warn

local event_mgr     = quanta.get("event_mgr")
local player_mgr    = quanta.get("player_mgr")
local config_mgr    = quanta.get("config_mgr")

local behavior_attr_db = config_mgr:get_table("behaviorattribute")


local BehaviorMgr = singleton()

function BehaviorMgr:__init()
    self._subscribes = {}

    self:setup()
end

function BehaviorMgr:setup()
    event_mgr:add_trigger(self, "ntf_behavior_event")
    event_mgr:add_trigger(self, "behavior_add_subscribe")
    event_mgr:add_trigger(self, "behavior_remove_subscribe")
end

-- 添加监听
function BehaviorMgr:behavior_add_subscribe(player_id, behavior_id, obj)
    self:add_subscribe(player_id, behavior_id, obj)
end

-- 移除监听
function BehaviorMgr:behavior_remove_subscribe(player_id, behavior_id, obj)
    self:remove_subscribe(player_id, behavior_id, obj)
end

-- 添加订阅
function BehaviorMgr:add_subscribe(player_id, behavior_id, obj)
    if not obj.evt_behavior_update then
        log_err("[BehaviorMgr][add_subscribe] no event handler: player_id:%s, behavior_id:%s", player_id, behavior_id)
        return
    end
    if not self._subscribes[player_id] then
        self._subscribes[player_id] = {}
    end
    if not self._subscribes[player_id][behavior_id] then
        self._subscribes[player_id][behavior_id] = {}
    end
    if self._subscribes[player_id][behavior_id][obj] then
        log_err("[BehaviorMgr][add_subscribe] player_id:%s, behavior_id=%s, object already subscribed", player_id, behavior_id)
        return
    end
    self._subscribes[player_id][behavior_id][obj] = true
end

-- 移除订阅
function BehaviorMgr:remove_subscribe(player_id, behavior_id, obj)
    if not self._subscribes[player_id] or
        not self._subscribes[player_id][behavior_id] or
        not self._subscribes[player_id][behavior_id][obj] then
        return
    end
    self._subscribes[player_id][behavior_id][obj] = nil
end

-- 行为变化通知
function BehaviorMgr:behavior_nofity(player, behavior_id, value)
    local player_id = player.player_id
    local player_subscribe = self._subscribes[player_id]
    if not player_subscribe then
        return
    end
    local objs = player_subscribe[behavior_id]
    if not objs then
        return
    end
    if not behavior_attr_db:find_one(behavior_id) then
        log_warn("[BehaviorMgr][behavior_nofity] behavior_id=%s not configed", behavior_id)
        return
    end
    for obj, _ in pairs(objs) do
        if obj.evt_behavior_update then
            obj:evt_behavior_update(player, behavior_id, value)
        end
    end
end

-- 订阅行为事件通知处理
function BehaviorMgr:ntf_behavior_event(player_id, behavior_id, value)
    local player = player_mgr:get_player(player_id)
    if not player then
        log_err("[BehaviorMgr][ntf_behavior_event]->get player failed! player_id:%s", player_id)
        return
    end
    log_debug("[BehaviorMgr][ntf_behavior_event]player_id:%s, behavior_id:%s, value:%s", player_id, behavior_id, value)
    player:set_behavior_attr(behavior_id, value)
    self:behavior_nofity(player, behavior_id, value)
end

-- export
quanta.behavior_mgr = BehaviorMgr()

return BehaviorMgr