--account.lua
local new_guid          = guid.new
local guid_string       = guid.string
local tinsert           = table.insert
local log_err           = logger.err
local log_dump          = logger.dump
local log_info          = logger.info
local serialize         = logger.serialize
local check_failed      = utility.check_failed

local SexType           = enum("SexType")
local RoomType          = enum("RoomType")
local KernCode          = enum("KernCode")
local LoginStatus       = enum("LoginStatus")

local plat_api          = quanta.get("plat_api")
local event_mgr         = quanta.get("event_mgr")
local config_mgr        = quanta.get("config_mgr")
local player_mgr        = quanta.get("player_mgr")
local router_mgr        = quanta.get("router_mgr")
local account_dao       = quanta.get("account_dao")

local utility_db        = config_mgr:get_table("utility")

-- 掉线超时时长
local OFFLINE_TIMEOUT   = utility_db:find_value("value", "offline_timeout")

-- 账号实体类
local Account = class()
local prop = property(Account)
prop:accessor("login_state", LoginStatus.WAITING)   -- 账号逻辑状态
prop:accessor("offline_time", 0)                    -- 离线时间
prop:accessor("session", nil)
prop:accessor("open_id", nil)
prop:accessor("account_id", nil)
prop:accessor("area_id", nil)
prop:accessor("platform_token", "")
prop:accessor("own_players", {})
prop:accessor("cur_player", nil)                    -- 当前选中的player
prop:accessor("machine_id", "")                     -- 账号登陆MAC地址

function Account:__init(session, area_id, open_id, account_id)
    self.session        = session
    self.open_id        = open_id
    self.area_id        = area_id
    self.account_id     = account_id
end

-- 登录
function Account:login()
    -- 设置账号加载中,开始加载事务
    self.login_state = LoginStatus.LOGINING
    local open_id, area_id = self.open_id, self.area_id
    -- 加载账号信息
    log_info("[Account][login] open_id=%s", open_id)
    local ok, acc_info = account_dao:load_account(open_id)
    if not ok then
        log_err("[Account][login] load_account_info failed: open_id=%s", open_id)
        self.login_state = LoginStatus.WAITING
        return KernCode.MONGO_FAILED
    end
    if not acc_info then
        acc_info = {
            open_id     = open_id,
            area_id     = area_id,
            account_id  = self.account_id,
            players     = {}
        }
        -- 对应的open_id没有账号需要立即新建并落库
        if not account_dao:update_account(area_id, open_id, acc_info) then
            log_err("[Account][login] update_account failed: open_id=%s", open_id)
            self.login_state = LoginStatus.WAITING
            return KernCode.MONGO_FAILED
        end
    end
    self.own_players = acc_info.players
    self.login_state = LoginStatus.WAITING
    self.platform_token = guid_string()
    -- 加载事务完成，等待玩家操作
    log_info("[Account][login] finish: open_id=%s", self.open_id)
    return KernCode.SUCCESS
end

-- 登出
function Account:logout()
    -- 设置账号退出中，开始退出事务
    self.login_state = LoginStatus.LOGOUTING
    -- 登出选中的角色
    local open_id = self.open_id
    local ec = self:logout_player()
    if check_failed(ec) then
        log_err("[Account][logout] logout_player: open_id=%s, ec=%s", open_id, ec)
        self.login_state = LoginStatus.WAITING
        return ec
    end
    -- 清除open_id(账号)的index状态
    local rpc_req = { open_id = open_id, area_id = self.area_id, lobby_id = quanta.id }
    local ok, code = router_mgr:call_index_hash(open_id, "rpc_rm_dispatch_lobby", rpc_req)
    if not ok or check_failed(code) then
        log_err("[PlayerMgr][logout] rpc_rm_dispatch_lobby error: open_id=%s,ok=%s,ec=%s", open_id, ok, code)
        self.login_state = LoginStatus.WAITING
        return code
    end
    log_info("[Account][logout] finish: open_id=%s", open_id)
    -- 退出事务完成，恢复初始状态
    return ec
end

-- 是否忙状态(独占)
function Account:is_holding()
    return self.login_state ~= LoginStatus.WAITING and self.login_state ~= LoginStatus.PLAYING
end

-- 设置网络在线
function Account:set_online()
    log_info("[Account][set_online] open_id=%s", self.open_id)
    self.offline_time = 0
    if self.cur_player then
        log_info("[Account][set_online] player_id=%s", self.cur_player:get_player_id())
    end
end

-- 设置网络掉线
function Account:set_offline()
    log_info("[Account][set_offline] open_id=%s", self.open_id)
    self.offline_time = quanta.now + OFFLINE_TIMEOUT
    local player = self.cur_player
    if player then
        player:save(true)
        player:platform_offline()
        local player_id = self.cur_player:get_player_id()
        log_info("[Account][set_offline] player_id=%s", player_id)
        event_mgr:notify_trigger("on_player_offline", player_id, player)
    end
end

function Account:is_online()
    return self.offline_time <= 0
end

-- 是否掉线超时
function Account:is_offline_timeout()
    if self.offline_time > 0 then
        return quanta.now > self.offline_time
    end
    return false
end

-- 生成player_id
function Account:new_player_id()
    return new_guid(self.area_id, quanta.index)
end

-- 新增一个player
function Account:add_player(nick, sex)
    -- 开始添加角色事务
    self.login_state = LoginStatus.CREATING_PLAYER
    -- 验证昵称是否可用
    local open_id, area_id = self.open_id, self.area_id
    local player_id = self:new_player_id()
    local nick_code = plat_api:check_player_name_used(player_id, nick)
    if check_failed(nick_code) then
        log_err("[Account][add_player] verify nick faild! open_id=%s, nick=%s, ec=%s", open_id, nick, nick_code)
        self.login_state = LoginStatus.WAITING
        -- 添加角色事务结束
        return nick_code
    end
    -- 构造player对象
    local player = player_mgr:create_player(open_id, player_id, area_id)
    if not player then
        log_err("[Account][add_player] create player faild! open_id=%s, player_id=%s", open_id, player_id)
        self.login_state = LoginStatus.WAITING
        return KernCode.MONGO_FAILED
    end
    local real_sex = (sex == SexType.MAN) and sex or SexType.WOMAN
    local player_data = { nick = nick, sex = real_sex, player_id = player_id }
    -- account中添加新的player_id
    local account_data = self:build_account_data(player_data)
    if not account_dao:update_account(area_id, open_id, account_data) then
        log_err("[Account][add_player] update_account failed: open_id=%s", open_id)
        self.login_state = LoginStatus.WAITING
        -- 添加角色事务结束
        return KernCode.MONGO_FAILED
    end
    tinsert(self.own_players, player_data)
    -- 执行新对象数据初始操作（可以尝试挪到setup中）
    player:set_nick(nick)
    player:set_sex(real_sex)
    player_mgr:set_player(player_id, player)
    self.login_state = LoginStatus.WAITING
    self:setup_new_player(player, player_id)
    return KernCode.SUCCESS, player_data
end

-- 选定player
function Account:login_player(player_id, is_create)
    self.login_state = LoginStatus.LOGINING_PLAYER
    -- 获取player实体
    local player = player_mgr:get_player(player_id)
    if not player then
        player = player_mgr:create_player(self.open_id, player_id, self.area_id)
        if not player then
            log_err("[Account][login_player] create player error: player_id=%s", player_id)
            self.login_state = LoginStatus.WAITING
            return KernCode.MONGO_FAILED
        end
    end
    -- 同步player状态到index
    local ok, ec, online = router_mgr:call_index_hash(player_id, "rpc_player_login", player_id, quanta.id)
    if not ok and check_failed(ec) then
        log_err("[Account][login_player] set index error: player_id=%s,ec=%s", player_id, ec)
        self.login_state = LoginStatus.WAITING
        return ok and ec or KernCode.RPC_FAILED
    end
    --恢复状态
    player:set_login_time()
    player:set_session(self.session)
    player:set_room_id(online.room_id)
    player:set_team_id(online.team_id)
    event_mgr:notify_trigger("on_load_player_begin", player_id, player)

    log_info("[Account][login_player] platform login: player=%s", player_id)
    local code = player:platform_login(self.platform_token, self.machine_id, is_create)
    if check_failed(code) then
        log_err("[Account][login_player] platform login error: player_id=%s,ec=%s", player_id, code)
        self.login_state = LoginStatus.WAITING
        return code
    end
    log_info("[Account][login_player] platform login finish: player=%s", player_id)

    self.cur_player = player
    self.session.player_id = player_id
    player_mgr:set_player(player_id, player)
    self.login_state = LoginStatus.PLAYING
    log_info("[Account][login_player] finish: player_id=%s, token=%s", player_id, self.session.token)
    return KernCode.SUCCESS, player
end

-- 退出当前游戏中的player
function Account:logout_player()
    local player = self.cur_player
    if not player then
        return KernCode.SUCCESS
    end
    self.login_state = LoginStatus.LOGOUTING_PLAYER
    player:set_logout_time()
    -- lobby内部退出事件
    local player_id = player:get_player_id()
    event_mgr:notify_trigger("on_player_logout", player_id, player)
    -- 登出之前必须调用一次存盘
    if not player:save(true) then
        local data = player:package_debug_data()
        log_err("[Account][logout_player] player:save can't save data: player_id=%s", player_id)
        log_dump("[Account][logout_player] player:save can't save data: player_id=%s,data=%s", player_id, serialize(data))
        self.login_state = LoginStatus.WAITING
        return KernCode.MONGO_FAILED
    end
    -- 移除player的index状态
    local ok, code = router_mgr:call_index_hash(player_id, "rpc_player_logout", player_id)
    if not ok or check_failed(code) then
        log_err("[PlayerMgr:player_logout]: rpc_player_logout error: player_id=%s,ok=%s,ec=%s", player_id, ok, code)
        self.login_state = LoginStatus.WAITING
        return code
    end
    -- 退出平台
    player:platform_logout()
    -- 只要有player，一定要发出退出事件完成当前进程能处理的退出逻辑
    log_info("[Account][logout_player] open_id=%s, player_id=%s", self.open_id, player_id)
    -- 设置player已登出
    player_mgr:remove_player(player, player_id)
    self.login_state = LoginStatus.WAITING
    self.cur_player = nil
    return KernCode.SUCCESS
end

-- setup new player
function Account:setup_new_player(player, player_id)
    -- 初始化默认数据
    player_mgr:init_player_default_data(player)
    -- 初始化生涯数据
    player:init_new_career()
    log_info("[Account][setup_new_player] open_id=%s,player_id=%s", self.open_id, player_id)
    event_mgr:notify_trigger("evt_create_new_player", player, player_id)
end

-- 是否游戏中
function Account:is_playing()
    if not self.cur_player then
        return false
    end
    local rm_type, _ = self.cur_player:get_room_type()
    if rm_type ~= RoomType.FIGHT then
        return false
    end
    return true
end

-- player重连
function Account:reconnect_selected_player()
    local player = self.cur_player
    if player then
        player:set_session(self.session)
        event_mgr:notify_trigger("on_player_reload", player:get_player_id(), player)
    end
end

-- 修改昵称
function Account:modify_nick(player_id, new_nick)
    for _, player_data in pairs(self.own_players) do
        if player_data.player_id == player_id then
            player_data.nick = new_nick
            --更新账号信息
            local account_data = self:build_account_data()
            account_dao:update_account(self.area_id, self.open_id, account_data)
            break
        end
    end
end

function Account:build_account_data(new_player_data)
    local account_data = {
        open_id     = self.open_id,
        account_id  = self.account_id,
        area_id     = self.area_id,
        players     = {}
    }
    for _, player_data in pairs(self.own_players) do
        tinsert(account_data.players, player_data)
    end
    if new_player_data then
        tinsert(account_data.players, new_player_data)
    end
    return account_data
end

-- 数据打包
function Account:package_debug_data()
    return self:build_account_data()
end

return Account
