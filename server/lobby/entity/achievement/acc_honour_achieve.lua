--acc_honour_achievelua.lua
-- 累计型荣耀成就
local HonourAchievement   = import("lobby/entity/achievement/honour_achievement.lua")

local AccHonourAchieve = class(HonourAchievement)

function AccHonourAchieve:set_progress(player, progress)
    if not progress then
        return false
    end

    if 0 == progress then
        -- reset
        self.achieve_data.progress = 0
    else
        self.achieve_data.progress = self.achieve_data.progress + progress
    end

    return true
end

return AccHonourAchieve