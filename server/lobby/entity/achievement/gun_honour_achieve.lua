--gun_honour_achievelua.lua
-- 累计型荣耀成就
local HonourAchievement = import("lobby/entity/achievement/honour_achievement.lua")

local GunHonourAchieve = class(HonourAchievement)

function GunHonourAchieve:set_progress(player, progress)
    self.achieve_data.progress = 0
    local guns = player:get_all_weapon_items()
    for _, gun in pairs(guns) do
        if gun.level > self.achieve_cfg.param2[2] then
            self.achieve_data.progress = self.achieve_data.progress + 1
        end
    end

    return true
end

return GunHonourAchieve