--cover_honour_achievelua.lua
-- 覆盖型荣耀成就
local HonourAchievement  = import("lobby/entity/achievement/honour_achievement.lua")

local CoverHonourAchieve = class(HonourAchievement)

function CoverHonourAchieve:set_progress(player, progress)
    if not progress then
        return false
    end

    if self.achieve_data.progress > progress then
        return false
    end

    self.achieve_data.progress = progress
    return true
end

return CoverHonourAchieve