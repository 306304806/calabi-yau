--achievement.lua
-- 成就基类
local log_debug             = logger.debug
local log_err               = logger.err
local serialize             = logger.serialize

local config_mgr            = quanta.get("config_mgr")
local event_mgr             = quanta.get("event_mgr")

local achievement_db        = config_mgr:get_table("achievement")

local Achievement = class()
local prop = property(Achievement)
prop:accessor("id", nil)                -- 成就id

function Achievement:__init(achieve_cfg)
    self.id = achieve_cfg.id
end

function Achievement:attach(achieve_data)
    if self.id ~= achieve_data.id then
        log_err("[Achievement][attach] id not match self.id = %s, achieve_data.id = %s", self.id, achieve_data.id)
    end

    self.achieve_cfg = achievement_db:find_one(self.id)
    self.achieve_data = achieve_data
end

function Achievement:prize(player)
end

function Achievement:set_progress()
    return false
end

function Achievement:check_progress()
    return false
end

function Achievement:update(player, progress, add_reach)
    local achieve_data = player:get_achieve(self.id)
    self:attach(achieve_data)

    if self:is_finish() then
        return false
    end

    log_debug("[Achievement][update] achieve_data %s", serialize(self.achieve_data))
    if add_reach then
        self:reach(player)
        return true
    end

    local need_update = self:set_progress(player, progress)
    if need_update then
        player:set_achieve(self.achieve_data)
    end

    if not self:check_progress() then
        return need_update
    end

    self:reach(player)
    return true
end

-- 成就达成
function Achievement:reach(player)
    self:update_achieve_cnt(player)

    self.achieve_data.reach = self.achieve_data.reach + 1
    player:set_achieve(self.achieve_data)

    self:prize(player)

    event_mgr:notify_trigger("evt_on_achieve_reach", player, self)
end

function Achievement:update_achieve_cnt(player)
    local old_cnt = player:get_achieve_cnt(self.achieve_cfg.type)
    local new_cnt = old_cnt + 1
    player:set_achieve_cnt(self.achieve_cfg.type, new_cnt)
    player:platform_achieve_sync()
end

-- 成就已经达成
function Achievement:is_finish()
    return false
end

return Achievement