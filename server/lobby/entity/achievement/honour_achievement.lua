--honour_achievement.lua
-- 荣耀成就（累计型）
local otime         = os.time
local log_err       = logger.err
local serialize     = logger.serialize
local tinsert       = table.insert
local check_failed  = utility.check_failed

local plat_api      = quanta.get("plat_api")

local MailType      = enum("MailType")
local Achievement   = import("lobby/entity/achievement/achievement.lua")

local HonourAchievement = class(Achievement)

function HonourAchievement:prize(player)
    if self.achieve_cfg.param3 ~= 1 then
        return
    end

    -- send mail
    local player_id = player:get_player_id()
    local rpc_req = {
        type          = MailType.SYS_MAIL,
        src_player_id = 0,
        tar_player_id = player_id,
        send_time     = otime(),
        title         = self.achieve_cfg.name,
        content       = self.achieve_cfg.details,
        attach_items  = {}
    }

    for _, item in pairs(self.achieve_cfg.param5) do
        local attach_item = {
            item_id = item.item_id,
            item_count = item.item_amount,
        }
        tinsert(rpc_req.attach_items, attach_item)
    end
    local code, _ = plat_api:send_player_mail(player_id, rpc_req)
    if check_failed(code) then
        log_err("[HonourAchievement][prize] code = %s, player_id = %s, rpc_req = %s", code, player_id, serialize(rpc_req))
    end
end

function HonourAchievement:check_progress()
    return self.achieve_data.progress >= self.achieve_cfg.param2[1]
end

function HonourAchievement:is_finish()
    return self.achieve_data.reach > 0
end

return HonourAchievement