--role_honour_achievelua.lua
-- 累计型荣耀成就
local HonourAchievement = import("lobby/entity/achievement/honour_achievement.lua")

local RoleHonourAchieve = class(HonourAchievement)

function RoleHonourAchieve:set_progress(player, progress)
    self.achieve_data.progress = 0
    local roles = player:get_role_infos()
    for _, role in pairs(roles) do
        if role.exp > self.achieve_cfg.param2[2] then
            self.achieve_data.progress = self.achieve_data.progress + 1
        end
    end

    return true
end

return RoleHonourAchieve