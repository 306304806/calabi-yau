--token_mgr.lua
local otime         = os.time
local guid_string   = guid.string
local log_warn      = logger.warn

local PeriodTime    = enum("PeriodTime")
local TokenExpire   = enum("TokenExpire")

local event_mgr     = quanta.get("event_mgr")
local timer_mgr     = quanta.get("timer_mgr")

local TokenMgr = singleton()
function TokenMgr:__init()
    self.oid2token = {}
    self.aid2token = {}

    timer_mgr:loop(PeriodTime.SECOND_5_MS, function()
        self:on_timer()
    end)
end

-- 定时器回调
function TokenMgr:on_timer()
    local cur_time = otime()
    for open_id, token_info in pairs(self.oid2token) do
        if cur_time > token_info.update_time then
            self.oid2token[open_id] = nil
        end
    end
    for account_id, token_info in pairs(self.aid2token) do
        if cur_time > token_info.update_time then
            self:update_account_token(account_id)
        end
    end
end

-- 根据给定的open_id,token创建/更新open_id令牌
function TokenMgr:reset_open_id_token(open_id, token, plat_url)
    self.oid2token[open_id] = {
        token = token,
        plat_url = plat_url,
        update_time = otime() + TokenExpire.LOGIN
    }
end

-- 校验open_id的令牌，获取plat_url
function TokenMgr:verify_open_id_token(open_id, token)
    local token_info = self.oid2token[open_id]
    if not token_info or token_info.token ~= token then
        return
    end
    return token_info.plat_url
end

-- 更新指定account_id的主token
-- 一个token会出现刚断线就更新token，则下次验证失效
-- 因此此处使用两个token，最近两个token都有效
function TokenMgr:update_account_token(account_id)
    local new_token = guid_string()
    local token_info = self.aid2token[account_id]
    self.aid2token[account_id] = {
        token           = new_token,
        update_time     = otime() + TokenExpire.ALIVE,
        backup_token    = token_info and token_info.token
    }
    event_mgr:notify_trigger("evt_update_account_token", account_id, new_token)
end

-- 校验account_id对应的token
function TokenMgr:verify_account_token(account_id, token)
    local token_info = self.aid2token[account_id]
    if not token_info then
        log_warn("[TokenMgr][verify_account_token] account logout: account_id=%s", account_id)
        return false
    end
    if token_info.token ~= token and token_info.backup_token ~= token then
        log_warn("[TokenMgr][verify_account_token] token error: account_id=%s", account_id)
        return false
    end
    return true
end

-- export
quanta.token_mgr = TokenMgr()

return TokenMgr
