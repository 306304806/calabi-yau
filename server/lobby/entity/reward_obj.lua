
-- reward_obj.lua

local tinsert       = table.insert
local otime         = os.time
local check_success = utility.check_success

local RewardType    = enum("RewardType")
local RewardStatus  = enum("RewardStatus")

local KernCode      = enum("KernCode")
local RewardCode    = enum("RewardCode")
local SUCCESS       = KernCode.SUCCESS

local reward_mgr    = quanta.get("reward_mgr")

local RewardObj = class()
local prop = property(RewardObj)
prop:accessor("module_id", 0)
prop:accessor("reward_id", 0)
prop:accessor("active_time",  0)
prop:accessor("recived_time", 0)
prop:accessor("expire_time", 0)

function RewardObj:__init(module_id, reward_id, active_time, recived_time)
    self.module_id    = module_id
    self.reward_id    = reward_id
    self.active_time  = active_time
    self.recived_time    = recived_time or 0

    local life_time = reward_mgr:get_reward_life_time(self.module_id, self.reward_id)
    if life_time and life_time > 0 then
        self.expire_time = self.active_time + life_time
    else
        self.expire_time = 0
    end
end

-- 是否已领取
function RewardObj:is_recived()
    return self.recived_time > 0
end

-- 是否过期
function RewardObj:is_expired()
    return self.expire_time > 0
end

-- 检查是否过期
function RewardObj:check_is_expired()
    if self.expire_time > 0 then
        local cur_time = otime()
        return cur_time >= self.expire_time
    end
end

-- 过期处理
function RewardObj:on_expired()
    -- todo:策划需求不明确，延后处理
end

-- 获取当前状态
function RewardObj:get_status()
    if self:is_recived() then
        return RewardStatus.RECIVED
    end

    if self:is_expired() then
        return RewardStatus.EXPIRED
    end

    return RewardStatus.RECIVING
end

-- 发放奖励
function RewardObj:give_reward_to_player(player)
    local reward_content = reward_mgr:get_reward_content(self.module_id, self.reward_id)
    if not next(reward_content) then
        return RewardCode.REWARD_NOT_EXIST
    end

    local ec = SUCCESS
    for reward_type, content in pairs(reward_content) do
        if reward_type == RewardType.ITEMS then
            local reward_items = {}
            for _, item_info in pairs(content) do
                local expire_time = item_info.expire_time or 0
                tinsert(reward_items, {item_id=item_info.item_id, num=item_info.item_count, add_reason=item_info.add_reason, expire_time=expire_time})
            end

             ec = player:add_items(reward_items)
        end
    end

    if check_success(ec) then
        self.recived_time = otime()
        player:set_reward_dirty(true)
     end
    return  ec
end

return RewardObj