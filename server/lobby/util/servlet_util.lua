--servlet_util.lua
local log_warn      = logger.warn
local serialize     = logger.serialize

local RoomType      = enum("RoomType")
local PlayerCode    = enum("PlayerCode")
local WeaponSubType = enum("WeaponSubType")

local config_mgr    = quanta.get("config_mgr")
local event_mgr     = quanta.get("event_mgr")
local player_mgr    = quanta.get("player_mgr")

local weapon_db     = config_mgr:get_table("weapon")

local ServletUtil = singleton()
function ServletUtil:__init()
    -- rpc SS
    event_mgr:add_listener(self, "rpc_send_tips")
end

function ServletUtil:sure_room_status(player, type)
    local rm_type, room_id = player:get_room_type()
    if rm_type and rm_type == type then
        return true, room_id
    end
    return false, room_id
end

function ServletUtil:check_player_room(player)
    local rm_type = player:get_room_type()
    if rm_type then
        local error_table = {
            [RoomType.FIGHT]   = PlayerCode.PLAYER_ERR_AREADY_IN_FIGHT,
            [RoomType.MATCH]   = PlayerCode.PLAYER_ERR_AREADY_IN_MATCH,
            [RoomType.CONTEST] = PlayerCode.PLAYER_ERR_AREADY_IN_ROOM,
        }
        return false, error_table[rm_type]
    end
    return true
end

function ServletUtil:check_target_room(player)
    local rm_type = player:get_room_type()
    if rm_type then
        local error_table = {
            [RoomType.FIGHT]   = PlayerCode.TARGET_ERR_AREADY_IN_FIGHT,
            [RoomType.MATCH]   = PlayerCode.TARGET_ERR_AREADY_IN_MATCH,
            [RoomType.CONTEST] = PlayerCode.TARGET_ERR_AREADY_IN_ROOM,
        }
        return false, error_table[rm_type]
    end
    return true
end

function ServletUtil:is_gun_by_type(weapon_type)
    return weapon_type >= WeaponSubType.AR and weapon_type <= WeaponSubType.SACB
end

function ServletUtil:get_guns_total_cnt()
    local total_cnt = 0
    for _, weapon_cfg in weapon_db:iterator() do
        if weapon_cfg.type and self:is_gun_by_type(weapon_cfg.type) then
            total_cnt = total_cnt + 1
        end
    end
    return total_cnt
end

-- rpc发送通用错误码提示
function ServletUtil:rpc_send_tips(player_id, code, ttype, ...)
    local player = player_mgr:get_player(player_id)
    if not player then
        log_warn("[ServletUtil][rpc_send_tips] player = nil, player_id = %s, code = %s, ttype = %s, params = %s",
            player_id, code, ttype, serialize({...}))
        return false, PlayerCode.TARGET_ERR_OFFLINE
    end
    return true, player:send_tips(code, ttype, ...)
end

quanta.servlet_util = ServletUtil()

return ServletUtil
