--item_util.lua
--物品相关配置管理器

local log_err           = logger.err

local config_mgr        = quanta.get("config_mgr")
local itemidinterval_db = config_mgr:get_table("itemidinterval")

local ItemType   = enum("ItemType")

local ItemUtil  = singleton()
function ItemUtil:__init()

end

function ItemUtil:get_item_type(id)
    if not id or id <= 0 then
        log_err("[ItemUtil][get_item_type] id para error!")
        return ItemType.NONE
    end

    for _, cfg in itemidinterval_db:iterator() do
        if cfg.id_lower <= id and id <= cfg.id_upper then
            return cfg.item_type
        end
    end

    log_err("[ItemUtil][get_item_type] id find item type failed! id:%s", id)
    return ItemType.NONE
end

function ItemUtil:is_bag_item_type(id)
    local item_type = self:get_item_type(id)
    local item_type_set = {
        [ItemType.BagItem_ModName]              = true,   -- 道具-改名卡
        [ItemType.BagItem_JinBiAdd]             = true,   -- 道具-金币加成卡
        [ItemType.BagItem_ExpAdd]               = true,   -- 道具-经验加成卡
        [ItemType.BagItem_WeaponExpAdd]         = true,   -- 道具-武器经验加成卡
        [ItemType.BagItem_FixedGift]            = true,   -- 道具-固定礼包
        [ItemType.BagItem_RandGift]             = true,   -- 道具-随机礼包
        [ItemType.BagItem_RoleCard]             = true,   -- 道具—角色体验卡
        [ItemType.BagItem_WeaponCard]           = true,   -- 道具—武器体验卡
        [ItemType.BagItem_RoleSkinCard]         = true,   -- 道具—角色皮肤体验卡
        [ItemType.BagItem_RoleActionCard]       = true,   -- 道具—角色动作体验卡
        [ItemType.BagItem_RoleVoiceCard]        = true,   -- 道具—角色语音体验卡
        [ItemType.BagItem_VCardAvatarCard]      = true,   -- 道具—名片形象体验卡
        [ItemType.BagItem_VCardBgCard]          = true,   -- 道具—名片背景体验卡
        [ItemType.BagItem_VCardFrameCard]       = true,   -- 道具—名片框体验卡
        [ItemType.BagItem_DecalCard]            = true,   -- 道具—喷漆体验卡
    }
    return item_type_set[item_type or 0]
end

function ItemUtil:is_weapon_item_type(id)
    return self:get_item_type(id) == ItemType.Weapon
end

function ItemUtil:is_decal_item_type(id)
    return self:get_item_type(id) == ItemType.Decal
end

function ItemUtil:is_currency_item_type(id)
    return self:get_item_type(id) == ItemType.Currency
end

function ItemUtil:get_item_config(id)
    -- 接口预留
end

quanta.item_util = ItemUtil()

return ItemUtil
