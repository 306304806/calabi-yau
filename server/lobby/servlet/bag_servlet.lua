--bag_servlet.lua
local otime         = os.time
local tinsert       = table.insert
local log_debug     = logger.debug
local log_err       = logger.err
local serialize     = logger.serialize
local check_failed  = utility.check_failed

local KernCode      = enum("KernCode")
local ReddotType    = enum("ReddotType")
local BagCode       = enum("BagCode")
local PlayerAttrID  = enum("PlayerAttrID")
local ParameterCfg  = enum("ParameterCfg")
local ResDelType    = enum("ResDelType")
local DlogWeapon    = enum("DlogWeapon")
local ItemType      = enum("ItemType")
local SUCCESS       = KernCode.SUCCESS

local CSCmdID       = ncmd_cs.NCmdId

local plat_api      = quanta.get("plat_api")
local event_mgr     = quanta.get("event_mgr")
local config_mgr    = quanta.get("config_mgr")
local dlog_mgr      = quanta.get("dlog_mgr")
local item_util     = quanta.get("item_util")

local bag_item_db   = config_mgr:get_table("item")
local weapon_db     = config_mgr:get_table("weapon")
local parameter_db  = config_mgr:get_table("parameter")

-- 背包服务
local BagServlet = singleton()
function BagServlet:__init()
    self:setup()
end

function BagServlet:setup()
    -- 内部玩家事件注册
    event_mgr:add_trigger(self, "on_load_player_begin")
    -- 添加触发器
    event_mgr:add_trigger(self, "ntf_part_items_to_client")
    event_mgr:add_trigger(self, "evt_items_change")
    event_mgr:add_trigger(self, "evt_weapon_change")

    -- 客户端消息注册
    event_mgr:add_cmd_listener(self, CSCmdID.NID_ITEM_USE_REQ            , "on_use_bag_item_req")        -- 使用礼包请求
    event_mgr:add_cmd_listener(self, CSCmdID.NID_BAG_MOD_NAME_REQ        , "on_mod_name_req")            -- 使用改名卡请求
    event_mgr:add_cmd_listener(self, CSCmdID.NID_BAG_ADDITION_CARD_REQ   , "on_addition_card_req")       -- 使用加成卡请求
    event_mgr:add_cmd_listener(self, CSCmdID.NID_BAG_ITEM_SELL_REQ       , "on_sell_bag_item_req")       -- 出售道具
end

-- 加载玩家数据开始触发回调
function BagServlet:on_load_player_begin(player_id, player)
    self:sync_all_items_to_client(player)
end

-- 客户端使用物品请求
function BagServlet:on_use_bag_item_req(player, player_id, body, session_id)
    local use_item_uuid = body.item_uuid
    local count  = body.count
    local resp = { code = SUCCESS, obtain_list = { result_list = {}, convert_list = {}}, use_item_uuid = use_item_uuid, item_id = 0, count = count}
    local bag_item = player:get_bag_item_by_uuid(use_item_uuid)
    if not bag_item then
        resp.code = BagCode.BAG_ERR_ITEM_NOT_EXIST
        player:callback(CSCmdID.NID_ITEM_USE_RES, resp, session_id)
        log_err("[BagServlet][on_use_bag_item_req]-> get goods failed! player_id:%s, use_item_uuid:%s", player_id, use_item_uuid)
        return
    end

    local item_id = bag_item:get_item_id()
    resp.item_id = item_id
    local code, result_list = self:use_bag_item(player, use_item_uuid, item_id, count)
    if check_failed(code) then
        resp.code = BagCode.BAG_ERR_ITEM_NOT_EXIST
        player:callback(CSCmdID.NID_ITEM_USE_RES, resp, session_id)
        return
    end

    player:change_items_data_sync_client(result_list)

    local temp_add_item = {}
    local temp_conv = {}
    for _, result in pairs(result_list) do
        if result.add_success then
            local add_item_id = result.item_id
            if not temp_add_item[add_item_id] then
                temp_add_item[add_item_id] = 0
            end
            temp_add_item[add_item_id] = temp_add_item[add_item_id] + (result.num or 0)

            if next(result.limit_conv_info) then
                if not temp_conv[add_item_id] then
                    temp_conv[add_item_id] = result.limit_conv_info
                else
                    local conv_data = temp_conv[add_item_id]
                    conv_data.currency_num =  conv_data.currency_num + result.limit_conv_info.currency_num
                    conv_data.conv_num = conv_data.conv_num + result.limit_conv_info.conv_num
                end
            end
        end
    end

    for id, num in pairs(temp_add_item) do
        tinsert(resp.obtain_list.result_list, {item_id = id, count = num})
    end

    for id, data in pairs(temp_conv) do
        tinsert(resp.obtain_list.convert_list, {item_id = id, count = data.conv_num, currency_id = data.currency_id, currency_cnt = data.currency_num})
    end

    log_debug("[BagServlet][on_use_bag_item_req]->player_id:%s, resp:%s", player_id, serialize(resp))
    player:callback(CSCmdID.NID_ITEM_USE_RES, resp, session_id)
end

-- 请求使用改名卡
function BagServlet:on_mod_name_req(player, player_id, body, session_id)
    log_debug("[BagServlet][on_mod_name_req]->player_id:%s, body:%s", player_id, serialize(body))
    local resp = {code = SUCCESS}
    local item_uuid = body.item_uuid
    local item_mod_name = player:get_bag_item_by_uuid(item_uuid)
    if not item_mod_name or item_util:get_item_type(item_mod_name:get_item_id()) ~= ItemType.BagItem_ModName then
        resp.code = BagCode.BAG_ERR_ITEM_NOT_EXIST
        player:callback(CSCmdID.NID_BAG_MOD_NAME_RES, resp, session_id)
        log_err("[BagServlet][on_mod_name_req]->get item failed! player_id:%s, item_uuid:%s", player_id, item_uuid)
        return
    end

    local new_nick = body.new_nick
    local nick_ec = plat_api:check_player_name_used(player_id, new_nick)
    if check_failed(nick_ec) then
        resp.code = nick_ec
        player:callback(CSCmdID.NID_BAG_MOD_NAME_RES, resp, session_id)
        log_err("[BagServlet][on_mod_name_req]->verify nick failedd! player_id:%s, item_uuid:%s", player_id, item_uuid)
        return
    end

    local item = parameter_db:find_one(ParameterCfg.MOD_NAME_CD)
    local cd_time = tonumber(item.para_value)
    if not cd_time or cd_time <= 0 then
        resp.code = KernCode.PARAM_ERROR
        player:callback(CSCmdID.NID_BAG_MOD_NAME_RES, resp, session_id)
        log_err("[BagServlet][on_mod_name_req]->param error! player_id:%s", player_id)
        return
    end

    -- 检查上次修改名字时间
    local cur_time = otime()
    local last_mod_time = player:get_attribute(PlayerAttrID.LAST_MOD_NAME)
    if last_mod_time > 0 and last_mod_time + cd_time > cur_time then
        resp.code = BagCode.BAG_ERR_MOD_NAME_FREQUENT
        resp.rest_cd = (last_mod_time + cd_time - cur_time)
        player:callback(CSCmdID.NID_BAG_MOD_NAME_RES, resp, session_id)
        log_err("[BagServlet][on_mod_name_req]->count down! player_id:%s, last_mod_time:%s", player_id, last_mod_time)
        return
    end

    -- 消耗物品
    local ret, _ = player:remove_item(item_mod_name:get_item_id(), 1, ResDelType.MOD_NAME, item_uuid)
    if not ret then
        resp.code = BagCode.BAG_ERR_ITEM_NOT_EXIST
        player:callback(CSCmdID.NID_BAG_MOD_NAME_RES, resp, session_id)
        log_err("[BagServlet][on_mod_name_req]->remove item failed! player_id:%s, item_uuid:%s", player_id, item_uuid)
        return
    end

    player:set_nick(new_nick)
    player:set_attribute(PlayerAttrID.LAST_MOD_NAME, cur_time)
    event_mgr:notify_trigger("evt_mod_nick", player, new_nick)

    log_debug("[BagServlet][on_mod_name_req]->player_id:%s, new_nick:%s", player_id, new_nick)

    -- 改名并通知平台
    player:platform_nick_sync()

    player:callback(CSCmdID.NID_BAG_MOD_NAME_RES, resp, session_id)
end

-- 请求使用加成卡
function BagServlet:on_addition_card_req(player, player_id, body, session_id)
    log_debug("[BagServlet][on_addition_card_req]->player_id:%s, body:%s", player_id, serialize(body))
    local resp = {code = SUCCESS, item_id = 0, item_count = 0}
    local item_uuid = body.item_uuid
    local use_count = body.item_count
    local item_addition_card = player:get_bag_item_by_uuid(item_uuid)
    if not item_addition_card then
        resp.code = BagCode.BAG_ERR_ITEM_NOT_EXIST
        player:callback(CSCmdID.NID_BAG_ADDITION_CARD_RES, resp, session_id)
        log_err("[BagServlet][on_addition_card_req]->get item failed! player_id:%s, item_uuid:%s", player_id, item_uuid)
        return
    end
    local item_id = item_addition_card:get_item_id()

    resp.item_id = item_id
    resp.item_count = use_count

    local item_type = item_util:get_item_type(item_id)
    if item_type ~= ItemType.BagItem_JinBiAdd and item_type ~= ItemType.BagItem_ExpAdd  and item_type ~= ItemType.BagItem_WeaponExpAdd then
        resp.code = BagCode.BAG_ERR_ITEM_NOT_EXIST
        player:callback(CSCmdID.NID_BAG_ADDITION_CARD_RES, resp, session_id)
        log_err("[BagServlet][on_addition_card_req]get item type error! player_id:%s, item_id:%s, item_type:%s", player_id, item_id, item_type)
        return
    end

    if not use_count or use_count <= 0 or item_addition_card:get_count() < use_count then
        resp.code = BagCode.BAG_ERR_NUM_NOT_ENOUGH
        player:callback(CSCmdID.NID_BAG_ADDITION_CARD_RES, resp, session_id)
        log_err("[BagServlet][on_addition_card_req]->get item failed! player_id:%s, item_uuid:%s", player_id, item_uuid)
        return
    end

    local code, result_list = self:use_bag_item(player, item_uuid, item_id, use_count)
    if check_failed(code) then
        resp.code = code
        player:callback(CSCmdID.NID_BAG_ADDITION_CARD_RES, resp, session_id)
        return
    end

    player:change_items_data_sync_client(result_list)

    player:callback(CSCmdID.NID_BAG_ADDITION_CARD_RES, resp, session_id)
    event_mgr:notify_trigger("ntf_sync_player_buffs", player)
end

-- 使用背包道具
function BagServlet:use_bag_item(player, item_uuid, item_id, use_count)
    local bag_item = player:get_bag_item_by_uuid(item_uuid)
    if not bag_item then
        log_err("[BagServlet][use_bag_item] get bag item error! player_id:%s, item_uuid:%s", player.player_id, item_uuid)
        return BagCode.BAG_ERR_ITEM_NOT_EXIST
    end

    if bag_item:get_count() < use_count then
        log_err("[BagServlet][use_bag_item] bag item not enough! player_id:%s, item_uuid:%s", player.player_id, item_uuid)
        return BagCode.BAG_ERR_NUM_NOT_ENOUGH
    end

    local item_cfg = bag_item_db:find_one(item_id)
    if not item_cfg then
        log_err("[BagServlet][use_bag_item]->get item config failed! item_id:%s", item_id)
        return BagCode.BAG_ERR_ITEM_NOT_EXIST
    end

    log_debug("[BagServlet][use_bag_item]->player_id:%s, item_uuid:%s, item_id:%s, use_count:%s", player.player_id, item_uuid, item_id, use_count)

    local result_list  = {}
    for idx = 1, use_count do
        local ret_flag, ret_list = player:use_bag_item(item_uuid, ResDelType.CLIENT_REQ_USE)
        if not ret_flag then
            log_err("[BagServlet][use_bag_item]->use bag item failed! player_id:%s, item_uuid:%s, idx:%s", player.player_id, item_uuid, idx)
            return BagCode.BAG_ERR_EFFECT_NOT_EXIST
        end

        for _, data in pairs(ret_list) do
            tinsert(result_list, data)
        end
    end

    return SUCCESS, result_list
end

-- 出售道具
function BagServlet:on_sell_bag_item_req(player, player_id, body, session_id)
    local item_uuid = body.item_uuid
    local sell_count = body.sell_count
    local resp = {code = SUCCESS, item_uuid = item_uuid, sell_count = sell_count, currency_type = 0, currency_total = 0, item_id = 0}
    local bag_item = player:get_bag_item_by_uuid(item_uuid)
    if not bag_item then
        log_err("[BagServlet][on_sell_bag_item_req] find item failed! player_id:%s, item_uuid:%s", player.player_id, item_uuid)
        resp.code = BagCode.BAG_ERR_ITEM_NOT_EXIST
        player:callback(CSCmdID.NID_BAG_ITEM_SELL_RES, resp, session_id)
        return
    end
    resp.item_id = bag_item:get_item_id()

    log_debug("[BagServlet][on_sell_bag_item_req]->player_id:%s, item_uuid:%s, sell_count:%s", player_id, item_uuid, sell_count)

    if not sell_count or sell_count <= 0 then
        log_err("[BagServlet][on_sell_bag_item_req] sell_count error! player_id:%s, sell_count:%s", player.player_id, sell_count)
        resp.code = KernCode.PARAM_ERROR
        player:callback(CSCmdID.NID_BAG_ITEM_SELL_RES, resp, session_id)
        return
    end

    local item_id = bag_item:get_item_id()
    local item_cfg = bag_item_db:find_one(item_id)
    if not item_cfg then
        log_err("[BagServlet][on_sell_bag_item_req] find item cfg failed! player_id:%s, item_id:%s", player.player_id, item_id)
        resp.code = BagCode.BAG_ERR_CFG_NOT_EXIST
        player:callback(CSCmdID.NID_BAG_ITEM_SELL_RES, resp, session_id)
        return
    end

    if not item_cfg.sale_type or item_cfg.sale_type <= 0 then
        log_err("[BagServlet][on_sell_bag_item_req] item not sell! player_id:%s, item_id:%s", player.player_id, item_id)
        resp.code = BagCode.BAG_ERR_ITEM_NOT_SELL
        player:callback(CSCmdID.NID_BAG_ITEM_SELL_RES, resp, session_id)
        return
    end

    local ret, _ = player:remove_item(item_id, sell_count, ResDelType.SELL, item_uuid)
    if ret then
        local currency_id = item_cfg.sale_type
        local currency_num = item_cfg.sale_param * sell_count
        if player:add_currency_item(currency_id, currency_num) then
            resp.currency_type = currency_id
            resp.currency_total = currency_num
            player:callback(CSCmdID.NID_BAG_ITEM_SELL_RES, resp, session_id)
        else
            resp.code = BagCode.BAG_ERR_SELL_HANLE_FAIL
            player:callback(CSCmdID.NID_BAG_ITEM_SELL_RES, resp, session_id)
        end
    else
        resp.code = BagCode.BAG_ERR_ITEM_NOT_SELL
        player:callback(CSCmdID.NID_BAG_ITEM_SELL_RES, resp, session_id)
        return
    end
end

local function build_bag_items(player, bag_items_set, red_dot_list, item_list)
    for _, data in pairs(bag_items_set or {}) do
        local item_info = {
                                item_id     = data.item_id,
                                item_uuid   = data.item_uuid,
                                type        = item_util:get_item_type(data.item_id),
                                count       = 0,
                                expire_time = 0,
                                obtain_time = 0,
                          }
        local bag_item = player:get_bag_item_by_uuid(data.item_uuid)
        if bag_item then
            item_info.count = bag_item:get_count()
            item_info.expire_time = bag_item:get_expire_time()
            item_info.obtain_time = bag_item:get_obtain_time()

            if data.red_dot then
                red_dot_list[data.item_uuid] = true
            end
        end
        tinsert(item_list, item_info)
    end
end

local function build_weapon_items(player, weapon_items_set, red_dot_list, weapon_list)
    for _, data in pairs(weapon_items_set or {}) do
        local weapon_info = {
                                weapon_base = {
                                                item_id     = data.item_id,
                                                item_uuid   = data.item_uuid,
                                                type        = ItemType.Weapon,
                                                count       = 1,
                                                expire_time = 0,
                                                obtain_time = 0,
                                            },
                                level       = 0,
                                cur_exp     = 0,
                                attachments = {},
                                unlocks     = {},
                            }

        local weapon_item = player:get_weapon_item_by_uuid(data.item_uuid)
        if weapon_item then
            weapon_info.weapon_base.expire_time = weapon_item:get_expire_time()
            weapon_info.weapon_base.obtain_time = weapon_item:get_obtain_time()
            if weapon_item:is_gun() then
                weapon_info.level       = weapon_item:get_level()
                weapon_info.cur_exp     = weapon_item:get_cur_exp()
                weapon_info.attachments = weapon_item:equiped_attachment_to_client()
                weapon_info.unlocks     = weapon_item:unlock_attachment_to_client()
            end

            if data.red_dot then
                red_dot_list[data.item_uuid] = true
            end
        end
        tinsert(weapon_list, weapon_info)
    end
end

local function build_decal_items(player, decal_items_set, red_dot_list, decal_list)
    for _, data in pairs(decal_items_set or {}) do
        local item_info = {
                                item_id     = data.item_id,
                                item_uuid   = data.item_uuid,
                                type        = item_util:get_item_type(data.item_id),
                                count       = 0,
                                expire_time = 0,
                                obtain_time = 0,
                          }
        local decal_item = player:get_decal_item_by_uuid(data.item_uuid)
        if decal_item then
            item_info.count = decal_item:get_count()
            item_info.expire_time = decal_item:get_expire_time()
            item_info.obtain_time = decal_item:get_obtain_time()

            if data.red_dot then
                red_dot_list[data.item_uuid] = true
            end
        end
        tinsert(decal_list, item_info)
    end
end

-- 通知客户端仓库 增量更新
function BagServlet:ntf_part_items_to_client(player, sync_items)
    local res = { item_list = {}, weapon_list = {}, decal_list = {} }
    local red_dot_list = {}

    build_bag_items(player, sync_items.bag_items_set, red_dot_list, res.item_list)
    build_weapon_items(player, sync_items.weapon_items_set, red_dot_list, res.weapon_list)
    build_decal_items(player, sync_items.decal_items_set, red_dot_list, res.decal_list)

    log_debug("[BagServlet][ntf_part_items_to_client]-> player_id:%s, res:%s", player:get_player_id(), serialize(res))

    player:send(CSCmdID.NID_BAG_PART_ITEMS_SYNC_NTF, res)

    for item_uuid in pairs(red_dot_list) do
        log_debug("[BagServlet][ntf_part_items_to_client]->red_dot->item_uuid:%s", item_uuid)
        plat_api:reddot_add_req(player:get_player_id(), ReddotType.NEW_ITEM, item_uuid)
    end
end

-- 通知客户端仓库中items列表全量更新
function BagServlet:sync_all_items_to_client(player)
    local bag = { item_list = {}, weapon_list = {}, decal_list = {}}
    for item_uuid, item in pairs(player:get_all_bag_items()) do
        tinsert(bag.item_list, {
                                    type = item:get_item_type(), item_uuid = item_uuid, item_id = item:get_item_id(), count = item:get_count(),
                                    expire_time = item:sync_client_expire_time(), obtain_time = item:get_obtain_time(),
                                }
        )
    end

    for weapon_uuid, item in pairs(player:get_all_weapon_items()) do
        local base_info = {
                                type = item:get_item_type(), item_uuid = weapon_uuid, item_id = item:get_item_id(), count = item:get_count(),
                                expire_time = item:sync_client_expire_time(), obtain_time = item:get_obtain_time(),
                          }
        if item and item:is_gun() then
            tinsert(bag.weapon_list, {
                                        weapon_base = base_info,
                                        level       = item:get_level(),
                                        cur_exp     = item:get_cur_exp(),
                                        attachments = item:equiped_attachment_to_client(),
                                        unlocks     = item:unlock_attachment_to_client(),
                                    }
            )
        else
            tinsert(bag.weapon_list, {weapon_base = base_info,})
        end
    end

    for decal_uuid, item in pairs(player:get_all_decal_items()) do
        tinsert(bag.decal_list, {
                                    type = item:get_item_type(), item_uuid = decal_uuid, item_id = item:get_item_id(), count = item:get_count(),
                                    expire_time = item:sync_client_expire_time(), obtain_time = item:get_obtain_time(),
                                }
        )
    end

    player:send(CSCmdID.NID_BAG_ALL_ITEMS_SYNC_NTF, bag)
end

function BagServlet:evt_items_change(player, info)
    local special_fields = {}
    special_fields.change_type = info.change_type
    special_fields.item_id     = info.item_id
    special_fields.item_num    = info.item_num
    special_fields.before_num  = info.before_num or 0
    special_fields.after_num   = info.after_num or 0
    special_fields.reason      = info.reason or 0
    special_fields.item_type   = info.item_type
    special_fields.item_name   = info.item_name
    special_fields.item_quality = info.item_quality
    special_fields.item_else_props_json = info.item_else_props_json
    -- 精分上报-道具流水
    dlog_mgr:send_dlog_game_item({public_fields = player:build_dlog_public_fields(), special_fields = special_fields})
end

function BagServlet:evt_weapon_change(player, weapon, opt_type)
    local weapon_cfg = weapon_db:find_one(weapon.item_id)
    if not weapon_cfg then return end

    local special_fields = {}
    special_fields.weapon_id = weapon.item_id
    special_fields.weapon_name = weapon_cfg.name
    special_fields.weapon_level = weapon.level
    if opt_type == DlogWeapon.NEW then
        special_fields.operation_type = 1020
    elseif opt_type == DlogWeapon.LEVEL_UP then
        special_fields.operation_type = 1021
    end

    -- 精分上报-道具流水
    dlog_mgr:send_dlog_game_weapon_klbq({public_fields = player:build_dlog_public_fields(), special_fields = special_fields})
end

quanta.bag_servlet = BagServlet()

return BagServlet
