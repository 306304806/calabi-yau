--settle_servlet.lua
-- 结算相关
local otime             = os.time
local mmin              = math.min
local mmax              = math.max
local log_err           = logger.err
local log_debug         = logger.debug
local log_info          = logger.info
local serialize         = logger.serialize
local tsize             = table_ext.size
local ssplit            = string_ext.split
local check_failed      = utility.check_failed

local KernCode          = enum("KernCode")
local SettleCode        = enum("SettleCode")
local GameMode          = enum("GameMode")
local DivisionStars     = enum("DivisionStars")
local AchieveType       = enum("AchieveType")
local AchieveHonourID   = enum("AchieveHonourID")
local BehaviorAttrID    = enum("BehaviorAttrID")

local SUCCESS           = KernCode.SUCCESS
local CSCmdID           = ncmd_cs.NCmdId

local event_mgr         = quanta.get("event_mgr")
local config_mgr        = quanta.get("config_mgr")
local router_mgr        = quanta.get("router_mgr")
local player_mgr        = quanta.get("player_mgr")
local rmsg_settlement   = quanta.get("rmsg_settlement")

local utility_db        = config_mgr:get_table("utility")
local division_db       = config_mgr:get_table("division")
local divisionstar_db   = config_mgr:get_table("divisionstar")
local divisionpoints_db = config_mgr:get_table("divisionpoints")

local EXP_PARAM         = utility_db:find_value("value", "settle_exp_param")
local IDEAL_PARAM       = utility_db:find_value("value", "settle_ideal_param")
local RANK_WIN_PARAM    = utility_db:find_value("value", "settle_rank_win_param")
local RANK_LOSE_PARAM   = utility_db:find_value("value", "settle_rank_lose_param")
local ROOM_WIN_PARAM    = utility_db:find_value("value", "settle_room_win_param")
local ROOM_LOSE_PARAM   = utility_db:find_value("value", "settle_room_lose_param")

local function get_settle_mode_param(room_mode, is_win)
    if room_mode ~= GameMode.ROOM then
        return is_win and RANK_WIN_PARAM or RANK_LOSE_PARAM
    end
    return is_win and ROOM_WIN_PARAM or ROOM_LOSE_PARAM
end

-- 获取玩法模式奖励分数
local function get_mode_score(room_mode, is_win)
    local ret_value = 0
    local bp_cfg = divisionpoints_db:find_one(1)
    if room_mode == GameMode.TEAM then
        ret_value = is_win and bp_cfg.w_mode1 or bp_cfg.l_mode1
    elseif room_mode == GameMode.BOMB then
        ret_value = is_win and bp_cfg.w_mode2 or bp_cfg.l_mode2
    elseif room_mode == GameMode.ROOM then
        ret_value = is_win and bp_cfg.w_mode3 or bp_cfg.l_mode3
    end
    return ret_value
end

-- 获得连胜奖励分数
local function get_streak_win_score(count)
    local bp_cfg = divisionpoints_db:find_one(1)
    local win_scores = {
        [0] = 0,
        [1] = bp_cfg.w_streak1,
        [2] = bp_cfg.w_streak2,
        [3] = bp_cfg.w_streak3,
        [4] = bp_cfg.w_streak4,
        [5] = bp_cfg.w_streak5,
    }
    local capacity = tsize(win_scores)
    if count > capacity then
        count = capacity
    end
    return win_scores[count]
end

-- 获得队伍综合实力奖励分数
local function get_power_score(is_win, average_rate, median_rate)
    if not average_rate or not median_rate then
        return 0
    end
    local score = 0
    local bp_cfg = divisionpoints_db:find_one(1)
    local avg_low, median_low = bp_cfg.high_level_param[1], bp_cfg.high_level_param[2]
    if average_rate >= avg_low and median_rate >= median_low then
        if is_win then
            score = bp_cfg.w_bonus
        else
            score = bp_cfg.l_bonus
        end
    end
    return score
end

-- 获得无挂机行为奖励分数
local function get_hook_score(is_win)
    local bp_cfg = divisionpoints_db:find_one(1)
    if is_win then
        return bp_cfg.w_onhook
    else
        return bp_cfg.l_onhook
    end
end

-- 获得局内全局排名奖励分数【失败】
local function get_ds_score(is_win, pos)
    local bp_cfg = divisionpoints_db:find_one(1)
    local scores = {
        [1] = is_win and bp_cfg.w_score1 or bp_cfg.l_score1,
        [2] = is_win and bp_cfg.w_score2 or bp_cfg.l_score2,
        [3] = is_win and bp_cfg.w_score3 or bp_cfg.l_score3,
        [4] = is_win and bp_cfg.w_score4 or bp_cfg.l_score4,
        [5] = is_win and bp_cfg.w_score5 or bp_cfg.l_score5,
    }
    return scores[pos]
end

-- 根据星星数获取对应的段位
local function get_division_by_star(star)
    local tmp_star = mmin(star, DivisionStars.STRONGEST)
    local cfg_info = divisionstar_db:find_one(tmp_star)
    if cfg_info then
        return cfg_info.division or 1
    else
        log_err("[get_division_by_star] get cfg failed! star:%s tmp_star:%s", star, tmp_star)
        return 0
    end
end

-- 排位赛胜利处理
local function match_win(room_mode, player_settle, context)
    local old_stars = context.old_stars
    local old_brave_score = context.old_brave_score
    local old_division = get_division_by_star(old_stars)
    local cfg_info = division_db:find_one(old_division)
    if not cfg_info then
        log_err("[match_win] find division cfg failed! stars:%s", old_stars)
        return SettleCode.SETTLE_ERR_CFG
    end
    local star_cfg_info = divisionstar_db:find_one(mmin(old_stars, DivisionStars.STRONGEST))
    if not star_cfg_info then
        log_err("[match_win] find star cfg failed! stars:%s", old_stars)
        return SettleCode.SETTLE_ERR_CFG
    end
    local add_star = star_cfg_info.win
    if old_brave_score >= cfg_info.score_max then
        add_star = add_star + 1
    end
    local new_stars = old_stars + add_star
    local new_division = get_division_by_star(new_stars)
    local new_cfg_info = division_db:find_one(new_division)
    if not new_cfg_info then
        log_err("[match_win] find division cfg failed! stars:%s", new_stars)
        return SettleCode.SETTLE_ERR_CFG
    end
    local new_brave_score = 0
    local score_max = new_cfg_info.score_max
    if old_brave_score >= score_max then
        new_brave_score = old_brave_score - score_max
    end
    -- 勇者积分 = 玩家无挂机行为奖励 + 玩法模式奖励 + 连胜奖励 + 局内本队排名奖励 + 实力较强奖励
    local detail_scores = context.detail_scores
    if not player_settle.on_hook then
        -- 无挂机行为奖励积分
        local on_hook_score = get_hook_score(true)
        detail_scores.on_hook_score = on_hook_score
        -- 模式奖励积分
        local mode_score    = get_mode_score(room_mode, true)
        detail_scores.mode_score = mode_score
        -- 连胜奖励积分
        local streak_score  = get_streak_win_score(context.streak_win)
        detail_scores.streak_score = streak_score
        -- 局内表现奖励积分
        local display_score = get_ds_score(true, player_settle.team_pos)
        detail_scores.display_score = display_score
        -- 实力奖励积分
        local power_score   = get_power_score(true, player_settle.average_rate, player_settle.median_rate)
        detail_scores.power_score = power_score
        -- 勇者积分
        local add_score = on_hook_score + mode_score + streak_score + display_score + power_score
        new_brave_score = mmin(new_brave_score + add_score, score_max)
    end
    context.new_stars = new_stars
    context.new_division = new_division
    context.new_brave_score = new_brave_score
    return SUCCESS
end

-- 排位赛失败处理
local function match_lose(room_mode, player_settle, context)
    local old_stars = context.old_stars
    local old_brave_score = context.old_brave_score
    local old_division = get_division_by_star(old_stars)
    local cfg_info = division_db:find_one(old_division)
    if not cfg_info then
        log_err("[match_lose] find division cfg failed! stars:%s", old_stars)
        return SettleCode.SETTLE_ERR_CFG
    end
    local star_cfg_info = divisionstar_db:find_one(mmin(old_stars, DivisionStars.STRONGEST))
    if not star_cfg_info then
        log_err("[match_win] find star cfg failed! stars:%s", old_stars)
        return SettleCode.SETTLE_ERR_CFG
    end
    local minus_star = star_cfg_info.lose
    local new_brave_score = old_brave_score
    if old_brave_score >= cfg_info.score_protect then
        new_brave_score = old_brave_score - cfg_info.score_protect
        minus_star = minus_star - 1
    end
    local new_stars = mmax(0, old_stars - minus_star)
    local new_division = get_division_by_star(new_stars)
    local new_cfg_info = division_db:find_one(new_division)
    if not new_cfg_info then
        log_err("[match_lose] find division cfg failed! stars:%s", old_stars)
        return SettleCode.SETTLE_ERR_CFG
    end
    local detail_scores = context.detail_scores
    if not player_settle.on_hook then
        -- 无挂机行为奖励积分
        local on_hook_score = get_hook_score(false)
        detail_scores.on_hook_score = on_hook_score
        -- 模式奖励积分
        local mode_score    = get_mode_score(room_mode, false)
        detail_scores.mode_score = mode_score
        -- 局内表现奖励积分
        local display_score = get_ds_score(false, player_settle.team_pos)
        detail_scores.display_score = display_score
        -- 实力奖励积分
        local power_score   = get_power_score(false, player_settle.average_rate, player_settle.median_rate)
        detail_scores.power_score = power_score
        -- 勇者积分
        local add_score = on_hook_score + mode_score + display_score + power_score
        new_brave_score = mmin(new_brave_score + add_score, new_cfg_info.score_max)
    end
    context.new_stars = new_stars
    context.new_division = new_division
    context.new_brave_score = new_brave_score
    return SUCCESS
end

local SettleServlet = singleton()

function SettleServlet:__init()
    event_mgr:add_listener(self, "rpc_player_settle")
end

-- rpc通知lobby玩家结算处理
function SettleServlet:rpc_player_settle(player_id, room_id)
    log_debug("[SettleServlet][rpc_player_settle]->player_id:%s, room_id:%s", player_id, room_id)
    local player = player_mgr:get_player(player_id)
    if not player then
        log_err("[SettleServlet][rpc_player_settle]->list message failed! player_id:%s", player_id)
        return KernCode.LOGIC_FAILED
    end

    -- 读取rmsg
    local code, room_settle_data, player_settle, db_uuid = self:load_player_settle_data(room_id, player_id)
    if check_failed(code) then
        return code
    end
    log_debug("[SettleServlet][rpc_player_settle]->room_id:%s, room_settle_data:%s, player_settle:%s",
               room_id, serialize(room_settle_data), serialize(player_settle))
    -- 1.删除数据库中结算数据
    local ok, ret_code = rmsg_settlement:delete_message(player_id, db_uuid)
    if not ok or check_failed(ret_code) then
        log_err("[SettleServlet][rpc_player_settle] delete_message failed! player_id:%s, db_uuid:%s, ok:%s, ret_code:%s", player_id, db_uuid, ok, ret_code)
        return ret_code
    end
    self:calc_player_settle(player, room_settle_data, player_settle)
    if room_settle_data.room_mode == GameMode.ROOM then
        return self:room_settle(room_id, player_id, player, room_settle_data, player_settle)
    else
        return self:match_settle(room_id, player_id, player, room_settle_data, player_settle)
    end
end

--通知客户端结算数据
function SettleServlet:calc_player_settle(player, room_settle, player_settle)
    local room_mode = room_settle.room_mode
    local is_win = room_settle.winner_team == player_settle.team_id
    local mode_param = get_settle_mode_param(room_mode, is_win)
    local gain_exp = player:add_exp(player_settle.res_score * mode_param * EXP_PARAM)
    local gain_ideal = player:add_ideal(player_settle.res_score * mode_param * IDEAL_PARAM)
    local settle_ntf = {
        room_mode   = room_mode,
        gain_exp    = gain_exp,
        gain_ideal  = gain_ideal,
        room_id     = room_settle.room_id,
        winner_team = room_settle.winner_team,
        map_id      = room_settle.map_id,
        players     = room_settle.players,
    }
    log_info("[SettleServlet][calc_player_settle]->settle info:%s", serialize(settle_ntf))
    player:send(CSCmdID.NID_SETTLE_BATTLE_GAME_NTF, settle_ntf)
end

-- 自定义房间结算
-- !!!  [只参与战绩显示  不参与成就&任务相关的逻辑]  !!!
function SettleServlet:room_settle(room_id, player_id, player, room_settle, player_settle)
    self:update_player_standings(player_id, room_settle, player_settle)
    return SUCCESS
end

-- 匹配赛结算
function SettleServlet:match_settle(room_id, player_id, player, room_settle, player_settle)
    local room_mode     = room_settle.room_mode
    local cur_stars     = player:get_stars()
    local streak_win    = player:get_streak_win()
    -- 构建排位赛结算上下文内容context
    local context = {
        room_mode       = room_mode,
        old_stars       = cur_stars,
        new_stars       = cur_stars,
        streak_win      = streak_win,
        is_win          = player_settle.win,
        old_brave_score = player:get_brave_score(),
        division_id     = player:get_division_id(),
        detail_scores       = {
            mode_score      = 0,
            onhook_score    = 0,
            streak_score    = 0,
            display_score   = 0,
            power_score     = 0,
        },
    }
    local code
    log_info("[SettleServlet][match_settle]->settle info:%s", serialize(context))
    if player_settle.win then
        streak_win = streak_win + 1
        code = match_win(room_mode, player_settle, context)
    else
        code = match_lose(room_mode, player_settle, context)
    end
    if check_failed(code) then
        return code
    end
    -- 更新玩家排位数据&段位信息
    player:update_qualifying_data(context)

    -- 通知客户端排位赛结算信息
    local detail_scores = context.detail_scores
    local sync_info = {
        room_id         = room_id,
        room_mode       = room_mode,
        streak_win      = streak_win,
        is_win          = context.is_win,
        old_stars       = context.old_stars,
        stars           = context.new_stars,
        scores          = context.new_brave_score,
        mode_score      = detail_scores.mode_score,
        streak_score    = detail_scores.streak_score,
        display_score   = detail_scores.display_score,
        power_score     = detail_scores.power_score,
    }
    log_info("[SettleServlet][match_settle]->settle info:%s", serialize(sync_info))
    player:send(CSCmdID.NID_SETTLE_QUALIFYING_NTF, sync_info)
    -- 排位赛数据变化服务器内部通知
    self:player_qualifying_data_change(player, context)
    -- 同步排位数据到平台
    player:platform_qualifying_sync()
    -- 如果玩家在最强王者阶段变化上报到汇总服
    if context.old_stars >= DivisionStars.STRONGEST or context.new_stars >= DivisionStars.STRONGEST then
        router_mgr:call_center_master("rpc_strongest_finish_qualifying", player_id, context.new_stars)
    end
    self:update_player_standings(player_id, room_settle, player_settle)
    self:update_player_career(player, room_settle, player_settle)
    self:update_player_behavior(player, room_settle, player_settle)
end

-- 更新玩家战绩
function SettleServlet:update_player_standings(player_id, room_settle, player_settle)
    log_info("[SettleServlet][update_player_standings] player_id:%d, room_settle:%s", player_id, serialize(room_settle))
    local scores = {}
    local room_json_data = room_settle.json_data
    local score_str = room_json_data.bomb_win_turns or room_json_data.team_score
    if score_str then
        scores = ssplit(score_str, '-')
    end
    local standings_title = {
        room_id     = room_settle.room_id,
        game_mode   = room_settle.room_mode,
        play_mode   = room_settle.play_mode or 0,
        map_id      = room_settle.map_id,
        score1      = scores[1] or 0,
        score2      = scores[2] or 0,
        win         = player_settle.win,
        time        = otime(),
        fight_time  = room_json_data.game_time,
    }
    event_mgr:notify_trigger("evt_update_player_standings", player_id, standings_title)
end

-- 从数据库拉取结算数据
function SettleServlet:load_player_settle_data(room_id, player_id)
    local ret_data = rmsg_settlement:list_message(player_id)
    if not ret_data then
        log_err("[SettleServlet][load_player_settle] find list failed! player_id:%s, room_id:%s", player_id, room_id)
        return SettleCode.SETTLE_ERR_NOT_EXIST
    end
    -- 从rmsg_settlement表中获取结算相关数据
    local room_settle = nil
    local db_uuid     = nil
    for _, data in pairs(ret_data) do
        if data.body.room_id == room_id then
            room_settle = data.body
            db_uuid     = data.uuid
            break
        end
    end
    if not room_settle or not db_uuid then
        log_err("[SettleServlet][load_player_settle] find room_settle failed! player_id:%s, room_id:%s", player_id, room_id)
        return SettleCode.SETTLE_ERR_NOT_EXIST
    end
    local player_settle = nil
    for _, data in pairs(room_settle.players) do
        if data.player_id == player_id then
            player_settle = data
            break
        end
    end
    if not player_settle then
        return SettleCode.SETTLE_ERR_NOT_EXIST
    end
    return SUCCESS, room_settle, player_settle, db_uuid
end

-- 排位赛数据变化服务器内部通知
function SettleServlet:player_qualifying_data_change(player, context)
    -- todo:基于context进行服务器内部通知，比如星数变化，段位变化，总场数变化等，触发玩家任务/成就/目标模块
end

function SettleServlet:update_player_career(player, room_settle, player_settle)
    local context = {
        room_mode   = room_settle.room_mode,
        damage      = player_settle.damage,
        kill_num    = player_settle.kill_num,
        spray_area  = player_settle.spray_area,
        hit_head    = player_settle.hit_head,
        hit_count   = player_settle.hit_count,
        win         = player_settle.win,
        mvp         = player_settle.mvp,
    }
    player:update_cur_career_data(context)
    player:update_his_career_data(context)
    local player_id = player:get_player_id()
    if player_settle.mvp then
        event_mgr:notify_trigger("evt_achieve_update", player_id, AchieveType.HONOUR, AchieveHonourID.MVP, 1)
    end
    event_mgr:notify_trigger("evt_role_employ_update", player_id, room_settle.room_mode, player_settle.role_id, 1)
end

function SettleServlet:update_player_behavior(player, room_settle, player_settle)
    local player_id = player:get_player_id()
    local room_mode = room_settle.room_mode
    if player_settle.win then
        event_mgr:notify_trigger("ntf_behavior_event", player_id, BehaviorAttrID.TOTAL_WIN_ROUOND, player:get_behavior_attr(BehaviorAttrID.TOTAL_WIN_ROUOND)+1)
        if room_mode == GameMode.BOMB then
            event_mgr:notify_trigger("ntf_behavior_event", player_id, BehaviorAttrID.BOMB_WIN_ROUND, player:get_behavior_attr(BehaviorAttrID.BOMB_WIN_ROUND)+1)
            event_mgr:notify_trigger("ntf_behavior_event",player_id, BehaviorAttrID.BOMB_STREAK_WIN, player:get_behavior_attr(BehaviorAttrID.BOMB_STREAK_WIN)+1)
        elseif room_mode == GameMode.TEAM then
            event_mgr:notify_trigger("ntf_behavior_event", player_id, BehaviorAttrID.TEAM_WIN_ROUND, player:get_behavior_attr(BehaviorAttrID.TEAM_WIN_ROUND)+1)
            event_mgr:notify_trigger("ntf_behavior_event",player_id, BehaviorAttrID.TEAM_STREAK_WIN, player:get_behavior_attr(BehaviorAttrID.TEAM_STREAK_WIN)+1)
        end
    else
        if room_mode == GameMode.BOMB then
            event_mgr:notify_trigger("ntf_behavior_event", player_id, BehaviorAttrID.BOMB_STREAK_WIN, 0)
        elseif room_mode == GameMode.TEAM then
            event_mgr:notify_trigger("ntf_behavior_event", player_id, BehaviorAttrID.TEAM_STREAK_WIN, 0)
        end
    end

    event_mgr:notify_trigger("ntf_behavior_event", player_id, BehaviorAttrID.TOTAL_ROUND, player:get_behavior_attr(BehaviorAttrID.TOTAL_ROUND) + 1)
    if room_mode == GameMode.BOMB then
        event_mgr:notify_trigger("ntf_behavior_event", player_id, BehaviorAttrID.BOMB_ROUND, player:get_behavior_attr(BehaviorAttrID.BOMB_ROUND) + 1)
    elseif room_mode == GameMode.TEAM then
        event_mgr:notify_trigger("ntf_behavior_event", player_id, BehaviorAttrID.TEAM_ROUND, player:get_behavior_attr(BehaviorAttrID.TEAM_ROUND) + 1)
    end

    if player_settle.hit_head then
        local old_hit_value = player:get_behavior_attr(BehaviorAttrID.TOTAL_HIT_HEAD)
        event_mgr:notify_trigger("ntf_behavior_event", player_id, BehaviorAttrID.TOTAL_HIT_HEAD, old_hit_value + player_settle.hit_head)
        event_mgr:notify_trigger("ntf_behavior_event", player_id, BehaviorAttrID.ROUND_HIT_HEAD, player_settle.hit_head)
    end
    if player_settle.damage then
        local old_damage_value = player:get_behavior_attr(BehaviorAttrID.TOTAL_DAMAGE)
        event_mgr:notify_trigger("ntf_behavior_event", player_id, BehaviorAttrID.ROUND_DAMAGE, player_settle.damage)
        event_mgr:notify_trigger("ntf_behavior_event", player_id, BehaviorAttrID.TOTAL_DAMAGE, old_damage_value + player_settle.damage)
    end
    if player_settle.kill_num then
        local old_kill_value = player:get_behavior_attr(BehaviorAttrID.TOTAL_KILL)
        event_mgr:notify_trigger("ntf_behavior_event", player_id, BehaviorAttrID.ROUND_KILL, player_settle.kill_num)
        event_mgr:notify_trigger("ntf_behavior_event", player_id, BehaviorAttrID.TOTAL_KILL, old_kill_value + player_settle.kill_num)
    end
end

quanta.settle_servlet = SettleServlet()

return SettleServlet
