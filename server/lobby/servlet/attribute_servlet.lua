--attribute_servlet.lua
local otime         = os.time
local tinsert       = table.insert

local CSCmdID       = ncmd_cs.NCmdId

local event_mgr     = quanta.get("event_mgr")
local config_mgr    = quanta.get("config_mgr")
local dlog_mgr      = quanta.get("dlog_mgr")

local PlayerAttrID  = enum("PlayerAttrID")
local BehaviorAttrID= enum("BehaviorAttrID")

local level_db      = config_mgr:get_table("playerlevel")

local AttributeServlet = singleton()
function AttributeServlet:__init()
    event_mgr:add_trigger(self, "on_load_player_begin")
    event_mgr:add_trigger(self, "evt_exp_change")
    event_mgr:add_trigger(self, "evt_money_change")
    event_mgr:add_trigger(self, "evt_player_attribute_change")
    event_mgr:add_trigger(self, "ntf_dirty_attribute")
end

function AttributeServlet:on_load_player_begin(player_id, player)
    self:send_attribute_sync_ntf(player, player:get_attr_map())
end

-- 根据经验值获取player等级
function AttributeServlet:get_player_lv_by_exp(exp)
    local lv = 1
    local need_exp = 0
    local item = level_db:find_one(lv)
    while item do
        need_exp = need_exp + item.exp
        if exp <= need_exp then
            break
        end
        lv = lv + 1
        item = level_db:find_one(lv)
    end
    return lv
end

-- 脏属性推送回调
function AttributeServlet:ntf_dirty_attribute(player, attrs)
    self:send_attribute_sync_ntf(player, attrs)
end

-- 推送属性同步
function AttributeServlet:send_attribute_sync_ntf(player, attrs)
    local send_attrs = {}
    for _, attr in pairs(attrs or {}) do
        tinsert(send_attrs, {id = attr.id, value = tostring(attr.value), value_type = attr.value_type})
    end
    player:send(CSCmdID.NID_ATTRIBUTE_SYNC_NTF, { items = send_attrs })
end

-- 玩家属性变化事件
function AttributeServlet:evt_player_attribute_change(player, attr_id, old_value, new_value, reason)
    -- 经验更新必须保证关联的等级设置
    local player_id = player:get_player_id()
    if attr_id == PlayerAttrID.EXP then
        local old_level = player:get_level()
        local level = self:get_player_lv_by_exp(new_value)
        local value_changed = new_value - old_value
        local new_total_exp = player:get_behavior_attr(BehaviorAttrID.TOTAL_ACCOUNT_EXP) + value_changed
        if old_level ~= level then
            player:set_level(level)
        end
        event_mgr:notify_trigger("ntf_behavior_event", player_id, BehaviorAttrID.CUR_ACCOUNT_EXP, value_changed)
        event_mgr:notify_trigger("ntf_behavior_event", player_id, BehaviorAttrID.TOTAL_ACCOUNT_EXP, new_total_exp)
        event_mgr:notify_trigger("evt_exp_change", player, old_level, level, old_value, new_value)
    elseif attr_id == PlayerAttrID.LEVEL then
        player:platform_level_sync()
        event_mgr:notify_trigger("evt_player_level_update", player, new_value)
        event_mgr:notify_trigger("ntf_behavior_event", player_id, BehaviorAttrID.LEVEL_UP, new_value)
    elseif attr_id == PlayerAttrID.HERMES then
        local value_changed = new_value - old_value
        local new_hermes = player:get_behavior_attr(BehaviorAttrID.TOTAL_HERMES) + value_changed
        event_mgr:notify_trigger("ntf_behavior_event", player_id, BehaviorAttrID.CUR_HERMES, value_changed)
        event_mgr:notify_trigger("ntf_behavior_event", player_id, BehaviorAttrID.TOTAL_HERMES, new_hermes)
        event_mgr:notify_trigger("evt_money_change", player, attr_id, old_value, new_value, reason)
    elseif attr_id == PlayerAttrID.CRYSTAL then
        local value_changed = new_value - old_value
        local new_crystal = player:get_behavior_attr(BehaviorAttrID.TOTAL_CRYSTAL) + value_changed
        event_mgr:notify_trigger("ntf_behavior_event", player_id, BehaviorAttrID.CUR_CRYSTAL, value_changed)
        event_mgr:notify_trigger("ntf_behavior_event", player_id, BehaviorAttrID.TOTAL_CRYSTAL, new_crystal)
        event_mgr:notify_trigger("evt_money_change", player, attr_id, old_value, new_value, reason)
    elseif attr_id == PlayerAttrID.IDEAL then
        local value_changed = new_value - old_value
        local new_ideal = player:get_behavior_attr(BehaviorAttrID.TOTAL_IDEAL) + value_changed
        event_mgr:notify_trigger("ntf_behavior_event", player_id, BehaviorAttrID.CUR_IDEAL, value_changed)
        event_mgr:notify_trigger("ntf_behavior_event", player_id, BehaviorAttrID.TOTAL_IDEAL, new_ideal)
        event_mgr:notify_trigger("evt_money_change", player, attr_id, old_value, new_value, reason)
    end
end

-- 订阅经验变化
function AttributeServlet:evt_exp_change(player, old_lv, new_lv, old_exp, new_exp)
    local special_fields = {character_name=player:get_nick(), before_level=old_lv,before_exp=old_exp, after_exp=new_exp, use_time=otime()}
    if new_lv ~= old_lv then
        -- 精分上报-经验提升
        dlog_mgr:send_dlog_game_character_levelup({public_fields = player:build_dlog_public_fields(), special_fields = special_fields})
    end
    dlog_mgr:send_dlog_game_character_expup({public_fields = player:build_dlog_public_fields(), special_fields = special_fields})
end

-- 订阅货币变化
function AttributeServlet:evt_money_change(player, attr_id, old_value, new_value, reason)
    local special_fields = {}
    special_fields.chang_type = (new_value > old_value) and 1 or 0
    special_fields.money_type = (attr_id == PlayerAttrID.IDEAL) and 1 or 2
    special_fields.money_num  = new_value - old_value
    special_fields.before_num = old_value
    special_fields.after_num  = new_value
    special_fields.reason     = reason or 0
    -- 精分上报-货币流水
    dlog_mgr:send_dlog_game_money({public_fields = player:build_dlog_public_fields(), special_fields = special_fields})
end

-- export
quanta.attribute_servlet = AttributeServlet()

return AttributeServlet
