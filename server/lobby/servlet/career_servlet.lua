--career_servlet.lua
local log_info      = logger.info
local log_err       = logger.err
local tinsert       = table.insert

local event_mgr     = quanta.get("event_mgr")
local router_mgr    = quanta.get("router_mgr")
local career_mgr    = quanta.get("career_mgr")
local player_mgr    = quanta.get("player_mgr")
local config_mgr    = quanta.get("config_mgr")

local CSCmdID       = ncmd_cs.NCmdId
local KernCode      = enum("KernCode")
local CareerCode    = enum("CareerCode")
local CareerAttrID  = enum("CareerAttrID")
local AchieveType   = enum("AchieveType")
local AchieveHonourID   = enum("AchieveHonourID")

local headicon_db   = config_mgr:get_table("playerheadicon")

local CareerServlet = singleton()
function CareerServlet:__init()
    --SS
    event_mgr:add_listener(self, "rpc_career_info_req")
    event_mgr:add_listener(self, "rpc_career_laud_req")
    --CS
    event_mgr:add_cmd_listener(self, CSCmdID.NID_CAREER_INFO_REQ, "on_career_info_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_CAREER_GET_HEAD_ICONS_REQ, "on_get_head_icons_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_CAREER_SET_HEAD_ICON_REQ, "on_set_head_icon_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_CAREER_LAUD_REQ, "on_laud_req")
    --headicon_db
    self.headicons = {}
    for _, cfg in headicon_db:iterator() do
        tinsert(self.headicons, cfg.id)
    end
end

function CareerServlet:on_career_info_req(player, player_id, body, session_id)
    local target_id = body.player_id or player_id
    log_info("[CareerServlet][on_career_info_req] player_id=%s, target_id=%s", player_id, target_id)
    if player_mgr:get_player(target_id) then
        local code, career_info = career_mgr:get_image_packege(target_id)
        player:callback(CSCmdID.NID_CAREER_INFO_RES, {code = code, career_info = career_info}, session_id)
        return
    end

    local ok, lobby = router_mgr:call_index_hash(target_id, "query_player_lobby", target_id)
    if not ok then
        player:callback(CSCmdID.NID_CAREER_INFO_RES, {code = KernCode.RPC_FAILED}, session_id)
        return
    end

    if lobby <= 0 then
        local code, career_info = career_mgr:get_image_packege(target_id)
        player:callback(CSCmdID.NID_CAREER_INFO_RES, {code = code, career_info = career_info}, session_id)
        return
    end

    local ok1, code, career_info = router_mgr:call_target(lobby, "rpc_career_info_req", player_id, target_id)
    log_info("[CareerServlet][on_career_info_req] get career_info. ok=%s, player_id=%s, target_id:%s, code=%s", ok1, player_id, target_id, code)
    player:callback(CSCmdID.NID_CAREER_INFO_RES, {code = code, career_info = career_info}, session_id)
end

function CareerServlet:on_get_head_icons_req(player, player_id, body, session_id)
    log_info("[CareerServlet][on_get_head_icons_req] player_id=%s", player_id)
    player:callback(CSCmdID.NID_CAREER_GET_HEAD_ICONS_RES, { icons = self.headicons }, session_id)
end

function CareerServlet:on_set_head_icon_req(player, player_id, body, session_id)
    local icon_id = body.icon_id
    log_info("[CareerServlet][on_set_head_icon_req] player_id=%s, icon_id=%s", player_id, icon_id)
    local icon = headicon_db:find_one(icon_id)
    if not icon then
        log_err("[CareerServlet][on_set_head_icon_req] icon config not exist, icon_id=%s", icon_id)
        player:callback(CSCmdID.NID_CAREER_SET_HEAD_ICON_RES, {code = CareerCode.HEAD_ICON_NO_EXIST}, session_id)
        return
    end
    if not icon.default then
        log_err("[CareerServlet][on_set_head_icon_req] icon_id=%s, default=%s", icon_id, icon.default)
        player:callback(CSCmdID.NID_CAREER_SET_HEAD_ICON_RES, {code = CareerCode.HEAD_ICON_NO_GET}, session_id)
        return
    end
    player:set_icon(icon_id)
    player:callback(CSCmdID.NID_CAREER_SET_HEAD_ICON_RES, {code = KernCode.SUCCESS, icon_id = icon_id}, session_id)
end

function CareerServlet:on_laud_req(player, player_id, body, session_id)
    local target_id = body.player_id
    log_info("[CareerServlet][on_laud_req] player_id=%s, target_id=%s", player_id, target_id)
    if target_id == player_id then
        player:callback(CSCmdID.NID_CAREER_LAUD_RES, {code = KernCode.OPERATOR_SELF}, session_id)
        return
    end

    local ok, lobby = router_mgr:call_index_hash(target_id, "query_player_lobby", target_id)
    if not ok or lobby <= 0 then
        player:callback(CSCmdID.NID_CAREER_LAUD_RES, {code = KernCode.RPC_FAILED}, session_id)
        return
    end
    local ok1, code = router_mgr:call_target(lobby, "rpc_career_laud_req", player_id, target_id)
    log_info("[CareerServlet][on_laud_req] send res: ok=%s, player_id=%s, target_id:%s, code=%s", ok1, player_id, target_id, code)
    player:callback(CSCmdID.NID_CAREER_LAUD_RES, {code = ok1 and code or KernCode.RPC_FAILED}, session_id)
end

function CareerServlet:rpc_career_info_req(player_id, target_id)
    log_info("[CareerServlet][rpc_career_info_req] player_id=%s, target_id=%s", player_id, target_id)
    return career_mgr:get_image_packege(target_id)
end

function CareerServlet:rpc_career_laud_req(player_id, target_id)
    log_info("[CareerServlet][rpc_career_laud_req] player_id=%s, target_id=%s", player_id, target_id)
    local player = player_mgr:get_player(target_id)
    if not player then
        log_err("[CareerServlet][rpc_career_laud_req] player no on line, target_id=%s", target_id)
        return KernCode.PLAYER_NOT_EXIST
    end

    local laud = player:get_career_his_attr(CareerAttrID.LAUD)
    player:set_career_his_attr(CareerAttrID.LAUD, laud + 1)

    event_mgr:notify_trigger("evt_achieve_update", player_id, AchieveType.HONOUR, AchieveHonourID.LAUD, 1)

    return KernCode.SUCCESS
end

quanta.career_servlet = CareerServlet()

return CareerServlet