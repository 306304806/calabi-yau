--report_servlete.lua

local otime         = os.time
local odate         = os.date
local log_err       = logger.err
local log_info      = logger.info
local env_get       = environ.get
local env_number    = environ.number
local check_failed  = utility.check_failed

local PeriodTime    = enum("PeriodTime")

local timer_mgr     = quanta.get("timer_mgr")
local router_mgr    = quanta.get("router_mgr")
local client_mgr    = quanta.get("client_mgr")
local config_mgr    = quanta.get("config_mgr")
local dlog_mgr      = quanta.get("dlog_mgr")

local utility_db    = config_mgr:get_table("utility")

-- 汇报服务
local ReportServlet = singleton()
function ReportServlet:__init()
    self.client_port = client_mgr:get_port()
    self.client_ip = env_get("QUANTA_HOST_IP")
    self.area_id = env_number("QUANTA_AREA_ID")

    router_mgr:watch_service_ready(self, "dirsvr")
    timer_mgr:loop(PeriodTime.SECOND_3_MS, function()
        self:on_timer_lobby_report()
    end)
    timer_mgr:loop(PeriodTime.MINUTE_MS, function()
        self:on_timer_dlog_report()
    end)
end

-- 汇报定时器
function ReportServlet:on_timer_lobby_report()
    local rpc_req = {
        id           = quanta.id,
        host         = self.client_host,
        listen_ip    = self.client_ip,
        listen_port  = self.client_port,
        area_id      = self.area_id,
        online_count = client_mgr:get_session_count(),
    }
    router_mgr:call_dirsvr_all("rpc_lobby_report", rpc_req)
end

-- 服务器上线
function ReportServlet:on_service_ready(quanta_id, service, router_id)
    log_info("[ReportServlet:on_service_ready] id=%s, service=%s", quanta_id, service)
    local rpc_req = {
        id           = quanta.id,
        host         = self.client_host,
        listen_ip    = self.client_ip,
        listen_port  = self.client_port,
        area_id      = self.area_id,
        online_count = client_mgr:get_session_count(),
    }
    local ok, ec = router_mgr:router_call(router_id, quanta_id, "rpc_lobby_report", rpc_req)
    if check_failed(ec) then
        log_err("[ReportServlet][on_service_ready] faild: ok=%s, ec=%s", ok, ec)
    end
end

-- 精分上报-在线人数
function ReportServlet:on_timer_dlog_report()
    local special_fields = {
        zone_id     = self.area_id,
        event_time  = odate("%Y-%m-%d %H:%M:%S", otime()),
        game_appkey = utility_db:find_value("value", "dlog_app_key"),
        platform    = utility_db:find_value("value", "dlog_plat_id"),
        online_num  = client_mgr:get_session_count(),
    }
    dlog_mgr:send_dlog_game_onlinenum({special_fields = special_fields})
end

-- export
quanta.report_servlete    = ReportServlet()

return ReportServlet
