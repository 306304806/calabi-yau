--achievement_servlet.lua
local tjoin             = table_ext.join
local log_info          = logger.info
local log_err           = logger.err
local serialize         = logger.serialize
local env_number        = environ.number

local CSCmdID           = ncmd_cs.NCmdId
local AchieveType       = enum("AchieveType")
local AchieveHonourID   = enum("AchieveHonourID")
local BattleEvtID       = enum("BattleEvtID")

local plat_api          = quanta.get("plat_api")
local event_mgr         = quanta.get("event_mgr")
local router_mgr        = quanta.get("router_mgr")
local player_mgr        = quanta.get("player_mgr")
local achievement_mgr   = quanta.get("achievement_mgr")

local AchievementServlet = singleton()
function AchievementServlet:__init()
    --SS
    event_mgr:add_trigger(self, "evt_friend_add")
    event_mgr:add_trigger(self, "evt_achievement_update")
    event_mgr:add_listener(self, "rpc_achievement_list_req")
    event_mgr:add_listener(self, "rpc_player_achieve_reach")
    event_mgr:add_listener(self, "rpc_player_battle_update")
    --CS
    event_mgr:add_cmd_listener(self, CSCmdID.NID_ACHIEVEMENT_LIST_REQ, "on_achievement_list_req")
    --subscribe
    plat_api:subscribe_register(env_number("QUANTA_AREA_ID"), "evt_friend_add")
end

function AchievementServlet:on_achievement_list_req(player, player_id, body, session_id)
    local target_id = body.player_id or player_id
    if body.player_id == 0 then
        target_id = player_id
    end

    log_info("[AchievementServlet][on_achievement_list_req] player_id=%s, target_id=%s", player_id, target_id)
    local ok, lobby = router_mgr:call_index_hash(target_id, "query_player_lobby", target_id)
    if not ok then
        log_err("[AchievementServlet][on_achievement_list_req] player_id=%s, target_id=%s", player_id, target_id)
        return
    end

    if lobby <= 0 then
        local achievement_list = achievement_mgr:get_player_achieves(target_id)
        player:callback(CSCmdID.NID_ACHIEVEMENT_LIST_RES, achievement_list, session_id)
        return
    end

    local ok1, achievement_list = router_mgr:call_target(lobby, "rpc_achievement_list_req", player_id, target_id)
    log_info("[AchievementServlet][on_achievement_list_req] get achievement_list. ok=%s, player_id=%s, target_id:%s", ok1, player_id, target_id)
    player:callback(CSCmdID.NID_ACHIEVEMENT_LIST_RES, achievement_list, session_id)
end

function AchievementServlet:rpc_achievement_list_req(player_id, target_id)
    local achievement_list = achievement_mgr:get_player_achieves(target_id)
    return achievement_list
end

function AchievementServlet:evt_friend_add(friend_data)
    local player_id, count = friend_data.player_id, friend_data.count
    log_info("[AchievementServlet][evt_friend_add] player_id=%s, count=%s", player_id, count)
    event_mgr:notify_trigger("evt_achieve_update", player_id, AchieveType.HONOUR, AchieveHonourID.FRIENDS, count)
end

-- 成就达成
function AchievementServlet:rpc_player_achieve_reach(player_id, achievement_ids)
    log_info("[AchievementServlet][rpc_player_achieve_reach] player_id=%s, achievement_ids=%s", player_id, serialize(achievement_ids))
    if achievement_ids then
        achievement_mgr:evt_achieve_reach(player_id, achievement_ids)
        self:evt_achievement_update(player_id, achievement_ids)
    end
end

-- 战斗数据更新
function AchievementServlet:rpc_player_battle_update(player_id, battle_evts)
    log_info("[AchievementServlet][rpc_player_battle_update] player_id=%s, battle_evts=%s", player_id, serialize(battle_evts))
    local honour_achieve_evt_map = {
        [BattleEvtID.PLACEBOMB] = AchieveHonourID.PLACE_BOMB,
        [BattleEvtID.DEFUSEBOMB] = AchieveHonourID.DEFUSE_BOMB,
        [BattleEvtID.DMAGE] = AchieveHonourID.DAMAGE,
        [BattleEvtID.KILLNUM] = AchieveHonourID.KILL,
        [BattleEvtID.HITHEAD] = AchieveHonourID.HIT_HEAD,
        [BattleEvtID.SPRAYAREA] = AchieveHonourID.SPARY,
    }
    local achievement_ids = {}
    for _, evt_data in pairs(battle_evts) do
        local achieve_param_type = honour_achieve_evt_map[evt_data.evt_id]
        local val = tonumber(evt_data.value)
        if achieve_param_type and val > 0 then
            local ids = achievement_mgr:evt_achieve_update(player_id, AchieveType.HONOUR, achieve_param_type, val, true)
            if ids then
                tjoin(ids, achievement_ids)
            end
        end
    end
    self:evt_achievement_update(player_id, achievement_ids)
end

-- 成就更新推送
function AchievementServlet:evt_achievement_update(player_id, achievement_ids)
    log_info("[AchievementServlet][evt_achievement_update] player_id=%s, achievement_ids=%s", player_id, serialize(achievement_ids))
    local achievement_list = achievement_mgr:get_player_achieves(player_id, achievement_ids)
    local player = player_mgr:get_player(player_id)
    if player then
        player:send(CSCmdID.NID_ACHIEVEMENT_LIST_RES, achievement_list)
    end
end

quanta.achievement_servlet = AchievementServlet()

return AchievementServlet