-- dropstar_servlet.lua
local log_debug     = logger.debug

local KernCode      = enum("KernCode")
local DivisionStars = enum("DivisionStars")
local SUCCESS       = KernCode.SUCCESS

local event_mgr     = quanta.get("event_mgr")
local player_mgr    = quanta.get("player_mgr")
local rmsg_drop_star= quanta.get("rmsg_drop_star")

local DropStarServlet = singleton()
function DropStarServlet:__init()
    event_mgr:add_trigger(self, "on_load_player_begin")
    event_mgr:add_listener(self, "rpc_player_drop_star_ntf")
end

-- 检查掉星
function DropStarServlet:on_load_player_begin(player_id, player)
    if player:get_stars() >= DivisionStars.STRONGEST then
        self:check_drop_star(player, player_id)
    end
end

-- 通知玩家掉星
function DropStarServlet:rpc_player_drop_star_ntf(player_id)
    log_debug("[DropStarServlet][rpc_player_drop_star_ntf]->player_id:%s", player_id)
    local player = player_mgr:get_player(player_id)
    if player then
        self:check_drop_star(player, player_id)
    end
    return SUCCESS
end

-- 掉星处理
function DropStarServlet:check_drop_star(player, player_id)
    local ret_data = rmsg_drop_star:list_message(player_id)
    if ret_data then
        local drop_star = 0
        for _, data in pairs(ret_data) do
            drop_star = data.body.drop_star + drop_star
            rmsg_drop_star:delete_message(player_id, data.uuid)
        end
        if drop_star > 0 then
            local old_star = player:get_stars()
            if drop_star < old_star then
                local new_star = old_star - drop_star
                player:set_stars(new_star)
            end
        end
    end
end

-- export
quanta.dropstar_servlet    = DropStarServlet()

return DropStarServlet
