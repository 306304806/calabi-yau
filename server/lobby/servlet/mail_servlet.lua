--mail_servlet.lua
local tinsert       = table.insert
local log_info      = logger.info
local env_number    = environ.number
local check_success = utility.check_success

local ResAddType    = enum("ResAddType")

local plat_api      = quanta.get("plat_api")
local event_mgr     = quanta.get("event_mgr")
local player_mgr    = quanta.get("player_mgr")

-- 邮件服务
local MailServlet = singleton()
function MailServlet:__init()
    self:setup()
end

function MailServlet:setup()
    --邮件附件领取通知
    event_mgr:add_trigger(self, "evt_mail_attach_take")
    plat_api:subscribe_register(env_number("QUANTA_AREA_ID"), "evt_mail_attach_take")
    --领取未领取的附件
    event_mgr:add_trigger(self, "on_load_player_end")
end

--加载结束
function MailServlet:on_load_player_end(player_id, player)
    self:evt_mail_attach_take({player_id = player_id})
end

-- 客户端请求发送邮件
function MailServlet:evt_mail_attach_take(data)
    local player_id = data.player_id
    log_info("[MailServlet][evt_mail_attach_take] player_id = %s", player_id)
    local player = player_mgr:get_player(player_id)
    if player then
        local code, all_attach_items = plat_api:take_mail_attach(player_id)
        if check_success(code) then
            local mail_ids = {}
            local items_to_add = {}
            for mail_id, attach_items in pairs(all_attach_items or {}) do
                for _, item in pairs(attach_items or {}) do
                    tinsert(items_to_add, {item_id = item.item_id, num = item.item_count, add_reason = ResAddType.MAIL_ATTACH, expire_time = 0})
                end
                tinsert(mail_ids, mail_id)
            end
            -- 道具添加到玩家身上
            if #items_to_add > 0 then
                player:add_items(items_to_add)
            end
            -- 删除邮件附件
            if #mail_ids > 0 then
                plat_api:del_mail_attach(player_id, mail_ids)
            end
        end
    end
end

-- 导出
quanta.mail_servlet = MailServlet()

return MailServlet
