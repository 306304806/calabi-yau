-- shop_servlet.lua
local tinsert       = table.insert
local log_info      = logger.info
local log_debug     = logger.debug
local log_err       = logger.err
local serialize     = logger.serialize
local check_failed  = utility.check_failed
local check_success = utility.check_success

local KernCode      = enum("KernCode")
local AttrCode      = enum("AttrCode")
local ShopCode      = enum("ShopCode")

local SUCCESS       = KernCode.SUCCESS
local CSCmdID       = ncmd_cs.NCmdId

local ResAddType    = enum("ResAddType")
local BagCode       = enum("BagCode")
local OrderType     = enum("OrderType")
local CurrencyType  = enum("CurrencyType")
local ItemType      = enum("ItemType")

local plat_api      = quanta.get("plat_api")
local event_mgr     = quanta.get("event_mgr")
local config_mgr    = quanta.get("config_mgr")
local dlog_mgr      = quanta.get("dlog_mgr")
local item_util     = quanta.get("item_util")

local utility_db    = config_mgr:get_table("utility")
local currency_db   = config_mgr:get_table("currency")

local ShopServlet = singleton()
function ShopServlet:__init()
    -- CS
    event_mgr:add_cmd_listener(self, CSCmdID.NID_SHOP_PAY_SELF_BUY_REQ, "on_pay_self_buy_order")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_SHOP_PAY_GIVING_REQ,   "on_pay_giving_order")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_SHOP_PAY_CLAIMING_REQ, "on_pay_claiming_order")
end

-- 扣钱
local function deduct_money(player, pay_info)
    local item_cfg = currency_db:find_one(pay_info.pay_currency_id)
    if not item_cfg then
        return KernCode.PARAM_ERROR
    end

    local currency_type = pay_info.pay_currency_id
    if not player:del_currency_item(currency_type, pay_info.total) then
        if currency_type == CurrencyType.IDEAL then
            return AttrCode.NEED_MORE_IDEAL
        elseif currency_type == CurrencyType.Hermes then
            return AttrCode.NEED_MORE_HERMES
        elseif currency_type == CurrencyType.CRYSTAL then
            return AttrCode.NEED_MORE_CRYSTAL
        else
            return ShopCode.PAY_FAILED
        end
    end

    return SUCCESS
end

local function shop_buy_role(player, role_id, expire_time)
    local add_cmd = {
                        role_id     = role_id,
                        life_time   = expire_time,
                        reason      = ResAddType.SHOP_BUY,
                    }
    player:add_role(add_cmd)
    local role = player:get_role(role_id)
    if role then
        return true
    else
        return false
    end
end

local function shop_buy_role_skin(player, role_skin_id, expire_time)
    local add_cmd = {
        role_skin_id = role_skin_id,
        life_time    = expire_time,
    }

    player:add_role_skin(add_cmd)
    return true
end

-- 自购发货
local function send_to_reciver(player, shipment_info)
    -- 若购买的是背包里的物品或者武器喷漆之类物品走add_items流程
    local item_list = {}
    for _, data in ipairs(shipment_info.items) do
        local item_id = data.item_id
        if item_util:is_weapon_item_type(item_id) or item_util:is_decal_item_type(item_id) or item_util:is_bag_item_type(item_id) then
            tinsert(item_list, {item_id = item_id, num = data.item_amount, add_reason = ResAddType.SHOP_BUY, expire_time = 0})
        else
            local item_type = item_util:get_item_type(item_id)
            if item_type == ItemType.Role then
                shop_buy_role(player, item_id, 0)
            elseif item_type == ItemType.RoleSkin then
                shop_buy_role_skin(player, item_id, 0)
            elseif item_type == ItemType.BagItem_AdvanceBP then
                player:gain_battlepass_vip(0)
            elseif item_type == ItemType.BagItem_AdvanceBPPlus then
                player:gain_battlepass_vip(utility_db:find_value("value", "battlepass_senior_explores"))
            elseif item_type == ItemType.BagItem_Expolre then
                player:add_explore(data.item_amount * shipment_info.buy_count)
            end
        end
    end

    if next(item_list) then
        local ret, result_list = player:add_items(item_list)
        if not ret then
            log_err("[ShopServlet][send_to_reciver]failed! reciver_id:%s, item_list:%s", player.player_id, serialize(item_list))
            return BagCode.BAG_ERR_ITEM_NOT_EXIST
        end

        log_info("[ShopServlet][send_to_reciver] self buy success send! player_id:%s, buy_items:%s, result_list:%s",
              player.player_id, serialize(item_list), serialize(result_list))
    end

    return SUCCESS
end

-- 支付自购订单
local function player_pay_order(player, player_id, order_id)
    if not order_id then
        return KernCode.PARAM_ERROR
    end
    -- 从平台获取订单支付信息
    local code, pay_info = plat_api:order_pay_info_req(player_id, order_id)
    if check_failed(code) then
        log_err("[ShopServlet][player_pay_order]->get pay info failed! player_id:%s, order_id:%s", player_id, order_id)
        return code
    end

    -- 扣钱
    code = deduct_money(player, pay_info)
    if check_failed(code) then
        log_err("[ShopServlet][player_pay_order]->pay for order money not enough! player_id:%s, order_id:%s", player_id, order_id)
        return code
    end

    -- 通知平台支付完成
    local ret_code, shipment_info = plat_api:order_pay_finish_req(player_id, order_id)
    if check_failed(ret_code) then
        log_err("[ShopServlet][player_pay_order]->pay ntf failed! player_id:%s, order_id:%s", player_id, order_id)
        return ret_code
    end

    -- 如果是自购则立即发货
    if shipment_info and shipment_info.order_type == OrderType.SELF_BUY then
        local ec = send_to_reciver(player, shipment_info)
        if check_failed(ec) then
            return ec
        end
    end

    return SUCCESS, shipment_info
end

-- 请求支付自购订单
function ShopServlet:on_pay_self_buy_order(player, player_id, body, session_id)
    local code, shipment_info = player_pay_order(player, player_id, body.order_id )
    local resp = { code = code, item_list = {}}
    if check_success(code) and shipment_info then
        for _, data in ipairs(shipment_info.items) do
            tinsert(resp.item_list, {item_id = data.item_id, item_count = data.item_amount,})
        end
    end
    log_debug("[ShopServlet][on_pay_self_buy_order]->player_id:%s, resp:%s", player_id, serialize(resp))
    player:callback(CSCmdID.NID_SHOP_PAY_SELF_BUY_RES, resp, session_id)

    if check_success(resp.code) then
        -- 精分上报-商城购买
        local buy_item = shipment_info.items[1]
        if buy_item then
            local special_fields = {}
            special_fields.mall_type   = shipment_info.mall_type
            special_fields.money_type  = shipment_info.pay_currency_id
            special_fields.mall_name   = quanta_const.SHOP_NAME[special_fields.money_type]
            special_fields.money_num   = shipment_info.price * shipment_info.buy_count
            special_fields.item_type   = item_util:get_item_type(buy_item.item_id)
            special_fields.item_id     = buy_item.item_id
            special_fields.item_num    = buy_item.item_amount * shipment_info.buy_count
            dlog_mgr:send_dlog_game_mall({public_fields = player:build_dlog_public_fields(), special_fields = special_fields})
        end
    end
end

-- 请求支付赠送订单
function ShopServlet:on_pay_giving_order(player, player_id, body, session_id)
    log_debug("[ShopServlet][on_pay_giving_order]->player_id:%s, body:%s", player_id, serialize(body))
    local code = player_pay_order(player, player_id, body.order_id)
    local resp = {code = code}
    log_debug("[ShopServlet][on_pay_giving_order]->player_id:%s, code:%s", player_id, resp.code)
    player:callback(CSCmdID.NID_SHOP_PAY_GIVING_RES, resp, session_id)
end

-- 请求支付索要订单
function ShopServlet:on_pay_claiming_order(player, player_id, body, session_id)
    log_debug("[ShopServlet][on_pay_claiming_order]->player_id:%s, body:%s", player_id, serialize(body))
    local code = player_pay_order(player, player_id, body.order_id)
    local resp = {code = code}
    log_debug("[ShopServlet][on_pay_claiming_order]->player_id:%s, code:%s", player_id, resp.code)
    player:callback(CSCmdID.NID_SHOP_PAY_CLAIMING_RES, resp, session_id)
end

-- export
quanta.shop_servlet    = ShopServlet()

return ShopServlet
