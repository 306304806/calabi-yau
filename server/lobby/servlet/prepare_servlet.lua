--prepare_servlet.lua

local log_debug         = logger.debug
local log_err           = logger.err
local serialize         = logger.serialize
local tinsert           = table.insert
local check_failed      = utility.check_failed

local BagCode           = enum("BagCode")
local KernCode          = enum("KernCode")
local WeaponSlot        = enum("WeaponSlot")
local ResAddType        = enum("ResAddType")

local SUCCESS           = KernCode.SUCCESS
local CSCmdID           = ncmd_cs.NCmdId

local event_mgr         = quanta.get("event_mgr")
local config_mgr        = quanta.get("config_mgr")
local dlog_mgr          = quanta.get("dlog_mgr")
local item_util         = quanta.get("item_util")

local role_db           = config_mgr:get_table("role")
local roleprofile_db    = config_mgr:get_table("roleprofile")

local PrepareServlet = singleton()
function PrepareServlet:__init()
    self:setup()
end

function PrepareServlet:setup()
    -- 内部玩家事件注册
    event_mgr:add_trigger(self, "on_load_player_begin")
    -- 添加触发器
    event_mgr:add_trigger(self, "ntf_bag_items_expired")
    event_mgr:add_trigger(self, "evt_player_add_role")
    event_mgr:add_trigger(self, "evt_player_role_timeout")

    -- 客户端消息注册
    event_mgr:add_cmd_listener(self, CSCmdID.NID_EQUIP_WEAPON_REQ             , "on_equip_weapon_req")              -- 角色装备武器请求
end

-- 加载玩家数据开始触发回调
function PrepareServlet:on_load_player_begin(player_id, player)
    local send_data = { roles = {}}
    for role_id in pairs(player:get_roles() or {}) do
        tinsert(send_data.roles, player:get_role_prepare_data(role_id))
    end
    log_debug("[PrepareServlet][sync_player_prepare]->ntf:%s", serialize(send_data))
    player:send(CSCmdID.NID_ROLE_PREPARE_SYNC_NTF, send_data)
end

local function check_equip_req_param(player, player_id, role_id, weapon_uuid, slot_pos)
    local role = player:get_role(role_id)
    if not role then
        log_err("[PrepareServlet][check_equip_req_param] get role failed! player_id:%s, role_id:%s", player_id, role_id)
        return KernCode.PARAM_ERROR
    end
    if WeaponSlot.MIN > slot_pos or WeaponSlot.MAX < slot_pos then
        log_err("[PrepareServlet][check_equip_req_param] slot_pos error! player_id:%s, slot_pos:%s", player_id, slot_pos)
        return KernCode.PARAM_ERROR
    end
    local weapon = player:get_weapon_item_by_uuid(weapon_uuid)
    if not weapon then
        return BagCode.BAG_ERR_ITEM_NOT_EXIST
    end
    if not weapon:can_equip_slot(slot_pos) then
        return KernCode.PARAM_ERROR
    end
    return SUCCESS
end

-- 角色装备武器请求
function PrepareServlet:on_equip_weapon_req(player, player_id, body, session_id)
    log_debug("[PrepareServlet][on_equip_weapon_req]->player_id:%s, msg:%s", player_id, serialize(body))
    local weapon_uuid   = body.weapon_uuid
    local role_id       = body.role_id
    local slot_pos      = body.slot_pos
    local code = check_equip_req_param(player, player_id, role_id, weapon_uuid, slot_pos)
    if check_failed(code) then
        log_err("[PrepareServlet][on_equip_weapon_req] failed! player_id:%s, code:%s", player_id, code)
        player:callback(CSCmdID.NID_EQUIP_WEAPON_RES, { code = code }, session_id)
        return
    end

    player:prepare_equip_weapon(role_id, slot_pos, weapon_uuid)
    player:send(CSCmdID.NID_ROLE_PREPARE_NTF, player:get_role_prepare_data(role_id))

    local res = {code = SUCCESS, role_id = role_id, weapon_uuid = weapon_uuid, slot_pos = slot_pos}
    player:callback(CSCmdID.NID_EQUIP_WEAPON_RES, res, session_id)
end

--备战模块处理订阅物品过期事件处理
function PrepareServlet:ntf_bag_items_expired(player, expired_items)
    for expired_item_uuid, itm_id in pairs(expired_items) do
        if item_util:is_weapon_item_type(itm_id) then
            local ret_roles = player:get_weapon_prepared_roles(expired_item_uuid)
            for role_id in pairs(ret_roles or {}) do
                player:replace_prepared_weapon(expired_item_uuid)
                player:send(CSCmdID.NID_ROLE_PREPARE_NTF, player:get_role_prepare_data(role_id))
            end
        end
    end
end

-- 通知获得新角色
function PrepareServlet:evt_player_add_role(player, role, add_reason)
    local sync_flag = true
    if add_reason == ResAddType.ROLE_DEFAULT then
        sync_flag = false
    end
    local role_id = role:get_role_id()
    if player:add_role_default_item(role_id, sync_flag) then
        if sync_flag then
            player:send(CSCmdID.NID_ROLE_PREPARE_NTF, player:get_role_prepare_data(role_id))
        end
    end
    local role_cfg        = role_db:find_one(role_id)
    local roleprofile_cfg = roleprofile_db:find_one(role_id)
    if role_cfg and roleprofile_cfg then
        local special_fields = {}
        special_fields.role_id         = role_id
        special_fields.role_name       = roleprofile_cfg.name_cn
        special_fields.role_level      = 1
        special_fields.operation_type  = 1010
        -- 精分上报-角色流水
        dlog_mgr:send_dlog_game_role_klbq({public_fields = player:build_dlog_public_fields(), special_fields = special_fields})
    else
        log_err("[PrepareServlet][evt_player_add_role] can't find config: role_id=%s", role_id)
    end
end

-- 角色到期
function PrepareServlet:evt_player_role_timeout(player, role)
    -- 需跟策划商量角色到期处理备战数据
end

quanta.prepare_servlet = PrepareServlet()

return PrepareServlet
