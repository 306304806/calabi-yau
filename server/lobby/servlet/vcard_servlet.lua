-- vcard_servlet.lua
local log_debug     = logger.debug

local CSCmdID       = ncmd_cs.NCmdId

local KernCode      = enum("KernCode")
local VcardCode     = enum("VcardCode")
local PlayerAttrID  = enum("PlayerAttrID")

local event_mgr     = quanta.get("event_mgr")

local VCardServlet = singleton()
function VCardServlet:__init()
    event_mgr:add_trigger(self, "on_load_player_begin")
    event_mgr:add_trigger(self, "ntf_vcard_resource_sync")      -- 名片资源更新
    event_mgr:add_trigger(self, "ntf_vcard_resource_timeout")   -- 名品资源超时

    event_mgr:add_cmd_listener(self, CSCmdID.NID_SELF_VCARD_UPDATE_REQ, "on_self_vcard_update_req")
end

function VCardServlet:on_load_player_begin(player_id, player)
    -- 推送名片资源
    local vcard_ntf = player:pack_vcard_resource()
    player:send(CSCmdID.NID_VCARD_RESOURCE_SYNC_NTF, vcard_ntf)
end

-- 进程内通知：名片框更新
function VCardServlet:ntf_vcard_resource_sync(player)
    local vcard_ntf = player:pack_vcard_resource()
    player:send(CSCmdID.NID_VCARD_RESOURCE_SYNC_NTF, vcard_ntf)
end

function VCardServlet:ntf_vcard_resource_timeout(player, finfo)
    local vcard_ntf = player:pack_vcard_resource()
    player:send(CSCmdID.NID_VCARD_RESOURCE_SYNC_NTF, vcard_ntf)
end

-- 更新名片
function VCardServlet:on_self_vcard_update_req(player, player_id, net_req, session_id)
    -- 参数合法性验证
    local resource_id, achie_id = net_req.resource_id, net_req.achie_id
    log_debug("[VCardServlet][on_self_vcard_update_req] resource_id:%s, achie_id:%s", resource_id, achie_id)
    if resource_id > 0 then
        local resource = player:get_vcard_resource(resource_id)
        if not resource then
            player:callback(CSCmdID.NID_SELF_VCARD_UPDATE_RES, { code = VcardCode.VCARD_FRAME_NOT_EXIST }, session_id)
            return
        end
        local attr_id = player:get_vcard_resource_attr(resource_id)
        if not attr_id then
            player:callback(CSCmdID.NID_SELF_VCARD_UPDATE_RES, { code = VcardCode.VCARD_FRAME_NOT_EXIST }, session_id)
            return
        end
        player:set_attribute(attr_id, resource_id)
    end
    if achie_id ~= 0 then
        player:set_attribute(PlayerAttrID.VCARD_ACHIE_ID, achie_id > 0 and achie_id or 0)
    end
    player:callback(CSCmdID.NID_SELF_VCARD_UPDATE_RES, { code = KernCode.SUCCESS }, session_id)
end

-- export
quanta.vcard_servlet    = VCardServlet()

return VCardServlet