--standings_servlet.lua

local log_err           = logger.err
local log_debug         = logger.debug
local serialize         = logger.serialize
local tinsert           = table.insert

local event_mgr         = quanta.get("event_mgr")
local router_mgr        = quanta.get("router_mgr")
local player_mgr        = quanta.get("player_mgr")

local KernCode          = enum("KernCode")
local StandingsCode     = enum("StandingsCode")
local StandingsConst    = enum("StandingsConst")

local CSCmdID           = ncmd_cs.NCmdId

local StandingsServlet = singleton()
function StandingsServlet:__init()
    event_mgr:add_trigger(self, "evt_update_player_standings")
    -- 客户端消息注册
    event_mgr:add_cmd_listener(self, CSCmdID.NID_STANDINGS_LIST_REQ, "on_standings_list_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_STANDINGS_INFO_REQ, "on_standings_info_req")
end

function StandingsServlet:pack_standings(standings, page_index)
    local ret = {}
    local count = #standings
    local start_index = count - (page_index - 1) * StandingsConst.PAGE_NUM
    local end_index = start_index - StandingsConst.PAGE_NUM + 1
    for i = start_index, end_index, -1 do
        if standings[i] then
            tinsert(ret, standings[i])
        end
    end
    log_debug("[StandingsMgr][pack_standings] ret=%s", serialize(ret))
    return ret
end

function StandingsServlet:on_standings_list_req(player, player_id, body, session_id)
    local index = body.index or 1
    log_debug("[StandingsServlet][on_standings_list_req] get standings. index:%s", index)
    local standings = self:pack_standings(player:get_standings(), index)
    player:callback(CSCmdID.NID_STANDINGS_LIST_RES, {titles = standings}, session_id)
end

function StandingsServlet:on_standings_info_req(player, player_id, body, session_id)
    local room_id = body.room_id
    log_debug("[StandingsServlet][on_standings_info_req] player_id = %s, room_id = %s", player_id, room_id)
    local ok, standings = router_mgr:call_center_master("rpc_get_standings", room_id)
    if not ok then
        log_err("[StandingsServlet][on_standings_info_req] rpc_get_standings not ok room_id = %s", room_id)
        player:callback(CSCmdID.NID_STANDINGS_INFO_RES, { code = KernCode.RPC_FAILED }, session_id)
        return
    end
    if not standings then
        log_err("[StandingsServlet][on_standings_info_req] rpc_get_standings standings not exist, room_id = %s", room_id)
        player:callback(CSCmdID.NID_STANDINGS_INFO_RES, { code = StandingsCode.STANDINGS_NO_EXIST }, session_id)
    end
    local res = { code = KernCode.SUCCESS, standings = standings, room_id = room_id }
    player:callback(CSCmdID.NID_STANDINGS_INFO_RES, res, session_id)
end

function StandingsServlet:evt_update_player_standings(player_id, one_standings)
    log_debug("[StandingsMgr][evt_update_player_standings] player_id = %s, one_standings = %s", player_id, serialize(one_standings))
    local player = player_mgr:get_player(player_id)
    if not player then
        log_err("[StandingsMgr][evt_update_player_standings] can not find player")
        return
    end
    player:add_player_standings(one_standings)
end

quanta.standings_servlet = StandingsServlet()

return StandingsServlet
