--room_servlet.lua

local guid_index        = guid.index
local log_err           = logger.err
local log_info          = logger.info
local log_warn          = logger.warn
local log_debug         = logger.debug
local serialize         = logger.serialize
local smake_id          = service.make_id
local check_success     = utility.check_success

local RoomType          = enum("RoomType")
local KernCode          = enum("KernCode")
local RoomCode          = enum("RoomCode")
local PlayerCode        = enum("PlayerCode")
local ReadyStatus       = enum("ReadyStatus")
local GameConst         = enum("GameConst")

local SUCCESS           = KernCode.SUCCESS
local CSCmdID           = ncmd_cs.NCmdId
local READY_OFFLINE     = ReadyStatus.OFFLINE

local plat_api          = quanta.get("plat_api")
local event_mgr         = quanta.get("event_mgr")
local router_mgr        = quanta.get("router_mgr")
local player_mgr        = quanta.get("player_mgr")
local servlet_util      = quanta.get("servlet_util")

local RoomServlet = singleton()
function RoomServlet:__init()
    event_mgr:add_trigger(self, "on_player_offline")
    event_mgr:add_trigger(self, "on_player_logout")
    event_mgr:add_trigger(self, "on_team_reload")
    --SS
    event_mgr:add_listener(self, "rpc_room_invite_req")
    --c2s事件
    event_mgr:add_cmd_listener(self, CSCmdID.NID_ROOM_CREATE_REQ, "on_room_create_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_ROOM_DESTORY_REQ, "on_room_destory_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_ROOM_SEARCH_REQ, "on_room_search_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_ROOM_ENTER_REQ, "on_room_enter_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_ROOM_QUIT_REQ, "on_room_quit_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_ROOM_READY_REQ, "on_room_ready_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_ROOM_BEGIN_REQ, "on_room_begin_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_ROOM_KICK_REQ, "on_room_kick_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_ROOM_ROBOT_REQ, "on_room_robot_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_ROOM_INVITE_REQ, "on_room_invite_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_ROOM_REPLY_REQ, "on_room_reply_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_ROOM_MODIFY_REQ, "on_room_modify_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_ROOM_SWITCH_REQ, "on_room_switch_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_ROOM_TRANS_LEADER_REQ, "on_room_trans_leader_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_ROOM_SWITCH_ANSWER_REQ, "on_room_switch_answer_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_ROOM_RECONNECT_REQ, "on_room_reconnect_req")
end

--本地事件
---------------------------------------------------------------------------------------
function RoomServlet:on_team_reload(player_id, player)
    local rm_type, room_id = player:get_room_type()
    if rm_type then
        local room_sid = smake_id("room", guid_index(room_id))
        local ok, code = router_mgr:call_target(room_sid, "rpc_reload_room", room_id, player_id)
        if ok and check_success(code) then
            return
        end
        log_warn("[RoomServlet][on_team_reload] reload room faild: ok=%s, code=%s", ok, code)
        player:set_room_id(nil)
    end
    local res = { room = {room_id = 0, serial = 0 } }
    player:send(CSCmdID.NID_ROOM_INFO_NTF, res)
end

function RoomServlet:on_player_offline(player_id, player)
    local rm_type, room_id = player:get_room_type()
    if rm_type and rm_type == RoomType.CONTEST then
        local room_sid = smake_id("room", guid_index(room_id))
        router_mgr:call_target(room_sid, "rpc_ready_contest", room_id, player_id, READY_OFFLINE)
    end
end

function RoomServlet:on_player_logout(player_id, player)
    local rm_type, room_id = player:get_room_type()
    if rm_type and rm_type == RoomType.CONTEST then
        local room_sid = smake_id("room", guid_index(room_id))
        router_mgr:call_target(room_sid, "rpc_quit_contest", room_id, player_id)
    end
end

--rpc处理
---------------------------------------------------------------------------------------
--客户端匹配请求
function RoomServlet:on_room_create_req(player, player_id, body, session_id)
    local name, map_id, passwd, visable, version = body.name, body.map_id, body.passwd, body.visable, body.version
    if player:get_team_id() then
        log_info("[RoomServlet][on_room_create_req] res: player=%s name=%s, map_id=%s, version=%s, aready in team", player_id, name, map_id, version)
        player:callback(CSCmdID.NID_ROOM_CREATE_RES, {code = PlayerCode.PLAYER_ERR_AREADY_IN_TEAM}, session_id)
        return
    end
    local ok, code = servlet_util:check_player_room(player)
    if not ok then
        log_info("[RoomServlet][on_room_create_req] res: player=%s, name=%s, map_id=%s, version=%s, code=%s", player_id, name, map_id, version, code)
        player:callback(CSCmdID.NID_ROOM_CREATE_RES, { code = code }, session_id)
        return
    end
    local code1 = plat_api:fliterword_check(player_id, name)
    if not check_success(code1) then
        log_info("[RoomServlet][on_room_create_req] res: player=%s, name=%s, map_id=%s, version=%s, code=%s", player_id, name, map_id, version, code1)
        player:callback(CSCmdID.NID_ROOM_CREATE_RES, { code = code1 }, session_id)
        return
    end
    local user = player:package()
    local config = { visable = visable, name = name, passwd = passwd, map_id = map_id, version = version, choose_time = GameConst.ROOM_WAIT_TIME}
    local ok1, code2 = router_mgr:call_room_hash(name, "rpc_create_contest", user, config)
    log_info("[RoomServlet][on_room_create_req] res: player=%s, name=%s, map_id=%s, version=%s, code=%s", player_id, name, map_id, version, code2)
    player:callback(CSCmdID.NID_ROOM_CREATE_RES, { code = ok1 and code2 or KernCode.LOGIC_FAILED }, session_id)
end

function RoomServlet:on_room_destory_req(player, player_id, body, session_id)
    local room_id = body.room_id
    local room_sid = smake_id("room", guid_index(room_id))
    local ok, code = router_mgr:call_target(room_sid, "rpc_destory_contest", room_id, player_id)
    log_info("[RoomServlet][on_room_destory_req] res: player=%s, room=%s, code=%s", player_id, room_id, code)
    player:callback(CSCmdID.NID_ROOM_DESTORY_RES, { code = ok and code or KernCode.LOGIC_FAILED }, session_id)
end

function RoomServlet:on_room_enter_req(player, player_id, body, session_id)
    local room_id, passwd = body.room_id, body.passwd
    local ok, code = servlet_util:check_player_room(player)
    if not ok then
        log_info("[RoomServlet][on_room_enter_req] res: player=%s, room=%s, passwd=%s, code=%s", player_id, room_id, passwd, code)
        player:callback(CSCmdID.NID_ROOM_ENTER_RES, { code = code }, session_id)
        return
    end
    local user = player:package()
    local room_sid = smake_id("room", guid_index(room_id))
    local ok1, code1 = router_mgr:call_target(room_sid, "rpc_enter_contest", room_id, user, passwd)
    log_info("[RoomServlet][on_room_enter_req] res: player=%s, room=%s, passwd=%s, code=%s", player_id, room_id, passwd, code1)
    player:callback(CSCmdID.NID_ROOM_ENTER_RES, { code = ok1 and code1 or KernCode.LOGIC_FAILED }, session_id)
end

function RoomServlet:on_room_quit_req(player, player_id, body, session_id)
    local room_id = body.room_id
    local room_sid = smake_id("room", guid_index(room_id))
    local ok, code = router_mgr:call_target(room_sid, "rpc_quit_contest", room_id, player_id)
    log_info("[RoomServlet][on_room_quit_req] res: player=%s, room=%s, code=%s", player_id, room_id, code)
    player:callback(CSCmdID.NID_ROOM_QUIT_RES, { code = ok and code or KernCode.LOGIC_FAILED }, session_id)
end

function RoomServlet:on_room_ready_req(player, player_id, body, session_id)
    local room_id, ready = body.room_id, body.ready
    local room_sid = smake_id("room", guid_index(room_id))
    local ok, code = router_mgr:call_target(room_sid, "rpc_ready_contest", room_id, player_id, ready)
    log_info("[RoomServlet][on_room_ready_req] res: player=%s, room=%s, ready=%s, code=%s", player_id, room_id, ready, code)
    player:callback(CSCmdID.NID_ROOM_READY_RES, { code = ok and code or KernCode.LOGIC_FAILED }, session_id)
end

function RoomServlet:on_room_robot_req(player, player_id, body, session_id)
    local room_id, role, rank, count, team = body.room_id, body.role, body.rank, body.count, body.team
    local room_sid = smake_id("room", guid_index(room_id))
    local ok, code = router_mgr:call_target(room_sid, "rpc_robot_contest", room_id, player_id, role, rank, count, team)
    log_info("[RoomServlet][on_room_robot_req] res: player=%s, room=%s, role=%s, rank=%s, team=%s code=%s", player_id, room_id, role, rank, count, team, code)
    player:callback(CSCmdID.NID_ROOM_ROBOT_RES, { code = ok and code or KernCode.LOGIC_FAILED }, session_id)
end

function RoomServlet:on_room_kick_req(player, player_id, body, session_id)
    local room_id, tar_player_id = body.room_id, body.player_id
    local room_sid = smake_id("room", guid_index(room_id))
    local ok, code = router_mgr:call_target(room_sid, "rpc_kick_contest", room_id, player_id, tar_player_id)
    log_info("[RoomServlet][on_room_kick_req] res: player=%s, room=%s, tar_player_id=%s, code=%s", player_id, room_id, tar_player_id, code)
    player:callback(CSCmdID.NID_ROOM_KICK_RES, { code = ok and code or KernCode.LOGIC_FAILED }, session_id)
end

function RoomServlet:on_room_invite_req(player, player_id, body, session_id)
    local room_id, target_id, pos = body.room_id, body.target_id, body.pos
    if player_id == target_id then
        log_info("[TeamService][on_room_invite_req] send res: player_id=%s, target_id:%s operself", player_id, target_id)
        player:callback(CSCmdID.NID_ROOM_INVITE_RES, { code = KernCode.OPERATOR_SELF }, session_id)
        return
    end
    local ok1, lobby = router_mgr:call_index_hash(target_id, "query_player_lobby", target_id)
    if not ok1 or lobby <= 0 then
        log_info("[TeamService][on_room_invite_req] send res: player_id=%s, target_id:%s offline", player_id, target_id)
        player:callback(CSCmdID.NID_ROOM_INVITE_RES, { code = ok1 and PlayerCode.TARGET_ERR_OFFLINE or KernCode.RPC_FAILED }, session_id)
        return
    end
    local ok2, code2 = router_mgr:call_target(lobby, "rpc_room_invite_req", target_id, room_id, pos, player:package())
    log_info("[TeamService][on_room_invite_req] send res: player_id=%s, target_id:%s, code=%s", player_id, target_id, code2)
    player:callback(CSCmdID.NID_ROOM_INVITE_RES, { code = ok2 and code2 or KernCode.RPC_FAILED }, session_id)
end

function RoomServlet:rpc_room_invite_req(target_id, room_id, pos, inviter)
    local player = player_mgr:get_player(target_id)
    if not player then
        return PlayerCode.TARGET_ERR_OFFLINE
    end
    local cur_roomid = player:get_room_id()
    if cur_roomid == room_id then
        return RoomCode.ROOM_ERR_IN_ROOM
    end
    local cur_teamid = player:get_team_id()
    if cur_teamid then
        return PlayerCode.TARGET_ERR_AREADY_IN_TEAM
    end
    local ok, code = servlet_util:check_target_room(player)
    if not ok then
        return code
    end
    local invite_res = { room_id = room_id, player_id = inviter.player_id, nick = inviter.nick, icon = inviter.icon, pos = pos }
    log_info("[RoomServlet][rpc_room_invite_req] send ntf: target_id=%s, inviter:%s, room=%s", target_id, serialize(invite_res), room_id)
    player:send(CSCmdID.NID_ROOM_INVITE_NTF, invite_res)
    return SUCCESS
end

function RoomServlet:on_room_reply_req(player, player_id, body, session_id)
    local ok, code = servlet_util:check_player_room(player)
    if not ok then
        log_info("[RoomServlet][on_room_reply_req] send res: player_id=%s, code=%s", player_id, code)
        player:callback(CSCmdID.NID_ROOM_REPLY_RES, { code = code }, session_id)
        return
    end
    local member = player:package()
    local room_id, invite_id, pos, reply = body.room_id, body.invite_id, body.pos, body.reply
    local invite_res = { sure = reply, player_id = invite_id, nick = member.nick , icon = member.icon}
    router_mgr:call_index_hash(invite_id, "transfer_message", invite_id, "forward_player", invite_id, CSCmdID.NID_ROOM_REPLY_NTF, invite_res)
    if reply == 0 then
        player:callback(CSCmdID.NID_ROOM_REPLY_RES, { code = SUCCESS }, session_id)
        return
    end
    local room_sid = smake_id("room", guid_index(room_id))
    local ok1, code1 = router_mgr:call_target(room_sid, "rpc_invite_contest", room_id, member, invite_id, pos)
    log_info("[RoomServlet][on_room_reply_req] send res: player_id=%s, room=%s, code=%s", player_id, room_id, code1)
    player:callback(CSCmdID.NID_ROOM_REPLY_RES, { code = ok1 and code1 or KernCode.RPC_FAILED }, session_id)
end

function RoomServlet:on_room_modify_req(player, player_id, body, session_id)
    local room_id, name, map_id, passwd, visable = body.room_id, body.name, body.map_id, body.passwd, body.visable
    local room_sid = smake_id("room", guid_index(room_id))
    local code1 = plat_api:fliterword_check(player_id, name .. passwd)
    if not check_success(code1) then
        log_info("[RoomServlet][on_room_modify_req] res: player=%s, room=%s, name=%s, map_id=%s, code=%s", player_id, room_id, name, map_id, code1)
        player:callback(CSCmdID.NID_ROOM_MODIFY_RES, { code = code1 }, session_id)
        return
    end
    local config = { name = name, passwd = passwd, visable = visable, map_id = map_id }
    local ok, code2 = router_mgr:call_target(room_sid, "rpc_modify_contest", room_id, player_id, config)
    log_info("[RoomServlet][on_room_modify_req] res: player=%s, room=%s, name=%s, map_id=%s, code=%s", player_id, room_id, name, map_id, code2)
    player:callback(CSCmdID.NID_ROOM_MODIFY_RES, { code = ok and code2 or KernCode.LOGIC_FAILED }, session_id)
end

function RoomServlet:on_room_trans_leader_req(player, player_id, body, session_id)
    local room_id, tar_uid = body.room_id, body.player_id
    local room_sid = smake_id("room", guid_index(room_id))
    local ok, code = router_mgr:call_target(room_sid, "rpc_trans_leader", room_id, player_id, tar_uid)
    log_info("[RoomServlet][on_room_trans_leader_req] res: player=%s, room=%s, tar_uid=%s, code=%s", player_id, room_id, tar_uid, code)
    player:callback(CSCmdID.NID_ROOM_TRANS_LEADER_RES, { code = ok and code or KernCode.LOGIC_FAILED }, session_id)
end

function RoomServlet:on_room_search_req(player, player_id, body, session_id)
    local key = body.key
    local code1 = plat_api:fliterword_check(player_id, key)
    if not check_success(code1) then
        log_err("[RoomServlet][on_room_search_req] res: player=%s, key=%s, code=%s", player_id, key, code1)
        player:callback(CSCmdID.NID_ROOM_SEARCH_RES, { code = code1 }, session_id)
        return
    end
    local ok, code2, room_lists = router_mgr:collect_room("rpc_search_contest", key)
    if not ok or not check_success(code2) then
        log_err("[RoomServlet][on_room_search_req] res: player=%s, key=%s, code=%s", player_id, key, code2)
        player:callback(CSCmdID.NID_ROOM_SEARCH_RES, { code = ok and code2 or KernCode.LOGIC_FAILED }, session_id)
        return
    end
    for index, room_list in ipairs(room_lists) do
        if #room_list > 0 or index == 1 then
            player:callback(CSCmdID.NID_ROOM_SEARCH_NTF, {index = index, rooms = room_list})
        end
    end
    player:callback(CSCmdID.NID_ROOM_SEARCH_RES, { code = SUCCESS }, session_id)
end

function RoomServlet:on_room_begin_req(player, player_id, body, session_id)
    local room_id = body.room_id
    local room_sid = smake_id("room", guid_index(room_id))
    local ok, code = router_mgr:call_target(room_sid, "rpc_begin_contest", room_id, player_id)
    log_info("[RoomServlet][on_room_begin_req] res: player=%s, room=%s, code=%s", player_id, room_id, code)
    player:callback(CSCmdID.NID_ROOM_BEGIN_RES,  { code = ok and code or KernCode.LOGIC_FAILED }, session_id)
end

function RoomServlet:on_room_switch_req(player, player_id, body, session_id)
    local room_id, pos, target_id, pattern = body.room_id, body.pos, body.target_id, body.pattern
    local room_sid = smake_id("room", guid_index(room_id))
    local ok, code = router_mgr:call_target(room_sid, "rpc_switch_contest", room_id, player_id, pos, target_id, pattern)
    log_info("[RoomServlet][on_room_switch_req] res: player=%s, room=%s, pos=%s target_id=%s, code=%s", player_id, room_id, pos, target_id, code)
    player:callback(CSCmdID.NID_ROOM_BEGIN_RES, { code = ok and code or KernCode.LOGIC_FAILED }, session_id)
end

function RoomServlet:on_room_switch_answer_req(player, player_id, body, session_id)
    local room_id, tar_player_id, tar_pos, answer = body.room_id, body.tar_player_id, body.tar_pos, body.answer
    log_debug("[RoomServlet][on_room_switch_answer_req]: (%s, %s, %s, %s, %s)", player_id, room_id, tar_player_id, tar_pos, answer)
    local answer_res = { code = SUCCESS }
    if answer then
        local room_sid = smake_id("room", guid_index(room_id))
        local ok, code = router_mgr:call_target(room_sid, "rpc_switch_answer", room_id, player_id, tar_player_id, tar_pos, answer)
        answer_res.code = ok and code or KernCode.LOGIC_FAILED
    end
    log_info("[RoomServlet][on_room_switch_answer_req] res: player=%s, room=%s, answer=%s, code=%s", player_id, room_id, answer, answer_res.code)
    player:callback(CSCmdID.NID_ROOM_SWITCH_ANSWER_RES, answer_res, session_id)
end

function RoomServlet:on_room_reconnect_req(player, player_id, body, session_id)
    local sure = body.sure
    log_debug("[RoomServlet][on_room_reconnect_req]: (%s, %s)", player_id, sure)
    local rm_type, room_id = player:get_room_type()
    if not rm_type or rm_type ~= RoomType.FIGHT then
        player:callback(CSCmdID.NID_ROOM_RECONNECT_RES, {code = RoomCode.ROOM_ERR_NO_IN_FIGHT}, session_id)
        return
    end
    if sure ~= 0 then
        player:callback(CSCmdID.NID_ROOM_RECONNECT_RES, {code = SUCCESS}, session_id)
        return
    end
    local room_sid = smake_id("room", guid_index(room_id))
    local ok, code = router_mgr:call_target(room_sid, "rpc_quit_contest", room_id, player_id, true)
    log_info("[RoomServlet][on_room_reconnect_req] res: player=%s, room_id=%s, ok=%s, code=%s", player_id, room_id, ok, code)
    player:callback(CSCmdID.NID_ROOM_RECONNECT_RES, {code = ok and code or KernCode.RPC_FAILED}, session_id)
end

-- export
quanta.room_servlet     = RoomServlet()

return RoomServlet
