--role_servlet.lua

local tinsert       = table.insert
local log_info      = logger.info

local RoleCode      = enum("RoleCode")
local KernCode      = enum("KernCode")
local ResAddType    = enum("ResAddType")
local SUCCESS       = KernCode.SUCCESS
local CSCmdID       = ncmd_cs.NCmdId

local free_role_mgr = quanta.get("free_role_mgr")
local player_mgr    = quanta.get("player_mgr")
local event_mgr     = quanta.get("event_mgr")
local config_mgr    = quanta.get("config_mgr")

local role_db       = config_mgr:get_table("role")
local roleskin_db   = config_mgr:get_table("roleskin")

local RoleServlet = singleton()
function RoleServlet:__init()
    self.pb_role_cfg_list = {} -- -- 为pb编码预生成好的角色配置列表
    for _, role_cfg in role_db:iterator() do
        tinsert(self.pb_role_cfg_list, {role_id = role_cfg.role_id})
    end

    self.pb_rskin_cfg_list = {}  -- 为pb编码预生成好的皮肤配置列表
    for _, rskin_cfg in roleskin_db:iterator() do
        tinsert(self.pb_rskin_cfg_list, {role_skin_id = rskin_cfg.role_skin_id})
    end

    -- 玩家事件注册
    event_mgr:add_trigger(self, "on_load_player_begin")
    event_mgr:add_trigger(self, "evt_player_add_role")
    event_mgr:add_trigger(self, "evt_player_role_timeout")
    event_mgr:add_trigger(self, "evt_player_role_skin_timeout")
    event_mgr:add_trigger(self, "evt_player_role_skin_select")
    event_mgr:add_trigger(self, "evt_player_role_change")
    event_mgr:add_trigger(self, "evt_player_role_skin_change")
    event_mgr:add_trigger(self, "evt_player_role_voice_update")
    event_mgr:add_trigger(self, "evt_player_role_action_update")
    event_mgr:add_trigger(self, "evt_player_role_exp_update")

    -- 配置命令注册
    event_mgr:add_trigger(self, "cfg_update_role_cfg")
    event_mgr:add_trigger(self, "cfg_update_role_skin_cfg")
    event_mgr:add_trigger(self, "evt_free_roles_update")

    -- 命令注册
    event_mgr:add_cmd_listener(self, CSCmdID.NID_ROLE_SYNC_REQ, "on_role_sync_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_ROLE_CFG_SYNC_REQ, "on_role_cfg_sync_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_ROLE_SKIN_SYNC_REQ, "on_role_skin_sync_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_ROLE_SKIN_SELECT_REQ, "on_role_skin_select_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_LOVED_ROLE_UPDATE_REQ , "on_loved_role_update_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_ROLE_SKIN_CFG_SYNC_REQ, "on_role_skin_cfg_sync_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_WEEK_FREE_ROLES_SYNC_REQ, "on_week_free_roles_sync_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_DISPLAYING_ROLE_UPDATE_REQ,"on_displaying_role_update_req")
end

-- 玩家登录完成
function RoleServlet:on_load_player_begin(player_id, player)
    self:send_role_sync_ntf(player)
    self:send_role_skin_sync_ntf(player)
    self:send_role_voice_sync_ntf(player)
    self:send_role_action_sync_ntf(player)
end

function RoleServlet:evt_player_role_change(player, role_infos)
    player:send(CSCmdID.NID_ROLE_CHANGE_NTF, { change_list = role_infos })
end

function RoleServlet:evt_player_add_role(player, role, reason)
    -- 添加新角色后立刻添加默认皮肤
    local role_cfg = role_db:find_one(role.role_id)
    local skin_add_cmd = {role_skin_id = role_cfg.role_skin, life_time = 0, reason = ResAddType.ROLE_DEFAULT}
    player:add_role_skin(skin_add_cmd)

    -- 自动穿戴默认皮肤
    player:select_role_skin(role.role_id, role_cfg.role_skin)
end


function RoleServlet:evt_player_role_timeout(player, role_id)
    self:send_role_sync_ntf(player)
end

function RoleServlet:evt_player_role_skin_timeout(player, role_id, skin_id)
    self:send_role_skin_sync_ntf(player)
end

function RoleServlet:evt_player_role_skin_select(player, role_id, skin_id)
    player:send(CSCmdID.NID_ROLE_SKIN_SELECT_NTF, { role_id = role_id, role_skin_id = skin_id })
end

function RoleServlet:evt_player_role_skin_change(player, skin_infos)
    player:send(CSCmdID.NID_ROLE_SKIN_CHANGE_NTF, { change_list = skin_infos })
end

function RoleServlet:evt_player_role_voice_update(player, add_ids, remove_ids)
    player:send(CSCmdID.NID_ROLE_VOICE_UPDATE_NTF, { add_voice_ids = add_ids, remove_voice_ids = remove_ids })
end

function RoleServlet:evt_player_role_action_update(player, add_ids, remove_ids)
    player:send(CSCmdID.NID_ROLE_ACTION_UPDATE_NTF, { add_action_ids = add_ids, remove_action_ids = remove_ids })
end

function RoleServlet:evt_player_role_exp_update(player, role_id, exp)
    player:send(CSCmdID.NID_ROLE_EXP_SYNC_NTF, { role_id = role_id, exp = exp })
end

-- 获取角色配置
function RoleServlet:on_role_cfg_sync_req(player, player_id, body, session_id)
    local res = {
        code        = SUCCESS,
        version     = role_db:get_version(),
        diff_list   = self.pb_role_cfg_list,
    }
    player:callback(CSCmdID.NID_ROLE_CFG_SYNC_RES, res, session_id)
end

-- 获取玩家角色列表
function RoleServlet:on_role_sync_req(player, player_id, body, session_id)
    local res = {
        code               = SUCCESS,
        player_id          = player_id,
        role_list          = player:get_role_infos(),
        displaying_role_id = player:get_displaying_role_id(),
    }
    player:callback(CSCmdID.NID_ROLE_SYNC_RES, res, session_id)
end

-- 获取皮肤配置
function RoleServlet:on_role_skin_cfg_sync_req(player, player_id, body, session_id)
    local res = {
        code        = SUCCESS,
        version     = roleskin_db:get_version(),
        diff_list   = self.pb_rskin_cfg_list
    }

    player:callback(CSCmdID.NID_ROLE_SKIN_CFG_SYNC_RES, res, session_id)
end

-- 获取角色皮肤列表
function RoleServlet:on_role_skin_sync_req(player, player_id, body, session_id)
    local res = {
        code           = SUCCESS,
        player_id      = player_id,
        role_skin_list = player:get_role_skin_infos(),
    }

    player:callback(CSCmdID.NID_ROLE_SKIN_SYNC_RES, res, session_id)
end

-- 更新最喜爱的角色
function RoleServlet:on_loved_role_update_req(player, player_id, body, session_id)
    local res = {
        code    = SUCCESS,
        role_id = body.role_id,
        loved   = body.loved,
    }

    -- 校验role是否拥有
    if not player:is_owned_role(body.role_id) then
        res.code = RoleCode.ROLE_NOT_OWNED
        player:callback(CSCmdID.NID_LOVED_ROLE_UPDATE_RES, res, session_id)
        return
    end

    player:set_loved_role_id(body.role_id, body.loved)
    player:callback(CSCmdID.NID_LOVED_ROLE_UPDATE_RES, res, session_id)
end

-- 更新展示角色
function RoleServlet:on_displaying_role_update_req(player, player_id, net_req, session_id)
    local net_res = {
        code    = SUCCESS,
        role_id = net_req.role_id,
    }

    local role = player:get_role(net_req.role_id)

    if not role or not role:get_owned() then
        net_res.code = RoleCode.ROLE_NOT_OWNED
        player:callback(CSCmdID.NID_DISPLAYING_ROLE_UPDATE_RES, net_res, session_id)
        return
    end

    player:update_displaying_role_id(net_req.role_id)

    player:callback(CSCmdID.NID_DISPLAYING_ROLE_UPDATE_RES, net_res, session_id)
end

-- 角色装载皮肤
function RoleServlet:on_role_skin_select_req(player, player_id, net_req, session_id)
    local net_res = {
        code         = SUCCESS,
        role_id      = net_req.role_id,
        role_skin_id = net_req.role_skin_id,
    }

    -- 验证角色是否存在
    if not player:get_role(net_req.role_id) then
        net_res.code = RoleCode.ROLE_NOT_EXIST
        player:callback(CSCmdID.NID_ROLE_SKIN_SELECT_RES, net_res, session_id)
        return
    end

    -- 验证皮肤是否存在
    local skin_cfg = roleskin_db:find_one(net_req.role_skin_id)
    if not skin_cfg then
        net_res.code = RoleCode.ROLE_SKIN_NOT_EXIST
        player:callback(CSCmdID.NID_ROLE_SKIN_SELECT_RES, net_res, session_id)
        return
    end
    if 0 ~= net_req.role_skin_id then
        if not player:get_role_skin_info(net_req.role_skin_id) then
            net_res.code = RoleCode.ROLE_SKIN_NOT_EXIST
            player:callback(CSCmdID.NID_ROLE_SKIN_SELECT_RES, net_res, session_id)
            return
        elseif skin_cfg.role_id ~= net_req.role_id then
            net_res.code = RoleCode.ROLE_SKIN_NOT_MATCH
            player:callback(CSCmdID.NID_ROLE_SKIN_SELECT_RES, net_res, session_id)
            return
        end
    end

    player:select_role_skin(net_req.role_id, net_req.role_skin_id)

    player:callback(CSCmdID.NID_ROLE_SKIN_SELECT_RES, net_res, session_id)
end

-- 请求周免角色
function RoleServlet:on_week_free_roles_sync_req(player, player_id, net_req, session_id)
    local net_res = {
        code       = SUCCESS,
        start_time = 0,
        end_time   = 0,
        role_ids   = {},
    }

    -- 刷新服务器内存中的玩家免费角色
    player:flush_free_roles()

    -- 下发当前的周免英雄信息
    local info = free_role_mgr:get_wfree_info()
    if info then
        net_res.start_time = info.start_time
        net_res.end_time   = info.end_time
        net_res.role_ids   = info.role_ids
    end

    player:callback(CSCmdID.NID_WEEK_FREE_ROLES_SYNC_RES, net_res, session_id)
end

-- 角色配置更新
function RoleServlet:cfg_update_role_cfg(role_cfg)
    log_info("[RoleServlet:cfg_update_role_cfg]")
end

-- 角色皮肤配置更新
function RoleServlet:cfg_update_role_skin_cfg(role_skin_cfg)
    log_info("[RoleServlet:cfg_update_role_skin_cfg]")
end

-- 免费角色配置更新
function RoleServlet:evt_free_roles_update()
    player_mgr:boardcast_message(CSCmdID.NID_WEEK_FREE_ROLES_UPDATE_NTF)
end

-- 给玩家发送角色配置
function RoleServlet:send_role_cfg(player)
    local ntf = {
        version  = role_db:get_version(),
        role_cfg = self.pb_role_cfg_list,
    }

    player:send(CSCmdID.NID_ROLE_CFG_SYNC_NTF, ntf)
end

-- 给玩家发送角色皮肤配置
function RoleServlet:send_role_skin_cfg(player)
    local ntf = {
        version       = roleskin_db:get_version(),
        role_skin_cfg = self.pb_rskin_cfg_list
    }

    player:send(CSCmdID.NID_ROLE_SKIN_CFG_SYNC_NTF, ntf)
end

-- 指定的玩家发送角色列表
function RoleServlet:send_role_sync_ntf(player)
    local ntf = {
        player_id          = player:get_player_id(),
        role_list          = player:get_role_infos(),
        displaying_role_id = player:get_displaying_role_id(),
    }

    player:send(CSCmdID.NID_ROLE_SYNC_NTF, ntf)
end

-- 指定的玩家推送皮肤列表
function RoleServlet:send_role_skin_sync_ntf(player)
    local ntf = {
        player_id      = player:get_player_id(),
        role_skin_list = player:get_role_skin_infos(),
    }

    player:send(CSCmdID.NID_ROLE_SKIN_SYNC_NTF, ntf)
end

-- 指定玩家发送已解锁语音列表
function RoleServlet:send_role_voice_sync_ntf(player)
    local ntf = {
        voice_ids = player:get_all_voice_ids()
    }

    player:send(CSCmdID.NID_ROLE_VOICE_SYNC_NTF, ntf)
end

-- 指定玩家发送已解锁动作列表
function RoleServlet:send_role_action_sync_ntf(player)
    local ntf = {
        action_ids = player:get_all_action_ids()
    }

    player:send(CSCmdID.NID_ROLE_ACTION_SYNC_NTF, ntf)
end

-- export
quanta.role_servlet = RoleServlet()

return RoleServlet
