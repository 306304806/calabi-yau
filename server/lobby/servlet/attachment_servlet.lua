--attachment_servlet.lua
-- 武器配件相关

local log_err           = logger.err
local log_debug         = logger.debug
local serialize         = logger.serialize
local check_success     = utility.check_success

local BagCode           = enum("BagCode")
local KernCode          = enum("KernCode")
local ItemType          = enum("ItemType")
local ResDelType        = enum("ResDelType")
local ReddotType        = enum("ReddotType")
local AttachmentCode    = enum("AttachmentCode")
local AttactmentType    = enum("AttactmentType")
local UnlockComponent   = enum("UnlockComponent")
local AttachmentUnlockType = enum("AttachmentUnlockType")

local plat_api          = quanta.get("plat_api")
local event_mgr         = quanta.get("event_mgr")
local player_mgr        = quanta.get("player_mgr")
local config_mgr        = quanta.get("config_mgr")

local component_db      = config_mgr:get_table("component")
local functionunlock_db = config_mgr:get_table("functionunlock")

local CSCmdID           = ncmd_cs.NCmdId
local SUCCESS           = KernCode.SUCCESS

local AttachmentServlet = singleton()
function AttachmentServlet:__init()
    -- rpc消息订阅
    event_mgr:add_listener(self, "rpc_change_weapon_exp")

    -- 客户端消息注册
    event_mgr:add_cmd_listener(self, CSCmdID.NID_ATTACHMENT_EQUIP_REQ, "on_equip_attachment_req")            -- 请求装备配件
    event_mgr:add_cmd_listener(self, CSCmdID.NID_ATTACHMENT_UNLOAD_REQ, "on_unload_attachment_req")            -- 请求装备配件
    event_mgr:add_cmd_listener(self, CSCmdID.NID_ATTACHMENT_UNLOCK_REQ, "on_unlock_attachment_req")           -- 请求解锁武器配件
end

-- 检查玩家配件系统开放
local function check_open_attachment(player)
    local player_lv = player:get_level()
    local item = functionunlock_db:find_one(UnlockComponent.ATTACHMENT) or {}
    local unlock_lv = item.player_level
    --log_debug("[check_open_attachment]->check atachemnt lv! unlock_lv:%s, player_lv:%s", unlock_lv or 0, player_lv)
    if not unlock_lv or unlock_lv > player_lv then
        return false
    else
        return true
    end
end

-- 处理装备配件
function AttachmentServlet:equip_attachment(player, player_id, weapon_uuid, attach_type, attach_id)
    if not attach_type or attach_type < AttactmentType.MUZZLE or attach_type > AttactmentType.STOCK then
        return KernCode.PARAM_ERROR
    end

    if not check_open_attachment(player) then
        return AttachmentCode.ATTACHMENT_ERR_NOT_ENOUGH_LV
    end

    local weapon = player:get_weapon_item_by_uuid(weapon_uuid)
    if not weapon then
        return BagCode.BAG_ERR_ITEM_NOT_EXIST
    end

    if weapon:is_equiped_attachment(attach_type, attach_id) then
        return AttachmentCode.ATTACHMENT_ERR_EQUIPED
    end

    if not weapon:check_unlock(attach_id) then
        return AttachmentCode.ATTACHMENT_ERR_UNLOCK
    end

    player:equip_weapon_attachment(weapon_uuid, attach_type, attach_id)

    return SUCCESS
end

-- 请求装备配件
function AttachmentServlet:on_equip_attachment_req(player, player_id, body, session_id)
    local weapon_uuid = body.weapon_uuid
    local attach_type = body.attach_type
    local attach_id   = body.attach_id
    local resp = {code = SUCCESS, weapon_uuid = weapon_uuid, attach_type = attach_type, attach_id = attach_id}

    resp.code =self:equip_attachment(player, player_id, weapon_uuid, attach_type, attach_id)

    player:callback(CSCmdID.NID_ATTACHMENT_EQUIP_RES, resp, session_id)

    log_debug("[AttachmentServlet][on_equip_attachment_req]->player_id:%s, weapon_uuid:%s, attach_type:%s, attach_id:%s, code:%s",
    player_id, weapon_uuid, attach_type, attach_id, resp.code)
end

-- 处理解锁武器配件
function AttachmentServlet:unlock_attachment(player, player_id, weapon_uuid, attach_type, attach_id, item_uuid)
    if not check_open_attachment(player) then
        return AttachmentCode.ATTACHMENT_ERR_NOT_ENOUGH_LV
    end

    local weapon = player:get_weapon_item_by_uuid(weapon_uuid)
    if not weapon then
        return BagCode.BAG_ERR_ITEM_NOT_EXIST
    end

    local attach_cfg = component_db:find_one(attach_id)
    if not attach_cfg or attach_cfg.unlock_type ~= AttachmentUnlockType.ItemGoods then
        return KernCode.PARAM_ERROR
    end

    local unlock_item_id = attach_cfg.unlock_param[1]
    local ec, _ = player:remove_item(unlock_item_id, 1, ResDelType.UNLOCK_ATTACHMENT, item_uuid)
    if not check_success(ec) then
        return ec
    end

    player:unlock_weapon_attachment(weapon_uuid, attach_type, attach_id)

    return SUCCESS
end

-- 请求解锁武器配件
function AttachmentServlet:on_unlock_attachment_req(player, player_id, body, session_id)
    local weapon_uuid = body.weapon_uuid
    local attach_type = body.attach_type
    local attach_id   = body.attach_id
    local item_uuid   = body.item_uuid
    local resp = {code = SUCCESS, weapon_uuid = weapon_uuid, attach_type = attach_type, attach_id = attach_id}

    resp.code = self:unlock_attachment(player, player_id, weapon_uuid, attach_type, attach_id, item_uuid)

    player:callback(CSCmdID.NID_ATTACHMENT_UNLOCK_RES, resp, session_id)

    log_debug("[AttachmentServlet][on_unlock_attachment_req]->player_id:%s, weapon_uuid:%s, attach_type:%s, attach_id:%s, code:%s",
    player_id, weapon_uuid, attach_type, attach_id, resp.code)

    plat_api:reddot_add_req(player_id, ReddotType.UNLOCK_ATTACHMENT, weapon_uuid, nil, attach_id)
end

-- 武器经验变化
function AttachmentServlet:rpc_change_weapon_exp(player_id, weapon_exp_info)
    log_debug("[AttachmentServlet][rpc_change_weapon_exp]->player_id:%s, weapon_exp_info:%s", player_id, serialize(weapon_exp_info))
    local player = player_mgr:get_player(player_id)
    if not player then
        log_err("[AttachmentServlet][rpc_change_weapon_exp]->get player failed! player_id:%s", player_id)
        return
    end

    local sync_items = {}
    local notify_uuids = {}
    for weapon_uuid, add_exp in pairs(weapon_exp_info) do
        local flag, attach_ids = player:add_weapon_exp(weapon_uuid, add_exp)
        local weapon = player:get_weapon_item_by_uuid(weapon_uuid)
        if flag then
            notify_uuids[weapon_uuid] = attach_ids
            sync_items[weapon_uuid] = {item_type = ItemType.Weapon, item_id = weapon:get_item_id(), item_uuid = weapon_uuid}
        else
            log_err("[AttachmentServlet][rpc_change_weapon_exp]->get weapon failed! player_id:%s, weapon_uudid:%s", player_id, weapon_uuid)
        end
    end

    if next(sync_items) then
        event_mgr:notify_trigger("ntf_part_items_to_client", player, sync_items)
    end

    for weapon_uuid, data in pairs(notify_uuids) do
        for attach_id in pairs(data) do
            plat_api:reddot_add_req(player_id, ReddotType.UNLOCK_ATTACHMENT, weapon_uuid, nil, attach_id)
        end
    end
end

-- 处理卸下配件
function AttachmentServlet:unload_attachment(player, player_id, weapon_uuid, attach_type, attach_id)
    if not attach_type or attach_type < AttactmentType.MUZZLE or attach_type > AttactmentType.STOCK then
        return KernCode.PARAM_ERROR
    end

    if not check_open_attachment(player) then
        return AttachmentCode.ATTACHMENT_ERR_NOT_ENOUGH_LV
    end

    local weapon = player:get_weapon_item_by_uuid(weapon_uuid)
    if not weapon then
        return BagCode.BAG_ERR_ITEM_NOT_EXIST
    end

    if not weapon:is_equiped_attachment(attach_type, attach_id) then
        return KernCode.PARAM_ERROR
    end

    player:unload_weapon_attachment(weapon_uuid, attach_type, attach_id)

    return SUCCESS
end

-- 请求卸下配件
function AttachmentServlet:on_unload_attachment_req(player, player_id, body, session_id)
    local weapon_uuid = body.weapon_uuid
    local attach_type = body.attach_type
    local attach_id   = body.attach_id
    local resp = {code = SUCCESS, weapon_uuid = weapon_uuid, attach_type = attach_type, attach_id = attach_id}

    resp.code =self:unload_attachment(player, player_id, weapon_uuid, attach_type, attach_id)

    player:callback(CSCmdID.NID_ATTACHMENT_UNLOAD_RES, resp, session_id)

    log_debug("[AttachmentServlet][on_unload_attachment_req]->player_id:%s, weapon_uuid:%s, attach_type:%s, attach_id:%s, code:%s",
    player_id, weapon_uuid, attach_type, attach_id, resp.code)
end

quanta.weapon_attachment_servlet = AttachmentServlet()

return AttachmentServlet
