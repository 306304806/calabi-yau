--season_servlete.lua

local log_info      = logger.info
local log_err       = logger.err
local serialize     = logger.serialize
local env_number    = environ.number
local check_success = utility.check_success

local CSCmdID       = ncmd_cs.NCmdId
local PeriodTime    = enum("PeriodTime")

local plat_api          = quanta.get("plat_api")
local event_mgr         = quanta.get("event_mgr")
local router_mgr        = quanta.get("router_mgr")
local thread_mgr        = quanta.get("thread_mgr")
local battlepass_mgr    = quanta.get("battlepass_mgr")
local rankseason_mgr    = quanta.get("rankseason_mgr")

local SeasonServlet = singleton()
function SeasonServlet:__init()
    self:setup()
end

function SeasonServlet:setup()
    -- 内部玩家事件注册
    event_mgr:add_trigger(self, "on_load_player_begin")

    event_mgr:add_trigger(self, "evt_platform_connect")

    --subscribe
    self.area_id = env_number("QUANTA_AREA_ID")
    plat_api:subscribe_register(self.area_id, "evt_rank_season_change")
    plat_api:subscribe_register(self.area_id, "evt_battlepass_season_change")
    event_mgr:add_trigger(self, "evt_rank_season_change")
    event_mgr:add_trigger(self, "evt_battlepass_season_change")

    router_mgr:watch_service_ready(self, "center")
end

-- 玩家载入完成事件处理
function SeasonServlet:on_load_player_begin(player_id, player)
    local cur_season = rankseason_mgr:get_season_id()
    local ntf = {
        season = cur_season,
        stars = player:get_stars(),
        scores = player:get_brave_score(),
        streak_win = player:get_streak_win(),
        is_new_season = false,
        old_stars = player:get_stars()
    }

    local last_login_season = player:get_last_login_season()
    if last_login_season ~= cur_season then
        player:reset_season_qualifying_data()
        ntf.is_new_season = true
        ntf.stars = player:get_stars()
        -- 玩家赛季重置通知
        event_mgr:notify_trigger("reset_player_season_ntf", player_id)
    end

    log_info("[SeasonServlet][on_load_player_begin]->ntf:%s", serialize(ntf))
    player:send(CSCmdID.NID_RANKINFO_SYNC_NTF, ntf)
end

-- 排位赛季变更事件处理
function SeasonServlet:evt_rank_season_change(ntf_data)
    log_info("[SeasonServlet][evt_rank_season_change]->ntf_data:%s", serialize(ntf_data))
    local cur_season = rankseason_mgr:get_season_id()
    local new_season, start_time, finish_time = ntf_data.new_season, ntf_data.start_time, ntf_data.finish_time
    if cur_season ~= new_season and start_time and finish_time then
        rankseason_mgr:set_season_info(new_season, start_time, finish_time)
        -- 发布赛季变更事件
        event_mgr:notify_trigger("ntf_rank_season_update", cur_season, new_season)
    end
end

-- battlepass赛季变更事件处理
function SeasonServlet:evt_battlepass_season_change(ntf_data)
    local cur_season = battlepass_mgr:get_season_id()
    local new_season, start_time, finish_time = ntf_data.new_season, ntf_data.start_time, ntf_data.finish_time
    if cur_season ~= new_season and start_time and finish_time then
        battlepass_mgr:set_season_info(new_season, start_time, finish_time)
        -- 发布赛季变更事件
        event_mgr:notify_trigger("ntf_battlepass_season_update", cur_season, new_season)
    end
end

-- 成功连接platform事件处理
function SeasonServlet:evt_platform_connect()
    local code, season_info = plat_api:query_season_info(self.area_id)
    log_info("[SeasonServlet][evt_platform_connec]->query_season_info:%s", serialize(season_info))
    if check_success(code) then
        local rank_season_time = season_info.rank_season_time
        local battlepass_season_time = season_info.battlepass_season_time
        if rank_season_time and battlepass_season_time then
            rankseason_mgr:set_season_info(season_info.rank_season, rank_season_time.start_time, rank_season_time.finish_time)
            battlepass_mgr:set_season_info(season_info.battlepass_season, battlepass_season_time.start_time, battlepass_season_time.finish_time)
        end
    end
end

-- 服务器注册处理
function SeasonServlet:on_service_ready(id, service_name)
    log_info("[SeasonServlet][on_service_ready]->id:%s, service_name:%s", id, service_name)
    thread_mgr:success_call(PeriodTime.SECOND_MS, function()
        local rank_season = rankseason_mgr:get_season_id()
        if rank_season >= 0 then
            local ok, err = router_mgr:call_target(id, "rpc_strongest_rank_season_ntf", rank_season)
            if ok then
                return true
            end
            log_err("[SeasonServlet][on_service_ready] not failed! id:%s, service_name:%s, err:%s", id, service_name, err)
        end
        return false
    end)
end

quanta.season_servlete = SeasonServlet()

return SeasonServlet
