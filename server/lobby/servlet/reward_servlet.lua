--reward_servlet.lua
-- 奖励相关


local log_err           = logger.err
local log_debug         = logger.debug
local tinsert           = table.insert
local check_success     = utility.check_success

local KernCode          = enum("KernCode")
local RewardCode        = enum("RewardCode")
local RewardStatus      = enum("RewardStatus")
local SUCCESS           = KernCode.SUCCESS
local CSCmdID           = ncmd_cs.NCmdId

local event_mgr         = quanta.get("event_mgr")

local RewardServlet = singleton()
function RewardServlet:__init()
    self:setup()
end

function RewardServlet:setup()
    -- 内部玩家事件注册
    event_mgr:add_trigger(self, "on_load_player_begin")

    -- 添加触发器
    event_mgr:add_trigger(self, "nft_player_recive_reward")

    -- 客户端消息注册
    event_mgr:add_cmd_listener(self, CSCmdID.NID_REWARD_RECIVE_REQ            , "on_player_recive_reward_req")            -- 使用道具请求
end

-- 玩家数据加载结束回调
function RewardServlet:on_load_player_begin(player_id, player)
    local ntf = { all_reward_modules = {}, }
    local rewards_status = player:get_all_rewards_status() or {}
    for module_id, rewards_data in pairs(rewards_status) do
        local reward_info_list = {}
        for reward_id, status in pairs(rewards_data) do
            tinsert(reward_info_list, {reward_id = reward_id, status = status,})
        end

        tinsert(ntf.all_reward_modules, { module_id = module_id, reward_info_list = reward_info_list})
    end

    if next(ntf.all_reward_modules) then
        player:send(CSCmdID.NID_REWARD_ALL_MODULES_SYNC_NTF, ntf)
    end
end

-- 通知玩家奖励领取提示
function RewardServlet:nft_player_recive_reward(player, module_id, reward_info)
    local ntf = { single_reward_module = {}, }
    ntf.single_reward_module.module_id = module_id
    local reward_info_list = ntf.single_reward_module.reward_info_list
    for reward_id, status in pairs(reward_info) do
        tinsert(reward_info_list, {reward_id = reward_id, status = status,})
    end

    player:send(CSCmdID.NID_REWARD_MODULE_SYNC_NTF, ntf)
end

-- 玩家请求领取奖励
function RewardServlet:on_player_recive_reward_req(player, player_id, body, session_id)
    local module_id = body.module_id
    local reward_id = body.reward_id
    local resp = { code = SUCCESS, module_id = module_id, reward_id = reward_id, }

    local reward_obj = player:get_reward_obj(module_id, reward_id)
    if not reward_obj then
        log_err("[RewardServlet][on_player_recive_reward_req]->get reward obj failed! player_id:%s,module_id:%s,reward_id:%s", player_id, module_id, reward_id)
        resp.code = RewardCode.REWARD_NOT_EXIST
        player:callback(CSCmdID.NID_REWARD_RECIVE_RES, resp, session_id)
        return
    end

    if reward_obj:is_expired() then
        -- 奖励过期
        resp.code = RewardCode.REWARD_EXPIRED
        player:callback(CSCmdID.NID_REWARD_RECIVE_RES, resp, session_id)
        return
    end

    if reward_obj:is_recived() then
        -- 奖励已领取
        resp.code = RewardCode.REWARD_RECIVED
        player:callback(CSCmdID.NID_REWARD_RECIVE_RES, resp, session_id)
        return
    end

    -- 发放奖励
    local err_code = reward_obj:give_reward_to_player(player)
    resp.code = err_code
    if check_success(err_code) then
        resp.status = RewardStatus.RECIVED
        log_debug("[RewardServlet][on_player_recive_reward_req]->recive success!player_id:%s,module_id:%s,reward_id:%s", player_id, module_id, reward_id)
    end

    player:callback(CSCmdID.NID_REWARD_RECIVE_RES, resp, session_id)
end

quanta.reward_servlet = RewardServlet()

return RewardServlet
