-- watch_servlet.lua
local log_info      = logger.info
local log_debug     = logger.debug
local serialize     = logger.serialize

local KernCode      = enum("KernCode")
local SUCCESS       = KernCode.SUCCESS
local ncmdid        = ncmd_cs.NCmdId

local event_mgr     = quanta.get("event_mgr")
local router_mgr    = quanta.get("router_mgr")
local player_mgr    = quanta.get("player_mgr")

local WatchServlet = singleton()
function WatchServlet:__init()
    -- CS
    event_mgr:add_cmd_listener(self, ncmdid.NID_WATCH_OTHER_PLAYER_REQ, "on_watch_other_req")
    -- rpc SS
    event_mgr:add_listener(self, "rpc_lobby_exit_watch")
end

-- 请求观战
function WatchServlet:on_watch_other_req(player, player_id, body, session_id)
    local watched_id, room_id = body.player_id, body.room_id
    log_debug("[WatchServlet][on_watch_other_req]->player_id:%s, msg:%s", player_id, serialize(body))
    local resp = {code = SUCCESS, player_id = watched_id }
    if watched_id == player_id or not room_id or room_id <= 0 then
        log_info("[WatchServlet][on_watch_other_req] param error!->player_id:%s, room_id:%s", player_id, room_id)
        resp.code = KernCode.PARAM_ERROR
        player:callback(ncmdid.NID_WATCH_OTHER_PLAYER_RES, resp, session_id)
        return
    end

    local player_room_id = player:get_room_id()
    local player_team_id = player:get_team_id()
    if (player_room_id and player_room_id > 0) or (player_team_id and player_team_id > 0) then
        log_info("[WatchServlet][on_watch_other_req]->failed! player_id:%s, room_id:%s, team_id:%s", player_id, player_room_id, player_team_id)
        resp.code = KernCode.PARAM_ERROR
        player:callback(ncmdid.NID_WATCH_OTHER_PLAYER_RES, resp, session_id)
        return
    end

    local ok, ec, result = router_mgr:call_room_hash(player_id, "rpc_enter_watch", room_id, watched_id, player_id)
    if not ok or ec ~= SUCCESS then
        resp.code = ok and ec or KernCode.RPC_FAILED
        player:callback(ncmdid.NID_WATCH_OTHER_PLAYER_RES, resp, session_id)
        return
    end

    resp.player_id  = watched_id
    resp.ip         = result.ds_info.ip
    resp.port       = result.ds_info.port
    resp.room_id    = result.ds_info.room_id
    resp.token      = result.ds_info.token
    resp.players    = result.players
    resp.map_id     = result.map_id

    log_info("[WatchServlet] send watch resp: player_id=%s, code=%s, resp=%s", player_id, resp.code, serialize(resp))
    player:callback(ncmdid.NID_WATCH_OTHER_PLAYER_RES, resp, session_id)

    player:platform_watching(true)
end

-- 通知退出观战
function WatchServlet:rpc_lobby_exit_watch(player_id)
    log_debug("[WatchServlet][rpc_lobby_exit_watch]->player_id:%s", player_id)
    local player = player_mgr:get_player(player_id)
    if player then
        player:platform_watching(false)
    end
end

-- export
quanta.watch_servlet    = WatchServlet()

return WatchServlet
