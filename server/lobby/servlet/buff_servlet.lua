-- buff_servlet.lua

local CSCmdID       = ncmd_cs.NCmdId

local event_mgr     = quanta.get("event_mgr")

local BuffServlet = singleton()
function BuffServlet:__init()
    event_mgr:add_trigger(self, "on_load_player_begin")
    event_mgr:add_trigger(self, "ntf_sync_player_buffs")
end

-- 登陆完成回调
function BuffServlet:on_load_player_begin(player_id, player)
    local buff_list = player:get_buffs_list()
    if next(buff_list) then
        player:send(CSCmdID.NID_BUFF_LIST_NTF, {buff_list = buff_list})
    end
end

-- 通知同步玩家buff信息
function BuffServlet:ntf_sync_player_buffs(player)
    player:send(CSCmdID.NID_BUFF_LIST_NTF,  { buff_list = player:get_buffs_list() })
end

-- export
quanta.buff_servlet    = BuffServlet()

return BuffServlet
