--team_servlet.lua
local guid_index    = guid.index
local smake_id      = service.make_id
local log_info      = logger.info
local log_debug     = logger.debug
local log_warn      = logger.warn
local serialize     = logger.serialize
local check_failed  = utility.check_failed
local check_success = utility.check_success

local RoomType      = enum("RoomType")
local KernCode      = enum("KernCode")
local TeamCode      = enum("TeamCode")
local PlayerCode    = enum("PlayerCode")
local TeamStatus    = enum("TeamStatus")

local CSCmdID       = ncmd_cs.NCmdId
local SUCCESS       = KernCode.SUCCESS
local TEAM_OFFLINE  = TeamStatus.TEAM_OFFLINE

local plat_api      = quanta.get("plat_api")
local event_mgr     = quanta.get("event_mgr")
local router_mgr    = quanta.get("router_mgr")
local player_mgr    = quanta.get("player_mgr")
local servlet_util  = quanta.get("servlet_util")

local TeamServlet = singleton()
function TeamServlet:__init()
    event_mgr:add_trigger(self, "on_load_player_end")
    event_mgr:add_trigger(self, "on_player_offline")
    event_mgr:add_trigger(self, "on_player_logout")
    event_mgr:add_trigger(self, "on_player_reload")
    --SS
    event_mgr:add_listener(self, "rpc_team_invite_req")
    event_mgr:add_listener(self, "rpc_apply_reply_req")
    event_mgr:add_listener(self, "enter_team_res")
    event_mgr:add_listener(self, "quit_team_res")
    --CS
    event_mgr:add_cmd_listener(self, CSCmdID.NID_TEAM_CREATE_REQ, "on_team_create_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_TEAM_MODE_REQ, "on_team_mode_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_TEAM_READY_REQ, "on_team_ready_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_TEAM_EXIT_REQ, "on_team_exit_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_TEAM_KICK_REQ, "on_team_kick_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_TEAM_APPLY_REQ, "on_team_apply_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_TEAM_INVITE_REQ, "on_team_invite_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_TEAM_REPLY_REQ, "on_team_reply_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_TEAM_QUERY_REQ, "on_team_query_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_TEAM_APPLY_REPLY_REQ, "on_team_apply_reply_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_TEAM_TRANS_LEADER_REQ, "on_team_trans_leader_req")
end

--本地事件
---------------------------------------------------------------------------------------
function TeamServlet:on_load_player_end(player_id, player)
    local team_id = player:get_team_id()
    if team_id then
        local team_sid = smake_id("team", guid_index(team_id))
        local ok, code = router_mgr:call_target(team_sid, "rpc_reload_team", team_id, player_id)
        if not ok or check_failed(code) then
            log_warn("[TeamServlet][on_load_player_end] rpc_reload_team faild: ok=%s, code=%s", ok, code)
            player:set_team_id(nil)
        end
    end
    event_mgr:notify_trigger("on_team_reload", player_id, player)
end

function TeamServlet:on_player_reload(player_id, player)
    local team_id = player:get_team_id()
    if team_id then
        local team_sid = smake_id("team", guid_index(team_id))
        local ok, code = router_mgr:call_target(team_sid, "rpc_reload_team", team_id, player_id)
        if ok and check_success(code) then
            goto lab_next
        end
        log_warn("[TeamServlet][on_player_reload] rpc_reload_team faild: ok=%s, code=%s", ok, code)
        player:set_team_id(nil)
    end

    player:send(CSCmdID.NID_TEAM_EXIT_RES, { code = SUCCESS })

    ::lab_next::
    event_mgr:notify_trigger("on_team_reload", player_id, player)
end

function TeamServlet:on_player_offline(player_id, player)
    local team_id = player:get_team_id()
    if team_id then
        local team_sid = smake_id("team", guid_index(team_id))
        router_mgr:call_target(team_sid, "rpc_team_ready", team_id, player_id, TEAM_OFFLINE)
    end
end

function TeamServlet:on_player_logout(player_id, player)
    local team_id = player:get_team_id()
    if team_id then
        local rm_type = player:get_room_type()
        if rm_type ~= RoomType.FIGHT then
            local team_sid = smake_id("team", guid_index(team_id))
            router_mgr:call_target(team_sid, "rpc_exit_team", team_id, player_id)
        end
    end
end

--rpc处理
---------------------------------------------------------------------------------------
function TeamServlet:on_team_query_req(player, player_id, body, session_id)
    local team_id = body.team_id
    local team_sid = smake_id("team", guid_index(team_id))
    local ok, code, team_info = router_mgr:call_target(team_sid, "rpc_query_team_info", team_id)
    if not ok or check_failed(code) then
        player:callback(CSCmdID.NID_TEAM_QUERY_RES, { code = ok and code or KernCode.RPC_FAILED }, session_id)
        log_info("[TeamServlet][on_team_query_req] send res: player_id=%s code=%s", player_id, code)
    else
        team_info.code = code
        player:callback(CSCmdID.NID_TEAM_QUERY_RES, team_info, session_id)
        log_info("[TeamServlet][on_team_query_req] send res: player_id=%s team=%s", player_id, serialize(team_info))
    end
end

function TeamServlet:on_team_create_req(player, player_id, body, session_id)
    if player:get_team_id() then
        log_info("[TeamServlet][on_team_create_req] send res: player_id=%s aready in team", player_id)
        player:callback(CSCmdID.NID_TEAM_CREATE_RES, {code = PlayerCode.PLAYER_ERR_AREADY_IN_TEAM}, session_id)
        return
    end
    local ok, code = servlet_util:check_player_room(player)
    if not ok then
        log_info("[TeamServlet][on_team_create_req] send res: player_id=%s, code=%s", player_id, code)
        player:callback(CSCmdID.NID_TEAM_CREATE_RES, {code = code}, session_id)
        return
    end
    local ok1, code1 = router_mgr:call_team_hash(player_id, "rpc_create_team", player:package(), body.mode)
    log_info("[TeamServlet][on_team_create_req] send res: player_id=%s, code=%s", player_id, code1)
    player:callback(CSCmdID.NID_TEAM_CREATE_RES, { code = ok1 and code1 or KernCode.RPC_FAILED }, session_id)
end

function TeamServlet:on_team_mode_req(player, player_id, body, session_id)
    local team_id = player:get_team_id()
    if not team_id then
        log_info("[TeamServlet][on_team_mode_req] send res: player_id=%s aready in team", player_id)
        player:callback(CSCmdID.NID_TEAM_MODE_RES, {code = PlayerCode.TEAM_ERR_NOT_EXIST}, session_id)
        return
    end
    local team_sid = smake_id("team", guid_index(team_id))
    local ok, code = router_mgr:call_target(team_sid, "rpc_team_mode", team_id, player_id, body.mode)
    log_info("[TeamServlet][on_team_mode_req] send res: player_id=%s, code=%s", player_id, code)
    player:callback(CSCmdID.NID_TEAM_MODE_RES, { code = ok and code or KernCode.RPC_FAILED }, session_id)
end

function TeamServlet:on_team_exit_req(player, player_id, body, session_id)
    local rm_type, room_id = player:get_room_type()
    if rm_type == RoomType.MATCH then
        local match_sid = smake_id("match", guid_index(room_id))
        local ok, code = router_mgr:call_target(match_sid, "rpc_quit_match", room_id, player_id)
        if not ok then
            log_info("[TeamServlet][on_team_exit_req] send res: player_id=%s quit match failed: %s!", player_id, code)
            player:callback(CSCmdID.NID_TEAM_EXIT_RES, { code = ok and code or KernCode.LOGIC_FAILED }, session_id)
            return
        end
    end
    local team_id = player:get_team_id()
    if not team_id then
        player:callback(CSCmdID.NID_TEAM_EXIT_RES, { code = SUCCESS }, session_id)
        return
    end
    local team_sid = smake_id("team", guid_index(team_id))
    local ok, code = router_mgr:call_target(team_sid, "rpc_exit_team", team_id, player_id)
    log_info("[TeamServlet][on_team_exit_req] send res: player_id=%s, team_id=%s, code=%s", player_id, team_id, code)
    player:callback(CSCmdID.NID_TEAM_EXIT_RES, { code = ok and code or KernCode.RPC_FAILED }, session_id)
end

function TeamServlet:on_team_kick_req(player, player_id, body, session_id)
    local ok, code = servlet_util:check_player_room(player)
    if not ok then
        log_info("[TeamServlet][on_team_kick_req] send res: player_id=%s code!", player_id, code)
        player:callback(CSCmdID.NID_TEAM_KICK_RES, { code = code }, session_id)
        return
    end
    local kick_id = body.player_id
    local team_id = player:get_team_id()
    if not team_id then
        player:callback(CSCmdID.NID_TEAM_KICK_RES, { code = TeamCode.TEAM_ERR_NOT_EXIST }, session_id)
        return
    end
    local team_sid = smake_id("team", guid_index(team_id))
    local ok1, code1 = router_mgr:call_target(team_sid, "rpc_kick_member", team_id, player_id, kick_id)
    log_info("[TeamServlet][on_team_kick_req] send rest: player_id=%s, team_id=%s, kick_id:%s, code=%s", player_id, team_id, kick_id, code1)
    player:callback(CSCmdID.NID_TEAM_KICK_RES, { code = ok1 and code1 or KernCode.RPC_FAILED }, session_id)
end

function TeamServlet:on_team_invite_req(player, player_id, body, session_id)
    local ok, code = servlet_util:check_player_room(player)
    if not ok then
        log_info("[TeamServlet][on_team_invite_req] send res: player_id=%s, code=%s", player_id, code)
        player:callback(CSCmdID.NID_TEAM_INVITE_RES, { code = code }, session_id)
        return
    end
    local team_id, target_id = body.team_id, body.player_id
    if player_id == target_id then
        log_info("[TeamServlet][on_team_invite_req] send res: player_id=%s, target_id:%s operself", player_id, target_id)
        player:callback(CSCmdID.NID_TEAM_INVITE_RES, { code = KernCode.OPERATOR_SELF }, session_id)
        return
    end
    if not plat_api:query_friend_secret(player_id, target_id) then
        log_info("[TeamServlet][on_team_invite_req] send res: player_id=%s, target_id:%s secret", player_id, target_id)
        player:callback(CSCmdID.NID_TEAM_INVITE_RES, { code = SUCCESS }, session_id)
        return
    end
    local ok1, lobby = router_mgr:call_index_hash(target_id, "query_player_lobby", target_id)
    if not ok1 or lobby <= 0 then
        log_info("[TeamServlet][on_team_invite_req] send res: player_id=%s, target_id:%s offline", player_id, target_id)
        player:callback(CSCmdID.NID_TEAM_INVITE_RES, { code = ok1 and PlayerCode.TARGET_ERR_OFFLINE or KernCode.RPC_FAILED }, session_id)
        return
    end
    local ok2, code2 = router_mgr:call_target(lobby, "rpc_team_invite_req", target_id, team_id, player:package())
    log_info("[TeamServlet][on_team_invite_req] send res: player_id=%s, target_id:%s, code=%s", player_id, target_id, code2)
    player:callback(CSCmdID.NID_TEAM_INVITE_RES, { code = ok2 and code2 or KernCode.RPC_FAILED }, session_id)
end

function TeamServlet:rpc_team_invite_req(target_id, team_id, inviter)
    local player = player_mgr:get_player(target_id)
    if not player then
        return PlayerCode.TARGET_ERR_OFFLINE
    end
    local ok, code = servlet_util:check_target_room(player)
    if not ok then
        return code
    end
    local cur_teamid = player:get_team_id()
    if cur_teamid == team_id then
        return TeamCode.TEAM_ERR_TAR_IN_THIS_TEAM
    end
    local invite_res = { team_id = team_id, player_id = inviter.player_id, nick = inviter.nick, icon = inviter.icon }
    log_info("[TeamServlet][rpc_team_invite_req] send ntf: target_id=%s, inviter:%s, team_id=%s", target_id, serialize(invite_res), team_id)
    player:send(CSCmdID.NID_TEAM_INVITE_NTF, invite_res)
    return SUCCESS
end

function TeamServlet:on_team_ready_req(player, player_id, body, session_id)
    local ok, code = servlet_util:check_player_room(player)
    if not ok then
        log_info("[TeamServlet][on_team_ready_req] send res: player_id=%s, code=%s", player_id, code)
        player:callback(CSCmdID.NID_TEAM_READY_RES, { code = code }, session_id)
        return
    end
    local team_id, status = body.team_id, body.status
    local team_sid = smake_id("team", guid_index(team_id))
    local ok1, code1 = router_mgr:call_target(team_sid, "rpc_team_ready", team_id, player_id, status)
    log_info("[TeamServlet][on_team_ready_req] send res: player_id=%s, status=%s, code=%s", player_id, status, code1)
    player:callback(CSCmdID.NID_TEAM_READY_RES, { code = ok1 and code1 or KernCode.RPC_FAILED }, session_id)
end

function TeamServlet:on_team_apply_req(player, player_id, body, session_id)
    local team_id = body.team_id
    local member = player:package()
    member.team_id = player:get_team_id()
    local team_sid = smake_id("team", guid_index(team_id))
    local ok, code = router_mgr:call_target(team_sid, "rpc_apply_team", team_id, player_id, member)
    log_info("[TeamServlet][on_team_apply_req] send res: player_id=%s, team_id=%s, code=%s", player_id, team_id, code)
    player:callback(CSCmdID.NID_TEAM_APPLY_RES, { code = ok and code or KernCode.RPC_FAILED }, session_id)
end

function TeamServlet:on_team_apply_reply_req(player, player_id, body, session_id)
    local ok, code = servlet_util:check_player_room(player)
    if not ok then
        log_info("[TeamServlet][on_team_apply_reply_req] send res: player_id=%s, code=%s", player_id, code)
        player:callback(CSCmdID.NID_TEAM_APPLY_REPLY_RES, { code = code }, session_id)
        return
    end
    local target_id, sure = body.player_id, body.sure
    local ok1, lobby = router_mgr:call_index_hash(target_id, "query_player_lobby", target_id)
    if not ok1 or lobby <= 0 then
        log_info("[TeamServlet][on_team_apply_reply_req] send res: player_id=%s, target_id:%s offline", player_id, target_id)
        player:callback(CSCmdID.NID_TEAM_APPLY_REPLY_RES, { code = ok1 and PlayerCode.TARGET_ERR_OFFLINE or KernCode.RPC_FAILED }, session_id)
        return
    end
    local ok2, code2, old_teamid = router_mgr:call_target(lobby, "rpc_apply_reply_req", target_id)
    if not ok2 then
        log_info("[TeamServlet][on_team_apply_reply_req] send res: player_id=%s, target_id:%s offline", player_id, target_id)
        player:callback(CSCmdID.NID_TEAM_APPLY_REPLY_RES, { code = code2 }, session_id)
        return
    end
    local team_id = player:get_team_id()
    local team_sid = smake_id("team", guid_index(team_id))
    local ok3, code3 = router_mgr:call_target(team_sid, "rpc_reply_apply_team", team_id, player_id, target_id, old_teamid, sure)
    log_info("[TeamServlet][on_team_apply_reply_req] send res: player_id=%s, team_id=%s, code=%s", player_id, team_id, code3)
    player:callback(CSCmdID.NID_TEAM_APPLY_REPLY_RES, { code = ok3 and code3 or KernCode.RPC_FAILED }, session_id)
end

function TeamServlet:rpc_apply_reply_req(target_id)
    local player = player_mgr:get_player(target_id)
    if not player then
        return PlayerCode.TARGET_ERR_OFFLINE
    end
    local teamid = player:get_team_id()
    return SUCCESS, teamid
end

function TeamServlet:on_team_reply_req(player, player_id, body, session_id)
    local ok, code = servlet_util:check_player_room(player)
    if not ok then
        log_info("[TeamServlet][on_team_reply_req] send res: player_id=%s, code=%s", player_id, code)
        player:callback(CSCmdID.NID_TEAM_REPLY_RES, { code = code }, session_id)
        return
    end
    local member = player:package()
    local team_id, invite_id, sure = body.team_id, body.player_id, body.sure
    local invite_res = { sure = sure, player_id = player_id, nick = member.nick , icon = member.icon }
    router_mgr:call_index_hash(invite_id, "transfer_message", invite_id, "forward_player", invite_id, CSCmdID.NID_TEAM_REPLY_NTF, invite_res)
    if sure == 0 then
        player:callback(CSCmdID.NID_TEAM_REPLY_RES, { code = SUCCESS }, session_id)
        return
    end
    local old_teamid = player:get_team_id()
    local team_sid = smake_id("team", guid_index(team_id))
    local ok1, code1 = router_mgr:call_target(team_sid, "rpc_enter_team", team_id, invite_id, member, old_teamid, sure)
    log_info("[TeamServlet][on_team_reply_req] send res: player_id=%s, team_id=%s, code=%s", player_id, team_id, code1)
    player:callback(CSCmdID.NID_TEAM_REPLY_RES, { code = ok1 and code1 or KernCode.RPC_FAILED }, session_id)
end

function TeamServlet:on_team_trans_leader_req(player, player_id, body, session_id)
    local ok, code = servlet_util:check_player_room(player)
    if not ok then
        log_info("[TeamServlet][on_team_trans_leader_req] send res: player_id=%s, code=%s", player_id, code)
        player:callback(CSCmdID.NID_TEAM_TRANS_LEADER_RES, { code = code }, session_id)
        return
    end
    local team_id, tar_player_id = body.team_id, body.player_id
    local team_sid = smake_id("team", guid_index(team_id))
    local ok1, code1 = router_mgr:call_target(team_sid, "rpc_trans_leader", team_id, player_id, tar_player_id)
    log_info("[TeamServlet][on_team_trans_leader_req] send res: player_id=%s, tar_player_id: %s, code=%s", player_id, tar_player_id, code1)
    player:callback(CSCmdID.NID_TEAM_TRANS_LEADER_RES, { code = ok1 and code1 or KernCode.RPC_FAILED }, session_id)
end

function TeamServlet:enter_team_res(player_id, team_id)
    log_debug("[TeamServlet][enter_team_res] player_id:%s, team_id:%s", player_id, team_id)
    local player = player_mgr:get_player(player_id)
    if player then
        player:set_team_id(team_id)
        player:platform_team_sync()
    end
end

function TeamServlet:quit_team_res(player_id)
    log_debug("[TeamServlet][quit_team_res]: (%s)", player_id)
    local player = player_mgr:get_player(player_id)
    if player then
        player:set_team_id(nil)
        player:platform_team_sync()
    end
end

quanta.team_servlet = TeamServlet()

return TeamServlet
