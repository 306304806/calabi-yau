-- recharge_servlet.lua
local log_info      = logger.info
local log_warn      = logger.warn
--local log_dump      = logger.dump
local log_err       = logger.err
local serialize     = logger.serialize
local tinsert       = table.insert
local tsize         = table_ext.size
local check_failed  = utility.check_failed

local KernCode      = enum("KernCode")
local ResAddType    = enum("ResAddType")
local RechargeCode  = enum("RechargeCode")
local PeriodTime    = enum("PeriodTime")

local CSCmdID       = ncmd_cs.NCmdId

local event_mgr     = quanta.get("event_mgr")
local timer_mgr     = quanta.get("timer_mgr")
local plat_api      = quanta.get("plat_api")


local RechargeServlet = singleton()
function RechargeServlet:__init()
    event_mgr:add_trigger(self, "on_load_player_begin")

    event_mgr:add_cmd_listener(self, CSCmdID.NID_RECHARGE_ORDER_SEND_REQ, "on_recharge_order_send_req")
end

function RechargeServlet:on_load_player_begin(player_id, player)
    -- 延迟等待平台登录完成后：查询玩家的已支付，未完成订单，完成发货
    timer_mgr:once(PeriodTime.SECOND_MS * 15, function()
        local ec, plat_res = plat_api:recharge_ws_order_ids_query(player_id)
        if check_failed(ec) then
            log_warn("[RechargeServlet][on_load_player_begin] send order faild: player_id=%s,ec=%s", player_id, ec)
            return
        end

        for _, order_id in pairs(plat_res.ws_order_ids or {}) do
            self:do_recharge_order_send(player
            , player_id, order_id)
        end
    end)

end


-- 请求发货
function RechargeServlet:on_recharge_order_send_req(player, player_id, net_req, session_id)
    local net_res = {
        code          = KernCode.SUCCESS,
        order_id      = net_req.order_id,
        commodity_ids = {},
    }

    local ec, commodity_ids = self:do_recharge_order_send(player, player_id, net_req.order_id)
    if check_failed(ec) then
        net_res.code = ec
        player:callback(CSCmdID.NID_RECHARGE_ORDER_SEND_RES, net_res, session_id)
        return
    end

    net_res.commodity_ids = commodity_ids
    player:callback(CSCmdID.NID_RECHARGE_ORDER_SEND_RES, net_res, session_id)
end

function RechargeServlet:do_recharge_order_send(player, player_id, order_id)
    log_info("[RechargeServlet][do_recharge_order_send] player_id=%s, order_id=%s", player_id, order_id)
    local commodity_ids = {}
    -- 到平台请求订单发货
    local ec, plat_res = plat_api:recharge_order_send(player_id, order_id)
    if check_failed(ec) then
        log_warn("[RechargeServlet][do_recharge_order_send] send order faild: player_id=%s, order_id=%s, code=%s",
            player_id, order_id, ec)
        return ec
    end

    -- 发货成功
    if plat_res.send_ok then
        -- 协议字段校验
        if not plat_res.order_info or not (tsize(plat_res.order_info.commodity_infos) > 0) then
            log_err("[RechargeServlet][do_recharge_order_send] plat_res has no order_info, order_id=%s", order_id)
        end

        tinsert(commodity_ids, plat_res.order_info.commodity_infos[1].commodity_id)

        -- 完成实际商品发放
        log_info("[RechargeServlet][do_recharge_order_send] player_id=%s, order_id=%s, items=%s",
            player_id, order_id, serialize(plat_res.order_info.commodity_infos[1].items))
        -- 待添加的物品
        local items_to_add = {}
        for _, info in pairs(plat_res.order_info.commodity_infos[1].items or {}) do
            tinsert(items_to_add, {
                item_id    = info.item_id,
                num        = info.item_count,
                add_reason = ResAddType.RECHARGE,
                expire_time = 0
            })
        end
        -- 完成实际物品添加
        player:add_items(items_to_add)
    else
        -- 发货状态由平台管理，如果已经发货，平台应该直接返回错误码
        log_err("[RechargeServlet][do_recharge_order_send] already send: player_id=%s, order_id=%s", player_id, order_id)
        return RechargeCode.ORDER_ALREADY_SEND
    end

    return KernCode.SUCCESS, commodity_ids
end

-- export
quanta.recharge_servlet    = RechargeServlet()

return RechargeServlet