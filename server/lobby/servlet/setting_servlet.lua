--setting_servlet.lua
local tinsert       = table.insert

local KernCode      = enum("KernCode")

local CSCmdID       = ncmd_cs.NCmdId
local SUCCESS       = KernCode.SUCCESS

local event_mgr     = quanta.get("event_mgr")

local SettingServlet = singleton()
function SettingServlet:__init()
    event_mgr:add_cmd_listener(self, ncmd_cs.NCmdId.NID_SETTING_SYNC_REQ,   "on_setting_sync_req")
    event_mgr:add_cmd_listener(self, ncmd_cs.NCmdId.NID_SETTING_UPDATE_REQ, "on_setting_update_req")
end

function SettingServlet:on_setting_sync_req(player, player_id, body, session_id)
    player:callback(CSCmdID.NID_SETTING_SYNC_RES, { code = SUCCESS, setting_list = player:get_setting() }, session_id)
end

function SettingServlet:on_setting_update_req(player, player_id, body, session_id)
    local setting_list = {}
    for _, info in pairs(body.setting_list) do
        tinsert(setting_list, {value = info.value, key = info.key})
    end
    player:update_setting(setting_list)
    player:callback(CSCmdID.NID_SETTING_UPDATE_RES, { code = SUCCESS }, session_id)
end

-- export
quanta.setting_servlet = SettingServlet()

return SettingServlet
