--match_ervlet.lua

local guid_index        = guid.index
local smake_id          = service.make_id
local log_err           = logger.err
local log_info          = logger.info
local log_debug         = logger.debug
local check_failed      = utility.check_failed

local RoomType          = enum("RoomType")
local KernCode          = enum("KernCode")
local RoomCode          = enum("RoomCode")
local SUCCESS           = KernCode.SUCCESS
local CSCmdID           = ncmd_cs.NCmdId

local event_mgr         = quanta.get("event_mgr")
local router_mgr        = quanta.get("router_mgr")
local player_mgr        = quanta.get("player_mgr")
local servlet_util      = quanta.get("servlet_util")

local MatchServlet = singleton()
function MatchServlet:__init()
    --注册事件
    event_mgr:add_trigger(self, "on_load_player_begin")
    event_mgr:add_trigger(self, "on_player_offline")
    event_mgr:add_trigger(self, "on_player_logout")
    --s2s事件
    event_mgr:add_listener(self, "enter_room_res")
    event_mgr:add_listener(self, "quit_room_res")
    --c2s事件
    event_mgr:add_cmd_listener(self, CSCmdID.NID_MATCH_JOIN_REQ, "on_match_join_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_MATCH_QUIT_REQ, "on_match_quit_req")
end

--本地事件
---------------------------------------------------------------------------------------
function MatchServlet:quit_match(player_id, player)
    local rm_type, room_id = player:get_room_type()
    if rm_type and rm_type == RoomType.MATCH then
        local match_sid = smake_id("match", guid_index(room_id))
        router_mgr:call_target(match_sid, "rpc_quit_match", room_id, player_id)
    end
end

function MatchServlet:on_load_player_begin(player_id, player)
    self:quit_match(player_id, player)
end

function MatchServlet:on_player_offline(player_id, player)
    self:quit_match(player_id, player)
end

function MatchServlet:on_player_logout(player_id, player)
    self:quit_match(player_id, player)
end

--rpc处理
---------------------------------------------------------------------------------------
--客户端匹配请求
function MatchServlet:on_match_join_req(player, player_id, body, session_id)
    local version = body.version
    log_debug("[MatchServlet][on_match_join_req] mode, version-> (%s, %s)", player_id, version)
    local ok, code = servlet_util:check_player_room(player)
    if not ok then
        log_info("[MatchServlet][on_match_join_req] send result: player_id=%s, code=%s", player_id, code)
        player:callback(CSCmdID.NID_MATCH_JOIN_RES, {code = code}, session_id)
        return
    end
    local team_id = player:get_team_id()
    if not team_id then
        log_info("[MatchServlet][on_match_join_req] send result: player_id=%s team not exist", player_id)
        player:callback(CSCmdID.NID_MATCH_JOIN_RES, {code = RoomCode.ROOM_ERR_NOT_IN_TEAM}, session_id)
        return
    end
    local team_sid = smake_id("team", guid_index(team_id))
    local ok1, code1, team_info = router_mgr:call_target(team_sid, "rpc_get_team_data", team_id, player_id)
    if not ok1 or check_failed(code1) then
        log_info("[MatchServlet][on_match_join_req] send result: player_id=%s, code=%s", player_id, code1)
        player:callback(CSCmdID.NID_MATCH_JOIN_RES, { code = ok1 and code1 or KernCode.LOGIC_FAILED }, session_id)
        return
    end
    local mode = team_info.mode
    local ok2, code2 = router_mgr:call_match_hash(mode, "rpc_join_match", mode, team_info.map_id, team_info.members, version)
    local join_res = {code = ok2 and code2 or KernCode.LOGIC_FAILED }
    log_info("[MatchServlet][on_match_join_req] send result: player_id=%s, mode=%d, code=%d", player_id, mode, code2)
    player:callback(CSCmdID.NID_MATCH_JOIN_RES, join_res, session_id)
end

--客户端匹配请求
function MatchServlet:on_match_quit_req(player, player_id, body, session_id)
    local ret_msg = {code = SUCCESS}
    local ok, room_id = servlet_util:sure_room_status(player, RoomType.MATCH)
    if ok then
        local match_sid = smake_id("match", guid_index(room_id))
        local ok1, code1 = router_mgr:call_target(match_sid, "rpc_quit_match", room_id, player_id)
        ret_msg.code = ok1 and code1 or KernCode.LOGIC_FAILED
    end
    log_info("[MatchServlet][on_match_quit_req] send result: player_id=%s, code=%s", player_id, ret_msg.code)
    player:callback(CSCmdID.NID_MATCH_QUIT_RES, ret_msg, session_id)
end

---------------------------------------------------------------------------------------
--进入房间
function MatchServlet:enter_room_res(player_id, room_id)
    log_debug("[MatchServlet][enter_room_res]: player_id: %s, room_id: %s", player_id, room_id)
    local player = player_mgr:get_player(player_id)
    if player then
        player:set_room_id(room_id)
        local rm_type, _ = player:get_room_type()
        if rm_type == RoomType.FIGHT then
            local fight_data = player:build_ds_roles()
            local room_sid = smake_id("room", guid_index(room_id))
            local ok, code = router_mgr:call_target(room_sid, "rpc_trans_player_ds_roles", room_id, player_id, fight_data)
            if not ok or check_failed(code) then
                log_err("[MatchServlet][enter_room_res]->exe rpc_trans_player_ds_roles failed! room_id:%s, player_id:%s", room_id, player_id)
            end
        end
        player:platform_room_sync()
    end
end

--退出房间
function MatchServlet:quit_room_res(player_id)
    log_debug("[MatchServlet][quit_room_res]: player_id: %s", player_id)
    local player = player_mgr:get_player(player_id)
    if player then
        player:set_room_id(nil)
        player:platform_room_sync()
    end
end

-- export
quanta.match_servlet = MatchServlet()

return MatchServlet
