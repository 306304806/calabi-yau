-- battlepass_servlet.lua
local otime                 = os.time
local log_debug             = logger.debug
local serialize             = logger.serialize

local MailType              = enum("MailType")
local KernCode              = enum("KernCode")
local BattlePassCode        = enum("BattlePassCode")
local BattlePassPrizeMode   = enum("BattlePassPrizeMode")
local BattlePassTaskType    = enum("BattlePassTaskType")
local BattlePassTaskState   = enum("BattlePassTaskState")

local SUCCESS               = KernCode.SUCCESS
local CSCmdID               = ncmd_cs.NCmdId

local plat_api              = quanta.get("plat_api")
local event_mgr             = quanta.get("event_mgr")
local player_mgr            = quanta.get("player_mgr")
local battlepass_mgr        = quanta.get("battlepass_mgr")

local BattlePassServlet = singleton()
function BattlePassServlet:__init()
    event_mgr:add_trigger(self, "on_load_player_begin")

    event_mgr:add_trigger(self, "evt_btask_update")
    event_mgr:add_trigger(self, "evt_btask_finish")
    event_mgr:add_trigger(self, "evt_battlepass_vip")
    event_mgr:add_trigger(self, "evt_battle_pass_take_prize")
    event_mgr:add_trigger(self, "evt_battlepass_season_prize")
    event_mgr:add_trigger(self, "ntf_battlepass_season_update")

    event_mgr:add_cmd_listener(self, CSCmdID.NID_BATTLEPASS_REWARD_REQ ,     "on_battlepass_reward_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_BATTLEPASS_CLUEBOARD_REQ ,  "on_battlepass_clueboard_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_BATTLEPASS_TASK_CHANGE_REQ ,"on_battlepass_task_change_req")
end

--领取进程奖励
function BattlePassServlet:on_battlepass_reward_req(player, player_id, body, session_id)
    local level, vip = body.level, body.vip
    log_debug("[BattlePassServlet][on_battlepass_reward_req]->level:%s, vip:%s", level, vip)
    local reward = battlepass_mgr:get_reward_config(level, vip)
    if not reward then
        player:callback(CSCmdID.NID_BATTLEPASS_REWARD_RES, {code = BattlePassCode.REWARD_NOT_EXIST } , session_id)
        return
    end
    -- 检查battlepass等级
    if battlepass_mgr:get_battlepass_level(player) < level then
        player:callback(CSCmdID.NID_BATTLEPASS_REWARD_RES, {code = BattlePassCode.REWARD_NOT_UNLOCK }, session_id)
        return
    end
    if player:battlepsaa_received(level, vip) then
        player:callback(CSCmdID.NID_BATTLEPASS_REWARD_RES, {code = BattlePassCode.REWARD_RECEIVED }, session_id)
        return
    end
    -- 发奖励
    local ok, _ = player:take_bpass_prize(reward, BattlePassPrizeMode.PROCESS, "")
    if ok then
        player:gain_balltepass_rewards(level, vip)
        player:callback(CSCmdID.NID_BATTLEPASS_REWARD_RES, {code = SUCCESS, level = level, vip = vip }, session_id)
    else
        player:callback(CSCmdID.NID_BATTLEPASS_REWARD_RES, {code = BattlePassCode.REWARD_TAKE_FAILED }, session_id)
    end
end

--领取线索版奖励
function BattlePassServlet:on_battlepass_clueboard_req(player, player_id, body, session_id)
    local clue_id = body.clue_id
    log_debug("[BattlePassServlet][on_battlepass_clueboard_req]->clue_id:%s", clue_id)
    local clue_cfg = battlepass_mgr:get_clueboard_config(clue_id)
    if not clue_cfg then
        player:callback(CSCmdID.NID_BATTLEPASS_CLUEBOARD_RES, {code = BattlePassCode.CLUE_NOT_EXIST }, session_id)
        return
    end
    -- 检查battlepass等级
    if battlepass_mgr:get_battlepass_level(player) < clue_cfg.unlock_level then
        player:callback(CSCmdID.NID_BATTLEPASS_CLUEBOARD_RES, {code = BattlePassCode.CLUE_NOT_UNLOCK }, session_id)
        return
    end
    if player:clueboard_received(clue_id) then
        player:callback(CSCmdID.NID_BATTLEPASS_CLUEBOARD_RES, {code = BattlePassCode.REWARD_RECEIVED }, session_id)
        return
    end
    -- 发奖励
    local ok, _ = player:take_bpass_prize(clue_cfg.prize1, BattlePassPrizeMode.CLUE, "")
    if ok then
        player:gain_clueboard_rewards(clue_id)
        player:callback(CSCmdID.NID_BATTLEPASS_CLUEBOARD_RES, {code = SUCCESS, clue_id = clue_id }, session_id)
    else
        player:callback(CSCmdID.NID_BATTLEPASS_CLUEBOARD_RES, {code = BattlePassCode.REWARD_TAKE_FAILED }, session_id)
    end
end

--请求更换任务
function BattlePassServlet:on_battlepass_task_change_req(player, player_id, net_req, session_id)
    local tar_task_id = net_req.tar_task_id
    log_debug("[BattlePassServlet][on_battlepass_task_change_req]->tar_task_id:%s", tar_task_id)
    -- 任务不存在
    local btask = player:get_task(tar_task_id)
    if not btask then
        player:callback(CSCmdID.NID_BATTLEPASS_TASK_CHANGE_RES, {code = BattlePassCode.TASK_NOT_EXIST}, session_id)
        return
    end
    -- 不支持更换的类型
    if BattlePassTaskType.DAY ~= btask:get_task_type() then
        player:callback(CSCmdID.NID_BATTLEPASS_TASK_CHANGE_RES, {code = BattlePassCode.TYPE_CAN_NOT_CHANGE}, session_id)
        return
    end
    -- 已完成的任务不能更换
    if BattlePassTaskState.FINISH <= btask:get_state() then
        player:callback(CSCmdID.NID_BATTLEPASS_TASK_CHANGE_RES, {code = BattlePassCode.TASK_HAS_FINISH}, session_id)
        return
    end
    local net_res = {
        code = SUCCESS,
        old_task_id = tar_task_id,
        new_task_data = player:change_day_task(net_req.tar_task_id),
    }
    player:callback(CSCmdID.NID_BATTLEPASS_TASK_CHANGE_RES, net_res, session_id)
end

--通知赛季更新
function BattlePassServlet:notify_battlepass_season(player)
    local task_ntf = {
        drop_task_ids     = {},
        update_task_datas = player:get_task_progress(),
    }
    player:send(CSCmdID.NID_BATTLEPASS_SEASON_NTF, battlepass_mgr:get_season_info())
    player:send(CSCmdID.NID_BATTLEPASS_INFO_NTF, player:pack_battlepass_data())
    player:send(CSCmdID.NID_BATTLEPASS_TASK_PROGRESS_NTF, task_ntf)
end

-- 加载玩家数据完成触发回调
function BattlePassServlet:on_load_player_begin(player_id, player)
    self:notify_battlepass_season(player)
end

--battlepass赛季变更
function BattlePassServlet:ntf_battlepass_season_update(old_season, new_season)
    local player_map = player_mgr:get_player_map()
    for _, player in player_map:iterator() do
        player:change_battpepass_season(old_season, new_season)
        self:notify_battlepass_season(player)
    end
end

-- 奖励发放事件
function BattlePassServlet:evt_battle_pass_take_prize(player, obtain_data, module_id, context)
    local net_ntf = {
        module_id   = module_id,
        context     = context,
        obtain_data = obtain_data,
    }
    player:send(CSCmdID.NID_BATTLEPASS_TAKE_PRIZE_NTF, net_ntf)
end

-- 任务更新事件
function BattlePassServlet:evt_btask_update(player, drop_task_ids, update_task_datas)
    local task_ntf = {
        drop_task_ids     = drop_task_ids,
        update_task_datas = update_task_datas,
    }
    log_debug("[BattlePassServlet][evt_btask_update]->task_ntf:%s", serialize(task_ntf))
    player:send(CSCmdID.NID_BATTLEPASS_TASK_PROGRESS_NTF, task_ntf)
end

-- 任务完成时间
function BattlePassServlet:evt_btask_finish(player, task_id)
    local btask = player:get_task(task_id)
    if btask then
        player:take_btask_prize(task_id)
    end
end

--购买vip事件
function BattlePassServlet:evt_battlepass_vip(player, is_vip)
    player:send(CSCmdID.NID_BATTLEPASS_VIP_NTF, { battlepass_vip = is_vip })
end

-- 获取线索版附件
function BattlePassServlet:evt_battlepass_season_prize(player, old_season)
    local clue_attach_items = battlepass_mgr:get_clue_season_prize(player, old_season)
    if #clue_attach_items > 0 then
        local player_id = player:get_player_id()
        local rpc_req = {
            type          = MailType.SYS_MAIL,
            src_player_id = 0,
            tar_player_id = player_id,
            send_time     = otime(),
            title         = "clueboard prize",
            content       = "clueboard prize on season change ",
            attach_items  = clue_attach_items
        }
        plat_api:send_player_mail(player_id, rpc_req)
    end
    local battlepass_attach_items = battlepass_mgr:get_battlepass_season_prize(player, old_season)
    if #battlepass_attach_items > 0 then
        local player_id = player:get_player_id()
        local rpc_req = {
            type          = MailType.SYS_MAIL,
            src_player_id = 0,
            tar_player_id = player_id,
            send_time     = otime(),
            title         = "battlepass prize",
            content       = "battlepass prize on season change ",
            attach_items  = battlepass_attach_items
        }
        plat_api:send_player_mail(player_id, rpc_req)
    end
end

quanta.battlepass_servlet = BattlePassServlet()

return BattlePassServlet
