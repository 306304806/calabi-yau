-- account_servlet.lua
local log_err           = logger.err
local log_warn          = logger.warn
local log_info          = logger.info
local guid_string       = guid.string
local random_name       = quanta.random_name
local check_failed      = utility.check_failed

local KernCode          = enum("KernCode")
local LobbyCode         = enum("LobbyCode")
local PeriodTime        = enum("PeriodTime")
local SexType           = enum("SexType")
local PlayerCode        = enum("PlayerCode")
local SUCCESS           = KernCode.SUCCESS
local CSCmdID           = ncmd_cs.NCmdId

local plat_api          = quanta.get("plat_api")
local account_mgr       = quanta.get("account_mgr")
local client_mgr        = quanta.get("client_mgr")
local token_mgr         = quanta.get("token_mgr")
local player_mgr        = quanta.get("player_mgr")
local timer_mgr         = quanta.get("timer_mgr")
local event_mgr         = quanta.get("event_mgr")
local dlog_mgr          = quanta.get("dlog_mgr")

local AccountServlet = singleton()
function AccountServlet:__init()
    -- 进程内部事件监听
    event_mgr:add_trigger(self, "evt_mod_nick")                 -- 玩家改名
    event_mgr:add_trigger(self, "evt_update_account_token")     -- 重连token更新

    -- 集群rpc
    event_mgr:add_listener(self, "rpc_kick_out_of_lobby")       -- t玩家下线
    event_mgr:add_listener(self, "rpc_get_lobby_access_token")  -- 获取登录token

    -- hook session网络事件
    event_mgr:add_listener(self, "on_session_cmd")
    event_mgr:add_listener(self, "on_session_sync")
    event_mgr:add_listener(self, "on_session_err")
end

-- 进程内事件：token刷新
function AccountServlet:evt_update_account_token(account_id, token)
    local account = account_mgr:get_account_by_account_id(account_id)
    if account then
        local session = account:get_session()
        if session then
            local ntf = { account_id = account_id, token = token }
            client_mgr:send_dx(session, CSCmdID.NID_LOGIN_TOKEN_UPDATE_NTF, ntf)
        end
    end
end

-- 玩家改名
function AccountServlet:evt_mod_nick(player, new_nick)
    local player_id = player:get_player_id()
    log_info("[AccountServlet][evt_mod_nick] player_id=%s, new_nick=%s", player_id, new_nick)
    local account = account_mgr:get_account(player:get_open_id())
    if account then
        account:modify_nick(player_id, new_nick)
        -- 精分上报-玩家创角-修改名称
        local special_fields = {
            opt_type=3,
            ip = account.session.ip,
            character_name = new_nick,
            imei_idfa = account.machine_id,
            gender = (player:get_sex() == SexType.WOMAN and 1 or 2),
        }
        dlog_mgr:send_dlog_game_character_acts({public_fields = player:build_dlog_public_fields(), special_fields = special_fields})
    end
end

-- 集群rpc: 获取lobby访问令牌
function AccountServlet:rpc_get_lobby_access_token(open_id, plat_url)
    local lobby_access_token = guid_string()
    token_mgr:reset_open_id_token(open_id, lobby_access_token, plat_url)
    return SUCCESS, lobby_access_token
end

-- 客户端心跳
function AccountServlet:on_lobby_heart_req(session, net_req, session_id)
    local cserial = net_req.serial
    local sserial = client_mgr:check_serial(session, cserial)
    local data_res = { serial = sserial, time = quanta.now }
    client_mgr:callback_dx(session, CSCmdID.NID_HEARTBEAT_RES, data_res, session_id)
end

-- 客户端登录请求
function AccountServlet:on_lobby_login_req(session, net_req, session_id)
    local access_token, platform_id, machine_id = net_req.access_token, net_req.platform_id, net_req.machine_id
    local open_id, area_id, lobby_access_token = net_req.open_id, net_req.area_id, net_req.lobby_access_token
    log_info("[AccountServlet][on_lobby_login_req] open_id=%s, area_id=%s", open_id, area_id)
    -- 校验是否重复登录
    if session.open_id then
        client_mgr:callback_dx(session, CSCmdID.NID_LOGIN_RES, {code = LobbyCode.LOBBY_SESSION_ALREADY_LOGIN }, session_id)
        return
    end
    -- lobby令牌验证
    local platform_url = token_mgr:verify_open_id_token(open_id, lobby_access_token)
    if not platform_url then
        client_mgr:callback_dx(session, CSCmdID.NID_LOGIN_RES, {code = LobbyCode.LOBBY_ACCESS_TOKEN_ERROR }, session_id)
        return
    end
    -- open_id验证
    local code1, account_id = plat_api:login_account(area_id, platform_id, open_id, access_token)
    if SUCCESS ~= code1 then
        client_mgr:callback_dx(session, CSCmdID.NID_LOGIN_RES, {code = code1 }, session_id)
        return
    end
    --[[
    -- 精分上报-玩家注册
    local report_ctx = {public_fields = {area_id=account.area_id, open_id=open_id, character_id=account_id, character_level = 0}}
    report_ctx.special_fields = {imei_idfa = machine_id, ip = session.ip}
    dlog_mgr:send_dlog_game_user_reg(report_ctx)
    ]]
    -- 登录账号
    local code2, account = account_mgr:login_account(session, area_id, open_id, account_id, machine_id)
    if check_failed(code2) then
        client_mgr:callback_dx(session, CSCmdID.NID_LOGIN_RES, {code = code2 }, session_id)
        return
    end
    -- 通知登录验证结果
    local net_res = {
        code            = SUCCESS,
        area_id         = area_id,
        account_id      = account_id,
        platform_url    = platform_url,
        platform_token  = account:get_platform_token(),
    }
    client_mgr:callback_dx(session, CSCmdID.NID_LOGIN_RES, net_res, session_id)
    -- 注册token更新
    token_mgr:update_account_token(account_id)
    -- 发送player信息
    local cur_player = account:get_cur_player()
    client_mgr:send_dx(session, CSCmdID.NID_PLAYER_SYNC_NTF, { players = account:get_own_players() })
    if cur_player then
        client_mgr:send_dx(session, CSCmdID.NID_INIT_STATE_SYNC_FINISH_NTF)
        event_mgr:notify_trigger("on_load_player_end", cur_player:get_player_id(), cur_player)
    end
end

-- 玩家登出
function AccountServlet:on_lobby_logout_req(session, net_req, session_id)
    -- 玩家如果没有完成登录直接断开连接
    local open_id, account_id, player_id = session.open_id, session.account_id, session.player_id
    local code = account_mgr:logout_account(open_id)
    if check_failed(code) then
        client_mgr:callback_dx(session, CSCmdID.NID_LOGOUT_RES, { code = code })
        log_err("[AccountServlet][on_lobby_logout_req] _destroy_account ec=%s", code)
        return
    end
    log_info("[AccountServlet][on_lobby_logout_req] open_id=%s,account_id=%s, player_id=%s", open_id, account_id, player_id)

    client_mgr:callback_dx(session, CSCmdID.NID_LOGOUT_RES, { code = SUCCESS })
    -- 清理session绑定信息
    session.open_id = nil
    session.player_id = nil
    session.account_id = nil
    -- 完成登出处理后，断开网络连接
    timer_mgr:once(PeriodTime.SECOND_MS, function()
        client_mgr:close_session(session)
    end)
end

-- 玩家重连
function AccountServlet:on_lobby_reconnect_req(session, net_req, session_id)
    local token, account_id, open_id = net_req.token, session.account_id, session.open_id
    log_info("[AccountServlet][on_lobby_reconnect_req] account_id=%s", account_id)
    -- 校验是否重复登录
    if open_id then
        client_mgr:callback_dx(session, CSCmdID.NID_LOGIN_RES, { code = LobbyCode.LOBBY_SESSION_ALREADY_LOGIN }, session_id)
        return
    end
    -- 验证token
    local ok = token_mgr:verify_account_token(account_id, token)
    if not ok then
        client_mgr:callback_dx(session, CSCmdID.NID_RECONNECT_RES, {code = LobbyCode.LOBBY_RECONN_TOKEN_ERR}, session_id)
        return
    end
    -- 检查账号
    local account = account_mgr:get_account_by_account_id(account_id)
    if not account then
        client_mgr:callback_dx(session, CSCmdID.NID_RECONNECT_RES, {code = LobbyCode.LOBBY_NEED_RELOGIN }, session_id)
        return
    end
    local net_res = {code = account_mgr:reconnect_account(account:get_open_id(), session)}
    client_mgr:callback_dx(session, CSCmdID.NID_RECONNECT_RES, net_res, session_id)
end

-- 创建player
function AccountServlet:on_player_create_req(session, net_req, session_id)
    -- 玩家如果没有完成登录直接断开连接
    local open_id = session.open_id
    local account = account_mgr:get_account(open_id)
    -- 独占状态拒绝本次操作
    if account:is_holding() then
        log_warn("[AccountServlet][on_player_create_req] account is busy can't relogin:open_id=%s,logic_state=%s", open_id, account:get_login_state())
        client_mgr:callback_dx(session, CSCmdID.NID_LOGIN_RES, {code = LobbyCode.LOBBY_OP_TOO_OFTEN}, session_id)
        return
    end
    local ec, info = account:add_player(net_req.nick, net_req.sex)
    if check_failed(ec) then
        -- 创建失败
        log_warn("[AccountServlet][on_player_create_req] account:add_player faild: ec=%s,open_id=%s,logic_state=%s", ec, open_id, account:get_login_state())
        client_mgr:callback_dx(session, CSCmdID.NID_PLAYER_CREATE_RES, {code = ec}, session_id)
        return
    end
    local player_id = info.player_id
    local net_res = { code = SUCCESS, player = info }
    client_mgr:callback_dx(session, CSCmdID.NID_PLAYER_CREATE_RES, net_res, session_id)
    log_info("[AccountServlet][on_player_create_req] finish: open_id=%s, player_id=%s", open_id, player_id)

    -- 需要自动选择player
    ec = account:login_player(player_id, true)
    if check_failed(ec) then
        log_err("[AccountServlet][on_player_create_req] account:login_player ec=%s", ec)
        return
    end
    client_mgr:send_dx(session, CSCmdID.NID_INIT_STATE_SYNC_FINISH_NTF)

    -- 精分上报-玩家创角
    local public_fields={area_id=account.area_id, open_id=open_id, character_id=player_id, character_level = 1}
    local special_fields={character_name=info.nick, opt_type=1, gender=(info.sex==SexType.WOMAN and 1 or 2),imei_idfa=account.machine_id,ip=session.ip}
    dlog_mgr:send_dlog_game_character_acts({public_fields = public_fields, special_fields = special_fields})
end

-- 随机昵称请求
function AccountServlet:on_rand_nick_get_req(session, net_req, session_id)
    local net_res = {
        code = SUCCESS,
        nick = random_name()
    }
    client_mgr:callback_dx(session, CSCmdID.NID_RAND_NICK_GET_RES, net_res, session_id)
end

-- T玩家下线
function AccountServlet:rpc_kick_out_of_lobby(open_id)
    local account = account_mgr:get_account(open_id)
    if not account then
        return PlayerCode.TARGET_ERR_OFFLINE
    end
    local session = account:get_session()
    local ec = account_mgr:logout_account(open_id)
    if check_failed(ec) then
        log_err("[AccountServlet][rpc_kick_out_of_lobby] _destroy_account ec=%s", ec)
        return ec
    end
    log_info("[AccountServlet][rpc_kick_out_of_lobby] open_id=%s,account_id=%s, player_id=%s", open_id, session.account_id, session.player_id)
    client_mgr:send_dx(session, CSCmdID.NID_KICK_LOBBY_NTF)
    -- 清理session绑定信息
    session.open_id = nil
    session.player_id = nil
    session.account_id = nil
    -- 完成登出处理后，断开网络连接
    timer_mgr:once(PeriodTime.SECOND_MS, function()
        client_mgr:close_session(session)
    end)
    return SUCCESS
end

-------------------------------------------------------------------
-------- 网络接口
local dx_cmd_route_table = {
    [CSCmdID.NID_HEARTBEAT_REQ]       = AccountServlet.on_lobby_heart_req,
    [CSCmdID.NID_LOGIN_REQ]           = AccountServlet.on_lobby_login_req,
    [CSCmdID.NID_LOGOUT_REQ]          = AccountServlet.on_lobby_logout_req,
    [CSCmdID.NID_RECONNECT_REQ]       = AccountServlet.on_lobby_reconnect_req,
    [CSCmdID.NID_PLAYER_CREATE_REQ]   = AccountServlet.on_player_create_req,
    [CSCmdID.NID_RAND_NICK_GET_REQ]   = AccountServlet.on_rand_nick_get_req,
}

function AccountServlet:on_session_sync(session)
    player_mgr:on_session_sync(session)
end

function AccountServlet:on_session_cmd(session, cmd_id, body, session_id)
    local func = dx_cmd_route_table[cmd_id]
    if func then
        func(self, session, body, session_id)
    else
        player_mgr:on_session_cmd(session, cmd_id, body, session_id)
    end
end

function AccountServlet:on_session_err(session)
    local token, open_id, player_id = session.token, session.open_id, session.player_id
    log_info("[AccountServlet][on_session_err] token=%s, open_id=%s, player_id=%s", token, open_id, player_id)
    local account = account_mgr:get_account(open_id)
    if account then
        account:set_offline()
    end
end

-------------------------------------------------------------------
-- export
quanta.account_servlet = AccountServlet()

return AccountServlet