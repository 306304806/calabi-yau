--gm_servlet.lua
local log_err       = logger.err
local tinsert       = table.insert
local log_debug     = logger.debug
local serialize     = logger.serialize

local CSCmdID       = ncmd_cs.NCmdId
local KernCode      = enum("KernCode")
local SUCCESS       = KernCode.SUCCESS

local event_mgr     = quanta.get("event_mgr")
local router_mgr    = quanta.get("router_mgr")
local player_mgr    = quanta.get("player_mgr")

-- GM服务
local GMServlet = singleton()

function GMServlet:__init()
    event_mgr:add_cmd_listener(self, CSCmdID.NID_GM_COMMAND_REQ,        "on_client_gm_command_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_GET_ONLINE_PLAYERS_REQ,"on_get_online_players_req")
end

-- gm指令处理
function GMServlet:on_client_gm_command_req(player, player_id, body, session_id)
    log_debug("[GMServlet][on_client_gm_command_req]->player_id:%s, body:%s", player_id, serialize(body))
    local resp = { code = SUCCESS, tips = "" }
    local ok, res = router_mgr:call_center_master("rpc_execute_gm_cmd", body.command)
    if not ok then
        resp.code, resp.tips = KernCode.RPC_FAILED, res
        log_err("[GMServlet][on_client_gm_command_req]exec command failed! player_id:%s,command:%s, res.code:%s, ok:%s", player_id, body.command, resp.code, ok)
    elseif res.code ~= SUCCESS then
        resp.code, resp.tips = res.code, res.msg
        log_err("[GMServlet][on_client_gm_command_req]->exec command failed! player_id:%s, command:%s, res.code:%s", player_id, body.command, resp.code)
    end
    player:callback(CSCmdID.NID_GM_COMMAND_RES, resp, session_id)
end

-- 请求获取可以观战玩家 [测试用]
function GMServlet:on_get_online_players_req(player, player_id, body, session_id)
    local res = { players = {} }
    for pla_id, target in player_mgr:iterator() do
        if pla_id ~= player_id then
            tinsert(res.players, {
                player_id   = pla_id,
                sex         = target.sex,
                icon        = target.icon,
                rank        = target.rank,
                nick        = target.nick,
                team_id     = target:get_team_id(),
                room_id     = target:get_room_id(),
            })
            if #res.players >= 10 then
                break
            end
        end
    end
    player:callback(CSCmdID.NID_GET_ONLINE_PLAYERS_RES, res, session_id)
end

quanta.gm_servlet = GMServlet()

return GMServlet
