--const.lua

local OnlineStatus = enum("OnlineStatus", 0)
OnlineStatus.ONLINE                 = 1 --在线
OnlineStatus.WATCH                  = 2 --观战
OnlineStatus.LOST                   = 3 --掉线中
OnlineStatus.OFFLINE                = 4 --离线
OnlineStatus.LEAVE                  = 5 --离开

local LoginStatus = enum("LoginStatus", 0)
LoginStatus.WAITING             = 1 --等待玩家操作
LoginStatus.LOGINING            = 2 --登录状态
LoginStatus.CREATING_PLAYER     = 3 --创建角色中
LoginStatus.LOGINING_PLAYER     = 4 --加载角色中
LoginStatus.LOGOUTING_PLAYER    = 5 --卸载角色中
LoginStatus.PLAYING             = 7 --游戏中(角色选择完成)
LoginStatus.LOGOUTING           = 8 --退出状态

local TokenExpire = enum("TokenExpire", 0)
TokenExpire.LOGIN                   = 120 --登录
TokenExpire.ALIVE                   = 40  --保活

-- 数据状态状态(用于安全处理异步数据流程)
local DataStatus = enum("DataStatus", 0)
DataStatus.INIT                     = 0  -- 初始状态
DataStatus.LOADING                  = 1  -- 加载中
DataStatus.LOADED                   = 2  -- 加载完成
DataStatus.UPDATEING                = 3  -- 更新中
DataStatus.UNLOADING                = 3  -- 卸载中
DataStatus.UNLOADED                 = 4  -- 卸载完成

local ResAddType = enum("ResAddType", 0)
ResAddType.UNDEFINE                 = 0
ResAddType.UES_BAG_ITEM             = 1
ResAddType.ROLE_DEFAULT             = 2
ResAddType.PLAYER_DEFAULT           = 3
ResAddType.MAIL_ATTACH              = 4
ResAddType.REWARD_DIVISION          = 5
ResAddType.SHOP_BUY                 = 6
ResAddType.GIFT_RECEIVE             = 7
ResAddType.RECHARGE                 = 8
ResAddType.WEEK_FREE                = 9
ResAddType.EffectAdd                = 10
ResAddType.NOT_OWNED                = 11  -- 应为逻辑要求必须添加，但是添加完后不解锁(和周免类似)
ResAddType.BATTLE_PASS              = 12  -- 特殊行动
ResAddType.GM                       = 999999

local ResDelType = enum("ResDelType", 0)
ResDelType.UNDEFINE                 = 1000
ResDelType.CLIENT_REQ_USE           = 1001
ResDelType.CONFIG_AUTO_USE          = 1002
ResDelType.EXPIRED                  = 1003
ResDelType.UNLOCK_ATTACHMENT        = 1004
ResDelType.MOD_NAME                 = 1005
ResDelType.SELL                     = 1006
ResDelType.GM                       = 9999999

quanta_const.RES_ADD_NAME = {
    [ResAddType.UES_BAG_ITEM]           = "使用背包物品获得",
    [ResAddType.ROLE_DEFAULT]           = "角色默认附加",
    [ResAddType.PLAYER_DEFAULT]         = "玩家默认附加",
    [ResAddType.MAIL_ATTACH]            = "邮件附件",
    [ResAddType.REWARD_DIVISION]        = "段位奖励",
    [ResAddType.SHOP_BUY]               = "商城购买",
    [ResAddType.GIFT_RECEIVE]           = "礼物中心",
    [ResAddType.GM]                     = "GM添加获得",
}

quanta_const.RES_DEL_NAME = {
    [ResDelType.CLIENT_REQ_USE]     = "客户端请求使用",
    [ResDelType.CONFIG_AUTO_USE]    = "配置自动使用",
    [ResDelType.EXPIRED]            = "过期失效",
    [ResDelType.UNLOCK_ATTACHMENT]  = "解锁武器配件",
    [ResDelType.MOD_NAME]           = "改名消耗",
    [ResDelType.SELL]               = "玩家出售",
    [ResDelType.GM]                 = "GM消耗",
}

-- 物品子类型
local GoodsSubType = enum("GoodsSubType", 0)
GoodsSubType.IDEAL             = 1        -- 理想币
GoodsSubType.CRYSTAL           = 2        -- 巴布晶体
GoodsSubType.MOD_NAME          = 101      -- 改名卡
GoodsSubType.CRYSTAL_INC       = 102      -- 巴布晶体加成卡
GoodsSubType.EXP_INC           = 103      -- 经验加成卡
GoodsSubType.WEAPON_EXP_INC    = 104      -- 武器经验加成卡

-- 武器装备子类型枚举
local WeaponSubType = enum("WeaponSubType", 0)
WeaponSubType.NONE                  = 0
-- 主武器
WeaponSubType.AR                    = 1     -- 突击步枪
WeaponSubType.SR                    = 2     -- 栓动狙击
WeaponSubType.DMR                   = 3     -- 射手步枪
WeaponSubType.LMG                   = 4     -- 轻机枪
WeaponSubType.SMG                   = 5     -- 冲锋枪
WeaponSubType.SG                    = 6     -- 霰弹枪
-- 副武器
WeaponSubType.MSMG                  = 21    -- 微型冲锋枪
WeaponSubType.SOSG                  = 22    -- 短管霰弹枪
WeaponSubType.Pistol                = 23    -- 手枪
WeaponSubType.Rl                    = 24    -- 火箭筒
WeaponSubType.Gl                    = 25    -- 榴弹枪
WeaponSubType.HG                    = 26    -- 喇叭枪
WeaponSubType.PBG                   = 27    -- 颜料枪
WeaponSubType.SACB                  = 28    -- 半自动弩
-- 近战武器
WeaponSubType.Fist                  = 51    -- 拳头
WeaponSubType.Sword                 = 52    -- 小刀
WeaponSubType.WoodStick             = 53    -- 木棍
WeaponSubType.Pillow                = 54    -- 枕头
-- 手雷
WeaponSubType.Grenade               = 81    -- 常规手雷
WeaponSubType.FlashGrenade          = 82    -- 常规手雷
WeaponSubType.DetectionGrenade      = 83    -- 闪光手榴弹
-- 局内使用
WeaponSubType.C4                    = 100   -- 检测手榴弹

-- 武器装备位枚举
local WeaponSlot = enum("WeaponSlot", 0)
WeaponSlot.MIN                      = 0
WeaponSlot.PRIMARY                  = 1    -- 主武器
WeaponSlot.SECONDARY                = 2    -- 副武器
WeaponSlot.MELEE                    = 3    -- 近战武器
WeaponSlot.GRENADE_A                = 4    -- 手雷1
WeaponSlot.GRENADE_B                = 5    -- 手雷2
WeaponSlot.MAX                      = 9

-- 特效类型枚举
local VfxType = enum("VfxType", 0)
VfxType.UNDEFINE                    = 0
VfxType.FLY                         = 1    -- 飞行
VfxType.HIT                         = 2    -- 命中
VfxType.REVIVE                      = 3    -- 复活
VfxType.BEAT                        = 4    -- 击败
VfxType.MAX                         = 5

-- 效果类型
-- ！！！枚举类型修改需和策划进行确认！！！
local EffectType = enum("EffectType", 0)
EffectType.UNDEFINE                 = 0
EffectType.ADD_CURRENCY             = 1     -- 获得某种货币
EffectType.ADD_ROLE                 = 2     -- 获得某个角色
EffectType.ADD_WEAPON               = 3     -- 获得某个枪械
EffectType.ADD_ROLESKIN             = 4     -- 获得某个角色皮肤
EffectType.ADD_ROLEACTION           = 5     -- 获得某个角色动作
EffectType.ADD_ROLEVOICE            = 6     -- 获得某个角色语音
EffectType.ADD_DECAL                = 7     -- 获得某个贴花/喷漆
EffectType.ADD_VCARD_AVATAR         = 8     -- 获得某个名片形象
EffectType.ADD_VCARD_BG             = 9     -- 获得某个名片背景
EffectType.ADD_VCARD_FRAME          = 10    -- 获得某个名片框

EffectType.ADD_GOODS                = 101   -- 获得某个道具
EffectType.ADD_GOODS_GROUP          = 102   -- 获取某个掉落组

EffectType.ADD_BUFF                 = 201   -- 添加buff
EffectType.MOD_ATTRIBUTE            = 202   -- 属性修改


-- 解析效果参数枚举
local ParseParams = enum("ParseParams", 0)
ParseParams.ROLE                     = 1     -- 解析角色相关参数
ParseParams.SKIN                     = 2     -- 解析皮肤相关参数
ParseParams.WEAPON                   = 3     -- 解析武器相关参数
ParseParams.CURRENCY                 = 4     -- 解析货币相关参数
ParseParams.DECAL                    = 5     -- 解析贴花相关参数
ParseParams.ROLEACTION               = 6     -- 解析角色动作相关参数
ParseParams.ROLEVOICE                = 7     -- 解析角色语音相关参数
ParseParams.GOODS                    = 8     -- 解析物品相关参数
ParseParams.GOODSGROUP               = 0     -- 解析物品组相关参数
ParseParams.VCARD_FRAME              = 10    -- 解析名片框相关参数
ParseParams.ATTRIBUTE                = 11    -- 修改属性变化相关参数
ParseParams.BUFF                     = 12    -- 解析BUFF相关参数

-- 物品配置类型枚举
local ItemCfgType = enum("ItemCfgType", 0)
ItemCfgType.GOODS                   = 1     -- 道具
ItemCfgType.GIFT                    = 2     -- 礼包
ItemCfgType.ATTACHMENT              = 3     -- 限定配件

-- 武器配件类型
local AttactmentType = enum("AttactmentType", 0)
AttactmentType.MUZZLE               = 1  -- 枪口
AttactmentType.SIGHTS               = 2  -- 瞄准镜
AttactmentType.GRIP                 = 3  -- 握把
AttactmentType.MAGZINE              = 4  -- 弹夹
AttactmentType.STOCK                = 5  -- 枪托

-- 解锁组件类型
local UnlockComponent = enum("UnlockComponent", 0)
UnlockComponent.HERO_SEASON_CARD    = 1    -- 英雄季卡
UnlockComponent.ROLE                = 2    -- 角色
UnlockComponent.REPOSITORY          = 3    -- 仓库
UnlockComponent.CAREER              = 4    -- 生涯
UnlockComponent.SHOP                = 5    -- 商店
UnlockComponent.ATTACHMENT          = 30

-- 奖励模块
local RewardModule = enum("RewardModule", 0)
RewardModule.DIVISION               = 1    -- 段位奖励

-- 条件类型
local CondType = enum("CondType", 0)
CondType.CondStarsWinCnt            = 1   -- 玩家某星以上排位赛胜场判断
CondType.CondPlayerStars            = 2   -- 玩家星数判断

-- 奖励状态
local RewardStatus = enum("RewardStatus", 0)
RewardStatus.UNKNOWN                = 0    -- 未知
RewardStatus.RECIVING               = 1    -- 可领取
RewardStatus.RECIVED                = 2    -- 已领取
RewardStatus.EXPIRED                = 3    -- 已过期

-- 奖励类型
local RewardType = enum("RewardType", 0)
RewardType.UNKNOWN                  = 0    -- 未知
RewardType.ITEMS                    = 1    -- 奖励物品

local ParameterCfg = enum("ParameterCfg", 0)
ParameterCfg.VFX_DEFAULT_FLY        = 1006  -- 参数表默认飞行特效
ParameterCfg.VFX_DEFAULT_HIT        = 1007  -- 参数表默认命中特效
ParameterCfg.VFX_DEFAULT_RELIVE     = 1008  -- 参数表复活特效
ParameterCfg.VFX_DEFAULT_BEAT       = 1009  -- 参数表默认击败特效
ParameterCfg.BG_DEFAULT_INDEX       = 1010  -- 参数表默认背景主界面索引
ParameterCfg.DEFAULT_GOODS_INDEX    = 1999  -- 参数表初始获得的itemid
ParameterCfg.MOD_NAME_CD            = 1201  -- 改名CD(秒)

-- 枪械等级枚举
local GunLevel = enum("GunLevel", 0)
GunLevel.MIN                        = 1     -- 枪械最小等级
GunLevel.MAX                        = 50    -- 枪械最大等级

-- 配件解锁类型枚举
local AttachmentUnlock = enum("AttachmentUnlock", 0)
AttachmentUnlock.LEVEL              = 1     -- 等级解锁
AttachmentUnlock.ITEM               = 2     -- 物品解锁指定武器配件

-- 生涯属性
local CareerAttrTypes = enum("CareerAttrTypes", 0)
CareerAttrTypes.HISTORY             = 1     -- 历史数据
CareerAttrTypes.CURRENT             = 2     -- 当前赛季数据

local StandingsConst = enum("StandingsConst", 0)
StandingsConst.PAGE_NUM      = 20 -- 分页数量
StandingsConst.MAX_RECORD    = 200 -- 最大记录数

-- 商城类型
local ShopType   = enum("ShopType", 0)
ShopType.RECOMMEND                  = 1     --推荐
ShopType.WEAPONS                    = 2     --武器
ShopType.GOODS                      = 3     --物品

quanta_const.SHOP_NAME = {
    [ShopType.RECOMMEND]            = "推荐",
    [ShopType.WEAPONS]              = "武器",
    [ShopType.GOODS]                = "物品",
}

-- 武器流水
local DlogWeapon  = enum("DlogWeapon", 0)
DlogWeapon.NEW                      = 1     -- 获得新武器
DlogWeapon.LEVEL_UP                 = 2     -- 武器升级

-- 成就类型
local AchieveType = enum("AchieveType", 0)
AchieveType.MEDAL   = 1     -- 奖章
AchieveType.EPIC    = 2     -- 史诗
AchieveType.HONOUR  = 3     -- 荣耀

-- 成就奖章
local AchieveMedalID = enum("AchieveMedalID", 0)
AchieveMedalID.DAMAGE                = 1     -- 伤害
AchieveMedalID.SPARY                 = 2     -- 喷涂（废弃）
AchieveMedalID.KILL                  = 3     -- 杀戮
AchieveMedalID.ALIVE                 = 4     -- 生存
AchieveMedalID.HIT                   = 5     -- 命中
AchieveMedalID.HIT_HEAD              = 6     -- 爆头
AchieveMedalID.CONTINUE_KILL         = 7     -- 连续击杀
AchieveMedalID.FLY                   = 8     -- 飘飞
AchieveMedalID.SLIDE                 = 9     -- 滑行（废弃）
AchieveMedalID.OTHER_DAMAGE          = 10    -- 非枪械伤害
AchieveMedalID.PLACE_BOMB            = 11    -- 放置炸弹
AchieveMedalID.DEFUSE_BOMB           = 12    -- 拆除炸弹

-- 史诗成就
local AchieveEpicID = enum("AchieveEpicID", 0)
AchieveEpicID.BOMB_KILL             = 1     -- 炸弹炸死
AchieveEpicID.ALL_KILL              = 2     -- 灭队
AchieveEpicID.DEFUSE_BOMB           = 3     -- 拆弹
AchieveEpicID.DEATH_WIN             = 4     -- 全部死亡并且取胜
AchieveEpicID.TEAM_ACC_KILL         = 5     -- 团竟累计击杀
AchieveEpicID.TEAM_CON_KILL         = 6     -- 团竟连续击杀
AchieveEpicID.TEAM_CON_HIT_HEAD     = 7     -- 团竟连续爆头
AchieveEpicID.TEAM_FIRST_KILL       = 8     -- 死亡前杀人数
AchieveEpicID.RANK_BOMB_KILL        = 9     -- 排位炸死
AchieveEpicID.RANK_BOMB_FLUSH       = 10    -- 排位致盲

-- 成就荣耀
local AchieveHonourID = enum("AchieveHonourID", 0)
AchieveHonourID.FRIENDS             = 1    -- 好友
AchieveHonourID.FRIEND_TEAM         = 2    -- 好友组队战斗
AchieveHonourID.LAUD                = 3    -- 点赞数
AchieveHonourID.MVP                 = 4    -- mvp
AchieveHonourID.ACC_LOGIN           = 5    -- 累计登录
AchieveHonourID.CONTINUE_LOGIN      = 6    -- 连续登录
AchieveHonourID.RANK                = 7    -- 累计排位（废弃）
AchieveHonourID.ROLE                = 8    -- 拥有角色
AchieveHonourID.SKIN                = 9    -- 拥有皮肤
AchieveHonourID.GUN                 = 10   -- 拥有枪械
AchieveHonourID.PROFICIEN           = 11   -- 拥有熟练度的英雄
AchieveHonourID.LEVEL               = 12   -- 玩家等级
AchieveHonourID.GUN_PROFICIEN       = 13   -- 拥有熟练度的枪械
AchieveHonourID.SEG                 = 14   -- 段位荣耀（废弃）
AchieveHonourID.PLACE_BOMB          = 15   -- 放置炸弹
AchieveHonourID.DEFUSE_BOMB         = 16   -- 拆除炸弹
AchieveHonourID.DAMAGE              = 17   -- 伤害
AchieveHonourID.KILL                = 18   -- 击杀
AchieveHonourID.HIT_HEAD            = 19   -- 爆头
AchieveHonourID.SPARY               = 20   -- 喷涂
AchieveHonourID.MEDAL               = 21   -- 战斗勋章
AchieveHonourID.EPIC                = 22   -- 史诗成就
AchieveHonourID.HONOUR              = 23   -- 荣耀成就

-- 战斗数据事件
local BattleEvtID = enum("BattleEvtID", 0)
BattleEvtID.DMAGE                   = 1     -- 伤害
BattleEvtID.KILLNUM                 = 2     -- 击杀
BattleEvtID.HITHEAD                 = 3     -- 爆头
BattleEvtID.HITCOUNT                = 4     -- 命中次数
BattleEvtID.SPRAYAREA               = 5     -- 喷涂面积
BattleEvtID.PLACEBOMB               = 6     -- 放置炸弹
BattleEvtID.DEFUSEBOMB              = 7     -- 放置炸弹

-- 武器配件解锁类型
local AttachmentUnlockType = enum("AttachmentUnlockType", 0)
AttachmentUnlockType.Level          = 1     -- 等级解锁
AttachmentUnlockType.ItemGoods      = 2     -- 道具解锁

-- 武器配件所属类型
local AttachBelongType  = enum("AttachBelongType", 0)
AttachBelongType.SpecialWeaponId    = 1     -- 指定武器id
AttachBelongType.SpecialWeaponType  = 2     -- 指定武器类型

-- 新版物品类型枚举
local ItemType = enum("ItemType", 0)
ItemType.NONE                    = 0
ItemType.Currency                = 1     --货币
ItemType.Role                    = 2     --角色
ItemType.Weapon                  = 3     --武器
ItemType.RoleSkin                = 4     --角色皮肤
ItemType.RoleAction              = 5     --角色动作
ItemType.RoleVoice               = 6     --角色语音
ItemType.Decal                   = 7     --喷漆
ItemType.VCardAvatar             = 8     --名片形象
ItemType.VCardBg                 = 9     --名片背景
ItemType.VCardFrame              = 10    --名片框

ItemType.BagItem_ModName              = 100   -- 道具-改名卡
ItemType.BagItem_JinBiAdd             = 101   -- 道具-金币加成卡
ItemType.BagItem_ExpAdd               = 102   -- 道具-经验加成卡
ItemType.BagItem_WeaponExpAdd         = 103   -- 道具—武器经验加成卡
ItemType.BagItem_NormalBP             = 190   -- 道具-BattlePass普通战令
ItemType.BagItem_AdvanceBP            = 191   -- 道具-BattlePass高级战令
ItemType.BagItem_AdvanceBPPlus        = 192   -- 道具-BattlePass普通战令Plus
ItemType.BagItem_Expolre              = 193   -- 道具-探索点数
ItemType.BagItem_FixedGift            = 200   -- 道具-固定礼包
ItemType.BagItem_RandGift             = 201   -- 道具-随机礼包
ItemType.BagItem_RoleCard             = 302   -- 道具—角色体验卡
ItemType.BagItem_WeaponCard           = 303   -- 道具—武器体验卡
ItemType.BagItem_RoleSkinCard         = 304   -- 道具—角色皮肤体验卡
ItemType.BagItem_RoleActionCard       = 305   -- 道具—角色动作体验卡
ItemType.BagItem_RoleVoiceCard        = 306   -- 道具—角色语音体验卡
ItemType.BagItem_DecalCard            = 307   -- 道具—喷漆体验卡
ItemType.BagItem_VCardAvatarCard      = 308   -- 道具—名片形象体验卡
ItemType.BagItem_VCardBgCard          = 309   -- 道具—名片背景体验卡
ItemType.BagItem_VCardFrameCard       = 310   -- 道具—名片框体验卡


-- 货币类型
local CurrencyType = enum("CurrencyType", 0)
CurrencyType.IDEAL                       = 1     -- 理想币
CurrencyType.Hermes                      = 2     -- 赫尔币
CurrencyType.CRYSTAL                     = 3     -- 巴布洛晶体


-- battlepass奖励模块枚举
local BattlePassPrizeMode = enum("BattlePassPrizeMode", 0)
BattlePassPrizeMode.NONE                 = 0     -- 默认值
BattlePassPrizeMode.CLUE                 = 1     -- 线索板模块
BattlePassPrizeMode.PROCESS              = 2     -- 进程模块