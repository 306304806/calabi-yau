-- 玩家信息数据库接口

local log_err       = logger.err
local check_failed  = utility.check_failed

local db_agent      = quanta.get("db_agent")

local AdminDao = singleton()
function AdminDao:__init()
end

function AdminDao:load_player_attr(player_id)
    local query = { "player_attribute", { ["player_id"] = player_id }, { ["_id"] = 0, ["attributes"] = 1 } }
    local ok, ec, result = db_agent:find_one(player_id, query)
    if not ok or not result or check_failed(ec) then
        log_err("[AdminDao][load_player_attr] failed: player_id:%s, ec:%s", player_id, ec)
        return false
    end
    local attr_map = {}
    for _, attr in pairs(result.attributes or {}) do
        attr_map[attr.id] = attr.value
    end
    return true, attr_map
end

-- export
quanta.admin_dao = AdminDao()

return AdminDao
