-- account_data.lua
local log_err       = logger.err
local check_failed  = utility.check_failed

local cache_agent   = quanta.get("cache_agent")

local AccountDao = singleton()
function AccountDao:__init()
end

function AccountDao:load_account(open_id)
    local ec, result = cache_agent:load(open_id, "account")
    if check_failed(ec) then
        log_err("[AccountDao][load_account] failed: %s", ec)
        return false
    end
    local account = result.account
    if not account or not account.account_id then
        return true
    end
    return true, account
end

function AccountDao:update_account(area_id, open_id, acc_info)
    local row_data = {
        open_id     = open_id,
        area_id     = area_id,
        account_id  = acc_info.account_id,
        players     = acc_info.players or {},
    }
    local ec = cache_agent:update(open_id, "account", row_data, "account", true)
    if check_failed(ec) then
        log_err("[AccountDao][update_account] ec=%s", ec)
        return false
    end
    return true
end

-- export
quanta.account_dao = AccountDao()

return AccountDao
