-- 玩家信息数据库接口

local log_err       = logger.err
local tinsert       = table.insert
local check_failed  = utility.check_failed

local cache_agent   = quanta.get("cache_agent")

local PlayerDao = singleton()
function PlayerDao:__init()
end

-- 更新
function PlayerDao:update_player_attribute(player_id, attr_map, flush)
    local row_data = { player_id = player_id, attributes = attr_map }
    return self:update_player(player_id, "player_attribute", row_data, flush)
end

function PlayerDao:update_player_role_info(player_id, role_datas, flush)
    role_datas.player_id = player_id
    return self:update_player(player_id, "player_role_info", role_datas, flush)
end

function PlayerDao:update_player_role_skin(player_id, skins, flush)
    local row_data = { player_id = player_id, skins = skins }
    return self:update_player(player_id, "player_role_skin", row_data, flush)
end

-- 更新
function PlayerDao:update_player_items(player_id, bag_list, flush)
    local row_data = { player_id = player_id, bag_list = bag_list }
    return self:update_player(player_id, "player_bag", row_data, flush)
end

-- 更新
function PlayerDao:update_player_buff(player_id, buff_datas, flush)
    local row_data = { player_id = player_id, buff_datas = buff_datas }
    return self:update_player(player_id, "player_buff", row_data, flush)
end

-- 更新
function PlayerDao:update_player_career(player_id, career_map, flush)
    local career_attrs = {}
    for types, career in pairs(career_map) do
        career.types = types
        local attrs = {}
        for id, value in pairs(career) do
            tinsert(attrs, {id = id, value = value})
        end
        tinsert(career_attrs,  attrs)
    end
    local row_data = { player_id = player_id, attrs = career_attrs }
    return self:update_player(player_id, "player_career", row_data, flush)
end

function PlayerDao:update_player_achieves(player_id, achieve_data, flush)
    achieve_data.player_id = player_id
    return self:update_player(player_id, "player_achieve", achieve_data, flush)
end

-- 更新
function PlayerDao:update_player_prepare(player_id, prepare_data, flush)
    local row_datas = { player_id = player_id, prepare_data = prepare_data }
    return self:update_player(player_id, "player_prepare", row_datas, flush)
end

-- 更新
function PlayerDao:update_player_rewards(player_id, rewards, flush)
    local row_datas = { player_id = player_id, rewards = rewards }
    return self:update_player(player_id, "player_reward", row_datas, flush)
end

function PlayerDao:update_player_standings(player_id, standings, flush)
    local row_datas = { player_id = player_id, standings = standings }
    return self:update_player(player_id, "player_standings", row_datas, flush)
end

function PlayerDao:update_player_vcard(player_id, vcards, flush)
    local row_datas = { player_id = player_id, vcards = vcards }
    return self:update_player(player_id, "player_vcard", row_datas, flush)
end

-- 更新设置
function PlayerDao:update_player_setting(player_id, settings, flush)
    local row_datas = { player_id = player_id, settings = settings }
    return self:update_player(player_id, "player_setting", row_datas, flush)
end

-- 更新battlepass
function PlayerDao:update_player_battlepass(player_id, battlepass, flush)
    local row_datas = { player_id = player_id, battlepass = battlepass }
    return self:update_player(player_id, "player_battlepass", row_datas, flush)
end

-- 更新设置
function PlayerDao:update_player(player_id, table_name, table_data, flush)
    local ec = cache_agent:update(player_id, table_name, table_data, "player", flush)
    if check_failed(ec) then
        log_err("[PlayerDao][update_player] table=%s,ec=%s", table_name, ec)
        return false
    end
    return true
end

function PlayerDao:load_player(player_id)
    local ec, result = cache_agent:load(player_id)
    if check_failed(ec) then
        log_err("[PlayerDao][load_player] failed: %s", ec)
        return false
    end
    return true, result
end

function PlayerDao:flush_player(player_id)
    local ec, result = cache_agent:flush(player_id)
    if check_failed(ec) then
        log_err("[PlayerDao][flush_player] failed: %s", ec)
        return false
    end
    return true, result
end

-- export
quanta.player_dao = PlayerDao()

return PlayerDao
