-- 玩家信息数据库接口
local tinsert       = table.insert

local log_debug     = logger.debug
local log_err       = logger.err
local check_failed  = utility.check_failed

local cache_agent   = quanta.get("cache_agent")

-- 属性数组转map
local function attribute_array2map(attributes)
    local attr_map = {}
    for _, item in pairs(attributes or {}) do
        if item.game_mode then
            attr_map[item.game_mode] = attr_map[item.game_mode] or {}
            attr_map[item.game_mode][item.id] = item.value
        else
            attr_map[item.id] = item.value
        end
    end
    return attr_map
end

-- 属性map转数组
local function attribute_map2array(attr_map)
    local attributes = {}
    for id, value in pairs(attr_map) do
        tinsert(attributes, {id = id, value = value})
    end
    return attributes
end

-- db数据反序列化
local function db_data_deserialize(career_image)
    local mem_data = {}
    -- 角色使用数据
    mem_data.role_employs = attribute_array2map(career_image.role_employs)
    mem_data.attrs = {}
    for k, v in pairs(career_image.attrs or {}) do
        local tmp_map = attribute_array2map(v)
        local types = tmp_map["types"]
        tmp_map["types"] = nil
        mem_data.attrs[types] = tmp_map
    end
    return mem_data
end

-- 内存数据反序列化
local function memory_data_deserialize(career_image)
    local db_data = {}
    -- 角色使用数据
    local role_employs = {}
    for game_mode, employs in pairs(career_image.role_employs) do
        for id, value in pairs(employs) do
            tinsert(role_employs, {game_mode = game_mode, id = id, value = value})
        end
    end

    career_image.role_employs = nil
    db_data.role_employs = role_employs

    db_data.attrs = {}
    for k, v in pairs(career_image) do
        v.types = k
        local attrs  = attribute_map2array(v)
        tinsert(db_data.attrs, attrs)
    end

    return db_data
end

local CareerImageDao = singleton()
function CareerImageDao:__init()
end

-- 加载
function CareerImageDao:load_career_image(player_id)
    log_debug("[CareerImageDao][load_career_image] player_id=%s", player_id)
    local ec, result = cache_agent:load(player_id, "career_image")
    if check_failed(ec) then
        log_err("[CareerImageDao:load_career_image] find faild: ec=%s", ec)
        return false
    end
    local career_image = result.career_image
    if not career_image then
        return true
    end
    local mem_data = db_data_deserialize(career_image)
    return true, mem_data
end

-- 更新
function CareerImageDao:update_career_image(player_id, career_image)
    local row_data = memory_data_deserialize(career_image)
    row_data.player_id = player_id
    local ec, result = cache_agent:update(player_id, "career_image", row_data, "career_image")
    if check_failed(ec) then
        log_err("[CareerImageDao:update_player_career] update faild: ec=%s", ec)
        return false
    end
    return true, result
end

-- export
quanta.career_image_dao = CareerImageDao()

return CareerImageDao
