--init.lua

local config_mgr   = quanta.get("config_mgr")
config_mgr:init_table("item",   "id")
config_mgr:init_table("weapon", "id")
config_mgr:init_table("decal",  "id")
config_mgr:init_table("idcard", "id")
config_mgr:init_table("currency", "id")
config_mgr:init_table("weapontype", "id")
config_mgr:init_table("weaponlevel", "lv")
config_mgr:init_table("drop", "drop_id")
config_mgr:init_table("itemidinterval", "item_type")

config_mgr:init_table("battlepassdaytask", "id")
config_mgr:init_table("battlepasslooptask", "id")
config_mgr:init_table("battlepassweektask", "id")
config_mgr:init_table("battlepassprize", "index")
config_mgr:init_table("battlepassclue", "id")

config_mgr:init_table("division", "id")
config_mgr:init_table("divisionpoints", "id")
config_mgr:init_table("divisionstar", "star_id")
config_mgr:init_table("divisionreward", "id")

config_mgr:init_table("role", "role_id")
config_mgr:init_table("freerole", "id")
config_mgr:init_table("roleprofile", "role_id")
config_mgr:init_table("roleskin", "role_skin_id")
config_mgr:init_table("rolevoice", "role_voice_id")
config_mgr:init_table("roleaction", "role_action_id")

config_mgr:init_table("playerlevel", "lv")
config_mgr:init_table("playerheadicon", "id")
config_mgr:init_table("accountdefaultdata", "id")

config_mgr:init_table("buff", "buff_id")
config_mgr:init_table("effect", "effect_id")
config_mgr:init_table("achievement", "id")
config_mgr:init_table("achievementtype", "id", "level")

config_mgr:init_table("utility", "id")
config_mgr:init_table("functionunlock", "id")
config_mgr:init_table("component", "component_id")
config_mgr:init_table("parameter", "parameter_id")

config_mgr:init_enum_table("attribute", "PlayerAttrID", "id")
config_mgr:init_enum_table("careerattribute", "CareerAttrID", "id")
config_mgr:init_enum_table("behaviorattribute", "BehaviorAttrID", "id")


import("share/share.lua")
import("api/plat/plat_api.lua")
import("api/plat/plat_code.lua")
import("api/steam/steam_api.lua")
import("kernel/cache/cache_agent.lua")
import("lobby/const.lua")
import("dlog_game/dlog_mgr.lua")
import("lobby/util/item_util.lua")
import("lobby/dao/admin_dao.lua")
import("lobby/dao/player_dao.lua")
import("lobby/dao/account_dao.lua")
import("lobby/dao/career_image_dao.lua")
import("lobby/entity/effect/effect_helper.lua")
import("lobby/entity/effect_mgr.lua")
import("lobby/entity/condition_mgr.lua")
import("lobby/entity/reward_mgr.lua")
import("lobby/entity/battlepass_mgr.lua")
import("lobby/entity/rankseason_mgr.lua")
import("lobby/entity/free_role_mgr.lua")
import("lobby/entity/token_mgr.lua")
import("lobby/entity/player_mgr.lua")
import("lobby/entity/behavior_mgr.lua")
import("lobby/entity/account_mgr.lua")
import("lobby/entity/career_mgr.lua")
import("lobby/entity/achievement_mgr.lua")
import("lobby/entity/role.lua")
import("lobby/util/servlet_util.lua")
import("lobby/servlet/setting_servlet.lua")
import("lobby/servlet/role_servlet.lua")
import("lobby/servlet/team_servlet.lua")
import("lobby/servlet/room_servlet.lua")
import("lobby/servlet/match_servlet.lua")
import("lobby/servlet/watch_servlet.lua")
import("lobby/servlet/account_servlet.lua")
import("lobby/servlet/report_servlet.lua")
import("lobby/servlet/bag_servlet.lua")
import("lobby/servlet/gm_servlet.lua")
import("lobby/servlet/settle_servlet.lua")
import("lobby/servlet/prepare_servlet.lua")
import("lobby/servlet/mail_servlet.lua")
import("lobby/servlet/attribute_servlet.lua")
import("lobby/servlet/reward_servlet.lua")
import("lobby/servlet/vcard_servlet.lua")
import("lobby/servlet/attachment_servlet.lua")
import("lobby/servlet/dropstar_servlet.lua")
import("lobby/servlet/shop_servlet.lua")
import("lobby/servlet/career_servlet.lua")
import("lobby/servlet/buff_servlet.lua")
import("lobby/servlet/standings_servlet.lua")
import("lobby/servlet/recharge_servlet.lua")
import("lobby/servlet/achievement_servlet.lua")
import("lobby/servlet/season_servlet.lua")
import("lobby/servlet/battlepass_servlet.lua")

import("lobby/gm/bag_gm.lua")
import("lobby/gm/attribute_gm.lua")
import("lobby/gm/account_gm.lua")
import("lobby/gm/misc_gm.lua")
import("lobby/gm/role_gm.lua")
import("lobby/gm/battle_pass_gm.lua")

