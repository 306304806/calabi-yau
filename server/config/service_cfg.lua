--service_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local service = config_mgr:get_table("service")

--导出版本号
service:set_version(10000)

--导出配置内容
service:upsert({
    router_group = {},
    id = 1,
    index = 0,
    name = 'router',
})

service:upsert({
    router_group = {},
    id = 2,
    index = 0,
    name = 'monitor',
})

service:upsert({
    router_group = {},
    id = 3,
    index = 0,
    name = 'robot',
})

service:upsert({
    router_group = {1001,2001,3001},
    id = 4,
    index = 1,
    name = 'test',
})

service:upsert({
    router_group = {1001,2001},
    id = 5,
    index = 1,
    name = 'dbsvr',
})

service:upsert({
    router_group = {1001,2001},
    id = 6,
    index = 1,
    name = 'proxy',
})

service:upsert({
    router_group = {1001},
    id = 7,
    index = 1,
    name = 'dirsvr',
})

service:upsert({
    router_group = {1001,3001},
    id = 8,
    index = 1,
    name = 'lobby',
})

service:upsert({
    router_group = {3001},
    id = 9,
    index = 1,
    name = 'cachesvr',
})

service:upsert({
    router_group = {1001},
    id = 10,
    index = 1,
    name = 'match',
})

service:upsert({
    router_group = {1001,2001},
    id = 11,
    index = 1,
    name = 'room',
})

service:upsert({
    router_group = {1001},
    id = 12,
    index = 1,
    name = 'team',
})

service:upsert({
    router_group = {1001},
    id = 13,
    index = 1,
    name = 'center',
})

service:upsert({
    router_group = {1001},
    id = 14,
    index = 1,
    name = 'index',
})

service:upsert({
    router_group = {2001},
    id = 15,
    index = 1,
    name = 'dscenter',
})

service:upsert({
    router_group = {2001},
    id = 16,
    index = 1,
    name = 'dsagent',
})
