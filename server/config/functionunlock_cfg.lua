--functionunlock_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local functionunlock = config_mgr:get_table("functionunlock")

--导出版本号
functionunlock:set_version(10000)

--导出配置内容
functionunlock:upsert({
    id = 1,
    player_level = 9999,
})

functionunlock:upsert({
    id = 2,
    player_level = 1,
})

functionunlock:upsert({
    id = 3,
    player_level = 1,
})

functionunlock:upsert({
    id = 4,
    player_level = 1,
})

functionunlock:upsert({
    id = 5,
    player_level = 9999,
})

functionunlock:upsert({
    id = 30,
    player_level = 1,
    lock_tips = '玩家{PlayerLevel}级时解锁',
})
