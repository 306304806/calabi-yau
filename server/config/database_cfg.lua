--database_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local database = config_mgr:get_table("database")

--导出版本号
database:set_version(10000)

--导出配置内容
database:upsert({
    quanta_deploy = 'publish',
    group = 1,
    index = 1,
    driver = 'mongo',
    db = 'klbq_area_pub_1',
    host = '10.100.0.19',
    port = 27017,
})

database:upsert({
    quanta_deploy = 'publish',
    group = 1,
    index = 2,
    driver = 'mongo',
    db = 'klbq_area_pub_2',
    host = '10.100.0.19',
    port = 27017,
})

database:upsert({
    quanta_deploy = 'publish',
    group = 2,
    index = 1,
    driver = 'mongo',
    db = 'klbq_global_pub',
    host = '10.100.0.19',
    port = 27017,
})

database:upsert({
    quanta_deploy = 'publish',
    group = 3,
    index = 1,
    driver = 'mongo',
    db = 'klbq_hash_pub_1',
    host = '10.100.0.19',
    port = 27017,
})

database:upsert({
    quanta_deploy = 'publish',
    group = 3,
    index = 2,
    driver = 'mongo',
    db = 'klbq_hash_pub_2',
    host = '10.100.0.19',
    port = 27017,
})

database:upsert({
    quanta_deploy = 'publish',
    group = 3,
    index = 3,
    driver = 'mongo',
    db = 'klbq_hash_pub_3',
    host = '10.100.0.19',
    port = 27017,
})

database:upsert({
    quanta_deploy = 'develop',
    group = 1,
    index = 1,
    driver = 'mongo',
    db = 'klbq_area_dev_1',
    host = '10.100.0.19',
    port = 27017,
})

database:upsert({
    quanta_deploy = 'develop',
    group = 1,
    index = 2,
    driver = 'mongo',
    db = 'klbq_area_dev_2',
    host = '10.100.0.19',
    port = 27017,
})

database:upsert({
    quanta_deploy = 'develop',
    group = 2,
    index = 1,
    driver = 'mongo',
    db = 'klbq_global_dev',
    host = '10.100.0.19',
    port = 27017,
})

database:upsert({
    quanta_deploy = 'develop',
    group = 3,
    index = 1,
    driver = 'mongo',
    db = 'klbq_hash_dev_1',
    host = '10.100.0.19',
    port = 27017,
})

database:upsert({
    quanta_deploy = 'develop',
    group = 3,
    index = 2,
    driver = 'mongo',
    db = 'klbq_hash_dev_2',
    host = '10.100.0.19',
    port = 27017,
})

database:upsert({
    quanta_deploy = 'develop',
    group = 3,
    index = 3,
    driver = 'mongo',
    db = 'klbq_hash_dev_3',
    host = '10.100.0.19',
    port = 27017,
})

database:upsert({
    quanta_deploy = 'local',
    group = 1,
    index = 1,
    driver = 'mongo',
    db = 'klbq_area_loc_1',
    host = '10.100.0.19',
    port = 27017,
})

database:upsert({
    quanta_deploy = 'local',
    group = 1,
    index = 2,
    driver = 'mongo',
    db = 'klbq_area_loc_2',
    host = '10.100.0.19',
    port = 27017,
})

database:upsert({
    quanta_deploy = 'local',
    group = 2,
    index = 1,
    driver = 'mongo',
    db = 'klbq_global_loc',
    host = '10.100.0.19',
    port = 27017,
})

database:upsert({
    quanta_deploy = 'local',
    group = 3,
    index = 1,
    driver = 'mongo',
    db = 'klbq_hash_loc_1',
    host = '10.100.0.19',
    port = 27017,
})

database:upsert({
    quanta_deploy = 'local',
    group = 3,
    index = 2,
    driver = 'mongo',
    db = 'klbq_hash_loc_2',
    host = '10.100.0.19',
    port = 27017,
})

database:upsert({
    quanta_deploy = 'local',
    group = 3,
    index = 3,
    driver = 'mongo',
    db = 'klbq_hash_loc_3',
    host = '10.100.0.19',
    port = 27017,
})
