--battlepasslooptask_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local battlepasslooptask = config_mgr:get_table("battlepasslooptask")

--导出版本号
battlepasslooptask:set_version(10000)

--导出配置内容
battlepasslooptask:upsert({
    id = 20000,
    circle = true,
    desc = '获得10000点经验值',
    type = 3,
    conditions = {{behavior_id=9,value=10000,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})
