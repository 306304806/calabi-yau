--idcard_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local idcard = config_mgr:get_table("idcard")

--导出版本号
idcard:set_version(10000)

--导出配置内容
idcard:upsert({
    id = 31101001,
    type = 1,
    default = true,
    name = '米雪儿·李',
    sort_id = 1,
    limit_num = 99,
    limit_type = 0,
    limit_param = 0,
    gain_type = {0},
    gain_param1 = {},
})

idcard:upsert({
    id = 31101002,
    type = 1,
    default = false,
    name = '名片形象2',
    sort_id = 2,
    limit_num = 99,
    limit_type = 0,
    limit_param = 0,
    gain_type = {0},
    gain_param1 = {},
})

idcard:upsert({
    id = 31105001,
    type = 1,
    default = false,
    name = '奥黛丽',
    sort_id = 3,
    limit_num = 99,
    limit_type = 0,
    limit_param = 0,
    gain_type = {0},
    gain_param1 = {},
})

idcard:upsert({
    id = 31128001,
    type = 1,
    default = false,
    name = '拉薇',
    sort_id = 3,
    limit_num = 99,
    limit_type = 0,
    limit_param = 0,
    gain_type = {0},
    gain_param1 = {},
})

idcard:upsert({
    id = 31107001,
    type = 1,
    default = false,
    name = '玛德蕾娜',
    sort_id = 4,
    limit_num = 99,
    limit_type = 0,
    limit_param = 0,
    gain_type = {0},
    gain_param1 = {},
})

idcard:upsert({
    id = 31132001,
    type = 1,
    default = false,
    name = '明',
    sort_id = 1,
    limit_num = 99,
    limit_type = 0,
    limit_param = 0,
    gain_type = {0},
    gain_param1 = {},
})

idcard:upsert({
    id = 32000001,
    type = 2,
    default = false,
    name = '名片背景1',
    sort_id = 1,
    limit_num = 99,
    limit_type = 0,
    limit_param = 0,
    gain_type = {0},
    gain_param1 = {},
})

idcard:upsert({
    id = 32000002,
    type = 2,
    default = false,
    name = '名片背景2',
    sort_id = 2,
    limit_num = 99,
    limit_type = 0,
    limit_param = 0,
    gain_type = {0},
    gain_param1 = {},
})

idcard:upsert({
    id = 32000003,
    type = 2,
    default = false,
    name = '名片背景3',
    sort_id = 3,
    limit_num = 99,
    limit_type = 0,
    limit_param = 0,
    gain_type = {0},
    gain_param1 = {},
})

idcard:upsert({
    id = 32000004,
    type = 2,
    default = true,
    name = '名片背景4',
    sort_id = 4,
    limit_num = 99,
    limit_type = 0,
    limit_param = 0,
    gain_type = {0},
    gain_param1 = {},
})

idcard:upsert({
    id = 32000005,
    type = 2,
    default = false,
    name = '名片背景5',
    sort_id = 1,
    limit_num = 99,
    limit_type = 0,
    limit_param = 0,
    gain_type = {0},
    gain_param1 = {},
})

idcard:upsert({
    id = 33000001,
    type = 3,
    default = false,
    name = '名片边框1',
    sort_id = 1,
    limit_num = 99,
    limit_type = 0,
    limit_param = 0,
    gain_type = {0},
    gain_param1 = {},
})

idcard:upsert({
    id = 33000002,
    type = 3,
    default = false,
    name = '名片边框2',
    sort_id = 2,
    limit_num = 99,
    limit_type = 0,
    limit_param = 0,
    gain_type = {0},
    gain_param1 = {},
})

idcard:upsert({
    id = 33000003,
    type = 3,
    default = true,
    name = '名片边框3',
    sort_id = 3,
    limit_num = 99,
    limit_type = 0,
    limit_param = 0,
    gain_type = {0},
    gain_param1 = {},
})

idcard:upsert({
    id = 33000004,
    type = 3,
    default = false,
    name = '名片边框4',
    sort_id = 4,
    limit_num = 99,
    limit_type = 0,
    limit_param = 0,
    gain_type = {0},
    gain_param1 = {},
})

idcard:upsert({
    id = 33000005,
    type = 3,
    default = false,
    name = '名片边框5',
    sort_id = 1,
    limit_num = 99,
    limit_type = 0,
    limit_param = 0,
    gain_type = {0},
    gain_param1 = {},
})
