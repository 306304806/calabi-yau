--roleskin_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local roleskin = config_mgr:get_table("roleskin")

--导出版本号
roleskin:set_version(10000)

--导出配置内容
roleskin:upsert({
    role_skin_id = 20101001,
    name_cn = '米雪儿・李・默认',
    role_id = -2200464000,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20101002,
    name_cn = '米雪儿・李・绯红行动',
    role_id = -2200464000,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {1},
    gain_param1 = {20101002},
})

roleskin:upsert({
    role_skin_id = 20101003,
    name_cn = '米雪儿・李・落樱缤纷',
    role_id = -2200464000,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {1},
    gain_param1 = {20101003},
})

roleskin:upsert({
    role_skin_id = 20101004,
    name_cn = '米雪儿・李・露草微光',
    role_id = -2200464000,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {1},
    gain_param1 = {20101004},
})

roleskin:upsert({
    role_skin_id = 20105001,
    name_cn = '奥黛丽・默认',
    role_id = -2200118400,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20105002,
    name_cn = '奥黛丽・白蔷薇',
    role_id = -2200118400,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {1},
    gain_param1 = {20105002},
})

roleskin:upsert({
    role_skin_id = 20105003,
    name_cn = '奥黛丽・绿野仙踪',
    role_id = -2200118400,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {1},
    gain_param1 = {20105003},
})

roleskin:upsert({
    role_skin_id = 20105004,
    name_cn = '奥黛丽・天文时计',
    role_id = -2200118400,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {1},
    gain_param1 = {20105004},
})

roleskin:upsert({
    role_skin_id = 20105005,
    name_cn = '奥黛丽・维多利亚时代',
    role_id = -2200118400,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {1},
    gain_param1 = {20105005},
})

roleskin:upsert({
    role_skin_id = 20105006,
    name_cn = '奥黛丽・阿瓦隆之誓',
    role_id = -2200118400,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {1},
    gain_param1 = {20105006},
})

roleskin:upsert({
    role_skin_id = 20105007,
    name_cn = '奥黛丽・夏日沙滩',
    role_id = -2200118400,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {1},
    gain_param1 = {20105007},
})

roleskin:upsert({
    role_skin_id = 20105008,
    name_cn = '奥黛丽・冬日暖阳',
    role_id = -2200118400,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {1},
    gain_param1 = {20105008},
})

roleskin:upsert({
    role_skin_id = 20105009,
    name_cn = '奥黛丽・青春校园',
    role_id = -2200118400,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {1},
    gain_param1 = {20105009},
})

roleskin:upsert({
    role_skin_id = 20132001,
    name_cn = '明・默认',
    role_id = -2197785600,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20132002,
    name_cn = '明・绯红行动',
    role_id = -2197785600,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20132003,
    name_cn = '明・落樱缤纷',
    role_id = -2197785600,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20132004,
    name_cn = '明・露草微光',
    role_id = -2197785600,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20107001,
    name_cn = '玛德蕾娜・默认',
    role_id = -2199945600,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20107002,
    name_cn = '玛德蕾娜・白蔷薇',
    role_id = -2199945600,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20107003,
    name_cn = '玛德蕾娜・绿野仙踪',
    role_id = -2199945600,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20107004,
    name_cn = '玛德蕾娜・天文时计',
    role_id = -2199945600,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20107005,
    name_cn = '玛德蕾娜・维多利亚时代',
    role_id = -2199945600,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20107006,
    name_cn = '玛德蕾娜・阿瓦隆之誓',
    role_id = -2199945600,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20107007,
    name_cn = '玛德蕾娜・夏日沙滩',
    role_id = -2199945600,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20107008,
    name_cn = '玛德蕾娜・冬日暖阳',
    role_id = -2199945600,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20107009,
    name_cn = '玛德蕾娜・青春校园',
    role_id = -2199945600,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20128001,
    name_cn = '拉薇',
    role_id = -2198131200,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20128002,
    name_cn = '拉薇・白蔷薇',
    role_id = -2198131200,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20128003,
    name_cn = '拉薇・绿野仙踪',
    role_id = -2198131200,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20128004,
    name_cn = '拉薇・天文时计',
    role_id = -2198131200,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20128005,
    name_cn = '拉薇・维多利亚时代',
    role_id = -2198131200,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20128006,
    name_cn = '拉薇・阿瓦隆之誓',
    role_id = -2198131200,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20128007,
    name_cn = '拉薇・夏日沙滩',
    role_id = -2198131200,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20128008,
    name_cn = '拉薇・冬日暖阳',
    role_id = -2198131200,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20128009,
    name_cn = '拉薇・青春校园',
    role_id = -2198131200,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20108001,
    name_cn = '米雪儿・李・默认',
    role_id = -2199859200,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20108002,
    name_cn = '米雪儿・李・绯红行动',
    role_id = -2199859200,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20108003,
    name_cn = '米雪儿・李・落樱缤纷',
    role_id = -2199859200,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20108004,
    name_cn = '米雪儿・李・露草微光',
    role_id = -2199859200,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20110001,
    name_cn = '默认皮肤',
    role_id = -2199686400,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20111001,
    name_cn = '默认皮肤',
    role_id = -2199600000,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20112001,
    name_cn = '默认皮肤',
    role_id = -2199513600,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20113001,
    name_cn = '默认皮肤',
    role_id = -2199427200,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20114001,
    name_cn = '拉薇',
    role_id = -2199340800,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20106001,
    name_cn = '默认皮肤',
    role_id = -2200032000,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20116001,
    name_cn = '默认皮肤',
    role_id = -2199168000,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20117001,
    name_cn = '默认皮肤',
    role_id = -2199081600,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20118001,
    name_cn = '默认皮肤',
    role_id = -2198995200,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20120001,
    name_cn = '默认皮肤',
    role_id = -2198822400,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20121001,
    name_cn = '默认皮肤',
    role_id = -2198736000,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20122001,
    name_cn = '默认皮肤',
    role_id = -2198649600,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20123001,
    name_cn = '默认皮肤',
    role_id = -2198563200,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20124001,
    name_cn = '默认皮肤',
    role_id = -2198476800,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20125001,
    name_cn = '默认皮肤',
    role_id = -2198390400,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20126001,
    name_cn = '默认皮肤',
    role_id = -2198304000,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20127001,
    name_cn = '默认皮肤',
    role_id = -2198217600,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20129001,
    name_cn = '默认皮肤',
    role_id = -2198044800,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20130001,
    name_cn = '默认皮肤',
    role_id = -2197958400,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20131001,
    name_cn = '默认皮肤',
    role_id = -2197872000,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20133001,
    name_cn = '默认皮肤',
    role_id = -2197699200,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20134001,
    name_cn = '默认皮肤',
    role_id = -2197612800,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20135001,
    name_cn = '默认皮肤',
    role_id = -2197526400,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20136001,
    name_cn = '默认皮肤',
    role_id = -2197440000,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20137001,
    name_cn = '默认皮肤',
    role_id = -2197353600,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleskin:upsert({
    role_skin_id = 20138001,
    name_cn = '默认皮肤',
    role_id = -2197267200,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})
