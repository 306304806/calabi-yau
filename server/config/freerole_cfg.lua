--freerole_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local freerole = config_mgr:get_table("freerole")

--导出版本号
freerole:set_version(10000)

--导出配置内容
freerole:upsert({
    id = 1,
    start_time = 1616018400,
    end_time = 1637186400,
    role_ids = {101},
})
