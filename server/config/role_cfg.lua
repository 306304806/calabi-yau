--role_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local role = config_mgr:get_table("role")

--导出版本号
role:set_version(10000)

--导出配置内容
role:upsert({
    role_id = 101,
    hp_severe_injured = 100,
    hp_on_recovered = 50,
    role_skin = 20101001,
    default_weapon1 = 10101001,
    default_weapon2 = 12301001,
    default_weapon3 = 15101001,
    default_weapon4 = 18101001,
    default_weapon5 = 18301001,
    skill_active = {1011},
    skill_passive = {1012},
    skill_ultimate = {1073},
    quality = 0,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {1},
    gain_param1 = {101},
})

role:upsert({
    role_id = 105,
    hp_severe_injured = 100,
    hp_on_recovered = 50,
    role_skin = 20105001,
    default_weapon1 = 10403001,
    default_weapon2 = 12303001,
    default_weapon3 = 15101001,
    default_weapon4 = 18101001,
    default_weapon5 = 18301001,
    skill_active = {1051},
    skill_passive = {1052},
    skill_ultimate = {1073},
    quality = 0,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {1},
    gain_param1 = {105},
})

role:upsert({
    role_id = 132,
    hp_severe_injured = 100,
    hp_on_recovered = 50,
    role_skin = 20132001,
    default_weapon1 = 10102001,
    default_weapon2 = 12201001,
    default_weapon3 = 15101001,
    default_weapon4 = 18101001,
    default_weapon5 = 18301001,
    skill_active = {1321},
    skill_passive = {1322},
    skill_ultimate = {1323},
    quality = 0,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {1},
    gain_param1 = {101},
})

role:upsert({
    role_id = 107,
    hp_severe_injured = 100,
    hp_on_recovered = 50,
    role_skin = 20107001,
    default_weapon1 = 10301001,
    default_weapon2 = 10501001,
    default_weapon3 = 15101001,
    default_weapon4 = 18101001,
    default_weapon5 = 18301001,
    skill_active = {1071},
    skill_passive = {1072},
    skill_ultimate = {1073},
    quality = 0,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {1},
    gain_param1 = {101},
})

role:upsert({
    role_id = 128,
    hp_severe_injured = 100,
    hp_on_recovered = 50,
    role_skin = 20128001,
    default_weapon1 = 10104001,
    default_weapon2 = 12302001,
    default_weapon3 = 15101001,
    default_weapon4 = 18101001,
    default_weapon5 = 18301001,
    skill_active = {1281},
    skill_passive = {1282},
    skill_ultimate = {1283},
    quality = 0,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {1},
    gain_param1 = {101},
})
