--weaponlevel_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local weaponlevel = config_mgr:get_table("weaponlevel")

--导出版本号
weaponlevel:set_version(10000)

--导出配置内容
weaponlevel:upsert({
    lv = 1,
    exp = 500,
})

weaponlevel:upsert({
    lv = 2,
    exp = 500,
})

weaponlevel:upsert({
    lv = 3,
    exp = 500,
})

weaponlevel:upsert({
    lv = 4,
    exp = 500,
})

weaponlevel:upsert({
    lv = 5,
    exp = 500,
})

weaponlevel:upsert({
    lv = 6,
    exp = 600,
})

weaponlevel:upsert({
    lv = 7,
    exp = 600,
})

weaponlevel:upsert({
    lv = 8,
    exp = 600,
})

weaponlevel:upsert({
    lv = 9,
    exp = 600,
})

weaponlevel:upsert({
    lv = 10,
    exp = 600,
})

weaponlevel:upsert({
    lv = 11,
    exp = 700,
})

weaponlevel:upsert({
    lv = 12,
    exp = 700,
})

weaponlevel:upsert({
    lv = 13,
    exp = 700,
})

weaponlevel:upsert({
    lv = 14,
    exp = 700,
})

weaponlevel:upsert({
    lv = 15,
    exp = 700,
})

weaponlevel:upsert({
    lv = 16,
    exp = 800,
})

weaponlevel:upsert({
    lv = 17,
    exp = 800,
})

weaponlevel:upsert({
    lv = 18,
    exp = 800,
})

weaponlevel:upsert({
    lv = 19,
    exp = 800,
})

weaponlevel:upsert({
    lv = 20,
    exp = 900,
})

weaponlevel:upsert({
    lv = 21,
    exp = 900,
})

weaponlevel:upsert({
    lv = 22,
    exp = 900,
})

weaponlevel:upsert({
    lv = 23,
    exp = 900,
})

weaponlevel:upsert({
    lv = 24,
    exp = 1000,
})

weaponlevel:upsert({
    lv = 25,
    exp = 1000,
})

weaponlevel:upsert({
    lv = 26,
    exp = 1000,
})

weaponlevel:upsert({
    lv = 27,
    exp = 1000,
})

weaponlevel:upsert({
    lv = 28,
    exp = 1100,
})

weaponlevel:upsert({
    lv = 29,
    exp = 1100,
})

weaponlevel:upsert({
    lv = 30,
    exp = 1100,
})

weaponlevel:upsert({
    lv = 31,
    exp = 1200,
})

weaponlevel:upsert({
    lv = 32,
    exp = 1200,
})

weaponlevel:upsert({
    lv = 33,
    exp = 1200,
})

weaponlevel:upsert({
    lv = 34,
    exp = 1300,
})

weaponlevel:upsert({
    lv = 35,
    exp = 1300,
})

weaponlevel:upsert({
    lv = 36,
    exp = 1300,
})

weaponlevel:upsert({
    lv = 37,
    exp = 1400,
})

weaponlevel:upsert({
    lv = 38,
    exp = 1400,
})

weaponlevel:upsert({
    lv = 39,
    exp = 1400,
})

weaponlevel:upsert({
    lv = 40,
    exp = 1500,
})

weaponlevel:upsert({
    lv = 41,
    exp = 1500,
})

weaponlevel:upsert({
    lv = 42,
    exp = 1500,
})

weaponlevel:upsert({
    lv = 43,
    exp = 1600,
})

weaponlevel:upsert({
    lv = 44,
    exp = 1600,
})

weaponlevel:upsert({
    lv = 45,
    exp = 1700,
})

weaponlevel:upsert({
    lv = 46,
    exp = 1700,
})

weaponlevel:upsert({
    lv = 47,
    exp = 1800,
})

weaponlevel:upsert({
    lv = 48,
    exp = 1800,
})

weaponlevel:upsert({
    lv = 49,
    exp = 1900,
})

weaponlevel:upsert({
    lv = 50,
})
