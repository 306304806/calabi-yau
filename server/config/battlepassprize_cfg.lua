--battlepassprize_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local battlepassprize = config_mgr:get_table("battlepassprize")

--导出版本号
battlepassprize:set_version(10000)

--导出配置内容
battlepassprize:upsert({
    index = 1,
    season = 1,
    id = 1,
    explore = 0,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=101010020,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 2,
    season = 1,
    id = 2,
    explore = 100,
    prize1 = {{item_id=10101,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=201010020,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 3,
    season = 1,
    id = 3,
    explore = 200,
    prize1 = {{item_id=10102,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10201,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 4,
    season = 1,
    id = 4,
    explore = 300,
    prize1 = {{item_id=10201,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=101010030,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 5,
    season = 1,
    id = 5,
    explore = 400,
    prize1 = {{item_id=10301,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=201320030,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 6,
    season = 1,
    id = 6,
    explore = 500,
    prize1 = {{item_id=10302,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=201320040,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 7,
    season = 1,
    id = 7,
    explore = 600,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=311010010,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 8,
    season = 1,
    id = 8,
    explore = 700,
    prize1 = {{item_id=10101,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=211010010,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 9,
    season = 1,
    id = 9,
    explore = 800,
    prize1 = {{item_id=10102,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=221010010,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 10,
    season = 1,
    id = 10,
    explore = 900,
    prize1 = {{item_id=10201,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=300000010,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 11,
    season = 1,
    id = 11,
    explore = 1000,
    prize1 = {{item_id=10301,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=311010010,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 12,
    season = 1,
    id = 12,
    explore = 1100,
    prize1 = {{item_id=10302,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=320000010,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 13,
    season = 1,
    id = 13,
    explore = 1200,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=330000010,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 14,
    season = 1,
    id = 14,
    explore = 1300,
    prize1 = {{item_id=10101,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=201050050,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 15,
    season = 1,
    id = 15,
    explore = 1400,
    prize1 = {{item_id=10102,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 16,
    season = 1,
    id = 16,
    explore = 1500,
    prize1 = {{item_id=10201,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 17,
    season = 1,
    id = 17,
    explore = 1600,
    prize1 = {{item_id=10301,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 18,
    season = 1,
    id = 18,
    explore = 1700,
    prize1 = {{item_id=10302,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 19,
    season = 1,
    id = 19,
    explore = 1800,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 20,
    season = 1,
    id = 20,
    explore = 1900,
    prize1 = {{item_id=10101,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 21,
    season = 1,
    id = 21,
    explore = 2000,
    prize1 = {{item_id=10102,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 22,
    season = 1,
    id = 22,
    explore = 2100,
    prize1 = {{item_id=10201,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 23,
    season = 1,
    id = 23,
    explore = 2200,
    prize1 = {{item_id=10301,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 24,
    season = 1,
    id = 24,
    explore = 2300,
    prize1 = {{item_id=10302,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 25,
    season = 1,
    id = 25,
    explore = 2400,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 26,
    season = 1,
    id = 26,
    explore = 2500,
    prize1 = {{item_id=10101,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 27,
    season = 1,
    id = 27,
    explore = 2600,
    prize1 = {{item_id=10102,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 28,
    season = 1,
    id = 28,
    explore = 2700,
    prize1 = {{item_id=10201,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 29,
    season = 1,
    id = 29,
    explore = 2800,
    prize1 = {{item_id=10301,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 30,
    season = 1,
    id = 30,
    explore = 2900,
    prize1 = {{item_id=10302,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 31,
    season = 1,
    id = 31,
    explore = 3000,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 32,
    season = 1,
    id = 32,
    explore = 3100,
    prize1 = {{item_id=10101,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 33,
    season = 1,
    id = 33,
    explore = 3200,
    prize1 = {{item_id=10102,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 34,
    season = 1,
    id = 34,
    explore = 3300,
    prize1 = {{item_id=10201,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 35,
    season = 1,
    id = 35,
    explore = 3400,
    prize1 = {{item_id=10301,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 36,
    season = 1,
    id = 36,
    explore = 3500,
    prize1 = {{item_id=10302,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 37,
    season = 1,
    id = 37,
    explore = 3600,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 38,
    season = 1,
    id = 38,
    explore = 3700,
    prize1 = {{item_id=10101,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 39,
    season = 1,
    id = 39,
    explore = 3800,
    prize1 = {{item_id=10102,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 40,
    season = 1,
    id = 40,
    explore = 3900,
    prize1 = {{item_id=10201,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 41,
    season = 1,
    id = 41,
    explore = 4000,
    prize1 = {{item_id=10301,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 42,
    season = 1,
    id = 42,
    explore = 4100,
    prize1 = {{item_id=10302,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 43,
    season = 1,
    id = 43,
    explore = 4200,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 44,
    season = 1,
    id = 44,
    explore = 4300,
    prize1 = {{item_id=10101,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 45,
    season = 1,
    id = 45,
    explore = 4400,
    prize1 = {{item_id=10102,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 46,
    season = 1,
    id = 46,
    explore = 4500,
    prize1 = {{item_id=10201,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 47,
    season = 1,
    id = 47,
    explore = 4600,
    prize1 = {{item_id=10301,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 48,
    season = 1,
    id = 48,
    explore = 4700,
    prize1 = {{item_id=10302,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 49,
    season = 1,
    id = 49,
    explore = 4800,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 50,
    season = 1,
    id = 50,
    explore = 4900,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 51,
    season = 2,
    id = 1,
    explore = 0,
    prize1 = {{item_id=10302,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=201010020,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 52,
    season = 2,
    id = 2,
    explore = 100,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=101010020,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 53,
    season = 2,
    id = 3,
    explore = 200,
    prize1 = {{item_id=10101,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=101010030,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 54,
    season = 2,
    id = 4,
    explore = 300,
    prize1 = {{item_id=10102,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10201,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 55,
    season = 2,
    id = 5,
    explore = 400,
    prize1 = {{item_id=10201,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=201320030,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 56,
    season = 2,
    id = 6,
    explore = 500,
    prize1 = {{item_id=10301,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=330000010,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 57,
    season = 2,
    id = 7,
    explore = 600,
    prize1 = {{item_id=10302,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=311010010,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 58,
    season = 2,
    id = 8,
    explore = 700,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=311010010,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 59,
    season = 2,
    id = 9,
    explore = 800,
    prize1 = {{item_id=10101,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=221010010,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 60,
    season = 2,
    id = 10,
    explore = 900,
    prize1 = {{item_id=10102,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=201050050,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 61,
    season = 2,
    id = 11,
    explore = 1000,
    prize1 = {{item_id=10201,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=211010010,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 62,
    season = 2,
    id = 12,
    explore = 1100,
    prize1 = {{item_id=10301,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=320000010,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 63,
    season = 2,
    id = 13,
    explore = 1200,
    prize1 = {{item_id=10302,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=201320040,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 64,
    season = 2,
    id = 14,
    explore = 1300,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=300000010,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 65,
    season = 2,
    id = 15,
    explore = 1400,
    prize1 = {{item_id=10101,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 66,
    season = 2,
    id = 16,
    explore = 1500,
    prize1 = {{item_id=10102,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 67,
    season = 2,
    id = 17,
    explore = 1600,
    prize1 = {{item_id=10201,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 68,
    season = 2,
    id = 18,
    explore = 1700,
    prize1 = {{item_id=10301,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 69,
    season = 2,
    id = 19,
    explore = 1800,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 70,
    season = 2,
    id = 20,
    explore = 1900,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 71,
    season = 2,
    id = 21,
    explore = 2000,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 72,
    season = 2,
    id = 22,
    explore = 2100,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 73,
    season = 2,
    id = 23,
    explore = 2200,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 74,
    season = 2,
    id = 24,
    explore = 2300,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 75,
    season = 2,
    id = 25,
    explore = 2400,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 76,
    season = 2,
    id = 26,
    explore = 2500,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 77,
    season = 2,
    id = 27,
    explore = 2600,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 78,
    season = 2,
    id = 28,
    explore = 2700,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 79,
    season = 2,
    id = 29,
    explore = 2800,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 80,
    season = 2,
    id = 30,
    explore = 2900,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 81,
    season = 2,
    id = 31,
    explore = 3000,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 82,
    season = 2,
    id = 32,
    explore = 3100,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 83,
    season = 2,
    id = 33,
    explore = 3200,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 84,
    season = 2,
    id = 34,
    explore = 3300,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 85,
    season = 2,
    id = 35,
    explore = 3400,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 86,
    season = 2,
    id = 36,
    explore = 3500,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 87,
    season = 2,
    id = 37,
    explore = 3600,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 88,
    season = 2,
    id = 38,
    explore = 3700,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 89,
    season = 2,
    id = 39,
    explore = 3800,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 90,
    season = 2,
    id = 40,
    explore = 3900,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 91,
    season = 2,
    id = 41,
    explore = 4000,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 92,
    season = 2,
    id = 42,
    explore = 4100,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 93,
    season = 2,
    id = 43,
    explore = 4200,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 94,
    season = 2,
    id = 44,
    explore = 4300,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 95,
    season = 2,
    id = 45,
    explore = 4400,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 96,
    season = 2,
    id = 46,
    explore = 4500,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 97,
    season = 2,
    id = 47,
    explore = 4600,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 98,
    season = 2,
    id = 48,
    explore = 4700,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 99,
    season = 2,
    id = 49,
    explore = 4800,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})

battlepassprize:upsert({
    index = 100,
    season = 2,
    id = 50,
    explore = 4900,
    prize1 = {{item_id=10001,item_amount=1}},
    priority1 = 1,
    prize2 = {{item_id=10001,item_amount=1}},
    priority2 = 1,
})
