--achievement_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local achievement = config_mgr:get_table("achievement")

--导出版本号
achievement:set_version(10000)

--导出配置内容
achievement:upsert({
    id = 1,
    name = '伤害手',
    details = '卡拉比丘中常常存在这样的一种人，他们技巧高超，他们总能带领着团队走向胜利，这样的人被称之为伤害手',
    priority = 1,
    type = 1,
    param1 = 1,
    param2 = {100},
    param3 = 1,
    param4 = {1,2},
})

achievement:upsert({
    id = 3,
    name = '杀戮者',
    details = '有时候杀戮不是最好的办法，但却是走向胜利唯一的途径',
    priority = 3,
    type = 1,
    param1 = 3,
    param2 = {3},
    param3 = 1,
    param4 = {1,2},
})

achievement:upsert({
    id = 4,
    name = '生存大师',
    details = '有时候，活下去比什么都重要，只要还活着，就还有胜利的希望',
    priority = 4,
    type = 1,
    param1 = 4,
    param2 = {0},
    param3 = 1,
    param4 = {1,2},
})

achievement:upsert({
    id = 5,
    name = '有效打击',
    details = '神枪手？我不认识这样的家伙，根本就没有神枪手…或者说，人人都是神枪手！',
    priority = 5,
    type = 1,
    param1 = 5,
    param2 = {1},
    param3 = 1,
    param4 = {1,2},
})

achievement:upsert({
    id = 6,
    name = '爆头大师',
    details = '老子就是神枪手！',
    priority = 6,
    type = 1,
    param1 = 6,
    param2 = {10},
    param3 = 1,
    param4 = {1,2},
})

achievement:upsert({
    id = 7,
    name = '致命节奏',
    details = '在战场上，常常会有这样的一种人，他们技巧高超，他们身经百战，甚至是整个战局，都因为他们的存在而发生着风起云涌的变化',
    priority = 7,
    type = 1,
    param1 = 7,
    param2 = {10},
    param3 = 1,
    param4 = {1,2},
})

achievement:upsert({
    id = 8,
    name = '飞行大师',
    details = '这就是自由的感觉，这就是飞翔的感觉！飞啊飞，我骄傲的放纵！',
    priority = 8,
    type = 1,
    param1 = 8,
    param2 = {100},
    param3 = 1,
    param4 = {1,2},
})

achievement:upsert({
    id = 10,
    name = '多面手',
    details = '想要杀人，不一定非得要靠枪啊对不对，你说呢？',
    priority = 10,
    type = 1,
    param1 = 10,
    param2 = {1},
    param3 = 1,
    param4 = {1,2},
})

achievement:upsert({
    id = 11,
    name = '爆破手',
    details = '我有一个梦想，那就是让炸弹布满整个世界！',
    priority = 11,
    type = 1,
    param1 = 11,
    param2 = {2},
    param3 = 1,
    param4 = {1,2},
})

achievement:upsert({
    id = 12,
    name = '拆弹手',
    details = '我有一个梦想，那就是拆掉这个世界上所有的炸弹…',
    priority = 12,
    type = 1,
    param1 = 12,
    param2 = {2},
    param3 = 1,
    param4 = {1,2},
})

achievement:upsert({
    id = 101,
    name = '爆破鬼才',
    details = '仁慈的我，不忍心用我的枪魂来伤害敌人，所以……我选择用炸弹！',
    type = 2,
    param1 = 1,
    param2 = {3},
})

achievement:upsert({
    id = 102,
    name = '势如破竹',
    details = '不能解决爆破的问题，那就解决提出这个问题的人好了。',
    type = 2,
    param1 = 2,
    param2 = {5},
})

achievement:upsert({
    id = 103,
    name = '拆弹专家',
    details = '如果拆下来的炸弹可以卖废铁，也许……我能发财。',
    type = 2,
    param1 = 3,
    param2 = {4},
})

achievement:upsert({
    id = 104,
    name = '绝地翻盘',
    details = '有些人活着，但他们已经完了；有些人没了，但他们却赢了！',
    type = 2,
    param1 = 4,
    param2 = {4,5},
})

achievement:upsert({
    id = 105,
    name = '血腥杀戮',
    details = '队友？什么队友？我不需要队友。我只需要你们喊666！',
    type = 2,
    param1 = 5,
    param2 = {35},
})

achievement:upsert({
    id = 106,
    name = '潜能爆发',
    details = '说出来你们可能不相信，我刚刚一个人将他们包围了……',
    type = 2,
    param1 = 6,
    param2 = {15,5},
})

achievement:upsert({
    id = 107,
    name = '精准射击',
    details = '不是我射的准，而是他们接的实在是太好了…...',
    type = 2,
    param1 = 7,
    param2 = {3},
})

achievement:upsert({
    id = 108,
    name = '战场老兵',
    details = '我能一个打十个！',
    type = 2,
    param1 = 8,
    param2 = {10},
})

achievement:upsert({
    id = 109,
    name = '雷神',
    details = '一家人就是要齐齐整整的！',
    type = 2,
    param1 = 9,
    param2 = {3},
})

achievement:upsert({
    id = 110,
    name = '第六感',
    details = '我说我就随便开两枪，你们信吗？',
    type = 2,
    param1 = 10,
    param2 = {2},
})

achievement:upsert({
    id = 201,
    name = '三人成众',
    details = '在卡丘世界里，朋友多终归是有好处的。',
    type = 3,
    param1 = 1,
    param2 = {3},
    param3 = 1,
    param5 = {{item_id=20001,item_amount=1}},
})

achievement:upsert({
    id = 202,
    name = '友情羁绊',
    details = '只有与好友一起出生入死，才会体会到友谊的珍贵。',
    type = 3,
    param1 = 2,
    param2 = {100},
    param3 = 1,
    param5 = {{item_id=20001,item_amount=2}},
})

achievement:upsert({
    id = 203,
    name = '魅力达人',
    details = '这是在卡丘世界里人格魅力的证明！',
    type = 3,
    param1 = 3,
    param2 = {100},
    param3 = 1,
    param5 = {{item_id=20001,item_amount=3}},
})

achievement:upsert({
    id = 204,
    name = '全场焦点',
    details = '久经沙场的士兵，值得这份殊荣！',
    type = 3,
    param1 = 4,
    param2 = {100},
    param3 = 1,
    param5 = {{item_id=20001,item_amount=4}},
})

achievement:upsert({
    id = 205,
    name = '坚持不懈',
    details = '在卡丘世界中，即使是再不起眼的地方，坚持不懈也是会有回报的。',
    type = 3,
    param1 = 5,
    param2 = {100},
    param3 = 1,
    param5 = {{item_id=20001,item_amount=5}},
})

achievement:upsert({
    id = 206,
    name = '时间管理',
    details = '勤劳与准时，是在卡丘世界里生存的铁则。',
    type = 3,
    param1 = 6,
    param2 = {7},
    param3 = 1,
    param5 = {{item_id=20001,item_amount=6}},
})

achievement:upsert({
    id = 208,
    name = '角色达人',
    details = '有时候，多一个选择就多一条路',
    type = 3,
    param1 = 8,
    param2 = {10},
    param3 = 1,
    param5 = {{item_id=20001,item_amount=8}},
})

achievement:upsert({
    id = 209,
    name = '皮肤达人',
    details = '对自己喜欢的东西，就是不能犹豫',
    type = 3,
    param1 = 9,
    param2 = {10},
    param3 = 1,
    param5 = {{item_id=20001,item_amount=9}},
})

achievement:upsert({
    id = 210,
    name = '军火女王',
    details = '对待枪械要像对待情人一样……等等，你这情人有点儿多啊！',
    type = 3,
    param1 = 10,
    param2 = {30},
    param3 = 1,
    param5 = {{item_id=20001,item_amount=10}},
})

achievement:upsert({
    id = 211,
    name = '熟能生巧',
    details = '我能感觉到你对他们深深的爱',
    type = 3,
    param1 = 11,
    param2 = {2,2},
    param3 = 1,
    param5 = {{item_id=20001,item_amount=11}},
})

achievement:upsert({
    id = 212,
    name = '成长达人',
    details = '成长不会一蹴而就，且时常会有痛苦相随',
    type = 3,
    param1 = 12,
    param2 = {30},
    param3 = 1,
    param5 = {{item_id=20001,item_amount=12}},
})

achievement:upsert({
    id = 213,
    name = '军械专家',
    details = '工欲善其事必先利其器，会的多，总会有好处的',
    type = 3,
    param1 = 13,
    param2 = {10,2},
    param3 = 1,
    param5 = {{item_id=20001,item_amount=13}},
})

achievement:upsert({
    id = 220,
    name = '炸弹狂人',
    details = '距离让这个世界布满炸弹这个梦想又更近一步了呢！',
    type = 3,
    param1 = 15,
    param2 = {100},
    param3 = 1,
    param5 = {{item_id=20001,item_amount=20}},
})

achievement:upsert({
    id = 221,
    name = '拆弹狂人',
    details = '距离拆除掉这个世界所有的炸弹这个梦想又更近一步了呢！',
    type = 3,
    param1 = 16,
    param2 = {100},
    param3 = 1,
    param5 = {{item_id=20001,item_amount=21}},
})

achievement:upsert({
    id = 222,
    name = '杀戮机器',
    details = '你就这么渴望杀戮吗？',
    type = 3,
    param1 = 17,
    param2 = {10000},
    param3 = 1,
    param5 = {{item_id=20001,item_amount=22}},
})

achievement:upsert({
    id = 223,
    name = '绞肉机',
    details = '看看你手上沾染的鲜血吧！',
    type = 3,
    param1 = 18,
    param2 = {1000},
    param3 = 1,
    param5 = {{item_id=20001,item_amount=23}},
})

achievement:upsert({
    id = 224,
    name = '猎头者',
    details = '勋功章？难道你手上沾染的鲜血不就是你最好的勋功章吗？',
    type = 3,
    param1 = 19,
    param2 = {100},
    param3 = 1,
    param5 = {{item_id=20001,item_amount=24}},
})

achievement:upsert({
    id = 225,
    name = '勋章统计',
    details = '描述后面再说，目前测试逻辑1',
    type = 3,
    param1 = 21,
    param2 = {5},
    param3 = 1,
    param5 = {{item_id=20001,item_amount=22}},
})

achievement:upsert({
    id = 226,
    name = '史诗统计',
    details = '描述后面再说，目前测试逻辑2',
    type = 3,
    param1 = 22,
    param2 = {5},
    param3 = 1,
    param5 = {{item_id=20001,item_amount=22}},
})

achievement:upsert({
    id = 227,
    name = '荣耀统计',
    details = '描述后面再说，目前测试逻辑3',
    type = 3,
    param1 = 23,
    param2 = {5},
    param3 = 1,
    param5 = {{item_id=20001,item_amount=22}},
})
