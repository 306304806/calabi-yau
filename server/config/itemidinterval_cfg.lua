--itemidinterval_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local itemidinterval = config_mgr:get_table("itemidinterval")

--导出版本号
itemidinterval:set_version(10000)

--导出配置内容
itemidinterval:upsert({
    item_type = 1,
    id_lower = 1,
    id_upper = 9,
})

itemidinterval:upsert({
    item_type = 2,
    id_lower = 101,
    id_upper = 999,
})

itemidinterval:upsert({
    item_type = 3,
    id_lower = 10100001,
    id_upper = 19999999,
})

itemidinterval:upsert({
    item_type = 4,
    id_lower = 20101001,
    id_upper = 20999999,
})

itemidinterval:upsert({
    item_type = 5,
    id_lower = 21101001,
    id_upper = 21999999,
})

itemidinterval:upsert({
    item_type = 6,
    id_lower = 22101001,
    id_upper = 22999999,
})

itemidinterval:upsert({
    item_type = 7,
    id_lower = 30000001,
    id_upper = 30999999,
})

itemidinterval:upsert({
    item_type = 8,
    id_lower = 31101001,
    id_upper = 31999999,
})

itemidinterval:upsert({
    item_type = 9,
    id_lower = 32000001,
    id_upper = 32999999,
})

itemidinterval:upsert({
    item_type = 10,
    id_lower = 33000001,
    id_upper = 33999999,
})

itemidinterval:upsert({
    item_type = 100,
    id_lower = 10001,
    id_upper = 10001,
})

itemidinterval:upsert({
    item_type = 101,
    id_lower = 10101,
    id_upper = 10199,
})

itemidinterval:upsert({
    item_type = 102,
    id_lower = 10201,
    id_upper = 10299,
})

itemidinterval:upsert({
    item_type = 103,
    id_lower = 10301,
    id_upper = 10399,
})

itemidinterval:upsert({
    item_type = 190,
    id_lower = 19001,
    id_upper = 19001,
})

itemidinterval:upsert({
    item_type = 191,
    id_lower = 19101,
    id_upper = 19101,
})

itemidinterval:upsert({
    item_type = 192,
    id_lower = 19201,
    id_upper = 19201,
})

itemidinterval:upsert({
    item_type = 193,
    id_lower = 19301,
    id_upper = 19301,
})

itemidinterval:upsert({
    item_type = 200,
    id_lower = 20001,
    id_upper = 20999,
})

itemidinterval:upsert({
    item_type = 201,
    id_lower = 21001,
    id_upper = 21999,
})

itemidinterval:upsert({
    item_type = 302,
    id_lower = 1010,
    id_upper = 9999,
})

itemidinterval:upsert({
    item_type = 303,
    id_lower = 101000010,
    id_upper = 199999999,
})

itemidinterval:upsert({
    item_type = 304,
    id_lower = 201010010,
    id_upper = 209999999,
})

itemidinterval:upsert({
    item_type = 305,
    id_lower = 211010010,
    id_upper = 219999999,
})

itemidinterval:upsert({
    item_type = 306,
    id_lower = 221010010,
    id_upper = 229999999,
})

itemidinterval:upsert({
    item_type = 307,
    id_lower = 300000010,
    id_upper = 309999999,
})

itemidinterval:upsert({
    item_type = 308,
    id_lower = 311010010,
    id_upper = 319999999,
})

itemidinterval:upsert({
    item_type = 309,
    id_lower = 320000010,
    id_upper = 329999999,
})

itemidinterval:upsert({
    item_type = 310,
    id_lower = 330000010,
    id_upper = 339999999,
})
