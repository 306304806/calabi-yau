--divisionstar_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local divisionstar = config_mgr:get_table("divisionstar")

--导出版本号
divisionstar:set_version(10000)

--导出配置内容
divisionstar:upsert({
    star_id = 0,
    division = 1,
    show_star = 0,
    win = 1,
    lose = 0,
})

divisionstar:upsert({
    star_id = 1,
    division = 1,
    show_star = 1,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 2,
    division = 1,
    show_star = 2,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 3,
    division = 1,
    show_star = 3,
    win = 2,
    lose = 1,
})

divisionstar:upsert({
    star_id = 4,
    division = 2,
    show_star = 0,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 5,
    division = 2,
    show_star = 1,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 6,
    division = 2,
    show_star = 2,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 7,
    division = 2,
    show_star = 3,
    win = 2,
    lose = 1,
})

divisionstar:upsert({
    star_id = 8,
    division = 3,
    show_star = 0,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 9,
    division = 3,
    show_star = 1,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 10,
    division = 3,
    show_star = 2,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 11,
    division = 3,
    show_star = 3,
    win = 2,
    lose = 1,
})

divisionstar:upsert({
    star_id = 12,
    division = 4,
    show_star = 0,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 13,
    division = 4,
    show_star = 1,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 14,
    division = 4,
    show_star = 2,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 15,
    division = 4,
    show_star = 3,
    win = 2,
    lose = 1,
})

divisionstar:upsert({
    star_id = 16,
    division = 5,
    show_star = 0,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 17,
    division = 5,
    show_star = 1,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 18,
    division = 5,
    show_star = 2,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 19,
    division = 5,
    show_star = 3,
    win = 2,
    lose = 1,
})

divisionstar:upsert({
    star_id = 20,
    division = 6,
    show_star = 0,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 21,
    division = 6,
    show_star = 1,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 22,
    division = 6,
    show_star = 2,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 23,
    division = 6,
    show_star = 3,
    win = 2,
    lose = 1,
})

divisionstar:upsert({
    star_id = 24,
    division = 7,
    show_star = 0,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 25,
    division = 7,
    show_star = 1,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 26,
    division = 7,
    show_star = 2,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 27,
    division = 7,
    show_star = 3,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 28,
    division = 7,
    show_star = 4,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 29,
    division = 8,
    show_star = 0,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 30,
    division = 8,
    show_star = 1,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 31,
    division = 8,
    show_star = 2,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 32,
    division = 8,
    show_star = 3,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 33,
    division = 8,
    show_star = 4,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 34,
    division = 9,
    show_star = 0,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 35,
    division = 9,
    show_star = 1,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 36,
    division = 9,
    show_star = 2,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 37,
    division = 9,
    show_star = 3,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 38,
    division = 9,
    show_star = 4,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 39,
    division = 10,
    show_star = 0,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 40,
    division = 10,
    show_star = 1,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 41,
    division = 10,
    show_star = 2,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 42,
    division = 10,
    show_star = 3,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 43,
    division = 10,
    show_star = 4,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 44,
    division = 11,
    show_star = 0,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 45,
    division = 11,
    show_star = 1,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 46,
    division = 11,
    show_star = 2,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 47,
    division = 11,
    show_star = 3,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 48,
    division = 11,
    show_star = 4,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 49,
    division = 12,
    show_star = 0,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 50,
    division = 12,
    show_star = 1,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 51,
    division = 12,
    show_star = 2,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 52,
    division = 12,
    show_star = 3,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 53,
    division = 12,
    show_star = 4,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 54,
    division = 13,
    show_star = 0,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 55,
    division = 13,
    show_star = 1,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 56,
    division = 13,
    show_star = 2,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 57,
    division = 13,
    show_star = 3,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 58,
    division = 13,
    show_star = 4,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 59,
    division = 14,
    show_star = 0,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 60,
    division = 14,
    show_star = 1,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 61,
    division = 14,
    show_star = 2,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 62,
    division = 14,
    show_star = 3,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 63,
    division = 14,
    show_star = 4,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 64,
    division = 15,
    show_star = 0,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 65,
    division = 15,
    show_star = 1,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 66,
    division = 15,
    show_star = 2,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 67,
    division = 15,
    show_star = 3,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 68,
    division = 15,
    show_star = 4,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 69,
    division = 15,
    show_star = 5,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 70,
    division = 16,
    show_star = 0,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 71,
    division = 16,
    show_star = 1,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 72,
    division = 16,
    show_star = 2,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 73,
    division = 16,
    show_star = 3,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 74,
    division = 16,
    show_star = 4,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 75,
    division = 16,
    show_star = 5,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 76,
    division = 17,
    show_star = 0,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 77,
    division = 17,
    show_star = 1,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 78,
    division = 17,
    show_star = 2,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 79,
    division = 17,
    show_star = 3,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 80,
    division = 17,
    show_star = 4,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 81,
    division = 17,
    show_star = 5,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 82,
    division = 18,
    show_star = 0,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 83,
    division = 18,
    show_star = 1,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 84,
    division = 18,
    show_star = 2,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 85,
    division = 18,
    show_star = 3,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 86,
    division = 18,
    show_star = 4,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 87,
    division = 18,
    show_star = 5,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 88,
    division = 19,
    show_star = 0,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 89,
    division = 19,
    show_star = 1,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 90,
    division = 19,
    show_star = 2,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 91,
    division = 19,
    show_star = 3,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 92,
    division = 19,
    show_star = 4,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 93,
    division = 19,
    show_star = 5,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 94,
    division = 20,
    show_star = 0,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 95,
    division = 20,
    show_star = 1,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 96,
    division = 20,
    show_star = 2,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 97,
    division = 20,
    show_star = 3,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 98,
    division = 20,
    show_star = 4,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 99,
    division = 20,
    show_star = 5,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 100,
    division = 21,
    show_star = 0,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 101,
    division = 21,
    show_star = 1,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 102,
    division = 21,
    show_star = 2,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 103,
    division = 21,
    show_star = 3,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 104,
    division = 21,
    show_star = 4,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 105,
    division = 21,
    show_star = 5,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 106,
    division = 22,
    show_star = 0,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 107,
    division = 22,
    show_star = 1,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 108,
    division = 22,
    show_star = 2,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 109,
    division = 22,
    show_star = 3,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 110,
    division = 22,
    show_star = 4,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 111,
    division = 22,
    show_star = 5,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 112,
    division = 23,
    show_star = 0,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 113,
    division = 23,
    show_star = 1,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 114,
    division = 23,
    show_star = 2,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 115,
    division = 23,
    show_star = 3,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 116,
    division = 23,
    show_star = 4,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 117,
    division = 23,
    show_star = 5,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 118,
    division = 24,
    show_star = 0,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 119,
    division = 24,
    show_star = 1,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 120,
    division = 24,
    show_star = 2,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 121,
    division = 24,
    show_star = 3,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 122,
    division = 24,
    show_star = 4,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 123,
    division = 24,
    show_star = 5,
    win = 1,
    lose = 1,
})

divisionstar:upsert({
    star_id = 124,
    division = 25,
    show_star = 9999,
    win = 1,
    lose = 1,
})
