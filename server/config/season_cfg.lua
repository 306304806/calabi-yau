--season_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local season = config_mgr:get_table("season")

--导出版本号
season:set_version(10000)

--导出配置内容
season:upsert({
    id = 1,
    start = 1584482400,
})

season:upsert({
    id = 2,
    start = 1600380000,
})

season:upsert({
    id = 3,
    start = 1605650400,
})
