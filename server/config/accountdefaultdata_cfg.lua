--accountdefaultdata_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local accountdefaultdata = config_mgr:get_table("accountdefaultdata")

--导出版本号
accountdefaultdata:set_version(10000)

--导出配置内容
accountdefaultdata:upsert({
    id = 1,
    default_role = {101,132,105,107,128},
    default_roleskin = {20101001,20132001,20105001,20107001,20128001},
    default_item = {10001},
    default_appearance = {31101001,31105001,31128001,31107001,31132001},
    default_background = {32000001,32000004},
    default_frame = {33000001,33000003},
    default_weapon = {},
})
