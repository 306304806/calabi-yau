--attribute_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local attribute = config_mgr:get_table("attribute")

--导出版本号
attribute:set_version(10000)

--导出配置内容
attribute:upsert({
    id = 1,
    enum_key = 'NICK',
    name = '昵称',
    type = 'string',
    default = '默认昵称',
    range = 1,
    save2db = true,
    sync = false,
})

attribute:upsert({
    id = 2,
    enum_key = 'SEX',
    name = '性别',
    type = 'uint32',
    default = 0,
    range = 1,
    save2db = true,
    sync = false,
})

attribute:upsert({
    id = 3,
    enum_key = 'EXP',
    name = '经验值',
    type = 'uint32',
    default = 0,
    range = 1,
    save2db = true,
    sync = false,
})

attribute:upsert({
    id = 4,
    enum_key = 'LEVEL',
    name = '等级',
    type = 'uint32',
    default = 1,
    range = 1,
    save2db = true,
    sync = false,
})

attribute:upsert({
    id = 5,
    enum_key = 'CRYSTAL',
    name = '巴布晶体',
    type = 'uint64',
    default = 0,
    range = 1,
    save2db = true,
    sync = false,
})

attribute:upsert({
    id = 6,
    enum_key = 'IDEAL',
    name = '理想币',
    type = 'uint64',
    default = 0,
    range = 1,
    save2db = true,
    sync = false,
})

attribute:upsert({
    id = 7,
    enum_key = 'ICON',
    name = '图标',
    type = 'uint32',
    default = 1,
    range = 1,
    save2db = true,
    sync = true,
})

attribute:upsert({
    id = 8,
    enum_key = 'LOGIN_TIME',
    name = '登录时间',
    type = 'uint32',
    default = 0,
    range = 1,
    save2db = true,
    sync = false,
})

attribute:upsert({
    id = 9,
    enum_key = 'HERMES',
    name = '赫尔币',
    type = 'uint64',
    default = 0,
    range = 1,
    save2db = true,
    sync = false,
})

attribute:upsert({
    id = 10,
    enum_key = 'EXPLORE',
    name = '探索点',
    type = 'uint32',
    default = 0,
    range = 1,
    save2db = true,
    sync = true,
})

attribute:upsert({
    id = 11,
    enum_key = 'LOGOUT_TIME',
    name = '登出时间',
    type = 'uint32',
    default = 0,
    range = 1,
    save2db = true,
    sync = false,
})

attribute:upsert({
    id = 31,
    enum_key = 'VCARD_AVATAR_ID',
    name = '名片底框ID',
    type = 'uint32',
    default = 0,
    range = 1,
    save2db = true,
    sync = true,
})

attribute:upsert({
    id = 32,
    enum_key = 'VCARD_FRAME_ID',
    name = '名片外形ID',
    type = 'uint32',
    default = 0,
    range = 1,
    save2db = true,
    sync = true,
})

attribute:upsert({
    id = 33,
    enum_key = 'VCARD_BORDER_ID',
    name = '名片边框ID',
    type = 'uint32',
    default = 0,
    range = 1,
    save2db = true,
    sync = true,
})

attribute:upsert({
    id = 34,
    enum_key = 'VCARD_ACHIE_ID',
    name = '名片成就ID',
    type = 'uint32',
    default = 0,
    range = 1,
    save2db = true,
    sync = true,
})

attribute:upsert({
    id = 51,
    enum_key = 'CRYSTAL_INC_RATE',
    name = '巴布晶体加成(万分比)',
    type = 'uint32',
    default = 0,
    range = 1,
    save2db = false,
    sync = false,
})

attribute:upsert({
    id = 52,
    enum_key = 'IDEAL_INC_RATE',
    name = '理想币加成(万分比)',
    type = 'uint32',
    default = 0,
    range = 1,
    save2db = false,
    sync = false,
})

attribute:upsert({
    id = 53,
    enum_key = 'EXP_INC_RATE',
    name = '经验加成(万分比)',
    type = 'uint32',
    default = 0,
    range = 1,
    save2db = false,
    sync = false,
})

attribute:upsert({
    id = 54,
    enum_key = 'ROLE_EXP_INC_RATE',
    name = '角色熟练度加成(万分比)',
    type = 'uint32',
    default = 0,
    range = 1,
    save2db = false,
    sync = false,
})

attribute:upsert({
    id = 55,
    enum_key = 'WEAPON_EXP_INC_RATE',
    name = '武器经验加成(万分比)',
    type = 'uint32',
    default = 0,
    range = 1,
    save2db = false,
    sync = false,
})

attribute:upsert({
    id = 71,
    enum_key = 'WEEK_EXP',
    name = '周经验累计',
    type = 'uint32',
    default = 0,
    range = 1,
    save2db = true,
    sync = false,
})

attribute:upsert({
    id = 72,
    enum_key = 'WEEK_IDEAL',
    name = '周理想币累计',
    type = 'uint32',
    default = 0,
    range = 1,
    save2db = true,
    sync = false,
})

attribute:upsert({
    id = 101,
    enum_key = 'LAST_MOD_NAME',
    name = '上次改名时间',
    type = 'uint32',
    default = 0,
    range = 1,
    save2db = true,
    sync = false,
})
