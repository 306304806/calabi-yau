--divisionreward_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local divisionreward = config_mgr:get_table("divisionreward")

--导出版本号
divisionreward:set_version(10000)

--导出配置内容
divisionreward:upsert({
    id = 1,
    season = 1,
    type = 1,
    icon_division_reward = '/Game/PaperMan/UI/Atlas/DynamicResource/RoleSkin/RoleList/role_list_select_10101.role_list_select_10101',
    icon_division_reward2 = '/Game/PaperMan/UI/Atlas/DynamicResource/RoleSkin/RoleList/role_list_select_10101.role_list_select_10101',
    title = '皮肤大奖啊',
    quality = 4,
    desc = '大奖励啊，拉格朗日中值定理又称拉氏定理，是微分学中的基本定理之一，它反映了可导函数在闭区间上的整体的平均变化率与区间内某点的局部变化率的关系。拉格朗日中值定理是罗尔中值定理',
    condition = 2,
    param = 10,
    condition_desc = '黄金段位以上获胜10场',
    reward = {{item_id=20001,item_amount=1}},
})

divisionreward:upsert({
    id = 2,
    season = 1,
    type = 2,
    icon_division_reward = '/Game/PaperMan/UI/Atlas/DynamicResource/Item/ItemIcon/T_Dynamic_Item_10001.T_Dynamic_Item_10001',
    icon_division_reward2 = '/Game/PaperMan/UI/Atlas/DynamicResource/Item/ItemIcon/T_Dynamic_Item_10001.T_Dynamic_Item_10001',
    title = '青铜奖励啊',
    quality = 3,
    desc = '小奖励，拉格朗日中值定理又称拉氏定理，是微分学中的基本定理之一，它反映了可导函数在闭区间上的整体的平均变化率与区间内某点的局部变化率的关系。拉格朗日中值定理是罗尔中值定理',
    condition = 1,
    param = 0,
    condition_desc = '达到青铜段位',
    reward = {{item_id=20001,item_amount=1},{item_id=20001,item_amount=1}},
})

divisionreward:upsert({
    id = 3,
    season = 1,
    type = 2,
    icon_division_reward = '/Game/PaperMan/UI/Atlas/DynamicResource/Item/ItemIcon/T_Dynamic_Item_10001.T_Dynamic_Item_10001',
    icon_division_reward2 = '/Game/PaperMan/UI/Atlas/DynamicResource/Item/ItemIcon/T_Dynamic_Item_10001.T_Dynamic_Item_10001',
    title = '白银奖励啊',
    quality = 3,
    desc = '小奖励，拉格朗日中值定理又称拉氏定理，是微分学中的基本定理之一，它反映了可导函数在闭区间上的整体的平均变化率与区间内某点的局部变化率的关系。拉格朗日中值定理是罗尔中值定理',
    condition = 1,
    param = 0,
    condition_desc = '达到白银段位',
    reward = {{item_id=20001,item_amount=1},{item_id=20001,item_amount=1}},
})

divisionreward:upsert({
    id = 4,
    season = 1,
    type = 2,
    icon_division_reward = '/Game/PaperMan/UI/Atlas/DynamicResource/Item/ItemIcon/T_Dynamic_Item_10001.T_Dynamic_Item_10001',
    icon_division_reward2 = '/Game/PaperMan/UI/Atlas/DynamicResource/Item/ItemIcon/T_Dynamic_Item_10001.T_Dynamic_Item_10001',
    title = '黄金奖励啊',
    quality = 3,
    desc = '小奖励，拉格朗日中值定理又称拉氏定理，是微分学中的基本定理之一，它反映了可导函数在闭区间上的整体的平均变化率与区间内某点的局部变化率的关系。拉格朗日中值定理是罗尔中值定理',
    condition = 1,
    param = 0,
    condition_desc = '达到黄金段位',
    reward = {{item_id=20001,item_amount=1},{item_id=20001,item_amount=1}},
})

divisionreward:upsert({
    id = 5,
    season = 1,
    type = 2,
    icon_division_reward = '/Game/PaperMan/UI/Atlas/DynamicResource/Item/ItemIcon/T_Dynamic_Item_10001.T_Dynamic_Item_10001',
    icon_division_reward2 = '/Game/PaperMan/UI/Atlas/DynamicResource/Item/ItemIcon/T_Dynamic_Item_10001.T_Dynamic_Item_10001',
    title = '铂金奖励啊',
    quality = 3,
    desc = '小奖励，拉格朗日中值定理又称拉氏定理，是微分学中的基本定理之一，它反映了可导函数在闭区间上的整体的平均变化率与区间内某点的局部变化率的关系。拉格朗日中值定理是罗尔中值定理',
    condition = 1,
    param = 0,
    condition_desc = '达到铂金段位',
    reward = {{item_id=20001,item_amount=1},{item_id=20001,item_amount=1}},
})

divisionreward:upsert({
    id = 6,
    season = 1,
    type = 2,
    icon_division_reward = '/Game/PaperMan/UI/Atlas/DynamicResource/Item/ItemIcon/T_Dynamic_Item_10001.T_Dynamic_Item_10001',
    icon_division_reward2 = '/Game/PaperMan/UI/Atlas/DynamicResource/Item/ItemIcon/T_Dynamic_Item_10001.T_Dynamic_Item_10001',
    title = '钻石奖励啊',
    quality = 3,
    desc = '小奖励，拉格朗日中值定理又称拉氏定理，是微分学中的基本定理之一，它反映了可导函数在闭区间上的整体的平均变化率与区间内某点的局部变化率的关系。拉格朗日中值定理是罗尔中值定理',
    condition = 1,
    param = 0,
    condition_desc = '达到钻石段位',
    reward = {{item_id=20001,item_amount=1},{item_id=20001,item_amount=1}},
})

divisionreward:upsert({
    id = 7,
    season = 1,
    type = 2,
    icon_division_reward = '/Game/PaperMan/UI/Atlas/DynamicResource/Item/ItemIcon/T_Dynamic_Item_10001.T_Dynamic_Item_10001',
    icon_division_reward2 = '/Game/PaperMan/UI/Atlas/DynamicResource/Item/ItemIcon/T_Dynamic_Item_10001.T_Dynamic_Item_10001',
    title = '星耀奖励啊',
    quality = 3,
    desc = '小奖励，拉格朗日中值定理又称拉氏定理，是微分学中的基本定理之一，它反映了可导函数在闭区间上的整体的平均变化率与区间内某点的局部变化率的关系。拉格朗日中值定理是罗尔中值定理',
    condition = 1,
    param = 0,
    condition_desc = '达到星耀段位',
    reward = {{item_id=20001,item_amount=1},{item_id=20001,item_amount=1}},
})

divisionreward:upsert({
    id = 8,
    season = 1,
    type = 2,
    icon_division_reward = '/Game/PaperMan/UI/Atlas/DynamicResource/Item/ItemIcon/T_Dynamic_Item_10001.T_Dynamic_Item_10001',
    icon_division_reward2 = '/Game/PaperMan/UI/Atlas/DynamicResource/Item/ItemIcon/T_Dynamic_Item_10001.T_Dynamic_Item_10001',
    title = '王者奖励啊',
    quality = 3,
    desc = '小奖励，拉格朗日中值定理又称拉氏定理，是微分学中的基本定理之一，它反映了可导函数在闭区间上的整体的平均变化率与区间内某点的局部变化率的关系。拉格朗日中值定理是罗尔中值定理',
    condition = 1,
    param = 0,
    condition_desc = '达到王者段位',
    reward = {{item_id=20001,item_amount=1},{item_id=20001,item_amount=1}},
})

divisionreward:upsert({
    id = 9,
    season = 2,
    type = 1,
    icon_division_reward = '/Game/PaperMan/UI/Atlas/DynamicResource/RoleSkin/RoleList/role_list_select_10101.role_list_select_10101',
    icon_division_reward2 = '/Game/PaperMan/UI/Atlas/DynamicResource/RoleSkin/RoleList/role_list_select_10101.role_list_select_10101',
    title = '皮肤大奖啊',
    quality = 4,
    desc = '大奖励啊，拉格朗日中值定理又称拉氏定理，是微分学中的基本定理之一，它反映了可导函数在闭区间上的整体的平均变化率与区间内某点的局部变化率的关系。拉格朗日中值定理是罗尔中值定理',
    condition = 2,
    param = 10,
    condition_desc = '黄金段位以上获胜10场',
    reward = {{item_id=20001,item_amount=1}},
})

divisionreward:upsert({
    id = 10,
    season = 2,
    type = 2,
    icon_division_reward = '/Game/PaperMan/UI/Atlas/DynamicResource/Item/ItemIcon/T_Dynamic_Item_10001.T_Dynamic_Item_10001',
    icon_division_reward2 = '/Game/PaperMan/UI/Atlas/DynamicResource/Item/ItemIcon/T_Dynamic_Item_10001.T_Dynamic_Item_10001',
    title = '青铜奖励啊',
    quality = 3,
    desc = '小奖励，拉格朗日中值定理又称拉氏定理，是微分学中的基本定理之一，它反映了可导函数在闭区间上的整体的平均变化率与区间内某点的局部变化率的关系。拉格朗日中值定理是罗尔中值定理',
    condition = 1,
    param = 0,
    condition_desc = '达到青铜段位',
    reward = {{item_id=20001,item_amount=1},{item_id=20001,item_amount=1}},
})

divisionreward:upsert({
    id = 11,
    season = 2,
    type = 2,
    icon_division_reward = '/Game/PaperMan/UI/Atlas/DynamicResource/Item/ItemIcon/T_Dynamic_Item_10001.T_Dynamic_Item_10001',
    icon_division_reward2 = '/Game/PaperMan/UI/Atlas/DynamicResource/Item/ItemIcon/T_Dynamic_Item_10001.T_Dynamic_Item_10001',
    title = '白银奖励啊',
    quality = 3,
    desc = '小奖励，拉格朗日中值定理又称拉氏定理，是微分学中的基本定理之一，它反映了可导函数在闭区间上的整体的平均变化率与区间内某点的局部变化率的关系。拉格朗日中值定理是罗尔中值定理',
    condition = 1,
    param = 0,
    condition_desc = '达到白银段位',
    reward = {{item_id=20001,item_amount=1},{item_id=20001,item_amount=1}},
})

divisionreward:upsert({
    id = 12,
    season = 2,
    type = 2,
    icon_division_reward = '/Game/PaperMan/UI/Atlas/DynamicResource/Item/ItemIcon/T_Dynamic_Item_10001.T_Dynamic_Item_10001',
    icon_division_reward2 = '/Game/PaperMan/UI/Atlas/DynamicResource/Item/ItemIcon/T_Dynamic_Item_10001.T_Dynamic_Item_10001',
    title = '黄金奖励啊',
    quality = 3,
    desc = '小奖励，拉格朗日中值定理又称拉氏定理，是微分学中的基本定理之一，它反映了可导函数在闭区间上的整体的平均变化率与区间内某点的局部变化率的关系。拉格朗日中值定理是罗尔中值定理',
    condition = 1,
    param = 0,
    condition_desc = '达到黄金段位',
    reward = {{item_id=20001,item_amount=1},{item_id=20001,item_amount=1}},
})

divisionreward:upsert({
    id = 13,
    season = 2,
    type = 2,
    icon_division_reward = '/Game/PaperMan/UI/Atlas/DynamicResource/Item/ItemIcon/T_Dynamic_Item_10001.T_Dynamic_Item_10001',
    icon_division_reward2 = '/Game/PaperMan/UI/Atlas/DynamicResource/Item/ItemIcon/T_Dynamic_Item_10001.T_Dynamic_Item_10001',
    title = '铂金奖励啊',
    quality = 3,
    desc = '小奖励，拉格朗日中值定理又称拉氏定理，是微分学中的基本定理之一，它反映了可导函数在闭区间上的整体的平均变化率与区间内某点的局部变化率的关系。拉格朗日中值定理是罗尔中值定理',
    condition = 1,
    param = 0,
    condition_desc = '达到铂金段位',
    reward = {{item_id=20001,item_amount=1},{item_id=20001,item_amount=1}},
})

divisionreward:upsert({
    id = 14,
    season = 2,
    type = 2,
    icon_division_reward = '/Game/PaperMan/UI/Atlas/DynamicResource/Item/ItemIcon/T_Dynamic_Item_10001.T_Dynamic_Item_10001',
    icon_division_reward2 = '/Game/PaperMan/UI/Atlas/DynamicResource/Item/ItemIcon/T_Dynamic_Item_10001.T_Dynamic_Item_10001',
    title = '钻石奖励啊',
    quality = 3,
    desc = '小奖励，拉格朗日中值定理又称拉氏定理，是微分学中的基本定理之一，它反映了可导函数在闭区间上的整体的平均变化率与区间内某点的局部变化率的关系。拉格朗日中值定理是罗尔中值定理',
    condition = 1,
    param = 0,
    condition_desc = '达到钻石段位',
    reward = {{item_id=20001,item_amount=1},{item_id=20001,item_amount=1}},
})

divisionreward:upsert({
    id = 15,
    season = 2,
    type = 2,
    icon_division_reward = '/Game/PaperMan/UI/Atlas/DynamicResource/Item/ItemIcon/T_Dynamic_Item_10001.T_Dynamic_Item_10001',
    icon_division_reward2 = '/Game/PaperMan/UI/Atlas/DynamicResource/Item/ItemIcon/T_Dynamic_Item_10001.T_Dynamic_Item_10001',
    title = '星耀奖励啊',
    quality = 3,
    desc = '小奖励，拉格朗日中值定理又称拉氏定理，是微分学中的基本定理之一，它反映了可导函数在闭区间上的整体的平均变化率与区间内某点的局部变化率的关系。拉格朗日中值定理是罗尔中值定理',
    condition = 1,
    param = 0,
    condition_desc = '达到星耀段位',
    reward = {{item_id=20001,item_amount=1},{item_id=20001,item_amount=1}},
})

divisionreward:upsert({
    id = 16,
    season = 2,
    type = 2,
    icon_division_reward = '/Game/PaperMan/UI/Atlas/DynamicResource/Item/ItemIcon/T_Dynamic_Item_10001.T_Dynamic_Item_10001',
    icon_division_reward2 = '/Game/PaperMan/UI/Atlas/DynamicResource/Item/ItemIcon/T_Dynamic_Item_10001.T_Dynamic_Item_10001',
    title = '王者奖励啊',
    quality = 3,
    desc = '小奖励，拉格朗日中值定理又称拉氏定理，是微分学中的基本定理之一，它反映了可导函数在闭区间上的整体的平均变化率与区间内某点的局部变化率的关系。拉格朗日中值定理是罗尔中值定理',
    condition = 1,
    param = 0,
    condition_desc = '达到王者段位',
    reward = {{item_id=20001,item_amount=1},{item_id=20001,item_amount=1}},
})
