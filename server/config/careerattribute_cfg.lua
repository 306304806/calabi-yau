--careerattribute_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local careerattribute = config_mgr:get_table("careerattribute")

--导出版本号
careerattribute:set_version(10000)

--导出配置内容
careerattribute:upsert({
    id = 1,
    enum_key = 'LAUD',
    is_his = true,
    is_cur = false,
    image_only = false,
    name = '点赞数',
    default = 0,
})

careerattribute:upsert({
    id = 2,
    enum_key = 'FLOGIN_TIME',
    is_his = true,
    is_cur = false,
    image_only = false,
    name = '首次登录时间',
    default = 0,
})

careerattribute:upsert({
    id = 31,
    enum_key = 'BOMB_ROUND',
    is_his = true,
    is_cur = true,
    image_only = false,
    name = '排位场次',
    default = 0,
})

careerattribute:upsert({
    id = 32,
    enum_key = 'BOMB_WIN',
    is_his = true,
    is_cur = true,
    image_only = false,
    name = '爆破胜利场次',
    default = 0,
})

careerattribute:upsert({
    id = 33,
    enum_key = 'BOMB_MVP',
    is_his = true,
    is_cur = true,
    image_only = false,
    name = '爆破mvp场次',
    default = 0,
})

careerattribute:upsert({
    id = 34,
    enum_key = 'BOMB_DAMAGE',
    is_his = true,
    is_cur = false,
    image_only = false,
    name = '爆破伤害',
    default = 0,
})

careerattribute:upsert({
    id = 35,
    enum_key = 'BOMB_KILL',
    is_his = true,
    is_cur = false,
    image_only = false,
    name = '爆破击杀数',
    default = 0,
})

careerattribute:upsert({
    id = 36,
    enum_key = 'BOMB_HIT_HEAD',
    is_his = true,
    is_cur = false,
    image_only = false,
    name = '爆破爆头数',
    default = 0,
})

careerattribute:upsert({
    id = 37,
    enum_key = 'BOMB_STREAK_WIN',
    is_his = false,
    is_cur = true,
    image_only = false,
    name = '爆破连胜',
    default = 0,
})

careerattribute:upsert({
    id = 38,
    enum_key = 'BOMB_STREAK_LOSE',
    is_his = false,
    is_cur = true,
    image_only = false,
    name = '爆破连败',
    default = 0,
})

careerattribute:upsert({
    id = 39,
    enum_key = 'BOMB_BATTLE_TIME',
    is_his = false,
    is_cur = true,
    image_only = false,
    name = '上次打爆破赛时间',
    default = 0,
})

careerattribute:upsert({
    id = 40,
    enum_key = 'BOMB_HIT_COUNT',
    is_his = true,
    is_cur = false,
    image_only = false,
    name = '爆破命中次数',
    default = 0,
})

careerattribute:upsert({
    id = 61,
    enum_key = 'TEAM_ROUND',
    is_his = true,
    is_cur = true,
    image_only = false,
    name = '团竟场次',
    default = 0,
})

careerattribute:upsert({
    id = 62,
    enum_key = 'TEAM_WIN',
    is_his = true,
    is_cur = true,
    image_only = false,
    name = '团竟胜利场次',
    default = 0,
})

careerattribute:upsert({
    id = 63,
    enum_key = 'TEAM_MVP',
    is_his = true,
    is_cur = true,
    image_only = false,
    name = '团竟mvp场次',
    default = 0,
})

careerattribute:upsert({
    id = 64,
    enum_key = 'TEAM_DAMAGE',
    is_his = true,
    is_cur = false,
    image_only = false,
    name = '团竟伤害',
    default = 0,
})

careerattribute:upsert({
    id = 65,
    enum_key = 'TEAM_KILL',
    is_his = true,
    is_cur = false,
    image_only = false,
    name = '团竟击杀数',
    default = 0,
})

careerattribute:upsert({
    id = 66,
    enum_key = 'TEAM_HIT_HEAD',
    is_his = true,
    is_cur = false,
    image_only = false,
    name = '团竟爆头数',
    default = 0,
})

careerattribute:upsert({
    id = 67,
    enum_key = 'TEAM_STREAK_WIN',
    is_his = false,
    is_cur = true,
    image_only = false,
    name = '团竟连胜',
    default = 0,
})

careerattribute:upsert({
    id = 68,
    enum_key = 'TEAM_STREAK_LOSE',
    is_his = false,
    is_cur = true,
    image_only = false,
    name = '团竟连败',
    default = 0,
})

careerattribute:upsert({
    id = 69,
    enum_key = 'TEAM_BATTLE_TIME',
    is_his = false,
    is_cur = true,
    image_only = false,
    name = '上次打团竟赛时间',
    default = 0,
})

careerattribute:upsert({
    id = 70,
    enum_key = 'TEAM_HIT_COUNT',
    is_his = true,
    is_cur = false,
    image_only = false,
    name = '团竟命中次数',
    default = 0,
})

careerattribute:upsert({
    id = 91,
    enum_key = 'TOP_SEASONS',
    is_his = true,
    is_cur = false,
    image_only = false,
    name = '最高段位赛季列表',
    default = {},
})

careerattribute:upsert({
    id = 92,
    enum_key = 'TOP_SEG',
    is_his = true,
    is_cur = true,
    image_only = false,
    name = '最高段位',
    default = 0,
})

careerattribute:upsert({
    id = 93,
    enum_key = 'CUR_SEASON',
    is_his = false,
    is_cur = true,
    image_only = false,
    name = '当前赛季',
    default = 0,
})

careerattribute:upsert({
    id = 94,
    enum_key = 'CUR_SEG',
    is_his = false,
    is_cur = true,
    image_only = false,
    name = '当前段位',
    default = 0,
})

careerattribute:upsert({
    id = 95,
    enum_key = 'BRAVE_SCORE',
    is_his = false,
    is_cur = true,
    image_only = false,
    name = '勇者积分',
    default = 0,
})

careerattribute:upsert({
    id = 96,
    enum_key = 'STAR_GAME',
    is_his = false,
    is_cur = true,
    image_only = false,
    name = '星数对应场数',
    default = {},
})

careerattribute:upsert({
    id = 201,
    enum_key = 'ICON',
    is_his = true,
    is_cur = false,
    image_only = true,
    name = '玩家头像',
    default = 0,
})

careerattribute:upsert({
    id = 202,
    enum_key = 'NAME',
    is_his = true,
    is_cur = false,
    image_only = true,
    name = '玩家名字',
    default = '昵称',
})

careerattribute:upsert({
    id = 203,
    enum_key = 'SEX',
    is_his = true,
    is_cur = false,
    image_only = true,
    name = '玩家性别',
    default = 1,
})

careerattribute:upsert({
    id = 204,
    enum_key = 'LEVEL',
    is_his = true,
    is_cur = false,
    image_only = true,
    name = '玩家等级',
    default = 0,
})

careerattribute:upsert({
    id = 205,
    enum_key = 'EXP',
    is_his = true,
    is_cur = false,
    image_only = true,
    name = '玩家经验值',
    default = 0,
})

careerattribute:upsert({
    id = 206,
    enum_key = 'ID',
    is_his = true,
    is_cur = false,
    image_only = true,
    name = '玩家ID',
    default = 0,
})

careerattribute:upsert({
    id = 231,
    enum_key = 'VCARD_AVATAR_ID',
    is_his = true,
    is_cur = false,
    image_only = true,
    name = '名片外形ID',
    default = 0,
})

careerattribute:upsert({
    id = 232,
    enum_key = 'VCARD_FRAME_ID',
    is_his = true,
    is_cur = false,
    image_only = true,
    name = '名片底框ID',
    default = 0,
})

careerattribute:upsert({
    id = 233,
    enum_key = 'VCARD_BORDER_ID',
    is_his = true,
    is_cur = false,
    image_only = true,
    name = '名片边框ID',
    default = 0,
})

careerattribute:upsert({
    id = 234,
    enum_key = 'VCARD_ACHIE_ID',
    is_his = true,
    is_cur = false,
    image_only = true,
    name = '名片成就ID',
    default = 0,
})

careerattribute:upsert({
    id = 251,
    enum_key = 'ROLE_CNT',
    is_his = true,
    is_cur = false,
    image_only = true,
    name = '玩家拥有的角色数量',
    default = 0,
})

careerattribute:upsert({
    id = 252,
    enum_key = 'SKIN_CNT',
    is_his = true,
    is_cur = false,
    image_only = true,
    name = '玩家拥有的皮肤数量',
    default = 0,
})

careerattribute:upsert({
    id = 253,
    enum_key = 'WEAPON_CNT',
    is_his = true,
    is_cur = false,
    image_only = true,
    name = '玩家拥有的武器数量',
    default = 0,
})
