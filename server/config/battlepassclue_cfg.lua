--battlepassclue_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local battlepassclue = config_mgr:get_table("battlepassclue")

--导出版本号
battlepassclue:set_version(10000)

--导出配置内容
battlepassclue:upsert({
    id = 1,
    season = 1,
    clue_id = 1,
    title = 'P-type卡丘身处理公告',
    unlock_level = 1,
    prize1 = {{item_id=10001,item_amount=1}},
})

battlepassclue:upsert({
    id = 2,
    season = 1,
    clue_id = 2,
    title = 'P-type卡丘身处理公告',
    unlock_level = 10,
    prize1 = {{item_id=10001,item_amount=1}},
})

battlepassclue:upsert({
    id = 3,
    season = 1,
    clue_id = 3,
    title = 'P-type卡丘身处理公告',
    unlock_level = 20,
    prize1 = {{item_id=10001,item_amount=1}},
})

battlepassclue:upsert({
    id = 4,
    season = 1,
    clue_id = 4,
    title = 'P-type卡丘身处理公告',
    unlock_level = 30,
    prize1 = {{item_id=10001,item_amount=1}},
})

battlepassclue:upsert({
    id = 5,
    season = 1,
    clue_id = 5,
    title = 'P-type卡丘身处理公告',
    unlock_level = 40,
    prize1 = {{item_id=10001,item_amount=1}},
})

battlepassclue:upsert({
    id = 6,
    season = 2,
    clue_id = 1,
    title = 'P-type卡丘身处理公告',
    unlock_level = 1,
    prize1 = {{item_id=10001,item_amount=1}},
})

battlepassclue:upsert({
    id = 7,
    season = 2,
    clue_id = 2,
    title = 'P-type卡丘身处理公告',
    unlock_level = 10,
    prize1 = {{item_id=10001,item_amount=1}},
})

battlepassclue:upsert({
    id = 8,
    season = 2,
    clue_id = 3,
    title = 'P-type卡丘身处理公告',
    unlock_level = 20,
    prize1 = {{item_id=10001,item_amount=1}},
})

battlepassclue:upsert({
    id = 9,
    season = 2,
    clue_id = 4,
    title = 'P-type卡丘身处理公告',
    unlock_level = 30,
    prize1 = {{item_id=10001,item_amount=1}},
})

battlepassclue:upsert({
    id = 10,
    season = 2,
    clue_id = 5,
    title = 'P-type卡丘身处理公告',
    unlock_level = 40,
    prize1 = {{item_id=10001,item_amount=1}},
})

battlepassclue:upsert({
    id = 11,
    season = 3,
    clue_id = 1,
    title = 'P-type卡丘身处理公告',
    unlock_level = 1,
    prize1 = {{item_id=10001,item_amount=1}},
})

battlepassclue:upsert({
    id = 12,
    season = 3,
    clue_id = 2,
    title = 'P-type卡丘身处理公告',
    unlock_level = 10,
    prize1 = {{item_id=10001,item_amount=1}},
})

battlepassclue:upsert({
    id = 13,
    season = 3,
    clue_id = 3,
    title = 'P-type卡丘身处理公告',
    unlock_level = 20,
    prize1 = {{item_id=10001,item_amount=1}},
})

battlepassclue:upsert({
    id = 14,
    season = 3,
    clue_id = 4,
    title = 'P-type卡丘身处理公告',
    unlock_level = 30,
    prize1 = {{item_id=10001,item_amount=1}},
})

battlepassclue:upsert({
    id = 15,
    season = 3,
    clue_id = 5,
    title = 'P-type卡丘身处理公告',
    unlock_level = 40,
    prize1 = {{item_id=10001,item_amount=1}},
})
