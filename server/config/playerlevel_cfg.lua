--playerlevel_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local playerlevel = config_mgr:get_table("playerlevel")

--导出版本号
playerlevel:set_version(10000)

--导出配置内容
playerlevel:upsert({
    lv = 1,
    exp = 100,
})

playerlevel:upsert({
    lv = 2,
    exp = 150,
})

playerlevel:upsert({
    lv = 3,
    exp = 200,
})

playerlevel:upsert({
    lv = 4,
    exp = 250,
})

playerlevel:upsert({
    lv = 5,
    exp = 300,
})

playerlevel:upsert({
    lv = 6,
    exp = 350,
})

playerlevel:upsert({
    lv = 7,
    exp = 400,
})

playerlevel:upsert({
    lv = 8,
    exp = 450,
})

playerlevel:upsert({
    lv = 9,
    exp = 500,
})

playerlevel:upsert({
    lv = 10,
    exp = 550,
})

playerlevel:upsert({
    lv = 11,
    exp = 600,
})

playerlevel:upsert({
    lv = 12,
    exp = 650,
})

playerlevel:upsert({
    lv = 13,
    exp = 700,
})

playerlevel:upsert({
    lv = 14,
    exp = 750,
})

playerlevel:upsert({
    lv = 15,
    exp = 800,
})

playerlevel:upsert({
    lv = 16,
    exp = 850,
})

playerlevel:upsert({
    lv = 17,
    exp = 900,
})

playerlevel:upsert({
    lv = 18,
    exp = 950,
})

playerlevel:upsert({
    lv = 19,
    exp = 1000,
})

playerlevel:upsert({
    lv = 20,
    exp = 1050,
})

playerlevel:upsert({
    lv = 21,
    exp = 1100,
})

playerlevel:upsert({
    lv = 22,
    exp = 1150,
})

playerlevel:upsert({
    lv = 23,
    exp = 1200,
})

playerlevel:upsert({
    lv = 24,
    exp = 1250,
})

playerlevel:upsert({
    lv = 25,
    exp = 1300,
})

playerlevel:upsert({
    lv = 26,
    exp = 1350,
})

playerlevel:upsert({
    lv = 27,
    exp = 1400,
})

playerlevel:upsert({
    lv = 28,
    exp = 1450,
})

playerlevel:upsert({
    lv = 29,
    exp = 1500,
})

playerlevel:upsert({
    lv = 30,
    exp = 1550,
})

playerlevel:upsert({
    lv = 31,
    exp = 1600,
})

playerlevel:upsert({
    lv = 32,
    exp = 1650,
})

playerlevel:upsert({
    lv = 33,
    exp = 1700,
})

playerlevel:upsert({
    lv = 34,
    exp = 1750,
})

playerlevel:upsert({
    lv = 35,
    exp = 1800,
})

playerlevel:upsert({
    lv = 36,
    exp = 1850,
})

playerlevel:upsert({
    lv = 37,
    exp = 1900,
})

playerlevel:upsert({
    lv = 38,
    exp = 1950,
})

playerlevel:upsert({
    lv = 39,
    exp = 2000,
})

playerlevel:upsert({
    lv = 40,
    exp = 2050,
})

playerlevel:upsert({
    lv = 41,
    exp = 2100,
})

playerlevel:upsert({
    lv = 42,
    exp = 2150,
})

playerlevel:upsert({
    lv = 43,
    exp = 2200,
})

playerlevel:upsert({
    lv = 44,
    exp = 2250,
})

playerlevel:upsert({
    lv = 45,
    exp = 2300,
})

playerlevel:upsert({
    lv = 46,
    exp = 2350,
})

playerlevel:upsert({
    lv = 47,
    exp = 2400,
})

playerlevel:upsert({
    lv = 48,
    exp = 2450,
})

playerlevel:upsert({
    lv = 49,
    exp = 2500,
})

playerlevel:upsert({
    lv = 50,
    exp = 2550,
})

playerlevel:upsert({
    lv = 51,
    exp = 2600,
})

playerlevel:upsert({
    lv = 52,
    exp = 2650,
})

playerlevel:upsert({
    lv = 53,
    exp = 2700,
})

playerlevel:upsert({
    lv = 54,
    exp = 2750,
})

playerlevel:upsert({
    lv = 55,
    exp = 2800,
})

playerlevel:upsert({
    lv = 56,
    exp = 2850,
})

playerlevel:upsert({
    lv = 57,
    exp = 2900,
})

playerlevel:upsert({
    lv = 58,
    exp = 2950,
})

playerlevel:upsert({
    lv = 59,
    exp = 3000,
})

playerlevel:upsert({
    lv = 60,
    exp = 3050,
})

playerlevel:upsert({
    lv = 61,
    exp = 3100,
})

playerlevel:upsert({
    lv = 62,
    exp = 3150,
})

playerlevel:upsert({
    lv = 63,
    exp = 3200,
})

playerlevel:upsert({
    lv = 64,
    exp = 3250,
})

playerlevel:upsert({
    lv = 65,
    exp = 3300,
})

playerlevel:upsert({
    lv = 66,
    exp = 3350,
})

playerlevel:upsert({
    lv = 67,
    exp = 3400,
})

playerlevel:upsert({
    lv = 68,
    exp = 3450,
})

playerlevel:upsert({
    lv = 69,
    exp = 3500,
})

playerlevel:upsert({
    lv = 70,
    exp = 3550,
})

playerlevel:upsert({
    lv = 71,
    exp = 3600,
})

playerlevel:upsert({
    lv = 72,
    exp = 3650,
})

playerlevel:upsert({
    lv = 73,
    exp = 3700,
})

playerlevel:upsert({
    lv = 74,
    exp = 3750,
})

playerlevel:upsert({
    lv = 75,
    exp = 3800,
})

playerlevel:upsert({
    lv = 76,
    exp = 3850,
})

playerlevel:upsert({
    lv = 77,
    exp = 3900,
})

playerlevel:upsert({
    lv = 78,
    exp = 3950,
})

playerlevel:upsert({
    lv = 79,
    exp = 4000,
})

playerlevel:upsert({
    lv = 80,
    exp = 4050,
})

playerlevel:upsert({
    lv = 81,
    exp = 4100,
})

playerlevel:upsert({
    lv = 82,
    exp = 4150,
})

playerlevel:upsert({
    lv = 83,
    exp = 4200,
})

playerlevel:upsert({
    lv = 84,
    exp = 4250,
})

playerlevel:upsert({
    lv = 85,
    exp = 4300,
})

playerlevel:upsert({
    lv = 86,
    exp = 4350,
})

playerlevel:upsert({
    lv = 87,
    exp = 4400,
})

playerlevel:upsert({
    lv = 88,
    exp = 4450,
})

playerlevel:upsert({
    lv = 89,
    exp = 4500,
})

playerlevel:upsert({
    lv = 90,
    exp = 4550,
})

playerlevel:upsert({
    lv = 91,
    exp = 4600,
})

playerlevel:upsert({
    lv = 92,
    exp = 4650,
})

playerlevel:upsert({
    lv = 93,
    exp = 4700,
})

playerlevel:upsert({
    lv = 94,
    exp = 4750,
})

playerlevel:upsert({
    lv = 95,
    exp = 4800,
})

playerlevel:upsert({
    lv = 96,
    exp = 4850,
})

playerlevel:upsert({
    lv = 97,
    exp = 4900,
})

playerlevel:upsert({
    lv = 98,
    exp = 4950,
})

playerlevel:upsert({
    lv = 99,
    exp = 5000,
})
