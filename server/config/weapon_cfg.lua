--weapon_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local weapon = config_mgr:get_table("weapon")

--导出版本号
weapon:set_version(10000)

--导出配置内容
weapon:upsert({
    id = 10000001,
    type = 100,
    limit = 0,
    param = {0},
    component = {},
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 10000002,
    type = 102,
    limit = 0,
    param = {0},
    component = {},
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 10001073,
    type = 55,
    quality = 1,
    limit = 0,
    param = {0},
    component = {},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 10001321,
    type = 56,
    quality = 1,
    limit = 0,
    param = {0},
    component = {},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 10101001,
    type = 1,
    quality = 1,
    limit = 1,
    param = {101,132,108,110,111,114,106,116},
    component = {1,2,3,4,5},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 10101002,
    type = 1,
    quality = 1,
    limit = 1,
    param = {101},
    component = {1,2,3,4,5},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 10101003,
    type = 1,
    quality = 1,
    limit = 1,
    param = {101},
    component = {1,2,3,4,5},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 10102001,
    type = 1,
    quality = 1,
    limit = 1,
    param = {132},
    component = {1,2,3,4},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 10103001,
    type = 1,
    quality = 1,
    limit = 1,
    param = {108},
    component = {1,2,3,4,5},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 10103002,
    type = 1,
    quality = 2,
    limit = 1,
    param = {101},
    component = {1,2,3,4,5},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 10103003,
    type = 1,
    quality = 3,
    limit = 1,
    param = {101},
    component = {1,2,3,4,5},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 10104001,
    type = 1,
    quality = 1,
    limit = 1,
    param = {128},
    component = {1,2,3,4,5},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 10301001,
    type = 3,
    quality = 1,
    limit = 1,
    param = {107},
    component = {1,2,3,4,5},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 10403001,
    type = 4,
    quality = 1,
    limit = 1,
    param = {105,107,128,112,113},
    component = {1,2,3,4,5},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 10501001,
    type = 5,
    quality = 1,
    limit = 1,
    param = {131},
    component = {1,2,3,4,5},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 10601001,
    type = 6,
    quality = 1,
    limit = 1,
    param = {108},
    component = {1,2,3,4,5},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 12201001,
    type = 22,
    quality = 1,
    limit = 1,
    param = {132},
    component = {},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 12301001,
    type = 23,
    quality = 1,
    limit = 1,
    param = {101,132,108,110,111,114,106,116},
    component = {},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 12301002,
    type = 23,
    quality = 2,
    limit = 1,
    param = {101},
    component = {},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 12301003,
    type = 23,
    quality = 3,
    limit = 1,
    param = {101},
    component = {},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 12302001,
    type = 23,
    quality = 1,
    limit = 1,
    param = {108},
    component = {},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 12303001,
    type = 23,
    quality = 1,
    limit = 1,
    param = {105},
    component = {},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 12501001,
    type = 25,
    quality = 1,
    limit = 1,
    param = {105,107,128,112,113},
    component = {},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 12701001,
    type = 27,
    quality = 1,
    limit = 1,
    param = {107},
    component = {},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 12702001,
    type = 101,
    quality = 1,
    limit = 1,
    param = {107},
    component = {},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 15101001,
    type = 51,
    quality = 1,
    limit = 1,
    param = {101,105,132,107,128,108},
    component = {},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 15101002,
    type = 51,
    quality = 1,
    limit = 1,
    param = {101,105,132,107,128,108},
    component = {},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 15101003,
    type = 51,
    quality = 1,
    limit = 1,
    param = {101,105,132,107,128,108},
    component = {},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 15201001,
    type = 52,
    quality = 1,
    limit = 1,
    param = {108},
    component = {},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 15401001,
    type = 54,
    quality = 1,
    limit = 1,
    param = {108},
    component = {},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 18101001,
    type = 81,
    quality = 1,
    limit = 0,
    param = {0},
    component = {},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 18201001,
    type = 82,
    quality = 1,
    limit = 0,
    param = {0},
    component = {},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 12300001,
    type = 53,
    quality = 1,
    limit = 0,
    param = {0},
    component = {},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})

weapon:upsert({
    id = 18301001,
    type = 83,
    quality = 1,
    limit = 0,
    param = {0},
    component = {},
    limit_num = 1,
    limit_type = 1,
    limit_param = 10086,
    gain_type = {0},
    gain_param1 = {},
})
