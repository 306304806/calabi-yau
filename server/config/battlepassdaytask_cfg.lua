--battlepassdaytask_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local battlepassdaytask = config_mgr:get_table("battlepassdaytask")

--导出版本号
battlepassdaytask:set_version(10000)

--导出配置内容
battlepassdaytask:upsert({
    id = 1000,
    circle = false,
    type = 1,
    difficult = 1,
    conditions = {{behavior_id=1,value=1,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=10}},
})

battlepassdaytask:upsert({
    id = 1001,
    circle = false,
    type = 1,
    difficult = 2,
    conditions = {{behavior_id=2,value=1,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=10}},
})

battlepassdaytask:upsert({
    id = 1002,
    circle = false,
    type = 1,
    difficult = 1,
    conditions = {{behavior_id=26,value=10,op_type=4,cmp_type=1}},
    prize = {{item_id=4,item_amount=10}},
})

battlepassdaytask:upsert({
    id = 1003,
    circle = false,
    type = 1,
    difficult = 2,
    conditions = {{behavior_id=27,value=10,op_type=4,cmp_type=1}},
    prize = {{item_id=4,item_amount=10}},
})

battlepassdaytask:upsert({
    id = 1004,
    circle = false,
    type = 1,
    difficult = 1,
    conditions = {{behavior_id=26,value=10,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=10}},
})

battlepassdaytask:upsert({
    id = 1005,
    circle = false,
    type = 1,
    difficult = 2,
    conditions = {{behavior_id=22,value=2000,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=10}},
})

battlepassdaytask:upsert({
    id = 1006,
    circle = false,
    type = 1,
    difficult = 1,
    conditions = {{behavior_id=27,value=10,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=10}},
})

battlepassdaytask:upsert({
    id = 1007,
    circle = false,
    type = 1,
    difficult = 1,
    conditions = {{behavior_id=5,value=1,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=10}},
})

battlepassdaytask:upsert({
    id = 1008,
    circle = false,
    type = 1,
    difficult = 2,
    conditions = {{behavior_id=6,value=1,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=10}},
})

battlepassdaytask:upsert({
    id = 1009,
    circle = false,
    type = 1,
    difficult = 1,
    conditions = {{behavior_id=7,value=1,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=10}},
})

battlepassdaytask:upsert({
    id = 1010,
    circle = false,
    type = 1,
    difficult = 2,
    conditions = {{behavior_id=8,value=1,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=10}},
})
