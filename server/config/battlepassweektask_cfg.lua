--battlepassweektask_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local battlepassweektask = config_mgr:get_table("battlepassweektask")

--导出版本号
battlepassweektask:set_version(10000)

--导出配置内容
battlepassweektask:upsert({
    id = 10001,
    circle = false,
    week = 1,
    desc = '完成15场比赛',
    type = 2,
    conditions = {{behavior_id=1,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10002,
    circle = false,
    week = 1,
    desc = '获得15场比赛胜利',
    type = 2,
    conditions = {{behavior_id=2,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10003,
    circle = false,
    week = 1,
    desc = '在一场对局中击杀10名对手',
    type = 2,
    conditions = {{behavior_id=26,value=10,op_type=4,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10004,
    circle = false,
    week = 1,
    desc = '在一场对局中爆头10次',
    type = 2,
    conditions = {{behavior_id=27,value=10,op_type=4,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10005,
    circle = false,
    week = 1,
    desc = '累计击杀50名对手',
    type = 2,
    conditions = {{behavior_id=26,value=50,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10006,
    circle = false,
    week = 1,
    desc = '累计造成10000点伤害',
    type = 2,
    conditions = {{behavior_id=22,value=10000,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10007,
    circle = false,
    week = 1,
    desc = '累计爆头50次',
    type = 2,
    conditions = {{behavior_id=27,value=50,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10008,
    circle = false,
    week = 1,
    desc = '完成15场团竞模式',
    type = 2,
    conditions = {{behavior_id=5,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10009,
    circle = false,
    week = 1,
    desc = '获得15场团竞模式胜利',
    type = 2,
    conditions = {{behavior_id=6,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10010,
    circle = false,
    week = 1,
    desc = '完成15场爆破模式',
    type = 2,
    conditions = {{behavior_id=7,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10011,
    circle = false,
    week = 1,
    desc = '获得15场爆破模式胜利',
    type = 2,
    conditions = {{behavior_id=8,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10012,
    circle = false,
    week = 2,
    desc = '完成15场比赛',
    type = 2,
    conditions = {{behavior_id=1,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10013,
    circle = false,
    week = 2,
    desc = '获得15场比赛胜利',
    type = 2,
    conditions = {{behavior_id=2,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10014,
    circle = false,
    week = 2,
    desc = '在一场对局中击杀10名对手',
    type = 2,
    conditions = {{behavior_id=26,value=10,op_type=4,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10015,
    circle = false,
    week = 2,
    desc = '在一场对局中爆头10次',
    type = 2,
    conditions = {{behavior_id=27,value=10,op_type=4,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10016,
    circle = false,
    week = 2,
    desc = '累计击杀50名对手',
    type = 2,
    conditions = {{behavior_id=26,value=50,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10017,
    circle = false,
    week = 2,
    desc = '累计造成10000点伤害',
    type = 2,
    conditions = {{behavior_id=22,value=10000,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10018,
    circle = false,
    week = 2,
    desc = '累计爆头50次',
    type = 2,
    conditions = {{behavior_id=27,value=50,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10019,
    circle = false,
    week = 2,
    desc = '完成15场团竞模式',
    type = 2,
    conditions = {{behavior_id=5,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10020,
    circle = false,
    week = 2,
    desc = '获得15场团竞模式胜利',
    type = 2,
    conditions = {{behavior_id=6,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10021,
    circle = false,
    week = 2,
    desc = '完成15场爆破模式',
    type = 2,
    conditions = {{behavior_id=7,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10022,
    circle = false,
    week = 2,
    desc = '获得15场爆破模式胜利',
    type = 2,
    conditions = {{behavior_id=8,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10023,
    circle = false,
    week = 3,
    desc = '完成15场比赛',
    type = 2,
    conditions = {{behavior_id=1,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10024,
    circle = false,
    week = 3,
    desc = '获得15场比赛胜利',
    type = 2,
    conditions = {{behavior_id=2,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10025,
    circle = false,
    week = 3,
    desc = '在一场对局中击杀10名对手',
    type = 2,
    conditions = {{behavior_id=26,value=10,op_type=4,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10026,
    circle = false,
    week = 3,
    desc = '在一场对局中爆头10次',
    type = 2,
    conditions = {{behavior_id=27,value=10,op_type=4,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10027,
    circle = false,
    week = 3,
    desc = '累计击杀50名对手',
    type = 2,
    conditions = {{behavior_id=26,value=50,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10028,
    circle = false,
    week = 3,
    desc = '累计造成10000点伤害',
    type = 2,
    conditions = {{behavior_id=22,value=10000,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10029,
    circle = false,
    week = 3,
    desc = '累计爆头50次',
    type = 2,
    conditions = {{behavior_id=27,value=50,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10030,
    circle = false,
    week = 3,
    desc = '完成15场团竞模式',
    type = 2,
    conditions = {{behavior_id=5,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10031,
    circle = false,
    week = 3,
    desc = '获得15场团竞模式胜利',
    type = 2,
    conditions = {{behavior_id=6,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10032,
    circle = false,
    week = 3,
    desc = '完成15场爆破模式',
    type = 2,
    conditions = {{behavior_id=7,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10033,
    circle = false,
    week = 3,
    desc = '获得15场爆破模式胜利',
    type = 2,
    conditions = {{behavior_id=8,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10034,
    circle = false,
    week = 4,
    desc = '完成15场比赛',
    type = 2,
    conditions = {{behavior_id=1,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10035,
    circle = false,
    week = 4,
    desc = '获得15场比赛胜利',
    type = 2,
    conditions = {{behavior_id=2,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10036,
    circle = false,
    week = 4,
    desc = '在一场对局中击杀10名对手',
    type = 2,
    conditions = {{behavior_id=26,value=10,op_type=4,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10037,
    circle = false,
    week = 4,
    desc = '在一场对局中爆头10次',
    type = 2,
    conditions = {{behavior_id=27,value=10,op_type=4,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10038,
    circle = false,
    week = 4,
    desc = '累计击杀50名对手',
    type = 2,
    conditions = {{behavior_id=26,value=50,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10039,
    circle = false,
    week = 4,
    desc = '累计造成10000点伤害',
    type = 2,
    conditions = {{behavior_id=22,value=10000,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10040,
    circle = false,
    week = 4,
    desc = '累计爆头50次',
    type = 2,
    conditions = {{behavior_id=27,value=50,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10041,
    circle = false,
    week = 4,
    desc = '完成15场团竞模式',
    type = 2,
    conditions = {{behavior_id=5,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10042,
    circle = false,
    week = 4,
    desc = '获得15场团竞模式胜利',
    type = 2,
    conditions = {{behavior_id=6,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10043,
    circle = false,
    week = 4,
    desc = '完成15场爆破模式',
    type = 2,
    conditions = {{behavior_id=7,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10044,
    circle = false,
    week = 4,
    desc = '获得15场爆破模式胜利',
    type = 2,
    conditions = {{behavior_id=8,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10045,
    circle = false,
    week = 5,
    desc = '完成15场比赛',
    type = 2,
    conditions = {{behavior_id=1,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10046,
    circle = false,
    week = 5,
    desc = '获得15场比赛胜利',
    type = 2,
    conditions = {{behavior_id=2,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10047,
    circle = false,
    week = 5,
    desc = '在一场对局中击杀10名对手',
    type = 2,
    conditions = {{behavior_id=26,value=10,op_type=4,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10048,
    circle = false,
    week = 5,
    desc = '在一场对局中爆头10次',
    type = 2,
    conditions = {{behavior_id=27,value=10,op_type=4,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10049,
    circle = false,
    week = 5,
    desc = '累计击杀50名对手',
    type = 2,
    conditions = {{behavior_id=26,value=50,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10050,
    circle = false,
    week = 5,
    desc = '累计造成10000点伤害',
    type = 2,
    conditions = {{behavior_id=22,value=10000,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10051,
    circle = false,
    week = 5,
    desc = '累计爆头50次',
    type = 2,
    conditions = {{behavior_id=27,value=50,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10052,
    circle = false,
    week = 5,
    desc = '完成15场团竞模式',
    type = 2,
    conditions = {{behavior_id=5,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10053,
    circle = false,
    week = 5,
    desc = '获得15场团竞模式胜利',
    type = 2,
    conditions = {{behavior_id=6,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10054,
    circle = false,
    week = 5,
    desc = '完成15场爆破模式',
    type = 2,
    conditions = {{behavior_id=7,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10055,
    circle = false,
    week = 5,
    desc = '获得15场爆破模式胜利',
    type = 2,
    conditions = {{behavior_id=8,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10056,
    circle = false,
    week = 6,
    desc = '完成15场比赛',
    type = 2,
    conditions = {{behavior_id=1,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10057,
    circle = false,
    week = 6,
    desc = '获得15场比赛胜利',
    type = 2,
    conditions = {{behavior_id=2,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10058,
    circle = false,
    week = 6,
    desc = '在一场对局中击杀10名对手',
    type = 2,
    conditions = {{behavior_id=26,value=10,op_type=4,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10059,
    circle = false,
    week = 6,
    desc = '在一场对局中爆头10次',
    type = 2,
    conditions = {{behavior_id=27,value=10,op_type=4,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10060,
    circle = false,
    week = 6,
    desc = '累计击杀50名对手',
    type = 2,
    conditions = {{behavior_id=26,value=50,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10061,
    circle = false,
    week = 6,
    desc = '累计造成10000点伤害',
    type = 2,
    conditions = {{behavior_id=22,value=10000,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10062,
    circle = false,
    week = 6,
    desc = '累计爆头50次',
    type = 2,
    conditions = {{behavior_id=27,value=50,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10063,
    circle = false,
    week = 6,
    desc = '完成15场团竞模式',
    type = 2,
    conditions = {{behavior_id=5,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10064,
    circle = false,
    week = 6,
    desc = '获得15场团竞模式胜利',
    type = 2,
    conditions = {{behavior_id=6,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10065,
    circle = false,
    week = 6,
    desc = '完成15场爆破模式',
    type = 2,
    conditions = {{behavior_id=7,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10066,
    circle = false,
    week = 6,
    desc = '获得15场爆破模式胜利',
    type = 2,
    conditions = {{behavior_id=8,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10067,
    circle = false,
    week = 7,
    desc = '完成15场比赛',
    type = 2,
    conditions = {{behavior_id=1,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10068,
    circle = false,
    week = 7,
    desc = '获得15场比赛胜利',
    type = 2,
    conditions = {{behavior_id=2,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10069,
    circle = false,
    week = 7,
    desc = '在一场对局中击杀10名对手',
    type = 2,
    conditions = {{behavior_id=26,value=10,op_type=4,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10070,
    circle = false,
    week = 7,
    desc = '在一场对局中爆头10次',
    type = 2,
    conditions = {{behavior_id=27,value=10,op_type=4,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10071,
    circle = false,
    week = 7,
    desc = '累计击杀50名对手',
    type = 2,
    conditions = {{behavior_id=26,value=50,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10072,
    circle = false,
    week = 7,
    desc = '累计造成10000点伤害',
    type = 2,
    conditions = {{behavior_id=22,value=10000,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10073,
    circle = false,
    week = 7,
    desc = '累计爆头50次',
    type = 2,
    conditions = {{behavior_id=27,value=50,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10074,
    circle = false,
    week = 7,
    desc = '完成15场团竞模式',
    type = 2,
    conditions = {{behavior_id=5,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10075,
    circle = false,
    week = 7,
    desc = '获得15场团竞模式胜利',
    type = 2,
    conditions = {{behavior_id=6,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10076,
    circle = false,
    week = 7,
    desc = '完成15场爆破模式',
    type = 2,
    conditions = {{behavior_id=7,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10077,
    circle = false,
    week = 7,
    desc = '获得15场爆破模式胜利',
    type = 2,
    conditions = {{behavior_id=8,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10078,
    circle = false,
    week = 8,
    desc = '完成15场比赛',
    type = 2,
    conditions = {{behavior_id=1,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10079,
    circle = false,
    week = 8,
    desc = '获得15场比赛胜利',
    type = 2,
    conditions = {{behavior_id=2,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10080,
    circle = false,
    week = 8,
    desc = '在一场对局中击杀10名对手',
    type = 2,
    conditions = {{behavior_id=26,value=10,op_type=4,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10081,
    circle = false,
    week = 8,
    desc = '在一场对局中爆头10次',
    type = 2,
    conditions = {{behavior_id=27,value=10,op_type=4,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10082,
    circle = false,
    week = 8,
    desc = '累计击杀50名对手',
    type = 2,
    conditions = {{behavior_id=26,value=50,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10083,
    circle = false,
    week = 8,
    desc = '累计造成10000点伤害',
    type = 2,
    conditions = {{behavior_id=22,value=10000,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10084,
    circle = false,
    week = 8,
    desc = '累计爆头50次',
    type = 2,
    conditions = {{behavior_id=27,value=50,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10085,
    circle = false,
    week = 8,
    desc = '完成15场团竞模式',
    type = 2,
    conditions = {{behavior_id=5,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10086,
    circle = false,
    week = 8,
    desc = '获得15场团竞模式胜利',
    type = 2,
    conditions = {{behavior_id=6,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10087,
    circle = false,
    week = 8,
    desc = '完成15场爆破模式',
    type = 2,
    conditions = {{behavior_id=7,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})

battlepassweektask:upsert({
    id = 10088,
    circle = false,
    week = 8,
    desc = '获得15场爆破模式胜利',
    type = 2,
    conditions = {{behavior_id=8,value=15,op_type=1,cmp_type=1}},
    prize = {{item_id=4,item_amount=100}},
})
