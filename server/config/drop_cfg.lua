--drop_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local drop = config_mgr:get_table("drop")

--导出版本号
drop:set_version(10000)

--导出配置内容
drop:upsert({
    drop_id = 1,
    group_id = 1,
    items = {{item_id=10001,item_amount=1},{item_id=20001,item_amount=2}},
    weight = 100,
})

drop:upsert({
    drop_id = 2,
    group_id = 1,
    items = {{item_id=10001,item_amount=1}},
    weight = 100,
})

drop:upsert({
    drop_id = 3,
    group_id = 1,
    items = {{item_id=10101,item_amount=1}},
    weight = 100,
})

drop:upsert({
    drop_id = 4,
    group_id = 1,
    items = {{item_id=10102,item_amount=1}},
    weight = 100,
})

drop:upsert({
    drop_id = 5,
    group_id = 2,
    items = {{item_id=10001,item_amount=1}},
    weight = 60,
})

drop:upsert({
    drop_id = 6,
    group_id = 2,
    items = {{item_id=10001,item_amount=1}},
    weight = 30,
})

drop:upsert({
    drop_id = 7,
    group_id = 2,
    items = {{item_id=10001,item_amount=1}},
    weight = 1,
})
