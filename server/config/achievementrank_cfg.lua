--achievementrank_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local achievementrank = config_mgr:get_table("achievementrank")

--导出版本号
achievementrank:set_version(10000)

--导出配置内容
achievementrank:upsert({
    type = 1,
    seg = 1,
    min_val = 1,
    max_val = 10,
})

achievementrank:upsert({
    type = 1,
    seg = 2,
    min_val = 11,
    max_val = 100,
})

achievementrank:upsert({
    type = 1,
    seg = 3,
    min_val = 101,
    max_val = 1000,
})

achievementrank:upsert({
    type = 1,
    seg = 4,
    min_val = 1001,
    max_val = 10000,
})

achievementrank:upsert({
    type = 1,
    seg = 5,
    min_val = 10001,
    max_val = 10000000,
})

achievementrank:upsert({
    type = 2,
    seg = 1,
    min_val = 1,
    max_val = 10,
})

achievementrank:upsert({
    type = 2,
    seg = 2,
    min_val = 11,
    max_val = 100,
})

achievementrank:upsert({
    type = 2,
    seg = 3,
    min_val = 101,
    max_val = 1000,
})

achievementrank:upsert({
    type = 2,
    seg = 4,
    min_val = 1001,
    max_val = 10000,
})

achievementrank:upsert({
    type = 2,
    seg = 5,
    min_val = 10001,
    max_val = 10000000,
})
