--rolevoice_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local rolevoice = config_mgr:get_table("rolevoice")

--导出版本号
rolevoice:set_version(10000)

--导出配置内容
rolevoice:upsert({
    role_voice_id = 22101001,
    role_id = 101,
    quality = 1,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22101002,
    role_id = 101,
    quality = 1,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22101003,
    role_id = 101,
    quality = 2,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22101004,
    role_id = 101,
    quality = 2,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22101005,
    role_id = 101,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22101006,
    role_id = 101,
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22101007,
    role_id = 101,
    quality = 5,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22132001,
    role_id = 132,
    quality = 1,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22132002,
    role_id = 132,
    quality = 1,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22132003,
    role_id = 132,
    quality = 2,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22132004,
    role_id = 132,
    quality = 2,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22132005,
    role_id = 132,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22132006,
    role_id = 132,
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22132007,
    role_id = 132,
    quality = 5,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22107001,
    role_id = 107,
    quality = 1,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22107002,
    role_id = 107,
    quality = 1,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22107003,
    role_id = 107,
    quality = 2,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22107004,
    role_id = 107,
    quality = 2,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22107005,
    role_id = 107,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22107006,
    role_id = 107,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22107007,
    role_id = 107,
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22107008,
    role_id = 107,
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22107009,
    role_id = 107,
    quality = 5,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22105001,
    role_id = 105,
    quality = 1,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22105002,
    role_id = 105,
    quality = 1,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22105003,
    role_id = 105,
    quality = 2,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22105004,
    role_id = 105,
    quality = 2,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22105005,
    role_id = 105,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22105006,
    role_id = 105,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22105007,
    role_id = 105,
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22105008,
    role_id = 105,
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22105009,
    role_id = 105,
    quality = 5,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22128001,
    role_id = 128,
    quality = 1,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22128002,
    role_id = 128,
    quality = 1,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22128003,
    role_id = 128,
    quality = 2,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22128004,
    role_id = 128,
    quality = 2,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22128005,
    role_id = 128,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22128006,
    role_id = 128,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22128007,
    role_id = 128,
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22128008,
    role_id = 128,
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22128009,
    role_id = 128,
    quality = 5,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22108001,
    role_id = 108,
    quality = 1,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22108002,
    role_id = 108,
    quality = 1,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22108003,
    role_id = 108,
    quality = 2,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22108004,
    role_id = 108,
    quality = 2,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22108005,
    role_id = 108,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22108006,
    role_id = 108,
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22108007,
    role_id = 108,
    quality = 5,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22110001,
    role_id = 110,
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22111001,
    role_id = 111,
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22112001,
    role_id = 112,
    quality = 5,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22113001,
    role_id = 113,
    quality = 1,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22114001,
    role_id = 114,
    quality = 1,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22106001,
    role_id = 106,
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22116001,
    role_id = 116,
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22117001,
    role_id = 117,
    quality = 5,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22118001,
    role_id = 118,
    quality = 1,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22120001,
    role_id = 120,
    quality = 1,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22121001,
    role_id = 121,
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22122001,
    role_id = 122,
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22123001,
    role_id = 123,
    quality = 5,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22124001,
    role_id = 124,
    quality = 1,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22125001,
    role_id = 125,
    quality = 1,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22126001,
    role_id = 126,
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22127001,
    role_id = 127,
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22129001,
    role_id = 129,
    quality = 5,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22130001,
    role_id = 130,
    quality = 1,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22131001,
    role_id = 131,
    quality = 1,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22133001,
    role_id = 133,
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22134001,
    role_id = 134,
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22135001,
    role_id = 135,
    quality = 5,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22136001,
    role_id = 136,
    quality = 1,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22137001,
    role_id = 137,
    quality = 1,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

rolevoice:upsert({
    role_voice_id = 22138001,
    role_id = 138,
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})
