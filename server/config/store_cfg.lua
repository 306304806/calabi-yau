--store_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local store = config_mgr:get_table("store")

--导出版本号
store:set_version(10000)

--导出配置内容
store:upsert({
    id = 1,
    store_type = 1,
    recommendation = 1,
    itemid = 10101010,
    limit_type = 4,
    limits = 1,
    origin_price = {{item_id=1,item_amount=588}},
    now_price = {{item_id=1,item_amount=388}},
    present_available = true,
    present_price = {item_id=1,item_amount=588},
    time_limit = true,
    up_time = 1584482400,
    down_time = 1605650400,
})

store:upsert({
    id = 2,
    store_type = 1,
    recommendation = 2,
    itemid = 10140010,
    limit_type = 4,
    limits = 1,
    origin_price = {{item_id=1,item_amount=588}},
    now_price = {{item_id=1,item_amount=588}},
    present_available = true,
    present_price = {item_id=1,item_amount=588},
    time_limit = true,
    up_time = 1584482400,
    down_time = 1608242400,
})

store:upsert({
    id = 3,
    store_type = 1,
    recommendation = 3,
    itemid = 103010010,
    limit_type = 0,
    origin_price = {{item_id=1,item_amount=588}},
    now_price = {{item_id=1,item_amount=288}},
    present_available = true,
    present_price = {item_id=1,item_amount=588},
    up_time = 1584482400,
    down_time = 1592690400,
})

store:upsert({
    id = 4,
    store_type = 1,
    recommendation = 3,
    itemid = 103030010,
    limit_type = 0,
    origin_price = {{item_id=1,item_amount=588}},
    now_price = {{item_id=1,item_amount=588}},
    present_available = true,
    present_price = {item_id=1,item_amount=588},
    up_time = 1584482400,
    down_time = 1605650400,
})

store:upsert({
    id = 5,
    store_type = 1,
    recommendation = 4,
    itemid = 103050010,
    limit_type = 0,
    origin_price = {{item_id=1,item_amount=588}},
    now_price = {{item_id=1,item_amount=288}},
    present_available = true,
    present_price = {item_id=1,item_amount=288},
    time_limit = true,
    up_time = 1584482400,
    down_time = 1605650400,
})

store:upsert({
    id = 6,
    store_type = 1,
    recommendation = 4,
    itemid = 103060010,
    limit_type = 0,
    origin_price = {{item_id=1,item_amount=588}},
    now_price = {{item_id=1,item_amount=588}},
    present_available = true,
    present_price = {item_id=1,item_amount=588},
    up_time = 1584482400,
    down_time = 1605650400,
})

store:upsert({
    id = 7,
    store_type = 1,
    recommendation = 5,
    itemid = 104010010,
    limit_type = 0,
    origin_price = {{item_id=1,item_amount=888}},
    now_price = {{item_id=1,item_amount=588}},
    present_available = true,
    present_price = {item_id=1,item_amount=588},
    up_time = 1584482400,
    down_time = 1605650400,
})

store:upsert({
    id = 8,
    store_type = 1,
    recommendation = 6,
    itemid = 105010010,
    limit_type = 0,
    origin_price = {{item_id=1,item_amount=1888}},
    now_price = {{item_id=1,item_amount=888}},
    present_available = true,
    present_price = {item_id=1,item_amount=588},
    up_time = 1584482400,
    down_time = 1605650400,
})

store:upsert({
    id = 9,
    store_type = 1,
    recommendation = 7,
    itemid = 121000010,
    limit_type = 0,
    origin_price = {{item_id=1,item_amount=588}},
    now_price = {{item_id=1,item_amount=588}},
    present_available = true,
    present_price = {item_id=1,item_amount=588},
    up_time = 1584482400,
    down_time = 1605650400,
})

store:upsert({
    id = 10,
    store_type = 1,
    recommendation = 8,
    itemid = 122000010,
    limit_type = 0,
    origin_price = {{item_id=1,item_amount=588}},
    now_price = {{item_id=1,item_amount=588}},
    present_price = {item_id=1,item_amount=588},
    up_time = 1584482400,
    down_time = 1605650400,
})

store:upsert({
    id = 11,
    store_type = 2,
    itemid = 121000010,
    limit_type = 1,
    limits = 2,
    origin_price = {{item_id=1,item_amount=588}},
    now_price = {{item_id=1,item_amount=588}},
    present_available = true,
    present_price = {item_id=1,item_amount=588},
    up_time = 1584482400,
    down_time = 1605650400,
})

store:upsert({
    id = 12,
    store_type = 2,
    itemid = 122000010,
    limit_type = 2,
    limits = 3,
    origin_price = {{item_id=1,item_amount=588}},
    now_price = {{item_id=1,item_amount=588}},
    present_available = true,
    present_price = {item_id=1,item_amount=588},
    up_time = 1584482400,
    down_time = 1605650400,
})

store:upsert({
    id = 13,
    store_type = 2,
    itemid = 103010010,
    limit_type = 0,
    origin_price = {{item_id=1,item_amount=588}},
    now_price = {{item_id=1,item_amount=588}},
    present_available = true,
    present_price = {item_id=1,item_amount=588},
    up_time = 1584482400,
    down_time = 1605650400,
})

store:upsert({
    id = 14,
    store_type = 2,
    itemid = 103030010,
    limit_type = 0,
    origin_price = {{item_id=1,item_amount=588}},
    now_price = {{item_id=1,item_amount=588}},
    present_available = true,
    present_price = {item_id=1,item_amount=588},
    up_time = 1584482400,
    down_time = 1605650400,
})

store:upsert({
    id = 15,
    store_type = 2,
    itemid = 103050010,
    limit_type = 0,
    origin_price = {{item_id=1,item_amount=588}},
    now_price = {{item_id=1,item_amount=588}},
    present_available = true,
    present_price = {item_id=1,item_amount=588},
    up_time = 1584482400,
    down_time = 1605650400,
})

store:upsert({
    id = 16,
    store_type = 2,
    itemid = 103060010,
    limit_type = 0,
    origin_price = {{item_id=1,item_amount=588}},
    now_price = {{item_id=1,item_amount=588}},
    present_available = true,
    present_price = {item_id=1,item_amount=588},
    up_time = 1584482400,
    down_time = 1605650400,
})

store:upsert({
    id = 17,
    store_type = 2,
    itemid = 104010010,
    limit_type = 0,
    origin_price = {{item_id=1,item_amount=888}},
    now_price = {{item_id=1,item_amount=588}},
    present_available = true,
    present_price = {item_id=1,item_amount=588},
    up_time = 1584482400,
    down_time = 1605650400,
})

store:upsert({
    id = 18,
    store_type = 2,
    itemid = 105010010,
    limit_type = 0,
    origin_price = {{item_id=1,item_amount=1888}},
    now_price = {{item_id=1,item_amount=888}},
    present_available = true,
    present_price = {item_id=1,item_amount=588},
    up_time = 1584482400,
    down_time = 1605650400,
})

store:upsert({
    id = 19,
    store_type = 2,
    itemid = 106020010,
    limit_type = 0,
    origin_price = {{item_id=1,item_amount=588}},
    now_price = {{item_id=1,item_amount=588}},
    present_available = true,
    present_price = {item_id=1,item_amount=588},
    up_time = 1584482400,
    down_time = 1605650400,
})

store:upsert({
    id = 20,
    store_type = 2,
    itemid = 107060010,
    limit_type = 0,
    origin_price = {{item_id=1,item_amount=588}},
    now_price = {{item_id=1,item_amount=588}},
    present_available = true,
    present_price = {item_id=1,item_amount=588},
    up_time = 1584482400,
    down_time = 1605650400,
})

store:upsert({
    id = 21,
    store_type = 2,
    itemid = 108010010,
    limit_type = 0,
    origin_price = {{item_id=1,item_amount=588}},
    now_price = {{item_id=1,item_amount=588}},
    present_available = true,
    present_price = {item_id=1,item_amount=588},
    up_time = 1584482400,
    down_time = 1605650400,
})

store:upsert({
    id = 22,
    store_type = 2,
    itemid = 109030010,
    limit_type = 0,
    origin_price = {{item_id=1,item_amount=588}},
    now_price = {{item_id=1,item_amount=588}},
    present_available = true,
    present_price = {item_id=1,item_amount=588},
    up_time = 1584482400,
    down_time = 1605650400,
})

store:upsert({
    id = 23,
    store_type = 3,
    itemid = 10001,
    limit_type = 0,
    origin_price = {{item_id=1,item_amount=588}},
    now_price = {{item_id=1,item_amount=588}},
    present_available = true,
    present_price = {item_id=1,item_amount=588},
    up_time = 1584482400,
    down_time = 1605650400,
})

store:upsert({
    id = 24,
    store_type = 3,
    itemid = 10101,
    limit_type = 0,
    origin_price = {{item_id=1,item_amount=688}},
    now_price = {{item_id=1,item_amount=588}},
    present_available = true,
    present_price = {item_id=1,item_amount=588},
    up_time = 1584482400,
    down_time = 1605650400,
})

store:upsert({
    id = 25,
    store_type = 3,
    itemid = 20001,
    limit_type = 0,
    origin_price = {{item_id=1,item_amount=988}},
    now_price = {{item_id=1,item_amount=588}},
    present_available = true,
    present_price = {item_id=1,item_amount=588},
    up_time = 1584482400,
    down_time = 1605650400,
})
