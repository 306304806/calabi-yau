--weapontype_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local weapontype = config_mgr:get_table("weapontype")

--导出版本号
weapontype:set_version(10000)

--导出配置内容
weapontype:upsert({
    id = 1,
    slot = 1,
    upgrade = true,
    id1 = 1,
    id2 = 2,
    id3 = 3,
    id4 = 4,
    id5 = 5,
    id6 = 6,
})

weapontype:upsert({
    id = 2,
    slot = 1,
    upgrade = true,
    id1 = 1,
    id2 = 2,
    id3 = 3,
    id4 = 4,
    id5 = 5,
    id6 = 6,
})

weapontype:upsert({
    id = 3,
    slot = 1,
    upgrade = true,
    id1 = 1,
    id2 = 2,
    id3 = 3,
    id4 = 4,
    id5 = 5,
    id6 = 6,
})

weapontype:upsert({
    id = 4,
    slot = 1,
    upgrade = true,
    id1 = 1,
    id2 = 2,
    id3 = 3,
    id4 = 4,
    id5 = 5,
    id6 = 6,
})

weapontype:upsert({
    id = 5,
    slot = 1,
    upgrade = true,
    id1 = 1,
    id2 = 2,
    id3 = 3,
    id4 = 4,
    id5 = 5,
    id6 = 6,
})

weapontype:upsert({
    id = 6,
    slot = 1,
    upgrade = true,
    id1 = 1,
    id2 = 2,
    id3 = 3,
    id4 = 4,
    id5 = 5,
    id6 = 6,
})

weapontype:upsert({
    id = 21,
    slot = 2,
    upgrade = true,
    id1 = 1,
    id2 = 2,
    id3 = 3,
    id4 = 4,
    id5 = 5,
    id6 = 6,
})

weapontype:upsert({
    id = 22,
    slot = 2,
    upgrade = true,
    id1 = 1,
    id2 = 2,
    id3 = 3,
    id4 = 4,
    id5 = 5,
    id6 = 6,
})

weapontype:upsert({
    id = 23,
    slot = 2,
    upgrade = true,
    id1 = 1,
    id2 = 2,
    id3 = 3,
    id4 = 4,
    id5 = 5,
    id6 = 6,
})

weapontype:upsert({
    id = 24,
    slot = 2,
    upgrade = true,
    id1 = 1,
    id2 = 2,
    id3 = 3,
    id4 = 4,
    id5 = 5,
    id6 = 6,
})

weapontype:upsert({
    id = 25,
    slot = 2,
    upgrade = true,
    id1 = 1,
    id2 = 2,
    id3 = 3,
    id4 = 4,
    id5 = 5,
    id6 = 6,
})

weapontype:upsert({
    id = 26,
    slot = 2,
    upgrade = true,
    id1 = 1,
    id2 = 2,
    id3 = 3,
    id4 = 4,
    id5 = 5,
    id6 = 6,
})

weapontype:upsert({
    id = 27,
    slot = 2,
    upgrade = true,
    id1 = 1,
    id2 = 2,
    id3 = 3,
    id4 = 4,
    id5 = 5,
    id6 = 6,
})

weapontype:upsert({
    id = 28,
    slot = 2,
    upgrade = true,
    id1 = 1,
    id2 = 2,
    id3 = 3,
    id4 = 4,
    id5 = 5,
    id6 = 6,
})

weapontype:upsert({
    id = 51,
    slot = 3,
    upgrade = false,
    id1 = 1,
    id2 = 2,
    id7 = 7,
})

weapontype:upsert({
    id = 52,
    slot = 3,
    upgrade = false,
    id1 = 1,
    id2 = 2,
    id7 = 7,
})

weapontype:upsert({
    id = 54,
    slot = 3,
    upgrade = false,
    id1 = 1,
    id2 = 2,
    id7 = 7,
})

weapontype:upsert({
    id = 81,
    slot = 4,
    upgrade = false,
    id1 = 1,
    id7 = 7,
})

weapontype:upsert({
    id = 82,
    slot = 4,
    upgrade = false,
    id1 = 1,
    id7 = 7,
})

weapontype:upsert({
    id = 83,
    slot = 4,
    upgrade = false,
    id1 = 1,
    id7 = 7,
})

weapontype:upsert({
    id = 53,
    slot = 8,
    upgrade = false,
    id7 = 7,
})

weapontype:upsert({
    id = 55,
    slot = 10,
    upgrade = false,
})

weapontype:upsert({
    id = 56,
    slot = 8,
    upgrade = false,
    id7 = 7,
})

weapontype:upsert({
    id = 100,
    slot = 7,
    upgrade = false,
})

weapontype:upsert({
    id = 101,
    slot = 8,
    upgrade = false,
    id1 = 1,
    id2 = 2,
    id3 = 3,
    id4 = 4,
    id5 = 5,
    id6 = 6,
})

weapontype:upsert({
    id = 102,
    slot = 9,
})
