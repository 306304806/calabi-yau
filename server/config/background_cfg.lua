--background_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local background = config_mgr:get_table("background")

--导出版本号
background:set_version(10000)

--导出配置内容
background:upsert({
    background_id = 300001,
    name = '学校天台',
    quality = 1,
})

background:upsert({
    background_id = 300002,
    name = '夏日街道',
    quality = 2,
})

background:upsert({
    background_id = 300003,
    name = '主界面背景3',
    quality = 3,
})

background:upsert({
    background_id = 300004,
    name = '主界面背景4',
    quality = 4,
})

background:upsert({
    background_id = 300005,
    name = '主界面背景5',
    quality = 1,
})

background:upsert({
    background_id = 300006,
    name = '主界面背景6',
    quality = 2,
})
