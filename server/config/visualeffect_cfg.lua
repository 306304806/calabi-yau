--visualeffect_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local visualeffect = config_mgr:get_table("visualeffect")

--导出版本号
visualeffect:set_version(10000)

--导出配置内容
visualeffect:upsert({
    vfx_id = 200001,
    name = '撒达路寒风',
    quality = 1,
    vfx_type = 1,
})

visualeffect:upsert({
    vfx_id = 200002,
    name = '飞行特效2',
    quality = 2,
    vfx_type = 1,
})

visualeffect:upsert({
    vfx_id = 200003,
    name = '飞行特效3',
    quality = 3,
    vfx_type = 1,
})

visualeffect:upsert({
    vfx_id = 200004,
    name = '飞行特效4',
    quality = 4,
    vfx_type = 1,
})

visualeffect:upsert({
    vfx_id = 200005,
    name = '飞行特效5',
    quality = 1,
    vfx_type = 1,
})

visualeffect:upsert({
    vfx_id = 200006,
    name = '飞行特效6',
    quality = 2,
    vfx_type = 1,
})

visualeffect:upsert({
    vfx_id = 210001,
    name = '挽歌',
    quality = 1,
    vfx_type = 2,
})

visualeffect:upsert({
    vfx_id = 210002,
    name = '命中特效2',
    quality = 2,
    vfx_type = 2,
})

visualeffect:upsert({
    vfx_id = 210003,
    name = '命中特效3',
    quality = 3,
    vfx_type = 2,
})

visualeffect:upsert({
    vfx_id = 210004,
    name = '命中特效4',
    quality = 4,
    vfx_type = 2,
})

visualeffect:upsert({
    vfx_id = 210005,
    name = '命中特效5',
    quality = 1,
    vfx_type = 2,
})

visualeffect:upsert({
    vfx_id = 210006,
    name = '命中特效6',
    quality = 2,
    vfx_type = 2,
})

visualeffect:upsert({
    vfx_id = 220001,
    name = '绘春',
    quality = 1,
    vfx_type = 3,
})

visualeffect:upsert({
    vfx_id = 220002,
    name = '复活特效2',
    quality = 2,
    vfx_type = 3,
})

visualeffect:upsert({
    vfx_id = 220003,
    name = '复活特效3',
    quality = 3,
    vfx_type = 3,
})

visualeffect:upsert({
    vfx_id = 220004,
    name = '复活特效4',
    quality = 4,
    vfx_type = 3,
})

visualeffect:upsert({
    vfx_id = 220005,
    name = '复活特效5',
    quality = 1,
    vfx_type = 3,
})

visualeffect:upsert({
    vfx_id = 220006,
    name = '复活特效6',
    quality = 2,
    vfx_type = 3,
})

visualeffect:upsert({
    vfx_id = 230001,
    name = '融解',
    quality = 1,
    vfx_type = 4,
})

visualeffect:upsert({
    vfx_id = 230002,
    name = '击败特效2',
    quality = 2,
    vfx_type = 4,
})

visualeffect:upsert({
    vfx_id = 230003,
    name = '击败特效3',
    quality = 3,
    vfx_type = 4,
})

visualeffect:upsert({
    vfx_id = 230004,
    name = '击败特效4',
    quality = 4,
    vfx_type = 4,
})

visualeffect:upsert({
    vfx_id = 230005,
    name = '击败特效5',
    quality = 1,
    vfx_type = 4,
})

visualeffect:upsert({
    vfx_id = 230006,
    name = '击败特效6',
    quality = 2,
    vfx_type = 4,
})
