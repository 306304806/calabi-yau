--pbscan_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local pbscan = config_mgr:get_table("pbscan")

--导出版本号
pbscan:set_version(10000)

--导出配置内容
pbscan:upsert({
    type = 'TYPE_BOOL',
    ref_value = {-1, false, true, 1, 999},
})

pbscan:upsert({
    type = 'TYPE_UINT64',
    ref_value = {-1, 0, 9999},
})

pbscan:upsert({
    type = 'TYPE_UINT32',
    ref_value = {-1, 0, 9999},
})

pbscan:upsert({
    type = 'TYPE_INT32',
    ref_value = {-1, 0, 9999},
})

pbscan:upsert({
    type = 'TYPE_INT64',
    ref_value = {-1, 0, 9999},
})

pbscan:upsert({
    type = 'TYPE_STRING',
    ref_value = {"", 0, "select x from b where a=b", "ddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd"},
})
