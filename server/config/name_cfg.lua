--name_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local name = config_mgr:get_table("name")

--导出版本号
name:set_version(10000)

--导出配置内容
name:upsert({
    id = 1,
    name = '忠诚',
    type = 1,
})

name:upsert({
    id = 2,
    name = '冷雨',
    type = 1,
})

name:upsert({
    id = 3,
    name = '隐匿',
    type = 1,
})

name:upsert({
    id = 4,
    name = '暴躁',
    type = 1,
})

name:upsert({
    id = 5,
    name = '机灵',
    type = 1,
})

name:upsert({
    id = 6,
    name = '无情',
    type = 1,
})

name:upsert({
    id = 7,
    name = '高大',
    type = 1,
})

name:upsert({
    id = 8,
    name = '矮小',
    type = 1,
})

name:upsert({
    id = 9,
    name = '黑暗',
    type = 1,
})

name:upsert({
    id = 10,
    name = '炙热',
    type = 1,
})

name:upsert({
    id = 11,
    name = '烈焰',
    type = 1,
})

name:upsert({
    id = 12,
    name = '活泼',
    type = 1,
})

name:upsert({
    id = 13,
    name = '鲁莽',
    type = 1,
})

name:upsert({
    id = 14,
    name = '脆弱',
    type = 1,
})

name:upsert({
    id = 15,
    name = '无畏',
    type = 1,
})

name:upsert({
    id = 16,
    name = '耿直',
    type = 1,
})

name:upsert({
    id = 17,
    name = '机敏',
    type = 1,
})

name:upsert({
    id = 18,
    name = '善良',
    type = 1,
})

name:upsert({
    id = 19,
    name = '轻率',
    type = 1,
})

name:upsert({
    id = 20,
    name = '顽固',
    type = 1,
})

name:upsert({
    id = 21,
    name = '傲慢',
    type = 1,
})

name:upsert({
    id = 22,
    name = '骄傲',
    type = 1,
})

name:upsert({
    id = 23,
    name = '疯狂',
    type = 1,
})

name:upsert({
    id = 24,
    name = '蒸汽',
    type = 1,
})

name:upsert({
    id = 25,
    name = '荆棘',
    type = 1,
})

name:upsert({
    id = 26,
    name = '迷失',
    type = 1,
})

name:upsert({
    id = 27,
    name = '游走',
    type = 1,
})

name:upsert({
    id = 28,
    name = '寒霜',
    type = 1,
})

name:upsert({
    id = 29,
    name = '荣耀',
    type = 1,
})

name:upsert({
    id = 30,
    name = '秩序',
    type = 1,
})

name:upsert({
    id = 31,
    name = '狂热',
    type = 1,
})

name:upsert({
    id = 32,
    name = '亨利',
    type = 2,
})

name:upsert({
    id = 33,
    name = '枪手',
    type = 2,
})

name:upsert({
    id = 34,
    name = '火星',
    type = 2,
})

name:upsert({
    id = 35,
    name = '喵酱',
    type = 2,
})

name:upsert({
    id = 36,
    name = '斗鱼',
    type = 2,
})

name:upsert({
    id = 37,
    name = '樱桃',
    type = 2,
})

name:upsert({
    id = 38,
    name = '飞猪',
    type = 2,
})

name:upsert({
    id = 39,
    name = '小熊',
    type = 2,
})

name:upsert({
    id = 40,
    name = '汪酱',
    type = 2,
})

name:upsert({
    id = 41,
    name = '小花',
    type = 2,
})

name:upsert({
    id = 42,
    name = '瓶子',
    type = 2,
})

name:upsert({
    id = 43,
    name = '西瓜',
    type = 2,
})

name:upsert({
    id = 44,
    name = '小鸟',
    type = 2,
})

name:upsert({
    id = 45,
    name = '小月',
    type = 2,
})

name:upsert({
    id = 46,
    name = '椰子',
    type = 2,
})

name:upsert({
    id = 47,
    name = '桔子',
    type = 2,
})

name:upsert({
    id = 48,
    name = '橙子',
    type = 2,
})

name:upsert({
    id = 49,
    name = '荔枝',
    type = 2,
})

name:upsert({
    id = 50,
    name = '香蕉',
    type = 2,
})

name:upsert({
    id = 51,
    name = '甜甜圈',
    type = 2,
})

name:upsert({
    id = 52,
    name = '团子',
    type = 2,
})

name:upsert({
    id = 53,
    name = '红茶',
    type = 2,
})

name:upsert({
    id = 54,
    name = '柚子',
    type = 2,
})

name:upsert({
    id = 55,
    name = '扇贝',
    type = 2,
})

name:upsert({
    id = 56,
    name = '抹茶',
    type = 2,
})

name:upsert({
    id = 57,
    name = '凉瓜',
    type = 2,
})

name:upsert({
    id = 58,
    name = '小米',
    type = 2,
})

name:upsert({
    id = 59,
    name = '小麦',
    type = 2,
})

name:upsert({
    id = 60,
    name = '玉米',
    type = 2,
})

name:upsert({
    id = 61,
    name = '桃桃',
    type = 2,
})

name:upsert({
    id = 62,
    name = '青柠',
    type = 2,
})

name:upsert({
    id = 63,
    name = '小狼',
    type = 2,
})

name:upsert({
    id = 64,
    name = '绿豆',
    type = 2,
})

name:upsert({
    id = 65,
    name = '红豆',
    type = 2,
})

name:upsert({
    id = 66,
    name = '花生',
    type = 2,
})

name:upsert({
    id = 67,
    name = '芝麻',
    type = 2,
})

name:upsert({
    id = 68,
    name = '甜虾',
    type = 2,
})

name:upsert({
    id = 69,
    name = '欧包',
    type = 2,
})

name:upsert({
    id = 70,
    name = '土豆',
    type = 2,
})

name:upsert({
    id = 71,
    name = '青椒',
    type = 2,
})

name:upsert({
    id = 72,
    name = '春天',
    type = 2,
})

name:upsert({
    id = 73,
    name = '花茶',
    type = 2,
})

name:upsert({
    id = 74,
    name = '夕夕',
    type = 2,
})

name:upsert({
    id = 75,
    name = '泡沫',
    type = 2,
})

name:upsert({
    id = 76,
    name = '小白',
    type = 2,
})

name:upsert({
    id = 77,
    name = '兔兔',
    type = 2,
})

name:upsert({
    id = 78,
    name = '蜜瓜',
    type = 2,
})

name:upsert({
    id = 79,
    name = '豆花',
    type = 2,
})

name:upsert({
    id = 80,
    name = '旅行家',
    type = 2,
})

name:upsert({
    id = 81,
    name = '二哈',
    type = 2,
})

name:upsert({
    id = 82,
    name = '蜜桃',
    type = 2,
})

name:upsert({
    id = 83,
    name = '企鹅',
    type = 2,
})

name:upsert({
    id = 84,
    name = '灯灯',
    type = 2,
})

name:upsert({
    id = 85,
    name = '巧克力',
    type = 2,
})

name:upsert({
    id = 86,
    name = '多肉',
    type = 2,
})

name:upsert({
    id = 87,
    name = '贝壳',
    type = 2,
})

name:upsert({
    id = 88,
    name = '鲶鱼',
    type = 2,
})

name:upsert({
    id = 89,
    name = '勇士',
    type = 2,
})

name:upsert({
    id = 90,
    name = '饼饼',
    type = 2,
})

name:upsert({
    id = 91,
    name = '饺子',
    type = 2,
})

name:upsert({
    id = 92,
    name = '秋葵',
    type = 2,
})

name:upsert({
    id = 93,
    name = '猎人',
    type = 2,
})

name:upsert({
    id = 94,
    name = '薄荷',
    type = 2,
})

name:upsert({
    id = 95,
    name = '冰茶',
    type = 2,
})

name:upsert({
    id = 96,
    name = '豪杰',
    type = 2,
})

name:upsert({
    id = 97,
    name = '阴影',
    type = 2,
})

name:upsert({
    id = 98,
    name = '护卫',
    type = 2,
})

name:upsert({
    id = 99,
    name = '无序者',
    type = 2,
})

name:upsert({
    id = 100,
    name = '守护者',
    type = 2,
})

name:upsert({
    id = 101,
    name = '十面',
    type = 2,
})

name:upsert({
    id = 102,
    name = '奇兵',
    type = 2,
})

name:upsert({
    id = 103,
    name = '水母',
    type = 2,
})

name:upsert({
    id = 104,
    name = '小竹',
    type = 2,
})

name:upsert({
    id = 105,
    name = '之枪',
    type = 2,
})

name:upsert({
    id = 106,
    name = '之镰',
    type = 2,
})

name:upsert({
    id = 107,
    name = '龙王',
    type = 2,
})

name:upsert({
    id = 108,
    name = '双子',
    type = 2,
})

name:upsert({
    id = 109,
    name = '女巫',
    type = 2,
})

name:upsert({
    id = 110,
    name = '妖狐',
    type = 2,
})

name:upsert({
    id = 111,
    name = '鱼人',
    type = 2,
})

name:upsert({
    id = 112,
    name = '神鸟',
    type = 2,
})

name:upsert({
    id = 113,
    name = '之灵',
    type = 2,
})

name:upsert({
    id = 114,
    name = '蜘蛛',
    type = 2,
})

name:upsert({
    id = 115,
    name = '妖猫',
    type = 2,
})

name:upsert({
    id = 116,
    name = '河童',
    type = 2,
})

name:upsert({
    id = 117,
    name = '鬣狗',
    type = 2,
})

name:upsert({
    id = 118,
    name = '领主',
    type = 2,
})

name:upsert({
    id = 119,
    name = '执行者',
    type = 2,
})

name:upsert({
    id = 120,
    name = 'A',
    type = 3,
})

name:upsert({
    id = 121,
    name = 'B',
    type = 3,
})

name:upsert({
    id = 122,
    name = 'C',
    type = 3,
})

name:upsert({
    id = 123,
    name = 'D',
    type = 3,
})

name:upsert({
    id = 124,
    name = 'E',
    type = 3,
})

name:upsert({
    id = 125,
    name = 'F',
    type = 3,
})

name:upsert({
    id = 126,
    name = 'G',
    type = 3,
})

name:upsert({
    id = 127,
    name = 'H',
    type = 3,
})

name:upsert({
    id = 128,
    name = 'I',
    type = 3,
})

name:upsert({
    id = 129,
    name = 'J',
    type = 3,
})

name:upsert({
    id = 130,
    name = 'K',
    type = 3,
})

name:upsert({
    id = 131,
    name = 'L',
    type = 3,
})

name:upsert({
    id = 132,
    name = 'M',
    type = 3,
})

name:upsert({
    id = 133,
    name = 'N',
    type = 3,
})

name:upsert({
    id = 134,
    name = 'O',
    type = 3,
})

name:upsert({
    id = 135,
    name = 'P',
    type = 3,
})

name:upsert({
    id = 136,
    name = 'Q',
    type = 3,
})

name:upsert({
    id = 137,
    name = 'R',
    type = 3,
})

name:upsert({
    id = 138,
    name = 'S',
    type = 3,
})

name:upsert({
    id = 139,
    name = 'T',
    type = 3,
})

name:upsert({
    id = 140,
    name = 'U',
    type = 3,
})

name:upsert({
    id = 141,
    name = 'V',
    type = 3,
})

name:upsert({
    id = 142,
    name = 'W',
    type = 3,
})

name:upsert({
    id = 143,
    name = 'X',
    type = 3,
})

name:upsert({
    id = 144,
    name = 'Y',
    type = 3,
})

name:upsert({
    id = 145,
    name = 'Z',
    type = 3,
})

name:upsert({
    id = 146,
    name = '1',
    type = 3,
})

name:upsert({
    id = 147,
    name = '2',
    type = 3,
})

name:upsert({
    id = 148,
    name = '3',
    type = 3,
})

name:upsert({
    id = 149,
    name = '4',
    type = 3,
})

name:upsert({
    id = 150,
    name = '5',
    type = 3,
})

name:upsert({
    id = 151,
    name = '6',
    type = 3,
})

name:upsert({
    id = 152,
    name = '7',
    type = 3,
})

name:upsert({
    id = 153,
    name = '8',
    type = 3,
})

name:upsert({
    id = 154,
    name = '9',
    type = 3,
})

name:upsert({
    id = 155,
    name = '人工智能',
    type = 4,
})

name:upsert({
    id = 156,
    name = '00001',
    type = 5,
})

name:upsert({
    id = 157,
    name = '00002',
    type = 5,
})

name:upsert({
    id = 158,
    name = '00003',
    type = 5,
})

name:upsert({
    id = 159,
    name = '00004',
    type = 5,
})

name:upsert({
    id = 160,
    name = '00005',
    type = 5,
})

name:upsert({
    id = 161,
    name = '00006',
    type = 5,
})

name:upsert({
    id = 162,
    name = '00007',
    type = 5,
})

name:upsert({
    id = 163,
    name = '00008',
    type = 5,
})

name:upsert({
    id = 164,
    name = '00009',
    type = 5,
})

name:upsert({
    id = 165,
    name = '00010',
    type = 5,
})

name:upsert({
    id = 166,
    name = '00011',
    type = 5,
})

name:upsert({
    id = 167,
    name = '00012',
    type = 5,
})

name:upsert({
    id = 168,
    name = '00013',
    type = 5,
})

name:upsert({
    id = 169,
    name = '00014',
    type = 5,
})

name:upsert({
    id = 170,
    name = '00015',
    type = 5,
})

name:upsert({
    id = 171,
    name = '00016',
    type = 5,
})

name:upsert({
    id = 172,
    name = '00017',
    type = 5,
})

name:upsert({
    id = 173,
    name = '00018',
    type = 5,
})

name:upsert({
    id = 174,
    name = '00019',
    type = 5,
})

name:upsert({
    id = 175,
    name = '00020',
    type = 5,
})
