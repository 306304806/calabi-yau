--battlepassseason_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local battlepassseason = config_mgr:get_table("battlepassseason")

--导出版本号
battlepassseason:set_version(10000)

--导出配置内容
battlepassseason:upsert({
    id = 1,
    start = 1613599200,
    finish = 1618696799,
})

battlepassseason:upsert({
    id = 2,
    start = 1618696800,
    finish = 1623967199,
})

battlepassseason:upsert({
    id = 3,
    start = 1623967200,
    finish = 1629237599,
})
