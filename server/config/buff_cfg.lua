--buff_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local buff = config_mgr:get_table("buff")

--导出版本号
buff:set_version(10000)

--导出配置内容
buff:upsert({
    buff_id = 10101,
    effects = {{effect_id=520001, trigger_type=2}},
    time = 3600,
    store = true,
    share_group = 1,
    share_priority = 100,
    overlap_group = 1,
    overlap_type = 4,
    overlap_layer = 0,
    mutex_group = 0,
    mutex_priority = 0,
})

buff:upsert({
    buff_id = 10102,
    effects = {{effect_id=520001, trigger_type=2}},
    time = 7200,
    store = true,
    share_group = 1,
    share_priority = 100,
    overlap_group = 1,
    overlap_type = 4,
    overlap_layer = 0,
    mutex_group = 0,
    mutex_priority = 0,
})

buff:upsert({
    buff_id = 10201,
    effects = {{effect_id=530001, trigger_type=2}},
    time = 3600,
    store = true,
    share_group = 2,
    share_priority = 100,
    overlap_group = 3,
    overlap_type = 4,
    overlap_layer = 0,
    mutex_group = 0,
    mutex_priority = 0,
})

buff:upsert({
    buff_id = 10202,
    effects = {{effect_id=530001, trigger_type=2}},
    time = 7200,
    store = true,
    share_group = 2,
    share_priority = 100,
    overlap_group = 3,
    overlap_type = 4,
    overlap_layer = 0,
    mutex_group = 0,
    mutex_priority = 0,
})

buff:upsert({
    buff_id = 10301,
    effects = {{effect_id=550001, trigger_type=2}},
    time = 3600,
    store = true,
    share_group = 3,
    share_priority = 100,
    overlap_group = 4,
    overlap_type = 4,
    overlap_layer = 0,
    mutex_group = 0,
    mutex_priority = 0,
})

buff:upsert({
    buff_id = 10302,
    effects = {{effect_id=550001, trigger_type=2}},
    time = 7200,
    store = true,
    share_group = 3,
    share_priority = 100,
    overlap_group = 4,
    overlap_type = 4,
    overlap_layer = 0,
    mutex_group = 0,
    mutex_priority = 0,
})

buff:upsert({
    buff_id = 20001,
    effects = {{effect_id=520003, trigger_type=2}},
    time = 259200,
    store = false,
    share_group = 1,
    share_priority = 10,
    overlap_group = 2,
    overlap_type = 4,
    overlap_layer = 0,
    mutex_group = 0,
    mutex_priority = 0,
})
