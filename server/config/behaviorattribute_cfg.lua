--behaviorattribute_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local behaviorattribute = config_mgr:get_table("behaviorattribute")

--导出版本号
behaviorattribute:set_version(10000)

--导出配置内容
behaviorattribute:upsert({
    id = 1,
    enum_key = 'TOTAL_ROUND',
    name = '总场次',
    default = 0,
    store = true,
    operation = 2,
})

behaviorattribute:upsert({
    id = 2,
    enum_key = 'TOTAL_WIN_ROUOND',
    name = '胜利总场次',
    default = 0,
    store = true,
    operation = 2,
})

behaviorattribute:upsert({
    id = 3,
    enum_key = 'TOTAL_KILL',
    name = '总击杀数',
    default = 0,
    store = true,
    operation = 2,
})

behaviorattribute:upsert({
    id = 4,
    enum_key = 'TOTAL_HIT_HEAD',
    name = '总爆头数',
    default = 0,
    store = true,
    operation = 2,
})

behaviorattribute:upsert({
    id = 5,
    enum_key = 'TEAM_ROUND',
    name = '团竞完成场次',
    default = 0,
    store = true,
    operation = 2,
})

behaviorattribute:upsert({
    id = 6,
    enum_key = 'TEAM_WIN_ROUND',
    name = '团竞胜利场次',
    default = 0,
    store = true,
    operation = 2,
})

behaviorattribute:upsert({
    id = 7,
    enum_key = 'BOMB_ROUND',
    name = '爆破完成场次',
    default = 0,
    store = true,
    operation = 2,
})

behaviorattribute:upsert({
    id = 8,
    enum_key = 'BOMB_WIN_ROUND',
    name = '爆破胜利场次',
    default = 0,
    store = true,
    operation = 2,
})

behaviorattribute:upsert({
    id = 9,
    enum_key = 'CUR_ACCOUNT_EXP',
    name = '当前获得账号经验',
    default = 0,
    store = false,
    operation = 1,
})

behaviorattribute:upsert({
    id = 10,
    enum_key = 'TOTAL_ACCOUNT_EXP',
    name = '获得账号总经验',
    default = 0,
    store = true,
    operation = 2,
})

behaviorattribute:upsert({
    id = 11,
    enum_key = 'CUR_ROLE_EXP',
    name = '当前获得角色经验',
    default = 0,
    store = false,
    operation = 1,
})

behaviorattribute:upsert({
    id = 12,
    enum_key = 'TOTAL_ROLE_EXP',
    name = '获得角色总经验',
    default = 0,
    store = true,
    operation = 2,
})

behaviorattribute:upsert({
    id = 13,
    enum_key = 'CUR_WEAPON_EXP',
    name = '当前获得武器经验',
    default = 0,
    store = false,
    operation = 1,
})

behaviorattribute:upsert({
    id = 14,
    enum_key = 'TOTAL_WEAPON_EXP',
    name = '获得武器总经验',
    default = 0,
    store = true,
    operation = 2,
})

behaviorattribute:upsert({
    id = 15,
    enum_key = 'CUR_IDEAL',
    name = '当前获得理想币',
    default = 0,
    store = false,
    operation = 1,
})

behaviorattribute:upsert({
    id = 16,
    enum_key = 'TOTAL_IDEAL',
    name = '获得总理想币',
    default = 0,
    store = true,
    operation = 2,
})

behaviorattribute:upsert({
    id = 17,
    enum_key = 'CUR_HERMES',
    name = '当前获得赫尔币',
    default = 0,
    store = false,
    operation = 1,
})

behaviorattribute:upsert({
    id = 18,
    enum_key = 'TOTAL_HERMES',
    name = '获得总赫尔币',
    default = 0,
    store = true,
    operation = 2,
})

behaviorattribute:upsert({
    id = 19,
    enum_key = 'CUR_CRYSTAL',
    name = '当前获得巴布洛晶体',
    default = 0,
    store = false,
    operation = 1,
})

behaviorattribute:upsert({
    id = 20,
    enum_key = 'TOTAL_CRYSTAL',
    name = '获得总巴布洛晶体',
    default = 0,
    store = true,
    operation = 2,
})

behaviorattribute:upsert({
    id = 21,
    enum_key = 'LEVEL_UP',
    name = '等级提升',
    default = 0,
    store = false,
    operation = 1,
})

behaviorattribute:upsert({
    id = 22,
    enum_key = 'ROUND_DAMAGE',
    name = '本局伤害',
    default = 0,
    store = false,
    operation = 1,
})

behaviorattribute:upsert({
    id = 23,
    enum_key = 'TOTAL_DAMAGE',
    name = '累计伤害',
    default = 0,
    store = true,
    operation = 2,
})

behaviorattribute:upsert({
    id = 24,
    enum_key = 'TEAM_STREAK_WIN',
    name = '团竞连胜',
    default = 0,
    store = true,
    operation = 2,
})

behaviorattribute:upsert({
    id = 25,
    enum_key = 'BOMB_STREAK_WIN',
    name = '爆破连胜',
    default = 0,
    store = true,
    operation = 2,
})

behaviorattribute:upsert({
    id = 26,
    enum_key = 'ROUND_KILL',
    name = '本局击杀',
    default = 0,
    store = false,
    operation = 1,
})

behaviorattribute:upsert({
    id = 27,
    enum_key = 'ROUND_HIT_HEAD',
    name = '本局爆头',
    default = 0,
    store = false,
    operation = 1,
})
