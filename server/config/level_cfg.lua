--level_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local level = config_mgr:get_table("level")

--导出版本号
level:set_version(10000)

--导出配置内容
level:upsert({
    lv = 1,
    exp = 100,
})

level:upsert({
    lv = 2,
    exp = 150,
})

level:upsert({
    lv = 3,
    exp = 200,
})

level:upsert({
    lv = 4,
    exp = 250,
})

level:upsert({
    lv = 5,
    exp = 300,
})

level:upsert({
    lv = 6,
    exp = 350,
})

level:upsert({
    lv = 7,
    exp = 400,
})

level:upsert({
    lv = 8,
    exp = 450,
})

level:upsert({
    lv = 9,
    exp = 500,
})

level:upsert({
    lv = 10,
    exp = 550,
})

level:upsert({
    lv = 11,
    exp = 600,
})

level:upsert({
    lv = 12,
    exp = 650,
})

level:upsert({
    lv = 13,
    exp = 700,
})

level:upsert({
    lv = 14,
    exp = 750,
})

level:upsert({
    lv = 15,
    exp = 800,
})

level:upsert({
    lv = 16,
    exp = 850,
})

level:upsert({
    lv = 17,
    exp = 900,
})

level:upsert({
    lv = 18,
    exp = 950,
})

level:upsert({
    lv = 19,
    exp = 1000,
})

level:upsert({
    lv = 20,
    exp = 1050,
})

level:upsert({
    lv = 21,
    exp = 1100,
})

level:upsert({
    lv = 22,
    exp = 1150,
})

level:upsert({
    lv = 23,
    exp = 1200,
})

level:upsert({
    lv = 24,
    exp = 1250,
})

level:upsert({
    lv = 25,
    exp = 1300,
})

level:upsert({
    lv = 26,
    exp = 1350,
})

level:upsert({
    lv = 27,
    exp = 1400,
})

level:upsert({
    lv = 28,
    exp = 1450,
})

level:upsert({
    lv = 29,
    exp = 1500,
})

level:upsert({
    lv = 30,
    exp = 1550,
})

level:upsert({
    lv = 31,
    exp = 1600,
})

level:upsert({
    lv = 32,
    exp = 1650,
})

level:upsert({
    lv = 33,
    exp = 1700,
})

level:upsert({
    lv = 34,
    exp = 1750,
})

level:upsert({
    lv = 35,
    exp = 1800,
})

level:upsert({
    lv = 36,
    exp = 1850,
})

level:upsert({
    lv = 37,
    exp = 1900,
})

level:upsert({
    lv = 38,
    exp = 1950,
})

level:upsert({
    lv = 39,
    exp = 2000,
})

level:upsert({
    lv = 40,
    exp = 2050,
})

level:upsert({
    lv = 41,
    exp = 2100,
})

level:upsert({
    lv = 42,
    exp = 2150,
})

level:upsert({
    lv = 43,
    exp = 2200,
})

level:upsert({
    lv = 44,
    exp = 2250,
})

level:upsert({
    lv = 45,
    exp = 2300,
})

level:upsert({
    lv = 46,
    exp = 2350,
})

level:upsert({
    lv = 47,
    exp = 2400,
})

level:upsert({
    lv = 48,
    exp = 2450,
})

level:upsert({
    lv = 49,
    exp = 2500,
})

level:upsert({
    lv = 50,
    exp = 2550,
})

level:upsert({
    lv = 51,
    exp = 2600,
})

level:upsert({
    lv = 52,
    exp = 2650,
})

level:upsert({
    lv = 53,
    exp = 2700,
})

level:upsert({
    lv = 54,
    exp = 2750,
})

level:upsert({
    lv = 55,
    exp = 2800,
})

level:upsert({
    lv = 56,
    exp = 2850,
})

level:upsert({
    lv = 57,
    exp = 2900,
})

level:upsert({
    lv = 58,
    exp = 2950,
})

level:upsert({
    lv = 59,
    exp = 3000,
})

level:upsert({
    lv = 60,
    exp = 3050,
})

level:upsert({
    lv = 61,
    exp = 3100,
})

level:upsert({
    lv = 62,
    exp = 3150,
})

level:upsert({
    lv = 63,
    exp = 3200,
})

level:upsert({
    lv = 64,
    exp = 3250,
})

level:upsert({
    lv = 65,
    exp = 3300,
})

level:upsert({
    lv = 66,
    exp = 3350,
})

level:upsert({
    lv = 67,
    exp = 3400,
})

level:upsert({
    lv = 68,
    exp = 3450,
})

level:upsert({
    lv = 69,
    exp = 3500,
})

level:upsert({
    lv = 70,
    exp = 3550,
})

level:upsert({
    lv = 71,
    exp = 3600,
})

level:upsert({
    lv = 72,
    exp = 3650,
})

level:upsert({
    lv = 73,
    exp = 3700,
})

level:upsert({
    lv = 74,
    exp = 3750,
})

level:upsert({
    lv = 75,
    exp = 3800,
})

level:upsert({
    lv = 76,
    exp = 3850,
})

level:upsert({
    lv = 77,
    exp = 3900,
})

level:upsert({
    lv = 78,
    exp = 3950,
})

level:upsert({
    lv = 79,
    exp = 4000,
})

level:upsert({
    lv = 80,
    exp = 4050,
})

level:upsert({
    lv = 81,
    exp = 4100,
})

level:upsert({
    lv = 82,
    exp = 4150,
})

level:upsert({
    lv = 83,
    exp = 4200,
})

level:upsert({
    lv = 84,
    exp = 4250,
})

level:upsert({
    lv = 85,
    exp = 4300,
})

level:upsert({
    lv = 86,
    exp = 4350,
})

level:upsert({
    lv = 87,
    exp = 4400,
})

level:upsert({
    lv = 88,
    exp = 4450,
})

level:upsert({
    lv = 89,
    exp = 4500,
})

level:upsert({
    lv = 90,
    exp = 4550,
})

level:upsert({
    lv = 91,
    exp = 4600,
})

level:upsert({
    lv = 92,
    exp = 4650,
})

level:upsert({
    lv = 93,
    exp = 4700,
})

level:upsert({
    lv = 94,
    exp = 4750,
})

level:upsert({
    lv = 95,
    exp = 4800,
})

level:upsert({
    lv = 96,
    exp = 4850,
})

level:upsert({
    lv = 97,
    exp = 4900,
})

level:upsert({
    lv = 98,
    exp = 4950,
})

level:upsert({
    lv = 99,
    exp = 5000,
})
