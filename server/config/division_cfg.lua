--division_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local division = config_mgr:get_table("division")

--导出版本号
division:set_version(10000)

--导出配置内容
division:upsert({
    id = 1,
    name = '倔强青铜III',
    ratio = 95,
    total_star = 3,
    score_max = 50,
    score_protect = 30,
    inherit = 1,
    group = 15,
    gradation = 1,
    gname = '倔强青铜',
})

division:upsert({
    id = 2,
    name = '倔强青铜II',
    ratio = 91,
    total_star = 3,
    score_max = 50,
    score_protect = 30,
    inherit = 1,
    group = 16,
    gradation = 1,
    gname = '倔强青铜',
})

division:upsert({
    id = 3,
    name = '倔强青铜I',
    ratio = 87,
    total_star = 3,
    score_max = 50,
    score_protect = 30,
    inherit = 2,
    group = 17,
    gradation = 1,
    gname = '倔强青铜',
})

division:upsert({
    id = 4,
    name = '秩序白银III',
    ratio = 83,
    total_star = 3,
    score_max = 50,
    score_protect = 30,
    inherit = 2,
    group = 18,
    gradation = 2,
    gname = '秩序白银',
})

division:upsert({
    id = 5,
    name = '秩序白银II',
    ratio = 79,
    total_star = 3,
    score_max = 50,
    score_protect = 30,
    inherit = 3,
    group = 19,
    gradation = 2,
    gname = '秩序白银',
})

division:upsert({
    id = 6,
    name = '秩序白银I',
    ratio = 75,
    total_star = 3,
    score_max = 50,
    score_protect = 30,
    inherit = 3,
    group = 20,
    gradation = 2,
    gname = '秩序白银',
})

division:upsert({
    id = 7,
    name = '荣耀黄金IV',
    ratio = 71,
    total_star = 4,
    score_max = 50,
    score_protect = 30,
    inherit = 4,
    group = 21,
    gradation = 3,
    gname = '荣耀黄金',
})

division:upsert({
    id = 8,
    name = '荣耀黄金III',
    ratio = 67,
    total_star = 4,
    score_max = 50,
    score_protect = 30,
    inherit = 4,
    group = 22,
    gradation = 3,
    gname = '荣耀黄金',
})

division:upsert({
    id = 9,
    name = '荣耀黄金II',
    ratio = 63,
    total_star = 4,
    score_max = 50,
    score_protect = 30,
    inherit = 5,
    group = 23,
    gradation = 3,
    gname = '荣耀黄金',
})

division:upsert({
    id = 10,
    name = '荣耀黄金I',
    ratio = 59,
    total_star = 4,
    score_max = 50,
    score_protect = 30,
    inherit = 5,
    group = 24,
    gradation = 3,
    gname = '荣耀黄金',
})

division:upsert({
    id = 11,
    name = '尊贵铂金IV',
    ratio = 55,
    total_star = 4,
    score_max = 50,
    score_protect = 30,
    inherit = 6,
    group = 25,
    gradation = 4,
    gname = '尊贵铂金',
})

division:upsert({
    id = 12,
    name = '尊贵铂金III',
    ratio = 51,
    total_star = 4,
    score_max = 50,
    score_protect = 30,
    inherit = 7,
    group = 26,
    gradation = 4,
    gname = '尊贵铂金',
})

division:upsert({
    id = 13,
    name = '尊贵铂金II',
    ratio = 47,
    total_star = 4,
    score_max = 50,
    score_protect = 30,
    inherit = 7,
    group = 27,
    gradation = 4,
    gname = '尊贵铂金',
})

division:upsert({
    id = 14,
    name = '尊贵铂金I',
    ratio = 43,
    total_star = 4,
    score_max = 50,
    score_protect = 30,
    inherit = 8,
    group = 28,
    gradation = 4,
    gname = '尊贵铂金',
})

division:upsert({
    id = 15,
    name = '永恒钻石V',
    ratio = 39,
    total_star = 5,
    score_max = 50,
    score_protect = 30,
    inherit = 8,
    group = 29,
    gradation = 5,
    gname = '永恒钻石',
})

division:upsert({
    id = 16,
    name = '永恒钻石IV',
    ratio = 35,
    total_star = 5,
    score_max = 50,
    score_protect = 30,
    inherit = 9,
    group = 30,
    gradation = 5,
    gname = '永恒钻石',
})

division:upsert({
    id = 17,
    name = '永恒钻石III',
    ratio = 31,
    total_star = 5,
    score_max = 50,
    score_protect = 30,
    inherit = 9,
    group = 31,
    gradation = 5,
    gname = '永恒钻石',
})

division:upsert({
    id = 18,
    name = '永恒钻石II',
    ratio = 27,
    total_star = 5,
    score_max = 50,
    score_protect = 30,
    inherit = 10,
    group = 32,
    gradation = 5,
    gname = '永恒钻石',
})

division:upsert({
    id = 19,
    name = '永恒钻石I',
    ratio = 23,
    total_star = 5,
    score_max = 50,
    score_protect = 30,
    inherit = 10,
    group = 33,
    gradation = 5,
    gname = '永恒钻石',
})

division:upsert({
    id = 20,
    name = '至尊星耀V',
    ratio = 19,
    total_star = 5,
    score_max = 50,
    score_protect = 30,
    inherit = 11,
    group = 34,
    gradation = 6,
    gname = '至尊星耀',
})

division:upsert({
    id = 21,
    name = '至尊星耀IV',
    ratio = 15,
    total_star = 5,
    score_max = 50,
    score_protect = 30,
    inherit = 11,
    group = 35,
    gradation = 6,
    gname = '至尊星耀',
})

division:upsert({
    id = 22,
    name = '至尊星耀III',
    ratio = 11,
    total_star = 5,
    score_max = 50,
    score_protect = 30,
    inherit = 12,
    group = 36,
    gradation = 6,
    gname = '至尊星耀',
})

division:upsert({
    id = 23,
    name = '至尊星耀II',
    ratio = 7,
    total_star = 5,
    score_max = 50,
    score_protect = 30,
    inherit = 12,
    group = 37,
    gradation = 6,
    gname = '至尊星耀',
})

division:upsert({
    id = 24,
    name = '至尊星耀I',
    ratio = 3,
    total_star = 5,
    score_max = 50,
    score_protect = 30,
    inherit = 13,
    group = 38,
    gradation = 6,
    gname = '至尊星耀',
})

division:upsert({
    id = 25,
    name = '最强王者',
    ratio = 1,
    total_star = 9999,
    score_max = 50,
    score_protect = 30,
    inherit = 13,
    group = 39,
    gradation = 7,
    gname = '最强王者',
})
