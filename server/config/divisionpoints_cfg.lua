--divisionpoints_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local divisionpoints = config_mgr:get_table("divisionpoints")

--导出版本号
divisionpoints:set_version(10000)

--导出配置内容
divisionpoints:upsert({
    id = 1,
    w_onhook = 0,
    l_onhook = 0,
    w_mode1 = 30,
    w_mode2 = 30,
    w_mode3 = 0,
    l_mode1 = 1,
    l_mode2 = 3,
    l_mode3 = 0,
    w_streak1 = 3,
    w_streak2 = 8,
    w_streak3 = 13,
    w_streak4 = 18,
    w_streak5 = 23,
    w_score1 = 5,
    w_score2 = 4,
    w_score3 = 3,
    w_score4 = 3,
    w_score5 = 3,
    l_score1 = 5,
    l_score2 = 4,
    l_score3 = 3,
    l_score4 = 3,
    l_score5 = 3,
    w_bonus = 10,
    l_bonus = 10,
    high_level_param = {0.6,0.55},
})
