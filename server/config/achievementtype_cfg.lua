--achievementtype_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local achievementtype = config_mgr:get_table("achievementtype")

--导出版本号
achievementtype:set_version(10000)

--导出配置内容
achievementtype:upsert({
    id = 1,
    level = 1,
    need = 0,
    mail_title = '成就-战斗勋章-D级',
    mail_content = '恭喜玩家达成战斗勋章-D级获得奖励如下',
    level_icon = 'D',
})

achievementtype:upsert({
    id = 1,
    level = 2,
    need = 20,
    reward = {{item_id=20001,item_amount=2}},
    mail_title = '成就-战斗勋章-C级',
    mail_content = '恭喜玩家达成战斗勋章-C级获得奖励如下',
    level_icon = 'C',
})

achievementtype:upsert({
    id = 1,
    level = 3,
    need = 30,
    reward = {{item_id=20001,item_amount=4}},
    mail_title = '成就-战斗勋章-B级',
    mail_content = '恭喜玩家达成战斗勋章-B级获得奖励如下',
    level_icon = 'B',
})

achievementtype:upsert({
    id = 1,
    level = 4,
    need = 40,
    reward = {{item_id=20001,item_amount=8}},
    mail_title = '成就-战斗勋章-A级',
    mail_content = '恭喜玩家达成战斗勋章-A级获得奖励如下',
    level_icon = 'A',
})

achievementtype:upsert({
    id = 1,
    level = 5,
    need = 50,
    reward = {{item_id=20001,item_amount=16}},
    mail_title = '成就-战斗勋章-S级',
    mail_content = '恭喜玩家达成战斗勋章-S级获得奖励如下',
    level_icon = 'S',
})

achievementtype:upsert({
    id = 2,
    level = 1,
    need = 0,
    mail_title = '成就-史诗成就-D级',
    mail_content = '恭喜玩家达成成就-史诗成就-D级奖励如下',
    level_icon = 'D',
})

achievementtype:upsert({
    id = 2,
    level = 2,
    need = 20,
    reward = {{item_id=20001,item_amount=2}},
    mail_title = '成就-史诗成就-C级',
    mail_content = '恭喜玩家达成成就-史诗成就-C级奖励如下',
    level_icon = 'C',
})

achievementtype:upsert({
    id = 2,
    level = 3,
    need = 30,
    reward = {{item_id=20001,item_amount=4}},
    mail_title = '成就-史诗成就-B级',
    mail_content = '恭喜玩家达成成就-史诗成就-B级奖励如下',
    level_icon = 'B',
})

achievementtype:upsert({
    id = 2,
    level = 4,
    need = 40,
    reward = {{item_id=20001,item_amount=8}},
    mail_title = '成就-史诗成就-A级',
    mail_content = '恭喜玩家达成成就-史诗成就-A级奖励如下',
    level_icon = 'A',
})

achievementtype:upsert({
    id = 2,
    level = 5,
    need = 50,
    reward = {{item_id=20001,item_amount=16}},
    mail_title = '成就-史诗成就-S级',
    mail_content = '恭喜玩家达成成就-史诗成就-S级奖励如下',
    level_icon = 'S',
})

achievementtype:upsert({
    id = 3,
    level = 1,
    need = 0,
    mail_title = '成就-荣耀成就-D级',
    mail_content = '恭喜玩家达成成就-荣耀成就-D级',
    level_icon = 'D',
})

achievementtype:upsert({
    id = 3,
    level = 2,
    need = 20,
    reward = {{item_id=20001,item_amount=2}},
    mail_title = '成就-荣耀成就-C级',
    mail_content = '恭喜玩家达成成就-荣耀成就-C级',
    level_icon = 'C',
})

achievementtype:upsert({
    id = 3,
    level = 3,
    need = 30,
    reward = {{item_id=20001,item_amount=4}},
    mail_title = '成就-荣耀成就-B级',
    mail_content = '恭喜玩家达成成就-荣耀成就-B级',
    level_icon = 'B',
})

achievementtype:upsert({
    id = 3,
    level = 4,
    need = 40,
    reward = {{item_id=20001,item_amount=8}},
    mail_title = '成就-荣耀成就-A级',
    mail_content = '恭喜玩家达成成就-荣耀成就-A级',
    level_icon = 'A',
})

achievementtype:upsert({
    id = 3,
    level = 5,
    need = 50,
    reward = {{item_id=20001,item_amount=16}},
    mail_title = '成就-荣耀成就-S级',
    mail_content = '恭喜玩家达成成就-荣耀成就-S级',
    level_icon = 'S',
})
