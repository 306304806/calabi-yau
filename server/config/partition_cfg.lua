--partition_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local partition = config_mgr:get_table("partition")

--导出版本号
partition:set_version(10000)

--导出配置内容
partition:upsert({
    partition_id = 1,
    partition_name = '稳定版',
    partition_state = 0,
})

partition:upsert({
    partition_id = 2,
    partition_name = '开发版',
    partition_state = 0,
})

partition:upsert({
    partition_id = 3,
    partition_name = 'enic',
    partition_state = 0,
})
