--parameter_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local parameter = config_mgr:get_table("parameter")

--导出版本号
parameter:set_version(10000)

--导出配置内容
parameter:upsert({
    parameter_id = 1201,
    para_value = '259200',
})

parameter:upsert({
    parameter_id = 1202,
    para_value = '7',
})

parameter:upsert({
    parameter_id = 1203,
    para_value = '2',
})

parameter:upsert({
    parameter_id = 1204,
    para_value = '5',
})

parameter:upsert({
    parameter_id = 1999,
    para_value = '20004',
})

parameter:upsert({
    parameter_id = 2001,
    para_value = '300',
})

parameter:upsert({
    parameter_id = 2002,
    para_value = '1680',
})

parameter:upsert({
    parameter_id = 2003,
    para_value = '20',
})

parameter:upsert({
    parameter_id = 2004,
    para_value = '4',
})

parameter:upsert({
    parameter_id = 3001,
    para_value = '60',
})

parameter:upsert({
    parameter_id = 3002,
    para_value = '60',
})

parameter:upsert({
    parameter_id = 3003,
    para_value = '100',
})

parameter:upsert({
    parameter_id = 4001,
    para_value = '0#3#4#5#6#7#8',
})

parameter:upsert({
    parameter_id = 4002,
    para_value = '105',
})

parameter:upsert({
    parameter_id = 5001,
    para_value = '50',
})

parameter:upsert({
    parameter_id = 5002,
    para_value = '10',
})

parameter:upsert({
    parameter_id = 5003,
    para_value = '0.1',
})

parameter:upsert({
    parameter_id = 5004,
    para_value = '100',
})

parameter:upsert({
    parameter_id = 5005,
    para_value = '3',
})

parameter:upsert({
    parameter_id = 5006,
    para_value = '60',
})

parameter:upsert({
    parameter_id = 6001,
    para_value = '50',
})

parameter:upsert({
    parameter_id = 6002,
    para_value = '900000',
})

parameter:upsert({
    parameter_id = 6003,
    para_value = '3000',
})

parameter:upsert({
    parameter_id = 6004,
    para_value = '3000',
})

parameter:upsert({
    parameter_id = 6005,
    para_value = '15000',
})

parameter:upsert({
    parameter_id = 6101,
    para_value = '160000',
})

parameter:upsert({
    parameter_id = 6102,
    para_value = '50000',
})

parameter:upsert({
    parameter_id = 6103,
    para_value = '6000',
})

parameter:upsert({
    parameter_id = 6104,
    para_value = '8000',
})

parameter:upsert({
    parameter_id = 6901,
    para_value = '团队竞技',
})

parameter:upsert({
    parameter_id = 6902,
    para_value = '5v5团队竞赛\n\n胜利条件：\n1.率先击败50个敌人\n2.限定时间到时，击败了更多的敌人\n\n复活等待时间 3秒\n单局时间总长 15分钟',
})

parameter:upsert({
    parameter_id = 6903,
    para_value = '爆破玩法',
})

parameter:upsert({
    parameter_id = 6904,
    para_value = '5v5攻守对战\n\n胜利条件：\n1.进攻方：使用P-4装置炸毁A或B区域。\n2.防守方：保护A和B区域。\n\n单回合内无法重生\n每回合时长 3分钟',
})

parameter:upsert({
    parameter_id = 6905,
    para_value = '占塔玩法',
})

parameter:upsert({
    parameter_id = 6906,
    para_value = '记得补充文字，内容在Parameter表中',
})

parameter:upsert({
    parameter_id = 6907,
    para_value = '道具赛',
})

parameter:upsert({
    parameter_id = 6908,
    para_value = '记得补充文字，内容在Parameter表中',
})

parameter:upsert({
    parameter_id = 7000,
    para_value = '4',
})

parameter:upsert({
    parameter_id = 7001,
    para_value = '1.5',
})

parameter:upsert({
    parameter_id = 7002,
})

parameter:upsert({
    parameter_id = 7003,
})

parameter:upsert({
    parameter_id = 7004,
})

parameter:upsert({
    parameter_id = 7005,
})

parameter:upsert({
    parameter_id = 7006,
})

parameter:upsert({
    parameter_id = 7007,
})

parameter:upsert({
    parameter_id = 7008,
})

parameter:upsert({
    parameter_id = 7009,
})

parameter:upsert({
    parameter_id = 7010,
})

parameter:upsert({
    parameter_id = 7011,
})

parameter:upsert({
    parameter_id = 7012,
})

parameter:upsert({
    parameter_id = 7013,
})

parameter:upsert({
    parameter_id = 7014,
    para_value = '1500',
})

parameter:upsert({
    parameter_id = 7015,
    para_value = '500',
})

parameter:upsert({
    parameter_id = 7016,
    para_value = '20',
})

parameter:upsert({
    parameter_id = 7901,
    para_value = '20',
})

parameter:upsert({
    parameter_id = 8001,
    para_value = '3600',
})

parameter:upsert({
    parameter_id = 8002,
    para_value = '1296000',
})

parameter:upsert({
    parameter_id = 8101,
    para_value = '19101',
})

parameter:upsert({
    parameter_id = 8102,
    para_value = '19201',
})

parameter:upsert({
    parameter_id = 8103,
    para_value = '19301',
})
