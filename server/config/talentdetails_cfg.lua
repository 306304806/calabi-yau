--talentdetails_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local talentdetails = config_mgr:get_table("talentdetails")

--导出版本号
talentdetails:set_version(10000)

--导出配置内容
talentdetails:upsert({
    talent_id = 1,
    talent_lv = 0,
    talent_name = '《拾穗者》',
    talent_price = 1000,
    talent_param = 0,
})

talentdetails:upsert({
    talent_id = 1,
    talent_lv = 1,
    talent_name = '《拾穗者》',
    talent_price = 2000,
    talent_param = 100,
})

talentdetails:upsert({
    talent_id = 1,
    talent_lv = 2,
    talent_name = '《拾穗者》',
    talent_price = 5000,
    talent_param = 200,
})

talentdetails:upsert({
    talent_id = 1,
    talent_lv = 3,
    talent_name = '《拾穗者》',
    talent_price = 0,
    talent_param = 300,
})

talentdetails:upsert({
    talent_id = 2,
    talent_lv = 0,
    talent_name = '《雅典学院》',
    talent_price = 1000,
    talent_param = 0,
})

talentdetails:upsert({
    talent_id = 2,
    talent_lv = 1,
    talent_name = '《雅典学院》',
    talent_price = 2000,
    talent_param = 100,
})

talentdetails:upsert({
    talent_id = 2,
    talent_lv = 2,
    talent_name = '《雅典学院》',
    talent_price = 5000,
    talent_param = 200,
})

talentdetails:upsert({
    talent_id = 2,
    talent_lv = 3,
    talent_name = '《雅典学院》',
    talent_price = 0,
    talent_param = 300,
})

talentdetails:upsert({
    talent_id = 3,
    talent_lv = 0,
    talent_name = '《自由引导人民》',
    talent_price = 1000,
    talent_param = 0,
})

talentdetails:upsert({
    talent_id = 3,
    talent_lv = 1,
    talent_name = '《自由引导人民》',
    talent_price = 2000,
    talent_param = 5,
})

talentdetails:upsert({
    talent_id = 3,
    talent_lv = 2,
    talent_name = '《自由引导人民》',
    talent_price = 5000,
    talent_param = 10,
})

talentdetails:upsert({
    talent_id = 3,
    talent_lv = 3,
    talent_name = '《自由引导人民》',
    talent_price = 0,
    talent_param = 20,
})

talentdetails:upsert({
    talent_id = 4,
    talent_lv = 0,
    talent_name = '《迪克西的研究》',
    talent_price = 1000,
    talent_param = 0,
})

talentdetails:upsert({
    talent_id = 4,
    talent_lv = 1,
    talent_name = '《迪克西的研究》',
    talent_price = 2000,
    talent_param = 5,
})

talentdetails:upsert({
    talent_id = 4,
    talent_lv = 2,
    talent_name = '《迪克西的研究》',
    talent_price = 5000,
    talent_param = 10,
})

talentdetails:upsert({
    talent_id = 4,
    talent_lv = 3,
    talent_name = '《迪克西的研究》',
    talent_price = 0,
    talent_param = 20,
})

talentdetails:upsert({
    talent_id = 5,
    talent_lv = 0,
    talent_name = '《阿利雅塔医生》',
    talent_price = 1000,
    talent_param = 0,
})

talentdetails:upsert({
    talent_id = 5,
    talent_lv = 1,
    talent_name = '《阿利雅塔医生》',
    talent_price = 2000,
    talent_param = 500,
})

talentdetails:upsert({
    talent_id = 5,
    talent_lv = 2,
    talent_name = '《阿利雅塔医生》',
    talent_price = 5000,
    talent_param = 1000,
})

talentdetails:upsert({
    talent_id = 5,
    talent_lv = 3,
    talent_name = '《阿利雅塔医生》',
    talent_price = 0,
    talent_param = 2000,
})

talentdetails:upsert({
    talent_id = 6,
    talent_lv = 0,
    talent_name = '《启程狩猎》',
    talent_price = 1000,
    talent_param = 0,
})

talentdetails:upsert({
    talent_id = 6,
    talent_lv = 1,
    talent_name = '《启程狩猎》',
    talent_price = 2000,
    talent_param = 100,
})

talentdetails:upsert({
    talent_id = 6,
    talent_lv = 2,
    talent_name = '《启程狩猎》',
    talent_price = 5000,
    talent_param = 200,
})

talentdetails:upsert({
    talent_id = 6,
    talent_lv = 3,
    talent_name = '《启程狩猎》',
    talent_price = 0,
    talent_param = 300,
})

talentdetails:upsert({
    talent_id = 7,
    talent_lv = 0,
    talent_name = '《平安祈愿》',
    talent_price = 1000,
    talent_param = 0,
})

talentdetails:upsert({
    talent_id = 7,
    talent_lv = 1,
    talent_name = '《平安祈愿》',
    talent_price = 2000,
    talent_param = 100,
})

talentdetails:upsert({
    talent_id = 7,
    talent_lv = 2,
    talent_name = '《平安祈愿》',
    talent_price = 5000,
    talent_param = 200,
})

talentdetails:upsert({
    talent_id = 7,
    talent_lv = 3,
    talent_name = '《平安祈愿》',
    talent_price = 0,
    talent_param = 300,
})

talentdetails:upsert({
    talent_id = 8,
    talent_lv = 0,
    talent_name = '《老手》',
    talent_price = 1000,
    talent_param = 0,
})

talentdetails:upsert({
    talent_id = 8,
    talent_lv = 1,
    talent_name = '《老手》',
    talent_price = 2000,
    talent_param = 500,
})

talentdetails:upsert({
    talent_id = 8,
    talent_lv = 2,
    talent_name = '《老手》',
    talent_price = 5000,
    talent_param = 1000,
})

talentdetails:upsert({
    talent_id = 8,
    talent_lv = 3,
    talent_name = '《老手》',
    talent_price = 0,
    talent_param = 1500,
})

talentdetails:upsert({
    talent_id = 9,
    talent_lv = 0,
    talent_name = '《夜巡》',
    talent_price = 1000,
    talent_param = 0,
})

talentdetails:upsert({
    talent_id = 9,
    talent_lv = 1,
    talent_name = '《夜巡》',
    talent_price = 2000,
    talent_param = 300,
})

talentdetails:upsert({
    talent_id = 9,
    talent_lv = 2,
    talent_name = '《夜巡》',
    talent_price = 5000,
    talent_param = 600,
})

talentdetails:upsert({
    talent_id = 9,
    talent_lv = 3,
    talent_name = '《夜巡》',
    talent_price = 0,
    talent_param = 1000,
})

talentdetails:upsert({
    talent_id = 10,
    talent_lv = 0,
    talent_name = '《斩杀红龙》',
    talent_price = 1000,
    talent_param = 0,
})

talentdetails:upsert({
    talent_id = 10,
    talent_lv = 1,
    talent_name = '《斩杀红龙》',
    talent_price = 2000,
    talent_param = 200,
})

talentdetails:upsert({
    talent_id = 10,
    talent_lv = 2,
    talent_name = '《斩杀红龙》',
    talent_price = 5000,
    talent_param = 500,
})

talentdetails:upsert({
    talent_id = 10,
    talent_lv = 3,
    talent_name = '《斩杀红龙》',
    talent_price = 0,
    talent_param = 1000,
})

talentdetails:upsert({
    talent_id = 11,
    talent_lv = 0,
    talent_name = '《奔马》',
    talent_price = 1000,
    talent_param = 0,
})

talentdetails:upsert({
    talent_id = 11,
    talent_lv = 1,
    talent_name = '《奔马》',
    talent_price = 2000,
    talent_param = 200,
})

talentdetails:upsert({
    talent_id = 11,
    talent_lv = 2,
    talent_name = '《奔马》',
    talent_price = 5000,
    talent_param = 500,
})

talentdetails:upsert({
    talent_id = 11,
    talent_lv = 3,
    talent_name = '《奔马》',
    talent_price = 0,
    talent_param = 800,
})

talentdetails:upsert({
    talent_id = 12,
    talent_lv = 0,
    talent_name = '《海浪》',
    talent_price = 1000,
    talent_param = 0,
})

talentdetails:upsert({
    talent_id = 12,
    talent_lv = 1,
    talent_name = '《海浪》',
    talent_price = 2000,
    talent_param = 200,
})

talentdetails:upsert({
    talent_id = 12,
    talent_lv = 2,
    talent_name = '《海浪》',
    talent_price = 5000,
    talent_param = 500,
})

talentdetails:upsert({
    talent_id = 12,
    talent_lv = 3,
    talent_name = '《海浪》',
    talent_price = 0,
    talent_param = 9000,
})

talentdetails:upsert({
    talent_id = 13,
    talent_lv = 0,
    talent_name = '《誓言》',
    talent_price = 1000,
    talent_param = 0,
})

talentdetails:upsert({
    talent_id = 13,
    talent_lv = 1,
    talent_name = '《誓言》',
    talent_price = 2000,
    talent_param = 500,
})

talentdetails:upsert({
    talent_id = 13,
    talent_lv = 2,
    talent_name = '《誓言》',
    talent_price = 5000,
    talent_param = 1000,
})

talentdetails:upsert({
    talent_id = 13,
    talent_lv = 3,
    talent_name = '《誓言》',
    talent_price = 0,
    talent_param = 9000,
})

talentdetails:upsert({
    talent_id = 14,
    talent_lv = 0,
    talent_name = '《阿喀琉斯的教育》',
    talent_price = 1000,
    talent_param = 0,
})

talentdetails:upsert({
    talent_id = 14,
    talent_lv = 1,
    talent_name = '《阿喀琉斯的教育》',
    talent_price = 2000,
    talent_param = 200,
})

talentdetails:upsert({
    talent_id = 14,
    talent_lv = 2,
    talent_name = '《阿喀琉斯的教育》',
    talent_price = 5000,
    talent_param = 400,
})

talentdetails:upsert({
    talent_id = 14,
    talent_lv = 3,
    talent_name = '《阿喀琉斯的教育》',
    talent_price = 0,
    talent_param = 9000,
})

talentdetails:upsert({
    talent_id = 15,
    talent_lv = 0,
    talent_name = '《猎手月神》',
    talent_price = 1000,
    talent_param = 0,
})

talentdetails:upsert({
    talent_id = 15,
    talent_lv = 1,
    talent_name = '《猎手月神》',
    talent_price = 2000,
    talent_param = 200,
})

talentdetails:upsert({
    talent_id = 15,
    talent_lv = 2,
    talent_name = '《猎手月神》',
    talent_price = 5000,
    talent_param = 300,
})

talentdetails:upsert({
    talent_id = 15,
    talent_lv = 3,
    talent_name = '《猎手月神》',
    talent_price = 0,
    talent_param = 9000,
})
