--currency_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local currency = config_mgr:get_table("currency")

--导出版本号
currency:set_version(10000)

--导出配置内容
currency:upsert({
    id = 1,
    quality = 1,
    limit_num = 0,
    limit_type = 0,
    limit_param = 0,
    gain_type = {0},
    gain_param1 = {},
})

currency:upsert({
    id = 2,
    quality = 3,
    limit_num = 0,
    limit_type = 0,
    limit_param = 0,
    gain_type = {0},
    gain_param1 = {},
})

currency:upsert({
    id = 3,
    quality = 5,
    limit_num = 5000,
    limit_type = 0,
    limit_param = 0,
    gain_type = {0},
    gain_param1 = {},
})

currency:upsert({
    id = 4,
    quality = 5,
    limit_num = 0,
    limit_type = 0,
    limit_param = 0,
    gain_type = {0},
    gain_param1 = {},
})
