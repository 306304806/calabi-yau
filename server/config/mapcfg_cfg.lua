--mapcfg_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local mapcfg = config_mgr:get_table("mapcfg")

--导出版本号
mapcfg:set_version(10000)

--导出配置内容
mapcfg:upsert({
    id = 103,
    name = '空间研究所',
    desc = '我是一句地图描述的话，这是道具地图',
    open = true,
    type = 1,
    bots = 9,
    loading = '/Game/PaperMan/Maps/Loading/Loading',
    scene = '/Game/PaperMan/Maps/Envi_Qydx/Envi_Qydx',
    scene_minimap = '/Game/PaperMan/Maps/BM_NYS/BM_NYS_MiniMap',
    minimap_camera_distance = '(X=11245.787109,Y=8859.251953,Z=-20000.000000)',
    minimap_center = '(X=0,Y=0,Z=0)',
    icon_map_mini = '103',
    icon_map_mini2 = '103',
    icon_map_intro = '103',
    icon_map_strategy = '103',
})

mapcfg:upsert({
    id = 102,
    name = '印象站',
    desc = '我是一句地图描述的话，这是团竞地图',
    open = false,
    type = 1,
    bots = 9,
    loading = '/Game/PaperMan/Maps/Loading/Loading',
    scene = '/Game/PaperMan/Maps/Envi_Qydx/Envi_Qydx',
    minimap_camera_distance = '(X=11245.787109,Y=8859.251953,Z=-20000.000000)',
    minimap_center = '(X=0,Y=0,Z=0)',
    icon_map_mini = '102',
    icon_map_mini2 = '102',
    icon_map_intro = '102',
    icon_map_strategy = '102',
})

mapcfg:upsert({
    id = 104,
    name = '道吉街道',
    desc = '我是一句地图描述的话，这是团竞地图',
    open = false,
    type = 1,
    bots = 0,
    loading = '/Game/PaperMan/Maps/Loading/Loading',
    scene = '/Game/PaperMan/Maps/Envi_Qydx/Envi_Qydx',
    minimap_camera_distance = '(X=11245.787109,Y=8859.251953,Z=-20000.000000)',
    minimap_center = '(X=0,Y=0,Z=0)',
    icon_map_mini = '102',
    icon_map_mini2 = '102',
    icon_map_intro = '102',
    icon_map_strategy = '102',
})

mapcfg:upsert({
    id = 105,
    name = '云顶大厦',
    desc = '我是一句地图描述的话，这是团竞地图',
    open = true,
    type = 1,
    bots = 0,
    loading = '/Game/PaperMan/Maps/Loading/Loading',
    scene = '/Game/PaperMan/Maps/LevelDesign/Envi_Ydds/Envi_Jdhcz_BSP_Test',
    minimap_camera_distance = '(X=11245.787109,Y=8859.251953,Z=-20000.000000)',
    minimap_center = '(X=0,Y=0,Z=0)',
    icon_map_mini = '102',
    icon_map_mini2 = '102',
    icon_map_intro = '102',
    icon_map_strategy = '102',
})

mapcfg:upsert({
    id = 106,
    name = '超弦实验室',
    desc = '白盒关卡，原型Dust_TD',
    open = false,
    type = 1,
    bots = 0,
    loading = '/Game/PaperMan/Maps/Loading/Loading',
    scene = '/Game/PaperMan/Maps/Envi_Qydx/Envi_Qydx',
    minimap_camera_distance = '(X=11245.787109,Y=8859.251953,Z=-20000.000000)',
    minimap_center = '(X=0,Y=0,Z=0)',
    icon_map_mini = '102',
    icon_map_mini2 = '102',
    icon_map_intro = '102',
    icon_map_strategy = '102',
})

mapcfg:upsert({
    id = 107,
    name = '印象站（扩大版）',
    desc = '白盒关卡，原型印象站',
    open = true,
    type = 1,
    bots = 0,
    loading = '/Game/PaperMan/Maps/Loading/Loading',
    scene = '/Game/PaperMan/Maps/LevelDesign/Envi_Jdhcz_Modify/Envi_Jdhcz_BSP_Test',
    minimap_camera_distance = '(X=11245.787109,Y=8859.251953,Z=-20000.000000)',
    minimap_center = '(X=0,Y=0,Z=0)',
    icon_map_mini = '102',
    icon_map_mini2 = '102',
    icon_map_intro = '102',
    icon_map_strategy = '102',
})

mapcfg:upsert({
    id = 108,
    name = '研究所（扩大版）',
    desc = '我是一句地图描述的话，这是道具地图',
    open = true,
    type = 1,
    bots = 0,
    loading = '/Game/PaperMan/Maps/Loading/Loading',
    scene = '/Game/PaperMan/Maps/LevelDesign/Envi_Qydx_Modify/Envi_Qydx_BSP_Test',
    minimap_camera_distance = '(X=11245.787109,Y=8859.251953,Z=-20000.000000)',
    minimap_center = '(X=0,Y=0,Z=0)',
    icon_map_mini = '103',
    icon_map_mini2 = '102',
    icon_map_intro = '103',
    icon_map_strategy = '103',
})

mapcfg:upsert({
    id = 203,
    name = '88区测试',
    desc = '我是一句地图描述的话，这是爆破地图',
    open = true,
    type = 2,
    bots = 0,
    loading = '/Game/PaperMan/Maps/Loading/Loading',
    scene = '/Game/PaperMan/Maps/LevelDesign/Envi_88Area_Modify/Envi_Umeda_Test',
    minimap_camera_distance = '(X=11245.787109,Y=8859.251953,Z=-20000.000000)',
    minimap_2d = '203',
    minimap_center = '(X=0,Y=0,Z=0)',
    minimap_worlddimension = '20000',
    minimap_zoom = '0.6',
    icon_map_mini = '203',
    icon_map_mini2 = '203',
    icon_map_intro = '203',
    icon_map_strategy = '203',
})

mapcfg:upsert({
    id = 204,
    name = '88区美术',
    desc = '我是一句地图描述的话，这是爆破地图',
    open = true,
    type = 2,
    bots = 0,
    loading = '/Game/PaperMan/Maps/Loading/Loading',
    scene = '/Game/PaperMan/Maps/Envi_Umeda/Envi_Umeda',
    scene_minimap = '/Game/PaperMan/Maps/Envi_Umeda/Envi_Umeda_Minimap',
    minimap_camera_distance = '(X=11245.787109,Y=8859.251953,Z=-20000.000000)',
    minimap_2d = '203',
    minimap_center = '(X=0,Y=0,Z=0)',
    minimap_worlddimension = '20000',
    minimap_zoom = '0.6',
    icon_map_mini = '203',
    icon_map_mini2 = '203',
    icon_map_intro = '203',
    icon_map_strategy = '203',
})

mapcfg:upsert({
    id = 998,
    name = '爆破测试-程序专用',
    desc = '我是一句地图描述的话，这是爆破地图',
    open = true,
    type = 2,
    bots = 0,
    loading = '/Game/PaperMan/Maps/Loading/Loading',
    scene = '/Game/PaperMan/Maps/DebugMaps/BombMatch/BombMatch',
    scene_minimap = '/Game/PaperMan/Maps/Envi_Umeda/Envi_Umeda_Minimap',
    minimap_camera_distance = '(X=11245.787109,Y=8859.251953,Z=-20000.000000)',
    minimap_2d = '203',
    minimap_center = '(X=0,Y=0,Z=0)',
    minimap_worlddimension = '20000',
    minimap_zoom = '0.6',
    icon_map_mini = '203',
    icon_map_mini2 = '998',
    icon_map_intro = '203',
    icon_map_strategy = '203',
})

mapcfg:upsert({
    id = 999,
    name = '团竞测试-程序专用',
    desc = '我是一句地图描述的话，这是道具地图',
    open = true,
    type = 1,
    bots = 0,
    loading = '/Game/PaperMan/Maps/Loading/Loading',
    scene = '/Game/PaperMan/Maps/DebugMaps/EmptyScene/EmptyScene',
    scene_minimap = '/Game/PaperMan/Maps/BM_NYS/BM_NYS_MiniMap',
    minimap_camera_distance = '(X=11245.787109,Y=8859.251953,Z=-20000.000000)',
    minimap_2d = '203',
    minimap_center = '(X=0,Y=0,Z=0)',
    minimap_worlddimension = '20000',
    minimap_zoom = '0.6',
    icon_map_mini = '103',
    icon_map_mini2 = '103',
    icon_map_intro = '103',
    icon_map_strategy = '103',
})
