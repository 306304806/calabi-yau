--roleposture_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local roleposture = config_mgr:get_table("roleposture")

--导出版本号
roleposture:set_version(10000)

--导出配置内容
roleposture:upsert({
    role_posture_id = 10101,
    name = '鼓掌',
    belong_role_id = 101,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 10102,
    name = '哭泣',
    belong_role_id = 101,
    quality = 2,
})

roleposture:upsert({
    role_posture_id = 10103,
    name = '拒绝',
    belong_role_id = 101,
    quality = 3,
})

roleposture:upsert({
    role_posture_id = 10104,
    name = '开心',
    belong_role_id = 101,
    quality = 3,
})

roleposture:upsert({
    role_posture_id = 10105,
    name = '生气',
    belong_role_id = 101,
    quality = 3,
})

roleposture:upsert({
    role_posture_id = 10201,
    name = '鼓掌',
    belong_role_id = 102,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 10301,
    name = '鼓掌',
    belong_role_id = 103,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 10302,
    name = '哭泣',
    belong_role_id = 103,
    quality = 2,
})

roleposture:upsert({
    role_posture_id = 10303,
    name = '拒绝',
    belong_role_id = 103,
    quality = 3,
})

roleposture:upsert({
    role_posture_id = 10401,
    name = '鼓掌',
    belong_role_id = 104,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 10501,
    name = '鼓掌',
    belong_role_id = 105,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 10601,
    name = '鼓掌',
    belong_role_id = 106,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 10602,
    name = '哭泣',
    belong_role_id = 106,
    quality = 2,
})

roleposture:upsert({
    role_posture_id = 10603,
    name = '拒绝',
    belong_role_id = 106,
    quality = 3,
})

roleposture:upsert({
    role_posture_id = 10701,
    name = '鼓掌',
    belong_role_id = 107,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 10801,
    name = '鼓掌',
    belong_role_id = 108,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 10901,
    name = '鼓掌',
    belong_role_id = 109,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 11001,
    name = '鼓掌',
    belong_role_id = 110,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 11101,
    name = '鼓掌',
    belong_role_id = 111,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 11201,
    name = '鼓掌',
    belong_role_id = 112,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 11301,
    name = '鼓掌',
    belong_role_id = 113,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 11302,
    name = '哭泣',
    belong_role_id = 113,
    quality = 2,
})

roleposture:upsert({
    role_posture_id = 11303,
    name = '嘲讽',
    belong_role_id = 113,
    quality = 3,
})

roleposture:upsert({
    role_posture_id = 11401,
    name = '鼓掌',
    belong_role_id = 114,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 11501,
    name = '鼓掌',
    belong_role_id = 115,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 11601,
    name = '鼓掌',
    belong_role_id = 116,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 11701,
    name = '鼓掌',
    belong_role_id = 117,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 11801,
    name = '鼓掌',
    belong_role_id = 118,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 11901,
    name = '鼓掌',
    belong_role_id = 119,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 12001,
    name = '鼓掌',
    belong_role_id = 120,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 12101,
    name = '鼓掌',
    belong_role_id = 121,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 12201,
    name = '鼓掌',
    belong_role_id = 122,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 12301,
    name = '鼓掌',
    belong_role_id = 123,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 12401,
    name = '鼓掌',
    belong_role_id = 124,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 12501,
    name = '鼓掌',
    belong_role_id = 125,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 12601,
    name = '鼓掌',
    belong_role_id = 126,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 12701,
    name = '鼓掌',
    belong_role_id = 127,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 12801,
    name = '鼓掌',
    belong_role_id = 128,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 12901,
    name = '鼓掌',
    belong_role_id = 129,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 13001,
    name = '鼓掌',
    belong_role_id = 130,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 13101,
    name = '鼓掌',
    belong_role_id = 131,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 13201,
    name = '鼓掌',
    belong_role_id = 132,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 13301,
    name = '鼓掌',
    belong_role_id = 133,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 13401,
    name = '鼓掌',
    belong_role_id = 134,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 13501,
    name = '鼓掌',
    belong_role_id = 135,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 13601,
    name = '鼓掌',
    belong_role_id = 136,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 13701,
    name = '鼓掌',
    belong_role_id = 137,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 13801,
    name = '鼓掌',
    belong_role_id = 138,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 13901,
    name = '鼓掌',
    belong_role_id = 139,
    quality = 1,
})

roleposture:upsert({
    role_posture_id = 14001,
    name = '鼓掌',
    belong_role_id = 140,
    quality = 1,
})
