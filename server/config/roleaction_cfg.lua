--roleaction_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local roleaction = config_mgr:get_table("roleaction")

--导出版本号
roleaction:set_version(10000)

--导出配置内容
roleaction:upsert({
    role_action_id = 21101001,
    role_id = 101,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21101002,
    role_id = 101,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21101003,
    role_id = 101,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21101004,
    role_id = 101,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21101005,
    role_id = 101,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21101006,
    role_id = 101,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21102001,
    role_id = 102,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21102002,
    role_id = 102,
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21102003,
    role_id = 102,
    quality = 5,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21102004,
    role_id = 102,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21102005,
    role_id = 102,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21102006,
    role_id = 102,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21103001,
    role_id = 103,
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21103002,
    role_id = 103,
    quality = 5,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21104001,
    role_id = 104,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21105001,
    role_id = 105,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21105002,
    role_id = 105,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21105003,
    role_id = 105,
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21106001,
    role_id = 106,
    quality = 5,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21107001,
    role_id = 107,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21107002,
    role_id = 107,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21107003,
    role_id = 107,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21108001,
    role_id = 108,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21108002,
    role_id = 108,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21108003,
    role_id = 108,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21108004,
    role_id = 108,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21108005,
    role_id = 108,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21108006,
    role_id = 108,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21109001,
    role_id = 109,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21110001,
    role_id = 110,
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21111001,
    role_id = 111,
    quality = 5,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21112001,
    role_id = 112,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21113001,
    role_id = 113,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21114001,
    role_id = 114,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21115001,
    role_id = 115,
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21115002,
    role_id = 115,
    quality = 5,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21116001,
    role_id = 116,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21117001,
    role_id = 117,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21118001,
    role_id = 118,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21119001,
    role_id = 119,
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21119002,
    role_id = 119,
    quality = 5,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21120001,
    role_id = 120,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21121001,
    role_id = 121,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21122001,
    role_id = 122,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21123001,
    role_id = 123,
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21124001,
    role_id = 124,
    quality = 5,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21125001,
    role_id = 125,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21126001,
    role_id = 126,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21127001,
    role_id = 127,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21128001,
    role_id = 128,
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21128002,
    role_id = 128,
    quality = 5,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21128003,
    role_id = 128,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21129001,
    role_id = 129,
    quality = 5,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21130001,
    role_id = 130,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21131001,
    role_id = 131,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21132001,
    role_id = 132,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21132002,
    role_id = 132,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21132003,
    role_id = 132,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21132004,
    role_id = 132,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21132005,
    role_id = 132,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

roleaction:upsert({
    role_action_id = 21132006,
    role_id = 132,
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})
