--decal_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local decal = config_mgr:get_table("decal")

--导出版本号
decal:set_version(10000)

--导出配置内容
decal:upsert({
    id = 30000001,
    desc = '<Blue14>剪断虚妄，归还真实。乌尔比诺阵营专属喷漆。</>',
    quality = 1,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

decal:upsert({
    id = 30000002,
    desc = '<Blue14>忠诚勇气，正直荣耀。乌尔比诺阵营专属喷漆。</>',
    quality = 2,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

decal:upsert({
    id = 30000003,
    desc = '<Blue14>心之所向，身之所往。乌尔比诺阵营专属喷漆。</>',
    quality = 3,
    limit_num = 1,
    limit_type = 1,
    limit_param = 100,
    gain_type = {0},
    gain_param1 = {},
})

decal:upsert({
    id = 30000004,
    desc = '<Blue14>艺术是生活的镜子，若是生活丧失意义，镜子的把戏也就不会使人喜欢了。</>',
    quality = 4,
    limit_num = 1,
    limit_type = 1,
    limit_param = 0,
    gain_type = {0},
    gain_param1 = {},
})

decal:upsert({
    id = 30000005,
    desc = '<Blue14>涂鸦艺术家很早就学会了对自己的作品不能太过留恋。</>',
    quality = 1,
    limit_num = 1,
    limit_type = 1,
    limit_param = 0,
    gain_type = {0},
    gain_param1 = {},
})

decal:upsert({
    id = 30000006,
    desc = '<Blue14>来打我呀！</>',
    quality = 2,
    limit_num = 1,
    limit_type = 1,
    limit_param = 0,
    gain_type = {0},
    gain_param1 = {},
})
