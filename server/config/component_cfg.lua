--component_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local component = config_mgr:get_table("component")

--导出版本号
component:set_version(10000)

--导出配置内容
component:upsert({
    component_id = 1,
    name = '消音器',
    type = 1,
    belo_type = 1,
    belo_param = {10301001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 2,
    name = '枪口补偿器',
    type = 1,
    belo_type = 1,
    belo_param = {10301001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 3,
    name = '红点瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10301001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 4,
    name = '全息瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10301001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 5,
    name = '1.5倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10301001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 6,
    name = '2倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10301001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 7,
    name = '3倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10301001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 8,
    name = '4倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10301001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 9,
    name = '轻型握把',
    type = 3,
    belo_type = 1,
    belo_param = {10301001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 10,
    name = '垂直握把',
    type = 3,
    belo_type = 1,
    belo_param = {10301001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 11,
    name = '直角前握把',
    type = 3,
    belo_type = 1,
    belo_param = {10301001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 12,
    name = '快速弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10301001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 13,
    name = '扩容弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10301001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 14,
    name = '快速扩容弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10301001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 15,
    name = '战术枪托',
    type = 5,
    belo_type = 1,
    belo_param = {10301001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 16,
    name = '消音器',
    type = 1,
    belo_type = 1,
    belo_param = {10305001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 17,
    name = '枪口补偿器',
    type = 1,
    belo_type = 1,
    belo_param = {10305001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 18,
    name = '红点瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10305001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 19,
    name = '全息瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10305001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 20,
    name = '1.5倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10305001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 21,
    name = '2倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10305001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 22,
    name = '3倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10305001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 23,
    name = '4倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10305001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 24,
    name = '快速弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10305001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 25,
    name = '扩容弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10305001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 26,
    name = '快速扩容弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10305001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 27,
    name = '消音器',
    type = 1,
    belo_type = 1,
    belo_param = {10306001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 28,
    name = '枪口补偿器',
    type = 1,
    belo_type = 1,
    belo_param = {10306001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 29,
    name = '红点瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10306001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 30,
    name = '全息瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10306001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 31,
    name = '1.5倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10306001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 32,
    name = '2倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10306001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 33,
    name = '3倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10306001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 34,
    name = '4倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10306001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 35,
    name = '轻型握把',
    type = 3,
    belo_type = 1,
    belo_param = {10306001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 36,
    name = '垂直握把',
    type = 3,
    belo_type = 1,
    belo_param = {10306001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 37,
    name = '直角前握把',
    type = 3,
    belo_type = 1,
    belo_param = {10306001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 38,
    name = '快速弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10306001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 39,
    name = '扩容弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10306001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 40,
    name = '快速扩容弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10306001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 41,
    name = '消音器',
    type = 1,
    belo_type = 1,
    belo_param = {10303001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 42,
    name = '枪口补偿器',
    type = 1,
    belo_type = 1,
    belo_param = {10303001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 43,
    name = '红点瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10303001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 44,
    name = '全息瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10303001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 45,
    name = '1.5倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10303001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 46,
    name = '2倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10303001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 47,
    name = '3倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10303001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 48,
    name = '4倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10303001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 49,
    name = '轻型握把',
    type = 3,
    belo_type = 1,
    belo_param = {10303001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 50,
    name = '垂直握把',
    type = 3,
    belo_type = 1,
    belo_param = {10303001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 51,
    name = '直角前握把',
    type = 3,
    belo_type = 1,
    belo_param = {10303001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 52,
    name = '快速弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10303001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 53,
    name = '扩容弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10303001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 54,
    name = '快速扩容弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10303001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 55,
    name = '战术枪托',
    type = 5,
    belo_type = 1,
    belo_param = {10303001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 56,
    name = '消音器',
    type = 1,
    belo_type = 1,
    belo_param = {10501001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 57,
    name = '枪口补偿器',
    type = 1,
    belo_type = 1,
    belo_param = {10501001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 58,
    name = '红点瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10501001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 59,
    name = '全息瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10501001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 60,
    name = '1.5倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10501001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 61,
    name = '2倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10501001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 62,
    name = '3倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10501001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 63,
    name = '4倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10501001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 64,
    name = '快速弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10501001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 65,
    name = '扩容弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10501001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 66,
    name = '快速扩容弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10501001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 67,
    name = '战术枪托',
    type = 5,
    belo_type = 1,
    belo_param = {10501001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 68,
    name = '消音器',
    type = 1,
    belo_type = 1,
    belo_param = {10801001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 69,
    name = '枪口补偿器',
    type = 1,
    belo_type = 1,
    belo_param = {10801001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 70,
    name = '红点瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10801001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 71,
    name = '全息瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10801001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 72,
    name = '1.5倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10801001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 73,
    name = '2倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10801001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 74,
    name = '3倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10801001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 75,
    name = '4倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10801001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 76,
    name = '轻型握把',
    type = 3,
    belo_type = 1,
    belo_param = {10801001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 77,
    name = '垂直握把',
    type = 3,
    belo_type = 1,
    belo_param = {10801001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 78,
    name = '直角前握把',
    type = 3,
    belo_type = 1,
    belo_param = {10801001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 79,
    name = '快速弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10801001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 80,
    name = '扩容弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10801001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 81,
    name = '快速扩容弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10801001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 82,
    name = '战术枪托',
    type = 5,
    belo_type = 1,
    belo_param = {10801001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 83,
    name = '消音器',
    type = 1,
    belo_type = 1,
    belo_param = {10602001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 84,
    name = '枪口补偿器',
    type = 1,
    belo_type = 1,
    belo_param = {10602001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 85,
    name = '红点瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10602001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 86,
    name = '全息瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10602001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 87,
    name = '1.5倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10602001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 88,
    name = '2倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10602001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 89,
    name = '3倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10602001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 90,
    name = '4倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10602001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 91,
    name = '快速弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10602001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 92,
    name = '扩容弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10602001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 93,
    name = '快速扩容弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10602001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 94,
    name = '消音器',
    type = 1,
    belo_type = 1,
    belo_param = {10401001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 95,
    name = '枪口补偿器',
    type = 1,
    belo_type = 1,
    belo_param = {10401001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 96,
    name = '红点瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10401001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 97,
    name = '全息瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10401001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 98,
    name = '1.5倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10401001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 99,
    name = '2倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10401001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 100,
    name = '3倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10401001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 101,
    name = '4倍瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10401001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 102,
    name = '轻型握把',
    type = 3,
    belo_type = 1,
    belo_param = {10401001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 103,
    name = '垂直握把',
    type = 3,
    belo_type = 1,
    belo_param = {10401001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 104,
    name = '直角前握把',
    type = 3,
    belo_type = 1,
    belo_param = {10401001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 105,
    name = '快速弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10401001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 106,
    name = '扩容弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10401001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 107,
    name = '快速扩容弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10401001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 108,
    name = '战术枪托',
    type = 5,
    belo_type = 1,
    belo_param = {10401001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 109,
    name = '红点瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10706001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 110,
    name = '全息瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10706001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 111,
    name = '轻型握把',
    type = 3,
    belo_type = 1,
    belo_param = {10706001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 112,
    name = '垂直握把',
    type = 3,
    belo_type = 1,
    belo_param = {10706001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 113,
    name = '直角前握把',
    type = 3,
    belo_type = 1,
    belo_param = {10706001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 114,
    name = '快速弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10706001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 115,
    name = '扩容弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10706001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 116,
    name = '快速扩容弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10706001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 117,
    name = '红点瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10903001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 118,
    name = '全息瞄准镜',
    type = 2,
    belo_type = 1,
    belo_param = {10903001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 119,
    name = '快速弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10903001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 120,
    name = '扩容弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10903001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 121,
    name = '快速扩容弹夹',
    type = 4,
    belo_type = 1,
    belo_param = {10903001},
    unlock_type = 1,
    unlock_param = {1},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 1001,
    name = 'M4A1限定瞄具',
    type = 2,
    belo_type = 1,
    belo_param = {10301001},
    unlock_type = 2,
    unlock_param = {30001},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})

component:upsert({
    component_id = 1002,
    name = '突击步枪限定瞄具',
    type = 2,
    belo_type = 2,
    belo_param = {2},
    unlock_type = 2,
    unlock_param = {30002},
    blue_print = '/Game/PaperMan/Weapons/AK47/BP/BP_AK47_Part_Sight.BP_AK47_Part_Sight',
})
