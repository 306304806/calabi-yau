--playerheadicon_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local playerheadicon = config_mgr:get_table("playerheadicon")

--导出版本号
playerheadicon:set_version(10000)

--导出配置内容
playerheadicon:upsert({
    id = 1,
    default = true,
})

playerheadicon:upsert({
    id = 2,
    default = true,
})

playerheadicon:upsert({
    id = 3,
    default = true,
})

playerheadicon:upsert({
    id = 4,
    default = true,
})

playerheadicon:upsert({
    id = 5,
    default = true,
})

playerheadicon:upsert({
    id = 6,
    default = true,
})

playerheadicon:upsert({
    id = 7,
    default = true,
})

playerheadicon:upsert({
    id = 8,
    default = true,
})

playerheadicon:upsert({
    id = 9,
    default = false,
})

playerheadicon:upsert({
    id = 10,
    default = false,
})
