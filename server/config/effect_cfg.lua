--effect_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local effect = config_mgr:get_table("effect")

--导出版本号
effect:set_version(10000)

--导出配置内容
effect:upsert({
    effect_id = 520001,
    effect_type = 202,
    parameter = {52,10000},
    continuous = true,
})

effect:upsert({
    effect_id = 520002,
    effect_type = 202,
    parameter = {52,5000},
    continuous = true,
})

effect:upsert({
    effect_id = 520003,
    effect_type = 202,
    parameter = {52,1000},
    continuous = true,
})

effect:upsert({
    effect_id = 530001,
    effect_type = 202,
    parameter = {53,10000},
    continuous = true,
})

effect:upsert({
    effect_id = 550001,
    effect_type = 202,
    parameter = {55,10000},
    continuous = true,
})

effect:upsert({
    effect_id = 10101,
    effect_type = 201,
    parameter = {10101},
    continuous = true,
})

effect:upsert({
    effect_id = 10102,
    effect_type = 201,
    parameter = {10102},
    continuous = true,
})

effect:upsert({
    effect_id = 10201,
    effect_type = 201,
    parameter = {10201},
    continuous = true,
})

effect:upsert({
    effect_id = 10202,
    effect_type = 201,
    parameter = {10202},
    continuous = true,
})

effect:upsert({
    effect_id = 10301,
    effect_type = 201,
    parameter = {10301},
    continuous = true,
})

effect:upsert({
    effect_id = 10302,
    effect_type = 201,
    parameter = {10302},
    continuous = true,
})

effect:upsert({
    effect_id = 20001,
    effect_type = 101,
    parameter = {{10101,2},{10001,10}},
    continuous = false,
})

effect:upsert({
    effect_id = 21001,
    effect_type = 102,
    parameter = {1,2,10000},
    continuous = false,
})

effect:upsert({
    effect_id = 1010,
    effect_type = 2,
    parameter = {101,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1011,
    effect_type = 2,
    parameter = {101,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1012,
    effect_type = 2,
    parameter = {101,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1013,
    effect_type = 2,
    parameter = {101,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1020,
    effect_type = 2,
    parameter = {102,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1021,
    effect_type = 2,
    parameter = {102,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1022,
    effect_type = 2,
    parameter = {102,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1023,
    effect_type = 2,
    parameter = {102,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1030,
    effect_type = 2,
    parameter = {103,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1031,
    effect_type = 2,
    parameter = {103,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1032,
    effect_type = 2,
    parameter = {103,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1033,
    effect_type = 2,
    parameter = {103,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1040,
    effect_type = 2,
    parameter = {104,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1041,
    effect_type = 2,
    parameter = {104,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1042,
    effect_type = 2,
    parameter = {104,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1043,
    effect_type = 2,
    parameter = {104,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1050,
    effect_type = 2,
    parameter = {105,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1051,
    effect_type = 2,
    parameter = {105,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1052,
    effect_type = 2,
    parameter = {105,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1053,
    effect_type = 2,
    parameter = {105,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1060,
    effect_type = 2,
    parameter = {106,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1061,
    effect_type = 2,
    parameter = {106,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1062,
    effect_type = 2,
    parameter = {106,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1063,
    effect_type = 2,
    parameter = {106,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1070,
    effect_type = 2,
    parameter = {107,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1071,
    effect_type = 2,
    parameter = {107,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1072,
    effect_type = 2,
    parameter = {107,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1073,
    effect_type = 2,
    parameter = {107,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1080,
    effect_type = 2,
    parameter = {108,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1081,
    effect_type = 2,
    parameter = {108,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1082,
    effect_type = 2,
    parameter = {108,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1083,
    effect_type = 2,
    parameter = {108,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1090,
    effect_type = 2,
    parameter = {109,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1091,
    effect_type = 2,
    parameter = {109,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1092,
    effect_type = 2,
    parameter = {109,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1093,
    effect_type = 2,
    parameter = {109,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1100,
    effect_type = 2,
    parameter = {110,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1101,
    effect_type = 2,
    parameter = {110,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1102,
    effect_type = 2,
    parameter = {110,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1103,
    effect_type = 2,
    parameter = {110,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1110,
    effect_type = 2,
    parameter = {111,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1111,
    effect_type = 2,
    parameter = {111,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1112,
    effect_type = 2,
    parameter = {111,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1113,
    effect_type = 2,
    parameter = {111,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1120,
    effect_type = 2,
    parameter = {112,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1121,
    effect_type = 2,
    parameter = {112,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1122,
    effect_type = 2,
    parameter = {112,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1123,
    effect_type = 2,
    parameter = {112,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1130,
    effect_type = 2,
    parameter = {113,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1131,
    effect_type = 2,
    parameter = {113,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1132,
    effect_type = 2,
    parameter = {113,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1133,
    effect_type = 2,
    parameter = {113,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1140,
    effect_type = 2,
    parameter = {114,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1141,
    effect_type = 2,
    parameter = {114,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1142,
    effect_type = 2,
    parameter = {114,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1143,
    effect_type = 2,
    parameter = {114,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1150,
    effect_type = 2,
    parameter = {115,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1151,
    effect_type = 2,
    parameter = {115,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1152,
    effect_type = 2,
    parameter = {115,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1153,
    effect_type = 2,
    parameter = {115,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1160,
    effect_type = 2,
    parameter = {116,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1161,
    effect_type = 2,
    parameter = {116,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1162,
    effect_type = 2,
    parameter = {116,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1163,
    effect_type = 2,
    parameter = {116,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1170,
    effect_type = 2,
    parameter = {117,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1171,
    effect_type = 2,
    parameter = {117,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1172,
    effect_type = 2,
    parameter = {117,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1173,
    effect_type = 2,
    parameter = {117,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1180,
    effect_type = 2,
    parameter = {118,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1181,
    effect_type = 2,
    parameter = {118,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1182,
    effect_type = 2,
    parameter = {118,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1183,
    effect_type = 2,
    parameter = {118,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1190,
    effect_type = 2,
    parameter = {119,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1191,
    effect_type = 2,
    parameter = {119,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1192,
    effect_type = 2,
    parameter = {119,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1193,
    effect_type = 2,
    parameter = {119,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1200,
    effect_type = 2,
    parameter = {120,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1201,
    effect_type = 2,
    parameter = {120,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1202,
    effect_type = 2,
    parameter = {120,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1203,
    effect_type = 2,
    parameter = {120,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1210,
    effect_type = 2,
    parameter = {121,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1211,
    effect_type = 2,
    parameter = {121,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1212,
    effect_type = 2,
    parameter = {121,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1213,
    effect_type = 2,
    parameter = {121,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1220,
    effect_type = 2,
    parameter = {122,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1221,
    effect_type = 2,
    parameter = {122,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1222,
    effect_type = 2,
    parameter = {122,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1223,
    effect_type = 2,
    parameter = {122,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1230,
    effect_type = 2,
    parameter = {123,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1231,
    effect_type = 2,
    parameter = {123,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1232,
    effect_type = 2,
    parameter = {123,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1233,
    effect_type = 2,
    parameter = {123,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1240,
    effect_type = 2,
    parameter = {124,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1241,
    effect_type = 2,
    parameter = {124,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1242,
    effect_type = 2,
    parameter = {124,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1243,
    effect_type = 2,
    parameter = {124,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1250,
    effect_type = 2,
    parameter = {125,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1251,
    effect_type = 2,
    parameter = {125,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1252,
    effect_type = 2,
    parameter = {125,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1253,
    effect_type = 2,
    parameter = {125,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1260,
    effect_type = 2,
    parameter = {126,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1261,
    effect_type = 2,
    parameter = {126,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1262,
    effect_type = 2,
    parameter = {126,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1263,
    effect_type = 2,
    parameter = {126,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1270,
    effect_type = 2,
    parameter = {127,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1271,
    effect_type = 2,
    parameter = {127,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1272,
    effect_type = 2,
    parameter = {127,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1273,
    effect_type = 2,
    parameter = {127,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1280,
    effect_type = 2,
    parameter = {128,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1281,
    effect_type = 2,
    parameter = {128,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1282,
    effect_type = 2,
    parameter = {128,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1283,
    effect_type = 2,
    parameter = {128,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1290,
    effect_type = 2,
    parameter = {129,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1291,
    effect_type = 2,
    parameter = {129,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1292,
    effect_type = 2,
    parameter = {129,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1293,
    effect_type = 2,
    parameter = {129,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1300,
    effect_type = 2,
    parameter = {130,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1301,
    effect_type = 2,
    parameter = {130,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1302,
    effect_type = 2,
    parameter = {130,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1303,
    effect_type = 2,
    parameter = {130,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1310,
    effect_type = 2,
    parameter = {131,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1311,
    effect_type = 2,
    parameter = {131,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1312,
    effect_type = 2,
    parameter = {131,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1313,
    effect_type = 2,
    parameter = {131,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1320,
    effect_type = 2,
    parameter = {132,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1321,
    effect_type = 2,
    parameter = {132,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1322,
    effect_type = 2,
    parameter = {132,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1323,
    effect_type = 2,
    parameter = {132,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1330,
    effect_type = 2,
    parameter = {133,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1331,
    effect_type = 2,
    parameter = {133,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1332,
    effect_type = 2,
    parameter = {133,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1333,
    effect_type = 2,
    parameter = {133,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1340,
    effect_type = 2,
    parameter = {134,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1341,
    effect_type = 2,
    parameter = {134,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1342,
    effect_type = 2,
    parameter = {134,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1343,
    effect_type = 2,
    parameter = {134,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1350,
    effect_type = 2,
    parameter = {135,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1351,
    effect_type = 2,
    parameter = {135,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1352,
    effect_type = 2,
    parameter = {135,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1353,
    effect_type = 2,
    parameter = {135,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1360,
    effect_type = 2,
    parameter = {136,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1361,
    effect_type = 2,
    parameter = {136,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1362,
    effect_type = 2,
    parameter = {136,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1363,
    effect_type = 2,
    parameter = {136,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1370,
    effect_type = 2,
    parameter = {137,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1371,
    effect_type = 2,
    parameter = {137,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1372,
    effect_type = 2,
    parameter = {137,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1373,
    effect_type = 2,
    parameter = {137,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1380,
    effect_type = 2,
    parameter = {138,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1381,
    effect_type = 2,
    parameter = {138,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1382,
    effect_type = 2,
    parameter = {138,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1383,
    effect_type = 2,
    parameter = {138,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1390,
    effect_type = 2,
    parameter = {139,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1391,
    effect_type = 2,
    parameter = {139,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1392,
    effect_type = 2,
    parameter = {139,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1393,
    effect_type = 2,
    parameter = {139,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 1400,
    effect_type = 2,
    parameter = {140,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 1401,
    effect_type = 2,
    parameter = {140,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 1402,
    effect_type = 2,
    parameter = {140,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 1403,
    effect_type = 2,
    parameter = {140,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 101010010,
    effect_type = 3,
    parameter = {10101001,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 101010011,
    effect_type = 3,
    parameter = {10101001,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 101010012,
    effect_type = 3,
    parameter = {10101001,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 101010013,
    effect_type = 3,
    parameter = {10101001,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 101010020,
    effect_type = 3,
    parameter = {10101002,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 101010021,
    effect_type = 3,
    parameter = {10101002,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 101010022,
    effect_type = 3,
    parameter = {10101002,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 101010023,
    effect_type = 3,
    parameter = {10101002,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 101010030,
    effect_type = 3,
    parameter = {10101003,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 101010031,
    effect_type = 3,
    parameter = {10101003,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 101010032,
    effect_type = 3,
    parameter = {10101003,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 101010033,
    effect_type = 3,
    parameter = {10101003,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 101020010,
    effect_type = 3,
    parameter = {10102001,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 101020011,
    effect_type = 3,
    parameter = {10102001,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 101020012,
    effect_type = 3,
    parameter = {10102001,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 101020013,
    effect_type = 3,
    parameter = {10102001,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 123010010,
    effect_type = 3,
    parameter = {12301001,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 123010011,
    effect_type = 3,
    parameter = {12301001,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 123010012,
    effect_type = 3,
    parameter = {12301001,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 123010013,
    effect_type = 3,
    parameter = {12301001,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 104030010,
    effect_type = 3,
    parameter = {10403001,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 104030011,
    effect_type = 3,
    parameter = {10403001,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 104030012,
    effect_type = 3,
    parameter = {10403001,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 104030013,
    effect_type = 3,
    parameter = {10403001,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 201010010,
    effect_type = 4,
    parameter = {20101001,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 201010011,
    effect_type = 4,
    parameter = {20101001,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 201010012,
    effect_type = 4,
    parameter = {20101001,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 201010013,
    effect_type = 4,
    parameter = {20101001,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 201010020,
    effect_type = 4,
    parameter = {20101002,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 201010021,
    effect_type = 4,
    parameter = {20101002,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 201010022,
    effect_type = 4,
    parameter = {20101002,259200},
    continuous = false,
})

effect:upsert({
    effect_id = 201010023,
    effect_type = 4,
    parameter = {20101002,604800},
    continuous = false,
})

effect:upsert({
    effect_id = 211010010,
    effect_type = 5,
    parameter = {21101001,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 211010011,
    effect_type = 5,
    parameter = {21101001,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 221010010,
    effect_type = 6,
    parameter = {22101001,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 221010011,
    effect_type = 6,
    parameter = {22101001,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 300000010,
    effect_type = 7,
    parameter = {30000001,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 300000011,
    effect_type = 7,
    parameter = {30000001,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 311010010,
    effect_type = 8,
    parameter = {31101001,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 311010011,
    effect_type = 8,
    parameter = {31101001,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 320000010,
    effect_type = 9,
    parameter = {32000001,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 320000011,
    effect_type = 9,
    parameter = {32000001,86400},
    continuous = false,
})

effect:upsert({
    effect_id = 330000010,
    effect_type = 10,
    parameter = {33000001,-1},
    continuous = false,
})

effect:upsert({
    effect_id = 330000011,
    effect_type = 10,
    parameter = {33000001,86400},
    continuous = false,
})
