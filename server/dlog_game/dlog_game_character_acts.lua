--!!!!以下代码由工具自动生成，如需修改请前往生成文件dlog2lua.lua!!!!
--dlog_game_character_acts.lua
--玩家创角
--luacheck: ignore 631

local sformat    = string.format
local log_err    = logger.err
local DlogGameCharacterActs = class()
local prop = property(DlogGameCharacterActs)
prop:accessor("table_name", "dlog_game_character_acts")

--!!!以下是公共字段，调用时自动填充!!!
prop:accessor("has_public", true)
--[[(公共字段) 格式：YYYY-MM-DD HH:MM:SS (必填)]]
prop:accessor("event_time", "")
--[[(公共字段) 游戏申请的乐逗SDK APPKEY (必填)]]
prop:accessor("game_appkey", "")
--[[(公共字段) 游戏全局唯一区服ID (必填)]]
prop:accessor("zone_id", "")
--[[(公共字段) 对外玩家Id(注：数据来自乐逗SDK中的对外OpenId)，也要兼容非乐逗体系用户id (必填)]]
prop:accessor("open_id", "")
--[[(公共字段) SDK设备ID（SDK端可获取） (必填)]]
prop:accessor("sdk_device_id", "")
--[[(公共字段) android=1、ios=2、h5=3，pc=4 (必填)]]
prop:accessor("platform", "")
--[[(公共字段) 比如：华为、OPPO、VIVO、应用宝等,接了乐逗SDK则必须是从乐逗SDK获取的渠道商ID (必填)]]
prop:accessor("channel_group_id", "")
--[[(公共字段) 各渠道下的子渠道ID，比如华为1包、华为2包…接了乐逗SDK则必须是从乐逗SDK获取的channel_id (必填)]]
prop:accessor("channel_id", "")
--[[(公共字段) 游戏版本号 (必填)]]
prop:accessor("game_version", "")
--[[(公共字段) 全局唯一 (必填)]]
prop:accessor("character_id", "")
--[[(公共字段) 角色等级 (必填)]]
prop:accessor("character_level", "")
--[[(公共字段) 当前用户VIP等级 (必填)]]
prop:accessor("vip_level", "")


--!!!以下是特有字段，需调用方填充ctx!!!
--[[(独立字段) 不允许包含"|" (必填)]]
prop:accessor("character_name", "")
--[[(独立字段) 角色操作类型：1-创建角色 2-删除角色 3-修改名称 (必填)]]
prop:accessor("opt_type", "")
--[[(独立字段) 职业编号，全局唯一 (选填)]]
prop:accessor("career_id", "")
--[[(独立字段) 职业名称，不允许包含“"|" (选填)]]
prop:accessor("career_name", "")
--[[(独立字段) 性别编号(1=男性，2=女性，0=不确定) (选填)]]
prop:accessor("gender", "")
--[[(独立字段) 安卓：IMEI / IOS:IDFA (必填)]]
prop:accessor("imei_idfa", "")
--[[(独立字段) IP (必填)]]
prop:accessor("ip", "")

function DlogGameCharacterActs:__init()
    self.special_fields = { ["character_name"] = true, ["opt_type"] = true, ["career_id"] = true, ["career_name"] = true, ["gender"] = true, ["imei_idfa"] = true, ["ip"] = true,}

    self.filter_fields = { ["character_name"] = true, ["career_name"] = true,}

end

function DlogGameCharacterActs:contruct_report_str(ctx)
    if not ctx then
        log_err("DlogGameCharacterActs report param error!")
        return
    end

    for field_name in pairs(self.special_fields) do
        if self.filter_fields and self.filter_fields[field_name] then
            self[field_name] = string.gsub(ctx.special_fields[field_name] or "", "|", "*")
        else
            self[field_name] = ctx.special_fields[field_name] or ""
        end
    end

    return sformat("%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s", self.table_name,  self.event_time, self.game_appkey, self.zone_id, self.open_id, self.sdk_device_id, self.platform, self.channel_group_id, self.channel_id, self.game_version, self.character_id, self.character_level, self.vip_level, self.character_name, self.opt_type, self.career_id, self.career_name, self.gender, self.imei_idfa, self.ip)
end

return DlogGameCharacterActs