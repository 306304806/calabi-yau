--!!!!以下代码由工具自动生成，如需修改请前往生成文件dlog2lua.lua!!!!
--dlog_game_mall.lua
--商城购买
--luacheck: ignore 631

local sformat    = string.format
local log_err    = logger.err
local DlogGameMall = class()
local prop = property(DlogGameMall)
prop:accessor("table_name", "dlog_game_mall")

--!!!以下是公共字段，调用时自动填充!!!
prop:accessor("has_public", true)
--[[(公共字段) 格式：YYYY-MM-DD HH:MM:SS (必填)]]
prop:accessor("event_time", "")
--[[(公共字段) 游戏申请的乐逗SDK APPKEY (必填)]]
prop:accessor("game_appkey", "")
--[[(公共字段) 游戏全局唯一区服ID (必填)]]
prop:accessor("zone_id", "")
--[[(公共字段) 对外玩家Id(注：数据来自乐逗SDK中的对外OpenId)，也要兼容非乐逗体系用户id (必填)]]
prop:accessor("open_id", "")
--[[(公共字段) SDK设备ID（SDK端可获取） (必填)]]
prop:accessor("sdk_device_id", "")
--[[(公共字段) android=1、ios=2、h5=3，pc=4 (必填)]]
prop:accessor("platform", "")
--[[(公共字段) 比如：华为、OPPO、VIVO、应用宝等,接了乐逗SDK则必须是从乐逗SDK获取的渠道商ID (必填)]]
prop:accessor("channel_group_id", "")
--[[(公共字段) 各渠道下的子渠道ID，比如华为1包、华为2包…接了乐逗SDK则必须是从乐逗SDK获取的channel_id (必填)]]
prop:accessor("channel_id", "")
--[[(公共字段) 游戏版本号 (必填)]]
prop:accessor("game_version", "")
--[[(公共字段) 全局唯一 (必填)]]
prop:accessor("character_id", "")
--[[(公共字段) 角色等级 (必填)]]
prop:accessor("character_level", "")
--[[(公共字段) 当前用户VIP等级 (必填)]]
prop:accessor("vip_level", "")


--!!!以下是特有字段，需调用方填充ctx!!!
--[[(独立字段) 游戏内商城类型 (必填)]]
prop:accessor("mall_type", "")
--[[(独立字段) 商城页签，比如推荐，礼包，钻石商城，充值，兑换商店 (选填)]]
prop:accessor("mall_name", "")
--[[(独立字段) 钻石/点数/公会币…. (必填)]]
prop:accessor("money_type", "")
--[[(独立字段) 消耗多少钻石/点数/…. (必填)]]
prop:accessor("money_num", "")
--[[(独立字段) 购买的物品的道具类型 (必填)]]
prop:accessor("item_type", "")
--[[(独立字段) 道具ID (必填)]]
prop:accessor("item_id", "")
--[[(独立字段) 购买的数量 (必填)]]
prop:accessor("item_num", "")
--[[(独立字段) 匹配同一操作产生的多条不同类型日志的操作ID(全局唯一) (选填)]]
prop:accessor("opt_id", "")
--[[(独立字段) 商品ID (必填)]]
prop:accessor("shop_id", "")

function DlogGameMall:__init()
    self.special_fields = { ["mall_type"] = true, ["mall_name"] = true, ["money_type"] = true, ["money_num"] = true, ["item_type"] = true, ["item_id"] = true, ["item_num"] = true, ["opt_id"] = true, ["shop_id"] = true,}

end

function DlogGameMall:contruct_report_str(ctx)
    if not ctx then
        log_err("DlogGameMall report param error!")
        return
    end

    for field_name in pairs(self.special_fields) do
        if self.filter_fields and self.filter_fields[field_name] then
            self[field_name] = string.gsub(ctx.special_fields[field_name] or "", "|", "*")
        else
            self[field_name] = ctx.special_fields[field_name] or ""
        end
    end

    return sformat("%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s", self.table_name,  self.event_time, self.game_appkey, self.zone_id, self.open_id, self.sdk_device_id, self.platform, self.channel_group_id, self.channel_id, self.game_version, self.character_id, self.character_level, self.vip_level, self.mall_type, self.mall_name, self.money_type, self.money_num, self.item_type, self.item_id, self.item_num, self.opt_id, self.shop_id)
end

return DlogGameMall