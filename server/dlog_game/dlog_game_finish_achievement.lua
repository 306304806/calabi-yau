--!!!!以下代码由工具自动生成，如需修改请前往生成文件dlog2lua.lua!!!!
--dlog_game_finish_achievement.lua
--任务成就
--luacheck: ignore 631

local sformat    = string.format
local log_err    = logger.err
local DlogGameFinishAchievement = class()
local prop = property(DlogGameFinishAchievement)
prop:accessor("table_name", "dlog_game_finish_achievement")

--!!!以下是公共字段，调用时自动填充!!!
prop:accessor("has_public", true)
--[[(公共字段) 格式：YYYY-MM-DD HH:MM:SS (必填)]]
prop:accessor("event_time", "")
--[[(公共字段) 游戏申请的乐逗SDK APPKEY (必填)]]
prop:accessor("game_appkey", "")
--[[(公共字段) 游戏全局唯一区服ID (必填)]]
prop:accessor("zone_id", "")
--[[(公共字段) 对外玩家Id(注：数据来自乐逗SDK中的对外OpenId)，也要兼容非乐逗体系用户id (必填)]]
prop:accessor("open_id", "")
--[[(公共字段) SDK设备ID（SDK端可获取） (必填)]]
prop:accessor("sdk_device_id", "")
--[[(公共字段) android=1、ios=2、h5=3，pc=4 (必填)]]
prop:accessor("platform", "")
--[[(公共字段) 比如：华为、OPPO、VIVO、应用宝等,接了乐逗SDK则必须是从乐逗SDK获取的渠道商ID (必填)]]
prop:accessor("channel_group_id", "")
--[[(公共字段) 各渠道下的子渠道ID，比如华为1包、华为2包…接了乐逗SDK则必须是从乐逗SDK获取的channel_id (必填)]]
prop:accessor("channel_id", "")
--[[(公共字段) 游戏版本号 (必填)]]
prop:accessor("game_version", "")
--[[(公共字段) 全局唯一 (必填)]]
prop:accessor("character_id", "")
--[[(公共字段) 角色等级 (必填)]]
prop:accessor("character_level", "")
--[[(公共字段) 当前用户VIP等级 (必填)]]
prop:accessor("vip_level", "")


--!!!以下是特有字段，需调用方填充ctx!!!
--[[(独立字段) 任务/成就ID (必填)]]
prop:accessor("achievement_id", "")
--[[(独立字段)  (选填)]]
prop:accessor("achievemen_name", "")
--[[(独立字段) 操作ID号,关联物品流水或代币流水 中的opt_id (选填)]]
prop:accessor("opt_id", "")
--[[(独立字段) 成就类型ID/任务类型ID (必填)]]
prop:accessor("achievement_type", "")
--[[(独立字段) 成就状态：可领取，已领取 (必填)]]
prop:accessor("achievement_status", "")
--[[(独立字段) 任务ID子参数（环数等） (选填)]]
prop:accessor("extstr3", "")

function DlogGameFinishAchievement:__init()
    self.special_fields = { ["achievement_id"] = true, ["achievemen_name"] = true, ["opt_id"] = true, ["achievement_type"] = true, ["achievement_status"] = true, ["extstr3"] = true,}

end

function DlogGameFinishAchievement:contruct_report_str(ctx)
    if not ctx then
        log_err("DlogGameFinishAchievement report param error!")
        return
    end

    for field_name in pairs(self.special_fields) do
        if self.filter_fields and self.filter_fields[field_name] then
            self[field_name] = string.gsub(ctx.special_fields[field_name] or "", "|", "*")
        else
            self[field_name] = ctx.special_fields[field_name] or ""
        end
    end

    return sformat("%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s", self.table_name,  self.event_time, self.game_appkey, self.zone_id, self.open_id, self.sdk_device_id, self.platform, self.channel_group_id, self.channel_id, self.game_version, self.character_id, self.character_level, self.vip_level, self.achievement_id, self.achievemen_name, self.opt_id, self.achievement_type, self.achievement_status, self.extstr3)
end

return DlogGameFinishAchievement