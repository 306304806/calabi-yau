--!!!!以下代码由工具自动生成，如需修改请前往生成文件dlog2lua.lua!!!!
--dlog_game_character_logout.lua
--玩家登出
--luacheck: ignore 631

local sformat    = string.format
local log_err    = logger.err
local DlogGameCharacterLogout = class()
local prop = property(DlogGameCharacterLogout)
prop:accessor("table_name", "dlog_game_character_logout")

--!!!以下是公共字段，调用时自动填充!!!
prop:accessor("has_public", true)
--[[(公共字段) 格式：YYYY-MM-DD HH:MM:SS (必填)]]
prop:accessor("event_time", "")
--[[(公共字段) 游戏申请的乐逗SDK APPKEY (必填)]]
prop:accessor("game_appkey", "")
--[[(公共字段) 游戏全局唯一区服ID (必填)]]
prop:accessor("zone_id", "")
--[[(公共字段) 对外玩家Id(注：数据来自乐逗SDK中的对外OpenId)，也要兼容非乐逗体系用户id (必填)]]
prop:accessor("open_id", "")
--[[(公共字段) SDK设备ID（SDK端可获取） (必填)]]
prop:accessor("sdk_device_id", "")
--[[(公共字段) android=1、ios=2、h5=3，pc=4 (必填)]]
prop:accessor("platform", "")
--[[(公共字段) 比如：华为、OPPO、VIVO、应用宝等,接了乐逗SDK则必须是从乐逗SDK获取的渠道商ID (必填)]]
prop:accessor("channel_group_id", "")
--[[(公共字段) 各渠道下的子渠道ID，比如华为1包、华为2包…接了乐逗SDK则必须是从乐逗SDK获取的channel_id (必填)]]
prop:accessor("channel_id", "")
--[[(公共字段) 游戏版本号 (必填)]]
prop:accessor("game_version", "")
--[[(公共字段) 全局唯一 (必填)]]
prop:accessor("character_id", "")
--[[(公共字段) 角色等级 (必填)]]
prop:accessor("character_level", "")
--[[(公共字段) 当前用户VIP等级 (必填)]]
prop:accessor("vip_level", "")


--!!!以下是特有字段，需调用方填充ctx!!!
--[[(独立字段) 不允许包含“"|" (必填)]]
prop:accessor("character_name", "")
--[[(独立字段) 单位：秒 (必填)]]
prop:accessor("online_time", "")
--[[(独立字段) 战力 (选填)]]
prop:accessor("fight_value", "")
--[[(独立字段) 玩家剩余体力 (选填)]]
prop:accessor("strength_point", "")
--[[(独立字段) 当前帮派id，默认填0 (选填)]]
prop:accessor("group_id", "")
--[[(独立字段) 经验 (选填)]]
prop:accessor("exp", "")
--[[(独立字段) 一级代币：指游戏内的主代币. 一般指充值购买兑换的主代币，比如钻石、宝石等 (必填)]]
prop:accessor("main_money_stock", "")
--[[(独立字段) 二级代币剩余量 (选填)]]
prop:accessor("sub_money_stock", "")
--[[(独立字段) json数组格式：[“{"type":'代币名称,"value":剩余量},…] (选填)]]
prop:accessor("othermoney_stock_json", "")
--[[(独立字段) 比如 关卡、任务、副本 (选填)]]
prop:accessor("scene_type", "")
--[[(独立字段) 流失场景ID (选填)]]
prop:accessor("scene_id", "")
--[[(独立字段) 地图ID (选填)]]
prop:accessor("map_id", "")
--[[(独立字段) 坐标 (选填)]]
prop:accessor("position", "")
--[[(独立字段) 人民币，单位：元 (选填)]]
prop:accessor("total_cash", "")
--[[(独立字段) 安卓：IMEI / IOS:IDFA (必填)]]
prop:accessor("imei_idfa", "")
--[[(独立字段) IP (必填)]]
prop:accessor("Ip", "")
--[[(独立字段) {"roles":[{"id":106,"level":6},{"id":113,"level":11},{"id":102,"level":4},{"id":101,"level":2},{"id":112,"level":12},{"id":104,"level":10},{"id":100,"level":4}],"pets":[{"id":1000,"level":1}]} (选填)]]
prop:accessor("character_info", "")
--[[(独立字段) 扩展字段2，上报好友数量 (选填)]]
prop:accessor("friends_nums", "")

function DlogGameCharacterLogout:__init()
    self.special_fields = { ["character_name"] = true, ["online_time"] = true, ["fight_value"] = true, ["strength_point"] = true, ["group_id"] = true, ["exp"] = true, ["main_money_stock"] = true, ["sub_money_stock"] = true, ["othermoney_stock_json"] = true, ["scene_type"] = true, ["scene_id"] = true, ["map_id"] = true, ["position"] = true, ["total_cash"] = true, ["imei_idfa"] = true, ["Ip"] = true, ["character_info"] = true, ["friends_nums"] = true,}

    self.filter_fields = { ["character_name"] = true,}

end

function DlogGameCharacterLogout:contruct_report_str(ctx)
    if not ctx then
        log_err("DlogGameCharacterLogout report param error!")
        return
    end

    for field_name in pairs(self.special_fields) do
        if self.filter_fields and self.filter_fields[field_name] then
            self[field_name] = string.gsub(ctx.special_fields[field_name] or "", "|", "*")
        else
            self[field_name] = ctx.special_fields[field_name] or ""
        end
    end

    return sformat("%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s", self.table_name,  self.event_time, self.game_appkey, self.zone_id, self.open_id, self.sdk_device_id, self.platform, self.channel_group_id, self.channel_id, self.game_version, self.character_id, self.character_level, self.vip_level, self.character_name, self.online_time, self.fight_value, self.strength_point, self.group_id, self.exp, self.main_money_stock, self.sub_money_stock, self.othermoney_stock_json, self.scene_type, self.scene_id, self.map_id, self.position, self.total_cash, self.imei_idfa, self.Ip, self.character_info, self.friends_nums)
end

return DlogGameCharacterLogout