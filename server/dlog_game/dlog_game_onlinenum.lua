--!!!!以下代码由工具自动生成，如需修改请前往生成文件dlog2lua.lua!!!!
--dlog_game_onlinenum.lua
--在线人数
--luacheck: ignore 631

local sformat    = string.format
local log_err    = logger.err
local DlogGameOnlinenum = class()
local prop = property(DlogGameOnlinenum)
prop:accessor("table_name", "dlog_game_onlinenum")



--!!!以下是特有字段，需调用方填充ctx!!!
--[[(独立字段) 格式：YYYY-MM-DD HH:MM:SS (必填)]]
prop:accessor("event_time", "")
--[[(独立字段) 游戏申请的乐逗SDK APPKEY (必填)]]
prop:accessor("game_appkey", "")
--[[(独立字段) 区ID (必填)]]
prop:accessor("zone_id", "")
--[[(独立字段) android=1、ios=2、h5=3 (必填)]]
prop:accessor("platform", "")
--[[(独立字段) 在线人数 (必填)]]
prop:accessor("online_num", "")

function DlogGameOnlinenum:__init()
    self.special_fields = { ["event_time"] = true, ["game_appkey"] = true, ["zone_id"] = true, ["platform"] = true, ["online_num"] = true,}

end

function DlogGameOnlinenum:contruct_report_str(ctx)
    if not ctx then
        log_err("DlogGameOnlinenum report param error!")
        return
    end

    for field_name in pairs(self.special_fields) do
        if self.filter_fields and self.filter_fields[field_name] then
            self[field_name] = string.gsub(ctx.special_fields[field_name] or "", "|", "*")
        else
            self[field_name] = ctx.special_fields[field_name] or ""
        end
    end

    return sformat("%s|%s|%s|%s|%s|%s", self.table_name,  self.event_time, self.game_appkey, self.zone_id, self.platform, self.online_num)
end

return DlogGameOnlinenum