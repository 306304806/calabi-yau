--!!!!以下代码由工具自动生成，如需修改请前往生成文件dlog2lua.lua!!!!
--dlog_mgr.lua
local log_debug      = logger.debug
local otime          = os.time
local odate          = os.date
local config_mgr     = quanta.get("config_mgr")
local utility_db     = config_mgr:init_table("utility", "id")
local router_mgr     = quanta.get("router_mgr")
local thread_mgr     = quanta.get("thread_mgr")

local DlogGameMall = import("dlog_game/dlog_game_mall.lua")
local DlogGameWeaponKlbq = import("dlog_game/dlog_game_weapon_klbq.lua")
local DlogGameCharacterExpup = import("dlog_game/dlog_game_character_expup.lua")
local DlogGameCharacterLogout = import("dlog_game/dlog_game_character_logout.lua")
local DlogGameMoney = import("dlog_game/dlog_game_money.lua")
local DlogGameRoleKlbq = import("dlog_game/dlog_game_role_klbq.lua")
local DlogGameFinishAchievement = import("dlog_game/dlog_game_finish_achievement.lua")
local DlogGameItem = import("dlog_game/dlog_game_item.lua")
local DlogGameUserReg = import("dlog_game/dlog_game_user_reg.lua")
local DlogGameCharacterActs = import("dlog_game/dlog_game_character_acts.lua")
local DlogGameOnlinenum = import("dlog_game/dlog_game_onlinenum.lua")
local DlogGameCharacterLogin = import("dlog_game/dlog_game_character_login.lua")
local DlogGameCharacterLevelup = import("dlog_game/dlog_game_character_levelup.lua")

local DlogMgr = singleton()

function DlogMgr:__init()
end

function DlogMgr:fill_public_fields(dlog_obj, ctx)
    dlog_obj["event_time"] = odate("%Y-%m-%d %H:%M:%S", otime())
    dlog_obj["game_appkey"] = utility_db:find_value("value", "dlog_app_key")
    dlog_obj["zone_id"] = ctx.public_fields.area_id
    dlog_obj["open_id"] = ctx.public_fields.open_id or ctx.public_fields.character_id
    dlog_obj["sdk_device_id"] = ""
    dlog_obj["platform"] = utility_db:find_value("value", "dlog_plat_id")
    dlog_obj["channel_group_id"] = ""
    dlog_obj["channel_id"] = ""
    dlog_obj["game_version"] = "1.0.0"
    dlog_obj["character_id"] = ctx.public_fields.character_id
    dlog_obj["character_level"] = ctx.public_fields.character_level
    dlog_obj["vip_level"] = ""
end


-- 商城购买
function DlogMgr:send_dlog_game_mall(ctx)
    local function send_func()
        local dlog_obj = DlogGameMall()
        if dlog_obj.has_public then
            self:fill_public_fields(dlog_obj, ctx)
        end
        local report_str = dlog_obj:contruct_report_str(ctx)
        self:send(report_str)
    end
    thread_mgr:fork(send_func)
end

-- 武器流水
function DlogMgr:send_dlog_game_weapon_klbq(ctx)
    local function send_func()
        local dlog_obj = DlogGameWeaponKlbq()
        if dlog_obj.has_public then
            self:fill_public_fields(dlog_obj, ctx)
        end
        local report_str = dlog_obj:contruct_report_str(ctx)
        self:send(report_str)
    end
    thread_mgr:fork(send_func)
end

-- 经验提升
function DlogMgr:send_dlog_game_character_expup(ctx)
    local function send_func()
        local dlog_obj = DlogGameCharacterExpup()
        if dlog_obj.has_public then
            self:fill_public_fields(dlog_obj, ctx)
        end
        local report_str = dlog_obj:contruct_report_str(ctx)
        self:send(report_str)
    end
    thread_mgr:fork(send_func)
end

-- 玩家登出
function DlogMgr:send_dlog_game_character_logout(ctx)
    local function send_func()
        local dlog_obj = DlogGameCharacterLogout()
        if dlog_obj.has_public then
            self:fill_public_fields(dlog_obj, ctx)
        end
        local report_str = dlog_obj:contruct_report_str(ctx)
        self:send(report_str)
    end
    thread_mgr:fork(send_func)
end

-- 货币流水
function DlogMgr:send_dlog_game_money(ctx)
    local function send_func()
        local dlog_obj = DlogGameMoney()
        if dlog_obj.has_public then
            self:fill_public_fields(dlog_obj, ctx)
        end
        local report_str = dlog_obj:contruct_report_str(ctx)
        self:send(report_str)
    end
    thread_mgr:fork(send_func)
end

-- 角色流水
function DlogMgr:send_dlog_game_role_klbq(ctx)
    local function send_func()
        local dlog_obj = DlogGameRoleKlbq()
        if dlog_obj.has_public then
            self:fill_public_fields(dlog_obj, ctx)
        end
        local report_str = dlog_obj:contruct_report_str(ctx)
        self:send(report_str)
    end
    thread_mgr:fork(send_func)
end

-- 任务成就
function DlogMgr:send_dlog_game_finish_achievement(ctx)
    local function send_func()
        local dlog_obj = DlogGameFinishAchievement()
        if dlog_obj.has_public then
            self:fill_public_fields(dlog_obj, ctx)
        end
        local report_str = dlog_obj:contruct_report_str(ctx)
        self:send(report_str)
    end
    thread_mgr:fork(send_func)
end

-- 道具流水
function DlogMgr:send_dlog_game_item(ctx)
    local function send_func()
        local dlog_obj = DlogGameItem()
        if dlog_obj.has_public then
            self:fill_public_fields(dlog_obj, ctx)
        end
        local report_str = dlog_obj:contruct_report_str(ctx)
        self:send(report_str)
    end
    thread_mgr:fork(send_func)
end

-- 玩家注册
function DlogMgr:send_dlog_game_user_reg(ctx)
    local function send_func()
        local dlog_obj = DlogGameUserReg()
        if dlog_obj.has_public then
            self:fill_public_fields(dlog_obj, ctx)
        end
        local report_str = dlog_obj:contruct_report_str(ctx)
        self:send(report_str)
    end
    thread_mgr:fork(send_func)
end

-- 玩家创角
function DlogMgr:send_dlog_game_character_acts(ctx)
    local function send_func()
        local dlog_obj = DlogGameCharacterActs()
        if dlog_obj.has_public then
            self:fill_public_fields(dlog_obj, ctx)
        end
        local report_str = dlog_obj:contruct_report_str(ctx)
        self:send(report_str)
    end
    thread_mgr:fork(send_func)
end

-- 在线人数
function DlogMgr:send_dlog_game_onlinenum(ctx)
    local function send_func()
        local dlog_obj = DlogGameOnlinenum()
        if dlog_obj.has_public then
            self:fill_public_fields(dlog_obj, ctx)
        end
        local report_str = dlog_obj:contruct_report_str(ctx)
        self:send(report_str)
    end
    thread_mgr:fork(send_func)
end

-- 玩家登录
function DlogMgr:send_dlog_game_character_login(ctx)
    local function send_func()
        local dlog_obj = DlogGameCharacterLogin()
        if dlog_obj.has_public then
            self:fill_public_fields(dlog_obj, ctx)
        end
        local report_str = dlog_obj:contruct_report_str(ctx)
        self:send(report_str)
    end
    thread_mgr:fork(send_func)
end

-- 等级提升
function DlogMgr:send_dlog_game_character_levelup(ctx)
    local function send_func()
        local dlog_obj = DlogGameCharacterLevelup()
        if dlog_obj.has_public then
            self:fill_public_fields(dlog_obj, ctx)
        end
        local report_str = dlog_obj:contruct_report_str(ctx)
        self:send(report_str)
    end
    thread_mgr:fork(send_func)
end

function DlogMgr:send(report_str)
    log_debug("dlog report_str:%s", report_str)
    router_mgr:call_proxy_hash(quanta.id, "rpc_http_post", utility_db:find_value("value", "dlog_report_url"), {}, report_str, {})
end

-- export
quanta.dlog_mgr    = DlogMgr()

return DlogMgr