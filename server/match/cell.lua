--cell.lua
local pairs         = pairs
local mfloor        = math.floor
local mrandom       = math.random
local tsort         = table.sort
local tinsert       = table.insert
local tremove       = table.remove
local log_info      = logger.info

local GameConst     = enum("GameConst")
local MatchConst    = enum("MatchConst")
local TEAM_SIZE     = GameConst.TEAM_SIZE

local match_mgr     = quanta.get("match_mgr")

local MatchCell = class()
local prop = property(MatchCell)
prop:accessor("id", nil)            --id
prop:accessor("rank", 0)            --rank
prop:accessor("lvl", 0)             --lvl
prop:accessor("pool", 0)            --pool
prop:accessor("mode", 0)            --mode
prop:accessor("teams", {})          --teams

function MatchCell:__init(id, mode, pool, lvl, rank)
    self.id = id
    self.rank = rank
    self.lvl = lvl
    self.mode = mode
    self.pool = pool
end

local function cmp_team(t1, t2)
    if t1:timeout() == t2:timeout() then
        return t1:size() > t2:size()
    end
    return t1:size() > t2:size()
end

function MatchCell:find_team_need()
    if self.pool == MatchConst.POOL_NEWBEE_AAI then
        local camp = mfloor(mrandom(0, 999) / 500)
        if camp == 0 then
            return 1, 0
        end
        return 0, 1
    elseif self.pool == MatchConst.POOL_NEWBEE_HAI or self.pool == MatchConst.POOL_REWARD then
        local camp = mfloor(mrandom(0, 999) / 500)
        if camp == 0 then
            return TEAM_SIZE, 0
        end
        return 0, TEAM_SIZE
    end
    return TEAM_SIZE, TEAM_SIZE
end

--适配匹配阵营
function MatchCell:random_camp(need1, need2, camp_size)
    if need1 < need2 then
        if need2 >= camp_size then
            return 2
        end
    elseif need1 > need2 then
        if need1 >= camp_size then
            return 1
        end
    else
        if need1 >= camp_size then
            return mrandom(1, 2)
        end
    end
end

--能否匹配
function MatchCell:check_match()
    tsort(self.teams, cmp_team)
    local camps = {[1] = {}, [2] = {}}
    local need1, need2 = self:find_team_need()
    for _, team in pairs(self.teams) do
        if team:finish() or team:fail() then
            goto continue
        end
        local camp_size = team:size()
        local campid = self:random_camp(need1, need2, camp_size)
        if campid then
            if campid == 1 then
                need1 = need1 - camp_size
            elseif campid == 2 then
                need2 = need2 - camp_size
            end
            tinsert(camps[campid], team)
            if need1 == 0 and need2 == 0 then
                self:match_success(camps)
                return 0
            end
        end
        ::continue::
    end
    return need1 + need2, camps
end

--删除队伍
function MatchCell:del_team(team)
    local id = team:get_id()
    for k, rteam in pairs(self.teams) do
        if rteam:get_id() == id then
            log_info("[MatchCell][del_team] team (%d) leave cell: %s)", id, self.id)
            tremove(self.teams, k)
            break
        end
    end
end

--添加队伍/残局
function MatchCell:add_team(team)
    local id = team:get_id()
    log_info("[MatchCell][add_team] team (%d) enter cell: %s)", id, self.id)
    tinsert(self.teams, team)
    team:add_cell(self)
    local need_user = self:check_match()
    return need_user == 0
end

function MatchCell:calc_robot_rank(size, avg_rank, rank_t)
    local team_num = TEAM_SIZE
    if size < team_num then
        return (avg_rank * team_num - rank_t) // (team_num - size)
    end
    return avg_rank
end

function MatchCell:build_match_camps_confg(camps)
    local players = {}
    local size_a, size_b = 0, 0
    local rank_ta, rank_tb = 0, 0
    local config = { mode = self.mode }
    for camp, teams in pairs(camps) do
        for _, team in pairs(teams) do
            for _, player in pairs(team:get_players()) do
                player.campid = camp
                tinsert(players, player)
                if camp == 1 then
                    size_a = size_a + 1
                    rank_ta = rank_ta + player.rank
                else
                    size_b = size_b + 1
                    rank_tb = rank_tb + player.rank
                end
            end
            team:match_success()
            if not config.map_id then
                config.map_id = team:get_map_id()
                config.version = team:get_version()
            end
        end
    end
    local avg_rank = (rank_ta + rank_tb) // (size_a + size_b)
    config.robot_rank_ra = self:calc_robot_rank(size_a, avg_rank, rank_ta)
    config.robot_rank_rb = self:calc_robot_rank(size_b, avg_rank, rank_tb)
    config.robot_a = TEAM_SIZE - size_a
    config.robot_b = TEAM_SIZE - size_b
    config.rank = avg_rank
    config.choose_time = GameConst.MATCH_WAIT_TIME
    return players, config
end

--匹配完成
function MatchCell:match_success(camps)
    local players, config = self:build_match_camps_confg(camps)
    match_mgr:create_room(players, config)
end

return MatchCell
