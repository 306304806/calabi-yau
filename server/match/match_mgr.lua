--match_mgr.lua
local pairs         = pairs
local new_guid      = guid.new
local tremove       = table.remove
local room_group    = guid.room_group
local serialize     = logger.serialize
local log_debug     = logger.debug

local cell_mgr      = quanta.get("cell_mgr")
local event_mgr     = quanta.get("event_mgr")
local timer_mgr     = quanta.get("timer_mgr")
local router_mgr    = quanta.get("router_mgr")

local RoomType      = enum("RoomType")
local MatchConst    = enum("MatchConst")
local PeriodTime    = enum("PeriodTime")

local MatchMgr = singleton()
local prop = property(MatchMgr)
prop:accessor("players", {})
prop:accessor("room_map", {})
prop:accessor("team_map", {})
prop:accessor("expect", MatchConst.EXPECT_TIME)
prop:accessor("expect_time", MatchConst.EXPECT_TIME)
prop:accessor("expect_count", 1)

function MatchMgr:__init()
    -- 初始化监听事件
    event_mgr:add_listener(self, "ensure_player")
    event_mgr:add_listener(self, "unsure_player")

    -- 定时器
    timer_mgr:loop(PeriodTime.SECOND_2_MS, function()
        self:update()
    end)
end

function MatchMgr:ensure_player(player_id, room_id)
    self.players[player_id] = room_id
    router_mgr:call_index_hash(player_id, "rpc_enter_room", player_id, room_id, quanta.id)
end

function MatchMgr:unsure_player(player_id, room_id)
    self.players[player_id] = nil
    router_mgr:call_index_hash(player_id, "rpc_quit_room", player_id, room_id, quanta.id)
end

--上报匹配时间
function MatchMgr:report_match_time(time, count)
    self.expect_count = self.expect_count + count
    self.expect_time = self.expect_time + time * count
    self.expect = self.expect_time // self.expect_count
    --超过次数后，复位重新计算预计时间
    if self.expect_count > MatchConst.EXPECT_COUNT then
        self.expect_time = self.expect
        self.expect_count = 1
    end
end

--匹配机器人
function MatchMgr:try_match_robot(robots)
    local robot = tremove(robots, 1)
    while robot do
        if not self:match_robot(robot) then
            break
        end
        robot = tremove(robots, 1)
    end
    return true
end

function MatchMgr:match_robot(robot)
    --匹配房间
    for _, room in pairs(self.room_map) do
        local room_need = room:need_robot()
        if room_need > 0 then
            room:robot_enter(robot)
            return true
        end
    end
    return false
end

function MatchMgr:update()
    local need_robot = 0
    local now = quanta.now
    for _, team in pairs(self.team_map) do
        self:check_team(team, now)
    end
    for room_id, room in pairs(self.room_map) do
        local room_need = room:need_robot()
        if room_need == 0 then
            if room:enter_battle() then
                self.room_map[room_id] = nil
            end
        else
            need_robot = need_robot + room_need
        end
    end
    if need_robot > 0 then
        local ok, robots = router_mgr:call_center_master("rpc_create_robot", need_robot)
        if ok and robots then
            self:try_match_robot(robots)
        end
    end
end

function MatchMgr:check_team(team, now)
    if team:fail() then
        team:clear_cells()
        self:destroy_team(team)
        return
    end
    if team:finish() then
        self:destroy_team(team)
        return
    end
    if team:check_timeout(now) then
        cell_mgr:match_timeout(team)
        return
    end
    if team:check_extra_lvl(now) and cell_mgr:extra_lvl(team) then
        return
    end
    if team:check_extra_rank(now) and cell_mgr:extra_rank(team) then
        return
    end
    if team:check_extra_pool(now) and cell_mgr:extra_pool(team) then
        return
    end
end

function MatchMgr:get_team(team_id)
    return self.team_map[team_id]
end

function MatchMgr:destroy_team(team)
    local team_id = team:get_id()
    self.team_map[team_id] = nil
    team:clear_cells()
    team:dismiss()
end

function MatchMgr:create_team(players, mode, map_id, version)
    local group = room_group(RoomType.MATCH, mode)
    local teamid = new_guid(quanta.index, group)
    log_debug("[MatchMgr][create_team]: teamid=%d, camps=%s, mode=%s", teamid, serialize(players), mode)
    local MatchTeam = import("match/team.lua")
    local team = MatchTeam(teamid, players, mode, map_id, version)
    self.team_map[teamid] = team
    cell_mgr:add_team(team)
    return teamid, team
end

function MatchMgr:create_room(players, config)
    local group = room_group(RoomType.MATCH, config.mode)
    local room_id = new_guid(quanta.index, group)
    log_debug("[MatchMgr][create_room]: room_id=%d, players=%s, config=%s", room_id, serialize(players), serialize(config))
    local MatchRoom = import("match/room.lua")
    local room = MatchRoom(room_id, players, config)
    self.room_map[room_id] = room
    return room_id, room
end

quanta.match_mgr = MatchMgr()

return MatchMgr
