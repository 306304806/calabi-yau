--match_team.lua
local ipairs        = ipairs
local tinsert       = table.insert
local log_debug     = logger.debug

local GameConst     = enum("GameConst")
local MatchConst    = enum("MatchConst")
local MatchStatus   = enum("MatchStatus")
local TEAM_SIZE     = GameConst.TEAM_SIZE

local event_mgr     = quanta.get("event_mgr")
local match_mgr     = quanta.get("match_mgr")
local router_mgr    = quanta.get("router_mgr")

local MatchTeam = class()
local prop = property(MatchTeam)
prop:reader("id")                       --id
prop:accessor("pool", 0)                --pool
prop:accessor("mode", 0)                --mode
prop:accessor("map_id", 0)              --map_id
prop:accessor("version", 0)             --version
prop:accessor("cells", {})              --cells
prop:accessor("players", {})            --players
prop:accessor("status", nil)            --status
prop:accessor("create", 0)              --create
prop:accessor("level_min", 0)           --level_min
prop:accessor("level_max", 0)           --level_max
prop:accessor("extra_pool", 0)          --extra_pool
prop:accessor("match_time_out", 0)      --match_time_out
prop:accessor("extra_lvl_time", 0)      --extra_lvl_time
prop:accessor("extra_rank_time", 0)     --extra_rank_time
prop:accessor("extra_pool_time", 0)     --extra_pool_time

function MatchTeam:__init(team_id, players, mode, map_id, version)
    self.id = team_id
    self.mode = mode
    self.map_id = map_id
    self.version = version
    self.players = players
    self:setup()
end

function MatchTeam:setup()
    local rank = 0
    local size = #self.players
    for _, player in pairs(self.players) do
        event_mgr:notify_listener("ensure_player", player.player_id, self.id)
        rank = rank + player.rank
    end
    local orig_rank = rank // size
    self.level_min = 1
    self.level_max = 1
    self.create = quanta.now
    self.rank_min = orig_rank
    self.rank_max = orig_rank
    self.rank_orig = orig_rank
    self.status = MatchStatus.WAIT
    self.pool = self:build_pool(size)
    self.extra_pool =self.pool
    --match_setup
    self:match_setup()
end

function MatchTeam:build_pool(size)
    if size == 5 then
        return MatchConst.POOL_TEAM_5
    end
    if size == 1 then
        return MatchConst.POOL_TEAM_1
    end
    return MatchConst.POOL_TEAM_2
end

function MatchTeam:match_setup()
    log_debug("[MatchTeam][match_setup]: teamid=%d", self.id)
    self.status = MatchStatus.BEGIN
    self.match_time_out = self.create + MatchConst.MATCH_TIMEOUT
    self.extra_lvl_time = self.create + MatchConst.EXTRA_LVL_TIME
    self.extra_rank_time = self.create + MatchConst.EXTRA_RANK_TIME
    self.extra_pool_time = self.create + MatchConst.EXTRA_POOL_TIME
end

function MatchTeam:size()
    return #self.players
end

function MatchTeam:dismiss()
    for _, player in pairs(self.players) do
        event_mgr:notify_listener("unsure_player", player.player_id, self.id)
    end
end

function MatchTeam:board2client(...)
    for _, player in pairs(self.players) do
        local player_id = player.player_id
        router_mgr:send_index_hash(player_id, "transfer_message", player_id, "forward_player", player_id, ...)
    end
end

function MatchTeam:get_extra_pool(extra)
    if extra and self.extra_pool > MatchConst.POOL_TEAM_1 then
        self.extra_pool = self.extra_pool - extra
    end
    return self.extra_pool
end

function MatchTeam:get_lvl_min(extra)
    local old = self.level_min
    if extra and self.level_min > 1 then
        self.level_min = self.level_min - extra
    end
    return self.level_min, old
end

function MatchTeam:get_lvl_max(extra)
    local old = self.level_max
    if extra and self.level_max < 100 then
        self.level_max = self.level_max + extra
    end
    return self.level_max, old
end

function MatchTeam:get_rank_min(extra)
    local old = self.rank_min
    if extra and self.rank_min > MatchConst.MATCH_RANK_MIN then
        self.rank_min = self.rank_min - extra
    end
    return self.rank_min, old
end

function MatchTeam:get_rank_max(extra)
    local old = self.rank_max
    if extra and self.rank_max < MatchConst.MATCH_RANK_MAX then
        self.rank_max = self.rank_max + extra
    end
    return self.rank_max, old
end

function MatchTeam:check_timeout(now)
    return now >= self.match_time_out
end

function MatchTeam:check_extra_pool(now)
    if self.extra_pool > MatchConst.POOL_TEAM_1 and now >= self.extra_pool_time then
        self.extra_pool_time = now + MatchConst.EXTRA_POOL_TIME
        return true
    end
    return false
end

function MatchTeam:check_extra_lvl(now)
    if self.level_max - self.level_min >= MatchConst.EXTRA_LVL_LIMIT then
        return false
    end
    if now >= self.extra_lvl_time then
        self.extra_lvl_time = now + MatchConst.EXTRA_LVL_TIME
        return true
    end
    return false
end

function MatchTeam:check_extra_rank(now)
    if self.rank_max - self.rank_orig >= MatchConst.EXTRA_RANK_LIMIT then
        return false
    end
    if self.rank_orig - self.rank_min >= MatchConst.EXTRA_RANK_LIMIT then
        return false
    end
    if now >= self.extra_rank_time then
        self.extra_rank_time = now + MatchConst.EXTRA_RANK_TIME
        return true
    end
    return false
end

function MatchTeam:match_timeout()
    log_debug("[MatchTeam][match_timeout] teamid=%d", self.id)
    self.status = MatchStatus.TIMEOUT
    return self:choose_cell()
end

function MatchTeam:match_cancel()
    log_debug("[MatchTeam][match_cancel] teamid=%d", self.id)
    if self.status <= MatchStatus.BEGIN then
        self.status = MatchStatus.CANCEL
        return true
    end
    return false
end

function MatchTeam:match_success()
    log_debug("[MatchTeam][match_success] teamid=%d", self.id)
    self:clear_cells()
    --上报匹配时间
    local match_time = quanta.now - self.create
    match_mgr:report_match_time(match_time, #self.players)
    self.status = MatchStatus.SUCCESS
end

function MatchTeam:finish()
    return self.status == MatchStatus.SUCCESS
end

function MatchTeam:fail()
    return self.status == MatchStatus.CANCEL
end

function MatchTeam:timeout()
    return self.status == MatchStatus.TIMEOUT
end

function MatchTeam:add_cell(cell)
    tinsert(self.cells, cell)
end


function MatchTeam:clear_cells()
    for _, cell in ipairs(self.cells) do
        cell:del_team(self)
    end
    self.cells = {}
end

function MatchTeam:choose_cell()
    local cell, camps
    local need = TEAM_SIZE * 2
    for _, f_cell in ipairs(self.cells) do
        local f_need, f_camps = f_cell:check_match()
        if f_need > 0 and f_need < need then
            cell, camps, need = f_cell, f_camps, f_need
        end
    end
    return cell, camps
end

return MatchTeam
