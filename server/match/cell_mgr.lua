--cell_mgr.lua
local sformat       = string.format
local log_info      = logger.info
local log_err       = logger.err

local GameMode      = enum("GameMode")
local MatchConst    = enum("MatchConst")

local thread_mgr    = quanta.get("thread_mgr")

local CellMgr = singleton()
local prop = property(CellMgr)
prop:accessor("cells", {})          --cells

function CellMgr:__init()
    self:init_cell()
end

--添加队伍
function CellMgr:add_team(team)
    local pool = team:get_pool()
    local lvl = team:get_lvl_min()
    local rank = team:get_rank_min()
    return self:add_team_detail(team, pool, lvl, rank)
end

--添加队伍
function CellMgr:add_team_detail(team, pool, lvl, rank)
    local mode = team:get_mode()
    local cell = self:get_cell(mode, pool, lvl, rank)
    if cell then
        return cell:add_team(team)
    end
    log_err("[CellMgr][add_team_detail] find cell failed!")
    return false
end

--扩展匹配池
function CellMgr:extra_pool(team)
    local lvl_min = team:get_lvl_min()
    local lvl_max = team:get_lvl_max()
    local rank_min = team:get_rank_min()
    local rank_max = team:get_rank_max()
    local extra_pool = team:get_extra_pool(1)
    log_info("[CellMgr][extra_pool] team(%d): pool(%d) lvl(%d, %d), rank(%d, %d)", team:get_id(), extra_pool, lvl_min, lvl_max, rank_min, rank_max)
    for lvl = lvl_min, lvl_max do
        for rank = rank_min, rank_max do
            if self:add_team_detail(team, extra_pool, lvl, rank) then
                return true
            end
        end
    end
    return false
end

--扩展lvl
function CellMgr:extra_lvl(team)
    local pool_max = team:get_pool()
    local pool_min = team:get_extra_pool()
    local rank_min = team:get_rank_min()
    local rank_max = team:get_rank_max()
    local lvl_min, old_min = team:get_lvl_min(MatchConst.MATCH_EXTRA_LVL)
    local lvl_max, old_max = team:get_lvl_max(MatchConst.MATCH_EXTRA_LVL)
    print(team:get_id(), pool_min, pool_max, lvl_min, lvl_max, rank_min, rank_max)
    log_info("[CellMgr][extra_lvl] team(%d): pool(%d, %d) lvl(%d, %d), rank(%d, %d)", team:get_id(), pool_min, pool_max, lvl_min, lvl_max, rank_min, rank_max)
    for pool = pool_min, pool_max do
        for lvl = lvl_min, old_min - 1 do
            for rank = rank_min, rank_max do
                if self:add_team_detail(team, pool, lvl, rank) then
                    return true
                end
            end
        end
        for lvl = old_max + 1, lvl_max do
            for rank = rank_min, rank_max do
                if self:add_team_detail(team, pool, lvl, rank) then
                    return true
                end
            end
        end
    end
    return false
end

--扩展rank分数
function CellMgr:extra_rank(team)
    local lvl_min = team:get_lvl_min()
    local lvl_max = team:get_lvl_max()
    local pool_max = team:get_pool()
    local pool_min = team:get_extra_pool()
    local rank_min, old_min = team:get_rank_min(MatchConst.MATCH_EXTRA_RANK)
    local rank_max, old_max = team:get_rank_max(MatchConst.MATCH_EXTRA_RANK)
    log_info("[CellMgr][extra_rank] team(%d): pool(%d, %d) lvl(%d, %d), rank(%d, %d)", team:get_id(), pool_min, pool_max, lvl_min, lvl_max, rank_min, rank_max)
    for pool = pool_min, pool_max do
        for lvl = lvl_min, lvl_max do
            for rank = rank_min, old_min - 1 do
                if self:add_team_detail(team, pool, lvl, rank) then
                    return true
                end
            end
            for rank = old_max + 1, rank_max do
                if self:add_team_detail(team, pool, lvl, rank) then
                    return true
                end
            end
        end
    end
    return false
end

--timeout
function CellMgr:match_timeout(team)
    local cell, camps = team:match_timeout()
    if cell then
        log_info("[CellMgr][match_timeout] team(%d) choose cell(%s)", team:get_id(), cell:get_id())
        cell:match_success(camps)
    end
end

--new cell id
function CellMgr:new_cell_id(mode, pool, lvl, rank)
    return sformat("cell-%d-%s-%s-%d", mode, pool, lvl, rank)
end

function CellMgr:get_cell_by_id(id)
    return self.cells[id]
end

--获取cell
function CellMgr:get_cell(mode, pool, lvl, rank)
    local id = self:new_cell_id(mode, pool, lvl, rank)
    return self:get_cell_by_id(id)
end

--初始化cell
function CellMgr:init_cell()
    thread_mgr:fork(function()
        thread_mgr:sleep(100)
        local MatchCell = import("match/cell.lua")
        for m = GameMode.TEAM, GameMode.BOMB do
            for l = MatchConst.MATCH_LV_MIN, MatchConst.MATCH_LV_MAX do
                for s = MatchConst.MATCH_RANK_MIN, MatchConst.MATCH_RANK_MAX do
                    for p = MatchConst.POOL_NEWBEE_AAI, MatchConst.POOL_TEAM_5 do
                        local id = self:new_cell_id(m, p, l, s)
                        self.cells[id] = MatchCell(id, m, p, l, s)
                    end
                end
            end
        end
    end)
end

quanta.cell_mgr = CellMgr()

return CellMgr
