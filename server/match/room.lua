--room.lua
local mmin          = math.min
local tinsert       = table.insert
local log_err       = logger.err
local log_info      = logger.info
local serialize     = logger.serialize

local GameConst     = enum("GameConst")

local router_mgr    = quanta.get("router_mgr")
local config_mgr    = quanta.get("config_mgr")
local map_db        = config_mgr:init_table("mapcfg", "id")

local MatchRoom = class()
local prop = property(MatchRoom)
prop:accessor("id", nil)        --id
prop:accessor("curnum", 0)      --curnum
prop:accessor("robot_num", 0)   --robot_num
prop:accessor("players", {})    --players
prop:accessor("try_time", 3)    --try_time
prop:accessor("trying", false)  --trying
prop:accessor("config", nil)    --config

function MatchRoom:__init(id, players, config)
    self.id = id
    self.config = config
    self.players = players
    self.curnum = #players
    self:check_config(config)
end

function MatchRoom:get_robot_rank(campid)
    local config = self.config
    if campid == 1 then
        config.robot_a = config.robot_a - 1
        return config.robot_rank_ra
    else
        config.robot_b = config.robot_b - 1
        return config.robot_rank_rb
    end
end

function MatchRoom:robot_enter(player)
    local campid = 1
    local config = self.config
    if config.robot_a < config.robot_b then
        campid = 2
    end
    self.robot_num = self.robot_num + 1
    player.campid = campid
    player.rank = self:get_robot_rank(campid)
    tinsert(self.players, player)
    return self:check_state()
end

function MatchRoom:check_state()
    self.curnum = #self.players
    if self.curnum == GameConst.TEAM_SIZE * 2 then
        return true
    end
    return false
end

function MatchRoom:need_robot()
    local cfg_item = map_db:find_one(self.config.map_id)
    local free_pos = GameConst.TEAM_SIZE * 2 - self.curnum
    return mmin(cfg_item.bots - self.robot_num, free_pos)
end

function MatchRoom:enter_battle()
    if self.trying then
        return false
    end
    if self.try_time <= 0  then
        log_err("[MatchRoom][enter_battle] failed: roomid=%d, trytime over", self.id)
        return true
    end
    self.trying = true
    self.try_time = self.try_time - 1
    --开始推送战斗
    local config = self.config
    local ok, err = router_mgr:call_room_hash(self.id, "rpc_create_room", self.players, config)
    if ok then
        log_info("[MatchRoom][enter_battle] success: id=%d, players=%s, config=%s", self.id, serialize(self.players), serialize(config))
    else
        log_err("[MatchRoom][enter_battle] failed: roomid=%d, err=%s", self.id, err)
    end
    self.trying = false
    return ok
end

function MatchRoom:check_config()
    local config = self.config
    if not config.robot_rank_ra then
        config.robot_rank_ra = config.rank
    end
    if not config.robot_rank_rb then
        config.robot_rank_rb = config.rank
    end
end

return MatchRoom
