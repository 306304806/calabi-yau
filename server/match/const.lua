--const.lua

--匹配状态
local MatchStatus = enum("MatchStatus", 0)
MatchStatus.WAIT        = 1
MatchStatus.BEGIN       = 2
MatchStatus.SUCCESS     = 3
MatchStatus.CANCEL      = 4
MatchStatus.TIMEOUT     = 5

-- 匹配常量
local MatchConst = enum("MatchConst", 0)
MatchConst.MATCH_LV_MIN         = 1     -- 匹配最小等级
MatchConst.MATCH_LV_MAX         = 1     -- 匹配最大等级
MatchConst.MATCH_RANK_MIN       = 1     -- 匹配最小RANK
MatchConst.MATCH_RANK_MAX       = 25    -- 匹配最大RANK
MatchConst.EXPECT_TIME          = 60    -- 匹配默认预计时间
MatchConst.POOL_NEWBEE_AAI      = 1     -- 新手全人机
MatchConst.POOL_NEWBEE_HAI      = 2     -- 新手半人机
MatchConst.POOL_NEWBEE_NAI      = 3     -- 新手无人机
MatchConst.POOL_REWARD          = 4     -- 福利局
MatchConst.POOL_NEWBIRD         = 5     -- 菜鸟局
MatchConst.POOL_TEAM_1          = 6     -- 1人局
MatchConst.POOL_TEAM_2          = 7     -- 2/3/4人局
MatchConst.POOL_TEAM_5          = 8     -- 5人局
MatchConst.MATCH_TIMEOUT        = 15    -- 匹配超时时间
MatchConst.MATCH_EXTRA_RANK     = 1     -- 匹配扩展RANK
MatchConst.MATCH_EXTRA_LVL      = 1     -- 匹配扩展等级
MatchConst.EXTRA_RANK_LIMIT     = 5     -- 扩展RANK限制
MatchConst.EXTRA_LVL_LIMIT      = 0     -- 扩展LVL限制
MatchConst.EXTRA_POOL_TIME      = 10    -- 匹配池扩展时间
MatchConst.EXTRA_LVL_TIME       = 10    -- 匹配LVL扩展时间
MatchConst.EXTRA_RANK_TIME      = 5     -- 匹配RANK扩展时间
MatchConst.EXPECT_COUNT         = 100   -- 匹配平均时间计算数量
