--match_servlet.lua
local log_debug     = logger.debug
local serialize     = logger.serialize

local event_mgr     = quanta.get("event_mgr")
local match_mgr     = quanta.get("match_mgr")

local KernCode      = enum("KernCode")
local PlayerCode    = enum("PlayerCode")
local SUCCESS       = KernCode.SUCCESS

local CSCmdID       = ncmd_cs.NCmdId

local MatchServlet = singleton()
function MatchServlet:__init()
    --注册事件
    event_mgr:add_listener(self, "rpc_join_match")
    event_mgr:add_listener(self, "rpc_quit_match")
end

--客户端调用
-------------------------------------------------------------------------
function MatchServlet:rpc_join_match(mode, map_id, players, version)
    log_debug("[MatchServlet][rpc_join_match] players:%s, mode:%s, map_id:%s, version:%s", serialize(players), mode, map_id, version)
    local team_id, team = match_mgr:create_team(players, mode, map_id, version)
    if team then
        team:board2client(CSCmdID.NID_MATCH_JOIN_NTF, { code = SUCCESS, ticket = team_id, expect = match_mgr:get_expect()})
    end
    return SUCCESS
end

function MatchServlet:rpc_quit_match(team_id, player_id)
    log_debug("[MatchServlet][rpc_quit_match] team_id:%s player_id:%s", team_id, player_id)
    local team = match_mgr:get_team(team_id)
    if team then
        if team:match_cancel() then
            match_mgr:destroy_team(team)
            team:board2client(CSCmdID.NID_MATCH_QUIT_NTF, { code = SUCCESS })
            return SUCCESS
        end
        return PlayerCode.PLAYER_ERR_AREADY_IN_FIGHT
    end
    return SUCCESS
end

quanta.match_servlet = MatchServlet()

return MatchServlet
