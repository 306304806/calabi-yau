empty:
	@echo "====No target! Please specify a target to make!"
	@echo "====If you want to compile all targets, use 'make core' && 'make server'"
	@echo "===='make all', which shoule be the default target is unavailable for UNKNOWN reaseon now."
	@echo "====server is composed of dbx,session,gate,name and world. You can only compile the module you need."


.PHONY: clean proj deply

all: clean proj deply

proj:
	cd quanta; make all;

deply: 
	cp quanta/bin/*.so bin/; cp quanta/bin/quanta bin/;

clean:
	rm -rf quanta/temp; rm -fr bin/*.so; rm -fr bin/quanta;
