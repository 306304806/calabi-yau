
�
lobby_role_cs.protoncmd_cs.lobbyncmd_cs.proto"#
role_cfg
role_id (RroleId"�
	role_info
role_id (RroleId
skin_id (RskinId
loved (Rloved
exp (Rexp
expire_time (R
expireTime
obtain_time (R
obtainTime
owned (Rowned"-
role_cfg_sync_req
version (	Rversion"w
role_cfg_sync_res
code (Rcode
version (	Rversion4
	diff_list (2.ncmd_cs.lobby.role_cfgRdiffList"a
role_cfg_sync_ntf
version (	Rversion2
role_cfg (2.ncmd_cs.lobby.role_cfgRroleCfg",
role_sync_req
	player_id (RplayerId"�
role_sync_res
code (Rcode
	player_id (RplayerId5
	role_list (2.ncmd_cs.lobby.role_infoRroleList,
displaying_role_id (RdisplayingRoleId"�
role_sync_ntf
	player_id (RplayerId5
	role_list (2.ncmd_cs.lobby.role_infoRroleList,
displaying_role_id (RdisplayingRoleId"1
role_skin_cfg 
role_skin_id (R
roleSkinId"2
role_skin_cfg_sync_req
version (	Rversion"�
role_skin_cfg_sync_res
code (Rcode
version (	Rversion9
	diff_list (2.ncmd_cs.lobby.role_skin_cfgRdiffList"t
role_skin_cfg_sync_ntf
version (	Rversion@
role_skin_cfg (2.ncmd_cs.lobby.role_skin_cfgRroleSkinCfg"1
role_skin_sync_req
	player_id (RplayerId"t
role_skin_info 
role_skin_id (R
roleSkinId
expire_time (R
expireTime
obtain_time (R
obtainTime"�
role_skin_sync_res
code (Rcode
	player_id (RplayerIdC
role_skin_list (2.ncmd_cs.lobby.role_skin_infoRroleSkinList"v
role_skin_sync_ntf
	player_id (RplayerIdC
role_skin_list (2.ncmd_cs.lobby.role_skin_infoRroleSkinList"F
loved_role_update_req
role_id (RroleId
loved (Rloved"Z
loved_role_update_res
code (Rcode
role_id (RroleId
loved (Rloved"5
displaying_role_update_req
role_id (RroleId"I
displaying_role_update_res
code (Rcode
role_id (RroleId"Q
role_skin_select_req
role_id (RroleId 
role_skin_id (R
roleSkinId"e
role_skin_select_res
code (Rcode
role_id (RroleId 
role_skin_id (R
roleSkinId"Q
role_skin_select_ntf
role_id (RroleId 
role_skin_id (R
roleSkinId"
week_free_roles_sync_req"
week_free_roles_update_ntf"�
week_free_roles_sync_res
code (Rcode

start_time (R	startTime
end_time (RendTime
role_ids (RroleIds"L
role_change_ntf9
change_list (2.ncmd_cs.lobby.role_infoR
changeList"V
role_skin_change_ntf>
change_list (2.ncmd_cs.lobby.role_skin_infoR
changeList"2
role_voice_sync_ntf
	voice_ids (RvoiceIds"e
role_voice_update_ntf"
add_voice_ids (RaddVoiceIds(
remove_voice_ids (RremoveVoiceIds"5
role_action_sync_ntf

action_ids (R	actionIds"j
role_action_update_ntf$
add_action_ids (RaddActionIds*
remove_action_ids (RremoveActionIds">
role_exp_sync_ntf
role_id (RroleId
exp (Rexp