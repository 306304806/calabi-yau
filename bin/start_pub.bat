taskkill /f /fi "windowtitle eq klbq_*"

start "klbq_monitor"   ./quanta.exe ./publish/monitor.conf  --index=1
start "klbq_router1"   ./quanta.exe ./publish/router.conf   --index=1
start "klbq_router2"   ./quanta.exe ./publish/router.conf   --index=2
start "klbq_router3"   ./quanta.exe ./publish/router.conf   --index=3
start "klbq_dbsvr"     ./quanta.exe ./publish/dbsvr.conf    --index=1
start "klbq_proxy"     ./quanta.exe ./publish/proxy.conf    --index=1
start "klbq_dirsvr"    ./quanta.exe ./publish/dirsvr.conf   --index=1
start "klbq_center"    ./quanta.exe ./publish/center.conf  --index=1
start "klbq_team"      ./quanta.exe ./publish/team.conf     --index=1
start "klbq_index"     ./quanta.exe ./publish/index.conf    --index=1
start "klbq_cachesvr1"  ./quanta.exe ./publish/cachesvr.conf  --index=1
start "klbq_room1"     ./quanta.exe ./publish/room.conf     --index=1
start "klbq_match1"    ./quanta.exe ./publish/match.conf    --index=1
start "klbq_lobby1"    ./quanta.exe ./publish/lobby.conf    --index=1
start "klbq_dscenter"    ./quanta.exe ./publish/dscenter.conf    --index=1
start "klbq_dsagent1"    ./quanta.exe ./publish/dsagent.conf    --index=1

exit
