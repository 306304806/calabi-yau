taskkill /f /fi "windowtitle eq klbq_*"

start "klbq_monitor"   ./quanta.exe ./local/monitor.conf  --index=1
start "klbq_router1"   ./quanta.exe ./local/router.conf   --index=1
start "klbq_router2"   ./quanta.exe ./local/router.conf   --index=2
start "klbq_router3"   ./quanta.exe ./local/router.conf   --index=3
start "klbq_dbsvr"     ./quanta.exe ./local/dbsvr.conf    --index=1
start "klbq_proxy"     ./quanta.exe ./local/proxy.conf    --index=1
start "klbq_dirsvr"    ./quanta.exe ./local/dirsvr.conf   --index=1
start "klbq_center"    ./quanta.exe ./local/center.conf  --index=1
start "klbq_team"      ./quanta.exe ./local/team.conf     --index=1
start "klbq_index"     ./quanta.exe ./local/index.conf    --index=1
start "klbq_cachesvr1"  ./quanta.exe ./local/cachesvr.conf  --index=1
start "klbq_room1"     ./quanta.exe ./local/room.conf     --index=1
start "klbq_match1"    ./quanta.exe ./local/match.conf    --index=1
start "klbq_lobby1"    ./quanta.exe ./local/lobby.conf    --index=1
start "klbq_dscenter"    ./quanta.exe ./local/dscenter.conf    --index=1
start "klbq_dsagent1"    ./quanta.exe ./local/dsagent.conf    --index=1

exit
