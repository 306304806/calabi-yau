taskkill /f /fi "windowtitle eq klbq_*"

start "klbq_monitor"   ./quanta.exe ./develop/monitor.conf  --index=1
start "klbq_router1"   ./quanta.exe ./develop/router.conf   --index=1
start "klbq_router2"   ./quanta.exe ./develop/router.conf   --index=2
start "klbq_router3"   ./quanta.exe ./develop/router.conf   --index=3
start "klbq_dbsvr"     ./quanta.exe ./develop/dbsvr.conf    --index=1
start "klbq_proxy"     ./quanta.exe ./develop/proxy.conf    --index=1
start "klbq_dirsvr"    ./quanta.exe ./develop/dirsvr.conf   --index=1
start "klbq_center"    ./quanta.exe ./develop/center.conf  --index=1
start "klbq_team"      ./quanta.exe ./develop/team.conf     --index=1
start "klbq_index"     ./quanta.exe ./develop/index.conf    --index=1
start "klbq_cachesvr1"  ./quanta.exe ./develop/cachesvr.conf  --index=1
start "klbq_room1"     ./quanta.exe ./develop/room.conf     --index=1
start "klbq_match1"    ./quanta.exe ./develop/match.conf    --index=1
start "klbq_lobby1"    ./quanta.exe ./develop/lobby.conf    --index=1
start "klbq_dscenter"    ./quanta.exe ./develop/dscenter.conf    --index=1
start "klbq_dsagent1"    ./quanta.exe ./develop/dsagent.conf    --index=1

exit
