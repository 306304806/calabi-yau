@echo off

chcp 65001

..\..\bin\quanta.exe .\dlog2lua.conf

cd ./
set CURR_PATH=%cd%
set GAME_DLOG_PATH=%CURR_PATH%\..\..\server
set PLAT_DLOG_PATH=%CURR_PATH%\..\..\..\curvity\server
echo GAME_DLOG_PATH: %GAME_DLOG_PATH%
echo PLAT_DLOG_PATH: %PLAT_DLOG_PATH%

if not exist %GAME_DLOG_PATH% (
    echo "GAME_DLOG_PATH path not exist!! please modify bat game dlog path!!"
    pause
    exit
)

if not exist %PLAT_DLOG_PATH% (
    echo %PLAT_DLOG_PATH%" path not exist!! please modify plat dlog path!!"
    pause 
)

xcopy %CURR_PATH%\dlog_game %GAME_DLOG_PATH%\dlog_game /s /y /f /i
xcopy %CURR_PATH%\dlog_plat %PLAT_DLOG_PATH%\dlog_plat /s /y /f /i

rd %CURR_PATH%\dlog_game /s /q
rd %CURR_PATH%\dlog_plat /s /q

pause

