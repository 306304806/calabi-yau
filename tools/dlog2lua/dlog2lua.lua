--convertor.lua
local lfs    = require('lfs')
local lexcel = require('luaxlsx')

local type          = type
local pairs         = pairs
local iopen         = io.open
local ogetenv       = os.getenv
local ldir          = lfs.dir
local lmkdir        = lfs.mkdir
local lcurdir       = lfs.currentdir
local ssub          = string.sub
local sfind         = string.find
local sgsub         = string.gsub
local sformat       = string.format
local tconcat       = table.concat
local tinsert       = table.insert
local slower        = string.lower
local supper        = string.upper

local slash         = "/"

local field_index_idx = 1
local field_name_idx  = 2
local field_type_idx  = 3
local field_mean_idx  = 4
local field_grade_idx = 5
local field_desc_idx  = 6
local field_filter_idx = 7

--设置utf8
if quanta.platform == "linux" then
    local locale = os.setlocale("C.UTF-8")
    if not locale then
        print("switch utf8 mode failed!")
    end
else
    local locale = os.setlocale(".UTF8")
    if not locale then
        print("switch utf8 mode failed!")
    end
    slash = "\\"
end

-- 生成类名
local function gen_class_name(table_name)
    local t = {}
    while #table_name > 0 do
        local pos = sfind(table_name, "_")
        if not pos then
            t[#t + 1] = table_name
            break
        end
        if pos > 1 then
            t[#t + 1] = ssub(table_name, 1, pos - 1)
        end
        table_name = ssub(table_name, pos + 1, #table_name)
    end
    
    local class_name = ""
    for _, data in ipairs(t) do
        class_name = class_name .. supper(ssub(data, 1, 1)) .. slower(ssub(data, 2, #data))
    end
    return class_name
end

local function gen_dlog_mgr(interfaces_data, public_fields, is_plat)
    local dlog_path = lcurdir() .. slash .. "dlog_game"
    if is_plat then
        dlog_path = lcurdir() .. slash .. "dlog_plat"
    end
    lmkdir(dlog_path)
    local filename = sformat("%s%sdlog_mgr.lua", dlog_path, slash)
    local export_file = iopen(filename, "w")
    if not export_file then
        print(sformat("gen_dlog_mgr->open output file %s failed!", filename))
        return
    end

    local class_name = "DlogMgr"
    local lines = {}
    tinsert(lines, '--!!!!以下代码由工具自动生成，如需修改请前往生成文件dlog2lua.lua!!!!')
    tinsert(lines, '--dlog_mgr.lua')
    tinsert(lines, 'local log_debug      = logger.debug')
    tinsert(lines, 'local otime          = os.time')
    tinsert(lines, 'local odate          = os.date')
    tinsert(lines, 'local config_mgr     = quanta.get("config_mgr")')
    tinsert(lines, 'local utility_db     = config_mgr:init_table("utility", "id")')
    tinsert(lines, 'local router_mgr     = quanta.get("router_mgr")')
    tinsert(lines, 'local thread_mgr     = quanta.get("thread_mgr")\n')

    for _, table_name in pairs(interfaces_data) do
        if is_plat then
            tinsert(lines, sformat('local %s = import("dlog_plat/%s.lua")', gen_class_name(table_name), table_name))
        else
            tinsert(lines, sformat('local %s = import("dlog_game/%s.lua")', gen_class_name(table_name), table_name))
        end
    end

    tinsert(lines, sformat('\nlocal %s = singleton()', class_name))
    tinsert(lines, sformat('\nfunction %s:__init()\nend\n', class_name))
    tinsert(lines, sformat('function %s:fill_public_fields(dlog_obj, ctx)', class_name))
    for _, fields in ipairs(public_fields) do
        if fields.filed_name == 'open_id' then
            tinsert(lines, sformat('    dlog_obj["%s"] = ctx.public_fields.open_id or ctx.public_fields.character_id', fields.filed_name))
        elseif fields.filed_name == 'character_id' then
            tinsert(lines, sformat('    dlog_obj["%s"] = ctx.public_fields.character_id', fields.filed_name))
        elseif fields.filed_name == 'event_time' then
            local str_time = sformat('    dlog_obj["%s"] = ', fields.filed_name) .. 'odate("%Y-%m-%d %H:%M:%S", otime())'
            tinsert(lines, str_time)
        elseif fields.filed_name == 'game_version' then
            tinsert(lines, sformat('    dlog_obj["%s"] = "1.0.0"', fields.filed_name))
        elseif fields.filed_name == 'game_appkey' then
            tinsert(lines, sformat('    dlog_obj["%s"] = utility_db:find_value("value", "dlog_app_key")', fields.filed_name))
        elseif fields.filed_name == 'platform' then
            tinsert(lines, sformat('    dlog_obj["%s"] = utility_db:find_value("value", "dlog_plat_id")', fields.filed_name))
        elseif fields.filed_name == 'zone_id' then
            tinsert(lines, sformat('    dlog_obj["%s"] = ctx.public_fields.area_id', fields.filed_name))
        elseif fields.filed_name == 'character_level' then
            tinsert(lines, sformat('    dlog_obj["%s"] = ctx.public_fields.character_level', fields.filed_name))
        else
            tinsert(lines, sformat('    dlog_obj["%s"] = ""', fields.filed_name))
        end
        
    end
    tinsert(lines, 'end\n')

    for annotation, table_name in pairs(interfaces_data) do
        tinsert(lines, sformat("\n-- %s", annotation))
        tinsert(lines, sformat('function %s:send_%s(ctx)\n    local function send_func()\n        local dlog_obj = %s()\n        if dlog_obj.has_public then\n            self:fill_public_fields(dlog_obj, ctx)\n        end\n        local report_str = dlog_obj:contruct_report_str(ctx)\n        self:send(report_str)\n    end\n    thread_mgr:fork(send_func)\nend', class_name, table_name, gen_class_name(table_name)))
    end

    local report_code = sformat('\nfunction %s:send(report_str)\n    ', class_name) .. 'log_debug("dlog report_str:%s", report_str)\n    router_mgr:call_proxy_hash(quanta.id, "rpc_http_post", utility_db:find_value("value", "dlog_report_url"), {}, report_str, {})\nend'
    tinsert(lines,report_code)

    tinsert(lines, '\n-- export')
    tinsert(lines, sformat('quanta.dlog_mgr    = %s()\n', class_name))
    tinsert(lines, sformat('return %s', class_name))
    local output_data = tconcat(lines, "\n")
    export_file:write(output_data)
    export_file:close()
end

--读取各dlog接口并生成lua文件
local function gen_dlog_lua(workbook, interfaces_data, public_fields, is_plat)
    local dlog_path = lcurdir() .. slash .. "dlog_game"
    if is_plat then
        dlog_path = lcurdir() .. slash .. "dlog_plat"
    end
    lmkdir(dlog_path)
    local sheets = workbook:sheets()
    for idx = 3, #sheets do
        local sheet = sheets[idx]
        if not sheet then break end

        local dim = sheet:dimension()
        local sheet_name = sheet:name()
        local dlog_file_name = interfaces_data[sheet_name]
        if not dlog_file_name then
            goto continue
        end
        local filename = sformat("%s%s%s.lua", dlog_path, slash, dlog_file_name)
        local export_file = iopen(filename, "w")
        if not export_file then
            print(sformat("open output file %s failed!", filename))
            break
        end

        local class_name = gen_class_name(dlog_file_name)
        local lines = {}
        tinsert(lines, '--!!!!以下代码由工具自动生成，如需修改请前往生成文件dlog2lua.lua!!!!')
        tinsert(lines, sformat('--%s.lua', dlog_file_name))
        tinsert(lines, sformat('--%s\n--luacheck: ignore 631\n', sheet_name))
        tinsert(lines, 'local sformat    = string.format')
        tinsert(lines, 'local log_err    = logger.err')
        tinsert(lines, sformat('local %s = class()', class_name))
        tinsert(lines, sformat('local prop = property(%s)', class_name))

        local prop_fields = {}
        local filter_fields = {}
        local has_public = false
        local filter_title_cell = sheet:cell(2, field_filter_idx)
        local has_filter = false
        if filter_title_cell and filter_title_cell.type ~= "blank" and sfind(filter_title_cell.value, '|') then
            has_filter = true
        end
        for row = 3, dim.lastRow do
            local index_cell = sheet:cell(row, field_index_idx)
            if not index_cell or index_cell.type == "blank" or not tonumber(index_cell.value) then
                break
            end
            local name_cell  = sheet:cell(row, field_name_idx)
            local str_name = sgsub(name_cell.value, "^%s*(.-)%s*$", "%1")
            if str_name == "public" then
                has_public = true
            else
                local type_cell  = sheet:cell(row, field_type_idx)
                local mean_cell  = sheet:cell(row, field_mean_idx)
                local grade_cell = sheet:cell(row, field_grade_idx)
                local desc_cell  = sheet:cell(row, field_desc_idx)
                
                local fileds = {filed_index = index_cell.value, filed_name = str_name, filed_type = type_cell.value, filed_mean = mean_cell.value, field_grade = grade_cell.value, field_desc = desc_cell.value}
                tinsert(prop_fields, fileds)

                if has_filter then
                    local filter_cell  = sheet:cell(row, field_filter_idx)
                    if filter_cell and filter_cell.type ~= "blank" and tonumber(filter_cell.value) then
                        filter_fields[str_name] = true
                    end
                end
            end
        end

        tinsert(lines, sformat('prop:accessor("table_name", "%s")\n', dlog_file_name))
        if has_public then
            tinsert(lines, '--!!!以下是公共字段，调用时自动填充!!!')
            tinsert(lines, 'prop:accessor("has_public", true)')
            for _, fileds in ipairs(public_fields) do
                local desc = fileds.field_desc
                if not desc or desc == "" then
                    desc = fileds.filed_mean
                end
                tinsert(lines, sformat('--[[(公共字段) %s (%s)]]', desc, fileds.field_grade))
                tinsert(lines, sformat('prop:accessor("%s", "")', fileds.filed_name))
            end
        end

        tinsert(lines, '\n\n--!!!以下是特有字段，需调用方填充ctx!!!')
        for _, fileds in ipairs(prop_fields) do
            local desc = fileds.field_desc
            if not desc or desc == "" then
                desc = fileds.filed_mean
            end
            tinsert(lines, sformat('--[[(独立字段) %s (%s)]]', desc, fileds.field_grade))
            tinsert(lines, sformat('prop:accessor("%s", "")', fileds.filed_name))
        end

        tinsert(lines, sformat('\nfunction %s:__init()', class_name))
        local special_str_line = "    self.special_fields = {"
        for _, fileds in ipairs(prop_fields) do
            special_str_line = special_str_line .. ' ["' .. fileds.filed_name .. '"] = true,'
        end
        special_str_line = special_str_line .. '}'
        tinsert(lines, sformat('%s\n', special_str_line))

        if next(filter_fields) then
            local filter_str_line = "    self.filter_fields = {"
            for filed_name in pairs(filter_fields) do
                filter_str_line = filter_str_line .. ' ["' .. filed_name .. '"] = true,'
            end
            filter_str_line = filter_str_line .. '}'
            tinsert(lines, sformat('%s\n', filter_str_line))
        end
        tinsert(lines, 'end\n')

        local str_line_pre = ''
        local str_line_back = ''
        if has_public then
            for _, fileds in ipairs(public_fields) do
                if str_line_pre == '' then
                    str_line_pre = '%s'
                else
                    str_line_pre = str_line_pre .. '|%s'
                end

                if str_line_back == '' then
                    str_line_back = ' self.' .. fileds.filed_name
                else
                    str_line_back = str_line_back .. ', self.' .. fileds.filed_name
                end
            end
        end
        for _, fileds in ipairs(prop_fields) do
            if str_line_pre == '' then
                str_line_pre = '%s'
            else
                str_line_pre = str_line_pre .. '|%s'
            end

            if str_line_back == '' then
                str_line_back = ' self.' .. fileds.filed_name
            else
                str_line_back = str_line_back .. ', self.' .. fileds.filed_name
            end
        end

        str_line_pre  = str_line_pre .. "|%s"
        str_line_back = 'self.table_name, ' .. str_line_back
        local format_line = sformat('\n    return sformat("%s", %s)', str_line_pre, str_line_back)

        tinsert(lines, sformat('function %s:contruct_report_str(ctx)\n    if not ctx then\n        log_err("%s report param error!")\n        return\n    end\n', class_name, class_name))
        tinsert(lines, sformat('    for field_name in pairs(self.special_fields) do\n        if self.filter_fields and self.filter_fields[field_name] then\n            self[field_name] = string.gsub(ctx.special_fields[field_name] or "", "|", "*")\n        else\n            self[field_name] = ctx.special_fields[field_name] or ""\n        end\n    end'))
        tinsert(lines, sformat('%s\nend', format_line))

        tinsert(lines, sformat('\nreturn %s', class_name))
        local output_data = tconcat(lines, "\n")
        export_file:write(output_data)
        export_file:close()

        :: continue ::
    end
end

-- 读取公共字段数据
local function export_public_fields(workbook)
    local sheets = workbook:sheets()
    local sheet = sheets and sheets[2]
    if not sheet then
        return false
    end
    local dim = sheet:dimension()
    local sheet_name = sheet:name()
    print("export_public_fields->sheet_name:", sheet_name)

    local public_fields = {}
    for row = 2, dim.lastRow do
        local index_cell = sheet:cell(row, field_index_idx)
        if not index_cell or index_cell.type == "blank" or not tonumber(index_cell.value) then
            break
        end
        local name_cell  = sheet:cell(row, field_name_idx)
        local type_cell  = sheet:cell(row, field_type_idx)
        local mean_cell  = sheet:cell(row, field_mean_idx)
        local grade_cell = sheet:cell(row, field_grade_idx)
        local desc_cell  = sheet:cell(row, field_desc_idx)

        local str_name = sgsub(name_cell.value, "^%s*(.-)%s*$", "%1")
        local fileds = {filed_index = index_cell.value, filed_name = str_name, filed_type = type_cell.value, filed_mean = mean_cell.value, field_grade = grade_cell.value, field_desc = desc_cell.value}
        tinsert(public_fields, fileds)
    end

    return true, public_fields
end

-- 读取接口总览数据
local function export_interfaces_view(workbook)
    local sheets = workbook:sheets()
    local sheet = sheets and sheets[1]
    if not sheet then
        return false
    end
    local dim = sheet:dimension()
    local sheet_name = sheet:name()
    print("export_interfaces_view->sheet_name:", sheet_name)

    local name_col      = 3
    local interface_col = 4
    local priority_col  = 8
    local ds_col        = 9
    local game_interfaces = {}
    local plat_interfaces = {}
    for row = 3, dim.lastRow do
        local priority_cell = sheet:cell(row, priority_col)
        if not priority_cell or priority_cell.type == "blank" or not tonumber(priority_cell.value) then
            goto continue
        end

        local ds_cell = sheet:cell(row, ds_col)
        local flag_num = tonumber(ds_cell.value)
        if not flag_num or flag_num == 1 then
            goto continue
        end

        local interface_cell = sheet:cell(row, interface_col)
        local str_interface = sgsub(interface_cell.value, "^%s*(.-)%s*$", "%1")
        if not sfind(str_interface, "dlog_game_") then
            goto continue
        end

        local name_cell = sheet:cell(row, name_col)
        local name = sgsub(name_cell.value, "^%s*(.-)%s*$", "%1")

        if flag_num == 2 then
            game_interfaces[name] = str_interface 
        elseif flag_num == 3 then
            plat_interfaces[name] = str_interface 
        end

        print("name, str_interface:", name, str_interface)

        :: continue ::
    end

    return true, game_interfaces, plat_interfaces
end

local function is_excel_file(file)
    if sfind(file, "~") then
        return false
    end
    local pos = sfind(file, "%.xlsm")
    if pos then
        return true
    end
    pos = sfind(file, "%.xlsx")
    if pos then
        return true
    end
    return false
end

--入口函数
local function export_dlogs()
    local input = lcurdir()
    for file in ldir(input) do
        if file == "." or file == ".." then
            goto continue
        end
        local full_name = input .. slash .. file
        if not is_excel_file(file) then
            goto continue
        end

        local workbook = lexcel.open(full_name)
        if not workbook then
            print(sformat("open excel %s failed!", file))
            goto continue
        end

        -- 1.读取接口总览数据
        local code, game_interfaces, plat_interfaces = export_interfaces_view(workbook)
        if not code then
            print("export_dlogs->export_interfaces_view failed!")
            break
        end

        -- 2.读取公共字段数据
        local ret, public_fields = export_public_fields(workbook)
        if not ret then
            print("export_dlogs->export_public_fields failed!")
            break
        end

        -- 3.读取各dlog接口并生成lua文件
        gen_dlog_lua(workbook, game_interfaces, public_fields)
        gen_dlog_lua(workbook, plat_interfaces, public_fields, true)

        -- 4.生成dlog_mgr
        gen_dlog_mgr(game_interfaces, public_fields)
        gen_dlog_mgr(plat_interfaces, public_fields, true)

        :: continue ::
    end
end

print("begin export dlog excel to lua!")
local ok, err = pcall(export_dlogs)
if not ok then
    print("export dlog excel to lua failed:", err)
end
print("success export dlog excel to lua!")
