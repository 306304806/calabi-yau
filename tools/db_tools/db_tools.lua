local driver            = require("driver.mongo")
local ssub              = string.sub
local sfind             = string.find

-- 获取命令行信息
local function get_options(args, confs)
    for _, arg in pairs(args) do
        if #arg > 2 and ssub(arg, 1, 2) == "--" then
            local pos = sfind(arg, "=", 1, true)
            if pos then
                local opt = ssub(arg, 3, pos - 1)
                confs[opt] = ssub(arg, pos + 1)
            end
        end
    end
    return confs
end

-- 表配置
local table_cfg = {
    game = {
        {table_name="tmp_account_platform",indexes={
                {key={open_id=1,platform_id=1},name="open_id-platform_id",unique=true},
                {key={open_id=1},name="open_id",unique=false}
            }
        },
        {table_name="account",indexes={{key={open_id=1},name="open_id",unique=true}}},
        {table_name="player_attribute",indexes={{key={player_id=1},name="player_id",unique=true}}},
        {table_name="player_bag",indexes={{key={player_id=1},name="player_id",unique=true}}},
        {table_name="player_division",indexes={{key={player_id=1},name="player_id",unique=true}}},
        {table_name="player_info",indexes={{key={player_id=1},name="player_id",unique=true}}},
        {table_name="player_rank",indexes={{key={player_id=1},name="player_id",unique=true}}},
        {table_name="player_role_info",indexes={{key={player_id=1},name="player_id",unique=true}}},
        {table_name="player_role_skin",indexes={{key={player_id=1},name="player_id",unique=true}}},
        {table_name="player_setting",indexes={{key={player_id=1},name="player_id",unique=true}}},
        {table_name="player_vcard",indexes={{key={player_id=1},name="player_id",unique=true}}},
        {table_name="player_buff",indexes={{key={player_id=1},name="player_id",unique=true}}},
    },
    plat = {
        {table_name="plat_dir_stars_rank",indexes={{key={player_id=1},name="player_id",unique=true}}},
        {table_name="plat_stars_rank",indexes={{key={player_id=1},name="player_id",unique=true}}},
        {table_name="plat_player_friend",indexes={{key={player_id=1},name="player_id",unique=true}}},
        {table_name="plat_group_mail",indexes={{key={player_id=1},name="player_id",unique=true}}},
        {table_name="plat_player_mail",indexes={{key={player_id=1},name="player_id",unique=true}}},
        {table_name="plat_player",indexes={
                {key={player_id=1},name="player_id",unique=true},
                {key={player_id=1},name="nick",unique=true},
            },
        },
        {table_name="plat_player_reddot",indexes={{key={player_id=1},name="player_id",unique=true}}},
        {table_name="plat_player_shop",indexes={{key={player_id=1},name="player_id",unique=true}}},
        {table_name="recharge_order",indexes={
                {key={player_id=1},name="player_id",unique=false},
                {key={order_id=1},name="order_id",unique=true},
                {key={pay_order_id=1},name="pay_order_id",unique=true},
                {key={insert_time=1},name="insert_time",unique=false},
            }
        },
    },
}

-- 显示帮助信息
local function show_help()
    print("db_tools: cmd list")
    print("db_tools: --op=drop --host=xx --port=xx --db_name=xx --tb_group=xx")
    print("db_tools: --op=build_index --host=xx --port=xx --db_name=xx --tb_group=xx")
    -- ..\..\quanta\bin\quanta.exe db_tools.conf ../tools/db_tools/db_tools.lua --op=drop --host=10.100.0.19 --port=27017 --db_name=paperman_x  --tb_group=game
    -- ..\..\quanta\bin\quanta.exe db_tools.conf ../tools/db_tools/db_tools.lua --op=build_index --host=10.100.0.19 --port=27017 --db_name=plat_x --tb_group=plat
end

-- 清理命令执行
local function process_drop(client, db_name, tb_group)
    local db = client:getDB(db_name)
    for _, cfg in pairs(table_cfg[tb_group] or {}) do
        local sock_err, ret = db:dropCollection(cfg.table_name);

        print(db_name, cfg.table_name, sock_err, ret)
    end
end

-- 建表+建索引
local function process_build_index(client, db_name, tb_group)
    local db = client:getDB(db_name)
    for _, cfg in pairs(table_cfg[tb_group] or {}) do
        local sock_err, ret = db:buildIndexes(cfg.table_name, cfg.indexes);

        print(db_name, cfg.table_name, sock_err, ret)
    end
end

-- 支持的命令列表
local op_map = {
    ["drop"]       = {processer=process_drop},
    ["build_index"] = {processer=process_build_index},
}

-- 处理命令
local function process_cmd()
    local cmd = {}
    cmd = get_options(quanta.args, cmd)
    if not cmd.op then
        print("error: need cmd parameter: --op")
        return
    end

    if not op_map[cmd.op] then
        print("error: unknown op parameter: ", cmd.op)
        return
    end

    if not cmd.host or not cmd.port or not cmd.db_name or not cmd.tb_group or not table_cfg[cmd.tb_group] then
        print("error: unknow db info: ")
        return
    end

    local info = op_map[cmd.op]
    if not info then
        print("error: unknown op: ", cmd.op)
        return
    end


    local cfg = {
        db   = cmd.db_name,
        host = cmd.host,
        port = cmd.port,
    }

    local ok, client = pcall(driver.client, cfg)
    if not ok then
        print("error: can not connect mongo: ", cfg.host, cfg.port)
        return
    end

    info.processer(client, cmd.db_name, cmd.tb_group)
end

-- entry
show_help()
process_cmd()
