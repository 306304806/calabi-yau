@echo off

chcp 65001

set RootDir=%~dp0
set CfgDir=%RootDir%\cfg_dir
set XlsmDir=%RootDir%\cfg_xls

:: 1.创建存放后台所需xlsm文件目录
if not exist %XlsmDir% md %XlsmDir%

if exist %CfgDir% (
    :: 2.存在目录直接svn更新策划提交配置文件
    TortoiseProc.exe /command:update /path:"%CfgDir%"  /closeonend:2
) else (    
    :: 3.创建目录并且svn checkout策划提交配置文件
    md "%CfgDir%"
    TortoiseProc.exe /command:checkout /path:"%CfgDir%" /url:"http://192.168.9.98/D1/PaperMan/Designer/策划案-已审核/配置表" /closeonend:1
)

:: 4. 拷贝后台所需xlsm文件到XlsmDir目录
for /f %%a in (export_excel_files.cfg) do (
    xcopy /Y %CfgDir%\%%a  %XlsmDir%
)

:: 5. 解析xlsm文件为lua
..\quanta\bin\quanta.exe ..\quanta\tools\excel2lua.conf --input=./cfg_xls --output=../server/config

pause

