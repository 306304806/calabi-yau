#!/bin/bash

if [ ! -n "$1" ]; then
    echo "you have input host ip!"
    exit
fi

interval=30
while true
do
    machine_info=$(vmstat -Sm 1 5|sed -n '3,$p'|awk '{x = x + $15;} {y = y + $4;} END {printf("%0.2f:%0.2f", 100-x/5, y/5)}')
    free_mem_total=${machine_info#*:}
    cpu_use_percent=${machine_info%:*}
    report_data='{"cmd_id":4,"service":15,"rpc":"on_remote_message","message":"web_report_machine_info","data":{"cpu_use_percent":'${cpu_use_percent}',"free_mem_toal":'${free_mem_total}',"host_ip":"'$1'"}}'
    echo ${report_data}
    curl --request POST --url http://localhost:9101/command --header 'content-type: application/json'  --data ${report_data}
    sleep $interval
done

